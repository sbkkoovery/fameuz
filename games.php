<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/games.php");
$objGames								=	new games();
?>
<script src="<?php echo SITE_ROOT_AWS_CDN?>js/jssor.js"></script>
<script src="<?php echo SITE_ROOT_AWS_CDN?>js/jssor.slider.js"></script>
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
  <div class="inner_top_border">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-sp-9">
      <div class="jobs-head">
        <div class="pagination_box_jobs pagination_box">
            <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
            <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
             <a href="#" class="active">Games</a>
            <?php
            include_once(DIR_ROOT."widget/notification_head.php");
            ?>
        </div>
       </div>
        <div class="tab_white_most_wanted">
	   <?php
		include_once(DIR_ROOT."widget/games_head_widget.php");
		?>  
        <div class="main-banner-games">           		
            <div id="slider1_container" style="position: relative; top: 0px; left: 0px; width: 830px;height: 331px; background: #191919; overflow: hidden;">
                <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 830px; height: 280px; overflow: hidden;">
                <?php 
				$getSliderGames				=	$objGames->listQuery("SELECT games.g_id,games.g_name,games.g_alias,games.g_img,games.g_banner,games.g_descr,play.game_users FROM games  LEFT JOIN (SELECT COUNT(gp_id) AS game_users,g_id FROM game_player GROUP BY g_id) AS play ON games.g_id = play.g_id WHERE games.g_status=1 ORDER BY g_created DESC LIMIT 10");	
				foreach($getSliderGames as $allSliderGames){
				?>
                        <div>
                        	
                            <a href="<?php echo SITE_ROOT.'games/show/'.$objCommon->html2text($allSliderGames['g_alias']).'-'.$objCommon->html2text($allSliderGames['g_id'])?>"><img u="image" src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/games/banner/'.$allSliderGames['g_banner']?>" /></a>
                            <img u="thumb" src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/games/img/'.$allSliderGames['g_img']?>" title="<?php echo ($allSliderGames['game_users'] >0)?$allSliderGames['game_users']:0?> Users" alt="<?php echo $allSliderGames['g_name'] ?>" />
                            <div class="banner_with_icon"><span class="img-sec-icon"><img src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/games/img/'.$allSliderGames['g_img']?>" /></span><span class="tile_sec"><h2><?php echo $objCommon->html2text($allSliderGames['g_name'])?></h2><!--<p>Strategy Game by KIXEYE</p>--></span></div>
                            <div class="desc-games-banner"><p><?php echo $objCommon->limitWords($allSliderGames['g_descr'],150)?></p><a href="<?php echo SITE_ROOT.'games/show/'.$objCommon->html2text($allSliderGames['g_alias']).'-'.$objCommon->html2text($allSliderGames['g_id'])?>">Play Now</a></div>
                             <div class="bring_shadow"></div>
                        </div>
                    <?php }?> 
                </div>
            <span u="arrowleft" class="jssora05l" style="top: 128px;left: 8px;"></span>
           <span u="arrowright" class="jssora05r" style="top: 128px; right: 8px"> </span>
            <div u="thumbnavigator" class="jssort01" style="left: 0px; bottom: 0px;">
                <div u="slides" style="cursor: default;">
                    <div u="prototype" class="p">
                        <div class=w><div u="thumbnailtemplate" class="t"></div></div>
                        <div class=c></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        <div class="games_categories">
        	<div class="row">
            	<div class="col-sm-6 game-container-inner">
                <div class="top-category">
                            <p><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/game-info.png" />New Games</p>
                        </div>
                	<div class="owl-carousel_1">
						 <div class="item_1">
						<?php
						$getNewGames					=	$objGames->listQuery("SELECT games.*,cat.gc_name,rating.gameRating FROM games LEFT JOIN game_category AS cat ON games.gc_id = cat.gc_id LEFT JOIN (SELECT AVG(gr_rating) AS gameRating,g_id FROM game_rating GROUP BY g_id) AS rating ON games.g_id = rating.g_id  WHERE  games.g_status=1 ORDER BY games.g_created desc LIMIT 12");
						$gameRate					   =	'';
						foreach($getNewGames as $keyNewGames=>$allNewGames){
						?>
                        	<div class="gamei">
								<a href="<?php echo SITE_ROOT.'games/show/'.$objCommon->html2text($allNewGames['g_alias']).'-'.$objCommon->html2text($allNewGames['g_id'])?>">
									<img src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/games/img/'.$objCommon->getThumb($allNewGames['g_img'])?>" />
								</a>
                                 <span class="game-info">
                                     <p><a href="<?php echo SITE_ROOT.'games/show/'.$objCommon->html2text($allNewGames['g_alias']).'-'.$objCommon->html2text($allNewGames['g_id'])?>"><?php echo $objCommon->html2text($allNewGames['g_name'])?></a></p>
									 <?php
									$gameRate			=	($allNewGames['gameRating']>0)?$allNewGames['gameRating']:0;
									$gameRate			=	round($gameRate)*20;
									?>
                                    <span class="rating_box">
                                        <span class="rating_yellow" style="width:<?php echo $gameRate?>%"></span>
                                    </span>
                                    <span class="game-cat"><p><?php echo $objCommon->html2text($allNewGames['gc_name'])?></p></span>
                                </span>
                            </div>
								<?php 
								if(($keyNewGames+1)%2==0){
									echo '</div><div class="item_1">';
								}
							}?>
						</div>
                   </div>
                   <div class="bottom-sec-game text-center">
                   		<a href="<?php echo SITE_ROOT?>games/search?category=new-games">See More</a>
                   </div>
                 </div>
				<div class="col-sm-6 game-container-inner">
                <div class="top-category">
                            <p><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/game-info.png" />Popular Games</p>
                        </div>
                	<div class="owl-carousel_1">
						<div class="item_1">
                    <?php
					$getPopularGames					=	$objGames->listQuery("SELECT games.*,cat.gc_name,count(player.gp_id) AS playerCount,rating.gameRating FROM games LEFT JOIN game_category AS cat ON games.gc_id = cat.gc_id LEFT JOIN game_player AS player ON games.g_id = player.g_id LEFT JOIN (SELECT AVG(gr_rating) AS gameRating,g_id FROM game_rating GROUP BY g_id) AS rating ON games.g_id = rating.g_id  WHERE  games.g_status=1 GROUP BY games.g_id ORDER BY playerCount desc,games.g_created desc LIMIT 12"); 
					$gameRate						   =	'';
					foreach($getPopularGames as $keyPopularGames=>$allPopularGames){
					?>
                        	<div class="gamei">
                                 <div class="img-size-thumb">
                                 <a href="<?php echo SITE_ROOT.'games/show/'.$objCommon->html2text($allPopularGames['g_alias']).'-'.$objCommon->html2text($allPopularGames['g_id'])?>">
									<img src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/games/img/'.$objCommon->getThumb($allPopularGames['g_img'])?>" />
								</a>
                                 </div>
                                 <span class="game-info">
                                    <p><a href="<?php echo SITE_ROOT.'games/show/'.$objCommon->html2text($allPopularGames['g_alias']).'-'.$objCommon->html2text($allPopularGames['g_id'])?>"><?php echo $objCommon->html2text($allPopularGames['g_name'])?></a></p>
									<?php
									$gameRate			=	($allPopularGames['gameRating']>0)?$allPopularGames['gameRating']:0;
									$gameRate			=	round($gameRate)*20;
									?>
                                    <span class="rating_box">
                                        <span class="rating_yellow" style="width:<?php echo $gameRate?>%"></span>
                                    </span>
                                    <span class="game-cat"><p><?php echo $objCommon->html2text($allPopularGames['gc_name'])?></p></span>
                                </span>
                            </div>
                        <?php 
								if(($keyPopularGames+1)%2==0){
									echo '</div><div class="item_1">';
								}
							}
						?>
						</div>
                   </div>
                   <div class="bottom-sec-game text-center">
                   		<a href="<?php echo SITE_ROOT?>games/search?category=popular-games">See More</a>
                   </div>
                 </div>
            </div>
            <?php
			$getGameCat				=	$objGames->listQuery("SELECT * FROM game_category ORDER BY gc_alias");
			foreach($getGameCat as $allGameCat){
			?>
            <div class="top-category">
				<p><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/game-info.png" /><?php echo $objCommon->html2text($allGameCat['gc_name'])?></p>
			</div>
            <div class="row">
            	<div class="col-sm-12">
                	<div class="owl-carousel_2">
                    <?php
					$gameRate					=	'';
					$getGames					=	$objGames->listQuery("SELECT games.g_id,games.g_name,games.g_alias,games.g_img,play.countUser,rating.gameRating FROM games LEFT JOIN (SELECT COUNT(gp_id) AS countUser,g_id FROM game_player GROUP BY g_id) AS play ON games.g_id = play.g_id LEFT JOIN (SELECT AVG(gr_rating) AS gameRating,g_id FROM game_rating GROUP BY g_id) AS rating ON games.g_id = rating.g_id  WHERE games.gc_id =".$allGameCat['gc_id']." AND games.g_status=1 ORDER BY games.g_created desc LIMIT 12");
					foreach($getGames as $allGetGames){
					?>
                        <div class="item_1">
                        	 <a href="<?php echo SITE_ROOT.'games/show/'.$objCommon->html2text($allGetGames['g_alias']).'-'.$objCommon->html2text($allGetGames['g_id'])?>">
							 	<img src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/games/img/'.$objCommon->getThumb($allGetGames['g_img'])?>" />
							 </a>
                                 <span class="game-info">
                                    <p><a href="<?php echo SITE_ROOT.'games/show/'.$objCommon->html2text($allGetGames['g_alias']).'-'.$objCommon->html2text($allGetGames['g_id'])?>"><?php echo $objCommon->html2text($allGetGames['g_name'])?></a></p>
									<?php
									$gameRate			=	($allGetGames['gameRating']>0)?$allGetGames['gameRating']:0;
									$gameRate			=	round($gameRate)*20;
									?>
                                    <span class="rating_box">
                                        <span class="rating_yellow" style="width:<?php echo $gameRate?>%"></span>
                                    </span>
                                    <span class="game-cat"><p><?php echo($allGetGames['countUser']>0)?$allGetGames['countUser']:'0'?> Users</p></span>
                                </span>
                            </div>
                           <?php }?>
                   </div>
                   <div class="bottom-sec-game text-center">
                   		<a href="<?php echo SITE_ROOT?>games/search?game_category=<?php echo $allGameCat['gc_id']?>">See More</a>
                   </div>
                </div>
            </div>
            <?php }?>
        </div>
      </div>
       <?php
	  include_once(DIR_ROOT."widget/games_right_widget.php");
	  ?>
    </div>
    
    
    </div>
  </div>
</div>

<script>

        jQuery(document).ready(function ($) {
			$('.fixer').fixedSidebar();

            var _SlideshowTransitions = [{$Duration:700,x:-1,$Easing:$JssorEasing$.$EaseInQuad}];

            var options = {
                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlayInterval: 2500,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                                //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

                $DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
                $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideDuration: 800,                                //Specifies default duration (swipe) for slide in milliseconds

                $SlideshowOptions: {                                //[Optional] Options to specify and enable slideshow or not
                    $Class: $JssorSlideshowRunner$,                 //[Required] Class to create instance of slideshow
                    $Transitions: _SlideshowTransitions,            //[Required] An array of slideshow transitions to play slideshow
                    $TransitionsOrder: 2,                           //[Optional] The way to choose transition to play slide, 1 Sequence, 0 Random
                    $ShowLink: true                                    //[Optional] Whether to bring slide link on top of the slider when slideshow is running, default value is false
                },

                $ArrowNavigatorOptions: {                       //[Optional] Options to specify and enable arrow navigator or not
                    $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 1                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                },

                $ThumbnailNavigatorOptions: {                       //[Optional] Options to specify and enable thumbnail navigator or not
                    $Class: $JssorThumbnailNavigator$,              //[Required] Class to create thumbnail navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always

                    $ActionMode: 1,                                 //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
                    $SpacingX: 110,                                   //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
                    $DisplayPieces: 6,                             //[Optional] Number of pieces to display, default value is 1
                    $ParkingPosition: 0                          //[Optional] The offset position to park thumbnail
                }
            };
			

            var jssor_slider1 = new $JssorSlider$("slider1_container", options);
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
                if (parentWidth)
                    jssor_slider1.$ScaleWidth(Math.max(Math.min(parentWidth, 818), 300));
                else
                    window.setTimeout(ScaleSlider, 30);
            }
            ScaleSlider();

            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
			
			
			var noOverflow		=	$(".jssort01");
				mainContianer	=	$("#slider1_container");
				thumbContainer	=	$(".p");
			
			
			
			noOverflow.children('div').css({"overflow" : "visible"});
			noOverflow.children('div').children('div').css({"overflow" : "visible"});
			mainContianer.children('div').children('div').css({"background" : "#ffffff"});
			noOverflow.children().children().children().css({"width" : "40px"});
			noOverflow.children().children().css({"width" : "145px"});
			
			var thumbDescriptions	=$(	'	<div class="desc-head-game">'+
										'	<div class="desc-head-main">'+
										'	<h5>Lasvegas Orginal</h5>'+
										'	</div>'+
										'	<div class="subhead-game">'+
										'	<p>1 Million Players</p>'+
										'	</div>'+
										'	</div>'
									);
			thumbDescriptions.insertAfter(thumbContainer);
			
			var totaldiv	=	mainContianer.children().children().children('div:nth-of-type(2)').children('div').addClass('selected');
				arraydiv	= mainContianer.children().children().children('div:nth-of-type(2)').find('div.selected');
				
			var	usertitle;
				arryoftitle	=[];
				arrayofalt	=[];
			for(i=1; i<arraydiv.length; i++){
			 usertitle =	$(arraydiv[i]).children("img").attr('title');
			 arryoftitle.push(usertitle);
			 useralt   =	$(arraydiv[i]).children("img").attr('alt');
			 arrayofalt.push(useralt);
			}
			var	thumbdiv		=	$(".jssort01").children().children().children('div:nth-of-type(2)').closest('div').addClass("selectedThumb");
				arrayofthumb	= 	$(".jssort01").children().children().children('div:nth-of-type(2)').find("div.selectedThumb");
				
				var userThumbtitle;
				for(i=0; i<arrayofthumb.length; i++){
					$(arrayofthumb[i]).children('.desc-head-main').children('h5').html(arrayofalt[i]);
					$(arrayofthumb[i]).children('.subhead-game').children('p').html(arryoftitle[i]);
				}
		 });
    </script>
<script type="text/javascript">
$(document).ready(function(){
	
	$(".screenshots_container").perfectScrollbar();
	
	var owl = $('.owl-carousel_1');
owl.owlCarousel({
    loop:true,
    nav:true,
	navText: [ '<i class="fa fa-chevron-left arr"></i>', '<i class="fa fa-chevron-right arr1"></i>' ],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },            
        960:{
            items:4
        },
        1200:{
            items:3
        }
    }
});
var owl = $('.owl-carousel_2');
owl.owlCarousel({
    loop:true,
    nav:true,
	navText: [ '<i class="fa fa-chevron-left arr"></i>', '<i class="fa fa-chevron-right arr1"></i>' ],
	margin: 5,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:5
        },            
        960:{
            items:5
        },
        1200:{
            items:6
        }
    }
});
	
	});
</script>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/fixed_sidebar.js"></script>
 <style>
 	.modal-body .ps-container > .ps-scrollbar-x-rail {
	  opacity: 1 !important;
	}
 </style>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  