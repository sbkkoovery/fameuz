<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
?>
<div class="inner_content_section">
	<div class="container">
		<div class="row">
        	<div class="col-sm-12">
				<div class="inner_top_border">
					<div class="user_profile">
					<?php echo $objCommon->checkEmailverification();?>
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                            <div class="content">
                                <div class="pagination_box">
                                    <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
                                    <a href="<?php echo SITE_ROOT.$getUserDetails['usl_fameuz']?>"> My profile </a><i class="fa fa-caret-right"></i></a>
                                    <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                    <a href="javascript:;" class="active"> Followers  </a>
                                    <?php
                                    include_once(DIR_ROOT."widget/notification_head.php");
                                    ?>
                                </div>
                                <div class="tab_white_search">
                                        <div class="section_search">
                                            <ul>
                                                <li class="visitor_head_box"><h5>Follower</h5><span class="point-it"></span></li>
                                            </ul>
                                            <span>
                                                <input type="text" placeholder="Search Followers" id="follower_search" name="follower_search">
                                            </span>
                                        </div>
                                    </div>
                                    <div class="visitor_list_box">
                                        <ul class="row"></ul>
                                    </div>
                            	</div>
                            </div>
                            <div class="col-sm-3 col-lg-sp-3">
								<?php
                                include_once(DIR_ROOT."widget/right_static_ad_bar.php");
                                ?>
                            </div>
                        </div>
					</div>
				</div>
			</div>
           </div>
	</div>
</div>
<script>
$(window).load(function(e) {
	$('.fixer').fixedSidebar();
});
$(document).ready(function(){
	getresult('<?php echo SITE_ROOT?>ajax/my_follower_list.php');
	 function getresult(url) {
		$.ajax({
			url: url,
			type: "GET",
			data:  {rowcount:$("#rowcount").val()},
			beforeSend: function(){
			$('#loader-icon').show();
			},
			complete: function(){
			$('#loader-icon').hide();
			},
			success: function(data){
				$(".visitor_list_box ul").append(data);
				if($("#total-count").val()>0){
				}else{
					$(".visitor_list_box ul").html('<li class="col-md-6"><p>No followers found....</p></li>');
				}
			},
			error: function(){} 	        
	   });
	}
	$(window).scroll(function(){
		if ($(window).scrollTop() == $(document).height() - $(window).height()){
			if($(".pagenum:last").val() <= $(".total-page").val()) {
				var pagenum = parseInt($(".pagenum:last").val()) + 1;
				getresult('<?php echo SITE_ROOT?>ajax/my_follower_list.php?page='+pagenum);
			}
		}
	}); 
	$('#follower_search').keyup(function(){
		var keyVal		=	$(this).val();
			getSearchresult('<?php echo SITE_ROOT?>ajax/my_follower_list.php?search='+keyVal+'&page=1');
	});
	function getSearchresult(url) {
		$.ajax({
			url: url,
			type: "GET",
			data:  {rowcount:$("#rowcount").val()},
			beforeSend: function(){
			$('#loader-icon').show();
			},
			complete: function(){
			$('#loader-icon').hide();
			},
			success: function(data){
				if(data!=""){
					$(".visitor_list_box ul").html(data);
				}else{
					$(".visitor_list_box ul").html('<li class="col-md-6"><p>No followers found....</p></li>');
				}
			},
			error: function(){} 	        
	   });
	}

});
//<input type="text" placeholder="Search" id="follower_search" name="follower_search">
</script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/fixed_sidebar.js"></script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  