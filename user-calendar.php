<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once (DIR_ROOT.'class/calendarClass.php');
include_once(DIR_ROOT."class/reminders.php");
$calendar = new CalendarPlus();
$objReminder	=	new reminders();	
$year  = '';
$month = '';
$selDay= '';
if(null==$year&&isset($_GET['year'])){
	$year = $_GET['year'];
}else if(null==$year){
	$year = date("Y",time());
}
if(null==$month&&isset($_GET['month'])){
	$month = $_GET['month'];
}else if(null==$month){
	$month = date("m",time());
}
if(isset($_GET['day'])){
	$selDay = $_GET['day'];
}else{
	$selDay = date("d",time());
	if(isset($_GET['month'],$_GET['year'])&&!isset($_GET['day'])){
		$selDay	=	1;
	}
}
$calendar->currentYear=$year;
$calendar->currentMonth=$month;
$calendar->selDay=$selDay;
$calendar->daysInMonth=$calendar->_daysInMonth($month,$year);  
         
$nextMonth = $calendar->currentMonth==12?1:intval($calendar->currentMonth)+1;
$nextYear = $calendar->currentMonth==12?intval($calendar->currentYear)+1:$calendar->currentYear;
$preMonth = $calendar->currentMonth==1?12:intval($calendar->currentMonth)-1;
$preYear = $calendar->currentMonth==1?intval($calendar->currentYear)-1:$calendar->currentYear;

?>
<link href="<?php echo SITE_ROOT?>css/reminder.css" rel="stylesheet" type="text/css" />
<link href="<?php echo SITE_ROOT?>css/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
<script src="<?php echo SITE_ROOT?>js/jquery-ui.js"></script>
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
  <div class="inner_top_border">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
	  <?php echo $objCommon->checkEmailverification();?>
      <div class="jobs-head">
        <div class="pagination_box_jobs pagination_box">
        <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
        <a href="<?php echo SITE_ROOT.$getUserDetails['usl_fameuz']?>"> My profile </a><i class="fa fa-caret-right"></i></a>
        <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
        <a href="javascript:;" class="active"> My Calendar</a>
       <?php
        include_once(DIR_ROOT."widget/notification_head.php");
        ?>
        </div>
       </div>
           <div class="polarids">
               <div class="calender-container">
                    <div class="calendarBox">
                    <div class="calendar-nofications">
<?php 
$eventDate		=	$year.'-'.$month.'-'.$selDay;
$eventMonth		=	$month;
$eventDay		=	$selDay;
$showDetailsFunction	=	'onclick="showDetails(\''.$year.'-'.$month.'-'.($selDay+1).'\')"';
$nextDay	=	date("d",strtotime($year.'-'.$month.'-'.($selDay+1)));
$prevDay	=	date("d",strtotime($preYear.'-'.$preMonth.'-'.($selDay-1)));

if($selDay+1>$calendar->daysInMonth){
	$showDetailsFunction	=	'onclick="window.location=\''.SITE_ROOT.'user/user-calendar/'.$nextMonth.'/'.$nextYear.'/\'"';
}else{
	$showDetailsFunction	=	'onclick="showDetails(\''.$year.'-'.$month.'-'.($selDay+1).'\')"';
}

if($eventDay-1<1){
	$showDetailsPreFunction	=	'onclick="window.location=\''.SITE_ROOT.'user/user-calendar/'.$preMonth.'/'.$preYear.'/'.$prevDay.'/\'"';
}else{
	$showDetailsPreFunction	=	'onclick="showDetails(\''.$year.'-'.$month.'-'.($prevDay).'\')"';
}

$friendPermission	=	"'1,0,0,0','1,0,0,1','1,0,1,0','1,0,1,1','1,1,0,0','1,1,0,1','1,1,1,0','1,1,1,1','0,1,0,0','0,1,0,1','0,1,1,0','0,1,1,1'";
$followPermission	=	"'1,0,0,0','1,0,0,1','1,0,1,0','1,0,1,1','1,1,0,0','1,1,0,1','1,1,1,0','1,1,1,1'";
$allEventSql	  =	"SELECT * FROM (
SELECT
user.user_id,user.first_name,user.last_name,user.display_name,book.bm_company,book.bm_work,book.book_to AS endDate, book.bm_country AS location,'booking' AS calCat
FROM book_model AS book 
LEFT JOIN users AS user ON book.agent_id=user.user_id 
WHERE book.model_id=".$_SESSION['userId']." AND book.bm_model_status=1 AND DATE(book.book_from)='".$eventDate."'
UNION
SELECT
r.remind_id AS user_id,u.first_name,u.last_name,u.display_name,r.remind_title AS bm_company ,r.remind_description AS bm_work,'NA' AS endDate,r.remind_place AS location,'reminder' AS calCat
FROM reminders AS r
LEFT JOIN users AS u ON r.user_id=u.user_id
WHERE r.user_id=".$_SESSION['userId']." AND r.remind_date='".$eventDate."'
UNION
SELECT
u.user_id,u.first_name,u.last_name,u.display_name,'NA' AS bm_company ,'NA' AS bm_work,up.p_dob AS endDate,'NA' AS location,'birthday' AS calCat
FROM users AS u
LEFT JOIN  personal_details as up ON u.user_id=up.user_id
LEFT JOIN following AS flw ON u.user_id=flw.follow_user1 OR u.user_id=flw.follow_user2
LEFT JOIN user_privacy as pr ON u.user_id=pr.user_id
WHERE flw.follow_status=2 AND pr.uc_p_dob IN(".$friendPermission.") AND (flw.follow_user1=".$_SESSION['userId']." OR flw.follow_user2=".$_SESSION['userId'].") AND MONTH( up.p_dob)='".$eventMonth."' AND DAYOFMONTH( up.p_dob)='".$eventDay."'
UNION
SELECT
u.user_id,u.first_name,u.last_name,u.display_name,'NA' AS bm_company ,'NA' AS bm_work,up.p_dob AS endDate,'NA' AS location,'birthday' AS calCat
FROM users AS u
LEFT JOIN  personal_details as up ON u.user_id=up.user_id
LEFT JOIN following AS flw ON u.user_id=flw.follow_user1 OR u.user_id=flw.follow_user2
LEFT JOIN user_privacy as pr ON u.user_id=pr.user_id
WHERE flw.follow_status=1 AND pr.uc_p_dob IN(".$followPermission.") AND flw.follow_user1=".$_SESSION['userId']." AND MONTH( up.p_dob)='".$eventMonth."' AND DAYOFMONTH( up.p_dob)='".$eventDay."'

) AS calTab WHERE 1
";
$remindDetails	=	$objReminder->listQuery($allEventSql);
?>
<div class="day-notifications">
    <div class="img-arw-left"><img src="<?php echo SITE_ROOT?>images/calendar-arw.png" <?php echo $showDetailsPreFunction; ?>/></div>
    <div class="date-clander"><p><?php echo date("d",strtotime($eventDate)); ?> <span><?php echo date("l",strtotime($eventDate)); ?></span></p></div>
    <div class="img-arw-right"><img src="<?php echo SITE_ROOT?>images/calendar-arw-right.png" <?php echo $showDetailsFunction; ?> /></div>
</div>
<div class="day-notification-content">
    <ul>
    <?php 
	if(count($remindDetails)>0){
		foreach($remindDetails as $reminder){
		if($reminder['calCat']=='reminder'){
	?>
        <li>
            <h5><?php echo $reminder['bm_company']; ?> <span class="events"><?php echo ucwords($reminder['calCat']); ?></span></h5>
            <p><?php echo $reminder['bm_work']; ?></p>
<!--            <p><img src="--><?php //echo SITE_ROOT?><!--images/calender-cal.png" /> Ends on 31, 2015</p>-->
            <?php if($reminder['bm_company']!='NA'){?>
            <p><img src="<?php echo SITE_ROOT?>images/marker-cal.png" /> <?php echo $reminder['location']; ?></p>
            <?php }?>
            <a href="javascript:;" class="editArrow" onclick="showOptions(this);"><i class="fa fa-angle-down"></i></a>
            <div class="dropdown_option_unfriend">
                <div class="menu-drop">
                    <?php echo '<a href="javascript:void(0);" data-toggle="modal" data-target="#editReminder" class="unfriend has-spinner" data-friend="" onclick="editReminder('.$calenderContent['user_id'].');">Edit<span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span></a>';
                    echo '<a href="'.SITE_ROOT.'access/delete_reminder.php?remindId='.$calenderContent['user_id'].'" class="cancel" onclick="return confirm(\'You want to delete..?\');">Delete</a>'; ?>
                </div>
            </div>
        </li>
    <?php }else if($reminder['calCat']=='birthday'){?>
        <li>
            <h5><?php echo ucwords($objCommon->displayName($reminder)); ?> <span><?php echo ucfirst($reminder['calCat']); ?></span></h5>
            <p><img src="<?php echo SITE_ROOT?>images/calender-cal.png" /> Date of Birth <?php echo date("dS F, Y",strtotime($reminder['endDate'])); ?></p>
            <?php if($reminder['bm_company']!='NA'){?>
            <p><img src="<?php echo SITE_ROOT?>images/marker-cal.png" /> <?php echo $reminder['location']; ?></p>
            <?php }?>
        </li>
    <?php }else if($reminder['calCat']=='booking'){?>
        <li>
            <h5><?php echo $reminder['bm_company']; ?> <span><?php echo ucfirst($reminder['calCat']); ?></span></h5>
            <p><img src="<?php echo SITE_ROOT?>images/calender-cal.png" /> Ends on <?php echo date("dS F, Y",strtotime($reminder['endDate'])); ?></p>
            <?php if($reminder['bm_company']!='NA'){?>
            <p><img src="<?php echo SITE_ROOT?>images/marker-cal.png" /> <?php echo $reminder['location']; ?></p>
            <?php }?>
        </li>    
    <?php }} }else{?>
    <li style="background:none;">Nothing found in this date.</li>
    <?php }?>
    </ul>
</div>
                    </div>
                    <?php 
                        $content='<div id="calendar">'.
                        '<div class="box">'.
                        $calendar->_createNavi().
                        '</div>'.
                        '<div class="box-content">'.
                                '<ul class="label">'.$calendar->_createLabels().'</ul>';   
                                $content.='<div class="clear"></div>';     
                                $content.='<ul class="dates">';    
                                 
                                $weeksInMonth = $calendar->_weeksInMonth($month,$year);
                                // Create weeks in a month
                                for( $i=0; $i<$weeksInMonth; $i++ ){
                                     
                                    //Create days in a week
                                    for($j=1;$j<=7;$j++){
                                        $content.=$calendar->_showDay($i*7+$j);
                                    }
                                }
                                 
                                $content.='</ul>';
                                 
                                $content.='<div class="clear"></div>';     
             
                        $content.='</div>';
                        $content.='</div>';
                        echo $content; 
                        ?>
                         <div class="clearfix"></div>
                    </div>
                </div>
                <div class="row">
                	<div class="col-sm-10">
                    	<?php echo $objCommon->displayMsg(); ?>
                    </div>
                	<div class="col-sm-2">
                        <div class="createNewEvents text-right">
                            <a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i>create</a>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
        <div class="col-sm-3 col-lg-sp-3">
			<?php
				include_once(DIR_ROOT."widget/right_static_ad_bar.php");
			?>
        </div>
    </div>
    <div class="clearfix"></div>
    </div>
  </div>
</div>


<div class="modal fade calendar-modal" id="editReminder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
    <div class="modal-content" id="reminderEdit">
    </div>
  </div>
</div>
<div class="modal fade calendar-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Create Private Event</h4>
      </div>
      <form action="<?php echo SITE_ROOT.'access/create_reminder.php' ?>" method="post">
      <div class="modal-body">
            <div class="form-group">
            <label>Name</label>
                <input type="text" name="remind_title" class="form-control" placeholder="Reminder Title">
            </div>
            <div class="form-group">
            <label>Details</label>
                <textarea class="form-control" name="remind_description" placeholder="Add more info" rows="4"></textarea>
            </div>
            <div class="form-group">
            <label>Where</label>
                <input type="text" class="form-control background-img addPlace" name="remind_place" placeholder="Add a place" name="remind_date" onFocus="geolocate()" autocomplete="off"  id="locality"  value="" />
            </div>
            <div class="row">
            	<div class="col-sm-6">
                    <div class="form-group">
                        <label>When</label>
                        <input type="text" name="remind_date" class="form-control background-img when" id="remindDate" placeholder="Add Date">
                    </div>
                </div>
                <div class="col-sm-6">
                	<div class="form-group">
                        <label>Time</label>
                        <input type="text" name="remind_time" class="form-control background-img time" placeholder="Add Time">
                    </div>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary">Create</button>
      </div>
        </form>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function(e) {
        $('div.day-notification-content').perfectScrollbar();
		$("#remindDate").datepicker({ minDate: 0});
    });
</script>
<script>
function editReminder(ID){
	if(ID!=""){
		$.get("<?php echo SITE_ROOT; ?>ajax/edit_reminder.php",{remindId:ID},
		function(data){
			$("#reminderEdit").html(data);
			$("#remindDate").datepicker({ minDate: 0});
			initialize();
		});
	}
}
function showOptions(remind){
	$(remind).parent().children(".dropdown_option_unfriend").fadeToggle(200);
}
function showDetails(date){
	if(date!=""){
		$.get("<?php echo SITE_ROOT; ?>ajax/listEvents.php",{eventDate:date},
		function(data){
			$(".calendar-nofications").html(data);
			$(".calendar-nofications").height();
		});
	}
}
window.onload=function(){initialize();};
var placeSearch, autocomplete;
var componentForm = {
  locality: 'long_name',
  country: 'long_name'
};

function initialize() {
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('locality')),
      { types: ['geocode'] });
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    fillInAddress();
  });
}
function fillInAddress() {
  var place = autocomplete.getPlace();
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      //var val = place.address_components[i][componentForm[addressType]];
     // document.getElementById(addressType).value = val;
    }
  }
}
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
// [END region_geolocation]

    </script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  