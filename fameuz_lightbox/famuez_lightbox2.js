// JavaScript Document
(function( $ ){
	var screenWidth		=	$(window).width();
	var screenHeight	=	$(window).height();
	var parent,displayImg,imagePlatform,commentSection,prev,next,grandParent,sideBarInside,pageNo,onPageClick,deleteComment,swap,displayVideo,swift,ajaxify,recentLikeCount,recentshareCount,workedStatus,recentCommentCount,recentCommentLikeCount,nextSwipe,limitVal,nextSwipeData,imageWidth,adjWidth,implementWidth;
	var startVal	=	0;
	var deltaZoom	=	0;
	var limitVal	=	9;
	var jsonObj;
	var arrayVal		=	[];
	
	$.fn.lightBox = function( options ) {  
	var inst	=	this;
	
    // Create some defaults, extending them with any options that were provided
	 var settings = $.extend( {
      sidebar        	: true,
	  setCoverPic		: true,
	  photosObj			: false,
	  videoObj			: false,
	  defaults			: false,
	  album				: false,
	  photoPage			: false,
      backgroundColor 	: '#000000',
	  videoSkin				: '../jqplayer/skin/',
	  deleteCommentUrl	: 'access/delete_comment.php',
	  photos			: 
	  	{
		  fetchUrl				: 'ajax/popdata.php',
		  ajaxUrl				: 'widget/pop_image_comment_box.php',
		  dataAttr				: {
			  dataAttrStatus	: true,
			  dataAttrVal		: [],
		  },
		  comment				:
		  {
			  CommentFormAction	: '../access/post_comment.php?action=add_comment',
			  commentLikeUrl	: 'ajax/like_comment.php',
		  },
		  //CommentFormAction		: '../access/post_comment.php?action=add_comment',
		  shareItems			:
		  {
			  review			: '',
			  likeAjaxUrl		: 'ajax/like.php',
			  shareAjaxUrl		: 'ajax/share.php"',
			  workedAjaxUrl		: 'ajax/worked_together.php',
		  }
	    },
	  video				:
		{
			url					: 'uploads/video.mpg',
			fetchUrl			: 'ajax/videodata.php',
			ajaxUrl				: 'widget/pop_video_comment_box.php',
			dataAttr			: [],
			comment				:
			{
			   CommentFormAction: '../access/post_comment.php?action=add_comment',
			   commentLikeUrl	: 'ajax/like_video_comment_pop.php',
			},
			//CommentFormAction	: '../access/post_comment.php?action=add_comment',
			shareItems			:
			{
			  review			:  '',
			  likeAjaxUrl		:  'ajax/like_video.php',
			  shareAjaxUrl		:  'ajax/share_video.php"',
			}
		},
	 skins				:
	 	{
			next			  	: '<img src="images/next.png">',
			prev			  	: '<img src="images/prev.png">',
			resets				: '<i class="fa fa-refresh"></i>',
			dismiss		  		: '<img src="images/close.png" width="15">',
			reviewIcon			: '<i class="fa fa-chevron-right"></i>',
			loader				: '../images/ajax-loader.gif',
		 }
	}, options);
	
	// initiating plugin when page loads
	
	var initFunctions	=	function(){
		if(!$('.overlay_bg').length) {
			overlayBuilder();
		}
		eventHandlers();
	}
	// lightbox body that initiate on page ready
	var overlayBuilder	=	function(){
		var sidebars		=	{
			defaultSidebar	:	'<div class="side_bar" id="side_bar">'+
								'<div class="user-data">'+
								'<div class="user_info">'+
								'<img/>'+
								'</div>'+
								'<div class="user_data">'+
								'<h3></h3>'+
								'<p></p>'+
								'</div>'+
								'</div>'+
								'<div class="like_countz">'+
								'<ul>'+
								'<li><i class="fa fa-heart"></i> <span class="likeCount">0</span></li>'+
								'<li><i class="fa fa-comments"></i> <span  class="commentCount">0</span></li>'+
								'<li><i class="fa fa-share-alt"></i> <span class="shareCount">0</span></li>'+
								'</ul>'+
								'</div>'+
								'<div class="image_comment_load">'+
								'<div id="middelSec">'+
								'<div class="desc_photoz">'+
								'<p></p>'+
								'</div>'+
								'<div class="comments_Extr">'+
								'</div>'+
								'</div>'+
								'<div class="comment_box">'+
								'<div class="prof_thump">'+
								'<img>'+
								'</div>'+
								'<div class="post_commnt">'+
								'<form enctype="multipart/form-data" method="post" action="">'+
								'<div class="arrow">'+
								'<img src="http://fameuz.burjalsafacomputers.com/images/arrow_left.png" alt="arrow">'+
								'</div>'+
								'<div class="textarea">'+
								'<span class="smile_icon"><i class="fa fa-smile-o"></i></span>'+
								'<textarea class="comment_descr_input2" rows="1" name="comment_descr" style="height: 34px; overflow: hidden;"></textarea>'+
								'<div class="ajaxloader text-center"><img src="'+settings.skins.loader+'" alt="comments_arrow"></div>'+
								'<input type="hidden" value="1" name="type" id="type">'+
								'<input type="hidden" value="84" name="id" id="id">'+
								'<input type="hidden" value="36" name="userto" id="userto">'+
								'<br><small>Press Enter to post.</small>'+
								'</form>'+
								'</div>'+
								'</div>'+
								'<img>'+
								'</div>'+
								'</div>'+
								'</div>',
			albumSidebar 	: 	'<div class="side_bar" id="side_bar">'+
								'<div class="user-data">'+
								'<div class="user_info">'+
								'<img/>'+
								'</div>'+
								'<div class="user_data">'+
								'<h3></h3>'+
								'<p></p>'+
								'</div>'+
								'</div>'+
								'<div class="like_countz">'+
								'<ul>'+
								'<li><i class="fa fa-heart"></i> <span class="likeCount">0</span></li>'+
								'<li><i class="fa fa-comments"></i> <span  class="commentCount">0</span></li>'+
								'<li><i class="fa fa-share-alt"></i> <span class="shareCount">0</span></li>'+
								'</ul>'+
								'</div>'+
								'<div class="image_comment_load">'+
								'<div id="middelSec">'+
								'</div>'+
								'</div>'+
								'</div>',
			photoSidebar	:	'<div class="side_bar" id="side_bar">'+
								'<div class="user-data">'+
								'<div class="user_info">'+
								'<img/>'+
								'</div>'+
								'<div class="user_data">'+
								'<h3></h3>'+
								'<p></p>'+
								'</div>'+
								'</div>'+
								'<div class="like_countz">'+
								'<ul>'+
								'<li><i class="fa fa-heart"></i> <span class="likeCount">0</span></li>'+
								'<li><i class="fa fa-comments"></i> <span  class="commentCount">0</span></li>'+
								'<li><i class="fa fa-share-alt"></i> <span class="shareCount">0</span></li>'+
								'</ul>'+
								'</div>'+
								'<div class="masnoryDiv">'+
								'<ul>'+
								'</ul>'+
								'</div>'+
								'<div class="image_comment_load">'+
								'<div id="middelSec">'+
								'<div class="desc_photoz">'+
								'<p></p>'+
								'</div>'+
								'<div class="comments_Extr">'+
								'</div>'+
								'</div>'+
								'<div class="comment_box">'+
								'<div class="prof_thump">'+
								'<img>'+
								'</div>'+
								'<div class="post_commnt">'+
								'<form enctype="multipart/form-data" method="post" action="">'+
								'<div class="arrow">'+
								'<img src="http://fameuz.burjalsafacomputers.com/images/arrow_left.png" alt="arrow">'+
								'</div>'+
								'<div class="textarea">'+
								'<span class="smile_icon"><i class="fa fa-smile-o"></i></span>'+
								'<textarea class="comment_descr_input2" rows="1" name="comment_descr" style="height: 34px; overflow: hidden;"></textarea>'+
								'<div class="ajaxloader text-center"><img src="'+settings.skins.loader+'" alt="comments_arrow"></div>'+
								'<input type="hidden" value="1" name="type" id="type">'+
								'<input type="hidden" value="84" name="id" id="id">'+
								'<input type="hidden" value="36" name="userto" id="userto">'+
								'<br><small>Press Enter to post.</small>'+
								'</form>'+
								'</div>'+
								'</div>'+
								'<img>'+
								'</div>'+
								'</div>'+
								'</div>',
		}
								
		var shareSection	=	'<div class="share_like_box">'+
								'<div class="review_message">'+
								'<a>Add Review <i class="fa fa-chevron-right"></i></a>'+
								'<a>Message <i class="fa fa-chevron-right"></i></a>'+
								'</div>'+
								'<div class="like">'+
								'<a class="like_btn" href="javascript:;"><i class="fa fa-heart"></i> Like</a>'+
								'</div>'+
								'<div class="share">'+
								'<a class="share_btn" data-share="100" data-sharecat="1" href="javascript:;"><i class="fa fa-share-alt"></i>Share </a>'+
								'</div>'+
								'<div class="makeCover">'+
								'<a class="makeCover_btn" href="javascript:;"><i class="fa fa-image"></i>Make Cover Photo </a>'+
								'</div>'+
								'<div class="worked">'+
								'<a href="javascript:;" class="worked_btn worked_request has-spinner" data-like="100" data-likecat="1">'+
								'<span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span>'+
								'<i class="fa fa-users"></i>Worked Together'+
								'</a>'+
								'</div>'+
								'</div>';
								
								
		var photoPreview	=	'<div class="bg_fixed"><div class="dismiss" id="terminate">'+settings.skins.dismiss+'</div><div class="zoom"><img id="zoomIn"/><img id="zoomIn"/></div><span class="prev">'+settings.skins.prev+'</span><span class="next">'+settings.skins.next+'</span><span class="reset">'+settings.skins.resets+'</span>'+shareSection+'<div class="bg_img_stand"><img></div></div>';
		var videoPreview	=	'<div class="bg_fixed"><div class="dismiss" id="terminate">'+settings.skins.dismiss+'</div><div class="zoom"><img id="zoomIn"/><img id="zoomIn"/></div><span class="prev">'+settings.skins.prev+'</span><span class="next">'+settings.skins.next+'</span>'+shareSection+'<div class="bg_img_stand"><video width="100%" height="100%" controls autoplay><source id="vidUrl" type="video/mp4"></video></div></div>';
		var platformObj	=	{
			photoSection 	: '<img>',
			videoSection	: '<div id="vidSection" class="vidSection"></div>'
			
		}
		var zoomPrev		=	'<div class="zoom_previw">'+
								'<div class="img_previw">'+
								'<img>'+
								'</div>'
								'</div>';
		var defaultoverlay		=	{
			photoOverlay	:	'<div class="overlay_bg"><div class="lightBox">'+sidebars.defaultSidebar+photoPreview+'</div></div>',
			videoOverlay	:	'<div class="overlay_bg"><div class="lightBox">'+sidebars.defaultSidebar+videoPreview+'</div></div>',
			multiOverlay	:	'<div class="overlay_bg">'+
								'<div class="lightBox">'+sidebars.defaultSidebar+
								'<div class="bg_fixed">'+
								'<div class="img_small_prev_overlay">'+
								'<div class="img_small_prev">'+
								'<div class="drag_overlay">'+
								'<div class="drag_zoom"></div>'+
								'<img>'+
								'</div>'+
								'</div>'+
								'<div class="zoom_value">'+
								'<p>0%</p>'+
								'</div>'+
								'</div>'+
								'<div class="dismiss" id="terminate">'+settings.skins.dismiss+
								'</div>'+
								'<div class="zoom">'+
								'<i class="fa fa-search-plus zoomIn"></i>'+
								'<i class="fa fa-search-minus zoomOut"></i>'+
								'<i class="fa fa-refresh reset"></i>'+
								'</div><span class="prev">'+settings.skins.prev+
								'</span>'+
								'<span class="next">'+settings.skins.next+
								'</span>'+shareSection+
								'<div class="bg_img_stand">'+platformObj.photoSection+platformObj.videoSection+
								'</div>'+
								'</div>'+
								'</div>'+
								'</div>',
		}
		var albumOverlay	=	{
			photoOverlay	:	'<div class="overlay_bg"><div class="lightBox">'+sidebars.albumSidebar+photoPreview+'</div></div>',
			videoOverlay	:	'<div class="overlay_bg"><div class="lightBox">'+sidebars.albumSidebar+videoPreview+'</div></div>',
			multiOverlay	:	'<div class="overlay_bg">'+
								'<div class="lightBox">'+sidebars.albumSidebar+
								'<div class="bg_fixed">'+
								'<div class="img_small_prev_overlay">'+
								'<div class="img_small_prev">'+
								'<div class="drag_overlay">'+
								'<div class="drag_zoom"></div>'+
								'<img>'+
								'</div>'+
								'</div>'+
								'<div class="zoom_value">'+
								'<p>0%</p>'+
								'</div>'+
								'</div>'+
								'<div class="dismiss" id="terminate">'+settings.skins.dismiss+
								'</div>'+
								'<div class="zoom">'+
								'<i class="fa fa-search-plus zoomIn"></i>'+
								'<i class="fa fa-search-minus zoomOut"></i>'+
								'</div><span class="prev">'+settings.skins.prev+
								'</span>'+
								'<span class="next">'+settings.skins.next+
								'</span>'+shareSection+
								'<div class="bg_img_stand">'+platformObj.photoSection+platformObj.videoSection+
								'</div>'+
								'</div>'+
								'</div>'+
								'</div>',
		}
		var photoPageOverlay =	'<div class="overlay_bg"><div class="lightBox">'+sidebars.photoSidebar+photoPreview+'</div></div>';
		
		if (settings.sidebar) {
			if(settings.defaults && !settings.album && !settings.photoPage) { //Default image popup
				if(settings.photosObj && !settings.videoObj) {
					if(defaultoverlay.photoOverlay ) {
						inst.parents('body').append(defaultoverlay.photoOverlay);
					}
				} else if(settings.videoObj && !settings.photosObj) {
					if(defaultoverlay.videoOverlay){
						inst.parents('body').append(defaultoverlay.videoOverlay);
					}
				} else if(settings.photosObj && settings.videoObj) {
					if(defaultoverlay.multiOverlay) {
						inst.parents('body').append(defaultoverlay.multiOverlay);
					}
				}
			} else if( !settings.defaults && settings.album  && !settings.photoPage) { // Album image Popup
				if(settings.photosObj && !settings.videoObj) {
					if(albumOverlay.photoOverlay) {
						inst.parents('body').append(albumOverlay.photoOverlay);
					}
				} else if(settings.videoObj && !settings.photosObj) {
					if(albumOverlay.videoOverlay) {
						inst.parents('body').append(albumOverlay.videoOverlay);
					}
				} else if(settings.photosObj && settings.videoObj) {
					if(albumOverlay.multiOverlay) {
						inst.parents('body').append(albumOverlay.multiOverlay);
					}
				}
			} else if( !settings.defaults && !settings.album  && settings.photoPage ) { // Photopage image Popup
				if( photoPageOverlay ) {
					inst.parents('body').append( photoPageOverlay );
				}
			}
			
			parent			=	inst.parents().find('.lightBox');
			grandParent		=	$('.overlay_bg');
			displayImg		=	$(".bg_img_stand img");
			imagePlatform	=	$('.bg_img_stand');
			commentSection	=	$('.comment');
			sideBar			=	$('.side_bar');
			prev			=	$('.prev');
			next			=	$('.next');
			sideBarInside	=	$('.side_bar');
			displayVideo	=	$("#vidUrl");
			VideoPlatform	=	$("#vidSection");
			formActnInject	=	$('.post_commnt form');
			commentParent	=	$('.comments_Extr');
			zoomIn			=	$('.zoomIn');
			zoomOut			=	$('.zoomOut');
			resets			=	$('.reset');
			zoomVal			=	$('.zoom_value p');
			imgPrevZoom		=	$('.img_small_prev img');
			zoomSectnOverlay=	$('.img_small_prev_overlay');
			likeBtn			=	$('.like_btn');
			shareBtn		=	$('.share_btn');
			workedBtn		=	$('.worked_btn');
			likeCount		=	$('.likeCount');
			shareCount		=	$('.shareCount');
			commentCount	=	$('.commentCount');
			commentLikeBtn	=	$('.like_button ');
			masnoryUl		=	$('.masnoryDiv ul');
			descSection		=	$('.desc_photoz');
			descPopup		=	$(".desc_photoz p");
			commentOverlay	=	$('.comments_Extr');
			reviewMessage	=	$('.review_message');
			makeCoverPhoto	=	$('.makeCover');
			makeCoverBtn	=	$('.makeCover_btn');
			
			//console.log(settings.photos.name);
			
			if(sideBarInside.length) {	
				$('#middelSec,.masnoryDiv').perfectScrollbar();
			}
			
		} 
		
	}
	
	//callback function to retrive data from ajax page 
	var  ajaxload	=	function( val ) {
			arrayVal	=	val;
			var order	=	val;
			var prevUserImage, getUrl,swift,dataKey;
			
			if( settings.photosObj && !settings.videoObj && !settings.photoPage ) {
				getUrl	=	settings.photos.fetchUrl;
				swift	=	0;
				dataKey	= {contentId:val[0],contentType:val[1],contentAlbum:val[2]};
				formActnInject.attr('action', settings.photos.comment.CommentFormAction );
			} else
			if( !settings.photosObj && settings.videoObj  && !settings.photoPage) {
				getUrl	=	settings.video.fetchUrl;
				dataKey	= {ecrId:val[0]};
				swift	=	1;
				formActnInject.attr('action', settings.video.comment.CommentFormAction );
			} else
			if( settings.photosObj && settings.videoObj  && !settings.photoPage) {
				if( swap	== 0 ) {
					getUrl	=	settings.photos.fetchUrl;
					swift	=	0;
					dataKey	= {contentId:val[0],contentType:val[1],contentAlbum:val[2]};
				formActnInject.attr('action', settings.photos.comment.CommentFormAction );
				}else if( swap == 1 ) {
					getUrl	=	settings.video.fetchUrl;
					dataKey	= {ecrId:val[0]};
					swift	=	1;
					formActnInject.attr('action', settings.video.comment.CommentFormAction );
				}
			} else
			if( settings.photoPage && settings.photosObj ) {
				getUrl	=	settings.photos.fetchUrl;
				dataKey	= {contentId:val[0],contentType:val[1],type:val[2]};
				formActnInject.attr('action', settings.photos.comment.CommentFormAction );
				swift	=	0;
				nextSwipe = 0;
			}
			$.ajax({
				url	: getUrl,
				dataType:"json",
				data:dataKey,
				type:"GET",
				cache: false,
				beforeSend: function() {
					displayImg.removeAttr('src');
					displayVideo.removeAttr('src');
					commentSection.remove();
					imagePlatform.hide();
					$(".vidSection").hide();
				},
				success: function( data ) {
					jsonObj	=	data;
					loadComment( val, data,null,swift);
					switch	(swift) {
						case 0:
							 //when there is only photos set to preview
							$("#vidSection").hide();
							if(data.userMe	===	data.userto){
								if(settings.setCoverPic){
									makeCoverPhoto.show();
									makeCoverBtn.attr({'data-id': val[0],'data-imgCat':val[1]});
									if(parseInt(data.coverImg) == 1){
										makeCoverBtn.children('i').removeClass('fa-image').addClass('fa-check');
									} else {
										makeCoverBtn.children('i').removeClass('fa-check').addClass('fa-image');
									}
								}else{
									makeCoverPhoto.hide();
								}
								reviewMessage.hide();
							} else{
								makeCoverPhoto.hide();
								reviewMessage.show();
							}
							removeAttr('.prev, .next', ['data-vidEncrId']);
							displayImg.add(workedBtn).show();
							zoomIn.add(zoomOut).add(resets).show();
							if(data.imageUrl) {
								displayImg.attr('src',data.imageUrl).load(function(){
									imagePlatform.show();
									imageWidth	=	imagePlatform.width();
								})
								.on('error', function(){
									displayImg.removeAttr('src');
									$('.bg_fixed').addClass('no_images');
										imagePlatform.show();
								});
							}
							setImage(data,imageWidth);
							if(data.imgDescrSmall) {
								descSection.show();
								descPopup.html(data.imgDescrSmall);
								   $(".readMore").on("click",function(){
									descPopup.html(data.imgDescrFull);
							   });
							} else {
								descSection.hide();
							}
							if( data.youLike ) {
								likeBtn.addClass('liked');
							} else if ( !data.youLike ){
								likeBtn.removeClass('liked');
							}
							
							if(data.youShare) {
								shareBtn.addClass('shared');
							} else if (!data.youShare) {
								shareBtn.removeClass('shared');
							}
							//var dataAlbumId	= (data.imageAlbumId > 1)?data.imageAlbumId:-1;
							
							if(nextSwipe != 0) {
								prev.attr({
									"data-contentId" 	: data.imagePrevId, 
									"data-contentType" 	: data.imagePrevType, 
									"data-contentAlbum"		: data.imageAlbumId
								});
								next.attr({
									"data-contentId" 	: data.imageNextId, 
									"data-contentType" 	: data.imageNextType, 
									"data-contentAlbum"		: data.imageAlbumId
								});
							} else if ( nextSwipe == 0 ) {
								if( nextSwipeData	!== 0 ) {
									prev.attr({
										"data-contentId" 		: data.imagePrevId, 
										"data-contentType" 		: data.imagePrevType, 
										"data-typeOf"			: val[2]
									});
									next.attr({
										"data-contentId" 		: data.imageNextId, 
										"data-contentType" 		: data.imageNextType, 
										"data-typeOf"			: val[2]
									});
								}
								loadMoreImages(data, masnoryUl, 9);
							}
							likeBtn.add(workedBtn).attr({
								"data-like"			:  val[0],	
								"data-likecat"		:  val[1],
								"data-userto"		:  data.userto,
							});
							shareBtn.attr({
								"data-share"		:  val[0],	
								"data-sharecat"		:  val[1],
								"data-userto"		:  data.userto,
							});
							likeCount.text(data.likeCount);
							shareCount.text(data.shareCount);
							commentCount.text(data.commentCount);
							if(data.workedTogether == 2) {
								workedBtn.children('i').removeClass('fa-users').removeClass('fa-star').addClass('fa-check');
								workedStatus	=	2;
							}else if(data.workedTogether == 1){
								workedBtn.children('i').removeClass('fa-check').removeClass('fa-star').addClass('fa-users');
								workedTogher();
								workedStatus	=	1;
							} else if(data.workedTogether == 3) {
								workedBtn.children('i').removeClass('fa-check').removeClass('fa-users').addClass('fa-star-o');
								workedStatus	=	3;
							}
							break;
						case 1:
							 //when there is only videos set to preview
							 
							if(data.userMe	===	data.userto){
								reviewMessage.add(makeCoverPhoto).hide();
							}
							displayImg.add(workedBtn).add(zoomIn).add(zoomOut).add(resets).hide();
							imagePlatform.show();
							removeAttr('.prev, .next', ['data-contentId', 'data-contentType', 'contentAlbum']);
							$('#id').attr('name', 'video_id');
							$('.comment_descr_input2').attr('name', 'comment_descr_input');
							$(".desc_photoz p").html(data.imgDescrSmall);
							   $(".readMore").on("click",function(){
								$(".desc_photoz p").html(data.imgDescrFull);
							   });
							if( data.youLike ) {
								likeBtn.addClass('liked');
							} else if ( !data.youLike ){
								likeBtn.removeClass('liked');
							}
							
							if(data.youShare) {
								shareBtn.addClass('shared');
							} else if (!data.youShare) {
								shareBtn.removeClass('shared');
							}
								
							jwplayer("vidSection").setup({
								flashplayer: "player.swf",
								image: data.videourl,
								file: data.videourl,
								skin: settings.videoSkin,
								width: "100%",
								height: "95%",
								autostart: true,
								stretching:"exactfit"
							});
							
							//end jwplayer
							prev.attr({
								"data-vidEncrId" 	: data.imagePrevId, 
							});
							next.attr({
								"data-vidEncrId" 	: data.imageNextId, 
							});
							
							likeBtn.attr({
								"data-like"			:  data.videoId,	
								"data-userto"		:  data.userto,
							});
							shareBtn.attr({
								"data-share"		:  data.videoId,
								"data-userto"		:  data.userto,
							});
							likeCount.text(data.likeCount);
							shareCount.text(data.shareCount);
							commentCount.text(data.commentCount);
							
							break;
							case 2:
							alert(data);
							break;
						
						default: return; // exit this handler for other keys
					}
					
					if(data.userImage !='' && data.userName != '' && data.imageCreatedOn != ''){
						$('.user_info img').attr('src', data.userImage);
						$('.user_data h3').text(data.userName);
						$('.user_data p').text(data.imageCreatedOn);
					}
					
					if(data.imagePrevId	==	null && data.imagePrevType	==	null || data.imagePrevId	==	'' ) {
						prev.hide();
					} else{
						prev.show();
					}
					if(data.imageNextId == null && data.imageNextType == null || data.imageNextId == '') {
						next.hide();
					} else{
						next.show();
					}
				},
				complete: function(){
					if (nextSwipe == 0) {
						bindEvents();
					}
				},
				error:function(){
					//if ajax failed to load the data
					console.log("failed to load the page. Error Executing Query");
				}
			});
	}
	var loadComment	=	function( val, data, pageNo,swift) {
		var getUrlComment,dataKeyComment,commentType,commentId;
			commentUserto		=	data.userto;
			var myImageUrl		=	data.myimage;
			
		if(settings.defaults && !settings.album && !settings.photoPage || !settings.defaults && !settings.album && settings.photoPage) {
			if( swift == 0 ) {
				commentType		=	val[0];
				commentId		=	val[1];
				
				getUrlComment	= 	settings.photos.ajaxUrl;
				
				if (settings.defaults && !settings.album && !settings.photoPage ) {
					
					dataKeyComment	=	{imageId:commentType,im_type:commentId,userto:data.userto,myimage:data.myimage,page:pageNo}	
					
				} else if ( !settings.defaults && !settings.album && settings.photoPage ) {
					
					dataKeyComment	=	{imageId:commentType,im_type:2,userto:data.userto,myimage:data.myimage,page:pageNo}
				}
				
			}else if( swift == 1 ){
				commentType		=	val[0];
				
				getUrlComment	=	settings.video.ajaxUrl;
				dataKeyComment	=	{imageId:commentType,userto:data.userto,im_type:4,page:pageNo}
			}
		} else if(!settings.defaults && settings.album && !settings.photoPage) {
			if( swift == 0 ) {
				imageId		=	val[0];
				im_type		=	val[1];
				albumId		=	val[2];
				
				getUrlComment	= 	settings.photos.ajaxUrl;
				dataKeyComment	=	{imageId:imageId,im_type:im_type,albumId:albumId}	
				
			} else if( swift == 1 ) {
				ecrId		=	val[0];
				
				getUrlComment	=	settings.video.ajaxUrl;
				dataKeyComment	=	{ecrId:ecrId}
			}
		} 
		$.ajax({
			url:getUrlComment,
			type:'GET',
			dataType:"html",
			cache: false,
			data:dataKeyComment,
			beforeSend: function() {
				sideBarInside.addClass('loadingData');
			},
			success: function( data ) {
					sideBarInside.removeClass('loadingData');
					if( settings.defaults && !settings.album && !settings.photoPage || !settings.defaults && !settings.album && settings.photoPage) {
						$('.prof_thump img').attr('src', myImageUrl);
						$('.comments_Extr').html(data);
						$('#id').val(commentType);
						$('#userto').val(commentUserto);
						
						if (settings.defaults && !settings.album && !settings.photoPage ) {
						switch (swift){
						case 0:
							$('#type').val(commentId);
						break;
						case 1:
							$('#type').val('');
						break;
						default: return;
						}
					} else if ( !settings.defaults && !settings.album && settings.photoPage ) {
						$('#type').val('2');
					}
					
				} else if( !settings.defaults && settings.album ) {
					switch ( swift ) {
						case 0:
						$('#middelSec').html(data);
						break;
						case 1:
						$('#middelSec').html(data);
						break;
					}
					
				}
			},
			error: function(){
				colsole.log('error fectching comment section from database');
			},
			complete: function(){
				onPageClick		=	commentParent.find('.loadMoreTime');
				deleteComment	=	commentParent.find('.deletComment');
				pageNo	=	onPageClick.attr('data-pageNo');
				initCallbackAjax(pageNo,deleteComment);
				
			}
		});
	}
	var eventHandlers	=	function() {
		inst.add(prev).add(next).click(function(){
			var _this	=	$(this);
			clearAll(next);
			if(settings.photosObj && !settings.videoObj && !settings.photoPage){
				var fetchedData = getDataAttr(_this, ['data-contentId','data-contentType','data-contentAlbum']);
				ajaxload(fetchedData);
				swap	=	0;
			}
			if(!settings.photosObj && settings.videoObj && !settings.photoPage){
				var _this	=	$(this);
				var fetchedData = getDataAttr(_this, ['data-vidEncrId']);
				ajaxload(fetchedData);
				swap	=	1;
			}
			if(settings.photosObj && settings.videoObj && !settings.photoPage){
				var vidEncrId	= $(this).attr('data-vidEncrId');
				var contentId	= $(this).attr('data-contentId');
				
				if(typeof contentId !== typeof undefined){
					swap	=	0;
					var _this	=	$(this);
					var fetchedData = getDataAttr(_this,  ['data-contentId','data-contentType','data-contentAlbum']);
					ajaxload(fetchedData);
					
				}else if(typeof vidEncrId !== typeof undefined){
					swap	=	1;
					var _this	=	$(this);
					var fetchedData = getDataAttr(_this, ['data-vidEncrId']);
					ajaxload(fetchedData);
				} 
			}else if(settings.photosObj && settings.photoPage){
				swap	=	0;
				var _this	= $(this);
				var fetchedData	= getDataAttr(_this, ['data-contentId', 'data-contentType', 'data-typeOf']);
				ajaxload(fetchedData);
				
			}else {
					console.log('Error fetching Data-attr, Please verify the data attribute provided');
				}
			grandParent.show();
			
			//adjusting lightbox in accordance with screen size
				adjWidth	=	parent.width();
				implementWidth	=	adjWidth-320;
				$('.bg_fixed').css({
					"width"	:	implementWidth,
			});
			var sideBarHeight	=	$('#side_bar').height();
			var maxHeightMiddle	=	sideBarHeight-186;
			$('#middelSec').css({'height' : maxHeightMiddle});
		});
		$("#terminate").click(function() {
			grandParent.hide();
			$('.user_data h3').text('');
			$('.user_data p').text('');
			$('.bg_img_stand').removeClass('bg_img_stand_img').css({width:'100%',height:'100%',top:0,left:0});
			if( swap == 1 ) {
				jwplayer( 'vidSection' ).stop();
			}
        });
		$(".overlay_bg").mouseup(function(e){
				var subject = $(".lightBox"); 
				if(e.target.id != subject.attr('id') && !subject.has(e.target).length) {
					 $(".overlay_bg").hide();
				}
		});
		$(document).keyup(function(e) {
		  switch(e.which) {
			case 27: // esc
			$('.bg_img_stand').removeClass('bg_img_stand_img').css({width:'100%',height:'100%',top:0,left:0});
			  grandParent.hide();
			if( swap == 1 ) {
				jwplayer( 'vidSection' ).stop();
			}
			break;
			case 37: // left
				if( swap == 0 ) {
					var fetchedData = getDataAttr(prev,  ['data-contentId','data-contentType','data-contentAlbum']);
						ajaxload(fetchedData);
				} else if ( swap == 1 ) {
					var fetchedData = getDataAttr(prev, ['data-vidEncrId']);
						ajaxload(fetchedData);
				}
			break;
	
			case 39: // right
				if( swap == 0 ) {
					var fetchedData = getDataAttr(next,  ['data-contentId','data-contentType','data-contentAlbum']);
					ajaxload(fetchedData);
				} else if ( swap == 1 ) {
					var fetchedData = getDataAttr(next, ['data-vidEncrId']);
				ajaxload(fetchedData);
				}
			break;
	
			default: return; // exit this handler for other keys
		}
		e.preventDefault(); // prevent the default action (scroll / move caret)
	});
		

		//----cooment
		
	 $('.comment_descr_input2').keydown(function(event){
		 recentCommentCount	=	commentCount.text();
		  if (event.keyCode == 13 && !event.shiftKey) {
			$(this).closest("form").find('.ajaxloader').show();
			$(this).closest("form").ajaxForm(
				{
					success:successCommentCall
					
				}).submit();
				return false;
		  }
	});
	
	var successCommentCall = 	function(){
			$('.ajaxloader').hide();
			$(".comment_descr_input2").val('');
			loadComment(arrayVal, jsonObj, null, swap);
			countVariation(recentCommentCount,commentCount,1);
		}
	
	likeBtn.on('click',function(){		
		ajaxify	=	0;
		recentLikeCount	= likeCount.text();

		if( swap == 0 ) {
			var likeData	= getDataAttr($(this), ['data-like', 'data-likecat', 'data-userto']);
				if( likeData.length > 0 ) {
					fetchDataLikes(likeData, recentLikeCount);
				}
		} else if (swap == 1) {
			var likeData	= getDataAttr($(this), ['data-like', 'data-userto']);
			fetchDataLikes(likeData, recentLikeCount);
		}
	});
		
	//Comment Like 
	commentParent.on('click', '.like_button ', function(){
		ajaxify	= 3;
		var that 	=	$(this);
		var CountLikes	=	that.children('span');
		recentCommentLikeCount	=	CountLikes.text();
		var commentLikeData	=	getDataAttr(that, ['data-like', 'data-userto']);
		fetchDataLikes(commentLikeData, null, that);
	});
	//share photo
	shareBtn.on("click",function(){
		recentShareCount	= shareCount.text();
		ajaxify	=	1;
		if( swap == 0 ) {
			var shareData	= getDataAttr($(this), ['data-share', 'data-sharecat', 'data-userto']);
				fetchDataLikes(shareData);
		} else if( swap == 1 ) {
			var shareData	= getDataAttr($(this), ['data-share', 'data-userto']);
				fetchDataLikes(shareData);
		}
		});
	}
	var setImage	=	function(data, imgWidth){
			imagePlatform.addClass('bg_img_stand_img');
		var objWidth,objHeight,contWidth,conHeight;
		var scaleVal		=	1.4;
			limitValue		=	4;
			pauseValue		=	limitValue*2;
			contHeight		=	$('.bg_fixed').height();
			
			var getVal	=	resizeImages(data.imageWidth,data.imageHeight,implementWidth,contHeight);
			data.imageWidth	=	getVal[0];
			data.imageHeight	=	getVal[1];
			imagePlatform.css({width:data.imageWidth,height:data.imageHeight,top:getVal[3],left:getVal[2]});
			
			objWidth	= imagePlatform.width();	
			objHeight	= imagePlatform.height();
			contWidth	= implementWidth;
			conHeight	= contHeight;
			
			//zoomImage(objWidth,objHeight,contWidth,conHeight);//
			zoomInside(objWidth,objHeight,contWidth,conHeight);
			zoomOutside(objWidth,objHeight,contWidth,conHeight);
			resetImage(objWidth,objHeight,contWidth,conHeight);
	}
	
	var resizeImages	=	function(width, height, contWidth, contHeight) {
		var ratioWidth,ratioHeight,adjWidth,adjHeight,adjLeft,adjTop;
		var elemSize	=	[];
		ratioWidth	=	contWidth/width;
		ratioHeight	=	contHeight/height;
		if(width > contWidth || height > contHeight) {
			if(width > contWidth){
				adjWidth	=	contWidth;
				adjHeight	=	height*ratioWidth;
			}
			if(height > contHeight) {
				adjWidth	=	width*ratioHeight;
				adjHeight	=	contHeight;
			}
		}else{
			adjWidth	=	width;
			adjHeight	=	height;
		}
		
		adjLeft	=	parseInt((contWidth -adjWidth)/2);
		adjTop	=	parseInt((contHeight - adjHeight)/2);
		
		elemSize.push(adjWidth,adjHeight,adjLeft,adjTop);
		
		return elemSize;
		
	}
	var zoomInside	=	function(width,height,conWidth,conHeight){
		var scaleVal	=	1.4;
			limitValue		=	4;
			pauseValue		=	limitValue*2;
			zoomIn.attr({'data-width': width, 'data-height':height });
		zoomIn.on('click', function(event){
				imagePlatform.addClass('trnasitionClass');
			var	_this	=	$(this);
			 	fetchedData = getDataAttr(_this, ['data-width', 'data-height']);
				event.preventDefault();
				event.stopPropagation();
				objWidthNew 	=	imagePlatform.width();
				objHeightNew	=	imagePlatform.height();
				scaleValWid	=	Math.round(objWidthNew*scaleVal);
				scaleValHig	=	Math.round(objHeightNew*scaleVal);
				offsetVal = imagePlatform.position().top;
				if(scaleValWid > fetchedData[0]*pauseValue) {
					scaleValWid	=	fetchedData[0]*pauseValue;
					scaleValHig	=	fetchedData[1]*pauseValue;
				}
				imagePlatform.css({
					width	: scaleValWid,
					height 	: scaleValHig,
					left 	: (conWidth - scaleValWid)/2,
					top 	: (conHeight - scaleValHig)/2,	
					
				});
				setTimeout(function(){
					topVal	=	imagePlatform.css('top');
					offsetVal = imagePlatform.position().top;
					offsetValLeft = imagePlatform.position().left;
				if(offsetVal < 0 || offsetValLeft< 0) {
					drag(imagePlatform,null,conWidth,conHeight);
				}
				}, 500);
				event.stopImmediatePropagation();
	});
		
	}
	var zoomOutside	=	function(width,height,conWidth,conHeight){
		var scaleVal	=	1.4;
			limitValue		=	1.4;
			pauseValue		=	limitValue*2;
			zoomOut.attr({'data-width': width, 'data-height':height });
		zoomOut.on('click', function(event){
			imagePlatform.addClass('trnasitionClass');
			var _this	=	$(this);
				fetchedData = getDataAttr(_this, ['data-width', 'data-height']);
			event.preventDefault();
			event.stopPropagation();
			objWidthNew 	=	imagePlatform.width();
			objHeightNew	=	imagePlatform.height();
			scaleValWid	=	Math.round(objWidthNew/scaleVal);
			scaleValHig	=	Math.round(objHeightNew/scaleVal);
			if(scaleValWid < fetchedData[0]) {
				scaleValWid	=	fetchedData[0];
				scaleValHig	=	fetchedData[1];
			}
			imagePlatform.css({
				width	: scaleValWid,
				height 	: scaleValHig,
				left 	: (conWidth - scaleValWid)/2,
				top 	: (conHeight - scaleValHig)/2,	
			});
			setTimeout(function(){
				offsetVal = imagePlatform.position().top;
				 offsetValLeft	=	imagePlatform.position().left;
				
				if(offsetVal >= 0 && offsetValLeft >= 0 ) {
					imagePlatform.removeClass('dragable').removeClass('trnasitionClass');
				}
			},500);
			event.stopImmediatePropagation();
	});
	}
	var resetImage	=	function(width,height,contWidth,conHeight){
		resets.attr({'data-width':width, 'data-height': height});
		resets.on('click', function(){
			imagePlatform.addClass('trnasitionClass');
			var _this	=	$(this);
			var defaultWidth	=	_this.attr('data-width');
				defaultHeight	=	_this.attr('data-height');
			imagePlatform.css({
				width	:	defaultWidth,
				height	:	defaultHeight,
				left 	: (contWidth - defaultWidth)/2,
				top 	: (conHeight - defaultHeight)/2,	
			});
			
		
	});
	}
	
	function drag(element, dir,contWidth,conHeight ){
			var pos_y,pos_x;
			element.addClass('dragable');
		var dragable	=	$('.dragable');
			dragable.on('mousedown', function(e){
					e.preventDefault();
					var _this	=	$(this);
					$('.dragable').addClass('draged');
					_this.removeClass('trnasitionClass');
					pos_y	=	_this.offset().top + _this.outerHeight() - e.pageY;
					pos_x	=	_this.offset().left + _this.outerWidth() - e.pageX;
				});
			dragable.on('mousemove', function(e){
				e.preventDefault();
				 x	=	e.pageX + pos_x - $('.draged').outerWidth();
				 y	=	e.pageY + pos_y - $('.draged').outerHeight();
				 
				 offsetVal = imagePlatform.position().top;
				 offsetValLeft	=	imagePlatform.position().left;
				 offseBottom	=	offsetVal+imagePlatform.outerHeight(true);
				 offsetRight	=	offsetValLeft+imagePlatform.outerWidth(true);
				 
				 if(offsetValLeft <= 0 && offsetVal <= 0) {
					 $('.draged').offset({
						top :	y,
						left:	x,
					});
					
					offsetVal = imagePlatform.position().top;
					offsetValLeft	=	imagePlatform.position().left;
					offseBottom	=	offsetVal+imagePlatform.outerHeight(true);
					offsetRight	=	offsetValLeft+imagePlatform.outerWidth(true);
					
					if(offsetVal > 0 || offsetValLeft > 0 || offseBottom < conHeight || offsetRight < contWidth){
						if(offsetVal > 0) {
							dragable.css({ top : 0, bottom : 'auto'});
							dragable.remove('draged');
						}
						if(offsetValLeft > 0){
							dragable.css({ left : 0});
							dragable.remove('draged');
						}
						if(offseBottom < conHeight){
							dragable.css({ bottom : 0, top: 'auto'});
							dragable.remove('draged');
						}
						if(offsetRight < contWidth){
							dragable.css({ right : 0, left: 'auto'});
							dragable.remove('draged');
						}
					}
					 
				 }else if(offsetValLeft <= 0) {
					$('.draged').offset({
						left:	x,
					});
				 	offsetValLeft	=	imagePlatform.position().left;
					offsetRight	=	offsetValLeft+imagePlatform.outerWidth(true);
					if(offsetValLeft > 0){
						dragable.css({ left : 0});
						dragable.remove('draged');
					}
					if(offsetRight < contWidth){
						dragable.css({ right : 0, left: 'auto'});
						dragable.remove('draged');
					}
				 }else if(offsetVal <= 0) {
					$('.draged').offset({
						top:	y,
					});
				 	offsetVal	=	imagePlatform.position().top;
					offseBottom	=	offsetVal+imagePlatform.outerHeight(true);
						if(offsetVal > 0) {
							dragable.css({ top : 0, bottom : 'auto'});
							dragable.remove('draged');
						}
						if(offseBottom < conHeight){
							$('.dragable').css({ bottom : 0, top: 'auto'});
							$('.dragable').remove('draged');
						}
					if(offseBottom < conHeight){
						dragable.css({ bottom : 0, top: 'auto'});
						dragable.remove('draged');
					}
				 }
				
			});
			$(window).on('mouseup', function(e){
				e.preventDefault();
				$('.bg_img_stand_img').removeClass('draged');
			});
	}
		
	var workedTogher = function(){
	// worked together
		workedBtn.on('click', function(){
			if(workedStatus === 1) {
				$(this).addClass('active');
				$(this).children('.fa-users').removeClass('fa-users').addClass('this');
				ajaxify	=	2;
				var workedData	=	getDataAttr($(this), ['data-like', 'data-likecat', 'data-userto']);
				fetchDataLikes(workedData);
			}
		});
	}
	
	//like,  share and worked Together
	
	var fetchDataLikes	=	function(fetchedData, recentLikeCount, that){
		//var ajaxifyUrl,dataKeySocial;
		switch ( swap ) {
			case 0:
			if(ajaxify == 0){
				ajaxifyUrl	=	settings.photos.shareItems.likeAjaxUrl;
				dataKeySocial	=	{likeId:fetchedData[0],likecat:fetchedData[1],imgUserId:fetchedData[2]};
				
			} else if ( ajaxify == 1 ){
				ajaxifyUrl	=	settings.photos.shareItems.shareAjaxUrl;
				dataKeySocial = {shareId:fetchedData[0],shareCat:fetchedData[1],shareWhoseId:fetchedData[2]};
				
			} else if ( ajaxify == 2 ) {
				ajaxifyUrl	=	settings.photos.shareItems.workedAjaxUrl;
				dataKeySocial	=	{img_id:fetchedData[0],img_cat:fetchedData[1],send_to:fetchedData[2]};
				
			} else if( ajaxify == 3 ) {
				ajaxifyUrl = settings.photos.comment.commentLikeUrl;
				dataKeySocial= {likeId:fetchedData[0],likecat:3,imgUserId:fetchedData[1],imageType:arrayVal[1],imageId:arrayVal[0]}
			}
			break;
			case 1:
			if( ajaxify == 0 ) {
				ajaxifyUrl	=	settings.video.shareItems.likeAjaxUrl;
				dataKeySocial	=	{likeId:fetchedData[0],likecat:4,imgUserId:fetchedData[2]};
				
			} else if ( ajaxify == 1 ){
				ajaxifyUrl	=	settings.video.shareItems.shareAjaxUrl;
				dataKeySocial = {shareId:fetchedData[0],shareCat:3,shareWhoseId:fetchedData[1]};
				
			} else if( ajaxify == 3 ) {
				ajaxifyUrl = settings.video.comment.commentLikeUrl;
				dataKeySocial= {likeId:fetchedData[0],likecat:3,imgUserId:fetchedData[1],imageType:arrayVal[1],imageId:arrayVal[0]}
			}
			break;
		}
		
		$.ajax({
		url: ajaxifyUrl,
		method: "POST",
		data:dataKeySocial,
		success:function(result){
			
			switch	( ajaxify ) {
				case	0:
					if( result == 'liked' ) {
						likeBtn.addClass('liked');
						countVariation(recentLikeCount,likeCount,1);
					}
					else if( result == 'like' ) {
						likeBtn.removeClass('liked');
						countVariation(recentLikeCount,likeCount,0);
					}
				break;
				case	1:
					if(result == 'shared') {
						shareBtn.addClass('shared');
						countVariation(recentShareCount,shareCount,1);
					}
					else if(result == 'share') {
						shareBtn.removeClass('shared');
						countVariation(recentShareCount,shareCount,0);
					}
				break;
				case	2:
					if( result == 'workedTogether' ) {
						workedBtn.removeClass('active');
						workedBtn.children('.this').removeClass('this').addClass('fa-check');
						workedStatus	= 2;
					}
					
				break;
				case	3:
					if( result	==	'cmdLiked' ) {
						that.addClass('liked');
						CountLikes	=	that.children('span');
						countVariation(recentCommentLikeCount,CountLikes,1);
						
					} else if ( result == 'cmdLike') {
						that.removeClass('liked');
						CountLikes	=	that.children('span');
						countVariation(recentCommentLikeCount,CountLikes,0);
					}
				break;
			}
		
		  }
		});
	}
	
	//remove attr of any elemet by passing it as parameter
	// eg :-   removeAttr('.prev,.next,.close, elem', ['data-user, 'data-info', 'data-exg', 'arrayAttr']);
	
	var removeAttr =  function( elem, arrayAttr ) {
			for(i=0; i<arrayAttr.length; i++) {
				$(elem).removeAttr(arrayAttr[i]);
			}
	}
	
	// fetching attr of element and pushing it into respective array
	// eg :- var fetchedData  =  getDataAttr($(this), ['data-user, 'data-info', 'data-exg', 'arrayAttr']);
	// data will return to 'fetchedData'
	
	var getDataAttr		=	function( _this, attrArray ) {
		var contentArr	=	[];
		if(attrArray.length > 0) {	
		
			for( i=0; i<attrArray.length; i++ ) {
				var dynamicVar	=	_this.attr(attrArray[i]);
				contentArr.push(dynamicVar);
			}
			return contentArr;
			
		} else {
			console.log(' error fetching data from the data-attribute on click event :( ');
		}
	}
	
	// increment and decrement of count (like, share, comment);
	//	1 for increment and 0 for decrement
	//	eg:- countVariation(recentCount, element, 1/0);
	var countVariation	=	function(recentCount,elemnt,action) {
		
		if(action == 1) {
			recentCount	=	elemnt.text();	
			newCount	= parseInt(recentCount) + 1;
			elemnt.text(newCount);
		} else if (action == 0) {
			recentCount	=	elemnt.text();	
			newCount	= parseInt(recentCount) - 1;
			elemnt.text(newCount);
			
		}
	}
	
	//extract json array for side images
	
	var loadMoreImages	=	function( data, element, limitVal ) {
		if(data.relatedImages.length > limitVal) {
				var range	=	data.relatedImages.length - limitVal;
			} else {
				var limitVal = data.relatedImages.length;
			}
		
		if((data.relatedImages.length -1) < limitVal) {
			limitVal	=	data.relatedImages.length;
			$('.loadMorePicz').remove();
			
		}
		for( k=parseInt(startVal); k<parseInt(limitVal); k++ ) {
			element.append('<li><a href="javascript:;" data-contentid="'+data.relatedImages[k].contentId+'" data-contenttype="'+data.relatedImages[k].contentType+'" data-typeof="'+data.relatedImages[k].type+'"><img src="'+data.relatedImages[k].relatedImgUrl+'"></a></li>');
		}
		
		if( !$('.loadMorePicz').length && range !=0 && range > 0 ) {
			$('<a class="loadMorePicz" href="javascript:;" data-limit="'+limitVal+'">Load More Images</a>').insertAfter(element);
		} else {
			$('.loadMorePicz').attr('data-limit', limitVal);
		}
	}
	
	//binding click events to masnory div
	
	var bindEvents	=	function(){
		$('.loadMorePicz').bind('click', function(){
			startVal	=	$(this).attr('data-limit');
			limitVal	=	parseInt(startVal) + 9;
			loadMoreImages(jsonObj, masnoryUl,limitVal);
			
			masnoryUl.children('li').children('a').bind('click', function(){
				clearAll(next);
				swap	=	0;
				nextSwipeData	=	0;
				var _this	=	$(this);
				fetchData	=	getDataAttr(_this, ['data-contentid','data-contenttype','data-typeof']);
				ajaxload(fetchData);
			});
		});
		masnoryUl.children('li').children('a').bind('click', function(){
			clearAll(next);
			swap	=	0;
			nextSwipeData	=	0;
			var _this	=	$(this);
			fetchData	=	getDataAttr(_this, ['data-contentid','data-contenttype','data-typeof']);
			ajaxload(fetchData);
			
			
		});

	}
	//clear items adjustments
	
	var clearAll	= function(next,prev, close){
		
		if(next == next || prev == prev) {
			
			var arrayObjects	=	[masnoryUl,commentOverlay,descPopup];
			for( var i=0; i<arrayObjects.length; i++ ) {
				arrayObjects[i].html('');
			}
			$('.bg_fixed').removeClass('no_images');
			$('.loadMorePicz').remove();
			startVal	=	0;
			nextSwipeData	=	1;
			
		} else if ( close == close ) {
			
		}
	}
	
	var initCallbackAjax = function(pageNo,deleteComment){
		
		// loading More Comments
		var loadMoreComments	=	function(pageNo) {
			
			if(typeof pageNo !== typeof undefined) {
				onPageClick.click(function(){
						pageNo	= parseInt(pageNo)+1;
						loadComment(arrayVal,jsonObj,pageNo,swap);
				});
			}
		}
		
		//delete comments........///
		var deleteComment  = function(){
			$('.deletComment').on('click', 'a',function(){
				recentCommentCount	=	commentCount.text();	
				var elem = $(this).closest('.item');	
				var that		=	this;			
				$.confirm({
					'title'	  : 'Delete Confirmation',
					'message'	: 'You are about to delete this comment ?',
					'buttons'	: {
					'Yes'	: {
					'class'	: 'blue',
					'action': function(){
					elem.slideUp();
						$('.ajaxloader').show();
						var delid       =	$(that).data("delid");
						if(delid){
							$.ajax({
								url :settings.deleteCommentUrl,
								method:"POST",
								data:{delid:delid},
								success:function(result){
									loadComment(arrayVal,jsonObj, null, swap);
									$('.ajaxloader').hide();
									countVariation(recentCommentCount,commentCount,0);
								}});
						}
					}
					},
					'No'	: {
					'class'	: 'gray',
					'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
					}
					}
				});
			});
		}
		loadMoreComments(pageNo);
		deleteComment();
	}
	//end delete comments.../////
	
	initFunctions();
  };
})( jQuery );