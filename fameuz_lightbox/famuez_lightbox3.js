/*
Fameuz Plugin 1.0
 |----------------------------------------------------|
 |				Fameuz Lightbox Plugin				  |
 |				Developed by Designdays		     	  |
 |													  |
 |----------------------------------------------------|
 */
;(function ( $, window, document, undefined ) {

	//"use strict";
	
	
	//retriving siteroot dynamically
	var SITE_ROOT	=	getAbsolutePath();
	
	function getAbsolutePath() {
		var loc = window.location;
		var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
		return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
	}
	
		// Create the defaults once
		var pluginName = "fameuzLightbox",
				defaults = {
					class		: '',
					sidebar		: 'default',
					photos:
					 {
						imageLoadUrl	: 'ajax/popdata.php',
						data_attr		: [],
						likeUrl			: 'ajax/like.php',
						shareUrl		: 'ajax/share.php',
						workedUrl		: 'ajax/worked_together.php',
						commentBoxUrl	: 'widget/pop_image_comment_box.php',
						limitCount		: 10,
						comments		:
						 {
							commentActionUrl	: 'access/post_comment.php?action=add_comment',
							commentLikeUrl		: '',
							commentDelete		: 'access/delete_comment.php',
						},
						commonAction: 
						{
							review		: '',
							message		: '',
							likeUrl		: 'ajax/like.php',
							shareUrl	: '',
							workedUrl	: '',
						}
						
					},
					videos: 
					{
						videoLoadUrl	: '',
						data_attr		: [],
						likeUrl			: '',
						commentBoxUrl	: '',
						comments		: 
						{
							commentActionUrl	: '',
							commentLikeUrl		: '',
						},
						commonAction: 
						{
							review		: '',
							message		: '',
							likeUrl		: '',
							shareUrl	: '',
							workedUrl	: '',
						}
						
					},
					comments: 
					{
						deleteComment	: '',
					},
					skin: 
					{
						next	: '<img src='+SITE_ROOT+'fameuz_lightbox/images/next.png>',
						prev	: '<img src='+SITE_ROOT+'fameuz_lightbox/images/prev.png>',
						reset	: '<i class="fa fa-refresh"></i>',
						close	: '<img src='+SITE_ROOT+'fameuz_lightbox/images/close.png width="15">',
						loader	: SITE_ROOT+'images/ajax-loader.gif',
						review	: '<i class="fa fa-chevron-right"></i>',
						video	: SITE_ROOT+'jw_player/six/six.xml',
					}
		};

		// The actual plugin constructor
		function Plugin ( element, options ) {
				this.element = element;
				this.settings = $.extend( {}, defaults, options );
				this._defaults = defaults;
				this._name = pluginName;
				this.init();
		}

		// Avoid Plugin.prototype conflicts
		$.extend(Plugin.prototype, {
				init: function () {
					var self	=	this;
					var element	=	self.element;
					
					//if body was not build then build the overlay for lightbox
					if(!$('.overlay_bg').length){
						self.buildFrag();	
					}
					// initiating plugin in accordance With class
					$(element).click(function(e){
						var myClass	=	(self.settings.class !== '')?self.settings.class:'lightBoxs';
						if($(e.target).hasClass(myClass)) {
							self.targeted	=	$(e.target);
							self.cycle();
						}else if($(e.target).parent().hasClass(myClass)){
							self.targeted	=	$(e.target).parent();
							self.cycle();
						}
					});
				},
				buildFrag: function(){
					var self	=	this;
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
						//Plugin Body with different sidebars
					var sidebar	=	'<div class="side_bar" id="side_bar"><div class="user-data"><div class="user_info"><img/></div><div class="user_data"><h3></h3><p></p></div></div><div class="like_countz"><ul><li><i class="fa fa-heart"></i> <span class="likeCount">0</span></li><li><i class="fa fa-comments"></i> <span  class="commentCount">0</span></li><li><i class="fa fa-share-alt"></i> <span class="shareCount">0</span></li></ul></div><div class="image_comment_load"><div id="middelSec">'+(self.settings.sidebar	===	'photo'?'<div class="masnoryDiv"><ul></ul></div>':'')+'<div class="desc_photoz"><p></p></div><div class="comments_Extr"></div></div>'+(self.settings.sidebar	===	'album'?'':'<div class="comment_box"><div class="drop_shasdow"></div><div class="prof_thump"><img></div><div class="post_commnt"><form enctype="multipart/form-data" method="post" action=""><div class="arrow"><img src="http://fameuz.burjalsafacomputers.com/images/arrow_left.png" alt="arrow"></div><div class="textarea"><span class="smile_icon"><i class="fa fa-smile-o"></i></span><textarea class="comment_descr_input2" rows="1" name="comment_descr_input" style="height: 34px; overflow: hidden;"></textarea><div class="ajaxloader text-center"><img src="'+self.settings.skin.loader+'" alt="comments_arrow"></div><input type="hidden" value="1" name="type" id="type"><input type="hidden" value="84" name="video_id" id="id"><input type="hidden" value="36" name="userto" id="userto"><br><small>Press Enter to post.</small></form></div></div></div>')+'</div></div>';
					var overlay		=	'<div class="overlay_bg neew"><div class="lightBox">'+sidebar+'<div class="bg_fixed"><div class="img_small_prev_overlay"><div class="img_small_prev"><div class="drag_overlay"><div class="drag_zoom"></div><img></div></div><div class="zoom_value"><p>0%</p></div></div><div class="dismiss" id="terminate">'+self.settings.skin.close+'</div><div class="zoom"><i class="fa fa-search-plus zoomIn"></i><i class="fa fa-search-minus zoomOut"></i><i class="fa fa-refresh reset"></i><i class="fa fa-gear switch"><span class="drop_down"><span class="left_side">Sidebar Left</span><span class="right_side">Sidebar Right</span></span></i></div><span class="prev">'+self.settings.skin.prev+'</span><span class="next">'+self.settings.skin.next+'</span><div class="share_like_box"><div class="review_message"><a>Add Review <i class="fa fa-chevron-right"></i></a><a>Message <i class="fa fa-chevron-right"></i></a></div><div class="like"><a class="like_btn_pop" href="javascript:;"><i class="fa fa-heart"></i> Like</a></div><div class="share"><a class="share_btn_pop" data-share="100" data-sharecat="1" href="javascript:;"><i class="fa fa-share-alt"></i>Share </a></div><div class="makeCover"><a class="makeCover_btn" href="javascript:;"><i class="fa fa-image"></i>Make Cover Photo </a></div><div class="worked"><a href="javascript:;" class="worked_btn worked_request has-spinner" data-like="100" data-likecat="1"><span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span><i class="fa fa-users"></i>Worked Together</a></div></div><div class="bg_img_stand"><img><div id="vidSection" class="vidSection"></div></div></div></div></div>';
					
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
					var wrapper	=	$('body');
					wrapper.append(overlay);
				},
				cycle: function () {
					var self	=	this;
						self.trigger();
						self.initPlugin();
				},
				trigger: function(){
					var self	=	this;
					
					//global variables
					self.switchCase	=	false;
					self.zoomExtends = {
						scale : 1.4,
						limit : 1.6,
						pause : parseInt(1.6*2),
					}	
					
					//cache jquery objects
					self.mainBody			=	$('body');
					self.lightbox			=	$('.lightbox');
					self.grandParent		=	$('.overlay_bg');
					self.mainContainer		=	$('.bg_fixed');
					self.dismiss			=	$('#terminate');
					self.displayImg			=	$(".bg_img_stand img");
					self.imagePlatform		=	$('.bg_img_stand');
					self.commentSection		=	$('.comment');
					self.sideBar			=	$('.side_bar');
					self.prev				=	$('.prev');
					self.next				=	$('.next');
					self.sideBarInside		=	$('.side_bar');
					self.displayVideo		=	$("#vidUrl");
					self.VideoPlatform		=	$("#vidSection");
					self.formActnInject		=	$('.post_commnt form');
					self.commentParent		=	$('.comments_Extr');
					self.zoomIn				=	$('.zoomIn');
					self.zoomOut			=	$('.zoomOut');
					self.resets				=	$('.reset');
					self.zoomVal			=	$('.zoom_value p');
					self.imgPrevZoom		=	$('.img_small_prev img');
					self.zoomSectnOverlay	=	$('.img_small_prev_overlay');
					self.likeBtn			=	$('.like_btn_pop');
					self.shareBtn			=	$('.share_btn_pop');
					self.workedBtn			=	$('.worked_btn');
					self.likeCount			=	$('.likeCount');
					self.shareCount			=	$('.shareCount');
					self.commentCount		=	$('.commentCount');
					self.commentLikeBtn		=	$('.like_button ');
					self.masnoryUl			=	$('.masnoryDiv ul');
					self.descSection		=	$('.desc_photoz');
					self.descPopup			=	$(".desc_photoz p");
					self.commentOverlay		=	$('.comments_Extr');
					self.reviewMessage		=	$('.review_message');
					self.makeCoverPhoto		=	$('.makeCover');
					self.makeCoverBtn		=	$('.makeCover_btn');
					self.userImage			=	$('.user_info img');
					self.userName			=	$('.user_data h3');
					self.timeUpdated		=	$('.user_data p');
					self.appendComment		=	$('#middelSec');
					self.myImageThumb		=	$('.prof_thump img');
					self.commentTextarea	=	$('.comment_descr_input2');
					self.leftSide			=	$('.left_side');
					self.rightSide			=	$('.right_side');
					self.deleteComents		=	$('.deletComment a');
					
					// jquery object defined
					self.initValue	=	0;
					self.countTemp		=	(self.settings.photos.limitCount)?self.settings.photos.limitCount:10;
					var windowHeight	=	$(window).height();
					self.count	=	(windowHeight <= 700)?3:self.countTemp;
					self.inputObj	=	{
						id		: $('#id'),
						type	: $('#type'),
						userto	: $('#userto'),
					};
					
					self.appendComment.perfectScrollbar();
					self.resetScrollTop();
					var _this	=	self.targeted;
					self.show(_this);
					self.next.add(self.prev).bind('click', function(){
						var _this	=	$(this);
						self.show(_this);
						if(self.settings.sidebar === 'photo'){
							self.masnoryUl.html('');
							self.resetScrollTop();
						}
					});
					self.likeBtn.bind('click', function(){
						var _this	=	$(this);
						var arrayofData	=	(self.typeofFile == 'image')?['data-like','data-likecat','data-userto']:['data-like','data-userto'];
						var resultsFetched	=	self.fetchDataAttr(arrayofData,_this);
						var postValues	= (self.typeofFile == 'image')?{likeId:resultsFetched[0],likecat:resultsFetched[1],imgUserId:resultsFetched[2]}:{likeId:resultsFetched[0],likecat:4,imgUserId:resultsFetched[1]};
						self.likeBtn.toggleClass('liked');
						self.likeTrigger(postValues);
					});
					self.shareBtn.bind('click', function(){
						var _this	=	$(this);
						var arrayofData	=	(self.typeofFile == 'image')?['data-share','data-sharecat','data-userto']:['data-share','data-userto'];
						var resultsFetched	=	self.fetchDataAttr(arrayofData, _this);
						var postValues	=	(self.typeofFile == 'image')?{shareId:resultsFetched[0],shareCat:resultsFetched[1],shareWhoseId:resultsFetched[2]}:{shareId:resultsFetched[0],shareCat:3,shareWhoseId:resultsFetched[1]};
						self.shareBtn.toggleClass('shared');
						self.shareTrigger(postValues);
					});
					self.workedBtn.bind('click', function(){
						var _this	=	$(this);
						var arrayofData	= ['data-like','data-likecat','data-userto'];
						var resultsFetched	= self.fetchDataAttr(arrayofData,_this);
						var postValues	=	{img_id:resultsFetched[0],img_cat:resultsFetched[1],send_to:resultsFetched[2]};
						self.workedTogetherTrigger(postValues,_this);
					});
					self.leftSide.bind('click', function(){
						self.sideBar.addClass('toggleSidebar');
						self.mainContainer.addClass('toggleBg');
					});
					self.rightSide.bind('click', function(){
						self.sideBar.removeClass('toggleSidebar');
						self.mainContainer.removeClass('toggleBg');
					});
					self.commentTextarea.on('keyup', function(event){
						if(event.keyCode == 13 && !event.shiftKey){
							var closestForm	=	$(this).closest('form');
							closestForm.find('.ajaxloader').show();
							closestForm.ajaxForm({
								success:sucessCallComment
							}).submit();
							return false;
						}
					});
					function sucessCallComment(){
						self.fetchComments(self.globalElement, self.RecentpageNo);
						var recentCount = parseInt(self.commentCount.html());
						self.commentCount.html(recentCount+1);
						self.commentTextarea.val('');
						$('.ajaxloader').hide();
					}
					self.zoomIn.bind('click', function(e){
						e.preventDefault();
						e.stopPropagation();
						self.zoomInside();
					});
					self.zoomOut.bind('click', function(e){
						e.preventDefault();
						e.stopPropagation();
						self.zoomOutside();
					});
					self.resets.bind('click', function(e){
						e.preventDefault();
						e.stopPropagation();
						self.resetZoom();
					});
					
					self.masnoryUl.on('click', '.popInside', function(){
						var _this	=	$(this);
						self.show(_this);
					});
					self.appendComment.on('click', '.loadMorePicz', function(){
						var _this	=	$(this);
						var initValue	=	parseInt(_this.attr('data-limit'));
						var count	=	parseInt(initValue)+ self.count;	
						self.loadMasnory(self.globalElement, initValue, count);
					});
					self.commentParent.on('click', '.loadMoreTime', function(){
						var pageNo	=	$(this).attr('data-pageno');
						if(typeof pageNo !== typeof undefined){
							var newPageNo	=	parseInt(pageNo)+1;
							self.fetchComments(self.globalElement, newPageNo);
						}
					});
					$(document).keyup(function(e) {
						switch(e.which){
							case 27:
							self.close();
							break;
						}
					});
					$(".overlay_bg").mouseup(function(e){
						var subject = $(".lightBox"); 
						if(e.target.id != subject.attr('id') && !subject.has(e.target).length) {
							 self.close();
						}
					});
				},
				initPlugin: function(){
					var self	=	this;
					if(self.grandParent.css('display') !== 'block') {
						self.grandParent.show();
					}
				},
				show: function(_this){
					var self	=	this;
					self.typeofFile	=	self.validateFileType(_this);
					if(self.typeofFile == 'image') {
						self.return = self.fetchDataAttr((self.settings.photos.data_attr.length > 0)?self.settings.photos.data_attr:self.typeofFile, _this);
					}else if(self.typeofFile == 'video') {
						self.return = self.fetchDataAttr((self.settings.videos.data_attr.length > 0)?self.settings.videos.data_attr:self.typeofFile, _this);
					}
					self.fetch(self.return, self.typeofFile).done(function(data){
						self.globalElement	=	data;
						if(self.settings.sidebar	===	'photo'){
							self.loadMasnory(data, 0, self.count);
						}
						self.fetchComments(data).error(function(){
							throw new Error('Error loading comments form database :-() ');
						});
						self.settingBuildFrag(self.typeofFile, data);
						self.exhibit(data, self.RecentpageNo);
						self.dismiss.bind('click', function(){
							self.close();
						});
					});
				},
				exhibit: function(data){
					var self	=	this;
					self.removeDataAttributes(self.next);
					self.removeDataAttributes(self.prev);
					self.onSucess();
					self.supply(data);
					self.display(data, self.typeofFile);
					self.like(data, self.val, self.typeofFile);
					self.share(data, self.val, self.typeofFile);
					self.workedTogether(data, self.val, self.typeofFile);
					(self.settings.sidebar !== 'album')?self.triggerCmnt(data):null;
				},
				fetch: function(val, typeofFile){
					var self	=	this;
					self.val	=	val;
					var url	=	(typeofFile	==	'image')?self.settings.photos.imageLoadUrl:self.settings.videos.videoLoadUrl;
					if(self.settings.sidebar != 'photo'){
						var dataKey	= (typeofFile == 'image')?{contentId:val[0],contentType:val[1],contentAlbum:val[2]}:{ecrId:val[0]};
					}else if (self.settings.sidebar === 'photo'){
						var dataKey = {contentId:self.val[0],contentType:self.val[1],type:self.val[2]};
					}
					return $.ajax({
						url	: url,
						dataType:"json",
						data:dataKey,
						type:"GET",
						cache: false,
						beforeSend: function(){
							self.beforeSend();
						},
						error: function(){
							console.log('error fetching data from database');
						}
					});
				},
				fetchComments: function(data,pageNo){
					var self	=	this;
					var url = (self.typeofFile == 'image')?self.settings.photos.commentBoxUrl:self.settings.videos.commentBoxUrl;
					if(self.settings.sidebar !== 'album'){
						var dataKey	=	(self.typeofFile == 'image')?{imageId:self.val[0],im_type:self.val[1],userto:data.userto,myimage:data.myimage,page:pageNo}:{imageId:self.val[0],userto:data.userto,im_type:4,page:pageNo};
					}else if(self.settings.sidebar === 'album'){
						var dataKey	=	(self.typeofFile === 'image')?{imageId:self.val[0],im_type:self.val[1],albumId:self.val[2]}:{ecrId:self.val[0]};
					}
					return $.ajax({
						url: url,
						data:'html',
						type:'GET',
						data: dataKey,
						cache:false,
						beforeSend: function(pageNo){
							self.commentClear(pageNo);
						},
						success: function(data){
							self.loadComment(data);
						}
					});
				},
				loadComment: function(data){
					var self	=	this;
					(self.sidebar !== 'album')?self.commentParent.html(data):self.commentParent.html(data);
					var loadMoreTime	=	$('.loadMoreTime');
					self.RecentpageNo	=	loadMoreTime.attr('data-pageno');
					self.delteComment();
				},
				sucessCallComment: function(){
					var self	=	this;
					$('.ajaxloader').hide();
					self.commentTextarea.val('');
				},
				beforeSend: function(){
					var self	=	this;
					self.masnoryUl.html('');
					self.partialClear();
				},
				commentClear: function(){
				},
				onSucess: function(){
					var self	=	this;
					self.mainContainer.removeClass('loadImages');
				},
				validateFile: function(data){
					//need to check the function return and condition
					var self	=	this;
					var ExtensiontypeofFile;
					if(!data.imageHeight === typeof 'undefined'||data.imageHeight !== null || data.imageHeight !== '') {
						ExtensiontypeofFile	=	(/\.(gif|jpg|jpeg|tiff|png)$/i).test(data.imageUrl)?'image':'unknown';
					} else if(data.videoId !== typeof 'undefined'||data.videoId !== null || data.videoId !== ''){
						ExtensiontypeofFile	= (/\.(mp4|m4v|f4v|mov|flv|webm)$/i).test(data.videoId)?'video':'unknown';
					}
					return ExtensiontypeofFile;
				},
				settingBuildFrag: function(ExtensiontypeofFile,data){
					var self	= this;
					var sidebarHeight	=	parseInt(self.sideBar.height()-180);
					self.displayImg.add(self.VideoPlatform).hide();
					(self.typeofFile === 'image')?self.displayImg.show():self.VideoPlatform.show();
					(self.typeofFile === 'image')?self.zoomIn.add(self.zoomOut).add(self.resets).show():self.zoomIn.add(self.zoomOut).add(self.resets).add(self.makeCoverBtn).hide();
					self.appendComment.css({height:sidebarHeight})
				},
				display: function(data, fileType){
					var self	=	this;
					self.imagePlatform.removeClass('bg_img_stand_img');
					if(fileType === 'image') {
						self.photo(data);
					} else if(fileType === 'video'){
						self.imagePlatform.show();
						self.mainContainer.addClass('loadImages');
						self.video(data);
					}
					if(fileType == 'image'){
						if(data.userMe	===	data.userto){
							self.makeCoverBtn.show();
							self.workedBtn.hide();
						}else{
							self.makeCoverBtn.hide();
							self.workedBtn.show();
						}
					}else{
						self.workedBtn.hide();
					}
				},
				resetScrollTop: function(){
					var self	=	this;
					self.appendComment.css({scrollTop: 0});
				},
				photo: function(data){
					var self	=	this;
					self.displayImg.attr('src', data.imageUrl).load(function(){
							self.imagePlatform.show();
						}).
						on('error', function(){
							self.displayImg.attr('src', 'images/no_img.jpg');
						});
						self.imagePlatform.addClass('bg_img_stand_img');
						self.imageWrapper	=	$('.bg_img_stand_img');
						self.newMessurments	=	self.setImage(data);
						self.imageWrapper.css({width:self.newMessurments[0],height:self.newMessurments[1], top:self.newMessurments[2],left:self.newMessurments[3]});
						self.objMesuarments = {
							imageWidth	:	self.newMessurments[0],
							imageHeight	:	self.newMessurments[1],
							contWidth	:	self.mainContainer.width(),
							contHeight	:	self.mainContainer.height(),
						}
				},
				video: function(data){
					var self	=	this;
					self.imagePlatform.css({width:'100%',height:'89%',top:40,left:0,right:0,bottom:50});
					jwplayer("vidSection").setup({
							flashplayer: "player.swf",
							image: data.imageUrl,
							file: data.videourl,
							skin: self.settings.skin.video,
							width: "100%",
							height: "100%",
							autostart: true,
							stretching:"exactfit",
						});
				},
				supply: function(data){
					var self	=	this;
					self.userImage.attr('src', data.userImage);
					self.myImageThumb.attr('src', data.myimage);
					if(data.imgDescrSmall){
						self.descSection.show();
						self.descPopup.html(data.imgDescrSmall);
					$(".readMore").on('click', function(){
						self.descPopup.html(data.imgDescrFull);
					});
					}else{
						self.descPopup.html('');
						self.descSection.hide();
					}
					var elementArray	=	[self.likeCount, self.shareCount, self.commentCount,self.userName,self.timeUpdated];
					var valueArray		=	[data.likeCount, data.shareCount, data.commentCount,data.userName,data.imageCreatedOn];
						for(var i=0; i<elementArray.length && valueArray.length; i++){
							elementArray[i].html(valueArray[i]);
						}
						(data.imageNextId == '' || data.imageNextId	==	null)?self.next.hide():self.next.show();
						(data.imagePrevId == '' || data.imagePrevId	==	null)?self.prev.hide():self.prev.show();
						if(self.settings.sidebar !== 'photo'){
							if(self.typeofFile == 'image'){
								self.next.attr({'data-contentId':data.imageNextId, 'data-contentType':data.imageNextType, 'data-contentAlbum':data.imageAlbumId});
								self.prev.attr({'data-contentId':data.imagePrevId, 'data-contentType':data.imagePrevType, 'data-contentAlbum':data.imageAlbumId});
							}else if(self.typeofFile == 'video'){
								self.next.attr({'data-vidEncrId':data.imageNextId});
								self.prev.attr({'data-vidEncrId':data.imagePrevId});
							}
						}else if (self.settings.sidebar === 'photo'){
								self.next.attr({'data-contentId': data.imageNextId, 'data-contentType': data.imageNextType, 'data-typeOf': self.val[2]});
								self.prev.attr({'data-contentId': data.imagePrevId, 'data-contentType': data.imagePrevType, 'data-typeOf': self.val[2]});
						}
						(self.typeofFile == 'image')?self.inputObj.id.attr('name', 'id'):self.inputObj.id.attr('name', 'video_id');
						(self.typeofFile == 'image')?self.commentTextarea.attr('name', 'comment_descr'):self.commentTextarea.attr('name', 'comment_descr_input');
				},
				loadMasnory: function(data, initValue, count){
					var self	=	this;
					if(data.relatedImages.length > count){
						var range	=	data.relatedImages.length - count;
					}else{
						var count	=	data.relatedImages.length;
					}
					if((data.relatedImages.length -1) < count){
						count	=	data.relatedImages.length;
						$('.loadMorePicz').remove();
					}
					for(var i=parseInt(initValue); i<parseInt(count); i++){
						self.masnoryUl.append('<li><a href="javascript:;" class="popInside" data-contentid="'+data.relatedImages[i].contentId+'" data-contenttype="'+data.relatedImages[i].contentType+'" data-typeof="'+data.relatedImages[i].type+'"><img src="'+data.relatedImages[i].relatedImgUrl+'"></a></li>');
					}
						if(!$('.loadMorePicz').length && range !=0 && range > 0){
							$('<a class="loadMorePicz" href="javascript:;" data-limit="'+count+'">Load More Images</a>').insertAfter(self.masnoryUl);
						}else{
							$('.loadMorePicz').attr({'data-limit': count})
						}
				},
				triggerCmnt: function(data){
					var self	=	this;
					self.inputObj.userto.val(data.userto);
					self.inputObj.id.val(self.val[0]);
					var actionUrl	=	(self.typeofFile == 'image')?self.settings.photos.comments.commentActionUrl:self.settings.videos.comments.commentActionUrl;
					self.formActnInject.attr('action', actionUrl);
					if(self.settings.sidebar == 'default'){
						(self.typeofFile == 'image')?self.inputObj.type.val(self.val[1]):self.inputObj.type.val('');
					}else if(self.settings.sidebar == 'photo'){
						self.inputObj.type.val(self.val[1]);
					}
				},
				like: function(data, val){
					var self	=	this;
					(data.youLike)?self.likeBtn.addClass('liked'):self.likeBtn.removeClass('liked');
					(self.typeofFile == 'image')?self.likeBtn.attr({'data-like': val[0], 'data-likecat':val[1], 'data-userto':data.userto}):
					self.likeBtn.attr({'data-like': data.videoId, 'data-userto':data.userto});
				},
				likeTrigger: function(postValues){
					var self	=	this;
					var url	=	(self.typeofFile == 'image')?self.settings.photos.likeUrl:self.settings.videos.likeUrl;
					self.apply(url, postValues).done(function(data){
						self.counts(data, 'liked', self.likeCount);
						(data !== 'liked')?self.likeBtn.removeClass('liked'):self.likeBtn.addClass('liked');
					});
				},
				share:	function(data, val){
					var self	=	this;
					(data.youShare)?self.shareBtn.addClass('shared'):self.shareBtn.removeClass('shared');
					(self.typeofFile == 'image')?self.shareBtn.attr({'data-share':val[0],'data-sharecat':val[1],'data-userto':data.userto}):
					self.shareBtn.attr({'data-share':data.videoId,'data-userto':data.userto});
				},
				shareTrigger: function(postValues){
					var self	=	this;
					var url	=	(self.typeofFile == 'image')?self.settings.photos.shareUrl:self.settings.videos.shareUrl;
					self.apply(url,postValues).done(function(data){
						self.counts(data, 'shared', self.shareCount);
						(data !== 'shared')?self.shareBtn.removeClass('shared'):self.shareBtn.addClass('shared');
					});
				},
				workedTogether:	function(data, val){
					var self	=	this;
					var targetedElement	=	self.workedBtn.children('i');
					switch(data.workedTogether) {
						case 1:
							targetedElement.removeClass('fa-check').removeClass('fa-star').addClass('fa-users');
						break;
						case 2:
							targetedElement.removeClass('fa-check').removeClass('fa-users').addClass('fa-star-o');
						break;
						case 3:
							targetedElement.removeClass('fa-users').removeClass('fa-star-o').addClass('fa-check');
						break;
					}
					self.workedBtn.attr({'data-like': val[0], 'data-likecat':val[1], 'data-userto':data.userto});
				},
				workedTogetherTrigger: function(postValues, _this){
					var self	=	this;
					var url	=	self.settings.photos.workedUrl;
					if(self.globalElement.workedTogether !==3 || self.globalElement.workedTogether !== 2){
						_this.addClass('active');
						self.apply(url,postValues).done(function(data){
							self.workedBtn.removeClass('active');
							var targetElement	=	self.workedBtn.children('i');
							if(data === 'workedTogether'){
								targetElement.removeClass('fa-users').removeClass('fa-check').addClass('fa-star-o');
							}
						});
					}	
				},
				apply: function(url, postValues){
					var self	=	this;
					var serialisedVal	=	$.param(postValues);
					//alert(serialisedVal);
					return $.ajax({
						url: url,
						type:'POST',
						data:serialisedVal,
						cache:false,
					});
				},
				counts: function(data, conditionalValue, targetElement){
					var self	=	this;
					var tempCount	=	targetElement.html();
					var newCount	=	(data == conditionalValue)?parseInt(tempCount)+1:parseInt(tempCount)-1;
					targetElement.html(parseInt(newCount));
				},
				setImage: function(data){
					var self	=	this;
					var imageWidth,imageHeight,resizedWidth,resizedHeight,adjTop,adjLeft;
					var imageOverlayWidth	=	self.mainContainer.width();
					var imageOverlayHeight	=	self.mainContainer.height();
					var mesurments	=	new Array();
					var ratioWidth	=	imageOverlayWidth/data.imageWidth;
					var ratioHeight	=	imageOverlayHeight/data.imageHeight;
					
					if(data.imageWidth > imageOverlayWidth || data.imageHeight >  imageOverlayHeight){
						if(data.imageWidth > imageOverlayWidth ){
							resizedWidth	=	imageOverlayWidth;
							resizedHeight	=	data.imageHeight*ratioWidth;
						}
						if(data.imageHeight >  imageOverlayHeight){
							resizedWidth	=	data.imageWidth*ratioHeight;
							resizedHeight	=	imageOverlayHeight;
						}
					} else{
						resizedWidth	=	data.imageWidth;
						resizedHeight	=	data.imageHeight;
					}
					adjTop	=	parseInt((imageOverlayHeight-resizedHeight)/2);
					adjLeft	=	parseInt((imageOverlayWidth-resizedWidth)/2)
					mesurments.push(resizedWidth,resizedHeight,adjTop,adjLeft);
					
					return mesurments;
				},
				zoomInside: function(){
					var self	=	this;
					self.imagePlatform.addClass('trnasitionClass');
					self.newObjMesuarment = {
						imageWidth	:	self.imagePlatform.width(),
						imageHeight : 	self.imagePlatform.height(),
					}
					var scaledWidth	=	Math.round(self.newObjMesuarment.imageWidth * self.zoomExtends.scale);
					var scaleHeight	=	Math.round(self.newObjMesuarment.imageHeight * self.zoomExtends.scale);
					if(scaledWidth > self.objMesuarments.imageWidth * self.zoomExtends.pause) {
						scaledWidth	=	self.objMesuarments.imageWidth * self.zoomExtends.pause;
						scaleHeight	=	self.objMesuarments.imageHeight * self.zoomExtends.pause;
					}
					var tops	=	(self.objMesuarments.contHeight-scaleHeight)/2;
					var lefts	=	(self.objMesuarments.contWidth-scaledWidth)/2;
					self.imagePlatform.css({width:scaledWidth,height:scaleHeight,top:tops,left:lefts,right:lefts,bottom:tops},{duration:500});
					setTimeout(function(){
						self.offsetTop	=	self.imagePlatform.position().top;
						self.offsetLeft	=	self.imagePlatform.position().left;
						
						if(self.offsetTop < 0 || self.offsetLeft < 0) {
							self.drag();
						}
					}, 500);
				},
				zoomOutside: function(){
					var self	=	this;
					self.imagePlatform.addClass('trnasitionClass');
						self.newObjMesuarment = {
							imageWidth	:	self.imagePlatform.width(),
							imageHeight : 	self.imagePlatform.height(),
						}
						var scaledWidth	=	Math.round(self.newObjMesuarment.imageWidth / self.zoomExtends.scale);
						var scaleHeight	=	Math.round(self.newObjMesuarment.imageHeight / self.zoomExtends.scale);
						if(scaledWidth < self.objMesuarments.imageWidth) {
							scaledWidth	=	self.objMesuarments.imageWidth;
							scaleHeight	=	self.objMesuarments.imageHeight;
						}
						var tops	=	(self.objMesuarments.contHeight-scaleHeight)/2;
						var lefts	=	(self.objMesuarments.contWidth-scaledWidth)/2;
						self.imagePlatform.css({width:scaledWidth,height:scaleHeight,top:tops,left:lefts,},{duration:500});
						
						setTimeout(function(){
							self.offsetTop	=	self.imagePlatform.position().top;
							self.offsetLeft	=	self.imagePlatform.position().left;
							
							if(self.offsetTop >= 0 && self.offsetLeft >= 0 ) {
								self.imagePlatform.removeClass('dragable').removeClass('trnasitionClass');
							}
						}, 500);
				},
				resetZoom: function(){
					var self	=	this;
					self.imagePlatform.addClass('trnasitionClass');
						var tops	=	(self.objMesuarments.contHeight- self.objMesuarments.imageHeight)/2;
						var lefts	=	(self.objMesuarments.contWidth- self.objMesuarments.imageWidth)/2;
						self.imagePlatform.css({width:self.objMesuarments.imageWidth,height:self.objMesuarments.imageHeight,top:tops,left:lefts,},500);
				},
				drag: function(){
					var self	=	this;
					var draged;
					self.imagePlatform.addClass('dragable');
					self.dragable	=	$('.dragable');
					self.dragable.removeClass('trnasitionClass');
					
					self.dragable.on('mousedown', function(e){
						e.preventDefault();
						var _this	=	$(this);
						_this.addClass('draged').removeClass('trnasitionClass');
						draged	=	$('.draged')
						self.posY	=	_this.offset().top + _this.outerHeight() - e.pageY;
						self.posX	=	_this.offset().left + _this.outerWidth() - e.pageX;
					});
					self.dragable.on('mousemove', function(e){
							e.preventDefault();
							self.x	=	e.pageX + self.posX - $('.draged').outerWidth();
							self.y	=	e.pageY + self.posY - $('.draged').outerHeight();
							
						self.offsetTop		=	self.imagePlatform.position().top;
						self.offsetLeft		=	self.imagePlatform.position().left;
						self.offsetBottom	=	self.offsetTop + self.imagePlatform.outerHeight(true);
						self.offsetRight	=	self.offsetLeft + self.imagePlatform.outerWidth(true);
						
						if(self.offsetLeft <= 0 && self.offsetTop <= 0) {
							$('.draged').offset({top:self.y,left:self.x});
							
							self.offsetTop		=	self.imagePlatform.position().top;
							self.offsetLeft		=	self.imagePlatform.position().left;
							self.offsetBottom	=	self.offsetTop + self.imagePlatform.outerHeight(true);
							self.offsetRight	=	self.offsetLeft + self.imagePlatform.outerWidth(true);
							
							if(self.offsetTop > 0 || self.offsetLeft > 0 || self.offsetBottom < self.objMesuarments.contHeight || self.offsetRight < self.objMesuarments.contWidth){
								if(self.offsetTop > 0){
									self.dragable.css({top:0,bottom:'auto'});
								}
								if(self.offsetLeft > 0){
									self.dragable.css({left:0, right:'auto'});
								}
								if(self.offsetBottom < self.objMesuarments.contHeight){
									self.dragable.css({bottom:0, top: 'auto'});
								}
								if(self.offsetRight < self.objMesuarments.contWidth){
									self.dragable.css({right:0, left:'auto'});
								}
							}
						}else if(self.offsetLeft <= 0){
							$('.draged').offset({left:self.x});
							
							self.offsetLeft		=	self.imagePlatform.position().left;
							self.offsetRight	=	self.offsetLeft + self.imagePlatform.outerWidth(true);
							
							if(self.offsetLeft > 0){
								self.dragable.css({left:0, right: 'auto'});
							}
							if(self.offsetRight < self.objMesuarments.contWidth){
								self.dragable.css({right:0, left:'auto'});
							}
						}else if(self.offsetTop <=0){
							$('.draged').offset({top:self.y});
							
							self.offsetTop		=	self.imagePlatform.position().top;
							self.offsetBottom	=	self.offsetTop + self.imagePlatform.outerHeight(true);
							
							if(self.offsetTop > 0){
								self.dragable.css({top:0,bottom:'auto'});
							}
							if(self.offsetBottom < self.objMesuarments.contHeight){
								self.dragable.css({bottom:0, top: 'auto'});
							}
						}
					});
					$(window).on('mouseup', function(){
						self.dragable.removeClass('draged');
					});
				},
				partialClear: function(){
					var self	=	this;
					self.mainContainer.addClass('loadImages');
					self.displayImg.removeAttr('src');
					self.imagePlatform.removeClass('trnasitionClass').removeClass('dragable');
					self.imagePlatform.hide();
					self.masnoryUl.html('');
					self.clearHtml([self.masnoryUl,self.commentParent]);
				},
				clear:	function(){
					var self	=	this;
					self.partialClear();
					self.imagePlatform.removeClass('bg_img_stand_img');
					
					if(self.typeofFile == 'video'){
						jwplayer( 'vidSection' ).stop();
					}
					self.removeDataAttributes(self.next);
					self.removeDataAttributes(self.prev);
					unbindClick([self.likeBtn,self.shareBtn,self.zoomIn,self.zoomOut,self.resets,self.workedBtn]);
					self.clearHtml([self.masnoryUl]);
					self.commentTextarea.off('keyup');
					self.appendComment.off('click');
					self.masnoryUl.html(' ').off('click');
					self.commentParent.off('click');
					
					function unbindClick(elem){
						for(var i=0; i<elem.length; i++){
							elem[i].unbind('click');
						}
					}
				},
				clearHtml: function(element){
					var self	=	this;
					for(var i =0; i<element.length; i++){
						element[i].html('');
					}
				},
				removeDataAttributes: function(target){
					var i,
						$target = $(target),
						attrName,
						dataAttrsToDelete = [],
						dataAttrs = $target.get(0).attributes,
						dataAttrsLen = dataAttrs.length;
				
					for (i=0; i<dataAttrsLen; i++) {
						if ( 'data-' === dataAttrs[i].name.substring(0,5) ) {
							dataAttrsToDelete.push(dataAttrs[i].name);
						}
					}
					$.each( dataAttrsToDelete, function( index, attrName ) {
						$target.removeAttr( attrName );
					});
					
				},
				validateFileType:	function(elem){
					var self	=	this;
					var contentId	=	elem.attr('data-contentId');
					var vidEncrId	=	elem.attr('data-vidEncrId');
					if(typeof contentId !== typeof undefined){
						self.fileTypedetected	=	'image';
					}else if(typeof vidEncrId !== typeof undefined){
						self.fileTypedetected	=	'video';
					}
					return self.fileTypedetected;
				},
				fetchDataAttr: function(data, element){
					var self	=	this;
					var dataAttr;
					if($.isArray(data)){
						dataAttr	=	data;
					}else{
						dataAttr = (data == 'image')?['data-contentId','data-contentType','data-contentAlbum']:['data-vidEncrId'];
					}
					var attributeArray	=	new Array();
					for(var i=0; i<dataAttr.length; i++) {
						var dataVal	=	element.attr(dataAttr[i]);
						if(dataVal !== null || dataVal != '') {
							attributeArray.push(dataVal);
						} else{
							 throw new Error('Wrong or empty data-attribute provided, Please recheck the attributes :-( ');
						}
					}
					if(attributeArray.length !== 0) {
						return attributeArray;
					}
				},
				delteComment: function(element){
					var self = this;
					$('.deletComment').on('click', 'a',function(){
						var deleteId	=	$(this).attr('data-delid');
						var elem = $(this).parents('.comment');
						$.confirm({
							'title'	  : 'Delete Confirmation',
							'message'	: 'You are about to delete this comment ?',
							'buttons'	: {
							'Yes'	: {
							'class'	: 'blue',
							'action': function(){
								$('.ajaxloader').show();
								var delid       =	deleteId;
								if(delid){
									$.ajax({
										url : self.settings.photos.comments.commentDelete,
										method:"POST",
										data:{delid:delid},
										success:function(){
											elem.remove();
											var recentCommentCount = self.commentCount.html();
											self.commentCount.html(parseInt(recentCommentCount)-1);
											$('.ajaxloader').hide();
										}});
								}
							}
							},
							'No'	: {
							'class'	: 'gray',
							'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
							}
							}
						});
					});
				},
				close: function(){
					var self	=	this;
					self.grandParent.hide();
					self.clear();
					self.next.add(self.prev).unbind('click');
					self.dismiss.unbind('click');
					//$(self.element).unbind('click');
				}
		});
		
		
		// A really lightweight plugin wrapper around the constructor,
		// preventing against multiple instantiations
		$.fn[ pluginName ] = function ( options ) {
					return new Plugin(this, options);
				//return this.each(function() {
//						if ( !$.data( this, "plugin_" + pluginName ) ) {
//								$.data( this, "plugin_" + pluginName, new Plugin( this, options ) );
//						}
//				});
		};

})( jQuery, window, document );
