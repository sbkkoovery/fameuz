<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
$user_url		=	$objCommon->esc($_GET['user_url']);
if($user_url==$getUserDetails['usl_fameuz'])
{
	header("location:".SITE_ROOT."user/my-album");
	exit;
}else{
$getMyFriendDetails			=	$objUsers->getRowSql("SELECT user.user_id,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz
															FROM users AS user 
															LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
															WHERE user.status=1 AND user.email_validation=1  AND social.usl_fameuz='".$user_url."'");				
}
?>
<div class="inner_content_section">
	<div class="container">
		<div class="row">
				<div class="inner_top_border">
					<div class="user_profile">
					<?php echo $objCommon->checkEmailverification();?>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
						<div class="content">
							<div class="pagination_box"><a href="#">Home <i class="fa fa-caret-right"></i></a><a href="<?php echo SITE_ROOT.$getMyFriendDetails['usl_fameuz']?>"> <?php echo $objCommon->displayName($getMyFriendDetails);?>  </a><i class="fa fa-caret-right"></i> <a href="#" class="active">Friends</a>
                                <?php
								include_once(DIR_ROOT."widget/notification_head.php");
								?>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="whiteBg left100">
										<div class="visitor_head_box left70"><h5><?php echo $objCommon->displayName($getMyFriendDetails);?>'s Friends</h5></div>
										<div class="search left28"><div class="search_box"><input type="text" placeholder="Search" id="follower_search" name="follower_search"></div></div>
									</div>
								</div>
							</div>
							<div class="visitor_list_box">
								<ul class="row"></ul>
							</div>
						</div>
                        </div>
						<?php
						include_once(DIR_ROOT."widget/right_static_ad_bar.php");
						?>
					</div>
				</div>
			</div>
	</div>
</div>
<script>
$(document).ready(function(){
	getresult('<?php echo SITE_ROOT?>ajax/user_follower_list.php');
	 function getresult(url) {
		$.ajax({
			url: url,
			type: "GET",
			data:  {rowcount:$("#rowcount").val(),friendId:'<?php echo $getMyFriendDetails['user_id']?>'},
			beforeSend: function(){
			$('#loader-icon').show();
			},
			complete: function(){
			$('#loader-icon').hide();
			},
			success: function(data){
				$(".visitor_list_box ul").append(data);
				if($("#total-count").val()>0){
				}else{
					$(".visitor_list_box ul").html('<li class="col-md-6"><p>No followers found....</p></li>');
				}
			},
			error: function(){} 	        
	   });
	}
	$(window).scroll(function(){
		if ($(window).scrollTop() == $(document).height() - $(window).height()){
			if($(".pagenum:last").val() <= $(".total-page").val()) {
				var pagenum = parseInt($(".pagenum:last").val()) + 1;
				getresult('<?php echo SITE_ROOT?>ajax/user_follower_list.php?page='+pagenum);
			}
		}
	}); 
	$('#follower_search').keyup(function(){
		var keyVal		=	$(this).val();
			getSearchresult('<?php echo SITE_ROOT?>ajax/user_follower_list.php?search='+keyVal+'&page=1');
	});
	function getSearchresult(url) {
		$.ajax({
			url: url,
			type: "GET",
			data:  {rowcount:$("#rowcount").val(),friendId:'<?php echo $getMyFriendDetails['user_id']?>'},
			beforeSend: function(){
			$('#loader-icon').show();
			},
			complete: function(){
			$('#loader-icon').hide();
			},
			success: function(data){
				if(data!=""){
					$(".visitor_list_box ul").html(data);
				}else{
					$(".visitor_list_box ul").html('<li class="col-md-6"><p>No followers found....</p></li>');
				}
			},
			error: function(){} 	        
	   });
	}

});
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  