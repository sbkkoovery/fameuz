<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/search_functions.php");
$objSearchFun						=	new search_functions();
$searchBlog						  =	$objCommon->esc($_GET['search']);
?><?php /*?>
<link href="<?php echo SITE_ROOT?>css/fixed_portions.css" rel="stylesheet" type="text/css"><?php */?>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.masonry.js"></script>
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
  	<div class="border-top-s"></div>
    <div class="inner_top_border">
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
        <div class="jobs-head">
          <?php include_once(DIR_ROOT."widget/blog_search_widget.php");?> 
         </div>
<?php
if($searchBlog){
	$filterSearchArr			=	$objSearchFun->filterSearchKeys($searchBlog);
	if(count($filterSearchArr)>0){
	$scoreFullTitle 			 = 	6;
	$scoreTitleKeyword 		  = 	5;
	$titleSQL				   =	array();
	$aliasSQL				   =	array();
	foreach($filterSearchArr as $searchArr){
			$titleSQL[] = "if (blog_title LIKE '%".$searchArr."%',{$scoreFullTitle},0)";
			$aliasSQL[] = "if (blog_alias LIKE '%".$searchArr."%',{$scoreTitleKeyword},0)";
			$getBlogsSql		=	"SELECT b.*,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name,group_concat(l.user_id) AS likeUser,count(comment_id) AS commentCount,
				(
					(
					".implode(" + ", $titleSQL)."
					)+
					(
					".implode(" + ", $aliasSQL)."
					)
				) as relevance
										FROM blogs AS b  
										LEFT JOIN users AS user ON b.user_id = user.user_id
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN personal_details AS personal ON user.user_id=personal.user_id
										LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
										LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN blogs_like AS l ON b.blog_id = l.blog_id AND l.bl_status = 1
										LEFT JOIN  comments ON b.blog_id=comments.comment_content AND comments.comment_cat=7
										WHERE user.status=1 AND user.email_validation=1  AND b.blog_status=1 GROUP BY b.blog_id   HAVING relevance > 0 ORDER BY relevance DESC, b.created_time desc LIMIT 0,20";
	}
	}else{
		$getBlogsSql					=	"SELECT b.*,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name,group_concat(l.user_id) AS likeUser,count(comment_id) AS commentCount
										FROM blogs AS b  
										LEFT JOIN users AS user ON b.user_id = user.user_id
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN personal_details AS personal ON user.user_id=personal.user_id
										LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
										LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN blogs_like AS l ON b.blog_id = l.blog_id AND l.bl_status = 1
										LEFT JOIN  comments ON b.blog_id=comments.comment_content AND comments.comment_cat=7
										WHERE user.status=1 AND user.email_validation=1  AND b.blog_status=1 AND b.blog_title LIKE '%".$searchBlog."%' GROUP BY b.blog_id  ORDER BY b.created_time desc LIMIT 0,20";
	}
}else{
$getBlogsSql					=	"SELECT b.*,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name,group_concat(l.user_id) AS likeUser,count(comment_id) AS commentCount
										FROM blogs AS b  
										LEFT JOIN users AS user ON b.user_id = user.user_id
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN personal_details AS personal ON user.user_id=personal.user_id
										LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
										LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN blogs_like AS l ON b.blog_id = l.blog_id AND l.bl_status = 1
										LEFT JOIN  comments ON b.blog_id=comments.comment_content AND comments.comment_cat=7
										WHERE user.status=1 AND user.email_validation=1  AND b.blog_status=1 GROUP BY b.blog_id  ORDER BY b.created_time desc LIMIT 0,20";
}
$getBlogs						=	$objUsers->listQuery($getBlogsSql);
if(count($getBlogs)>0){
	$likeUser					=	$getBlogs[0]['likeUser'];
	$likeUserArr				 =	'';
	$youLike			 		 =	'0';
	if($likeUser){
		$likeUserArr			 =	explode(",",$likeUser);
		$likeUserArr			 =	array_filter($likeUserArr);
		$likeCounts			  =	count($likeUserArr).' Likes';
		if(in_array($_SESSION['userId'],$likeUserArr)){
			$youLike			 =	'1';
		}else{
			$youLike			 =	'0';
		}
	}else{
		$likeCounts			  =	 '0 Likes';
	}
?>
            <div class="blog">
            	<div class="blog-sec">
                	
					<?php
					if($getBlogs[0]['blog_img']){
							list($widthu, $heighut, $typeu, $attru) = getimagesize(SITE_ROOT_AWS_IMAGES.'uploads/blogs/'.$getBlogs[0]['blog_img']); 
							if($widthu >800){
					?>
                    	<div class="blog-img">
                            <img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/blogs/<?php echo $objCommon->html2text($getBlogs[0]['blog_img'])?>" class="img-responsive center-block" />
                        </div>
					<?php
							}
					}
					?>
                  
                    <div class="blog-info">
                    	<div class="user-info-blog">
                        	<div class="row">
                            	<div class="col-sm-2 alt-width-user-img">
                                	<div class="img-user-blog">
                                    	<a href="<?php echo SITE_ROOT.$objCommon->html2text($getBlogs[0]['usl_fameuz'])?>"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($getBlogs[0]['upi_img_url'])?$getBlogs[0]['upi_img_url']:'profile_pic.jpg'?>" /></a>
                                    </div>
                                   </div>
                                <div class="col-sm-10 alt-padding-l">
                                    <div class="blog-user-desc">
                                    	<p><a href="<?php echo SITE_ROOT.$objCommon->html2text($getBlogs[0]['usl_fameuz'])?>"><?php echo $objCommon->displayName($getBlogs[0])?></a></p>
                                    </div>
                                    <div class="place-job">
                                    	<p>
                                            <span class="proffesion"></span>
                                            <span><?php echo $objCommon->html2text($getBlogs[0]['c_name'])?></span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                            	<div class="col-sm-12">
                                	<div class="blog-head">
                                    	<p><?php echo $objCommon->html2text($getBlogs[0]['blog_title'])?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="blog-info-more">
                                <p><span><?php echo date("F d, Y",strtotime($getBlogs[0]['created_time']))?></span><span> <?php echo $objCommon->html2text($getBlogs[0]['commentCount'])?> Comments </span><span class="likecount<?php echo $getBlogs[0]['blog_id']?>"> <?php echo $likeCounts?> </span></p>
                            </div>
                            <div class="blog-desc">
                            	<p><?php echo strip_tags($objCommon->limitWords($getBlogs[0]['blog_descr'],200))?></p>
                                <a href="<?php echo SITE_ROOT.'blog/show/'.$objCommon->html2text($getBlogs[0]['blog_alias']),'-'.$getBlogs[0]['blog_id']?>" class="read-more-blog"><i>Continue Reading...</i></a>
                            </div>
                            <div class="like-comment-share-blog">
                            	<div class="row">
                                	<div class="col-sm-2 alt-width-like-blog">
                                    	<div class="like-blog">
                                        	<a href="javascript:;" data-like="<?php echo $getBlogs[0]['blog_id']?>" class="like <?php echo ($youLike==1)?'liked-blog':''?>"><i class="fa fa-heart"></i><span><?php echo ($youLike==1)?'Liked':'Like'?></span></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-lg-6">
                                    <div class="like-blog">
										<a href="javascript:;"  data-share="<?php echo $getBlogs[0]['blog_id']?>" data-blogby="<?php echo $getBlogs[0]['user_id']?>" class="shareBlog"><i class="fa fa-share-alt"></i>Share</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-4 pull-right">
                                    	<!--<div class="share-btns pull-right">
                                        	<a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                            <a href="#"><i class="fa fa-instagram"></i></a>
                                            <a href="#" class="email-blog pull-right"><i class="fa fa-envelope"></i><span>Email</span></a>
                                        </div>--><!-- social share commeted -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
<?php
}else{
	echo '<div class="blog"><div class="blog-info"><p>No blogs found....</p></div>';
}
if(count($getBlogs)>1){
?>
                <div class=" row masnory">
                <?php for($i=1; $i < count($getBlogs); $i++){
					$likeUser					=	$getBlogs[$i]['likeUser'];
					$likeUserArr				 =	'';
					$youLike			 		 =	'0';
					if($likeUser){
						$likeUserArr			 =	explode(",",$likeUser);
						$likeUserArr			 =	array_filter($likeUserArr);
						$likeCounts			  =	count($likeUserArr).' Likes';
						if(in_array($_SESSION['userId'],$likeUserArr)){
							$youLike			 =	'1';
						}else{
							$youLike			 =	'0';
						}
					}else{
						$likeCounts			  =	 '0 Likes';
					}
					?>
                	<div class="col-sm-12 col-md-12 col-lg-6 item">
					<?php
					if($getBlogs[$i]['blog_img']){
					?>
                    	<div class="img-blog-mas">
                            <img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/blogs/<?php echo $objCommon->html2text($getBlogs[$i]['blog_img'])?>" class="img-responsive" />
                        </div>
					<?php
					}
					?>
                        <div class="blog-info">
                    	<div class="user-info-blog">
                        	<div class="row">
                            	<div class="col-sm-2 alt-width-user-img">
                                	<div class="img-user-blog">
                                    	<a href="<?php echo SITE_ROOT.$objCommon->html2text($getBlogs[$i]['usl_fameuz'])?>"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($getBlogs[$i]['upi_img_url'])?$getBlogs[$i]['upi_img_url']:'profile_pic.jpg'?>" /></a>
                                    </div>
                                   </div>
                                <div class="col-sm-10 alt-padding-l">
                                    <div class="blog-user-desc">
                                    	<p><a href="<?php echo SITE_ROOT.$objCommon->html2text($getBlogs[$i]['usl_fameuz'])?>"><?php echo $objCommon->displayName($getBlogs[$i])?></a></p>
                                    </div>
                                    <div class="place-job">
                                    	<p>
                                            <span class="proffesion"></span>
                                            <span><?php echo $objCommon->html2text($getBlogs[$i]['c_name'])?></span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            	<div class="col-sm-12">
                                	<div class="blog-head">
                                    	<p><?php echo $objCommon->html2text($getBlogs[$i]['blog_title'])?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="blog-info-more">
                                <p><span><?php echo date("F d, Y",strtotime($getBlogs[$i]['created_time']))?></span><span> <?php echo $objCommon->html2text($getBlogs[$i]['commentCount'])?> Comments </span><span class="likecount<?php echo $getBlogs[$i]['blog_id']?>"> <?php echo $likeCounts?></span></p>
                            </div>
                            <div class="blog-desc">
                            	<p><?php echo strip_tags($objCommon->limitWords($getBlogs[$i]['blog_descr'],200))?></p>
                                <a href="<?php echo SITE_ROOT.'blog/show/'.$objCommon->html2text($getBlogs[$i]['blog_alias']),'-'.$getBlogs[$i]['blog_id']?>" class="read-more-blog"><i>Continue Reading...</i></a>
                            </div>
                            <div class="like-comment-share-blog">
                            	<div class="row">
                                	<div class="col-sm-2 alt-width-like-blog">
                                    	<div class="like-blog">
                                        	<a href="javascript:;" data-like="<?php echo $getBlogs[$i]['blog_id']?>" class="like <?php echo ($youLike==1)?'liked-blog':''?>"><i class="fa fa-heart"></i><span><?php echo ($youLike==1)?'Liked':'Like'?></span></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-lg-6">
                                    <div class="like-blog">
                                        	<a href="javascript:;"  data-share="<?php echo $getBlogs[$i]['blog_id']?>" data-blogby="<?php echo $getBlogs[$i]['user_id']?>" class="shareBlog"><i class="fa fa-share-alt"></i>Share</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                    	<!--<div class="share-btns">
                                        	<a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                            <a href="#"><i class="fa fa-instagram"></i></a>
                                            <a href="#" class="email-blog pull-right"><i class="fa fa-envelope"></i><span>Email</span></a>
                                        </div>--><!-- social share commeted -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    
                   <?php }?> 
                </div>
<?php
}
?>
          </div>  
       </div>
       
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3 n_relative">
          <?php
		  include_once('widget/right_ads_sidebar.php');
		  ?>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(window).load(function(e) {
	$('.fixer').fixedSidebar();
    $('.masnory').masonry();
});
$(document).ready(function() {
	$('[data-toggle="tooltip"]').tooltip();	
});
$(".like-blog").on("click",".like",function(){
	var like		=	$(this).data("like");
	var that		=	this;
	if(like){
		$.ajax({
			url:"<?php echo SITE_ROOT?>ajax/like_blog.php",
			data:{like:like},
			type:"POST",
			success: function(dat1){
				$(that).parent().html(dat1);
				$(".likecount"+like).load('<?php echo SITE_ROOT?>ajax/like_blog_count.php?like='+like);
			}
		});
	}
});
$(".like-blog").on("click",".shareBlog",function(){
	var share		=	$(this).data("share");
		blogby	   =	$(this).data("blogby");
		that		 =	this;
	if(share){
		$.ajax({
			url:"<?php echo SITE_ROOT?>ajax/share_blog.php",
			data:{share:share,blogby:blogby},
			type:"POST",
			success: function(dat1){
				$(that).parent().html(dat1);
			}
		});
	}
});
</script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/fixed_sidebar.js"></script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
