<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/market_place_view.php");
$objmarketView						   =	new market_place_view();
$adId									=	$objCommon->esc($_GET['adId']);
if($adId){
	$adIdExpl							=	explode("-",$adId);
	$getAdId							 =	end($adIdExpl);
	$getAdAliasArr					   =	array_slice($adIdExpl, 0, -1);
	$getAdAlias						  =	implode("-",$getAdAliasArr);
	$getMarketPlace					  =	$objUsers->getRowSql("SELECT mp.*,mpc.mpc_name,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name
									FROM market_place AS mp 
									LEFT JOIN market_place_category AS mpc ON mp.mp_type =  mpc.mpc_id
									LEFT JOIN users AS user ON mp.user_id = user.user_id 
									LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
									LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
									LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
									LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
									WHERE mp.mp_status = 1 and user.status=1 AND user.email_validation=1 AND mp.mp_id=".$getAdId." AND mp.mp_alias='".$getAdAlias."' AND mp.mp_show_from <= CURDATE() AND  mp.mp_show_to >= CURDATE()  order by mp.mp_created_date desc");
	if($getMarketPlace['mp_id'] ==''){
		header("loaction:".SITE_ROOT.'market-place');
		exit;
	}
}else{
	header("loaction:".SITE_ROOT.'market-place');
	exit;
}
$countMarketPlaceView					=	$objmarketView->count("mp_id=".$getMarketPlace['mp_id']);
?>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.form.js"></script>
<link href="<?php echo SITE_ROOT?>css/pagination.css" rel="stylesheet" type="text/css"><?php /*?>
<link href="<?php echo SITE_ROOT?>css/fixed_portions.css" rel="stylesheet" type="text/css"><?php */?>
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
  <div class="border-top-s"></div>
  <div class="inner_top_border">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
      <div class="jobs-head">
        <div class="pagination_box_jobs pagination_box">
            <a href="<?php echo SITE_ROOT.'user/home'?>">Home <i class="fa fa-caret-right"></i></a>
            <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
            <a href="<?php echo SITE_ROOT?>market-place"class="active">Marketplace</a>
            <div class="post_jobs">
            <a href="<?php echo SITE_ROOT?>user/post-market-place" ><i class="fa fa-bullhorn"></i>&nbsp;Post Free Ads</a>
            </div>
            <?php
            if(isset($_SESSION['userId'])){
                include_once(DIR_ROOT."widget/notification_head.php");
            }
            ?>
        </div>
       </div>
       <div class="container_marketPlace_single">
       <div class="marketplace">
		   <div class="row">
		   		<div class="col-sm-12">
					<div class="job-head">
						<p><?php echo $objCommon->html2text($getMarketPlace['mp_title']);?></p>
					</div>
					 <?php
					if($getMarketPlace['mp_image']){
					?>
					<div class="marketplace-image-single">
						<img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/market_place/<?php echo $getMarketPlace['mp_image']?>" class="img-responsive" />
					 </div>
					<?php
					}
					?>
				</div>
		   </div>
		   <div class="row post-info_down">
		   		<div class="col-sm-12 col-md-12">
					<div class="row">
						<div class="col-sm-12">
							<div class="place-job make-inline">
								<p><i class="fa fa-file"></i> &nbsp;<?php echo $objCommon->html2text($getMarketPlace['mpc_name']);?> </p>
                                <p><span class="proffesion"></span> &nbsp;<span class="dark-me"><a href="<?php echo SITE_ROOT.$objCommon->html2text($getMarketPlace['usl_fameuz'])?>"><?php echo $objCommon->displayName($getMarketPlace);?></a></span> on : <?php echo date("d F , Y",strtotime($getMarketPlace['mp_created_date']));?></p>
                                <div class="post-info"> 
								<p class="dim-me"><i class="fa fa-globe"></i>&nbsp;<?php echo $objCommon->html2text($getMarketPlace['mp_locations']);?></p>
                                <?php
					if($getMarketPlace['mp_duration']){
					?>
                        <p>Duration : <?php echo $objCommon->html2text($getMarketPlace['mp_duration']);?> </p>
					<?php
					}
					?>
							</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-12">
					<?php
						$marketActualPrice							=	$objCommon->html2text($getMarketPlace['mp_actual_price']);
						$marketDiscountPrice						  =	$objCommon->html2text($getMarketPlace['mp_dis_price']);
						if($marketActualPrice !='' && $marketDiscountPrice !=''){
							$explActualPrice						  =	explode("###",$marketActualPrice);
							$newActualPrice						   =	$explActualPrice[0].' '.number_format($explActualPrice[1]);
							$explDiscountPrice						=	explode("###",$marketDiscountPrice);
							$newDiscountPrice						 =	$explDiscountPrice[0].' '.number_format($explDiscountPrice[1]);
							$priceStr								 =	'<a href="javascript:;" class="crossOriginal">'.$newActualPrice.'</a><a href="javacript:;" class="crossOriginalNone">'.$newDiscountPrice.'</a>';
						}else if ($marketActualPrice !=''){
							$explActualPrice						  =	explode("###",$marketActualPrice);
							$newActualPrice						   =	$explActualPrice[0].' '.number_format($explActualPrice[1]);
							$priceStr								 =	'<a href="javascript:;" class="crossOriginalNone">'.$newActualPrice.'</a>';
						}else if($marketDiscountPrice !=''){
							$explDiscountPrice						=	explode("###",$marketDiscountPrice);
							$newDiscountPrice						 =	$explDiscountPrice[0].' '.number_format($explDiscountPrice[1]);
							$priceStr								 =	'<a href="javascript:;" class="crossOriginalNone">'.$newDiscountPrice.'</a>';
						}
						?>
						<div class="market-price pull-left">
							<?php echo $priceStr;?>
                            <?php
						$getBookStatus		=	$objUsers->getRowSql("select mpb_id FROM market_place_booked WHERE mp_id = ".$getMarketPlace['mp_id']." AND user_id=".$_SESSION['userId']);
						//if($getBookStatus['mpb_id']=='' && $getMarketPlace['user_id']!= $_SESSION['userId']){
						?>
						<?php /*?><a href="javascript:;" class="enq_req has-spinner send_request"><span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span> &nbsp;&nbsp; Book it</a><?php */?>
						<?php
						//}
						if($getBookStatus['mpb_id']=='' && $getMarketPlace['user_id']!= $_SESSION['userId']){
						?>
                        <a href="javascript:;" data-toggle="modal" data-target=".requestAdd" class="requestsent has-spinner"><span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span> &nbsp;&nbsp; Send request</a>
						<?php
						}
						?>
                        <a href="javascript:;" data-share="<?php echo $getMarketPlace['mp_id']?>" data-marketby="<?php echo $getMarketPlace['user_id']?>" class="shareMarket"><i class="fa fa-share"></i>Share</a>
						</div>
						<p class="dim-men inside_market display-inline"><i class="fa_eye"></i>&nbsp;<?php echo number_format($countMarketPlaceView)?> Views</p>
				</div>
				
		   </div>
			<div class="row">
				<div class="col-sm-12">
					<div class="post-info_down no-border-dwn"><?php echo $objCommon->html2text($getMarketPlace['mp_description']);?></div>
				</div>
			</div>
       </div>
       </div>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3">
        <?php
		include_once('widget/right_ads_sidebar.php');
		?>
      </div>
    </div>
    </div>
  </div>
</div>

<!--------modal request message------>
<div class="modal fade requestAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content request-back">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Send Request</h4>
      </div>	
	  <form action="<?php echo SITE_ROOT?>ajax/book_market_place.php" id="requestForm" method="post">
      <div class="modal-body">
	  	<div id="preview"></div>
      	<div class="user-post-info-ad">
        	<div class="img-section">
            	<img class="img-responsive" src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo $getMarketPlace['upi_img_url']?>" />
            </div>
            <div class="pot-user-info">
            	<p><?php echo $objCommon->displayName($getMarketPlace);?></p>
                <p class="profesn"><span class="proffesion"></span><?php echo $getMarketPlace['c_name']?></p>
            </div>
            <div class="desc">
            	<?php echo $objCommon->html2text($getMarketPlace['mp_title']);?>
            </div>
        </div>
      		<input type="hidden" name="adId" value="<?php echo $getMarketPlace['mp_id']?>" />
            <div class="form-group">
                <textarea class="form-control" rows="6" placeholder="Write Your Message" name="request_msg"></textarea>
            </div>
        
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary sendRequest">Send Request</button>
      </div>
	  </form>
    </div>
  </div>
</div>
<!--------End modal request message------>

<script type="application/javascript">
/*$(".market-price").on("click",".send_request",function(){
	$(this).addClass('active');
	var adId			=	'<?php echo $getMarketPlace['mp_id']?>';
	if(adId){
		$.ajax({
			url:'<?php echo SITE_ROOT?>ajax/book_market_place.php',
			data:{adId:adId},
			type:"POST",
			success: function(result){
				$(".enq_req").removeClass("send_request");
				$(".enq_req").removeClass("active");
				$(".enq_req").html("Your request has sent");
			}
		});
	}
});*/
$(document).ready(function(e) {
	$('.sendRequest').on('click', function(){ 
			$(".requestsent").addClass('active');
			$(".requestAdd").modal('hide');
			$("#requestForm").ajaxForm(
					{
						target: '#preview',
						success:successCall
					
					}).submit();
	});
});
function successCall(){
	 $(".requestsent").removeClass('active');
	 $(".requestsent").addClass('requestsentDone');
	 $(".requestsent").html('<i class="fa fa-check"></i> Sent');
}
$(".market-price").on("click",".shareMarket",function(){
	var share		=	$(this).data("share");
		marketby	 =	$(this).data("marketby");
		that		 =	this;
	if(share){
		$.ajax({
			url:"<?php echo SITE_ROOT?>ajax/share_market_place.php",
			data:{share:share,marketby:marketby},
			type:"POST",
			success: function(dat1){
				$(that).html('<i class="fa fa-share"></i>Shared');
			}
		});
	}
});
</script>
<?php

$addViewMarketPlace				=	$objmarketView->getRowSql("SELECT mpv_id FROM market_place_view WHERE user_id=".$_SESSION['userId']." AND mp_id=".$getMarketPlace['mp_id']);
if($addViewMarketPlace['mpv_id'] == ''){
	$_POST['user_id']			  =	$_SESSION['userId'];
	$_POST['mp_id']				=	$getMarketPlace['mp_id'];
	$_POST['mpv_created']		  =	date("Y-m-d H:i:s");
	$objmarketView->insert($_POST);
}
include_once(DIR_ROOT."includes/footer.php");
?>
  