<?php
include_once("db_functions.php");
class email_message extends db_functions{
     var $tablename = "email_message";
     var $primaryKey = "em_id";
     var $table_fields = array("em_id"=>"","em_sent_from"=>"","em_sent_to"=>"","em_category"=>"","email_message"=>"","em_message"=>"","em_read_status"=>"","em_createdon"=>"","em_status"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>