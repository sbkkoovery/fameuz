<?php
header('Cache-Control: no-cache, no-store, must-revalidate');
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
require_once DIR_ROOT.'bigpipe/include/classes/class.bigpipe.php';
require_once DIR_ROOT.'bigpipe/include/classes/class.pagelet.php';
require_once DIR_ROOT.'bigpipe/include/functions.php';
$PageletRed = new Pagelet();
$preloaderframe = '';
for($i=0; $i<3; $i++){
	$preloaderframe .= '
	<section id="newsfeed">
		<div class="feedLoader">
			<div class="feedContainer">
				<div class="row">
					<div class="col-sm-1">
						<div class="img-s"></div>
					</div>
					<div class="col-sm-11">
						<div class="rectangle-md">
							<div class="rectangle"></div>
							<div class="rectangle-sm pull-right"></div>
						</div>
						<div class="rectangle-lg">
							<div class="rectangle-sm"></div>
							<div class="rectangle-sm"></div>
							<div class="rectangle-sm"></div>
						</div>
						<div class="rectangle-lg">
							<div class="rectangle-full"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="line"></div>
			<div class="rectangle-wd">
				<div class="rectangle-sm-down"></div>
				<div class="rectangle-sm-down"></div>
				<div class="rectangle-sm-down"></div>
			</div>
		</div>
	</section>';
}
$PageletRed->addHTML($preloaderframe);
$PageletRed->addCSS(SITE_ROOT.'bigpipe/static/red.php?cachebuster='.getRandomValue());
$PageletRed->addJS(SITE_ROOT.'bigpipe/static/delayJS.php?cachebuster='.getRandomValue());
$PageletRed->addJSCode("$('#newsfeed').load('".SITE_ROOT."widget/my_time_line.php',function(){ $('.feedLoader').hide(); }); ");
?>
<script src="<?php echo SITE_ROOT?>bigpipe/static/bigpipe.js" type="text/javascript"></script>
<script src="<?php echo SITE_ROOT?>bigpipe/static/bigpipe-callbacks.js" type="text/javascript"></script>
<script type="text/javascript">
	var globalExecution = function globalExecution(code) {
		window.execScript ? window.execScript(code) : window.eval.call(window, code);
	};
</script>
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
  <div class="inner_top_border">
   <div class="row">
      <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9 chk">
      	<div class="row">
			<div class="jobs-head">
				<div class="pagination_box_jobs">
					<span class="jobs_search">My Recent Activity &nbsp;</span>
					<?php
					include_once(DIR_ROOT."widget/notification_head.php");
					?>
				</div>
			</div>
  </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12">
			
            <div class="clr"></div>
          </div>
          <div class="clr"></div>
        </div>
		<?php echo $PageletRed;?>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3 widcalculator">
        <div>
            <?php
			include_once('widget/right_ads_sidebar.php');
			?>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(e) {
	var cssName = '<?php echo $cssName ?>';
	var windowwid = $(window).width();
	
	if(windowwid > 1200){
		if(cssName == 'user-home'){
			$(".top_section").css({
					"position" : "fixed"
			});
			$(".border-top-s").css({
				
					"margin-top" : "80px"
			});
			$(".inner_top_border").css({
				
					"margin-top" : "81px"
			});
			$(".fixer").css({
				
					"position" : "fixed",
					"width" : "312px"
			});
		}
	}
	
 });
 var myPoint	=	800;
 	 aimpoint	=	0;
 $(window).scroll(function(){
	 var windowoffset	=	$('body').offset().top;
	 	 scrollTop		=	$(this).scrollTop();
		 
	if(scrollTop < 1){
		$('#newsfeed').load('<?php echo SITE_ROOT?>widget/my_time_line.php',function(){ $('.feedLoader').hide(); });
	}else{
		//$('#newsfeed').load('<?php echo SITE_ROOT?>widget/my_time_line.php',function(){ $('.feedLoader').hide(); }); 
 }
	//console.log(aimpoint);
 });
</script>
<?php
BigPipe::render();
include_once(DIR_ROOT."includes/footer-home.php");
?>  