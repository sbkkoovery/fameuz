<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/pagination-class-front.php");
$keywords					=	$objCommon->esc($_GET['keywords']);
$type						=	$objCommon->esc($_GET['type']);
$num_results_per_page		= 	10;
$num_page_links_per_page 	 = 	5;
$pg_param 					= 	"";
$pagesection				 = 	'';
$searchSql				   =	'';
$actAllStatus=$actUpcoming=$actPending=$actComplted=$actExpired	=	'';
if($keywords){
	$searchSql			   =	" and mp_title LIKE '%".$keywords."%' ";
}
if($type){
	$searchSql			   =	" and mpc_id =".$type." ";
}
$getMyAdsSql				 =	"SELECT mp.*,mpc.mpc_name,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name,market_views.countViews
									FROM market_place AS mp 
									LEFT JOIN market_place_category AS mpc ON mp.mp_type =  mpc.mpc_id
									LEFT JOIN users AS user ON mp.user_id = user.user_id 
									LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
									LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
									LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
									LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
									LEFT JOIN (SELECT mp_id,count(mpv_id) AS countViews FROM market_place_view GROUP BY mp_id) AS market_views ON mp.mp_id = market_views.mp_id
									WHERE mp.mp_status = 1 and user.status=1 AND user.email_validation=1 ".$searchSql."  AND mp.mp_show_from <= CURDATE() AND  mp.mp_show_to >= CURDATE() order by mp.mp_created_date desc";
pagination($getMyAdsSql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$getMyAds			   	    =	$objUsers->listQuery($pg_result);
$getMarketPlaceCat		   =	$objUsers->listQuery("SELECT * FROM market_place_category ORDER BY mpc_alias");
?>
<link href="<?php echo SITE_ROOT?>css/pagination.css" rel="stylesheet" type="text/css"><?php /*?>
<link href="<?php echo SITE_ROOT?>css/fixed_portions.css" rel="stylesheet" type="text/css"><?php */?>
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
  <div class="border-top-s"></div>
  <div class="inner_top_border">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
      <div class="jobs-head">
        <div class="pagination_box_jobs pagination_box">
       <a href="<?php echo SITE_ROOT.'user/home'?>">Home <i class="fa fa-caret-right"></i></a>
         <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
        <a href="#" class="active">My Market Place  </a>
       <div class="post_jobs">
       <a href="<?php echo SITE_ROOT?>user/post-market-place"><i class="fa fa-bullhorn"></i>&nbsp;Post Free Ads</a>
       </div>
        <?php
		if(isset($_SESSION['userId'])){
        	include_once(DIR_ROOT."widget/notification_head.php");
		}
        ?>
        </div>
       </div>
       <div class="grey-sec-search">
       <form action="" method="get">
       <div class="row">
       	<div class="col-sm-6">
        <div class="form-group">
  <input type="text" class="form-control checkme"  placeholder="Keywords - e.g., models,fashion" name="keywords" value="<?php echo $objCommon->html2text($keywords)?>">
 </div>
        </div>
        <div class="col-sm-4">
        <div class="form-group upload-help">
		<select class="form-control" name="type">
			<option value="">Select type</option>
			<?php
			foreach($getMarketPlaceCat as $allMarketCat){
				?>
				<option value="<?php echo $objCommon->html2text($allMarketCat['mpc_id'])?>" <?php echo ($type==$allMarketCat['mpc_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allMarketCat['mpc_name'])?></option>
				<?php
			}
			?>
		</select>
  </div>
        </div>
        <div class="col-sm-2">
        <button type="submit" class="btn btn-default">Submit</button>
        </div>
       </div>
       </form>
       </div>
       <div class="container_marketPlace">
	   <?php
	   if(count($getMyAds)>0){
		    foreach($getMyAds as $keyAds=>$allAds){
	   ?>
       <div class="marketplace">
		   <div class="row">
			   
			   <?php
				if($allAds['mp_image']){
				?>
				<div class="col-sm-3 new-width-marketplace">
					<div class="marketplace-image">
						<a href="<?php echo SITE_ROOT.'market-place/show/'.$objCommon->html2text($allAds['mp_alias']).'-'.$allAds['mp_id']?>"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/market_place/<?php echo $allAds['mp_image']?>" /></a>
					 </div>
				 </div>
				<?php
				}
				?>
			    <div class="col-sm-7 col-md-7 col-lg-9 new-width-marketplace-right">
					<div class="info-job marketplace_main">
						<div class="row">
						<div class="col-sm-12 col-md-12">  
							<div class="job-head">
								<p><a href="<?php echo SITE_ROOT.'market-place/show/'.$objCommon->html2text($allAds['mp_alias']).'-'.$allAds['mp_id']?>"><?php echo $objCommon->html2text($allAds['mp_title']);?></a></p>
							</div>
                            <div class="place-job">
										<p><i class="fa fa-file"></i> &nbsp;<?php echo $objCommon->html2text($allAds['mpc_name']);?> </p>
									</div>
									<div class="place-job">
										<p><span class="proffesion"></span> &nbsp;<span class="dark-me"><?php echo $objCommon->html2text($allAds['c_name']);?></span></p>
									</div>
								
							<div class="row">
								<div class="col-sm-12">
									<div class="post-info"> 
                                    	<div class="post-info_down"><p><span class="dim-me">Product Features : </span><?php echo strip_tags($objCommon->limitWords($allAds['mp_description'],180));?></p></div>
									</div>
								</div>
							</div>
						</div>
						<?php
						$marketActualPrice							=	$objCommon->html2text($allAds['mp_actual_price']);
						$marketDiscountPrice						  =	$objCommon->html2text($allAds['mp_dis_price']);
						if($marketActualPrice !='' && $marketDiscountPrice !=''){
							$explActualPrice						  =	explode("###",$marketActualPrice);
							$newActualPrice						   =	$explActualPrice[0].' '.number_format($explActualPrice[1]);
							$explDiscountPrice						=	explode("###",$marketDiscountPrice);
							$newDiscountPrice						 =	$explDiscountPrice[0].' '.number_format($explDiscountPrice[1]);
							$priceStr								 =	'<a href="javascript:;" class="crossOriginal">'.$newActualPrice.'</a><a href="javascript:;" class="actualprice">'.$newDiscountPrice.'</a>';
						}else if ($marketActualPrice !=''){
							$explActualPrice						  =	explode("###",$marketActualPrice);
							$newActualPrice						   =	$explActualPrice[0].' '.number_format($explActualPrice[1]);
							$priceStr								 =	'<a href="javascript:;" class="actualprice">'.$newActualPrice.'</a>';
						}else if($marketDiscountPrice !=''){
							$explDiscountPrice						=	explode("###",$marketDiscountPrice);
							$newDiscountPrice						 =	$explDiscountPrice[0].' '.number_format($explDiscountPrice[1]);
							$priceStr								 =	'<a  href="javascript:;" class="actualprice">'.$newDiscountPrice.'</a>';
						}
						?>
						<div class="col-sm-12 col-md-12 col-lg-12">
                        <p class="dim-men display-inline"><i class="fa_globe"></i>&nbsp;<?php echo $objCommon->html2text($allAds['mp_locations']);?></p>
                        <p class="dim-men display-inline"><i class="fa_eye"></i>&nbsp;<?php echo number_format($allAds['countViews'])?> Views</p>
                         <p class="dim-men display-inline"><i class="fa_share_alt"></i>&nbsp;<a href="javascript:;" data-share="<?php echo $allAds['mp_id']?>" data-marketby="<?php echo $allAds['user_id']?>" class="shareMarket"> Share</a></p>
						 	<div class="market-price pull-right">
								<?php echo $priceStr;?>
                                 <a href="<?php echo SITE_ROOT.'market-place/show/'.$objCommon->html2text($allAds['mp_alias']).'-'.$allAds['mp_id']?>">See More</a>
							</div>
						</div>
						</div>
						<div class="row">
						<div class="col-sm-12">
						</div>
						</div>
					</div>
			   </div>
		   </div>
       </div>
	   <?php
			}
			echo '<div class="paginationDiv">'.$pagination_output.'</div>';
	   }else{ ?>
		  	<div class="dummy_box" style="background-image:url('images/no_post.png');"></div>
	  <?php }
	   ?>
      </div>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3 n_relative">
        <?php
		include_once('widget/right_ads_sidebar.php');
		?>
      </div>
    </div>
    </div>
  </div>
</div>
<script type="text/javascript">

$(window).load(function(e) {
	$('.fixer').fixedSidebar();
});
$(document).ready(function() {
		var swiftit = 0;
		$(".add-field").click(function(){
			var valho = $(this).parent().children(".checkme").val();
			
			if(swiftit == 0 && valho.length !== 0){
			$('<input type="text" placeholder="add one more job">').addClass("form-control").addClass("removeme").insertBefore(this).focus();
			swiftit = 1;
		}
		});
			$(".removeme").blur(function(){
			});
		
		$(".checkme").blur(function(){
			var chksize = $(this).parent().find(".removeme").size();
			if(chksize == 1){
				var myvalue = $(this).parent().children().val();
			}
			});
		$(".form-group").on("blur", ".removeme",function(){
		var myval = $(this).val();
			var me = $(".removeme").val().substr(0,1);
					me.replace(/\s/g,' ');
					$(this).val(me);
		
	if(myval.length == 0){
				$(this).remove();
				swiftit = 0;
			}else{
				swiftit = 0;
			}
		});
		$("#advance").click(function(){
			$(".collapse-advance").slideDown();
			$("#hide-advance").hide();
			$(this).hide();
			});
		$("#advance1").click(function(){
			$(".collapse-advance").slideUp();
			$("#hide-advance").delay(150).fadeIn();
			$("#advance").delay(150).fadeIn();
			});	
	 });
$(".dim-men").on("click",".shareMarket",function(){
	var share		=	$(this).data("share");
		marketby	 =	$(this).data("marketby");
		that		 =	this;
	if(share){
		$.ajax({
			url:"<?php echo SITE_ROOT?>ajax/share_market_place.php",
			data:{share:share,marketby:marketby},
			type:"POST",
			success: function(dat1){
				$(that).html(' Shared');
			}
		});
	}
});
</script>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/fixed_sidebar.js"></script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  