<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/book_model.php");
include_once(DIR_ROOT."class/pagination-class-front.php");
$objBookmodel				=	new book_model();
$book_status				 =	$objCommon->esc($_GET['book_status']);
$num_results_per_page		= 	20;
$num_page_links_per_page 	 = 	5;
$pg_param 					= 	"";
$pagesection				 = 	'';
$actAllStatus=$actUpcoming=$actPending=$actComplted=$actExpired	=	'';
if($book_status=='upcoming'){
	$sqlBookWhere			=	' and (booking.book_from > CURDATE() and booking.bm_model_status=1) ';
	$actUpcoming			 =	'class="active"';
}else if($book_status=='pending'){
	$sqlBookWhere			=	' and (booking.book_from > CURDATE() and booking.bm_model_status=0) ';
	$actPending			  =	'class="active"';
}else if($book_status=='completed'){
	$sqlBookWhere			=	' and (booking.book_from <= CURDATE() and booking.bm_model_status=1) ';
	$actComplted			 =	'class="active"';
}else if($book_status=='expired'){
	$sqlBookWhere			=	' and ((booking.book_from <= CURDATE() and booking.bm_model_status != 1) or (booking.book_from > CURDATE() and booking.bm_model_status=2) or (booking.bm_model_status=3)) ';
	$actExpired			  =	'class="active"';
}else{
	$sqlBookWhere			=	' ';
	$actAllStatus			=	'class="active"';			
}
$getMyBookingsSql			=	"SELECT booking.*,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name,personal.p_phone FROM book_model AS booking 
								  LEFT JOIN users AS user ON booking.model_id = user.user_id 
								  LEFT JOIN personal_details AS personal ON user.user_id=personal.user_id
								  LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
								  LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
								  LEFT JOIN category AS cat ON ucat.uc_c_id=cat.c_id
								  LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
								  WHERE booking.bm_agent_status=1 and booking.agent_id = ".$_SESSION['userId'].$sqlBookWhere."  order by bm_created desc";
pagination($getMyBookingsSql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$getMyBookings			   =	$objBookmodel->listQuery($pg_result);
include_once(DIR_ROOT."includes/booking_requests.php");
include_once(DIR_ROOT."includes/footer.php");
?>
  