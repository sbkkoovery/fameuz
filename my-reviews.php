<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
$usertype							  =	$objCommon->esc($_GET['usertype']);
include_once(DIR_ROOT."js/include/fameuz_lightbox_library.php");
if($usertype=='me'){
	include_once(DIR_ROOT."includes/my_reviews_me.php");
}else{
	include_once(DIR_ROOT."includes/my_reviews.php");
}
include_once(DIR_ROOT.'js/include/my_page.php');
include_once(DIR_ROOT."includes/footer.php");
?>
  