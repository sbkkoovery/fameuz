<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/user_reviews.php");
$objCommon					=	new common();
$objUserReviews		   	   =	new user_reviews();
$action				   	   =	$objCommon->esc($_GET['action']);
$user_id			 		  =	$_SESSION['userId'];
$friend_id					=	$objCommon->esc($_POST['friend_id']);
$reviewEdit				   =	$objCommon->esc($_POST['reviewEdit']);
if($action== 'add' && isset($_POST['rate_val'],$_POST['review_descr'],$user_id,$friend_id) && $_POST['rate_val']!='' && $_POST['review_descr']!='' && $user_id !='' && $friend_id !=''){
	$rate_val		  		 =	$objCommon->esc($_POST['rate_val']);
	$review_descr	  		 =	$_POST['review_msg']	=	$objCommon->esc($_POST['review_descr']);
	if($rate_val <= 5){
		$_POST['review_rate'] =	$rate_val;
	}else{
		$_POST['review_rate'] =	0;
	}
	if($reviewEdit){
		$objUserReviews->updateField(array("review_msg"=>$review_descr,"review_rate"=>$_POST['review_rate']),"review_id=".$reviewEdit);
		$objCommon->addMsg('Your review has been successfully updated...',1);
	}else{
		$review_relation   		  =	$objCommon->esc($_POST['review_relation']);
		$review_relation_other	=	$objCommon->esc($_POST['review_relation_other']);
		$_POST['review_date']	 =	date("Y-m-d H:i:s");
		$_POST['review_status']   =	1;
		$_POST['review_user_to_status']	=	1;
		if($review_relation){
			if($review_relation==3){
				if($review_relation_other){
					$_POST['review_relation']	=	$review_relation_other;
				}else{
					$_POST['review_relation']	=	$review_relation;
				}
			}else{
				$_POST['review_relation']		=	$review_relation;
			}
		}
		$_POST['user_id_by']					 =	$user_id;
		$_POST['user_id_to']					 =	$friend_id;
		$objUserReviews->insert($_POST);
		//----notification table------------------------------------
		$myDetails				   		   	   =	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$user_id);
		$displayName						  	 =	$objCommon->displayName($myDetails);
		$notiType								=	'reviews';
		$notiImg								 =	'';
		$notiDescr   	 	 					   =	'<b>'.$displayName.'</b> has post a review for <b>your profile</b>.';
		$notiUrl  								 =	SITE_ROOT.'user/my-reviews';
		$objCommon->pushNotification($friend_id,$user_id,$notiType,$notiImg,$notiDescr,$notiUrl);
		//----------------------------------------------------------
		$objCommon->addMsg('Your review has been successfully posted...',1);
	}
	
	header("location:".SITE_ROOT.'user/my-reviews/me');
}
else{
	$objCommon->addMsg('Please fill the fields...',0);
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}
?>