<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/email_message_users.php");
$objCommon				=	new common();
$objEmailMsgUsr		   =	new email_message_users();
$userId				   =	$_SESSION['userId'];
$checkedValues			=	$_POST['checkedValues'];
if($userId != '' && count($checkedValues)>0){
	foreach($checkedValues as $allCheckValues){
		$getMsgDetails	=	$objEmailMsgUsr->getRow("em_id=".$allCheckValues." AND(emu_from=".$userId." OR emu_to=".$userId.")");
		if($getMsgDetails['emu_from']==$userId){
			$objEmailMsgUsr->updateField(array("emu_delete_from"=>0),"emu_id=".$getMsgDetails['emu_id']);
		}else if($getMsgDetails['emu_to']==$userId){
			$objEmailMsgUsr->updateField(array("emu_delete_to"=>0),"emu_id=".$getMsgDetails['emu_id']);
		}
	}
}
?>