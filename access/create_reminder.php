<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/reminders.php");
$objCommon				=	new common();
$objReminder			  =	new reminders();
$objCommon->addMsg('Error occured, try again',0);
$editId	=	$objCommon->esc($_POST['remindId']);
if(isset($_SESSION['userId'])){
	if(isset($_POST['remind_title'],$_POST['remind_description'],$_POST['remind_place'],$_POST['remind_date'],$_POST['remind_time'])&&$_POST['remind_date']!=""&&$_POST['remind_title']!=""){
		$_POST['remind_date']	=	date("Y-m-d",strtotime($_POST['remind_date']));
		$_POST['remind_time']	=	($_POST['remind_time']!="")?date("H:i:s",strtotime($_POST['remind_time'])):"00:00:00";
		$_POST['user_id']		=	$_SESSION['userId'];
		if($editId){
			$objReminder->update($_POST,"remind_id=$editId");
			$objCommon->addMsg('Reminder has been updated successfuly..',1);
		}else{
			$objReminder->insert($_POST);
			$objCommon->addMsg('Reminder has been added successfuly..',1);
		}
	}
}
header("location:".$_SERVER['HTTP_REFERER']);
?>