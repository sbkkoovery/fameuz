<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/music.php");
$objCommon				   =	new common();
$path 						= 	$_GET['path'];
$userId					  =	$_SESSION['userId'];
$objMusic			   	   =	new music();
$youtube_link				=	$objCommon->esc($_POST['youtube_link']);
$_POST['user_id']			=	$userId;
$_POST['music_status']	   =	1;
$_POST['music_created']	  =	date("Y-m-d H:i:s");
$_POST['music_edited']	   =	date("Y-m-d H:i:s");
$uploadStatus				=	0;
mysql_query("START TRANSACTION");
$todayDate			   =	date('Y-m-d');
if(!file_exists($path.$todayDate)){
	mkdir($path.$todayDate);
	mkdir($path.$todayDate.'/thumb');
}
$path			   	   =	$path.$todayDate.'/';
$pathThumb		  	  =	$path.'thumb/';
$time			   	   =	time();
if($youtube_link !=''){
	$_POST['music_type']	 =	2;
	$_POST['music_url']	  =	$youtube_link;
	$objMusic->insert($_POST);
	$lastAlbImgId		    =	$objMusic->insertId();
	$uploadStatus			=	1;
}else if($_FILES['file']['tmp_name'] !=''){
	$_POST['music_type']	 =	1;
	$valid_formats 		   = 	array("mp3","wma","aac","MP3","WMA","AAC");
	$ext 				 	 = 	pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
	if(in_array($ext,$valid_formats))
	{
		$actual_music_name   = 	$time."_".$userId.".".$ext;
		if(move_uploaded_file($_FILES['file']['tmp_name'], $path.$actual_music_name)){
			//----------------start upload to AWS----------------------------------------
			require(DIR_ROOT."amazone_s3/S3.php");
			$s3			=	new S3(AWS_ID,AWS_KEY);
			S3::putObjectFile($path.$actual_music_name,AWS_MUSIC_BUCKET,'uploads/music/'.$todayDate.'/'.$actual_music_name,S3::ACL_PUBLIC_READ,array());
			//----------------end upload to AWS-------------------------------------------
		}
		$_POST['music_url']			 =	$todayDate.'/'.$actual_music_name;
		$objMusic->insert($_POST);
		$lastAlbImgId		   		   =	$objMusic->insertId();
		$uploadStatus				   =	1;
		echo '<input type="hidden" name="videoId" value="'.$lastAlbImgId.'" />';
	}
}
mysql_query("COMMIT");
?>

