<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/comments.php");
$objCommon				=	new common();
$objComments			  =	new comments();
$action				   =	$objCommon->esc($_GET['action']);
$userId				   =	$_SESSION['userId'];
if($action== 'add_comment' && $userId != ''){
	if(isset($_POST['comment_descr_input'],$_REQUEST['blogId'],$_REQUEST['userto']) && $_POST['comment_descr_input']!=''  && $_REQUEST['userto'] != ''){
		$blogId	 				  =	$objCommon->esc($_REQUEST['blogId']);
		$adIdExpl					=	explode("-",$blogId);
		$getAdId					 =	end($adIdExpl);
		$getAdAliasArr			   =	array_slice($adIdExpl, 0, -1);
		$getAdAlias				  =	implode("-",$getAdAliasArr);
		$getVidId					=	$objComments->getRowSql("select blog_id from blogs where blog_id=".$getAdId." AND blog_alias ='".$getAdAlias."' ");
		if($getVidId['blog_id']){
			mysql_query("START TRANSACTION");
			$_POST['comment_cat']		 =	$typecat	=	7;
			$_POST['comment_content']	 =	$getVidId['blog_id'];
			$_POST['comment_descr']	   =	$objCommon->esc($_POST['comment_descr_input']);
			$_POST['comment_user_to']	 =	$objCommon->esc($_REQUEST['userto']);
			$_POST['comment_user_by']     =	$userId;
			$_POST['comment_time']		=	date("Y-m-d H:i:s");
			$_POST['comment_status']	  =	1;
			$replyId					  =	$objCommon->esc($_REQUEST['replyId']);
			if($replyId){
				$_POST['comment_reply_id']=	$replyId;
			}else{
				$_POST['comment_reply_id']=	0;
			}
			$objComments->insert($_POST);
		//----notification table------------------------------------
			$myDetails				   		   	   =	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$userId);
			$displayName						  	 =	$objCommon->displayName($myDetails);
			$friend_id							   =	$objCommon->esc($_POST['comment_user_to']);
			$notiType								=	'blog-comments';
			$notiImg								 =	'';
			$notiDescr   	 	 					   =	'<b>'.$displayName.'</b> commented on your <b>blog</b>.';
			$notiUrl  								 =	SITE_ROOT.'blog/show/'.$blogId;
			$objCommon->pushNotification($friend_id,$userId,$notiType,$notiImg,$notiDescr,$notiUrl);
			//----------------------------------------------------------
			$_POST = array();
			mysql_query("COMMIT");
		}
	}	
}
?>