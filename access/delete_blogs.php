<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/blogs.php");
$objCommon				=	new common();
$objBlogs		  		 =	new blogs();
$action				   =	$objCommon->esc($_GET['action']);
$userId				   =	$_SESSION['userId'];
$jobId				   	=	$objCommon->esc($_GET['jobId']);
if($action== 'del_job' && $userId != '' && $jobId !=''){
	$getMarketDetails	 =	$objBlogs->getRow("blog_id=".$jobId);
	if($getMarketDetails['blog_img']){
		$explAiImages	   		=	explode("/",$getMarketDetails['blog_img']);
		$getAiImag1		  	  =	$explAiImages[0].'/thumb/'.$explAiImages[1];
		$getAiImag2		  	  =	$explAiImages[0].'/original/'.$explAiImages[1];
		$imgPath				 =	DIR_ROOT.'uploads/blogs/'.$getMarketDetails['blog_img'];
		$imgPath1				=	DIR_ROOT.'uploads/blogs/'.$getAiImag1;
		$imgPath2				=	DIR_ROOT.'uploads/blogs/'.$getAiImag2;
		unlink($imgPath);
		unlink($imgPath1);
		unlink($imgPath2);
	}
	$objBlogs->delete("blog_id=".$jobId);;
	$objCommon->addMsg('Selected item has been deleted...',1);
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}else{
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}
?>