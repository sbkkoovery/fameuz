<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/video_introduction.php");
$objCommon				=	new common();
$objVideoIntro			=	new video_introduction();
if($_FILES['video_introduction']['tmp_name'] !='' || $_POST['video_intro_youtube'] != ''){	
	mysql_query("START TRANSACTION");
	$objVideoIntro->delete("user_id=".$_SESSION['userId']);
	$path 						= 	DIR_ROOT.'uploads/video_introduction/';
	$youtube_link				=	$objCommon->esc($_POST['youtube_link']);
	$_POST['user_id']			=	$_SESSION['userId'];
	$_POST['vi_status']	   	  =	1;
	$_POST['vi_created_date']	=	date("Y-m-d H:i:s");
	$youtube_link				=	$objCommon->esc($_POST['video_intro_youtube']);
	$todayDate			       =	date('Y-m-d');
	if(!file_exists($path.$todayDate)){
		mkdir($path.$todayDate);
		mkdir($path.$todayDate.'/thumb');
	}
	$path			   	   =	$path.$todayDate.'/';
	$pathThumb		  	  =	$path.'thumb/';
	$time			   	   =	time();
	if($youtube_link !=''){
		$_POST['vi_type']	 	=	2;
		$_POST['vi_url']	  	 =	$youtube_link;
		//$getyoutube			  =	$objCommon->getYoutubeDetails($youtube_link);
		$videoID				 =	$objCommon->getYoutubeId($youtube_link);
		$thumbNail	 	  	   = 	'https://i.ytimg.com/vi/'.$videoID.'/hqdefault.jpg';
		$newYouTubeImgName	   =	$_SESSION['userId'].".jpg";
		file_put_contents ($pathThumb.$newYouTubeImgName,file_get_contents($thumbNail));
		$_POST['vi_thumb']	   =	$todayDate.'/thumb/'.$newYouTubeImgName;
		$objVideoIntro->insert($_POST);
	}else if($_FILES['video_introduction']['tmp_name'] !=''){
		$_POST['vi_type']	 	=	1;
		$valid_formats 		   = 	array("mp4", "mov","mpeg4","avi","wmv","MPEGPS","flv","3gp","WebM","AVI","MP4","FLV");
		$ext 				 	 = 	pathinfo($_FILES['video_introduction']['name'], PATHINFO_EXTENSION);
		if(in_array($ext,$valid_formats))
		{
			if($_FILES['video_introduction']['size'] < 104857600){
				$actual_vid_name 	= 	$time."_".$_SESSION['userId'].".".$ext;
				$newVidName		 =	$time."_".$_SESSION['userId'].".mp4";
				//shell_exec('/usr/local/bin/ffmpeg -i '.$_FILES['video_introduction']['tmp_name'].' -strict -2 '.$path.$newVidName);
				//shell_exec('/usr/local/bin/ffmpeg -itsoffset -4  -i '.$_FILES['video_introduction']['tmp_name'].' -vcodec mjpeg -vframes 1 -an -f rawvideo -s 320x240 '.$pathThumb.$time.'_'.$_SESSION['userId'].'.jpg');
				shell_exec('/usr/local/bin/ffmpeg -i '.$_FILES['video_introduction']['tmp_name'].' -strict -2 '.$path.$newVidName);
				shell_exec('/usr/local/bin/ffmpeg -i '.$path.$newVidName.' -ss 00:00:02.000 -vframes 1 '.$pathThumb.$time.'_'.$_SESSION['userId'].'.jpg');
				$videofile		  =	$path.$newVidName;
				$xyz 				= 	shell_exec("/usr/local/bin/ffmpeg -i ".$videofile." 2>&1");
				$search			 =	'/Duration: (.*?),/';
				preg_match($search, $xyz, $matches);
				$explode			= 	explode(':', $matches[1]);
				$_POST['vi_url']	=	$todayDate.'/'.$newVidName;
				$_POST['vi_thumb']  =	$todayDate.'/thumb/'.$time.'_'.$_SESSION['userId'].'.jpg';
				$objVideoIntro->insert($_POST);
			}
		}
	}
	mysql_query("COMMIT");
}
exit;
?>