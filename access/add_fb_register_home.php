<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/users.php");
include_once(DIR_ROOT."class/user_categories.php");
include_once(DIR_ROOT."class/user_verification.php");
include_once(DIR_ROOT."class/user_social_links.php");
include_once(DIR_ROOT."class/personal_details.php");
include_once(DIR_ROOT."class/user_privacy.php");
require_once(DIR_ROOT."PHPMailer/class.phpmailer.php");
include_once(DIR_ROOT."class/user_chat_status.php");
include_once(DIR_ROOT."class/model_details.php");
include_once(DIR_ROOT."class/user_profile_complete.php");
include_once(DIR_ROOT."class/user_fb_details.php");
$objCommon				=	new common();
$objUsers				 =	new users();
$objUserCategories		=	new user_categories();
$objUserVerification	  =	new user_verification();
$objUserSocialLinks	   =	new user_social_links();
$objPersonalDetails	   =	new personal_details();
$objUserPrivacy		   =	new user_privacy();
$objUserChatStatus   		=	new user_chat_status();
$objModelDetails		  =	new model_details();
$objProfileComplete	   =	new user_profile_complete();
$objFbdetails		     =	new user_fb_details();
$action				   =	$objCommon->esc($_GET['action']);
$fullname	=	$_SESSION['FULLNAME'];
$fb_id	=	$_SESSION['FBID'];
$nameparts = explode(" ", $fullname);
if($action== 'add' && isset($_POST['reg_main_catfb'],$_POST['reg_catfb'],$_POST['reg_emailfb']) && $_POST['reg_main_catfb']!='' && $_POST['reg_catfb']!='' && $_POST['reg_emailfb']!=''){
	$getOldUsers						  =	$objUsers->getRow("email='".$_POST['reg_emailfb']."'");
	if($getOldUsers['user_id'] ==''){
		if (!filter_var($_POST['reg_emailfb'], FILTER_VALIDATE_EMAIL)) {
		 $objCommon->addMsg('Invalid Email Format...',0);
		 header("location:".$_SERVER['HTTP_REFERER']); 
		 exit;
		}
	mysql_query("START TRANSACTION");
	$_POST['uc_m_id']					 =	$objCommon->esc($_POST['reg_main_cat']);
	$_POST['uc_c_id']					 =	$objCommon->esc($_POST['reg_cat']);
	$my_email							 =	$_POST['email']					   =	$objCommon->esc($_POST['reg_emailfb']);
	$_POST['first_name']				  =	$nameparts[0];
	$_POST['last_name']     			   =	$nameparts[1];
	$_POST['display_name']			    =	$fullname;
	$_POST['status']					  =	'1';
	$_POST['email_validation']			=	'1';
	$_POST['login_type']				  =	'2';
	$_POST['ucs_status']				  =	1;
	$_POST['created_on']				  =	date("Y-m-d H:i:s"); 
	$_POST['edited_on']				   =	date("Y-m-d H:i:s");
	$objUsers->insert($_POST);
	$_POST['user_id'] = $my_user_id	   =	$objUsers->insertId();
	$_SESSION['userId']		=	$my_user_id;
	$objFbdetails->updateField(array('user_id'=>$my_user_id),"fb_id=".$fb_id);
	
	$objUserCategories->insert($_POST);
	$uv_verification					  =	$_POST['uv_verification']			 =	$objCommon->randStrGen(15);
	$objUserVerification->insert($_POST);
	$explMyEmail						  =	explode("@",$my_email);
	$usl_fameuz			 			   =	$objCommon->getAlias($explMyEmail[0]);
	$getOlduslLink						=	$objUserSocialLinks->getRow("usl_fameuz='".$usl_fameuz."'");
	if($getOlduslLink['usl_id']){
		$_POST['usl_fameuz']			  =	$usl_fameuz.'-'.$objCommon->randStrGenDigit(3).'-'.'1001';
	}else{
		$_POST['usl_fameuz']			  =	$usl_fameuz.'-'.'1001';
	}
	$objUserSocialLinks->insert($_POST);
	$objPersonalDetails->insert($_POST);
	$objUserPrivacy->insert($_POST);
	$objUserChatStatus->insert($_POST);
	$objModelDetails->insert($_POST);
	$objProfileComplete->insert($_POST);
	mysql_query("INSERT INTO privacy_settings(ps_id,user_id,ps_value) VALUES (NULL, '".$my_user_id."', 'a:9:{s:11:\"profile_see\";a:1:{i:0;s:1:\"1\";}s:12:\"personal_see\";a:1:{i:0;s:1:\"1\";}s:10:\"photos_see\";a:1:{i:0;s:1:\"1\";}s:10:\"review_see\";a:1:{i:0;s:1:\"1\";}s:10:\"worked_see\";a:1:{i:0;s:1:\"1\";}s:10:\"disply_see\";a:12:{i:0;s:8:\"language\";i:1;s:8:\"location\";i:2;s:6:\"gender\";i:3;s:9:\"ethnicity\";i:4;s:6:\"height\";i:5;s:9:\"eye_color\";i:6;s:4:\"hair\";i:7;s:5:\"chest\";i:8;s:5:\"waist\";i:9;s:4:\"hips\";i:10;s:5:\"dress\";i:11;s:4:\"shoe\";}s:7:\"book_me\";a:1:{i:0;s:1:\"1\";}s:18:\"email_notification\";a:4:{i:0;s:9:\"booked_me\";i:1;s:9:\"worked_me\";i:2;s:13:\"login_attempt\";i:3;s:16:\"recover_password\";}s:21:\"profile_status_select\";a:1:{i:0;s:5:\"daily\";}}')");
	
	mysql_query("COMMIT");
	header("location:".SITE_ROOT."user/register-page");
	}else{
		$objCommon->addMsg('You have already registerd...',0);
		header("location:".$_SERVER['HTTP_REFERER']);
	}
}else{
	$objCommon->addMsg('Please fill the fields...',0);
	header("location:".$_SERVER['HTTP_REFERER']);
}
?>