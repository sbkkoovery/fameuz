<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/book_model.php");
$objCommon				=	new common();
$objBookModel			 =	new book_model();
$action				   =	$objCommon->esc($_GET['action']);
$userId				   =	$_SESSION['userId'];
if($action== 'add_booking' && $userId != ''){
 if(isset($_POST['book_from_date'],$_POST['book_to_date'],$_POST['book_title'],$_POST['book_email']) && $_POST['book_from_date']!='' && $_POST['book_to_date'] !='' && $_POST['book_title'] != '' && $_POST['book_email'] !=''){
		mysql_query("START TRANSACTION");
		$book_from_date					 =	$objCommon->esc($_POST['book_from_date']);
		$book_from_date_expl				=	explode("/",$book_from_date);
		$_POST['book_from']				 =	$book_from_date_expl[2].'-'.$book_from_date_expl[0].'-'.$book_from_date_expl['1'];
		$book_to_date					   =	$objCommon->esc($_POST['book_to_date']);
		$book_to_date_expl				  =	explode("/",$book_to_date);
		$_POST['book_to']				   =	$book_to_date_expl[2].'-'.$book_to_date_expl[0].'-'.$book_to_date_expl['1'];
		$_POST['bm_title']		   		  =	$objCommon->esc($_POST['book_title']);
		$_POST['bm_company']	   		  	=	$objCommon->esc($_POST['book_company']);
		$_POST['bm_work']	   			   =	$objCommon->esc($_POST['book_work']);
		$_POST['bm_country']	   		    =	$objCommon->esc($_POST['book_country']);
		$_POST['bm_budget']	   		   	 =	$objCommon->esc($_POST['book_budget']);
		$_POST['bm_email']	   		  	  =	$objCommon->esc($_POST['book_email']);
		$_POST['bm_phone']	   		  	  =	$objCommon->esc($_POST['book_phone']);
		$_POST['bm_allowance']	   		  =	$objCommon->esc($_POST['book_allowance']);
		$_POST['bm_descr']	   			  =	$objCommon->esc($_POST['book_brief_details']);
		$_POST['agent_id']				  =	$userId;
		$_POST['model_id']				  =	$objCommon->esc($_POST['hid_user_to']);
		$hid_edit_bookId					=	$objCommon->esc($_POST['hid_edit_bookId']);
		if($hid_edit_bookId){
			$objBookModel->update($_POST,"bm_id=".$hid_edit_bookId);
			if($_FILES["book_attach"]["name"]){
			$path			 =	DIR_ROOT.'uploads/booking_attachement/';
			$todayDate		=	date('Y-m-d');
			if(!file_exists($path.$todayDate)){
			mkdir($path.$todayDate);
			}
			$path	=	$path.$todayDate.'/';
			$allowedExts = array("gif", "jpeg", "jpg", "png", "doc","docx","odt", "pdf", "txt");
			$temp = explode(".", $_FILES["book_attach"]["name"]);
			$extension = end($temp);
			if ((($_FILES["book_attach"]["type"] == "image/gif") || ($_FILES["book_attach"]["type"] == "image/jpeg") || ($_FILES["book_attach"]["type"] == "image/jpg") || ($_FILES["book_attach"]["type"] == "image/pjpeg") || ($_FILES["book_attach"]["type"] == "image/x-png") || ($_FILES["book_attach"]["type"] == "image/png") || ($_FILES["book_attach"]["type"] == "application/pdf") || ($_FILES["book_attach"]["type"] == "text/plain") || ($_FILES["book_attach"]["type"] == "application/msword") || ($_FILES["book_attach"]["type"] == "application/vnd.oasis.opendocument.text")) && ($_FILES["book_attach"]["size"] < 500000) && in_array($extension, $allowedExts))
			{
			 $actual_image_name = time()."_".$userId.".".$extension;
			if ($_FILES["book_attach"]["error"] > 0)
			{
				//echo "error found";
			}
			else
			{
			if (file_exists($path.$actual_image_name))
			  {
			  //echo "file already exist";
			  }
			else
			  {
			  move_uploaded_file($_FILES["book_attach"]["tmp_name"],$path.$actual_image_name);
			  $bm_attachement					=	$todayDate.'/'.$actual_image_name;
			  $objBookModel->updateField(array("bm_attachement"=>$bm_attachement),"bm_id=".$hid_edit_bookId);
			  }
			}
			}
			else
			{
			//echo "fail filetype";
			}
			}
		}else{
			$_POST['bm_created']				=	date("Y-m-d H:i:s");
			$_POST['bm_model_status']		   =	0;
			$_POST['bm_agent_status']		   =	1;
			$_POST['bm_read_status']			=	0;
			$objBookModel->insert($_POST);
			$lastBookId						 =	$objBookModel->insertId();
			if($_FILES["book_attach"]["name"]){
			$path			 =	DIR_ROOT.'uploads/booking_attachement/';
			$todayDate		=	date('Y-m-d');
			if(!file_exists($path.$todayDate)){
			mkdir($path.$todayDate);
			}
			$path	=	$path.$todayDate.'/';
			$allowedExts = array("gif", "jpeg", "jpg", "png", "doc","docx","odt", "pdf", "txt");
			$temp = explode(".", $_FILES["book_attach"]["name"]);
			$extension = end($temp);
			if ((($_FILES["book_attach"]["type"] == "image/gif") || ($_FILES["book_attach"]["type"] == "image/jpeg") || ($_FILES["book_attach"]["type"] == "image/jpg") || ($_FILES["book_attach"]["type"] == "image/pjpeg") || ($_FILES["book_attach"]["type"] == "image/x-png") || ($_FILES["book_attach"]["type"] == "image/png") || ($_FILES["book_attach"]["type"] == "application/pdf") || ($_FILES["book_attach"]["type"] == "text/plain") || ($_FILES["book_attach"]["type"] == "application/msword") || ($_FILES["book_attach"]["type"] == "application/vnd.oasis.opendocument.text")) && ($_FILES["book_attach"]["size"] < 500000) && in_array($extension, $allowedExts))
			{
			 $actual_image_name = time()."_".$userId.".".$extension;
			if ($_FILES["book_attach"]["error"] > 0)
			{
				//echo "error found";
			}
			else
			{
			if (file_exists($path.$actual_image_name))
			  {
			  //echo "file already exist";
			  }
			else
			  {
			  move_uploaded_file($_FILES["book_attach"]["tmp_name"],$path.$actual_image_name);
			  $bm_attachement					=	$todayDate.'/'.$actual_image_name;
			  $objBookModel->updateField(array("bm_attachement"=>$bm_attachement),"bm_id=".$lastBookId);
			  }
			}
			}
			else
			{
			//echo "fail filetype";
			}
			}
			//----notification table------------------------------------
			$myDetails				   		   	   =	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$userId);
			$displayName						  	 =	$objCommon->displayName($myDetails);
			$friend_id							   =	$objCommon->esc($_POST['hid_user_to']);
			$notiType								=	'booking';
			$notiImg								 =	'';
			$notiDescr   	 	 					   =	'<b>'.$displayName.'</b> has sent you <b>booking request</b>';
			$notiUrl  								 =	SITE_ROOT.'user/my-booking';
			$objCommon->pushNotification($friend_id,$userId,$notiType,$notiImg,$notiDescr,$notiUrl);
			//----------------------------------------------------------
 		}
		mysql_query("COMMIT");
		$objCommon->addMsg('Your booking reguest has been sent',1);
		header("location:".SITE_ROOT."user/booking-requests");
	}else{
		$objCommon->addMsg('Please fill the fields...',0);
		header("location:".$_SERVER['HTTP_REFERER']);
	}
}
?>