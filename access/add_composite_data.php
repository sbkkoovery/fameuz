<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/composite_card_data.php");
$objCompData			  =	new composite_card_data();
$objCommon				=	new common();
$userId				   =	$_SESSION['userId'];
if($userId !='' && count($_POST['modelDataArr']) >0){
	$modelDataArr		 =	$_POST['modelDataArr'];
	$modelDataArr		 =	array_filter($modelDataArr);
	$addData			  =	implode(",",$modelDataArr);
	$getOldData		   =	$objCompData->getRow("user_id=".$userId);
	$_POST['user_id']	 =	$userId;
	if($getOldData['ccd_id']){
		$objCompData->updateField(array('ccd_data_table'=>$addData),"user_id=".$userId);
	}else{
		$_POST['ccd_data_table']	=	$addData;
		$objCompData->insert($_POST);
	}
}
header("location:".SITE_ROOT."user/composite-card");
?>