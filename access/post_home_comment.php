<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/comments.php");
include_once(DIR_ROOT."class/videos.php");
$objCommon				=	new common();
$objComments			  =	new comments();
$objVideos				=	new videos();
$action				   =	$objCommon->esc($_REQUEST['action']);
$userId				   =	$_SESSION['userId'];
if($action== 'add_comment' && $userId != ''){
	if(isset($_POST['comment_descr_input'],$_REQUEST['commentContent'],$_REQUEST['noti_whome'],$_REQUEST['commentCat']) && $_POST['comment_descr_input']!=''  && $_REQUEST['noti_whome'] != '' && $_REQUEST['commentCat'] != '' && $_REQUEST['commentContent'] != ''){
			mysql_query("START TRANSACTION");
			$_POST['comment_cat']		 =	$typecat			=	$objCommon->esc($_REQUEST['commentCat']);
			$_POST['comment_content']	 =	$comment_content	=	$objCommon->esc($_REQUEST['commentContent']);
			$_POST['comment_descr']	   =	$objCommon->esc($_POST['comment_descr_input']);
			$_POST['comment_user_to']	 =	$objCommon->esc($_REQUEST['noti_whome']);
			$_POST['comment_user_by']     =	$userId;
			$_POST['comment_time']		=	date("Y-m-d H:i:s");
			$_POST['comment_status']	  =	1;
			$replyId					  =	$objCommon->esc($_REQUEST['replyId']);
			if($replyId){
				$_POST['comment_reply_id']=	$replyId;
			}else{
				$_POST['comment_reply_id']=	0;
			}
			$objComments->insert($_POST);
		//----notification table------------------------------------
		$myDetails				   		   	   =	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$userId);
		$displayName						  	 =	$objCommon->displayName($myDetails);
		$friend_id							   =	$objCommon->esc($_POST['comment_user_to']);
		$notiImg								 =	'';
		if($typecat==1){
				$notiType		  				=	'comments';
				$notiDescr   	 	 			   =	'<b>'.$displayName.'</b> commented on your <b>photo</b>.';
				$notiUrl  						 =	SITE_ROOT.'user/single-image?type='.$typecat.'&id='.$comment_content;
			}else if($typecat==2){
				$notiType		  				=	'comments';
				$notiDescr   	 	 			   =	'<b>'.$displayName.'</b> commented on your <b>photo</b>.';
				$notiUrl  						 =	SITE_ROOT.'user/single-image?type='.$typecat.'&id='.$comment_content;
			}else if($typecat==4){
				$getVideoId			  		  =	$objComments->getRowSql("SELECT video_encr_id FROM videos WHERE video_id =".$comment_content);
				$notiType		  				=	'video-comments';
				$notiDescr   	 	 			   =	'<b>'.$displayName.'</b> commented on your <b>video</b>.';
				$notiUrl  						 =	SITE_ROOT.'video/watch/'.$getVideoId['video_encr_id'];
			}else if($typecat==6){
				$notiType		  				=	'comments';
				$notiDescr   	 	 			   =	'<b>'.$displayName.'</b> commented on your <b>profile image</b>.';
				$notiUrl  						 =	SITE_ROOT.'user/single-image?type='.$typecat.'&id='.$comment_content;
			}else if($typecat==9){
				$notiType		  				=	'album_comments';
				$notiDescr   	 	 			   =	'<b>'.$displayName."</b> commented on your <b>album '".$objCommon->html2text($getAlbumName['a_name'])."'</b>.";
				$notiUrl  						 =	SITE_ROOT.'user/my-album';
			}else if($typecat==10){
				$notiType		  				=	'posts_comments';
				$notiDescr   	 	 			   =	'<b>'.$displayName."</b> commented on your <b>post '".$objCommon->html2text($getAlbumName['a_name'])."'</b>.";
				$notiUrl  						 =	SITE_ROOT.'user/home';
			}
		$objCommon->pushNotification($friend_id,$userId,$notiType,$notiImg,$notiDescr,$notiUrl);
		//----------------------------------------------------------
			$_POST = array();
			mysql_query("COMMIT");
	}	
}
?>