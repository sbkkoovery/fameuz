<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/users.php");
include_once(DIR_ROOT."class/user_categories.php");
include_once(DIR_ROOT."class/user_social_links.php");
include_once(DIR_ROOT."class/personal_details.php");
require_once(DIR_ROOT."PHPMailer/class.phpmailer.php");
include_once(DIR_ROOT."class/model_details.php");
include_once(DIR_ROOT."class/user_privacy.php");
include_once(DIR_ROOT."class/user_profile_complete.php");
include_once(DIR_ROOT."class/video_introduction.php");
$objCommon				=	new common();
$objUsers				 =	new users();
$objUserCategories		=	new user_categories();
$objUserSocialLinks	   =	new user_social_links();
$objPersonalDetails	   =	new personal_details();
$mail             		 = 	new PHPMailer();
$objModelDetails		  =	new model_details();
$objUserPrivacy		   =	new user_privacy();
$objProfComplete		  =	new user_profile_complete();
$objVideoIntro			=	new video_introduction();
$action				   =	$objCommon->esc($_GET['action']);
if($action== 'edit_personal_details'){
 if(isset($_POST['per_f_name']) && $_POST['per_f_name']!=''){
		mysql_query("START TRANSACTION");
		$_POST['first_name']		=	$objCommon->esc($_POST['per_f_name']);
		$_POST['last_name']		 =	$objCommon->esc($_POST['per_l_name']);
		$_POST['p_phone']		   =	$objCommon->esc($_POST['per_phone']);
		$_POST['p_alt_phone']	   =	$objCommon->esc($_POST['per_alt_phone']);
		$_POST['p_alt_email']	   =	$objCommon->esc($_POST['per_alt_email']);
		$per_phone_privacy		  =	array(($_POST['per_phone_privacy1']==1)?1:0,($_POST['per_phone_privacy2']==1)?1:0,($_POST['per_phone_privacy3']==1)?1:0,($_POST['per_phone_privacy4']==1)?1:0);
		$_POST['uc_p_phone']		=	implode(",",$per_phone_privacy);
		$per_alt_phone_privacy	  =	array(($_POST['per_alt_phone_privacy1']==1)?1:0,($_POST['per_alt_phone_privacy2']==1)?1:0,($_POST['per_alt_phone_privacy3']==1)?1:0,($_POST['per_alt_phone_privacy4']==1)?1:0);
		$_POST['uc_p_alt_phone']	=	implode(",",$per_alt_phone_privacy);
		$per_password		  	   =	$objCommon->esc($_POST['per_password']);
		$per_con_password		   =	$objCommon->esc($_POST['per_con_password']);
		if($per_password == $per_con_password && $per_password !=''){
			$_POST['password']	  =	md5($per_password);
		}
		$objUsers->update($_POST,"user_id=".$_SESSION['userId']);
		$objPersonalDetails->update($_POST,"user_id=".$_SESSION['userId']);
		$objUserPrivacy->update($_POST,"user_id=".$_SESSION['userId']);
		mysql_query("COMMIT");
		$objCommon->addMsg('Changes has been updated',1);
		//header("location:".$_SERVER['HTTP_REFERER']);
	}else{
		$objCommon->addMsg('Please fill the fields...',0);
		header("location:".$_SERVER['HTTP_REFERER']);
		exit;
	}
}
if($action== 'edit_public_details'){
	mysql_query("START TRANSACTION");
	$_POST['display_name']		=	$objCommon->esc($_POST['pub_display_name']);
	$dob_date					 =	$_POST['pub_dob_date'];
	$dob_month					=	$objCommon->esc($_POST['pub_dob_month']);
	$dob_year					 =	$objCommon->esc($_POST['pub_dob_year']);
	$_POST['p_dob']			   =	$dob_year.'-'.$dob_month.'-'.$dob_date;
	$_POST['p_gender']			=	$objCommon->esc($_POST['pub_gender']);
	$_POST['p_ethnicity']	     =	$objCommon->esc($_POST['pub_ethnicity']);
	$_POST['p_languages']		 =	$objCommon->esc($_POST['pub_language']);
	$_POST['p_about']			 =	$objCommon->esc($_POST['pub_about']);
	$_POST['me_id']			   =	$objCommon->esc($_POST['pub_model_eyes']);
	$_POST['mhair_id']	  		=	$objCommon->esc($_POST['pub_model_hair']);
  	$_POST['mh_id']			   =	$objCommon->esc($_POST['pub_model_height']);
	$_POST['mw_id']			   =	$objCommon->esc($_POST['pub_model_waist']);
	$_POST['mhp_id']			  =	$objCommon->esc($_POST['pub_model_hips']);
	$_POST['mc_id']			   =	$objCommon->esc($_POST['pub_model_chest']);
	$_POST['ms_id']			   =	$objCommon->esc($_POST['pub_model_shoe']);
	$_POST['mdress_id']		   =	$objCommon->esc($_POST['pub_model_dress']);
	$_POST['mj_id']			   =	$objCommon->esc($_POST['pub_model_jacket']);
	$_POST['mt_id']			   =	$objCommon->esc($_POST['pub_model_trousers']);
	$_POST['mcollar_id']		  =	$objCommon->esc($_POST['pub_model_collar']);
	$_POST['n_id']				=	$objCommon->esc($_POST['pub_nationality']);
	$_POST['p_city']			  =	$objCommon->esc($_POST['pub_city']);
	$_POST['p_country']		   =	$objCommon->esc($_POST['pub_country']);		
	$pub_model_descipline		 =	$_POST['pub_model_descipline'];
	$per_dob_privacy		  	  =	array(($_POST['per_dob_privacy1']==1)?1:0,($_POST['per_dob_privacy2']==1)?1:0,($_POST['per_dob_privacy3']==1)?1:0,($_POST['per_dob_privacy4']==1)?1:0);
	$_POST['uc_p_dob']		  	=	implode(",",$per_dob_privacy);
	if(count($pub_model_descipline)>0){
		$model_dis_id			.=	',';
		foreach($pub_model_descipline as $all_model_descipline){
			$model_dis_id		.=	$all_model_descipline.",";
		}
		$_POST['model_dis_id']	=	$model_dis_id;
	}
	$pub_sub_category			 =	$_POST['pub_sub_category'];
	if(count($pub_sub_category)>0){
		$uc_s_id				 .=	',';
		foreach($pub_sub_category as $all_pub_sub_category){
			$uc_s_id			 .=	$all_pub_sub_category.",";
		}
		$_POST['uc_s_id']		 =	$uc_s_id;
	}
	if($_POST['display_name']){
		$getOldfameuzlink		=	$objUserSocialLinks->getRowSql("SELECT usl_fameuz FROM user_social_links WHERE user_id=".$_SESSION['userId']);
		$explOldLink			 =	explode("-",$getOldfameuzlink['usl_fameuz']);
		if(count($explOldLink)>0){
			if(in_array("1001", $explOldLink)){
				$dispName		=	$objCommon->getAlias($_POST['display_name']);
				$getOlduslLink   =	$objUserSocialLinks->getRow("usl_fameuz='".$dispName."'");
				if($getOlduslLink['usl_id']){
					$usl_fameuz			  =	$dispName.'-'.$objCommon->randStrGenDigit(3);
				}else{
					$usl_fameuz			  =	$dispName;
				}
				$objUserSocialLinks->updateField(array("usl_fameuz"=>$usl_fameuz),"user_id=".$_SESSION['userId']);
			}
		}
	}
	
	
	
	
	
	$objUsers->update($_POST,"user_id=".$_SESSION['userId']);
	$objPersonalDetails->update($_POST,"user_id=".$_SESSION['userId']);
	$objModelDetails->update($_POST,"user_id=".$_SESSION['userId']);
	if(isset($_POST['uc_s_id'])){
		$objUserCategories->update($_POST,"user_id=".$_SESSION['userId']);
	}
	$objUserPrivacy->update($_POST,"user_id=".$_SESSION['userId']);
	if($_FILES['video_introduction']['tmp_name'] !='' || $_POST['video_intro_youtube'] != ''){
		$objVideoIntro->delete("user_id=".$_SESSION['userId']);
		$path 						= 	DIR_ROOT.'uploads/video_introduction/';
		$youtube_link				=	$objCommon->esc($_POST['youtube_link']);
		$_POST['user_id']			=	$_SESSION['userId'];
		$_POST['vi_status']	   	  =	1;
		$_POST['vi_created_date']	=	date("Y-m-d H:i:s");
		$youtube_link				=	$objCommon->esc($_POST['video_intro_youtube']);
		$todayDate			       =	date('Y-m-d');
		if(!file_exists($path.$todayDate)){
			mkdir($path.$todayDate);
			mkdir($path.$todayDate.'/thumb');
		}
		$path			   	   =	$path.$todayDate.'/';
		$pathThumb		  	  =	$path.'thumb/';
		$time			   	   =	time();
		if($youtube_link !=''){
			$_POST['vi_type']	 	=	2;
			$_POST['vi_url']	  	 =	$youtube_link;
			//$getyoutube			  =	$objCommon->getYoutubeDetails($youtube_link);
			$videoID				 =	$objCommon->getYoutubeId($youtube_link);
			$thumbNail	 	  	   = 	'https://i.ytimg.com/vi/'.$videoID.'/hqdefault.jpg';
			$newYouTubeImgName	   =	$_SESSION['userId'].".jpg";
			file_put_contents ($pathThumb.$newYouTubeImgName,file_get_contents($thumbNail));
			$_POST['vi_thumb']	   =	$todayDate.'/thumb/'.$newYouTubeImgName;
			$objVideoIntro->insert($_POST);
		}else if($_FILES['video_introduction']['tmp_name'] !=''){
			$_POST['vi_type']	 	=	1;
			$valid_formats 		   = 	array("mp4", "mov","mpeg4","avi","wmv","MPEGPS","flv","3gp","WebM","AVI","MP4","FLV");
			$ext 				 	 = 	pathinfo($_FILES['video_introduction']['name'], PATHINFO_EXTENSION);
			if(in_array($ext,$valid_formats))
			{
				$actual_vid_name 	= 	$time."_".$_SESSION['userId'].".".$ext;
				$newVidName		 =	$time."_".$_SESSION['userId'].".mp4";
				//shell_exec('/usr/local/bin/ffmpeg -i '.$_FILES['video_introduction']['tmp_name'].' -strict -2 '.$path.$newVidName);
				//shell_exec('/usr/local/bin/ffmpeg -itsoffset -4  -i '.$_FILES['video_introduction']['tmp_name'].' -vcodec mjpeg -vframes 1 -an -f rawvideo -s 320x240 '.$pathThumb.$time.'_'.$_SESSION['userId'].'.jpg');
				shell_exec('/usr/local/bin/ffmpeg -i '.$_FILES['video_introduction']['tmp_name'].' -strict -2 '.$path.$newVidName);
				shell_exec('/usr/local/bin/ffmpeg -i '.$_FILES['video_introduction']['tmp_name'].' -ss 00:00:02.000 -vframes 1 '.$pathThumb.$time.'_'.$_SESSION['userId'].'.jpg');
				$videofile		  =	$path.$newVidName;
				$xyz 				= 	shell_exec("/usr/local/bin/ffmpeg -i \"{$videofile}\" 2>&1");
				$search			 =	'/Duration: (.*?),/';
				preg_match($search, $xyz, $matches);
				$explode			= 	explode(':', $matches[1]);
				$_POST['vi_url']	=	$todayDate.'/'.$newVidName;
				$_POST['vi_thumb']  =	$todayDate.'/thumb/'.$time.'_'.$_SESSION['userId'].'.jpg';
				$objVideoIntro->insert($_POST);
			}
		}
	}

	mysql_query("COMMIT");
	$objCommon->addMsg('Changes has been updated',1);
	//header("location:".$_SERVER['HTTP_REFERER']);
	}
if($action== 'edit_social_links'){
	mysql_query("START TRANSACTION");
	$_POST['usl_fb']				=	$objCommon->esc($_POST['social_user_fb']);
	$_POST['usl_twitter']		   =	$objCommon->esc($_POST['social_user_twitter']);
	$_POST['usl_instagram']		 =	$objCommon->esc($_POST['social_user_instagram']);
	$_POST['usl_gplus']			 =	$objCommon->esc($_POST['social_user_gplus']);
	$_POST['usl_others']			=	$objCommon->esc($_POST['social_user_others']);
	$objUserSocialLinks->update($_POST,"user_id=".$_SESSION['userId']);
	mysql_query("COMMIT");
	$objCommon->addMsg('Changes has been updated',1);
	//header("location:".$_SERVER['HTTP_REFERER']);
}
//---------------------------------profile complete status----------------------------------------------------------------------------------
$getUserComplete					=	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,personal.p_dob,personal.p_gender,personal.p_phone,personal.p_alt_email,personal.p_about, personal.p_ethnicity,personal.p_languages,personal.n_id,personal.p_country,personal.p_city,profileImg.upi_img_url,social.usl_fb,social.usl_twitter,social.usl_gplus,social.usl_instagram,social.usl_others,ucat.uc_m_id,det.mw_id,det.mt_id,det.ms_id,det.mj_id,det.model_dis_id,det.mhp_id,det.mh_id,det.me_id,det.mcollar_id,det.mc_id,det.mhair_id,album.ai_images,photos.photo_url
		FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
		LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id 
		LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id  
		LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
		LEFT JOIN personal_details as personal ON user.user_id = personal.user_id 
		LEFT JOIN model_details  AS det ON user.user_id = det.user_id
		LEFT JOIN album_images as album ON user.user_id = album.user_id AND album.ai_set_main =1
		LEFT JOIN  user_photos as photos ON user.user_id = photos.user_id AND photos.photo_set_main=1
		WHERE user.status=1 AND user.user_id=".$_SESSION['userId']);
$userMainCat						=	$getUserComplete['uc_m_id'];
$com_first_name					 =	($getUserComplete['first_name'] !='')?1:0;
$com_last_name					  =	($getUserComplete['last_name'] !='')?1:0;
$com_display_name				   =	($getUserComplete['display_name'] !='')?1:0;
if($userMainCat==2){
	$_POST['complete_primary']	  =	($com_first_name * 4)+($com_last_name * 4)+($com_display_name * 4);
}else if($userMainCat==1){
	$_POST['complete_primary']	  =	($com_first_name * 8)+($com_last_name * 8)+($com_display_name * 8);
}
$com_dob					 		=	($getUserComplete['p_dob'] !='0000-00-00' && $getUserComplete['p_dob'] !='' && $getUserComplete['p_dob'] !='NULL')?1:0;
$com_gender					 	 =	($getUserComplete['p_gender'] !='0' && $getUserComplete['p_gender'] !='' && $getUserComplete['p_gender'] !='NULL')?1:0;
$com_phone					 	  =	($getUserComplete['p_phone'] !='' && $getUserComplete['p_phone'] !='NULL')?1:0;
$com_alt_email					  =	($getUserComplete['p_alt_email'] !='' && $getUserComplete['p_alt_email'] !='NULL')?1:0;
$com_about					  	  =	($getUserComplete['p_about'] !='' && $getUserComplete['p_about'] !='NULL')?1:0;
$com_ethnicity					  =	($getUserComplete['p_ethnicity'] !='' && $getUserComplete['p_ethnicity'] !='NULL')?1:0;
$com_languages					  =	($getUserComplete['p_languages'] !='' && $getUserComplete['p_languages'] !='NULL')?1:0;
$com_nationality					=	($getUserComplete['n_id'] !='0' && $getUserComplete['n_id'] !='' && $getUserComplete['n_id'] !='NULL')?1:0;
$com_country					  	=	($getUserComplete['p_country'] !='' && $getUserComplete['p_country'] !='NULL')?1:0;
$com_city					  	   =	($getUserComplete['p_city'] !='' && $getUserComplete['p_city'] !='NULL')?1:0;
if($userMainCat==2){
	$_POST['complete_personal']	 =	($com_dob * 4)+($com_gender * 4)+($com_phone * 4)+($com_alt_email * 4)+($com_about * 4)+($com_ethnicity * 4)+($com_languages * 4)+($com_nationality * 4)+($com_country * 4)+($com_city * 4);
}else if($userMainCat==1){
	$_POST['complete_personal']	 =	($com_dob * 8)+($com_gender * 8)+($com_phone * 4)+($com_alt_email * 4)+($com_about * 4)+($com_ethnicity * 4)+($com_languages * 4)+($com_nationality * 8)+($com_country * 8)+($com_city * 8);
}
$com_profile_img					=	($getUserComplete['upi_img_url'] !='' && $getUserComplete['upi_img_url'] !='NULL')?1:0;
$com_cover_img1					 =	($getUserComplete['ai_images'] !='' && $getUserComplete['ai_images'] !='NULL')?1:0;
$com_cover_img2					 =	($getUserComplete['photo_url'] !='' && $getUserComplete['photo_url'] !='NULL')?1:0;
if($userMainCat==2){
	$_POST['complete_profile_img']  =	($com_profile_img * 4)+($com_cover_img1 * 4)+($com_cover_img2 * 4);
}else if($userMainCat==1){
	$_POST['complete_profile_img']  =	($com_profile_img * 4)+($com_cover_img1 * 4)+($com_cover_img2 * 4);
}
$com_social1					 	=	($getUserComplete['usl_fb'] !='' && $getUserComplete['usl_fb'] !='NULL')?1:0;
$com_social2					 	=	($getUserComplete['usl_twitter'] !='' && $getUserComplete['usl_twitter'] !='NULL')?1:0;
$com_social3					 	=	($getUserComplete['usl_gplus'] !='' && $getUserComplete['usl_gplus'] !='NULL')?1:0;
$com_social4					 	=	($getUserComplete['usl_instagram'] !='' && $getUserComplete['usl_instagram'] !='NULL')?1:0;
$com_social5					 	=	($getUserComplete['usl_others'] !='' && $getUserComplete['usl_others'] !='NULL')?1:0;
$com_social						 =	$com_social1+$com_social2+$com_social3+$com_social4+$com_social5;
if($com_social >=4){
	$_POST['complete_social']  	   =	8;
}else{
	$_POST['complete_social']  	   =	$com_social*2;
}
$com_det1						   =	($getUserComplete['mw_id'] !='0' && $getUserComplete['mw_id'] !='' && $getUserComplete['mw_id'] !='NULL')?1:0;
$com_det2						   =	($getUserComplete['mt_id'] !='0' && $getUserComplete['mt_id'] !='' && $getUserComplete['mt_id'] !='NULL')?1:0;
$com_det3						   =	($getUserComplete['ms_id'] !='0' && $getUserComplete['ms_id'] !='' && $getUserComplete['ms_id'] !='NULL')?1:0;
$com_det4						   =	($getUserComplete['mj_id'] !='0' && $getUserComplete['mj_id'] !='' && $getUserComplete['mj_id'] !='NULL')?1:0;
$com_det5						   =	($getUserComplete['model_dis_id'] !='0' && $getUserComplete['model_dis_id'] !='' && $getUserComplete['model_dis_id'] !='NULL')?1:0;
$com_det6						   =	($getUserComplete['mhp_id'] !='0' && $getUserComplete['mhp_id'] !='' && $getUserComplete['mhp_id'] !='NULL')?1:0;
$com_det7						   =	($getUserComplete['mh_id'] !='0' && $getUserComplete['mh_id'] !='' && $getUserComplete['mh_id'] !='NULL')?1:0;
$com_det8						   =	($getUserComplete['me_id'] !='0' && $getUserComplete['me_id'] !='' && $getUserComplete['me_id'] !='NULL')?1:0;
$com_det9						   =	($getUserComplete['mcollar_id'] !='0' && $getUserComplete['mcollar_id'] !='' && $getUserComplete['mcollar_id'] !='NULL')?1:0;
$com_det10						  =	($getUserComplete['mc_id'] !='0' && $getUserComplete['mc_id'] !='' && $getUserComplete['mc_id'] !='NULL')?1:0;
$com_det11						  =	($getUserComplete['mhair_id'] !='0' && $getUserComplete['mhair_id'] !='' && $getUserComplete['mhair_id'] !='NULL')?1:0;
if($userMainCat==2){
	$com_det						=	$com_det1+$com_det2+$com_det3+$com_det4+$com_det5+$com_det6+$com_det7+$com_det8+$com_det9+$com_det10+$com_det11;
	if($com_det >=8){
		$_POST['complete_public']   =	32;
	}else{
		$_POST['complete_public']   =	$com_det*2;
	}
}
$_POST['complete_created']		  =	date("Y-m-d H:i:s");
$countCompeteProf				   =	$objProfComplete->count("user_id=".$_SESSION['userId']);
if($countCompeteProf >0){
	$objProfComplete->update($_POST,"user_id=".$_SESSION['userId']);
}else{
	$_POST['user_id']			   =	$_SESSION['userId'];
	$objProfComplete->insert($_POST);
}
//-------------------------------------------------------------------------------------------------------------------------------------
header("location:".$_SERVER['HTTP_REFERER']);
exit;
?>