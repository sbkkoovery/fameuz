<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/users.php");
include_once(DIR_ROOT."class/user_chat_status.php");
$objCommon				=	new common();
$objUsers				 =	new users();
$objUserChatStatus   		=	new user_chat_status();
$action				   =	$objCommon->esc($_GET['action']);
if($action== 'login' && isset($_POST['login_email'],$_POST['login_passoword']) && $_POST['login_email']!='' && $_POST['login_passoword']!=''){
	$login_email		  =	$objCommon->esc($_POST['login_email']);
	$login_passoword	  =	$objCommon->esc($_POST['login_passoword']);
	$getLoginRow		  =	$objUsers->getRow("email='".$login_email."' AND password='".md5($login_passoword)."' AND (status=1 OR status=4)");
	if($getLoginRow['user_id']!=''){
		$createDate	   =	$getLoginRow['created_on'];
		$todayDate		=	date("Y-m-d H:i:s");
 		$dateDiff		 =	round(abs(strtotime($todayDate)-strtotime($createDate))/86400);
		if($getLoginRow['email_validation']==1 || $dateDiff <= 7){
			$_SESSION['userId']		=	$getLoginRow['user_id'];
			$_POST['ucs_status']	   =	'1';
			$objUserChatStatus->update($_POST,"user_id=".$_SESSION['userId']);
			if(isset($_SESSION['login_redirect_url'])){
				$login_redirect_url	=	$_SESSION['login_redirect_url'];
				unset($_SESSION['login_redirect_url']);
				header("location:".$login_redirect_url);
				exit;
			}
			header("location:".SITE_ROOT."user/home");
			exit;
		}else{
			$objCommon->addMsg('Sorry..Your account has been expired.please do email confirmation...!',0);
			header("location:".SITE_ROOT."user/login");
			exit;
		}
	}else{
		$objCommon->addMsg('Invalid login credentials...!',0);
		header("location:".SITE_ROOT."user/login");
		exit;
	}
}else{
	$objCommon->addMsg('Please fill the fields...',0);
	header("location:".SITE_ROOT."user/login");
	exit;
}
header("location:".SITE_ROOT."user/login");
exit;
?>