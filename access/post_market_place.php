<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/market_place.php");
include(DIR_ROOT."ajax/resize-class.php");
$objCommon				=	new common();
$objMarketPlace		   =	new market_place();
$action				   =	$objCommon->esc($_GET['action']);
$userId				   =	$_SESSION['userId'];
$valid_formats 			= 	array("jpg", "png", "gif", "bmp","jpeg","JPG", "PNG", "GIF", "BMP","JPEG");
if($action== 'post_market_place' && $userId != ''){
 if(isset($_POST['mp_title'],$_POST['mp_type'],$_POST['mp_locations']) && $_POST['mp_title']!='' && $_POST['mp_type'] !='' && $_POST['mp_locations'] != ''){
		mysql_query("START TRANSACTION");
		$mp_show_from					   =	$objCommon->esc($_POST['mp_show_from']);
		$mp_show_from_expl				  =	explode("/",$mp_show_from);
		$_POST['mp_show_from']			  =	$mp_show_from_expl[2].'-'.$mp_show_from_expl[0].'-'.$mp_show_from_expl['1'];
		$mp_show_to					   	 =	$objCommon->esc($_POST['mp_show_to']);
		$mp_show_to_expl				  	=	explode("/",$mp_show_to);
		$_POST['mp_show_to']				=	$mp_show_to_expl[2].'-'.$mp_show_to_expl[0].'-'.$mp_show_to_expl['1'];
		$_POST['mp_title']		   		  =	$objCommon->esc($_POST['mp_title']);
		$_POST['mp_alias']				  =	$objCommon->getAlias($_POST['mp_title']);
		$_POST['mp_type']	   		   	   =	$objCommon->esc($_POST['mp_type']);
		$_POST['mp_locations']	   		  =	$objCommon->esc($_POST['mp_locations']);
		$_POST['mp_duration']	   		   =	$objCommon->esc($_POST['mp_duration']);
		$_POST['mp_actual_price']	   	   =	$objCommon->esc($_POST['mp_actual_price']);
		if($_POST['mp_actual_price']){
			$_POST['mp_actual_price']	   =	$objCommon->esc($_POST['currency_actual']).'###'.$_POST['mp_actual_price'];
		}
		$_POST['mp_dis_price']	   		  =	$objCommon->esc($_POST['mp_dis_price']);
		if($_POST['mp_dis_price']){
			$_POST['mp_dis_price']	   	  =	$objCommon->esc($_POST['currency_discount']).'###'.$_POST['mp_dis_price'];
		}
		$_POST['mp_description']	   		=	$objCommon->esc($_POST['mp_description']);
		$_POST['mp_show_from']			  =	$objCommon->esc($_POST['mp_show_from']);
		$_POST['mp_show_to']			    =	$objCommon->esc($_POST['mp_show_to']);
		$hid_edit_jobId					 =	$objCommon->esc($_POST['editId']);
		if($hid_edit_jobId){
		$_POST['mp_edited_date']			=	date("Y-m-d H:i:s");
		if($_FILES['mp_image']['tmp_name']){ 
		$path			 =	DIR_ROOT.'uploads/market_place/';
		$todayDate		=	date('Y-m-d');
		if(!file_exists($path.$todayDate)){
			mkdir($path.$todayDate);
		}
		$path	=	$path.$todayDate.'/';
		$name = $_FILES['mp_image']['name'];
		$size = $_FILES['mp_image']['size'];
		if(strlen($name))
		{
			$ext	=	strtolower(pathinfo($name, PATHINFO_EXTENSION));
			if(in_array($ext,$valid_formats))
			{
			if($size<(3072*10240))
			{
			$time						=	time();				
			$actual_image_name_ext_no	=	$time."_".$userId;
			$actual_image_name 		   = 	$time."_".$userId.".".$ext;
			
			if(!file_exists($path."original")){
				mkdir($path."original");
			}
			if(!file_exists($path."thumb")){
				mkdir($path."thumb");
			}
			$tmpName = $_FILES['mp_image']['tmp_name'];    
			list($widthu, $heighut, $typeu, $attru) = getimagesize($tmpName); 
			if(move_uploaded_file($_FILES['mp_image']['tmp_name'], $path.'original/'.$actual_image_name)){
			
			$resizeObj = new resize($path.'original/'.$actual_image_name);
			$resizeObj -> resizeImage(200, 200, 'auto');
			$resizeObj -> saveImage($path.'thumb/'.$actual_image_name, 100);
				if ($heighut > 700 || $widthu >700){
					$resizeObj -> resizeImage(700, 700, 'auto');
					$resizeObj -> saveImage($path.$actual_image_name, 100);
				}else{
					$resizeObj -> resizeImage($widthu, $heighut, 'auto');
					$resizeObj -> saveImage($path.$actual_image_name, 100);
				}
				//----------------start upload to AWS----------------------------------------
				require(DIR_ROOT."amazone_s3/S3.php");
				$s3			=	new S3(AWS_ID,AWS_KEY);
				S3::putObjectFile($path.$actual_image_name,AWS_IMG_BUCKET,'uploads/market_place/'.$todayDate.'/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
				S3::putObjectFile($path.'thumb/'.$actual_image_name,AWS_IMG_BUCKET,'uploads/market_place/'.$todayDate.'/thumb/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
				//----------------end upload to AWS-------------------------------------------
				
			$_POST['mp_image']   		  =	$todayDate."/".$actual_image_name;
			 }
		}
		
		}
		
		}
			
}
			$objMarketPlace->update($_POST,"mp_id=".$hid_edit_jobId);
			$objCommon->addMsg('Your Ad has been updated',1);
			header("location:".SITE_ROOT."user/my-market-place");
		}else{
			$_POST['user_id']				=	$userId;
			$_POST['mp_created_date']		=	date("Y-m-d H:i:s");
			$_POST['mp_edited_date']		 =	date("Y-m-d H:i:s");
			$_POST['mp_status']		      =	1;
			
			if($_FILES['mp_image']['tmp_name']){ 
			$path			 				=	DIR_ROOT.'uploads/market_place/';
			$todayDate					   =	date('Y-m-d');
			if(!file_exists($path.$todayDate)){
				mkdir($path.$todayDate);
			}
			$path							=	$path.$todayDate.'/';
			$name 							= 	$_FILES['mp_image']['name'];
			$size 							= 	$_FILES['mp_image']['size'];
			if(strlen($name))
			{
				$ext	=	strtolower(pathinfo($name, PATHINFO_EXTENSION));
				if(in_array($ext,$valid_formats))
				{
					if($size<(3072*10240))
					{
					$time				  		=	time();				
					$actual_image_name_ext_no	=	$time."_".$userId;
					$actual_image_name 		   = 	$time."_".$userId.".".$ext;
					if(!file_exists($path."original")){
						mkdir($path."original");
					}
					if(!file_exists($path."thumb")){
						mkdir($path."thumb");
					}
					$tmpName 					 = 	$_FILES['mp_image']['tmp_name'];    
					list($widthu, $heighut, $typeu, $attru) = getimagesize($tmpName); 
					if(move_uploaded_file($_FILES['mp_image']['tmp_name'], $path.'original/'.$actual_image_name)){
						$resizeObj = new resize($path.'original/'.$actual_image_name);
						$resizeObj -> resizeImage(200, 200, 'auto');
						$resizeObj -> saveImage($path.'thumb/'.$actual_image_name, 100);
						if ($heighut > 700 || $widthu >700){
							$resizeObj -> resizeImage(700, 700, 'auto');
							$resizeObj -> saveImage($path.$actual_image_name, 100);
						}else{
							$resizeObj -> resizeImage($widthu, $heighut, 'auto');
							$resizeObj -> saveImage($path.$actual_image_name, 100);
						}
						//----------------start upload to AWS----------------------------------------
						require(DIR_ROOT."amazone_s3/S3.php");
						$s3			=	new S3(AWS_ID,AWS_KEY);
						S3::putObjectFile($path.$actual_image_name,AWS_IMG_BUCKET,'uploads/market_place/'.$todayDate.'/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
						S3::putObjectFile($path.'thumb/'.$actual_image_name,AWS_IMG_BUCKET,'uploads/market_place/'.$todayDate.'/thumb/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
						//----------------end upload to AWS-------------------------------------------
						$_POST['mp_image']   		  =	$todayDate."/".$actual_image_name;
					}
				}
			}
		}
	}
	$objMarketPlace->insert($_POST);
 		}
		mysql_query("COMMIT");
		$objCommon->addMsg('Your Ad has been posted',1);
		header("location:".SITE_ROOT."user/my-market-place");
		exit;
	}else{
		$objCommon->addMsg('Please fill the fields...',0);
		header("location:".$_SERVER['HTTP_REFERER']);
	}
}
?>