<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/email_message.php");
include_once(DIR_ROOT."class/email_message_users.php");
include_once(DIR_ROOT."class/users.php");
$objEmailMsg					=	new email_message();
$objEmailMsgUser				=	new email_message_users();
$objUsers					=	new users();
$userId				   		 =	$_SESSION['userId'];
$objCommon					  =	new common();
if($userId != '' && $_POST['replyof'] !='' && $_POST['replyto'] != '' && $_POST['email_message'] != ''){
	$replyof					=	$objCommon->esc($_POST['replyof']);
	$replyto					=	$objCommon->esc($_POST['replyto']);
	$_POST['em_message']	 	=	$objCommon->esc($_POST['email_message']);
	
	$getMesgDetails  		     =	$objEmailMsg->getRowSql("SELECT msg_user.emu_from,msg_user.emu_to,msg.em_subject
											  FROM email_message AS msg
											  LEFT JOIN email_message_users AS msg_user ON msg.em_id = msg_user.em_id
											  LEFT JOIN email_message_attachments AS msg_attach	ON msg.em_id = msg_attach.em_id
											   WHERE ((msg_user.emu_to=".$userId." AND msg_user.emu_from=".$replyto.") OR (msg_user.emu_from=".$userId." AND msg_user.emu_to=".$replyto."))  AND msg.em_id =".$replyof);
	$_POST['emu_from']		  =	$userId;
	$_POST['emu_to']		    =	($getMesgDetails['emu_from']==$userId)?$getMesgDetails['emu_to']:$getMesgDetails['emu_from'];
	$_POST['em_replyof']		=	$replyof;	
	$_POST['em_subject']		=	$getMesgDetails['em_subject'];
	$_POST['em_category']	   =	'contact_message';
	$_POST['em_createdon']	  =	date("Y-m-d H:i:s");
	$_POST['em_status']		 =	1;
	$_POST['emu_read_status']   =	0;
	$_POST['emu_delete_from']   =	1;
	$_POST['emu_delete_to']	 =	1;
	$objEmailMsg->insert($_POST);
	$lastMsgId				  =	$objEmailMsg->insertId();
	$_POST['em_id']			 =	$lastMsgId;
	
	$myDetails				   		   	   =	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$userId);
	$displayName						  	 =	$objCommon->displayName($myDetails);
	$friend_id                               =    $_POST['emu_to'];
	$notiType								=	'messages';
	$notiImg								 =	'';
	$notiDescr   	 	 					   =	'<b>'.$displayName.'</a></b> has sent a new mail for you</b>.';
	$notiUrl  								 =	SITE_ROOT.'user/my-messages';
		
	$objCommon->pushNotification($friend_id,$userId,$notiType,$notiImg,$notiDescr,$notiUrl);
	
	
	$objEmailMsgUser->insert($_POST);
	$objCommon->addMsg('Message has been sent',1);
}
header("location:".$_SERVER['HTTP_REFERER']);
exit;
?>