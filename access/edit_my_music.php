<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/music.php");
include(DIR_ROOT."ajax/resize-class.php");
$objCommon				   =	new common();
$objMusic			   	   	=	new music();
$userId					  =	$_SESSION['userId'];
//----------------start upload to AWS----------------------------------------
require(DIR_ROOT."amazone_s3/S3.php");
$s3			=	new S3(AWS_ID,AWS_KEY);
//----------------end upload to AWS-------------------------------------------
$valid_formats 			= 	array("jpg", "png", "gif", "bmp","jpeg","JPG", "PNG", "GIF", "BMP","JPEG");
if($_POST['editMusicId'] !='' && $userId !=''){
	mysql_query("START TRANSACTION");
	$hid_img_id			  =	$_POST['editMusicId'];
	$img_title	 		   =	$_POST['music_title'];
	$ai_caption	  		  =	$objCommon->esc($img_title);
	$img_descr			   =	$objCommon->esc($_POST['music_descr']);
	$privacy				 =	$objCommon->esc($_POST['privacy']);
	$video_tags			  =	$objCommon->esc($_POST['music_tags']);
	$music_artist			=	$objCommon->esc($_POST['music_artist']);
	$music_status			=	$objCommon->esc($_POST['music_status']);
	$music_edited		    =	date("Y-m-d H:i:s");
	if($privacy ==1){
		$video_privacy	   =	'1,0,0,0';
	}else if($privacy ==2){
		$video_privacy	   =	'0,1,0,0';
	}else if($privacy ==4){
		$video_privacy	   =	'0,0,0,1';
	}
	if($_FILES['music_thumb']['tmp_name']){ 
			$path			 				=	DIR_ROOT.'uploads/music/';
			$todayDate					   =	date('Y-m-d');
			if(!file_exists($path.$todayDate)){
				mkdir($path.$todayDate);
			}
			$path							=	$path.$todayDate.'/';
			$name 							= 	$_FILES['music_thumb']['name'];
			$size 							= 	$_FILES['music_thumb']['size'];
			if(strlen($name))
			{
				$ext	=	strtolower(pathinfo($name, PATHINFO_EXTENSION));
				if(in_array($ext,$valid_formats))
				{
					if($size<(3072*10240))
					{
					$time				  		=	time();				
					$actual_image_name_ext_no	=	$time."_".$userId;
					$actual_image_name 		   = 	$time."_".$userId.".".$ext;
					if(!file_exists($path."original")){
						mkdir($path."original");
					}
					if(!file_exists($path."thumb")){
						mkdir($path."thumb");
					}
					$tmpName 					 = 	$_FILES['music_thumb']['tmp_name'];    
					list($widthu, $heighut, $typeu, $attru) = getimagesize($tmpName); 
					if(move_uploaded_file($_FILES['music_thumb']['tmp_name'], $path.'original/'.$actual_image_name)){
						$resizeObj = new resize($path.'original/'.$actual_image_name);
				if ($heighut > 250 || $widthu >180){
					$resizeObj -> resizeImage(250, 180, 'auto');
					$resizeObj -> saveImage($path.'thumb/'.$actual_image_name, 100);
				}else{
					$resizeObj -> resizeImage($widthu, $heighut, 'auto');
					$resizeObj -> saveImage($path.'thumb/'.$actual_image_name, 100);
				}
					S3::putObjectFile($path.'thumb/'.$actual_image_name,AWS_MUSIC_BUCKET,'uploads/music/'.$todayDate.'/thumb/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
						$music_thumb   		  =	$todayDate."/thumb/".$actual_image_name;
					}
				}

			}

		}
	$objMusic->updateField(array('music_thumb'=>$music_thumb),"music_id=".$hid_img_id);
	}
	if($_FILES['file']['tmp_name'] !=''){
		$path			 				=	DIR_ROOT.'uploads/music/';
		$todayDate			   =	date('Y-m-d');
		if(!file_exists($path.$todayDate)){
			mkdir($path.$todayDate);
			mkdir($path.$todayDate.'/thumb');
		}
		$path			   	   =	$path.$todayDate.'/';
		$pathThumb		  	  =	$path.'thumb/';
		$time			   	   =	time();
		$valid_formats 		   = 	array("mp3","wma","aac","MP3","WMA","AAC");
		$ext 				 	 = 	pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
		if(in_array($ext,$valid_formats))
		{
			$actual_music_name   = 	$time."_".$userId.".".$ext;
			if(move_uploaded_file($_FILES['file']['tmp_name'], $path.$actual_music_name)){
				S3::putObjectFile($path.$actual_music_name,AWS_MUSIC_BUCKET,'uploads/music/'.$todayDate.'/'.$actual_music_name,S3::ACL_PUBLIC_READ,array());
			}
			$music_url		   =	$todayDate.'/'.$actual_music_name;
			$uploadStatus	    =	1;
		}
		$objMusic->updateField(array('music_url'=>$music_url),"music_id=".$hid_img_id);
	}
	$objMusic->updateField(array('music_title'=>$ai_caption,'music_artist'=>$music_artist,'music_descr'=>$img_descr,'music_privacy'=>$video_privacy,'music_tags'=>$video_tags,'music_status'=>$music_status,'music_edited'=>$music_edited),"music_id=".$hid_img_id);
	mysql_query("COMMIT");
	$objCommon->addMsg('Your music has been updated',1);
}
header("location:".SITE_ROOT."user/my-music");
?>