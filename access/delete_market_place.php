<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/market_place.php");
$objCommon				=	new common();
$objMarketplace		   =	new market_place();
$action				   =	$objCommon->esc($_GET['action']);
$userId				   =	$_SESSION['userId'];
$jobId				   	=	$objCommon->esc($_GET['jobId']);
if($action== 'del_job' && $userId != '' && $jobId !=''){
	$getMarketDetails	 =	$objMarketplace->getRow("mp_id=".$jobId);
	if($getMarketDetails['mp_image']){
		$explAiImages	   		=	explode("/",$getMarketDetails['mp_image']);
		$getAiImag1		  	  =	$explAiImages[0].'/thumb/'.$explAiImages[1];
		$getAiImag2		  	  =	$explAiImages[0].'/original/'.$explAiImages[1];
		$imgPath				 =	DIR_ROOT.'uploads/market_place/'.$getMarketDetails['mp_image'];
		$imgPath1				=	DIR_ROOT.'uploads/market_place/'.$getAiImag1;
		$imgPath2				=	DIR_ROOT.'uploads/market_place/'.$getAiImag2;
		unlink($imgPath);
		unlink($imgPath1);
		unlink($imgPath2);
	}
	$objMarketplace->delete("mp_id=".$jobId);;
	$objCommon->addMsg('Selected item has been deleted...',1);
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}else{
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}
?>