<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/jobs.php");
$objCommon				=	new common();
$objJobs			 	  =	new jobs();
$action				   =	$objCommon->esc($_GET['action']);
$userId				   =	$_SESSION['userId'];
$jobId				   	=	$objCommon->esc($_GET['jobId']);
if($action== 'del_job' && $userId != '' && $jobId !=''){
	$objJobs->updateField(array("job_status"=>'0'),"job_id=".$jobId);
	$objCommon->addMsg('Selected item has been deleted...',1);
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}else{
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}
?>