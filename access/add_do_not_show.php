<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/do_not_show.php");
$objCommon				=	new common();
$obj_do_not_show		  =	new do_not_show();
$action				   =	$objCommon->esc($_GET['action']);
$userId				   =	$_SESSION['userId'];
$dns_type				 =	$objCommon->esc($_POST['dns_type']);
if($dns_type != '' && $userId != ''){
	$_POST['dns_type']	=	$dns_type;
	$_POST['user_id']	 =	$userId;
	$obj_do_not_show->insert($_POST);
}
?>