<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/book_model.php");
$objCommon				=	new common();
$objBookModel			 =	new book_model();
$action				   =	$objCommon->esc($_GET['action']);
$userId				   =	$_SESSION['userId'];
$bookId				   =	$objCommon->esc($_GET['bookId']);
if($action== 'del_book' && $userId != '' && $bookId !=''){
	$objBookModel->updateField(array("bm_model_status"=>'3'),"bm_id=".$bookId);
	$objCommon->addMsg('Selected item has been deleted...',1);
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}else{
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}
?>