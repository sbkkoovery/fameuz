<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/comments.php");
include_once(DIR_ROOT."class/notifications.php");
$objCommon				=	new common();
$objComments			  =	new comments();
$delid				   	=	$objCommon->esc($_POST['delid']);
$userId				   =	$_SESSION['userId'];
if($delid != '' && $userId != ''){
	$getCommentDetails	=	$objComments->getRowSql("select comment_id,comment_cat,comment_content from comments where comment_id=".$delid." and comment_user_by=".$userId);
	if($getCommentDetails['comment_id']){
		$commentCat		   =	$getCommentDetails['comment_cat'];
		$commentContent	   =	$getCommentDetails['comment_content'];
		if($commentCat==1){
			$objComments->build_result_insert("update user_photos SET photo_comment_count=photo_comment_count-1 where photo_id=".$commentContent);
		}else if($commentCat==2){
			$objComments->build_result_insert("update album_images SET ai_comment_count=ai_comment_count-1 where ai_id=".$commentContent);
		}
		$getreplyIds		=	$objComments->getRowSql("select group_concat(comment_id) as replyIds from comments where comment_reply_id=".$delid);
		$objComments->delete("comment_id=".$delid);
		if($getreplyIds['replyIds']){
			$objComments->delete("comment_id IN (".$getreplyIds['replyIds'].")");
		}
	}
}
?>