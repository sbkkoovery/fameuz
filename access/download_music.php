<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
$objCommon				   =	new common();
$userId					 =	$_SESSION['userId'];
if(isset($_REQUEST['file'],$userId) && $_REQUEST['file']!="" && $userId != '') {
	$file = $_REQUEST['file']; 
	header("Content-type: application/x-file-to-save"); 
	header("Content-Disposition: attachment; filename=".basename($file)); 
	readfile($file); 
}else{
	header("location:".SITE_ROOT."music");
	exit;
}
?>