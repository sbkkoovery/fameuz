<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/users.php");
include_once(DIR_ROOT."class/user_categories.php");
include_once(DIR_ROOT."class/user_verification.php");
include_once(DIR_ROOT."class/user_social_links.php");
include_once(DIR_ROOT."class/personal_details.php");
include_once(DIR_ROOT."class/user_privacy.php");
require_once(DIR_ROOT."PHPMailer/class.phpmailer.php");
include_once(DIR_ROOT."class/user_chat_status.php");
include_once(DIR_ROOT."class/model_details.php");
include_once(DIR_ROOT."class/user_profile_complete.php");
$objCommon				=	new common();
$objUsers				 =	new users();
$objUserCategories		=	new user_categories();
$objUserVerification	  =	new user_verification();
$objUserSocialLinks	   =	new user_social_links();
$objPersonalDetails	   =	new personal_details();
$objUserPrivacy		   =	new user_privacy();
$mail             		 = 	new PHPMailer();
$objUserChatStatus   		=	new user_chat_status();
$objModelDetails		  =	new model_details();
$objProfileComplete	   =	new user_profile_complete();
$action				   =	$objCommon->esc($_GET['action']);
if($action== 'add' && isset($_POST['reg_main_cat'],$_POST['reg_cat'],$_POST['reg_email'],$_POST['reg_pwd']) && $_POST['reg_main_cat']!='' && $_POST['reg_cat']!='' && $_POST['reg_email']!='' && $_POST['reg_pwd']!=''){
	$getOldUsers						  =	$objUsers->getRow("email='".$_POST['reg_email']."'");
	if($getOldUsers['user_id'] ==''){
		if (!filter_var($_POST['reg_email'], FILTER_VALIDATE_EMAIL)) {
		 $objCommon->addMsg('Invalid Email Format...',0);
		 header("location:".$_SERVER['HTTP_REFERER']); 
		 exit;
		}
	mysql_query("START TRANSACTION");
	$_POST['uc_m_id']					 =	$objCommon->esc($_POST['reg_main_cat']);
	$_POST['uc_c_id']					 =	$objCommon->esc($_POST['reg_cat']);
	$my_email							 =	$_POST['email']					   =	$objCommon->esc($_POST['reg_email']);
	$_POST['password']					=	md5($objCommon->esc($_POST['reg_pwd']));
	$_POST['status']					  =	'1';
	$_POST['email_validation']			=	'0';
	$_POST['login_type']				  =	'1';
	$_POST['ucs_status']				  =	1;
	$_POST['created_on']				  =	date("Y-m-d H:i:s"); 
	$_POST['edited_on']				   =	date("Y-m-d H:i:s");
	$objUsers->insert($_POST);
	$_POST['user_id'] = $my_user_id	   =	$objUsers->insertId();
	$objUserCategories->insert($_POST);
	$uv_verification					  =	$_POST['uv_verification']			 =	$objCommon->randStrGen(15);
	$objUserVerification->insert($_POST);
	$explMyEmail						  =	explode("@",$my_email);
	$usl_fameuz			 			   =	$objCommon->getAlias($explMyEmail[0]);
	$getOlduslLink						=	$objUserSocialLinks->getRow("usl_fameuz='".$usl_fameuz."'");
	if($getOlduslLink['usl_id']){
		$_POST['usl_fameuz']			  =	$usl_fameuz.'-'.$objCommon->randStrGenDigit(3).'-'.'1001';
	}else{
		$_POST['usl_fameuz']			  =	$usl_fameuz.'-'.'1001';
	}
	$objUserSocialLinks->insert($_POST);
	$objPersonalDetails->insert($_POST);
	$objUserPrivacy->insert($_POST);
	$objUserChatStatus->insert($_POST);
	$objModelDetails->insert($_POST);
	$objProfileComplete->insert($_POST);
	mysql_query("INSERT INTO privacy_settings(ps_id,user_id,ps_value) VALUES (NULL, '".$my_user_id."', 'a:9:{s:11:\"profile_see\";a:1:{i:0;s:1:\"1\";}s:12:\"personal_see\";a:1:{i:0;s:1:\"1\";}s:10:\"photos_see\";a:1:{i:0;s:1:\"1\";}s:10:\"review_see\";a:1:{i:0;s:1:\"1\";}s:10:\"worked_see\";a:1:{i:0;s:1:\"1\";}s:10:\"disply_see\";a:12:{i:0;s:8:\"language\";i:1;s:8:\"location\";i:2;s:6:\"gender\";i:3;s:9:\"ethnicity\";i:4;s:6:\"height\";i:5;s:9:\"eye_color\";i:6;s:4:\"hair\";i:7;s:5:\"chest\";i:8;s:5:\"waist\";i:9;s:4:\"hips\";i:10;s:5:\"dress\";i:11;s:4:\"shoe\";}s:7:\"book_me\";a:1:{i:0;s:1:\"1\";}s:18:\"email_notification\";a:4:{i:0;s:9:\"booked_me\";i:1;s:9:\"worked_me\";i:2;s:13:\"login_attempt\";i:3;s:16:\"recover_password\";}s:21:\"profile_status_select\";a:1:{i:0;s:5:\"daily\";}}')");
	//---------------------------Email Part-----------------------------------------------------------------------
	$body	='<table border="0" cellspacing="0" cellpadding="0" style="max-width:600px;min-width:320px;border:1px solid #ededed" align="center">
  <tbody><tr>
    <td height="35"></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <td width="23"></td>
          <td><table width="227" border="0" cellspacing="0" cellpadding="0" align="left">
              <tbody><tr>
                <td align="left" style="font-family:Verdana,Arial;line-height:19px;padding-top:8px"><a href="#" title="fameuz.com" style="text-decoration:none" target="_blank"><img src="'.SITE_ROOT.'images/logo.png" alt="fameuz.com" width="181" height="81" border="0" align="absmiddle"></a></td>
              </tr>
            </tbody></table>
            <table border="0" cellspacing="0" cellpadding="0" align="left">
              <tbody><tr>
                <td width="290" valign="bottom" align="right">&nbsp;</td>
              </tr>
            </tbody></table></td>
          <td width="29"></td>
        </tr>
      </tbody></table></td>
  </tr>
  <tr>
    <td height="30"></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#4E4F53">
        <tbody><tr>
          <td width="7" height="109" bgcolor="#4E4F53"></td>
          <td width="36"></td>
          <td><font face="Trebuchet MS, Arial, Helvetica, sans-serif" color="#FFFFFF" style="font-size:34px">Welcome Fameuz</font></td>
          <td width="158" valign="top">&nbsp;</td>
          <td bgcolor="#FFFFFF" width="7"></td>
        </tr>
      </tbody></table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <td width="16" valign="top">&nbsp;</td>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #d5d5d5;border-top:0px;font-family:Tahoma,Arial,sans-serif;font-size:14px;color:#474747;line-height:21px" bgcolor="#ededed">
              <tbody><tr>
                <td height="26" width="36"></td>
                <td></td>
                <td width="40"></td>
              </tr>
              <tr>
                <td></td>
                <td width="492"><font style="font-size:15px"><strong>Dear '.$my_email.' ,</strong></font><br>
                  <br>
                  Thank you for being a part of Fameuz<span style="font-size:1px"> </span>.com. You are now part of the fastest growing Professional Social Network in the World! </td>
                <td></td>
              </tr>
              <tr>
                <td height="18"></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td height="18"></td>
                <td><strong>The account details are:</strong></td>
                <td></td>
              </tr>
              <tr>
                <td height="5"></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td><table border="0" align="left" cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffffff" style="font-family:Tahoma,Arial,sans-serif;font-size:14px;color:#474747;border:1px solid #dbdfdb">
                    <tbody><tr>
                      <td height="12" colspan="5"></td>
                    </tr>
                    <tr>
                      <td width="21" height="25"></td>
                      <td width="73">Username</td>
                      <td width="35">:</td>
                      <td width="346"><a href="mailto:'.$my_email.'" target="_blank">'.$my_email.'</a></td>
                      <td width="17"></td>
                    </tr>
                    <tr>
                      <td height="10"></td>
                      <td colspan="3"></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td height="30"></td>
                      <td colspan="3"><table border="0" cellspacing="0" cellpadding="0" align="left">
                          <tbody><tr>
                                                        <td width="183" bgcolor="#afb420" height="34" align="center" style="font-family:Tahoma,Arial,sans-serif;font-size:15px;"><a href="'.SITE_ROOT.'user/success-email/'.$uv_verification.'_fameuz'.$my_user_id.'" style="text-decoration:none;font-size:15px;color:#ffffff;line-height:34px;min-height:34px;display:block" target="_blank">Verify your Account Now</a></td>
                                                        <td width="47"></td>
                            <td width="220" align="right">&nbsp;</td>
                          </tr>
                        </tbody></table></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td height="12" colspan="5"></td>
                    </tr>
                  </tbody></table></td>
                <td></td>
              </tr>
              <tr>
                <td height="30"></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td height="23"></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td width="492">For any queries send us an email at <a href="mailto:test@fameuz.com" style="font-size:14px;text-decoration:underline;color:#1872b8" target="_blank">test@fameuz.com</a></td>
                <td></td>
              </tr>
              <tr>
                <td height="32"></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td>Regards,<br>
                  <a href="http://www.fameuz.com/" style="text-decoration:none" target="_blank"> <font color="#25a8e0">fameuz</font><font color="#116db6">.com</font> </a>team </td>
                <td></td>
              </tr>
              <tr>
                <td height="25"></td>
                <td></td>
                <td></td>
              </tr>
            </tbody></table></td>
          <td width="16" valign="top">&nbsp;</td>
        </tr>
      </tbody></table></td>
  </tr>
  <tr>
    <td><br /></td>
  </tr>
</tbody></table>';
	$mail->AddReplyTo("info@fameuz.com","Fameuz.com");
	$mail->SetFrom('info@fameuz.com', 'Fameuz.com');
	$mail->AddReplyTo("info@fameuz.com","Fameuz.com");
	$mail->AddAddress($my_email,$my_email);
	$mail->Subject    = "Welcome to fameuz.com";
	$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; 
	$mail->MsgHTML($body);
	$mail->Send();	
	$_SESSION['userId']	= $my_user_id;
//--------------------------------------------------------------------------------------------------------------------
if($_POST['other_prof']){
	include_once(DIR_ROOT."class/other_categories.php");
	$objOtherCat					=	new other_categories();
	$_POST['user_id']	 		   =	$my_user_id;
	$_POST['other_name']			=	$objCommon->esc($_POST['other_prof']);
	$_POST['other_parent']		  =	$_POST['uc_m_id'];
	$_POST['other_status']		  =	0;
	$_POST['other_created_on']	  =	date("Y-m-d H:i:s");
	$objOtherCat->insert($_POST);	
}
	mysql_query("COMMIT");
	header("location:".SITE_ROOT."user/register-page");
	}else{
		$objCommon->addMsg('You have already registerd...',0);
		header("location:".$_SERVER['HTTP_REFERER']);
	}
}else{
	$objCommon->addMsg('Please fill the fields...',0);
	header("location:".$_SERVER['HTTP_REFERER']);
}
?>