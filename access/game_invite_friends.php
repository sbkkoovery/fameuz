<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/invite_games.php");
include_once(DIR_ROOT."class/message_attachement.php");
include(DIR_ROOT."ajax/resize-class.php");
$objCommon						 =	new common();
$objInviteGames					=	new invite_games();
$userId							=	$_SESSION['userId'];
$inviteFrd						 =	$_POST['inviteFrd'];
if($userId != '' && count($inviteFrd)>0){
	mysql_query("START TRANSACTION");
	$_POST['ig_from']			 =	$userId;
	$_POST['ig_created']	  	  =	date("Y-m-d H:i:s");
	$_POST['ig_noti_status']	  =	0;
	$_POST['g_id']				=	$objCommon->esc($_POST['g_id']);
	foreach($inviteFrd as $allInviteFrd){
		$_POST['ig_to']		   =	$allInviteFrd;
		$objInviteGames->insert($_POST);
	}
	mysql_query("COMMIT");
}
?>