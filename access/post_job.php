<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/jobs.php");
include(DIR_ROOT."ajax/resize-class.php");
$objCommon				=	new common();
$objJobs			 	  =	new jobs();
$action				   =	$objCommon->esc($_GET['action']);
$userId				   =	$_SESSION['userId'];
$valid_formats 			= 	array("jpg", "png", "gif", "bmp","jpeg","JPG", "PNG", "GIF", "BMP","JPEG");
//----------------start upload to AWS----------------------------------------
require(DIR_ROOT."amazone_s3/S3.php");
$s3			=	new S3(AWS_ID,AWS_KEY);
//----------------end upload to AWS-------------------------------------------
if($action== 'post_job' && $userId != ''){
 if(isset($_POST['job_start'],$_POST['job_end'],$_POST['job_title'],$_POST['job_country'],$_POST['job_state']) && $_POST['job_start']!='' && $_POST['job_end'] !='' && $_POST['job_title'] != '' && $_POST['job_country'] !='' && $_POST['job_state'] != ''){
		mysql_query("START TRANSACTION");
		$job_start					 	  =	$objCommon->esc($_POST['job_start']);
		$job_start_expl					 =	explode("/",$job_start);
		$_POST['job_start']				 =	$job_start_expl[2].'-'.$job_start_expl[0].'-'.$job_start_expl['1'];
		$job_end					   		=	$objCommon->esc($_POST['job_end']);
		$job_end_expl				  	   =	explode("/",$job_end);
		$_POST['job_end']				   =	$job_end_expl[2].'-'.$job_end_expl[0].'-'.$job_end_expl['1'];
		$_POST['job_title']		   		 =	$objCommon->esc($_POST['job_title']);
		$_POST['job_country']	   		   =	$objCommon->esc($_POST['job_country']);
		$_POST['job_state']	   		     =	$objCommon->esc($_POST['job_state']);
		$_POST['job_city']	   			  =	$objCommon->esc($_POST['job_city']);
		$_POST['job_company']	   		   =	$objCommon->esc($_POST['job_company']);
		$_POST['job_skill']	   		   	 =	$objCommon->esc($_POST['job_skill']);
		$job_currency					   =	$objCommon->esc($_POST['job_currency']);
		$jobPayment						 =	$objCommon->esc($_POST['job_payment']);
		$_POST['job_payment']	   		   =	$job_currency.'###'.$jobPayment;
		$job_category	   		  		   =	$_POST['job_category'];
		if(count($job_category)>0){
			$_POST['job_category']		  =	",".implode(",",$job_category).",";
		}
		$_POST['job_descr']	   		  	 =	$objCommon->esc($_POST['job_descr']);
		$hid_edit_jobId					 =	$objCommon->esc($_POST['editId']);
		if($hid_edit_jobId){
			if($_FILES['job_image']['tmp_name']){ 
		$path			 =	DIR_ROOT.'uploads/jobs/';
		$todayDate		=	date('Y-m-d');
		if(!file_exists($path.$todayDate)){
			mkdir($path.$todayDate);
		}
		$path	=	$path.$todayDate.'/';
		$name = $_FILES['job_image']['name'];
		$size = $_FILES['job_image']['size'];
		if(strlen($name))
		{
			$ext	=	strtolower(pathinfo($name, PATHINFO_EXTENSION));
			if(in_array($ext,$valid_formats))
			{
			if($size<(3072*10240))
			{
			$time						=	time();				
			$actual_image_name_ext_no	=	$time."_".$userId;
			$actual_image_name 		   = 	$time."_".$userId.".".$ext;
			
			if(!file_exists($path."original")){
				mkdir($path."original");
			}
			if(!file_exists($path."thumb")){
				mkdir($path."thumb");
			}
			$tmpName = $_FILES['job_image']['tmp_name'];    
			list($widthu, $heighut, $typeu, $attru) = getimagesize($tmpName); 
			if(move_uploaded_file($_FILES['job_image']['tmp_name'], $path.'original/'.$actual_image_name)){
			
			$resizeObj = new resize($path.'original/'.$actual_image_name);
			$resizeObj -> resizeImage(200, 200, 'auto');
			$resizeObj -> saveImage($path.'thumb/'.$actual_image_name, 100);
				if ($heighut > 700 || $widthu >700){
					$resizeObj -> resizeImage(700, 700, 'auto');
					$resizeObj -> saveImage($path.$actual_image_name, 100);
				}else{
					$resizeObj -> resizeImage($widthu, $heighut, 'auto');
					$resizeObj -> saveImage($path.$actual_image_name, 100);
				}
			S3::putObjectFile($path.$actual_image_name,AWS_IMG_BUCKET,'uploads/jobs/'.$todayDate.'/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
			S3::putObjectFile($path.'thumb/'.$actual_image_name,AWS_IMG_BUCKET,'uploads/jobs/'.$todayDate.'/thumb/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
			$_POST['job_image']   		  =	$todayDate."/".$actual_image_name;
			 }
		}
		
		}
		
		}
			
}
			$_POST['job_edited']			 =	date("Y-m-d H:i:s");
			$objJobs->update($_POST,"job_id=".$hid_edit_jobId);
			mysql_query("COMMIT");
			$objCommon->addMsg('Your job has been updated',1);
			header("location:".SITE_ROOT."user/my-jobs");
			exit;
		}else{
			$_POST['user_id']				=	$userId;
			$_POST['job_created']			=	date("Y-m-d H:i:s");
			$_POST['job_edited']			 =	date("Y-m-d H:i:s");
			$_POST['job_status']		     =	1;
				if($_FILES['job_image']['tmp_name']){ 
			$path			 				=	DIR_ROOT.'uploads/jobs/';
			$todayDate					   =	date('Y-m-d');
			if(!file_exists($path.$todayDate)){
				mkdir($path.$todayDate);
			}
			$path							=	$path.$todayDate.'/';
			$name 							= 	$_FILES['job_image']['name'];
			$size 							= 	$_FILES['job_image']['size'];
			if(strlen($name))
			{
				$ext	=	strtolower(pathinfo($name, PATHINFO_EXTENSION));
				if(in_array($ext,$valid_formats))
				{
					if($size<(3072*10240))
					{
					$time				  		=	time();				
					$actual_image_name_ext_no	=	$time."_".$userId;
					$actual_image_name 		   = 	$time."_".$userId.".".$ext;
					if(!file_exists($path."original")){
						mkdir($path."original");
					}
					if(!file_exists($path."thumb")){
						mkdir($path."thumb");
					}
					$tmpName 					 = 	$_FILES['job_image']['tmp_name'];    
					list($widthu, $heighut, $typeu, $attru) = getimagesize($tmpName); 
					if(move_uploaded_file($_FILES['job_image']['tmp_name'], $path.'original/'.$actual_image_name)){
						$resizeObj = new resize($path.'original/'.$actual_image_name);
						$resizeObj -> resizeImage(200, 200, 'auto');
						$resizeObj -> saveImage($path.'thumb/'.$actual_image_name, 100);
						if ($heighut > 700 || $widthu >700){
							$resizeObj -> resizeImage(700, 700, 'auto');
							$resizeObj -> saveImage($path.$actual_image_name, 100);
						}else{
							$resizeObj -> resizeImage($widthu, $heighut, 'auto');
							$resizeObj -> saveImage($path.$actual_image_name, 100);
						}
						S3::putObjectFile($path.$actual_image_name,AWS_IMG_BUCKET,'uploads/jobs/'.$todayDate.'/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
						S3::putObjectFile($path.'thumb/'.$actual_image_name,AWS_IMG_BUCKET,'uploads/jobs/'.$todayDate.'/thumb/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
						$_POST['job_image']   		  =	$todayDate."/".$actual_image_name;
					}
				}

			}

		}
	
	}
			$objJobs->insert($_POST);
 		}
		mysql_query("COMMIT");
		$objCommon->addMsg('Your job has been posted',1);
		header("location:".SITE_ROOT."user/my-jobs");
		exit;
	}else{
		$objCommon->addMsg('Please fill the fields...',0);
		header("location:".$_SERVER['HTTP_REFERER']);
	}
}
?>