<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/videos.php");
$objCommon				   =	new common();
$path 						= 	$_GET['path'];
$userId					  =	$_SESSION['userId'];
$objVideos			   	   =	new videos();
$youtube_link				=	$objCommon->esc($_POST['youtube_link']);
$_POST['user_id']			=	$userId;
$_POST['video_status']	   =	1;
$_POST['video_created']	  =	date("Y-m-d H:i:s");
$_POST['video_updated']	  =	date("Y-m-d H:i:s");
$_POST['video_visits']	   =	0;
$_POST['video_likes']		=	0;
$uploadStatus				=	0;
mysql_query("START TRANSACTION");
$todayDate			   =	date('Y-m-d');
if(!file_exists($path.$todayDate)){
	mkdir($path.$todayDate);
	mkdir($path.$todayDate.'/thumb');
}
$path			   	   =	$path.$todayDate.'/';
$pathThumb		  	  =	$path.'thumb/';
$time			   	   =	time();
if($youtube_link !=''){
	$_POST['video_type']	 =	2;
	$_POST['video_url']	  =	$youtube_link;
	$getyoutube			  =	$objCommon->getYoutubeDetails($youtube_link);
	$_POST['video_duration'] =	$getyoutube['duration'];
	$newYouTubeImgName	   =	$time."_".$userId.".jpg";
	file_put_contents ($pathThumb.$newYouTubeImgName,file_get_contents($getyoutube['thumbnail']));
		//----------------start upload to AWS----------------------------------------
	require(DIR_ROOT."amazone_s3/S3.php");
	$s3			=	new S3(AWS_ID,AWS_KEY);
	S3::putObjectFile($pathThumb.$newYouTubeImgName,AWS_VIDEO_BUCKET,'uploads/videos/'.$todayDate.'/thumb/'.$newYouTubeImgName,S3::ACL_PUBLIC_READ,array());
	//----------------end upload to AWS-------------------------------------------
	$_POST['video_thumb']	=	$todayDate.'/thumb/'.$newYouTubeImgName;
	$objVideos->insert($_POST);
	$uploadStatus			=	1;
	$lastAlbImgId			=	$objVideos->insertId();
	$video_encr_id		   =	$objCommon->randStrGenSpecial(10).'-'.$lastAlbImgId.$objCommon->randStrGenSpecial(4);
	$objVideos->updateField(array("video_encr_id"=>$video_encr_id),"video_id=".$lastAlbImgId);
}else if($_FILES['file']['tmp_name'] !=''){
	$_POST['video_type']	 =	1;
	$valid_formats 		   = 	array("mp4", "mov","mpeg4","avi","wmv","MPEGPS","flv","3gp","WebM","AVI","MP4","FLV");
	$ext 				 	= 	pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
	if(in_array($ext,$valid_formats))
	{
		$actual_vid_name 	= 	$time."_".$userId.".".$ext;
		$newVidName		 =	$time."_".$userId.".mp4";
		//shell_exec('/usr/local/bin/ffmpeg -i '.$_FILES['file']['tmp_name'].' -strict -2 '.$path.$newVidName);
		//shell_exec('/usr/local/bin/ffmpeg -itsoffset -4  -i '.$_FILES['file']['tmp_name'].' -vcodec mjpeg -vframes 1 -an -f rawvideo -s 320x240 '.$pathThumb.$time.'_'.$userId.'.jpg');
		shell_exec('/usr/local/bin/ffmpeg -i '.$_FILES['file']['tmp_name'].' -strict -2 '.$path.$newVidName);
		shell_exec('/usr/local/bin/ffmpeg -i '.$path.$newVidName.' -ss 00:00:02.000 -vframes 1 '.$pathThumb.$time.'_'.$userId.'.jpg');
		$videofile		  =	$path.$newVidName;
		//----------------start upload to AWS----------------------------------------
		require(DIR_ROOT."amazone_s3/S3.php");
		$s3			=	new S3(AWS_ID,AWS_KEY);
		S3::putObjectFile($videofile,AWS_VIDEO_BUCKET,'uploads/videos/'.$todayDate.'/'.$newVidName,S3::ACL_PUBLIC_READ,array());
		S3::putObjectFile($pathThumb.$time.'_'.$userId.'.jpg',AWS_VIDEO_BUCKET,'uploads/videos/'.$todayDate.'/thumb/'.$time.'_'.$userId.'.jpg',S3::ACL_PUBLIC_READ,array());
		//----------------end upload to AWS-------------------------------------------
		$xyz 				= 	shell_exec("/usr/local/bin/ffmpeg -i \"{$videofile}\" 2>&1");
		$search			 =	'/Duration: (.*?),/';
		preg_match($search, $xyz, $matches);
		$explode			= 	explode(':', $matches[1]);
		$_POST['video_duration']		=	$explode[0].':'.$explode[1].':'.floor($explode[2]);
		$_POST['video_url']			 =	$todayDate.'/'.$newVidName;
		$_POST['video_thumb']		   =	$todayDate.'/thumb/'.$time.'_'.$userId.'.jpg';
		$objVideos->insert($_POST);
		$uploadStatus				   =	1;
		$lastAlbImgId				   =	$objVideos->insertId();
		$video_encr_id		   		  =	$objCommon->randStrGenSpecial(10).'-'.$lastAlbImgId.$objCommon->randStrGenSpecial(4);
		$objVideos->updateField(array("video_encr_id"=>$video_encr_id),"video_id=".$lastAlbImgId);	
	}
}
mysql_query("COMMIT");
if($uploadStatus==1){
	if (preg_match('%^https?://[^\s]+$%',$_POST['video_thumb'])) {
		$imgUrlshow			=	$_POST['video_thumb'];
	} else {
		$imgUrlshow			=	SITE_ROOT.'uploads/videos/'.$_POST['video_thumb'];
	}
	?>
<div id ="one">
	<div class="upload-preview" style="background-image:url('<?php echo $imgUrlshow?>');"></div>
	<div class="upload-status">
		<p><strong>Upload Status</strong></p>
		<p class="process">uploding completed</p>
	</div>
	<div class="upload-link">
		<p>Your video will be live at : </p>
		<a href="<?php echo SITE_ROOT.'video/watch/'.$video_encr_id?>"><?php echo SITE_ROOT.'video/watch/ '.$video_encr_id?></a>
	</div>
</div>
<div id ="two">
<input type="hidden" name="videoId" value="<?php echo $lastAlbImgId?>" />
</div>
<?php
}
?>
