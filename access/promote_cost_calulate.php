<?php
@session_start();
unset($_SESSION['totalCostCat']);
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	$objCommon		   		 =	new common();
	$getPromotionCat		   =	$objUsers->getRowSql("SELECT * FROM promotion_based_category");
	$getPromotionLoc		   =	$objUsers->getRowSql("SELECT * FROM promotion_based_location");
	$userId				   	=	$_SESSION['userId'];
	if($_POST['promoCat'] !='' && $userId !=''){
		$locationSel		   =	$objCommon->esc($_POST['locationSel']);
		$categorySel		   =	$objCommon->esc($_POST['categorySel']);
		$durationSel		   =	$objCommon->esc($_POST['durationSel']);
		$promoCat			  =	$objCommon->esc($_POST['promoCat']);
		$getDurationCost	   =	$objUsers->getRowSql("SELECT * FROM promotion_based_duration WHERE pbd_id=".$durationSel);
		if($promoCat=='profile'){
			$totalCostCat	  =	$getPromotionCat['pbc_profile']*$locationSel;
			$totalCostLoc	  =	$getPromotionLoc['pbl_profile']*$categorySel;
			$totalCostDur	  =	$getDurationCost['pbd_profile'];		
		}else if($promoCat=='video'){
			$totalCostCat	  =	$getPromotionCat['pbc_video']*$locationSel;
			$totalCostLoc	  =	$getPromotionLoc['pbl_video']*$categorySel;	
			$totalCostDur	  =	$getDurationCost['pbd_video'];
		}else if($promoCat=='music'){
			$totalCostCat	  =	$getPromotionCat['pbc_music']*$locationSel;
			$totalCostLoc	  =	$getPromotionLoc['pbl_music']*$categorySel;
			$totalCostDur	  =	$getDurationCost['pbd_music'];	
		}
		echo $totalCostCat		  =	$totalCostCat+$totalCostLoc+$totalCostDur;
		$_SESSION['totalCostCat']=array('total_cost'=>$totalCostCat,'total_duration'=>$getDurationCost['pbd_days']);
	}
}
?>