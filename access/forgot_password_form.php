<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/forgot_password.php");
include_once(DIR_ROOT."class/users.php");
$objCommon				  =	new common();
$objUser					=	new users();
$objForgot				  =	new forgot_password();
if($_GET['requestStr'] != '' && $_POST['reg_pwd'] != '' && $_POST['reg_conf_pwd'] != '' && $_POST['reg_pwd'] == $_POST['reg_conf_pwd']){
	$getForgotDetails	   =	$objForgot->getRow("fp_string='".$_GET['requestStr']."'");
	if($getForgotDetails['user_id']){
		$newpassword		=	md5($objCommon->esc($_POST['reg_pwd']));
		$objUser->updateField(array("password"=>$newpassword),"user_id=".$getForgotDetails['user_id']);
		$objForgot->delete("user_id=".$getForgotDetails['user_id']);
		$_SESSION['userId'] =	$getForgotDetails['user_id'];
		header("location:".SITE_ROOT);
	}else{
		$objCommon->addMsg('Invalid user account...',0);
		header("location:".$_SERVER['HTTP_REFERER']);
		exit;
	}
}else{
	$objCommon->addMsg('Please fill the required fields...',0);
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}
?>