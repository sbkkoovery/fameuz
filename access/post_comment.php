<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/comments.php");
$objCommon				=	new common();
$objComments			  =	new comments();
$action				   =	$objCommon->esc($_GET['action']);
$userId				   =	$_SESSION['userId'];
if($action== 'add_comment' && $userId != ''){
	if(isset($_POST['comment_descr'],$_POST['type'],$_POST['id'],$_POST['userto']) && $_POST['comment_descr']!='' && $_POST['type'] !='' && $_POST['id'] != '' && $_POST['userto'] != ''){
		mysql_query("START TRANSACTION");
		$_POST['comment_cat']		 =	$typecat	=	$objCommon->esc($_POST['type']);
		$_POST['comment_content']	 =	$objCommon->esc($_POST['id']);
		$_POST['comment_descr']	   =	$objCommon->esc($_POST['comment_descr']);
		$_POST['comment_user_to']	 =	$objCommon->esc($_POST['userto']);
		$_POST['comment_user_by']     =	$userId;
		$_POST['comment_time']		=	date("Y-m-d H:i:s");
		$_POST['comment_status']	  =	1;
		$objComments->insert($_POST);
		if($typecat==1){
			$objComments->build_result_insert("update user_photos SET photo_comment_count = photo_comment_count+1 where photo_id=".$_POST['comment_content']);
		}else if($typecat==2){
			$objComments->build_result_insert("update album_images SET ai_comment_count = ai_comment_count+1 where ai_id=".$_POST['comment_content']);
		}
		else if($typecat==6){
			$objComments->build_result_insert("update user_profile_image SET upi_comment_count = upi_comment_count+1 where upi_id=".$_POST['comment_content']);
		}
	//----notification table------------------------------------
		$myDetails				   		   	   =	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$userId);
		$displayName						  	 =	$objCommon->displayName($myDetails);
		$friend_id							   =	$objCommon->esc($_POST['comment_user_to']);
		$notiType								=	'comments';
		$notiImg								 =	'';
		$notiDescr   	 	 					   =	'<b>'.$displayName.'</b> commented on your <b>photo</b>.';
		$notiUrl  								 =	SITE_ROOT.'user/single-image?type='.$_POST['comment_cat'].'&id='.$_POST['comment_content'];
		$objCommon->pushNotification($friend_id,$userId,$notiType,$notiImg,$notiDescr,$notiUrl);
		//----------------------------------------------------------
		$_POST = array();
		mysql_query("COMMIT");
	}	
}
$imgtype	=	$_POST['type'];
$imgId		=	$_POST['id'];
$userTo		=	$_POST['userto'];

$jsonArr	=	array('imgtype'=>$imgtype, 'imgId'=>$imgId, 'userTo'=>$userTo);
echo json_encode($jsonArr);
?>