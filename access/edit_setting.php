<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/privacy_settings.php");
$objCommon						  =	new common();
$objPrivacySettings				 =	new privacy_settings();
$userId				   	   		 =	$_SESSION['userId'];
$privacy_arr				  		=	array();
if(isset($userId) && $userId!=''){
	mysql_query("START TRANSACTION");
	$profile_see		  	  		=	$_POST['profile_see'];
	$privacy_arr['profile_see'] 	 =	$profile_see;
	$personal_see				   =	$_POST['personal_see'];
	$privacy_arr['personal_see'] 	=	$personal_see;
	$photos_see				   	 =	$_POST['photos_see'];
	$privacy_arr['photos_see'] 	  =	$photos_see;
	$review_see				   	 =	$_POST['review_see'];
	$privacy_arr['review_see'] 	  =	$review_see;
	$worked_see				   	 =	$_POST['worked_see'];
	$privacy_arr['worked_see'] 	  =	$worked_see;
	$disply_see				   	 =	$_POST['disply_see'];
	$privacy_arr['disply_see'] 	  =	$disply_see;
	$book_me				   	 	=	$_POST['book_me'];
	$privacy_arr['book_me'] 	  	 =	$book_me;
	$email_notification		     =	$_POST['email_notification'];
	$privacy_arr['email_notification']=  $email_notification;
	$profile_status_select		   =	$_POST['profile_status_select'];
	$privacy_arr['profile_status_select']=  $profile_status_select;
	$serialArr					  =	serialize($privacy_arr);
	$getUserExist				   =	$objPrivacySettings->getRow("user_id=".$userId);
	if($getUserExist['user_id']){
		$objPrivacySettings->build_result_insert("UPDATE privacy_settings SET ps_value = '".$serialArr."' WHERE user_id = ".$userId);
	}else{
		$objPrivacySettings->build_result_insert("INSERT INTO privacy_settings (user_id,ps_value) VALUES (".$userId.", '".$serialArr."')");
	}
	mysql_query("COMMIT");
	$objCommon->addMsg('Changes has been updated',1);
	header("location:".SITE_ROOT.'user/edit-profile?active=settings');
	exit;
}else{
	$objCommon->addMsg('Please fill the fields...',0);
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}
?>