<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/message.php");
$objCommon				=	new common();
$objMessage			   =	new message();
$userId				   =	$_SESSION['userId'];
$checkedValues			=	$_POST['checkedValues'];
if($userId != '' && count($checkedValues)>0){
	foreach($checkedValues as $allCheckValues){
		$objMessage->updateField(array("msg_trash_from"=>0),"msg_from=".$userId." AND msg_to=".$allCheckValues);
		$objMessage->updateField(array("msg_trash_to"=>0),"msg_to=".$userId." AND msg_from=".$allCheckValues);
	}
}
?>