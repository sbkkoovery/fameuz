<?php
session_start();
	include_once("../includes/site_root.php");
	include("../class/common_class.php");
	$objCommon	=	new common();
	$path = "../uploads/profile_images/";
	$todayDate		=	date('Y-m-d');
	if(!file_exists($path.$todayDate)){
		mkdir($path.$todayDate);
	}
	if(!file_exists($path.$todayDate.'/original')){
		mkdir($path.$todayDate.'/original');
	}
	$path	=	$path.$todayDate.'/original/';
	if(isset($_REQUEST['ID'])){
		$valid_formats = array("jpg","JPG", "png", "gif", "bmp", "jpeg");
		$name 				  = $_FILES['photoimg']['name'];
		$size 				  = $_FILES['photoimg']['size'];
		$userId				= $_SESSION['userId'];
		
		if(strlen($name) >0 && $userId !=''){
				$ext	=	strtolower(pathinfo($name, PATHINFO_EXTENSION));
				if(in_array($ext,$valid_formats)){
						$actual_image_name =  time()."_".$userId;
						$tmp = $_FILES['photoimg']['tmp_name'];
						$imagename = $path."".$actual_image_name;
	
						$uploadedImage		=	$objCommon->addIMG($_FILES['photoimg'],$path,$actual_image_name,600,800,false);
						echo $todayDate.'/original/'.$uploadedImage;
					}				
			}
	}

?>
