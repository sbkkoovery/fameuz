<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/email_message.php");
$objCommon				=	new common();
$objEmailMessage		  =	new email_message();
$action				   =	$objCommon->esc($_GET['action']);
$userId				   =	$_SESSION['userId'];
$jobId				   	=	$objCommon->esc($_GET['jobId']);
if($action== 'del_job' && $userId != '' && $jobId !=''){
	$objEmailMessage->delete("em_id=".$jobId);;
	$objCommon->addMsg('Selected item has been deleted...',1);
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}else{
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}
?>