<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/notifications.php");
$objCommon				=	new common();
$objNotifications		 =	new notifications();
$userId				   =	$_SESSION['userId'];
$notid					=	$objCommon->esc($_POST['notid']);
if($notid != '' && $userId != ''){
	$objNotifications->delete("notification_id=".$notid);
}
?>