<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/promotion.php");
$objCommon					=	new common();
$objPromotion				=	new promotion();
$user_id					=	$_SESSION['userId'];
if(isset($_POST['proType'],$user_id,$_SESSION['totalCostCat']['total_cost']) && $_SESSION['totalCostCat']['total_cost']!='' && $user_id !=''){
	$promoteArray				=	[];
	$promoteloc									=	$_POST['promoteloc'];
	$promoteArray['promo_country']				=	",".implode(",",$promoteloc).",";
	$promotecat											=	$_POST['promotecat'];
	$promotecatStr										=	",".implode(",",$promotecat).",";
	$promoteArray['promo_category']						=	$promotecatStr;
	$promoteArray['promo_type']							=	$_POST['proType'];
	$promoteArray['promo_content_id']					=	$_POST['proContent'];
	$promoteArray['promo_start_date']					=	date("Y-m-d",strtotime($_POST['datefrom']));
	$promoteArray['promo_end_date']						=	date("Y-m-d",strtotime($_POST['datefto']));
	$promoteArray['user_id']							=	$user_id;
	$promoteArray['promo_duration']						=	$_SESSION['totalCostCat']['total_duration'];
	$promoteArray['promo_created_date']					=	date("Y-m-d H:i:s");
	$promoteArray['promo_admin_status']					=	1;
	$promoteArray['promo_status']						=	1;

	$objPromotion->insert($promoteArray);

	$totalCost											=	$_SESSION['totalCostCat']['total_cost'];

// PayPal settings
	$paypal_email = 'basil._1352649911_biz@gmail.com';
	$return_url = SITE_ROOT.'paypal/payment-successful.html';
	$cancel_url = SITE_ROOT.'paypal/payment-cancelled.html';
	$notify_url = SITE_ROOT.'paypal/payments.php';

	$item_name = 'Promote '.ucwords($_POST['proType']);
	$item_amount = $totalCost;
	$remainField	=	[];
	$remainField['cmd']		=	'_xclick';
	$remainField['no_note']	=	'1';
	$remainField['lc']		=	'UK';
	$remainField['currency_code']	=	'USD';
	$remainField['bn']	=	'PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest';
	$remainField['item_number']	=	$objPromotion->insertId();

	$querystring = '';
	// Firstly Append paypal account to querystring
	$querystring .= "?business=".urlencode($paypal_email)."&";

	// Append amount& currency (�) to quersytring so it cannot be edited in html

	//The item name and amount can be brought in dynamically by querying the $_POST['item_number'] variable.
	$querystring .= "item_name=".urlencode($item_name)."&";
	$querystring .= "amount=".urlencode($item_amount)."&";

	//loop for posted values and append to querystring
	foreach($remainField as $key => $value){
		$value = urlencode(stripslashes($value));
		$querystring .= "$key=$value&";
	}

	// Append paypal return addresses
	$querystring .= "return=".urlencode(stripslashes($notify_url))."&";
	$querystring .= "cancel_return=".urlencode(stripslashes($cancel_url))."&";
	$querystring .= "notify_url=".urlencode($notify_url);

	// Append querystring with custom field
	//$querystring .= "&custom=".USERID;

	// Redirect to paypal IPN
	//echo $querystring;
	header('location:https://www.sandbox.paypal.com/cgi-bin/webscr'.$querystring);
	exit;
}
else{
	$objCommon->addMsg('Please fill the fields...',0);
	//header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}
?>