<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/blogs.php");
include(DIR_ROOT."ajax/resize-class.php");
$objCommon				=	new common();
$objBlogs		   		 =	new blogs();
$action				   =	$objCommon->esc($_GET['action']);
$userId				   =	$_SESSION['userId'];
$valid_formats 			= 	array("jpg", "png", "gif", "bmp","jpeg","JPG", "PNG", "GIF", "BMP","JPEG");
if($action== 'create_blogs' && $userId != ''){
 if(isset($_POST['blog_title'],$_POST['blog_descr']) && $_POST['blog_title']!='' && $_POST['blog_descr'] !=''){
		mysql_query("START TRANSACTION");
		$_POST['blog_title']		   		=	$objCommon->esc($_POST['blog_title']);
		$_POST['bc_id']		   			 =	$objCommon->esc($_POST['bc_id']);
		$_POST['blog_alias']				=	$objCommon->getAlias($_POST['blog_title']);
		$_POST['blog_descr']	   			=	$objCommon->esc($_POST['blog_descr']);
		$_POST['blog_status']		       =	$objCommon->esc($_POST['blog_status']);
		$hid_edit_jobId					 =	$objCommon->esc($_POST['editId']);
		if($hid_edit_jobId){
		$_POST['edited_time']			   =	date("Y-m-d H:i:s");
		if($_FILES['blog_img']['tmp_name']){ 
		$path			 =	DIR_ROOT.'uploads/blogs/';
		$todayDate		=	date('Y-m-d');
		if(!file_exists($path.$todayDate)){
			mkdir($path.$todayDate);
		}
		$path	=	$path.$todayDate.'/';
		$name = $_FILES['blog_img']['name'];
		$size = $_FILES['blog_img']['size'];
		if(strlen($name))
		{
			$ext	=	strtolower(pathinfo($name, PATHINFO_EXTENSION));
			if(in_array($ext,$valid_formats))
			{
			if($size<(3072*10240))
			{
			$time						=	time();				
			$actual_image_name_ext_no	=	$time."_".$userId;
			$actual_image_name 		   = 	$time."_".$userId.".".$ext;
			
			if(!file_exists($path."original")){
				mkdir($path."original");
			}
			if(!file_exists($path."thumb")){
				mkdir($path."thumb");
			}
			$tmpName = $_FILES['blog_img']['tmp_name'];    
			list($widthu, $heighut, $typeu, $attru) = getimagesize($tmpName); 
			if(move_uploaded_file($_FILES['blog_img']['tmp_name'], $path.'original/'.$actual_image_name)){
			
			$resizeObj = new resize($path.'original/'.$actual_image_name);
			$resizeObj -> resizeImage(200, 200, 'auto');
			$resizeObj -> saveImage($path.'thumb/'.$actual_image_name, 100);
				if ($heighut > 886 || $widthu >886){
					$resizeObj -> resizeImage(886, 886, 'auto');
					$resizeObj -> saveImage($path.$actual_image_name, 100);
				}else{
					$resizeObj -> resizeImage($widthu, $heighut, 'auto');
					$resizeObj -> saveImage($path.$actual_image_name, 100);
				}
			//----------------start upload to AWS----------------------------------------
			require(DIR_ROOT."amazone_s3/S3.php");
			$s3			=	new S3(AWS_ID,AWS_KEY);
			S3::putObjectFile($path.$actual_image_name,AWS_IMG_BUCKET,'uploads/blogs/'.$todayDate.'/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
			S3::putObjectFile($path.'thumb/'.$actual_image_name,AWS_IMG_BUCKET,'uploads/blogs/'.$todayDate.'/thumb/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
			//----------------end upload to AWS-------------------------------------------
			$_POST['blog_img']   		  =	$todayDate."/".$actual_image_name;
			 }
		}
		
		}
		
		}
			
}
			$objBlogs->update($_POST,"blog_id=".$hid_edit_jobId);
			$objCommon->addMsg('Your blog has been updated',1);
			header("location:".SITE_ROOT."user/my-blogs");
		}else{
			$_POST['user_id']				=	$userId;
			$_POST['created_time']		   =	date("Y-m-d H:i:s");
			$_POST['edited_time']		    =	date("Y-m-d H:i:s");
			if($_FILES['blog_img']['tmp_name']){ 
			$path			 				=	DIR_ROOT.'uploads/blogs/';
			$todayDate					   =	date('Y-m-d');
			if(!file_exists($path.$todayDate)){
				mkdir($path.$todayDate);
			}
			$path							=	$path.$todayDate.'/';
			$name 							= 	$_FILES['blog_img']['name'];
			$size 							= 	$_FILES['blog_img']['size'];
			if(strlen($name))
			{
				$ext	=	strtolower(pathinfo($name, PATHINFO_EXTENSION));
				if(in_array($ext,$valid_formats))
				{
					if($size<(3072*10240))
					{
					$time				  		=	time();				
					$actual_image_name_ext_no	=	$time."_".$userId;
					$actual_image_name 		   = 	$time."_".$userId.".".$ext;
					if(!file_exists($path."original")){
						mkdir($path."original");
					}
					if(!file_exists($path."thumb")){
						mkdir($path."thumb");
					}
					$tmpName 					 = 	$_FILES['blog_img']['tmp_name'];    
					list($widthu, $heighut, $typeu, $attru) = getimagesize($tmpName); 
					if(move_uploaded_file($_FILES['blog_img']['tmp_name'], $path.'original/'.$actual_image_name)){
						$resizeObj = new resize($path.'original/'.$actual_image_name);
						$resizeObj -> resizeImage(200, 200, 'auto');
					$resizeObj -> saveImage($path.'thumb/'.$actual_image_name, 100);
				if ($heighut > 886 || $widthu >886){
					$resizeObj -> resizeImage(886, 886, 'auto');
					$resizeObj -> saveImage($path.$actual_image_name, 100);
				}else{
					$resizeObj -> resizeImage($widthu, $heighut, 'auto');
					$resizeObj -> saveImage($path.$actual_image_name, 100);
				}
				//----------------start upload to AWS---------------------------------------------------------------------------------------------------------------------
				require(DIR_ROOT."amazone_s3/S3.php");
				$s3			=	new S3(AWS_ID,AWS_KEY);
				S3::putObjectFile($path.$actual_image_name,AWS_IMG_BUCKET,'uploads/blogs/'.$todayDate.'/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
				S3::putObjectFile($path.'thumb/'.$actual_image_name,AWS_IMG_BUCKET,'uploads/blogs/'.$todayDate.'/thumb/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
				//----------------end upload to AWS------------------------------------------------------------------------------------------------------------------------
						$_POST['blog_img']   		  =	$todayDate."/".$actual_image_name;
					}
				}

			}

		}
	
	}
	$objBlogs->insert($_POST);
 		}
		mysql_query("COMMIT");
		$objCommon->addMsg('Your blog has been posted',1);
		header("location:".SITE_ROOT."user/my-blogs");
		exit;
	}else{
		$objCommon->addMsg('Please fill the fields...',0);
		header("location:".$_SERVER['HTTP_REFERER']);
	}
}
?>