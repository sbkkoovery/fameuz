<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/email_message.php");
include_once(DIR_ROOT."class/email_message_users.php");
$objCommon				=	new common();
$objEmailMessage		  =	new email_message();
$objEmailMessageUser	  =	new email_message_users();
$userId				   =	$_SESSION['userId'];
if(isset($userId,$_POST['user_id'],$_POST['contact_msg']) && $userId!='' && $_POST['user_id'] !='' && $_POST['contact_msg'] != ''){
	mysql_query("START TRANSACTION");
	$_POST['em_category']	   		      =	'contact_message';
	$_POST['em_subject']	   		       =	'Contact Message';
	$_POST['em_message']	   			   =	$objCommon->esc($_POST['contact_msg']);
	$_POST['em_read_status']	   		   =	0;
	$_POST['em_createdon']	   		   	 =	date("Y-m-d H:i:s");
	$_POST['em_status']	   		   		=	1;
	$objEmailMessage->insert($_POST);
	$insertid							  =	$objEmailMessage->insertId();
	$_POST['emu_from']		   		 	 =	$userId;
	$_POST['emu_to']	   		   	   	   =	$objCommon->esc($_POST['user_id']);
	$_POST['em_id']	   		   	   	    =	$insertid;
	$_POST['emu_read_status']			  =	0;
	$_POST['emu_delete_from']			  =	1;
	$_POST['emu_delete_to']			  	=	1;
	$objEmailMessageUser->insert($_POST);
		//----notification table------------------------------------
		$myDetails				   		   	   =	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$userId);
		$displayName						  	 =	$objCommon->displayName($myDetails);
		$friend_id							   =	$objCommon->esc($_POST['user_id']);
		$notiType								=	'contact_message';
		$notiImg								 =	'';
		$notiDescr   	 	 					   =	'<b>'.$displayName.'</b> has sent you <b>contact message</b>';
		$notiUrl  								 =	SITE_ROOT.'user/my-messages';
		$objCommon->pushNotification($friend_id,$userId,$notiType,$notiImg,$notiDescr,$notiUrl);
		$objCommon->addMsg("Contact message has been sent",1);
		//----------------------------------------------------------
	mysql_query("COMMIT");
	header("location:".SITE_ROOT.'user/my-sent-messages');
	exit;

}
?>