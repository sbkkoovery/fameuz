<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/message.php");
include_once(DIR_ROOT."class/message_attachement.php");
include(DIR_ROOT."ajax/resize-class.php");
$objCommon						 =	new common();
$objMsg							=	new message();
$objMsgAttach					  =	new message_attachement();
$userId							=	$_SESSION['userId'];
$msg_descr						 =	$objCommon->esc($_POST['msg_descr']);
if($userId != '' && (($msg_descr != '' && $msg_descr != '\\n') ||  $_FILES['add_photos']['tmp_name'] !='')){
	mysql_query("START TRANSACTION");
	$_POST['msg_from']			 =	$userId;
	$_POST['msg_to']			   =	$objCommon->esc($_POST['msg_to']);
	$_POST['msg_descr']			=	$msg_descr;
	$_POST['msg_created_date']	 =	date("Y-m-d H:i:s");
	$_POST['msg_edited_date']	  =	date("Y-m-d H:i:s");
	$_POST['msg_read_status']	  =	0;
	$_POST['msg_status']		   =	1;
	$objMsg->insert($_POST);
	$lastInsertId				  =	$objMsg->insertId();
	//--------------------------------------------
	if($_FILES['add_photos']['tmp_name']){
	$valid_formats 			= 	array("jpg", "png", "gif", "bmp","jpeg","JPG", "PNG", "GIF", "BMP","JPEG");
	$path			 =	DIR_ROOT.'uploads/message/';
	$todayDate		=	date('Y-m-d');
	if(!file_exists($path.$todayDate)){
		mkdir($path.$todayDate);
	}
	$path	=	$path.$todayDate.'/';
	$name = $_FILES['add_photos']['name'];
	$size = $_FILES['add_photos']['size'];
	if(strlen($name))
	{
		$ext	=	strtolower(pathinfo($name, PATHINFO_EXTENSION));
		if(in_array($ext,$valid_formats))
		{
		if($size<(3072*10240))
		{
		$time						=	time();				
		$actual_image_name_ext_no	=	$time."_".$userId;
		$actual_image_name 		   = 	$time."_".$userId.".".$ext;
		
		if(!file_exists($path."original")){
			mkdir($path."original");
		}
		if(!file_exists($path."thumb")){
			mkdir($path."thumb");
		}
		$tmpName = $_FILES['add_photos']['tmp_name'];    
		list($widthu, $heighut, $typeu, $attru) = getimagesize($tmpName); 
		if(move_uploaded_file($_FILES['add_photos']['tmp_name'], $path.'original/'.$actual_image_name)){
		
		$resizeObj = new resize($path.'original/'.$actual_image_name);
		$resizeObj -> resizeImage(200, 200, 'auto');
		$resizeObj -> saveImage($path.'thumb/'.$actual_image_name, 100);
			if ($heighut > 400 || $widthu > 400){
				$resizeObj -> resizeImage(400, 400, 'auto');
				$resizeObj -> saveImage($path.$actual_image_name, 100);
			}else{
				$resizeObj -> resizeImage($widthu, $heighut, 'auto');
				$resizeObj -> saveImage($path.$actual_image_name, 100);
			}
			$_POST['ma_attachement']   		  =	$todayDate."/".$actual_image_name;
			$_POST['ma_attach_type']		  =	1;
			$_POST['msg_id']				  =	$lastInsertId;
			$objMsgAttach->insert($_POST);
		 }
	}
	
	}
	
	}
			
}
//--------------------------------------------------------------------
	mysql_query("COMMIT");
}
?>