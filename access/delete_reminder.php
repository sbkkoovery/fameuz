<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/reminders.php");
$objCommon				=	new common();
$objReminder			  =	new reminders();
$userId				   =	$_SESSION['userId'];
$remindId				   =	$objCommon->esc($_GET['remindId']);
if($userId != '' && $remindId !=''){
	$objReminder->delete("remind_id=$remindId AND user_id=$userId");
	$objCommon->addMsg('Selected item has been deleted...',1);
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}else{
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}
?>