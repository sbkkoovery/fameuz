<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/notifications.php");
include_once(DIR_ROOT."class/worked_together.php");
$objCommon				=	new common();
$objNotifications		 =	new notifications();
$objWorkedTogether		=	new worked_together();
$userId				   =	$_SESSION['userId'];
$workedId				 =	$objCommon->esc($_POST['workedId']);
$confirmId				=	$objCommon->esc($_POST['confirmId']);
$notid					=	$objCommon->esc($_POST['notid']);
if($confirmId != '' && $workedId !='' && $userId != ''){
	if($confirmId==1){
		$objWorkedTogether->updateField(array('to_status'=>1),"worked_id='".$workedId."'");
	}else if($confirmId==0 && $notid !=''){
		$objNotifications->delete("notification_id=".$notid);
	}
}
?>