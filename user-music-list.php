<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/music.php");
$objMusic							=	new music();
$user_url							=	$objCommon->esc($_GET['user_url']);
if($user_url){
	$getMyFriendDetails			  =	$objUsers->getRowSql("SELECT user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url
															FROM users AS user 
															LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
															LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
															WHERE user.status=1 AND user.email_validation=1 AND social.usl_fameuz='".$user_url."'");
}else{
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}
if($getMyFriendDetails['user_id']){
	$friendId					   =	$getMyFriendDetails['user_id'];
}else{
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}
?>
<script src="<?php echo SITE_ROOT?>jw_player/jwplayer.js"></script>
    <div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="user_profile">
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                                    <div class="content music-pagein">
                                        <div class="pagination_box">
                                            <div class="row">   
                                                <div class="col-sm-12 not">
													<div class="pagination_box">
                                                        <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
														<a href="<?php echo SITE_ROOT.$user_url?>"> <?php echo $objCommon->displayName($getMyFriendDetails)?> </a><i class="fa fa-caret-right"></i></a>
														<a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                                        <a href="javascript:;" class="active"> <?php echo $objCommon->displayName($getMyFriendDetails)?>'s Music </a></a>
														<?php
														include_once(DIR_ROOT."widget/notification_head.php");
														?>
													</div>
                                                </div>
                                                </div>
                                      </div>
                                        <div class="clr"></div>            
                                    </div>
                                </div> 
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3 music-page">
                                    <div class="sidebar">
                                        <div class="search_box"><form action="<?php echo SITE_ROOT.'user/search-result'?>" method="get"><input type="text" placeholder="Search "  name="keywordSearch" /></form></div>
                                    </div>
                                </div> 
                            </div>
                            <div class="clr"></div>
                            <div class="videos">
                                <div class="row">
								<?php  echo $objCommon->displayMsg();?>
                                    <div class="clr"></div> 
                                    <div class="top_videos">
                                        <h1><?php echo $objCommon->displayName($getMyFriendDetails)?>'s Music</h1>
										<div class="loadMusic"></div>
                            </div>
                            <div class="clr"></div>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
<script type="text/javascript">
$(document).ready(function(e) {
	getresult('<?php echo SITE_ROOT?>widget/load_user_music.php');
});
function getresult(getUrl) {
	$.ajax({
		url: getUrl,
		type: "GET",
		data:  {rowcount:$("#rowcount").val(),friendId:'<?php echo $friendId?>'},
		beforeSend: function(){
		//$('#loader-icon').show();
		},
		complete: function(){
		//$('#loader-icon').hide();
		},
		success: function(data){
			$(".loadMusic").append(data);
			if($("#total-count").val()>0){
			}else{
				$(".loadMusic").html('<p>No music found....</p>');
			}
		},
		error: function(){} 	        
   });
}
$(window).scroll(function(){
		if ($(window).scrollTop() == $(document).height() - $(window).height()){
			if($(".pagenum:last").val() <= $(".total-page").val()) {
				var pagenum = parseInt($(".pagenum:last").val()) + 1;
				getresult('<?php echo SITE_ROOT?>widget/load_user_music.php?page='+pagenum);
			}
		}
	}); 
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
    