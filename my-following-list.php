<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
?>
</style>
<link href="<?php echo SITE_ROOT?>css/un_friend_pop.css" rel="stylesheet">
<div class="inner_content_section">
	<div class="container">
		<div class="row">
        	<div class="col-sm-12">
				<div class="inner_top_border">
					<div class="user_profile">
					<?php echo $objCommon->checkEmailverification();?>
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                            <div class="content">
                                <div class="pagination_box">
                                    <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
                                    <a href="<?php echo SITE_ROOT.$getUserDetails['usl_fameuz']?>"> My profile </a><i class="fa fa-caret-right"></i></a>
                                    <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                    <a href="javascript:;" class="active">Following</a>
                                    <?php
                                    include_once(DIR_ROOT."widget/notification_head.php");
                                    ?>
                                </div>
                                <div class="tab_white_search">
                                    <div class="section_search">
                                        <ul>
                                            <li class="visitor_head_box"><h5>Following</h5><span class="point-it"></span></li>
                                        </ul>
                                        <span>
                                            <input type="text" placeholder="Search Following" id="follower_search" name="follower_search">
                                        </span>
                                    </div>
                                </div>
                                <div class="visitor_list_box ">
                                    <ul class="row listVisitor"></ul>
                                </div>
                            </div>
                            </div>
                            <div class="col-sm-3 col-lg-sp-3">
                                <?php
                                include_once(DIR_ROOT."widget/right_static_ad_bar.php");
                                ?>
                            </div>
                        </div>
					</div>
				</div>
                </div>
			</div>
	</div>
</div>
<script>
$(window).load(function(e) {
	$('.fixer').fixedSidebar();
});
$(document).ready(function(){
	getresult('<?php echo SITE_ROOT?>ajax/my_following_list.php');
	 function getresult(url) {
		$.ajax({
			url: url,
			type: "GET",
			data:  {rowcount:$("#rowcount").val()},
			beforeSend: function(){
			$('#loader-icon').show();
			},
			complete: function(){
			$('#loader-icon').hide();
			},
			success: function(data){
				$(".visitor_list_box ul.listVisitor").append(data);
				initFollowing();
				if($("#total-count").val()>0){
				}else{
					$(".visitor_list_box ul.listVisitor").html('<li class="col-md-6"><p>No followers found....</p></li>');
				}
			},
			error: function(){} 	        
	   });
	}
	$(window).scroll(function(){
		if ($(window).scrollTop() == $(document).height() - $(window).height()){
			if($(".pagenum:last").val() <= $(".total-page").val()) {
				var pagenum = parseInt($(".pagenum:last").val()) + 1;
				getresult('<?php echo SITE_ROOT?>ajax/my_following_list.php?page='+pagenum);
			}
		}
	}); 
	$('#follower_search').keyup(function(){
		var keyVal		=	$(this).val();
			getSearchresult('<?php echo SITE_ROOT?>ajax/my_following_list.php?search='+keyVal+'&page=1');
	});
	function getSearchresult(url) {
		$.ajax({
			url: url,
			type: "GET",
			data:  {rowcount:$("#rowcount").val()},
			beforeSend: function(){
			$('#loader-icon').show();
			},
			complete: function(){
			$('#loader-icon').hide();
			},
			success: function(data){
				if(data!=""){
					$(".visitor_list_box ul.listVisitor").html(data);
					initFollowing();

				}else{
					$(".visitor_list_box ul.listVisitor").html('<li class="col-md-6"><p>No followers found....</p></li>');
				}
			},
			error: function(){} 	        
	   });
	}

});
function initFollowing(){
	$(".popUnfriend").on("click",function(){
		$(".dropdown_option_unfriend").hide();
		$(".has-spinner").removeClass("active");
		$(this).parent().children(".dropdown_option_unfriend").show();
		
	});
	$(".cancel").on("click",function(){
		$(".dropdown_option_unfriend").hide();
	});
	$(".unfriend").on("click",function(){
		$(this).addClass('active');
		var unfriendId	=	$(this).data("friend");
		if(unfriendId){
			$.ajax({
				url:'<?php echo SITE_ROOT.'ajax/unfollow_friend.php'?>',
				data:{unfriendId:unfriendId},
				type:"POST",
				success: function(data){
					$(".has-spinner").removeClass("active");
					$(".visitor_list_box ul.listVisitor").load('<?php echo SITE_ROOT?>ajax/my_following_list.php');
				}
				
			});
		}
		
	});
}
</script>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/fixed_sidebar.js"></script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  