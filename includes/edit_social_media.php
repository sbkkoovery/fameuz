<?php
@session_start();
include_once("site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/user_profile_image.php");
include_once(DIR_ROOT."class/user_social_links.php");
$objCommon		   				 =	new common();
$objUserSocialLinks				=	new user_social_links();
$getSocialLinks	  				=	$objUserSocialLinks->getRowSql("SELECT social.*,profileImg.upi_img_url FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 LEFT JOIN personal_details as personal ON user.user_id = personal.user_id WHERE user.status=1 AND user.user_id=".$_SESSION['userId']);
?>
<!-- Edit Social media-->
    <div class="help">
        <h1><span class="qstn"><img src="<?php echo SITE_ROOT?>images/bulb.png" alt="bulb" /></span> Tips....<span class="h_close"><a href="#"><i class="fa fa-close"></i></a></span></h1>
        <p>Please review our frequently 
        asked questions to the left if 
        you need help</p>
        <p>If you have a questions that is 
        not answered there, you can 
        contact us (response time will 
        usually be 1 to 2 business days)</p>
    </div>
    <form action="<?php echo SITE_ROOT?>access/edit_profile.php?action=edit_social_links" method="post" enctype="multipart/form-data">
         <h2 class="bg_heading">Link Your Account</h2>
         <p class="pic_dis">*Link your account inorder to share your profile</p>
         <div class="field_box_section">
            <div class="field_box box_1">
                <div class="image"><img src="<?php echo SITE_ROOT?>uploads/profile_images/<?php echo($getSocialLinks['upi_img_url'])?$getSocialLinks['upi_img_url']:'profile_pic.jpg'?>" align="prof_pic" class="social_img_prof"/></div>
                <div class="fiel_border">
                    <div class="icon"><i class="fa fa-link"></i></div>
                    <div class="text"><input type="text" name="social_user_fameuz"  id="social_user_fameuz" value="<?php echo $objCommon->html2text($getSocialLinks['usl_fameuz']);?>"  disabled="disabled"></div>
                </div>
                <div class="clr"></div>
                <!--<a class="link" href="#">Link</a>-->
            </div>
         </div>
         <div class="field_box_section">
            <div class="field_box">
                <div class="image"><img src="<?php echo SITE_ROOT?>images/facebook2.png" align="prof_pic" /></div>
                <div class="fiel_border">
                    <div class="icon"><i class="fa fa-link"></i></div>
                    <div class="text"><input type="text" placeholder="Link your account with facebook" name="social_user_fb"  id="social_user_fb" value="<?php echo $objCommon->html2text($getSocialLinks['usl_fb']);?>" ></div>
                </div>
                <div class="clr"></div>
                <!--<a class="link" href="#">Link</a>-->
            </div>
         </div>
         <div class="field_box_section">
            <div class="field_box">
                <div class="image"><img src="<?php echo SITE_ROOT?>images/twitter2.png" align="prof_pic" /></div>
                <div class="fiel_border">
                    <div class="icon"><i class="fa fa-link"></i></div>
                    <div class="text"><input type="text" placeholder="Link your account with Twitter"  name="social_user_twitter"  id="social_user_twitter" value="<?php echo $objCommon->html2text($getSocialLinks['usl_twitter']);?>" ></div>
                </div>
                <div class="clr"></div>
                <!--<a class="link" href="#">Link</a>-->
            </div>
         </div>
         <div class="field_box_section">
            <div class="field_box">
                <div class="image"><img src="<?php echo SITE_ROOT?>images/pintrest2.png" align="prof_pic" /></div>
                <div class="fiel_border">
                    <div class="icon"><i class="fa fa-link"></i></div>
                    <div class="text"><input type="text" placeholder="Link your account with Instagram" name="social_user_instagram"  id="social_user_instagram" value="<?php echo $objCommon->html2text($getSocialLinks['usl_instagram']);?>" ></div>
                </div>
                <div class="clr"></div>
                <!--<a class="link" href="#">Link</a>-->
            </div>
         </div>
         <div class="field_box_section">
            <div class="field_box">
                <div class="image"><img src="<?php echo SITE_ROOT?>images/google_plus2.png" align="prof_pic" /></div>
                <div class="fiel_border">
                    <div class="icon"><i class="fa fa-link"></i></div>
                    <div class="text"><input type="text" placeholder="Link your account with Google Plus" name="social_user_gplus"  id="social_user_gplus" value="<?php echo $objCommon->html2text($getSocialLinks['usl_gplus']);?>" ></div>
                </div>
                <div class="clr"></div>
                <!--<a class="link" href="#">Link</a>-->
            </div>
         </div>
         <div class="field_box_section">
            <div class="field_box last">
                <div class="image"><img src="<?php echo SITE_ROOT?>images/globe.png" align="prof_pic" /></div>
                <div class="fiel_border">
                    <div class="icon"><i class="fa fa-link"></i></div>
                    <div class="text"><input type="text" placeholder="Link  any additional websites that are not listed above" name="social_user_others"  id="social_user_others" value="<?php echo $objCommon->html2text($getSocialLinks['usl_others']);?>" ></div>
                </div>
                <div class="clr"></div>
                <!--<a class="link" href="#">Link</a>-->
            </div>
         </div>
         <div class="field_box_section">
            <div><input type="submit" value="Save Changes"></div>
        </div>
     </form>
<script type="application/javascript">
$('.h_close a').click(function(e){
	$(this).parent().parent().parent().remove();
	return false;
});
</script>