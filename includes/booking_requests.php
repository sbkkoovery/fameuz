<link href="<?php echo SITE_ROOT?>css/user-profile.css" rel="stylesheet" type="text/css">
<link href="<?php echo SITE_ROOT?>css/pagination.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.css" />
<script src="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.js"></script>
    <div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="user_profile">
						<?php echo $objCommon->checkEmailverification();?>
						<div class="content">
                            	<div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9"><div class="pagination_box">
                                        <a href="#">Home <i class="fa fa-caret-right"></i></a>
                                        <a href="#" class="active"> My Bookings  </a>
                                        <?php
                                            if(isset($_SESSION['userId'])){
                                                include_once(DIR_ROOT."widget/notification_head.php");
                                            }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3"><div class="search"><div class="search_box"><form action="<?php echo SITE_ROOT.'user/search-result'?>" method="get"><input type="text" placeholder="Search"  name="keywordSearch" /></form></div></div></div></div>
                                 <div class="row">
                                    <div class="col-lg-sp-2">
                            <?php
							include_once(DIR_ROOT."includes/profile_left.php");
							?>
                            </div>
							 <div class="col-lg-sp-10"><div class="sidebar">
                            	<div class="clr"></div>
                                <div class="booking">
                                	<h5>Sent Booking Requests</h5>
									<?php echo $objCommon->displayMsg();?>
                                    <div class="tab">
                                    	<ul class="c">
                                        	<li <?php echo $actAllStatus?>><a href="<?php echo SITE_ROOT.'user/booking-requests'?>">All Booking Requests</a></li>
                                            <li <?php echo $actUpcoming?>><a href="<?php echo SITE_ROOT.'user/booking-requests/upcoming'?>">Upcoming</a></li>
                                            <li <?php echo $actPending?>><a href="<?php echo SITE_ROOT.'user/booking-requests/pending'?>">Pending</a></li>
                                            <li <?php echo $actComplted?>><a href="<?php echo SITE_ROOT.'user/booking-requests/completed'?>">Completed</a></li>
                                            <li <?php echo $actExpired?>><a href="<?php echo SITE_ROOT.'user/booking-requests/expired'?>">Expired / Declined</a></li>
                                        </ul>
                                    </div>
                                    <div class="table">
                                    	<div class="table_hed c">
                                            <div class="hash left17">&nbsp;</div>
                                            <div class="name left27">Name</div>
                                            <div class="s_date left12"><i class="fa fa-calendar-o"></i> Start Date</div>
                                            <div class="e_date left12"><i class="fa fa-calendar-o"></i> End Date</div>
                                            <div class="budget left15"><i class="fa fa-dollar"></i> Budget</div>
                                            <div class="status left12"><i class="fa fa-info"></i> Status</div>
                                            <div class="delete left4">&nbsp;</div>
                                        </div>
										<?php
										 if(count($getMyBookings)>0){
											 $page_id			 	=	($_GET['page_id'])? ($num_results_per_page*($_GET['page_id']-1)):'';
											 foreach($getMyBookings as $keyBookings=>$allBookings){
											 	$siNo				=	$page_id+($keyBookings+1);
												if($allBookings['display_name']){
													$nameBooked	  =	$objCommon->html2text($allBookings['display_name']);
												}else if($getMyFriendDetails['first_name']){
													$nameBooked	  =	$objCommon->html2text($allBookings['first_name']);
												}else{
													$exploEmailFriend=	explode("@",$objCommon->html2text($allBookings['email']));
													$nameBooked	  =	$exploEmailFriend[0];
												}
												$bookFrom			=	($allBookings['book_from'])?date("Y-m-d",strtotime($allBookings['book_from'])):'&nbsp;';
												$bookTo			  =	($allBookings['book_to'])?date("Y-m-d",strtotime($allBookings['book_to'])):'&nbsp;';
												$bookBudget		  =	($allBookings['bm_budget'])?$objCommon->html2text($allBookings['bm_budget']):'&nbsp;';
												$bm_model_status	 =	$objCommon->html2text($allBookings['bm_model_status']);
												$todayDate		   =	date("Y-m-d");
												if(($bookFrom > $todayDate) && ($bm_model_status==1))
												{
													$modelStatusStr  =	'<span class="squrae upcoming"></span>Upcoming';
													$editStatus	  =	1;
												}else if(($bookFrom > $todayDate) && ($bm_model_status==0))
												{
													$modelStatusStr  =	'<span class="squrae expired"></span>Pending';
													$editStatus	  =	1;
												}else if(($bookFrom <= $todayDate) && ($bm_model_status==1))
												{
													$modelStatusStr  =	'<span class="squrae completed"></span>Completed';
													$editStatus	  =	0;
												}else if($bm_model_status==3){
													$modelStatusStr  =	'<span class="squrae pending"></span>Model Deleted';
													$editStatus	  =	0;
												}
												else if((($bookFrom <= $todayDate) && ($bm_model_status!=1)) || ($bookFrom > $todayDate) && ($bm_model_status==2))
												{
													$modelStatusStr  =	'<span class="squrae pending"></span>Expired';
													$editStatus	  =	0;
												}
										?>
                                        <div class="t_data">
                                        	<div class="data_hed <?php echo ($allBookings['bm_read_status']!=1)?'unread':''?>">
                                            	<div class="hash left17">
                                                	<span class="count"><?php echo $siNo?></span>
                                                    <div class="details" data-booking="<?php echo $objCommon->html2text($allBookings['bm_id']);?>"  data-read="<?php echo $objCommon->html2text($allBookings['bm_read_status']);?>">Details <span class="fa fa-angle-down"></span></div>
                                                    <!--<span class="detail_setting"><i class="fa fa-cog"></i>
                                                    	<span class="setting_option">
                                                        	<span class="arrow"><img src="<?php echo SITE_ROOT?>images/option_arrow.png" /></span>
                                                        	Hide From the list<br>
                                                            Keep Top 10 Updates
                                                        </span>
                                                    </span>-->                                                   
                                                </div>
                                                <div class="name left27"><?php echo $nameBooked;?></div>
                                                <div class="s_date left12"><?php echo $bookFrom?></div>
                                                <div class="e_date left12"><?php echo $bookTo?></div>
                                                <div class="budget left15"><?php echo $bookBudget?></div>
                                                <div class="status left12"><?php echo $modelStatusStr?></div>
                                                <div class="delete left4">
													<a data-delid="<?php echo $objCommon->html2text($allBookings['bm_id'])?>" class="delete_booking" href="javascript:;"><i class="fa fa-trash"></i></a>
													<?php
													if($editStatus==1){
													?>
													<a href="<?php echo SITE_ROOT.'user/'.$objCommon->html2text($allBookings['usl_fameuz']).'/add-book/edit'.$objCommon->html2text($allBookings['bm_id'])?>"><i class="fa fa-pencil"></i></a>
													<?php
													}
													?>
												</div>
                                                <div class="clr"></div>
                                            </div>
                                            <div class="data_content">
                                            	<div class="main_content">
                                                	<h5><?php echo $objCommon->html2text($allBookings['bm_title']);?></h5>
                                                    <p><?php echo $objCommon->html2textarea($allBookings['bm_descr']);?></p>
                                                    <h3><span>Work Details</span></h3>
                                                    <div class="w_details">
														<?php 
														if($allBookings['bm_company']){ 
															echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-building"></i> Company Name</div><div class="detail_colon">:</div><div class="detail_con">'.$objCommon->html2text($allBookings['bm_company']).'</div> </div>'; 
														}
														if($allBookings['bm_work']){ 
															echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-briefcase"></i> Work</div><div class="detail_colon">:</div><div class="detail_con">'.$objCommon->html2text($allBookings['bm_work']).'</div> </div>'; 
														}
														if($allBookings['bm_country']){ 
															echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-map-marker"></i> Country</div><div class="detail_colon">:</div><div class="detail_con">'.$objCommon->html2text($allBookings['bm_country']).'</div> </div>'; 
														}
														if(isset($allBookings['bm_allowance'])){ 
															$bm_allowance		=	($allBookings['bm_allowance']==1)?"Yes":"No";
															echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-ticket"></i> Air Ticket</div><div class="detail_colon">:</div><div class="detail_con">'.$bm_allowance.'</div> </div>'; 
														}
														if($bookBudget){ 
															echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-dollar"></i> Budget</div><div class="detail_colon">:</div><div class="detail_con">'.$bookBudget.'</div> </div>'; 
														}
														if($allBookings['bm_email'] !='' || $allBookings['bm_phone'] !=''){ 
															$bm_email			=	($allBookings['bm_email'])?$objCommon->html2text($allBookings['bm_email'])."<br>":"";
															$bm_phone			=	($allBookings['bm_phone'])?$objCommon->html2text($allBookings['bm_phone']):"";	
															echo '<div class="detail_row c"><div class="detail_hed">Contact</div><div class="detail_colon">:</div><div class="detail_con">'.$bm_email.$bm_phone.'</div> </div>'; 
														}
														if($allBookings['bm_attachement']){ 
															$downLoadFile		=	SITE_ROOT_AWS_IMAGES.'uploads/booking_attachement/'.$objCommon->html2text($allBookings['bm_attachement']);
															echo '<div class="detail_row c"><div class="detail_hed">Attachment</div><div class="detail_colon">:</div><a class="detail_con download" download href="'.$downLoadFile.'">Download</a> </div>'; 
														}
														?>
                                                        
                                                    </div>
                                                    <div class="duration">
                                                    	<h4>Work Duration</h4>
                                                        <div class="duration_dates">
                                                        	<div class="date round5">
                                                            	<div class="from">From</div>
                                                                <div class="day"><?php echo date('d',strtotime($bookFrom));?></div>
                                                                <div class="month"><?php echo date('F Y',strtotime($bookFrom));?></div>
                                                            </div>
                                                            <div class="date round5">
                                                            	<div class="from">To</div>
                                                                <div class="day"><?php echo date('d',strtotime($bookTo));?></div>
                                                                <div class="month"><?php echo date('F Y',strtotime($bookTo));?></div>
                                                            </div>
                                                        </div>
                                                    </div>
													
                                                    <div class="clr"></div>
                                                </div>
                                                <div class="side_box">
                                                	<div class="thump"><img src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.$objCommon->html2text($allBookings['upi_img_url'])?>" align="profile-pic" /></div>
                                                    <div class="prof_name"><?php echo $nameBooked;?></div>
                                                    <div class="prof_user_name">@<?php echo $objCommon->html2text($allBookings['usl_fameuz']);?></div>
                                                    <div>
														<?php
														echo ($allBookings['email'])?'<p>Profession: '.$objCommon->html2text($allBookings['c_name']).'</p>':'';
														echo ($allBookings['email'])?'<p><span class="bold">Email: '.$objCommon->html2text($allBookings['email']).'</span></p>':'';
														echo ($allBookings['p_phone'])?'<p><span class="bold">Phone: '.$objCommon->html2text($allBookings['p_phone']).'</span></p>':'';
														?>
                                                        <a href="<?php echo SITE_ROOT.$objCommon->html2text($allBookings['usl_fameuz'])?>" class="view round5" target="_blank">Visit Profile</a>
                                                    </div>
                                                </div>
                                                <div class="clr"></div>
                                            </div>
                                        </div>
										<?php
										}
										echo '<div class="paginationDiv">'.$pagination_output.'</div>';
										 }else{
											 echo '<p> No Results found... </p>';
										 }
										 ?>
                                    </div>
                                </div>
                            </div></div></div> </div>
                            <div class="clr"></div>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
		$(document).ready(function(e) {
			$('.details').click(function(e) {
				var t_data = $(this).parent().parent().parent();
                t_data.toggleClass('active');
				t_data.siblings().removeClass('active');
				return false;
            });
			$('.user_profile .sidebar .booking .t_data .data_hed .hash .detail_setting').click(function(e) {
				$(this).find('.setting_option').fadeToggle();
            });
			
			
			$('.delete_booking').click(function(){
				var delid = $(this).data("delid");
				var elem = $(this).closest('.item');
				var that			=	this;
				$.confirm({
					'title'		: 'Delete Confirmation',
					'message'	: 'You are about to delete this item ?',
					'buttons'	: {
					'Yes'	: {
					'class'	: 'blue',
					'action': function(){
					elem.slideUp();
					window.location.href = '<?php echo SITE_ROOT?>access/delete_book.php?action=del_book&bookId='+delid;
					}
					},
					'No'	: {
					'class'	: 'gray',
					'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
					}
					}
				});
			
			});
        });
	</script>