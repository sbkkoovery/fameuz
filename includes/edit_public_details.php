<?php
@session_start();
include_once("site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/users.php");
include_once(DIR_ROOT."class/user_privacy.php");
include_once(DIR_ROOT."class/languages.php");
include_once(DIR_ROOT."class/user_categories.php");
include_once(DIR_ROOT."class/ethnicity.php");
$objCommon		   =	new common();
$objUsers			=	new users();
$objUserPrivacy	  =	new user_privacy();
$objLanguages		=	new languages();
$objUserCategories   =	new user_categories();
$getUserPrivacy	  =	$objUserPrivacy->getRow("user_id=".$_SESSION['userId']);
$objEthnicity		=	new ethnicity();
$getAllEthnicity	 =	$objEthnicity->getAll("","ethnicity_name");
$getUserDetails	  =	$objUsers->getRowSql("SELECT user.*,ucat.uc_m_id,ucat.uc_c_id,ucat.uc_s_id,cat.c_book_by_me,cat.c_book_me,cat.c_model_details_yes,personal.p_dob,personal.p_gender,personal.p_ethnicity,personal.p_languages,personal.p_about,personal.n_id,personal.p_city,personal.p_country 
											  FROM users AS user 
											  LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id 
											  LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id   
											  LEFT JOIN personal_details as personal ON user.user_id = personal.user_id 
											  WHERE user.status=1 AND user.user_id=".$_SESSION['userId']);
$getAllLanguages	 =	$objLanguages->getAll("","languages_name");
foreach($getAllLanguages as $allLanguages){
		$langStr	.=	'"'.$allLanguages['languages_name'].'",';
}
?>
<!-- Edit Public Details-->
<p class="pic_dis">* These details are public and visible to all</p>
<form action="<?php echo SITE_ROOT?>access/edit_profile.php?action=edit_public_details" method="post" enctype="multipart/form-data">
	<div class="row edit-select">
	<div class="col-sm-9">
    	<div class="row">
        	<div class="col-sm-6">
                <div class="form-group edit-field">
                    <label>Display Name</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-tag"></i></span>
                        <input type="text" name="pub_display_name" id="pub_display_name" value="<?php echo ($getUserDetails['display_name'])?$objCommon->html2text($getUserDetails['display_name']):'';?>" >
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
        	<div class="col-sm-9 col-md-9 col-lg-7">
             <div class="edit-field">
            <label>Date Of Birth</label>
            </div>
                <div class="row">
                <?php
					$getDobEdit		=	$getUserDetails['p_dob'];
					$explDobEdit	   =	explode("-",$getDobEdit);
				?>
                    <div class="col-sm-4">
                    <div class="input-group edit-field">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <select name="pub_dob_date" id="pub_dob_date"  class="form-control">
                                <option value="0">DD</option>
                                <?php
                                for($dateI=1;$dateI<=31;$dateI++){
                                ?>
                                <option value="<?php echo sprintf("%02d",$dateI)?>" <?php echo ($explDobEdit[2]==sprintf("%02d",$dateI))?'selected="selected"':''?>><?php echo sprintf("%02d",$dateI)?></option>
                                <?php
                                }
                                ?>
                            </select>
                            </div>
                    </div>
                    <div class="col-sm-2 birthdayte">
						 <select name="pub_dob_month" id="pub_dob_month" class="form-control">
                    <option value="0">MM</option>
                    <?php
                    for($monthI=1;$monthI<=12;$monthI++){
                    ?>
                    <option value="<?php echo sprintf("%02d",$monthI)?>" <?php echo ($explDobEdit[1]==sprintf("%02d",$monthI))?'selected="selected"':''?> ><?php echo sprintf("%02d",$monthI)?></option>
                    <?php
                    }
                    ?>
                </select>
                           
                    </div>
                    <div class="col-sm-5">
                    <div class="input-group move-him edit-field">
						  <select name="pub_dob_year" id="pub_dob_year" class="form-control privacy-set">
                    <option value="0">YY</option>
                    <?php
                    for($yearI=date(Y);$yearI>=(date(Y)-80);$yearI--){
                    ?>
                    <option value="<?php echo $yearI?>" <?php echo ($explDobEdit[0]==$yearI)?'selected="selected"':''?> ><?php echo $yearI?></option>
                    <?php
                    }
                    ?>
                </select>
                         <span class="input-group-addon edited-privacy"><i class="fa fa-cog"></i>
            <span class="drop_down">
                <?php
					$exploPrivacyDob			=	explode(",",$getUserPrivacy['uc_p_dob']);
					?>
              <span class="arrow"><img alt="status_option_arrow" src="<?php echo SITE_ROOT?>images/status_option_arrow.png"></span>
                <span class="chkbox"><input type="checkbox" name="per_dob_privacy1" value="1" <?php echo ($exploPrivacyDob[0]==1)?'checked':''?> />Public</span>
                <span class="chkbox"><input type="checkbox" name="per_dob_privacy2" value="1" <?php echo ($exploPrivacyDob[1]==1)?'checked':''?> />Friends</span>
                <span class="chkbox"><input type="checkbox"  name="per_dob_privacy3" value="1" <?php echo ($exploPrivacyDob[2]==1)?'checked':''?>  />Professionals</span>
                <span class="chkbox"><input type="checkbox"  name="per_dob_privacy4" value="1" <?php echo ($exploPrivacyDob[3]==1)?'checked':''?>  />Only for me</span>
                </span>
            </span>
            </div>
                    </div>
                </div>
            </div>
        </div>
          <p class="click"> Click on the icon to change privacy</p>
        <div class="row">
        <div class="col-sm-6 col-md-6 col-lg-4">
         <div class="edit-field">
            <label>Gender</label>
            </div>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-mars"></i></span>
                          <select name="pub_gender" id="pub_gender" class="form-control">
                            <option value="0">Select</option>
                            <option value="2" <?php echo ($getUserDetails['p_gender']==2)?'selected="selected"':''?>>Female</option>
                            <option value="1" <?php echo ($getUserDetails['p_gender']==1)?'selected="selected"':''?>>Male</option>
                        </select>
                            </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                   <div class="edit-field">
            <label>Ethnicity</label>
            </div>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-info"></i></span>
                            <select name="pub_ethnicity" id="pub_ethnicity" class="form-control">
                            <option value="0">Please Select</option>
                            <?php
                            foreach($getAllEthnicity as $allEthnicity){
                            ?>
                            <option value="<?php echo $allEthnicity['ethnicity_id']?>" <?php echo ($allEthnicity['ethnicity_id']==$getUserDetails['p_ethnicity'])?'selected=selected':''?> ><?php echo $objCommon->html2text($allEthnicity['ethnicity_name'])?></option>
                            <?php
                            }
                            ?>
                        </select>
                            </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                    <div class="form-group edit-field">
                    <label>Language</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-tag"></i></span>
                       <input type="text"  id="pub_language" name="pub_language" value="<?php echo $getUserDetails['p_languages']?>">
                    </div>
                </div>
                    </div>
                    </div>
    </div>
    <div class="col-sm-3">
        <div class="help">
            <h1><span class="qstn">?</span> Help...?<span class="h_close"><a href="#"><i class="fa fa-close"></i></a></span></h1>
            <p>Please review our frequently 
            asked questions to the left if 
            you need help</p>
            <p>If you have a questions that is 
            not answered there, you can 
            contact us (response time will 
            usually be 1 to 2 business days)</p>
        </div>
    </div>
</div>
<?php
include_once(DIR_ROOT."class/model_disciplines.php");
$objModelDisciplines		  =	new model_disciplines();
$getAllModelDesciplines	   =	$objModelDisciplines->getAll("","model_dis_name");
if($getUserDetails['uc_m_id']==2 && $getUserDetails['c_model_details_yes']==1){
include_once(DIR_ROOT."class/model_chest.php");
include_once(DIR_ROOT."class/model_collar.php");
include_once(DIR_ROOT."class/model_dress.php");
include_once(DIR_ROOT."class/model_eyes.php");
include_once(DIR_ROOT."class/model_height.php");
include_once(DIR_ROOT."class/model_hips.php");
include_once(DIR_ROOT."class/model_jacket.php");
include_once(DIR_ROOT."class/model_shoes.php");
include_once(DIR_ROOT."class/model_trousers.php");
include_once(DIR_ROOT."class/model_waist.php");
include_once(DIR_ROOT."class/model_hair.php");
include_once(DIR_ROOT."class/model_details.php");
$objModelChest				=	new model_chest();
$objModelCollar			   =	new model_collar();
$objModelDress				=	new model_dress();
$objModelEyes				 =	new model_eyes();
$objModelHeight			   =	new model_height();
$objModelHips				 =	new model_hips();
$objModelJacket			   =	new model_jacket();
$objModelShoes				=	new model_shoes();
$objModelTrousers			 =	new model_trousers();
$objModelWaist				=	new model_waist();
$objModelDetails			  =	new model_details();
$objModelHair				 =	new model_hair();
$getAllModelChest			 =	$objModelChest->getAll("");
$getAllModelCollar			=	$objModelCollar->getAll("");
$getAllModelDress			 =	$objModelDress->getAll("");
$getAllModelEyes			  =	$objModelEyes->getAll("","me_name");
$getAllModelHeight			=	$objModelHeight->getAll("");
$getAllModelHips			  =	$objModelHips->getAll("");
$getAllModelJacket			=	$objModelJacket->getAll("");
$getAllModelShoes			 =	$objModelShoes->getAll("");
$getAllModelTrousers		  =	$objModelTrousers->getAll("");
$getAllModelwaist			 =	$objModelWaist->getAll("");
$getModelDetails			  =	$objModelDetails->getRow("user_id=".$_SESSION['userId']);
$getAllModelHair			  =	$objModelHair->getAll("","mhair_name");
?>
    <h2 class="bg_heading">Your Details</h2>
   			<div class="row edit-select">
            <div class="col-sm-3">
         <div class="edit-field">
            <label>Eyes</label>
            </div>
                <select name="pub_model_eyes" id="pub_model_eyes">
                            <option value="0">Please Select</option>
                            <?php
							foreach($getAllModelEyes as $allModelEyes){
							?>
                            <option value="<?php echo $allModelEyes['me_id']?>" <?php echo ($getModelDetails['me_id']==$allModelEyes['me_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelEyes['me_name'])?></option>
                            <?php
							}
							?>
                        </select>
                         
                    </div>
                    <div class="col-sm-3">
         <div class="edit-field">
            <label>Hair</label>
            </div>
                    
                           <select name="pub_model_hair" id="pub_model_hair">
                            <option value="0">Please Select</option>
                            <?php
							foreach($getAllModelHair as $allModelHair){
							?>
                            <option value="<?php echo $allModelHair['mhair_id']?>" <?php echo ($getModelDetails['mhair_id']==$allModelHair['mhair_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelHair['mhair_name'])?></option>
                            <?php
							}
							?>
                        </select>
                          
                    </div>
                    </div>
                    <div class="row edit-select">
            <div class="col-sm-4 col-md-4 col-lg-3">
         <div class="edit-field">
            <label>Height</label>
            </div>
                   
                            <select name="pub_model_height" id="pub_model_height">
                            <option value="0">Please Select</option>
                            <?php
							foreach($getAllModelHeight as $allModelHeight){
							?>
                            <option value="<?php echo $allModelHeight['mh_id']?>" <?php echo ($getModelDetails['mh_id']==$allModelHeight['mh_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelHeight['mh_name'])?></option>
                            <?php
							}
							?>
                        </select>
                            
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-3">
         <div class="edit-field">
            <label>Waist</label>
            </div>
                    
                           <select name="pub_model_waist" id="pub_model_waist">
                            <option value="0">Please Select</option>
                            <?php
							foreach($getAllModelwaist as $allModelwaist){
							?>
                            <option value="<?php echo $allModelwaist['mw_id']?>" <?php echo ($getModelDetails['mw_id']==$allModelwaist['mw_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelwaist['mw_name'])?></option>
                            <?php
							}
							?>
                        </select>
                            
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-3">
         <div class="edit-field">
            <label>Hips</label>
            </div>
                  
                              <select name="pub_model_hips" id="pub_model_hips">
                            <option value="0">Please Select</option>
                            <?php
							foreach($getAllModelHips as $allModelHips){
							?>
                            <option value="<?php echo $allModelHips['mhp_id']?>" <?php echo ($getModelDetails['mhp_id']==$allModelHips['mhp_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelHips['mhp_name'])?></option>
                            <?php
							}
							?>
                        </select>
                          
                    </div>
                    </div>
                    <div class="row edit-select">
            <div class="col-sm-4 col-md-4 col-lg-3">
         <div class="edit-field">
            <label class="bust">Bust</label>
            </div>
                    
                            <select name="pub_model_chest" id="pub_model_chest">
                            <option value="0">Please Select</option>
                            <?php
							foreach($getAllModelChest as $allModelChest){
							?>
                            <option value="<?php echo $allModelChest['mc_id']?>" <?php echo ($getModelDetails['mc_id']==$allModelChest['mc_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelChest['mc_name'])?></option>
                            <?php
							}
							?>
                        </select>
                            
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-3">
         <div class="edit-field">
            <label>Shoe</label>
            </div>
                    
                             <select name="pub_model_shoe" id="pub_model_shoe">
                            <option value="0">Please Select</option>
                            <?php
							foreach($getAllModelShoes as $allModelShoes){
							?>
                            <option value="<?php echo $allModelShoes['ms_id']?>" <?php echo ($getModelDetails['ms_id']==$allModelShoes['ms_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelShoes['ms_name'])?></option>
                            <?php
							}
							?>
                        </select>
                     
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-3 profile_female">
         <div class="edit-field">
            <label>Dress</label>
            </div>
                  
                             <select name="pub_model_dress" id="pub_model_dress">
                            <option value="0">Please Select</option>
                            <?php
							foreach($getAllModelDress as $allModelDress){
							?>
                            <option value="<?php echo $allModelDress['mdress_id']?>" <?php echo ($getModelDetails['mdress_id']==$allModelDress['mdress_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelDress['mdress_name'])?></option>
                            <?php
							}
							?>
                        </select>
                            
                    </div>
                    </div>
                    
                    <div class="row edit-select profile_male">
            <div class="col-sm-4 col-md-4 col-lg-3">
         <div class="edit-field">
            <label>Jacket</label>
            </div>
                    
                             <select name="pub_model_jacket" id="pub_model_jacket">
                            <option value="0">Please Select</option>
                            <?php
							foreach($getAllModelJacket as $allModelJacket){
							?>
                            <option value="<?php echo $allModelJacket['mj_id']?>" <?php echo ($getModelDetails['mj_id']==$allModelJacket['mj_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelJacket['mj_name'])?></option>
                            <?php
							}
							?>
                        </select>
                            
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-3">
         <div class="edit-field">
            <label>Trousers</label>
            </div>
                    
                         <select name="pub_model_trousers" id="pub_model_trousers">
                            <option value="0">Please Select</option>
                            <?php
							foreach($getAllModelTrousers as $allModelTrousers){
							?>
                            <option value="<?php echo $allModelTrousers['mt_id']?>" <?php echo ($getModelDetails['mt_id']==$allModelTrousers['mt_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelTrousers['mt_name'])?></option>
                            <?php
							}
							?>
                        </select>
                          
                    </div>
                    <div class="col-sm-4 col-md-4 col-lg-3">
         <div class="edit-field">
            <label>Collar</label>
            </div>
                    
                              <select name="pub_model_collar" id="pub_model_collar">
                            <option value="0">Please Select</option>
                            <?php
							foreach($getAllModelCollar as $allModelCollar){
							?>
                            <option value="<?php echo $allModelCollar['mcollar_id']?>" <?php echo ($getModelDetails['mcollar_id']==$allModelCollar['mcollar_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelCollar['mcollar_name'])?></option>
                            <?php
							}
							?>
                        </select>
                            
                    </div>
                    </div>
<?php
}
include_once(DIR_ROOT."includes/edit_location.php");
if($getUserDetails['uc_m_id'] ==2){
include_once(DIR_ROOT."class/sub_category.php");
$objSubCategory				=	new sub_category();
$getAllSubCategory			 =	$objSubCategory->getAll("c_id=".$getUserDetails['uc_c_id'],"s_name");
if(count($getAllSubCategory)>0){
	$editSubCat		=	$getUserDetails['uc_s_id'];
	$exploEditSubCat   =	explode(",",$editSubCat);
	$exploEditSubCat   =	array_filter($exploEditSubCat);
?>
    <h2 class="bg_heading">Interested In</h2>
    <div class="row displn">
    	<?php
		foreach($getAllSubCategory as $allSubCategory){
			?>
            <div class="col-sm-4">
            <label><input type="checkbox" name="pub_sub_category[]" value="<?php echo $allSubCategory['s_id']?>" <?php echo (in_array($allSubCategory['s_id'], $exploEditSubCat))?'checked="checked"':''?>> <?php echo $objCommon->html2text($allSubCategory['s_name'])?></label>
        	</div>
            <?php
		}
		?>
    </div>
<?php
}
?>
    <h2 class="bg_heading">Modeling Disciplines</h2>
    <div class="displn row">
    <?php
	foreach($getAllModelDesciplines as $allModelDesciplines){
		$getModelDesci		=	$getModelDetails['model_dis_id'];
		$explModelDesci	   =	explode(",",$getModelDesci);
		$explModelDesci	   =	array_filter($explModelDesci);
	?>
    <div class="field_box col-sm-4"><label><input type="checkbox" name="pub_model_descipline[]" value="<?php echo $allModelDesciplines['model_dis_id']?>" 
	<?php echo (in_array($allModelDesciplines['model_dis_id'], $explModelDesci))?'checked="checked"':''?> > <?php echo $objCommon->html2text($allModelDesciplines['model_dis_name'])?></label></div>
    <?php
	}
	?>
    </div>
	<?php
}
?>
<h2 class="bg_heading">Video Introdunction</h2>
<div class="field_box_section">
    <div class="field_box" style="width:360px;">
        <div style="width:235px;" class="fiel_border">
            <div class="text">
               <div class="file_up"> <font style=" padding-top:3px;float:left;">Choose file</font> <input type="file" name="video_introduction"> <span class="file_browse">Browse</span></div>
            </div>
        </div>
    </div>
	<div class="field_box" style="width:50px; padding-top:5px;">OR</div>
    <div class="field_box edit-field"><input type="text" name="video_intro_youtube" placeholder="youtube url here" /></div>
</div>
    <h2 class="bg_heading">About Me</h2>
    <div class="field_box_section displn">
        <div class="field_box">
            <textarea  id="pub_about" name="pub_about"><?php echo ($getUserDetails['p_about'])?$objCommon->displayEditor($getUserDetails['p_about']):'';?></textarea>
        </div>
    </div>
    <div class="field_box_section">
        <div class="field_box left50">
            <div><input type="submit" value="Save Changes"></div>
        </div>
        <div class="field_box a_right left50">
            <div class="fiel_border">
                <div class="text">
                    <div class="select_bg">
                        <select>
                            <option>Only members</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="clr"></div>
    </div>
</form>

<script language="javascript" type="text/javascript">
$(document).ready(function(){
	var genderValDef		 =	$("#pub_gender").val();
	if(genderValDef){
		showModelDetails(genderValDef);
	}
	$("#pub_gender").change(function(){
		var genderVal		=	$(this).val();
		showModelDetails(genderVal);
	});
	function showModelDetails(gender){
		$(".profile_male").hide();
		$(".profile_female").hide();
		if(gender==1){
			$(".profile_male").show();
			$(".profile_female").hide();
			$(".bust").html("Chest");
		}else if(gender==2){
			$(".profile_male").hide();
			$(".profile_female").show();
			$(".bust").html("Bust");
		}
	}
});
$('.edited-privacy').on("click",function(e) {
	$(this).find('.drop_down').slideToggle();
});
$('.h_close a').click(function(e){
	$(this).parent().parent().parent().remove();
	return false;
});

</script>
<script>
  $(function() {
    var availableTags = [
	<?php echo $langStr?>
    ];
    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
 
    $( "#pub_language" )
      // don't navigate away from the field on tab when selecting an item
      .bind( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
        minLength: 0,
        source: function( request, response ) {
          // delegate back to autocomplete, but extract the last term
          response( $.ui.autocomplete.filter(
            availableTags, extractLast( request.term ) ) );
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
      });
  });
  </script>
  