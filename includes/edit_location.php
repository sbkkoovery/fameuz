<?php
include_once(DIR_ROOT."class/nationality.php");
$objNationality					=	new nationality();
?>
<script>
//$(document).ready(function(){
	//initialize();
//});
window.onload=function(){initialize();};
var placeSearch, autocomplete;
var componentForm = {
  locality: 'long_name',
  country: 'long_name'
};

function initialize() {
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('locality')),
      { types: ['geocode'] });
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    fillInAddress();
  });
}
function fillInAddress() {
  var place = autocomplete.getPlace();
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
// [END region_geolocation]

    </script>
<h2 class="bg_heading">Current Location</h2>
  <div class="row edit-select">
            <div class="col-sm-4 col-md-4 col-lg-3">
         <div class="edit-field">
            <label>Nationality</label>
            </div>
            <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-flag"></i></span>
                <select name="pub_nationality">
                            <option value="">Please Select</option>
                            <?php 
                            $nationalityList	=	$objNationality->getAll();
                            foreach($nationalityList as $nationality){?>
                            <option value="<?php echo $nationality['n_id']; ?>" <?php echo ($getUserDetails['n_id']==$nationality['n_id'])?'selected':''?>><?php echo $nationality['n_name']; ?></option>
                            <?php }?>
                        </select>
                       </div>     
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group edit-field">
                    <label>Location</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                      <input type="text" name="pub_city" onFocus="geolocate()" autocomplete="off"  id="locality"  value="<?php echo $objCommon->html2text($getUserDetails['p_city']);?>" />
                    </div>
                </div>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group edit-field">
                    <label>Country</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                      <input type="text" name="pub_country" autocomplete="off"  id="country" value="<?php echo $objCommon->html2text($getUserDetails['p_country']);?>" />
                    </div>
                </div>
                    </div>
  </div>
<div class="field_box_section">
</div>