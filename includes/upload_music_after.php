<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.validate.js"></script>
<div class="row">
            <div class="col-md-12">
				<div class="row">
                <div class="col-sm-12">
                <div class="progressbar1 progress">
               		<div class="progression1 bar"></div>
					 <div class="percent">0%</div>
                </div>
                <div class="upload-info">
               <!-- <p><span class="vid-name">Home.mp4/</span><span class="vid-size">4.50MB of 4.50MB/</span><span class="vid-time">00:00 Remaining </span></p>-->
                </div>
                </div>
                </div>
                <div class="row">
                <div class="col-sm-12 vid-info-title">
                <p><strong>Basic Info</strong></p>
                <form class="video-upload" id="uploadmusic" method="post" action="<?php echo SITE_ROOT.'access/add_music_details.php'?>" enctype="multipart/form-data">
                	 <div class="form-group upload-help">
                        <label for="exampleInputEmail1">Title</label>
                        <div class="help-text">Music are more interesting when they have creative titles. We know you can do better than "My Video."</div>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="img_title">
                      </div>
					   <div class="form-group upload-help">
                        <label for="exampleInputEmail1">Artist</label>
                        <input type="text" class="form-control"  name="music_artist">
                      </div>
					   <div class="form-group upload-help">
                        <label for="exampleInputEmail1">Image</label>
						<input type="file" class="form-control" name="music_thumb" />
                      </div>
                      <div class="form-group upload-help">
                        <label for="exampleInputPassword1">Description</label>
                         <div class="help-text"></div>
                        <textarea class="form-control" id="exampleInputPassword1" name="img_descr"></textarea>
                      </div>
                      <div class="row">
                      <div class="col-sm-6">
                      <div class="form-group upload-help">
                        <label for="exampleInputEmail1">Tags</label>
                        <div class="help-text">Add related tags (separated by commas, please!).</div>
                        <input type="text" class="form-control"  name="video_tags">
                      </div>
                      </div>
                      <div class="col-sm-6">
                      <div class="form-group upload-help">
                        <label for="exampleInputEmail1">Privacy Settings</label>
                        <div class="help-text">&nbsp;</div>
                        <select class="form-control" name="privacy">
                          <option value="1">Public</option>
						  <option value="2">Friends</option>
						  <option value="4">Only for me</option>
                         </select>
                      </div>
                      </div>
					  <div class="row pubVidDet">
					  </div>
					  
                      </div>
					  <div class="loadPublichBtn" style="display:none;"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/album_preloader.GIF" /></div>
                      <button type="submit" class="btn btn-default publishBtn" style="display:none;">Publish</button>
                </form>
                </div>
                </div>
            </div>
            </div>
 <script type="text/javascript">
$("#uploadmusic").validate({
			rules: {
				img_title: "required",
				music_artist: "required"								
			},
			messages: {
				img_title: '<p style="color:#F30;">Please add a title</p>',
				music_artist: '<p style="color:#F30;">Please add artist</p>'
			}
    });
</script>