<link href="<?php echo SITE_ROOT?>css/user-profile.css" rel="stylesheet" type="text/css">
<link href="<?php echo SITE_ROOT?>css/jquery_scroll.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery-ias.min.js"></script>
<?php
include_once(DIR_ROOT."class/user_reviews.php");
$objUserReviews						=	new user_reviews();
$limit 								 =	6;
$page 								  =	(int) (!isset($_GET['p'])) ? 1 : $_GET['p'];
$filter_review						 =	$objCommon->esc($_GET['filter_review']);
if($filter_review == 'today'){
	$filterReviewSql				   =	' AND (review.review_date > DATE_SUB(NOW(), INTERVAL 1 DAY)) ';
}else if($filter_review == 'yesterday'){
	$filterReviewSql				   =	' AND (review.review_date BETWEEN DATE_ADD(CURDATE(), INTERVAL -1 day) AND CURDATE()) ';
}else if($filter_review == 'last-week'){
	$filterReviewSql				   =	' AND (review.review_date >= CURDATE() - INTERVAL DAYOFWEEK(CURDATE())+6 DAY AND review.review_date < CURDATE() - INTERVAL DAYOFWEEK(CURDATE())-1 DAY ) ';
}else if($filter_review == 'last-month'){
	$filterReviewSql				   =	' AND (review.review_date BETWEEN DATE_FORMAT(CURDATE() - INTERVAL 1 MONTH, "%Y-%m-01") AND LAST_DAY(CURDATE() - INTERVAL 1 MONTH)) ';
}else if($filter_review == 'last-year'){
	$filterReviewSql				   =	' AND (review.review_date >= DATE_SUB(NOW(),INTERVAL 1 YEAR)) ';
}else{
	$filterReviewSql				   =	'';
}
$sql_reviewsAll						=	"SELECT review.review_id,review.review_msg,review.review_rate,review.review_date,review.review_user_to_status,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz FROM user_reviews AS review LEFT JOIN users AS user ON review.user_id_to=user.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE review.review_status=1  AND  review.review_user_to_status != 0 ".$filterReviewSql." AND  review.user_id_by='".$_SESSION['userId']."' ORDER BY review.review_date desc ";
$start 								 =	($page * $limit) - $limit;
if(($objUserReviews->countRows($sql_reviewsAll)) > ($page * $limit) ){
	$next = ++$page;
}
$getMyAllReviews					   =	$objUserReviews->listQuery($sql_reviewsAll. " LIMIT ".$start.", ".$limit);
?>
    <div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="user_profile">
						<?php echo $objCommon->checkEmailverification();?>
						<div class="content">
                            	<div class="row"><div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9"><div class="pagination_box">
                                	<a href="<?php echo SITE_ROOT.'user/home'?>">Home <i class="fa fa-caret-right"></i></a>
                                    <a href="<?php echo SITE_ROOT.$getUserDetails['usl_fameuz']?>"> My profile </a><i class="fa fa-caret-right"></i></a>
        							 <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                    <a href="javascript:;" class="active"> My Reviews  </a>
                                    
                                      <?php
								include_once(DIR_ROOT."widget/notification_head.php");
								?>
                                </div></div>
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3"><div class="sidebar no-border">
                            	<div class="search_box"><form action="<?php echo SITE_ROOT.'user/search-result'?>" method="get"><input type="text" placeholder="Search"  name="keywordSearch" /></form></div>
                            </div></div></div>
                             <div class="row">
                                    <div class="col-lg-sp-2">
                            <?php
							include_once(DIR_ROOT."includes/profile_left.php");
							?>
                            </div>
							<div class="col-lg-sp-10"><div class="sidebar">
                            	<div class="clr"></div>
                                <div class="my_review_block">
									<div class="row">
										<div class="col-md-12">
											<div class="review_head">
												<ul>
													<li><a href="<?php echo SITE_ROOT.'user/my-reviews'?>" > Reviews About Me</a> </li>
													<li class="bgGreen"><a href="<?php echo SITE_ROOT.'user/my-reviews/me'?>" class="active"> Reviews By Me</a></li>
												</ul>
											</div>
											<p class="yellow">* Reviews by me</p>
											<?php echo $objCommon->displayMsg();?>
											<div class="row">
												<div class="col-md-9 wrap_review">
												<?php
												if(count($getMyAllReviews)>0){
													foreach($getMyAllReviews as $allMyReviews){
														$getrate			=	(int) $allMyReviews['review_rate'];
														$getRateStyle	   =	'style="width:'.($getrate*20).'%;"';
													?>
													<div class="review_box item_review" id="item_review-<?php echo $allMyReviews['review_id']?>">
														<div class="review_img">
															<span class="arrow_right"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/status_option_arrow_right.png" /></span>
															<img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($getUserDetails['upi_img_url'])?$getUserDetails['upi_img_url']:'profile_pic.jpg'?>" class="img-responsive" />
														</div>
														<div class="left82 content_desc_2">
															<div class="left100">
																<div class="review_content">
																	<p class="review_text"><?php echo $objCommon->html2text($allMyReviews['review_msg']);?></p>
																</div>
																<div class="review_action">
																	<div class="review_to_action_box">
																		<div class="img_to">
																			<a href="<?php echo SITE_ROOT.$objCommon->html2text($allMyReviews['usl_fameuz'])?>"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($allMyReviews['upi_img_url'])?$allMyReviews['upi_img_url']:'profile_pic.jpg'?>" width="50"  class="img-responsive"/></a>
																		</div>
																		<p><a href="<?php echo SITE_ROOT.$objCommon->html2text($allMyReviews['usl_fameuz'])?>"><?php echo ($allMyReviews['display_name'])?$objCommon->html2text($allMyReviews['display_name']):$objCommon->html2text($allMyReviews['first_name']);?></a></p>
																	</div>
																	<div class="pull-right">
																		<img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/review_top_arrow.png" />
																		<div class="review_action_drop">
																			<div class="arrow"><img src="<?php echo SITE_ROOT_AWS_IMAGES;?>images/status_option_arrow.png" alt="status_option_arrow" /></div>
																			<ul class="review_action_ul">
																				<li><a href="javascript:;" data-review-id="<?php echo $allMyReviews['review_id']?>" class="delete_review" ><i class="fa fa-trash-o"></i> Delete Review</a></li>
																			</ul>		
																		</div>
																	</div>
																</div>
																</div>
																<div  class="posted_rev_box left100">
																	
																	<div class="left100">
																		<div class="rating_box pull-left"><div <?php echo $getRateStyle?> class="rating_yellow"></div></div>
																		<div class="pull-right">
																			<div class="rate_edit pull-left"><a href="<?php echo SITE_ROOT.'user/'.$objCommon->html2text($allMyReviews['usl_fameuz']).'/add-review/review-'.$allMyReviews['review_id']?>"><i class="fa fa-pencil"></i> Edit</a></div>
																			<div class="rate_time_btm pull-left"><i class="fa fa-clock-o"></i> <?php echo date("d M , Y",strtotime($objCommon->local_time($allMyReviews['review_date'])));?></div>
																		</div>
																	</div>
																</div>
															</div>
													</div>
													<?php
													}
													if (isset($next)){ ?>
														<div class="nav">
														<a href="<?php echo SITE_ROOT.'user/my-reviews/me/'.$next?>">Next</a>
														</div>
														<?php
													}
													
												}else{
													echo '<p>No reviews found....</p>';
												}
												?>
												</div>
												<div class="col-md-3">
													
													<div class="review_search_date pull-left">
													<span class="arrow_left"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/status_option_arrow_left.png" /></span>
														<ul>
															<li><a href="<?php echo SITE_ROOT.'user/my-reviews/me/filter-today'?>">Today's Reviews</a></li>
															<li><a href="<?php echo SITE_ROOT.'user/my-reviews/me/filter-yesterday'?>">Yesterday's Review</a></li>
															<li><a href="<?php echo SITE_ROOT.'user/my-reviews/me/filter-last-week'?>">Last Week Reviews</a></li>
															<li><a href="<?php echo SITE_ROOT.'user/my-reviews/me/filter-last-month'?>">Last Month Reviews</a></li>
															<li><a href="<?php echo SITE_ROOT.'user/my-reviews/me/filter-last-year'?>">Last Year Reviews</a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
                            </div></div></div> </div>
                            <div class="clr"></div>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script language="javascript" type="text/javascript">
$(".review_action").on("click","img",function(){
		//$(".review_action_drop").slideUp();
        $(this).next(".review_action_drop").fadeToggle();
		$(this).toggleClass("act");
});
$(".review_action_drop").on("click",".delete_review",function(){
	var review_id		=	$(this).data("review-id");
	var changeStatme	 =	0;
	var that			 =	this;
	var elem = $(this).closest('.item');
	$.confirm({
					'title'	  : 'Delete Confirmation',
					'message'	: 'You are about to delete this item ?',
					'buttons'	: {
					'Yes'	: {
					'class'	: 'blue',
					'action': function(){
					elem.slideUp();
					if(review_id){
						$.get('<?php echo SITE_ROOT?>ajax/change_my_review_status.php',{'review_id':review_id,'changeStatme':changeStatme},function(data){
							$(".review_action_drop").slideUp();
							$(that).parent().parent().parent().parent().parent().parent().parent().parent().hide();
						});
					};
					}
					},
					'No'	: {
					'class'	: 'gray',
					'action': function(){
						$(".review_action_drop").slideUp();
					}	// Nothing to do in this case. You can as well omit the action property.
					}
					}
				});
});
$(document).ready(function(e) {
	 jQuery.ias({
		container : '.wrap_review', // main container where data goes to append
		item: '.item_review', // single items
		pagination: '.nav', // page navigation
		next: '.nav a', // next page selector
		loader: '<img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/ajax-loader.gif"/>', // loading gif
		triggerPageThreshold: 3 // show load more if scroll more than this
	});
});
</script>