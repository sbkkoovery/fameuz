<?php
@session_start();
ob_start();
include_once(DIR_ROOT."class/common_class.php");
$objCommon				=	new common();
$pageName				 =	basename($_SERVER['PHP_SELF']);
$cssName				  =	explode(".",$pageName);
$cssName				  =	$cssName[0];
if(isset($_SESSION['userId'])){
	include_once(DIR_ROOT."class/user_chat_status.php");
	$objUserChatStatus   =	new user_chat_status();
	$getUserDetails	  =	$objUsers->getRowSql("SELECT user.*,profileImg.upi_img_url,profileImg.upi_id,social.usl_fameuz,ucat.uc_m_id,ucat.uc_c_id,ucat.uc_s_id,cat.c_book_by_me,cat.c_book_me,personal.p_gender,compl.complete_primary,compl.complete_personal,
compl.complete_public,compl.complete_profile_img,compl.complete_social,vidInto.vi_url,vidInto.vi_thumb,vidInto.vi_type,cat.c_name FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id  LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 LEFT JOIN personal_details as personal ON user.user_id = personal.user_id LEFT JOIN user_profile_complete AS compl ON user.user_id = compl.user_id LEFT JOIN video_introduction AS vidInto ON user.user_id = vidInto.user_id WHERE (user.status=1 OR user.status=4) AND user.user_id=".$_SESSION['userId']);
	$book_by_me		  =	$getUserDetails['c_book_by_me'];
	$book_me			 =	$getUserDetails['c_book_me'];
	$displayName		 =	$objCommon->displayNameSmall($getUserDetails,18);
	$_POST['ucs_last_seen']	=	date('Y-m-d H:i:s');
	$objUserChatStatus->update($_POST,"user_id=".$_SESSION['userId']);
	$profileComplStatus		=	(int) $getUserDetails['complete_primary']+(int) $getUserDetails['complete_personal']+(int) $getUserDetails['complete_public']+(int) $getUserDetails['complete_profile_img']+(int) $getUserDetails['complete_social'];
	$profileComplStatus		=	($profileComplStatus !='' || $profileComplStatus !=0)?$profileComplStatus:'10';
}
if(@$ogTitle==''){
	@$ogTitle				  =	'Fameuz.com';
}
if(@$ogDescr==''){
	@$ogDescr				  =	'Fameuz.com';
}
if(@$ogImage==''){
	@$ogImgUser				=	($getUserDetails['upi_img_url'])?$getUserDetails['upi_img_url']:'profile_pic.jpg';
	@$ogImage				  =	SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.$ogImgUser;
}

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FAMEUZ</title>
    <!--------- Master Structure if css Where other css files are included -------->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
    <link href="<?php echo SITE_ROOT?>css/master.css" rel="stylesheet" type="text/css">
    <link href="<?php echo SITE_ROOT?>css/owl.carousel.css" rel="stylesheet" type="text/css">
    <!--------- End Master Structure if css Where other css files are included -------->
    <meta property="og:title"         content="<?php echo $ogTitle?>" />
    <meta property="og:description"   content="<?php echo $ogDescr?>" />
    <meta property="og:image"         content="<?php echo $ogImage?>" />
    <link href="<?php echo SITE_ROOT?>css/<?php echo $cssName; ?>.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="<?php echo SITE_ROOT_AWS_IMAGES?>images/favicon.png">
    <script src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery.min.js"  type="text/javascript" ></script>
    <script src="<?php echo SITE_ROOT_AWS_CDN?>js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo SITE_ROOT_AWS_CDN?>js/jqClock.min.js" type="text/javascript"></script>
   <?php /*?> <script src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery.nicescroll.min.js" type="text/javascript"></script><?php */?>
   	<script src="<?php echo SITE_ROOT_AWS_CDN?>js/lobibox.js" type="text/javascript"></script>
 	<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/perfect-scrollbar.js"></script>
	<script type="text/javascript">
	$(document).ready(function(e) {
			$.get('<?php echo SITE_ROOT?>ajax/local_time.php',function(data){
				var customtimestamp = new Date(data);
				customtimestamp = customtimestamp.getTime();
				$(".curTm").clock({"calendar":"true","format":"12","timestamp":customtimestamp});
			});
			var countryName			=	'<?php echo $_SESSION['country_user']['country_name']?>';
			var countryFlag			=	'<?php echo $_SESSION['country_user']['country_flag']?>';
			if(countryName ==''){
				 $.get('<?php echo SITE_ROOT?>action/ajax/get-country-flag',function(data){
					 $(".countryTimeZone .curTm").after(data);
				 });
			}else{
				$(".countryTimeZone .curTm").after('<span>'+countryFlag+'</span><font class="myCountry">'+countryName+'</font></div>');
			}
			
			$(".user_login .logout").click(function(e) {
				$(this).next(".dropdown_option").show();
				$(this).toggleClass("act");
			});
			;
			$("body").mouseup(function(e){
					var subject = $(".dropdown_option"); 
					if(e.target.id != subject.attr('id') && !subject.has(e.target).length) {
						 $(".dropdown_option").hide();
					}
			});
		});
	</script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	<div class="mob_menu left_menu">
    	<div id="close" class="a_right close_btn">
        	<a href="#">close<img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/mob_btn_close.png" alt="mob_btn_close" /></a>
        </div>
         <ul class="main_manu" id="mob_menu">
            <li><a href="<?php echo SITE_ROOT?>">Home </a></li>
				<li <?php echo (in_array($pageName,array('search.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'search?cat=members'?>"> Search       </a></li>
				<li <?php echo (in_array($pageName,array('post_jobs.php','jobs.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'jobs'?>">Jobs</a></li>
				<li <?php echo (in_array($pageName,array('new-comers.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'new-comers'?>">Newcomers   </a></li>
				<li <?php echo (in_array($pageName,array('music.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'music'?>">Music</a></li>
				<li <?php echo (in_array($pageName,array('videos.php','videos-single.php','video-upload.php','videos-search.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'videos'?>"> Video  </a></li>
				<li <?php echo (in_array($pageName,array('photos.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'photos'?>">Photos </a></li>
				<?php /*?><li <?php echo (in_array($pageName,array('games.php','games-search.php','game-single.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'games'?>">Games </a></li><?php */?>
				<li <?php echo (in_array($pageName,array('online-member.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'online'?>">Online</a></li>
				<li <?php echo (in_array($pageName,array('most-wanted.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'most-wanted'?>">Most Wanted</a></li>
				<?php /*?><li <?php echo (in_array($pageName,array('blog.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'blog'?>">Blogs</a></li>
				<li <?php echo (in_array($pageName,array('market-place.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'market-place'?>">Marketplace</a></li><?php */?>
        </ul>
    </div>
<?php
if($cssName=='index'){
?>
<div class="top_image_or_video">
    	<div class="background_image_or_video"><?php /*?><img src="<?php echo SITE_ROOT?>images/banner_image.jpg" alt="banner_image" /><?php */?></div>
  </div>
<?php
}
?>
  	<div class="top_section index-page">
    	<div class="logo_nav_section">
            <div class="container">
            	<div class="row mob_dis">
                	<div class="col-xs-8 logo  a_left"><a href="<?php echo SITE_ROOT?>"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/logo.png" alt="Logo" /></a></div>
                    <div class="col-xs-4 mob_menu_nav relative a_right">
                            <a href="#" class="mob_btn mob_menu_btn"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/mob_btn.png" alt="mob_btn" /></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2 logo mob_none"><a href="<?php echo SITE_ROOT?>"><img class="img-responsive" src="<?php echo SITE_ROOT_AWS_IMAGES?>images/logo.png" alt="Logo" /></a></div>
                    <div class="col-sm-10 nav_signup_margin relative">
                    	<div class="countryTimeZone"><div class="curTm">&nbsp;</div>
                    	<div>
                        <?php
                        if(isset($_SESSION['userId'])){
							//include_once(DIR_ROOT.'node/chat.php');
						?>
                        <div class="login_signup user_login">
                           <a href="#" class="user_thumb"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($getUserDetails['upi_img_url'])?$getUserDetails['upi_img_url']:'profile_pic.jpg'?>" alt="user_thumb" width="30"/></a>
                           <a href="<?php echo SITE_ROOT.$objCommon->html2text($getUserDetails['usl_fameuz'])?>" class="name"><?php echo $displayName?></a>
                           <a href="#" class="logout"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/controllArrow.png" alt="settings"></a>
                            <div class="dropdown_option">
                            	<div class="user-info">
                            	<div class="user-info-image">
                                <img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($getUserDetails['upi_img_url'])?$getUserDetails['upi_img_url']:'profile_pic.jpg'?>" alt="user_thumb" width="83"/>
                                </div>
                                <div class="user-info-sec">
                                <a href="<?php echo SITE_ROOT.$objCommon->html2text($getUserDetails['usl_fameuz'])?>" class="head-info"><?php echo $displayName?></a>
                                <a href="#" class="mail-info"  data-toggle="tooltip" data-placement="bottom" title="<?php echo $objCommon->html2text($getUserDetails['email']); ?>">
								<?php echo $objCommon->limitWords($getUserDetails['email'],20); ?>
								</a>
                                <span class="proffesion"></span><?php echo $objCommon->html2text($getUserDetails['c_name']);?>
                                </div>
                                <div class="account-info">
                                <div class="account pull-left">
                                <a href="<?php echo SITE_ROOT.$objCommon->html2text($getUserDetails['usl_fameuz'])?>">Account</a>
                                </div>
                                <div class="privacy pull-right">
                                <a href="<?php echo SITE_ROOT.'user/edit-profile?active=settings'?>">Privacy</a>
                                </div>
                                </div>
                                </div>
                                <div class="menu-drop">
								<?php
								if($profileComplStatus <100){
								?>
                               		<div class="progression">
										<div class="progress">
											 <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $profileComplStatus?>%">
									 		 <?php echo $profileComplStatus?>%
											 </div>
									  	</div>
									</div>
								 <?php
								}
								?>
                                <ul>
									<a href="#"><li>Create Ads</li></a>
									<a href="#"><li>Advertising on Fameuz</li></a>
									<a href="<?php echo SITE_ROOT.'user/notifications'?>"><li>Notifications<!--<span class="notify"><span class="text-notify center-block">30</span></span>--></li></a>
									<a href="#"><li>Help</li></a>
									<a href="#"><li>Report a Problem</li></a>
                                </ul>
                                </div>
                                <div class="bottom-sec">
                                <a href="<?php echo SITE_ROOT.'user/my-profile'?>" class="pro-view">View profile</a>
								<a href="<?php echo SITE_ROOT;?>user/logout" class="Logout-drop pull-right">Sign out</a>
                               
                                </div>
                                <div class="arrow"><img src="<?php echo SITE_ROOT_AWS_IMAGES;?>images/status_option_arrow.png" alt="status_option_arrow" /></div>
                                
                            </div>
                        </div>
                        <?php
						}else{
                        ?>
                        <div class="login_signup">
                            <a href="<?php echo SITE_ROOT?>user/register" id="signup">REGISTER</a>
                            <a href="#login" >Log In</a>
                        </div>
                        <?php
						}
						?>
                        <div class="main_nav mob_none">
                            <a href="#" class="mob_btn mob_menu_btn" ><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/mob_btn.png" alt="mob_btn" /></a>
                             <ul class="main_manu" id="mob_menu">
                                <li><a href="<?php echo SITE_ROOT?>">Home </a></li>
								<li <?php echo (in_array($pageName,array('search.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'search?cat=members'?>"> Search       </a></li>
								<li <?php echo (in_array($pageName,array('post_jobs.php','jobs.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'jobs'?>">Jobs</a></li>
								<li <?php echo (in_array($pageName,array('new-comers.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'new-comers'?>">Newcomers   </a></li>
								<li <?php echo (in_array($pageName,array('music.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'music'?>">Music</a></li>
								<li <?php echo (in_array($pageName,array('videos.php','videos-single.php','video-upload.php','videos-search.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'videos'?>"> Video  </a></li>
								<li <?php echo (in_array($pageName,array('photos.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'photos'?>">Photos </a></li>
								<?php /*?><li <?php echo (in_array($pageName,array('games.php','games-search.php','game-single.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'games'?>">Games </a></li><?php */?>
								<li <?php echo (in_array($pageName,array('online-member.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'online'?>">Online</a></li>
								<li <?php echo (in_array($pageName,array('most-wanted.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'most-wanted'?>">Most Wanted</a></li>
								<?php /*?><li <?php echo (in_array($pageName,array('blog.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'blog'?>">Blogs</a></li>
								<li <?php echo (in_array($pageName,array('market-place.php')))?'class="select"':'';?>><a href="<?php echo SITE_ROOT.'market-place'?>">Marketplace</a></li><?php */?>
                             </ul>      
                        </div>
                        <div class="clr"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
		<?php
		if($cssName=='index'){
		?>
<div class="banner_section">
        	<div class="container">
            	<div class="row">
                    <div class="col-md-8">
                    	<div class="banner_cnt">
                            <h1>Global show business network</h1>
                            <h2>Find a talent, get discovered, start your career here</h2>
                            <a href="<?php echo SITE_ROOT?>user/register?main-cat=2" class="banner_btn">I am professional in</a>
                            <a href="<?php echo SITE_ROOT?>user/register?main-cat=1" class="banner_btn">I am looking for professional</a>
                        </div>
                        <div class="featured_members_slider">
                            <div id="featured_members_slider" class="owl-carousel owl-theme">
                                <div class="item">
                                    <a href="<?php echo SITE_ROOT?>user/register?main-cat=2&cat=6">
                                        <span class="image"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/member1.jpg" alt="member1" /></span>
                                        <span class="title"><h4>Photographers</h4></span>
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="<?php echo SITE_ROOT?>user/register?main-cat=2&cat=4">
                                        <span class="image"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/member2.jpg" alt="member2" /></span>
                                        <span class="title"><h4>Professional model</h4></span>
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="<?php echo SITE_ROOT?>user/register?main-cat=2&cat=34">
                                        <span class="image"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/member3.jpg" alt="member3" /></span>
                                        <span class="title"><h4>Makeup & Hair</h4></span>
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="<?php echo SITE_ROOT?>user/register?main-cat=1&cat=14">
                                        <span class="image"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/member4.jpg" alt="member4" /></span>
                                        <span class="title"><h4>Event Agency</h4></span>
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="<?php echo SITE_ROOT?>user/register?main-cat=1&cat=1">
                                        <span class="image"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/member1.jpg" alt="member1" /></span>
                                        <span class="title"><h4>Casting Agent</h4></span>
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="<?php echo SITE_ROOT?>user/register?main-cat=2&cat=5">
                                        <span class="image"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/member2.jpg" alt="member2" /></span>
                                        <span class="title"><h4>Acting</h4></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                    	<div class="social_media_models">
							<?php
							include_once(DIR_ROOT."widget/home_page_premium_models.php");
							?>
                            <div class="social_icons">
                                <h2 class="home-head">Connect socially with us</h2>
                                <a href="#" class="facebook"></a>
                                <a href="#" class="twitter"></a>
                                <a href="#" class="youtube"></a>
                                <a href="#" class="google_plus"></a>
                                <a href="#" class="instagram"></a>
                                <a href="#" class="pintrest"></a>
                                <div class="clr"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
		<?php
		}
		?>
    </div>
<?php
if(@$getUserDetails['status']==4 && isset($_SESSION['userId'])){
?>
<div class="inner_content_section col-spcl">
	<div class="container" style="position:relative;">
		<div class="inner_top_border">
			<div class="row background-white" style="min-height:500px;">
				<div class="panel panel-warning"> 
					<div class="panel-heading"> 
						<h3 class="panel-title">Your Account Has Frozen Temporarily.</h3> 
					</div> 
					<div class="panel-body">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
						<ol>
							<li><p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p> </li>
							<li><p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p></li>
							<li><p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p> </li>
							<li><p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p> </li>
							<li><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p></li>
							<li><p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p></li>
						</ol>
						<p style="color:#afb420;">Please click here to re-activate your account.</p><a class="reacticateAccount has-spinner" href="javascript:;"><span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span>&nbsp;&nbsp;Re-activate</a>
					</div> 
				</div>
			</div>
		</div>
	</div>
</div>
<script language="javascript">
$(".reacticateAccount").on("click",function(){
	$(this).addClass('active');
	that	=	this;
	$.get('<?php echo SITE_ROOT?>access/re_activate_account.php',function(){
		$(that).removeClass('active');
		location.reload();
	});
});	
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
exit;
}
?>
    