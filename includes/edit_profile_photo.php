<?php
@session_start();
include_once("site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/user_profile_image.php");
$objCommon		   						=	new common();
$objUserProfileImg						=	new user_profile_image();
$getOtherProfileImages					=	$objUserProfileImg->getAll("user_id=".$_SESSION['userId']." and upi_status=0" ,"upi_created desc");
?>
<script src="<?php echo SITE_ROOT?>js/jquery.Jcrop.js"></script>
<script type="text/javascript">
function cropImage(){
	if (parseInt($('#w').val())){
		$(".preview-container").html('<div class="text-center"><img src="<?php echo SITE_ROOT?>images/1.gif" height="32" width="32"/></div>');
		var targetFile	=	$("#target").attr("src");
		
		var xvalue		=	$('#x').val();
		var yvalue		=	$('#y').val();
		var wvalue		=	$('#w').val();
		var hvalue		=	$('#h').val();
		var resWidth	  =	$("#target").width();
		var resHeight	  =	$("#target").height();
		$.get("<?php echo SITE_ROOT?>ajax/imageCrope.php",{x:xvalue,y:yvalue,w:wvalue,h:hvalue,src:targetFile,resWidth:resWidth,resHeight:resHeight},
		function(data){
			if(data!=""){
				d = new Date();
				var imageFile	=	'<img src="<?php echo SITE_ROOT?>uploads/profile_images/'+data+'?id='+d.getTime()+'" class="jcrop-preview" alt="Preview" />';
				var imageprofile =	'<img src="<?php echo SITE_ROOT?>uploads/profile_images/'+data+'" alt="profile_pic" />';
				//$(".preview-container").html(imageFile);
				$(".profile_pic").html(imageprofile);
				$(".user_thumb").html(imageprofile)
				$(".dp_show").hide();
				$(".alertPic").show();
				$( ".alertClose" ).delay( 4000 ).slideUp( 400 );
			}
		});
	}else{
		alert('Please select a crop region then press submit.');
	}
}
</script>
<link rel="stylesheet" href="<?php echo SITE_ROOT?>css/jquery.Jcrop.css" type="text/css" />
<script src="<?php echo SITE_ROOT?>js/jquery.upload-1.0.2.js"></script>
<script>
$(document).ready(function(){
$('input[type=file]').change(function() {
    $(this).upload('<?php echo SITE_ROOT?>access/upload-profile-image.php?ID=1', function(res) {
		if(res!=""){
			$(".dp_show").show();
			d = new Date();
			var uploadedContent	=	'<img src="<?php echo SITE_ROOT?>uploads/profile_images/'+res+'?'+d.getTime()+'" id="target" />';
			var uploadedPreview	=	'<img src="<?php echo SITE_ROOT?>uploads/profile_images/'+res+'?'+d.getTime()+'" class="jcrop-preview" alt="Preview" />';
			$(".uploadedImage").html(uploadedContent);
			$(".preview-container").html(uploadedPreview);
			jQuery(function($){

    // Create variables (in this scope) to hold the API and image size
    var jcrop_api,
        boundx,
        boundy,

        // Grab some information about the preview pane
        $preview = $('#preview-pane'),
        $pcnt = $('#preview-pane .preview-container'),
        $pimg = $('#preview-pane .preview-container img'),

        xsize = $pcnt.width(),
        ysize = $pcnt.height();
    console.log('init',[xsize,ysize]);
    $('#target').Jcrop({
      onChange: updatePreview,
      onSelect: updateCoords,
      aspectRatio: xsize / ysize
    },function(){
      // Use the API to get the real image size
	  
      var bounds = this.getBounds();
      boundx = bounds[0];
      boundy = bounds[1];
      // Store the API in the jcrop_api variable
      jcrop_api = this;
      jcrop_api.setSelect([70,20,70+350,65+385]);
      jcrop_api.setOptions({ bgFade: true });
      jcrop_api.ui.selection.addClass('jcrop-selection');

      // Move the preview into the jcrop container for css positioning
      //$preview.appendTo(jcrop_api.ui.holder);
    });

    function updatePreview(c){
      if (parseInt(c.w) > 0)
      {
		$("#preview-pane").show();
        var rx = xsize / c.w;
        var ry = ysize / c.h;

        $pimg.css({
          width: Math.round(rx * boundx) + 'px',
          height: Math.round(ry * boundy) + 'px',
          marginLeft: '-' + Math.round(rx * c.x) + 'px',
          marginTop: '-' + Math.round(ry * c.y) + 'px'
        });
      }
    };
	function updateCoords(c){
		$('#x').val(c.x);
		$('#y').val(c.y);
		$('#w').val(c.w);
		$('#h').val(c.h);
	 };
	function checkCoords(){
		if (parseInt($('#w').val())) return true;
		alert('Please select a crop region then press submit.');
		return false;
	  };

  });
		}
    });
});
});
</script>
 <!-- Edit Profile Photo-->
<p class="pic_dis">* Profile Picture will be visible to all</p>
	<div class="alert alertClose alert-success alert-dismissible alertPic" role="alert">
		<button type="button" class="close" data-dismiss="alertClose" aria-label="Close"><span aria-hidden="true">&times;</span></button>Your profile image has been updated..</div>
<div class="dp_show">
	<h2 class="bg_heading">Your Selected Profile Picture</h2>
	<p class="pic_dis">* Select the area of picture you wish to display</p>
	<div class="pic_frame">
	<div class="jc-demo-box">
		<div class="uploadedImage"></div>
		<div class="hiddenFields">
			<input type="hidden" id="x" name="x" />
			<input type="hidden" id="y" name="y" />
			<input type="hidden" id="w" name="w" />
			<input type="hidden" id="h" name="h" />
		</div>
		<div id="preview-pane">
			<div class="preview-container"></div>
			<input type="button" onClick="cropImage();" value="Save" class="btn btn-small btn-inverse btn-crop" />
		</div>    
		
		<div class="clearfix"></div>
	</div>
</div>
</div>
<h2 class="bg_heading">Change / Upload Photo </h2>
<div class="field_box_section">
    <div class="field_box">
        <div class="fiel_border" style="width:235px;">
            <div class="text">
               <div class="file_up"> <font style=" padding-top:3px;float:left;">Choose file</font> <input type="file" name="photoimg"/> <span class="file_browse">Browse</span></div>
			   
            </div>
        </div>
    </div>
    <div class="field_box"></div>
</div>
<div class="field_box_section">
    <p class="agree"><label><input type="checkbox" checked="checked">I certify that this image contains no nudity or other material, which could be deemed offensive by some users. </label><a href="javascript:;"> Terms of use</a></p>
</div>
<?php
if(count($getOtherProfileImages)>0){
?>
<form action="<?php echo SITE_ROOT?>access/change_profile_image.php" method="post">
<div class="field_box_section">
    <h2 class="bg_heading">Choose From Your Existing Pictures</h2>
    <p>Select which picture from your album do you want to use for the profile picture.</p>
</div>
<div class="field_box_section">
    <div class="pics">
		<?php
		foreach($getOtherProfileImages as $allotherProfileImages)
		{
		?>
		<div class="pic"><a href="javascript:;"><input type="radio"  name="p_img_sel" value="<?php echo $allotherProfileImages['upi_id']?>"/><img src="<?php echo SITE_ROOT?>uploads/profile_images/<?php echo $allotherProfileImages['upi_img_url']?>" alt="pic1"></a></div>
		<?php
		}
		?>
        <div class="clr"></div>
    </div>
</div>
<div class="field_box_section">
    <div><input type="submit" value="Save Changes" class="change_profile_image"></div>
</div>
</form>
<?php
}
?>
<script language="javascript" type="text/javascript">
$(".pic").on("click","a",function(){
	$(".pic").removeClass("active");
	$(this).parent().addClass("active");
});
</script>