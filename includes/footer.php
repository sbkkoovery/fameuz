<div class="footer_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="links">
					<p> 
						<a href="<?php echo SITE_ROOT.'page/about-us'?>">About us </a>       
						<a href="#">Contact us</a>        
						<a href="<?php echo SITE_ROOT.'page/privacy-policy'?>">Privacy Policy</a>        
						<a href="<?php echo SITE_ROOT.'page/terms-of-use'?>">Terms of use</a>
					</p>
				</div>
				<div class="copyright">
					<p>&copy;  Copyright  Famuez . <?php echo date("Y");?> - All rights reserved</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once(DIR_ROOT.'js/include/all_script_js.php');
?>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo SITE_ROOT_AWS_CDN?>js/popup.js" type="text/javascript" ></script>
<script src="<?php echo SITE_ROOT_AWS_CDN?>js/owl.carousel.js" type="text/javascript"  ></script>
<script src="<?php echo SITE_ROOT_AWS_CDN?>js/all-page-script.js" type="text/javascript" ></script>
<script type="text/javascript">
$(document).ready(function(e) {
	$('[data-toggle="tooltip"]').tooltip();
	$('#fb_login').click(function(e) {
		$('#register_form_box').fadeOut(function(){
			$('#register_fb_box').fadeIn();
		});
		return false;
	}); 
	$('#close_btn').click(function(e) {
		$('#register_fb_box').fadeOut(function(){
			$('#register_form_box').fadeIn();
		});
		return false;
	});
	//$(".user_login .logout").click(function(e) {
//        $(this).next(".dropdown_option").toggle();
//		$(this).toggleClass("act");
//		$(".user_thumb img").toggle();
//    });
	
});
</script>
</body>
</html>