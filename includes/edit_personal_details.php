<?php
@session_start();
include_once("site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/users.php");
include_once(DIR_ROOT."class/user_privacy.php");
$objCommon				 =	new common();
$objUsers				  =	new users();
$objUserPrivacy		   	=	new user_privacy();
$getUserPrivacy			=	$objUserPrivacy->getRow("user_id=".$_SESSION['userId']);
$getPersonalDetails	  	=	$objUsers->getRowSql("SELECT user.*,personal.* FROM users AS user  LEFT JOIN personal_details as personal ON user.user_id = personal.user_id WHERE user.status=1 AND user.user_id=".$_SESSION['userId']);
?>
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.css" />
<script src="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.js"></script>
<!-- Edit Personel Details--> 
    <form action="<?php echo SITE_ROOT?>access/edit_profile.php?action=edit_personal_details" method="post" enctype="multipart/form-data" id="personal_form">
    <p class="pic_dis">*Your personal details</p>
    <div class="row">
     <div class="col-sm-9">
     <div class="row">
    <div class="col-sm-6">
    <div class="form-group edit-field">
    <label>First Name</label>
    <div class="input-group">
  <span class="input-group-addon"><i class="fa fa-tag"></i></span>
   <input type="text" value="<?php echo ($getPersonalDetails['first_name'])?$objCommon->html2text($getPersonalDetails['first_name']):'';?>" name="per_f_name" id="per_f_name" >
   </div>
  </div>
    </div>
    <div class="col-sm-6">
    <div class="form-group edit-field">
    <label>Last Name</label>
    <div class="input-group">
  <span class="input-group-addon"><i class="fa fa-tag"></i></span>
   <input type="text" value="<?php echo ($getPersonalDetails['last_name'])?$objCommon->html2text($getPersonalDetails['last_name']):'';?>" name="per_l_name" id="per_l_name" >
   </div>
  </div>
    </div>
    </div>
    <div class="row">
    <div class="col-sm-6">
    <div class="form-group edit-field">
    <label>Phone Number</label>
    <div class="input-group move-him">
  <span class="input-group-addon"><i class="fa fa-phone"></i></span>
  <input type="text" value="<?php echo ($getPersonalDetails['p_phone'])?$objCommon->html2text($getPersonalDetails['p_phone']):'';?>" name="per_phone" id="per_phone" >
    <span class="input-group-addon edited-privacy"><i class="fa fa-cog"></i>
    <span class="drop_down">
    	<?php
		$exploPrivacyPhone			=	explode(",",$getUserPrivacy['uc_p_phone']);
		?>
      <span class="arrow"><img alt="status_option_arrow" src="<?php echo SITE_ROOT?>images/status_option_arrow.png"></span>
   		<span class="chkbox"><input type="checkbox" name="per_phone_privacy1" value="1" <?php echo ($exploPrivacyPhone[0]==1)?'checked':''?> />Public</span>
        <span class="chkbox"><input type="checkbox" name="per_phone_privacy2" value="1" <?php echo ($exploPrivacyPhone[1]==1)?'checked':''?> />Friends</span>
        <span class="chkbox"><input type="checkbox"  name="per_phone_privacy3" value="1" <?php echo ($exploPrivacyPhone[2]==1)?'checked':''?>  />Professionals</span>
        <span class="chkbox"><input type="checkbox"  name="per_phone_privacy4" value="1" <?php echo ($exploPrivacyPhone[3]==1)?'checked':''?>  />Only for me</span>
        </span>
    </span>
    </div>
    <p class="click">* Click on the icon to change privacy</p>
    </div>
    </div>
    <div class="col-sm-6">
    <div class="form-group edit-field">
    <label>Alternative Phone Number</label>
    <div class="input-group move-him">
  <span class="input-group-addon"><i class="fa fa-phone"></i></span>
  <input type="text" value="<?php echo ($getPersonalDetails['p_alt_phone'])?$objCommon->html2text($getPersonalDetails['p_alt_phone']):'';?>" name="per_alt_phone" id="per_alt_phone">
    <span class="input-group-addon edited-privacy"><i class="fa fa-cog"></i>
    <span class="drop_down">
    <?php
	$exploPrivacyAltPhone			=	explode(",",$getUserPrivacy['uc_p_alt_phone']);
	?>
      <span class="arrow"><img alt="status_option_arrow" src="<?php echo SITE_ROOT?>images/status_option_arrow.png"></span>
   		<span class="chkbox"><input type="checkbox" name="per_alt_phone_privacy1" value="1" <?php echo ($exploPrivacyAltPhone[0]==1)?'checked':''?> />Public</span>
        <span class="chkbox"><input type="checkbox" name="per_alt_phone_privacy2" value="1" <?php echo ($exploPrivacyAltPhone[1]==1)?'checked':''?> />Friends</span>
        <span class="chkbox"><input type="checkbox"  name="per_alt_phone_privacy3" value="1" <?php echo ($exploPrivacyAltPhone[2]==1)?'checked':''?>  />Professionals</span>
        <span class="chkbox"><input type="checkbox"  name="per_alt_phone_privacy4" value="1" <?php echo ($exploPrivacyAltPhone[3]==1)?'checked':''?>  />Only for me</span>
        </span>
    </span>
    </div>
    <p class="click">* Click on the icon to change privacy</p>
    </div>
    </div>
    </div>
    <div class="row">
    <div class="col-sm-6">
      <div class="form-group-sp">
      <label>Email</label>
      <div class="form-adon-sp">
      <span class="input-addon-sp-inside"><i class="fa fa-envelope"></i></span>
      <div class="input-field"><?php echo $objCommon->html2text($getPersonalDetails['email']); ?></div>
      </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group edit-field">
    <label>Alternative Email</label>
    <div class="input-group">
  <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
   <input type="text" value="<?php echo ($getPersonalDetails['p_alt_email'])?$objCommon->html2text($getPersonalDetails['p_alt_email']):'';?>" name="per_alt_email" id="per_alt_email"  >
   </div>
  </div>
    </div>
    </div>
    </div>
    <div class="col-sm-3">
    <div class="help">
        <h1><span class="qstn">?</span> Help...?<span class="h_close"><a href="#"><i class="fa fa-close"></i></a></span></h1>
        <p>Please review our frequently 
        asked questions to the left if 
        you need help</p>
        <p>If you have a questions that is 
        not answered there, you can 
        contact us (response time will 
        usually be 1 to 2 business days)</p>
    </div>
    </div>
    </div>
       <h2 class="bg_heading">Change Password</h2>
       	<div class="row">
    <div class="col-sm-5">
      <div class="form-group edit-field">
    <label>Change Password</label>
    <div class="input-group">
  <span class="input-group-addon"><i class="fa fa-lock"></i></span>
  <input type="password" name="per_password" id="per_password" autocomplete="off">
   </div>
  </div>
    </div>
    <div class="col-sm-5">
      <div class="form-group edit-field">
    <label>Re-type Password</label>
    <div class="input-group">
  <span class="input-group-addon"><i class="fa fa-lock"></i></span>
   <input type="password" name="per_con_password" id="per_con_password" autocomplete="off">
   </div>
  </div>
    </div>
    </div>
        <?php /*?><div class="field_box_section margin_bttm">
            <div class="field_box" style="position:relative;">
                <div class="label_name">Change Password</div>
                <div class="fiel_border">
                    <div class="icon"><i class="fa fa-lock"></i></div>
                    <div class="text"><input type="password" name="per_password" id="per_password" autocomplete="off"></div>
                </div>
            </div>
            <div class="field_box" style="position:relative;">
                <div class="label_name">Re-type Password</div>
                <div class="fiel_border">
                    <div class="icon"><i class="fa fa-lock"></i></div>
                    <div class="text"><input type="password" name="per_con_password" id="per_con_password" autocomplete="off"></div>
                </div>
            </div>
        </div><?php */?>
        <h2 class="bg_heading">Manage Account</h2>
        <div class="field_box_section">
            <!-- <p class="agree"><label><input type="checkbox" name="deactivate">Temporary deactivate my account</label></p>
            <p class="agree"><label><input type="checkbox" name="deactivate">Permenently deactivate my account </label></p>
            <p class="deactivate"><a href="javascript:;" class="account_delete"><i class="fa fa-trash"></i> Deactivate Account</a></p>-->
			<div class="row deactivate">
				<div class="col-md-6">
				<a href="javascript:;" class="account_delete"><i class="fa fa-trash"></i> Deactivate Account</a>
				</div>
				<div class="col-md-6">
				<a href="javascript:;" class="account_freeze"><i class="fa fa-trash-o"></i> Freeze my account</a>
				</div>
			</div>
            <div><input type="submit" value="Save Changes"></div>
        </div>
    </form>
<script language="javascript" type="text/javascript">
$("#personal_form").validate({
	rules: {
		per_password: {minlength:6},
		per_con_password: {equalTo:"#per_password"}
	},
	messages: {
		per_password: '<i class="fa fa-times"></i>',
		per_con_password: '<i class="fa fa-times"></i>'
	}

});
$('.edited-privacy').on("click",function(e) {
	$(this).find('.drop_down').slideToggle();
});
$('.h_close a').click(function(e){
	$(this).parent().parent().parent().remove();
	return false;
});
$(".account_delete").on("click",function(){
	var account_id	   =	'<?php echo $_SESSION['userId']?>';
	var that			 =	this;
	var elem = $(this).closest('.item');
	$.confirm({
		'title'	  : 'Permanently Delete Account',
		'message'	: 'your account has been deactivated from the site.',
		'buttons'	: {
		'Yes'	: {
		'class'	: 'blue',
		'action': function(){
		elem.slideUp();
		if(account_id){
			$.get('<?php echo SITE_ROOT?>access/delete_my_account.php',{'account_id':account_id},function(data){
				window.location.href	=	'<?php echo SITE_ROOT?>';
			});
		};
		}
		},
		'No'	: {
		'class'	: 'gray',
		'action': function(){
		}	// Nothing to do in this case. You can as well omit the action property.
		}
		}
	});
});
$(".account_freeze").on("click",function(){
	var account_id	   =	'<?php echo $_SESSION['userId']?>';
	var that			 =	this;
	var elem = $(this).closest('.item');
	$.confirm({
		'title'	  : 'Freeze My Account',
		'message'	: 'your account has been frozen from the site.',
		'buttons'	: {
		'Yes'	: {
		'class'	: 'blue',
		'action': function(){
		elem.slideUp();
		if(account_id){
			$.get('<?php echo SITE_ROOT?>access/freeze_my_account.php',{'account_id':account_id},function(data){
				window.location.href	=	'<?php echo SITE_ROOT?>';
			});
		};
		}
		},
		'No'	: {
		'class'	: 'gray',
		'action': function(){
		}	// Nothing to do in this case. You can as well omit the action property.
		}
		}
	});
});
</script>
