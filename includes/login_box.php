<!---------------------------------------->
<div class="remodal popup_box" data-remodal-id="login">
   <h2>Login to Fameuz</h2>
   <div class="login_table">
   		<form action="<?php echo SITE_ROOT?>access/login.php?action=login" method="post">
			<div class="tr">
				<div class="icon"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/popup_email.png" alt="popup_email" /></div>
				<div class="field"><input type="text" placeholder="Email address" name="login_email" id="login_email" /></div>
				<div class="clr"></div>
			</div>
			<div class="tr">
				<div class="icon"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/popup_pass.png" alt="popup_pass" /></div>
				<div class="field"><input type="password" placeholder="Password" name="login_passoword" id="login_passoword" /></div>
				<div class="clr"></div>
			</div>
			<div class="tr submit">
				<input type="submit" value="Login">
			</div>
		</form>
		<div class="tr submit forgot">
			<div class="forgot_pass"><a href="<?php echo SITE_ROOT?>user/forgot-password">Forgot password?</a></div>
			<div class="forgot_pass">Not a member? <a href="<?php echo SITE_ROOT?>user/register">Register</a></div>
			<div class="clr"></div>
		</div>
		<div class="tr">
			<div class="facebook_login"><a href="<?php echo SITE_ROOT?>fb/fbconfig.php">LOGIN WITH FACEBOOK</a></div>
		</div>
   </div>
</div>
<!---------------------------------------->
