<div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3">
	<div class="sidebar">
		<div class="search_box"><form action="<?php echo SITE_ROOT.'user/search-result'?>" method="get"><input type="text" placeholder="Search "  name="keywordSearch" /></form></div>
		<div class="background-white">
	<!--------------ad start----------->
		<script type='text/javascript'>
			  var googletag = googletag || {};
			  googletag.cmd = googletag.cmd || [];
			  (function() {
				var gads = document.createElement('script');
				gads.async = true;
				gads.type = 'text/javascript';
				var useSSL = 'https:' == document.location.protocol;
				gads.src = (useSSL ? 'https:' : 'http:') +
				  '//www.googletagservices.com/tag/js/gpt.js';
				var node = document.getElementsByTagName('script')[0];
				node.parentNode.insertBefore(gads, node);
			  })();
			</script>
			<script type='text/javascript'>
			  googletag.cmd.push(function() {
				googletag.defineSlot('/194259912/fameuz_burjalsafa_300x233', [300, 233], 'div-gpt-ad-1454585006799-0').addService(googletag.pubads());
			  <!--  googletag.pubads().enableSingleRequest();-->
				googletag.enableServices();
			  });
			</script>
				<div id='div-gpt-ad-1454585006799-0' style='height:233px; width:300px;'>
					<script type='text/javascript'>
					googletag.cmd.push(function() { googletag.display('div-gpt-ad-1454585006799-0'); });
					</script>
				</div>
				<!--------------ad end----------->
    </div>
		<div class="profile_border_box information">
			<h2 class="heading"><i class="fa fa-exclamation"></i>Information </h2>
			<ul>
			<?php
			
			if(($getMyFriendDetails['p_city'] != '' || $getMyFriendDetails['p_country'] != '') && is_array($disply_see_arr) && in_array("location",$disply_see_arr)){
				$friendPcity	=	($getMyFriendDetails['p_city'])?$objCommon->html2text($getMyFriendDetails['p_city'])." ,":"";
				echo '<li><span>Location </span><span>'.$friendPcity.' '.$objCommon->html2text($getMyFriendDetails['p_country']).'</span></li>';
			}
			if($getMyFriendDetails['p_dob'] !='' && $getMyFriendDetails['p_dob'] !='0000-00-00'){
				$privacy_dob		=	$objCommon->checkPrivacy($getMyFriendDetails['uc_p_dob']);
				if($privacy_dob=='1' || (($privacy_dob=='2' || $privacy_dob=='23') && $friendStatus !=0) || (($privacy_dob=='3' || $privacy_dob=='23') && $getUserDetails['uc_m_id']==1))
				{
					echo '<li><span>DOB </span><span>'.date("d F Y",strtotime($getMyFriendDetails['p_dob'])).'</span></li>';
				}
			}
			if($getMyFriendDetails['ethnicity_name'] && is_array($disply_see_arr) && in_array("ethnicity",$disply_see_arr)){
				echo '<li><span>Ethnicity </span><span>'.$objCommon->html2text($getMyFriendDetails['ethnicity_name']).'</span></li>';
			}
			if($getMyFriendDetails['p_gender'] && is_array($disply_see_arr) && in_array("gender",$disply_see_arr)){
				if($getMyFriendDetails['p_gender']==1){
					$genderFriend			=	'Male';
				}else if($getMyFriendDetails['p_gender']==2){
					$genderFriend			=	'Female';
				}else{
					$genderFriend			=	'NA';
				}
				
				echo '<li><span>Gender </span><span>'.$genderFriend.'</span></li>';
			}
			if($getMyFriendDetails['uc_m_id']==2){
				if($getModelDetails['mh_name'] && is_array($disply_see_arr) && in_array("height",$disply_see_arr)){
					echo '<li><span>Height </span><span>'.$objCommon->html2text($getModelDetails['mh_name']).'</span></li>';
				}
				if($getModelDetails['me_name'] && is_array($disply_see_arr) && in_array("eye_color",$disply_see_arr)){
					echo '<li><span>Eyes </span><span>'.$objCommon->html2text($getModelDetails['me_name']).'</span></li>';
				}
				if($getModelDetails['mhair_name'] && is_array($disply_see_arr) && in_array("hair",$disply_see_arr)){
					echo '<li><span>Hair </span><span>'.$objCommon->html2text($getModelDetails['mhair_name']).'</span></li>';
				}
				if($getModelDetails['mc_name'] && is_array($disply_see_arr) && in_array("chest",$disply_see_arr)){
					$modelChest		=	($getMyFriendDetails['p_gender']==1)?'Chest':'Bust';
					echo '<li><span>'.$modelChest.' </span><span>'.$objCommon->html2text($getModelDetails['mc_name']).'</span></li>';
				}
				if($getModelDetails['mw_name'] && is_array($disply_see_arr) && in_array("waist",$disply_see_arr)){
					echo '<li><span>Waist</span><span>'.$objCommon->html2text($getModelDetails['mw_name']).'</span></li>';
				}
				if($getModelDetails['mhp_name'] && is_array($disply_see_arr) && in_array("hips",$disply_see_arr)){
					echo '<li><span>Hips</span><span>'.$objCommon->html2text($getModelDetails['mhp_name']).'</span></li>';
				}
				if($getMyFriendDetails['p_gender']==2){
					if($getModelDetails['mdress_name'] && is_array($disply_see_arr) && in_array("dress",$disply_see_arr)){
						echo '<li><span>Dress</span><span>'.$objCommon->html2text($getModelDetails['mdress_name']).'</span></li>';
					}
				}
				if($getMyFriendDetails['p_gender']==1){
					if($getModelDetails['mj_name']){
						echo '<li><span>Jacket</span><span>'.$objCommon->html2text($getModelDetails['mj_name']).'</span></li>';
					}
					if($getModelDetails['mt_name']){
						echo '<li><span>Trousers</span><span>'.$objCommon->html2text($getModelDetails['mt_name']).'</span></li>';
					}
					if($getModelDetails['mcollar_name']){
						echo '<li><span>Collar</span><span>'.$objCommon->html2text($getModelDetails['mcollar_name']).'</span></li>';
					}
				}
				if($getModelDetails['ms_name'] && is_array($disply_see_arr) && in_array("shoe",$disply_see_arr)){
					echo '<li><span>Shoes</span><span>'.$objCommon->html2text($getModelDetails['ms_name']).'</span></li>';
				}
			?>
			</ul>
			<?php
			}
				if($book_by_me ==1 && $getMyFriendDetails['c_book_me']==1){
			?>
					<p class="booking margin_btm0"><a href="<?php echo SITE_ROOT.'user/'.$user_url.'/add-book'?>">Book <?php echo $displayNameFriend?></a></p>
			<?php
				}else{
					?>
					<p class="booking margin_btm0"><a data-target=".requestAdd" data-toggle="modal" href="javascript:;">Contact <?php echo $displayNameFriend?></a></p>
					<?php
				}
			?>
		</div>
		<div class="social_icons margin_btm14">
			 <h2 class="heading">Connect socially with us</h2>
			 <a href="<?php echo ($getMyFriendDetails['usl_fb'])?$objCommon->html2text($getMyFriendDetails['usl_fb']):'https://www.facebook.com/'?>" target="_blank"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/fb_s.png" alt="fb_s" /></a>
			 <a href="<?php echo ($getMyFriendDetails['usl_twitter'])?$objCommon->html2text($getMyFriendDetails['usl_twitter']):'https://twitter.com/'?>" target="_blank"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/tw_s.png" alt="tw_s" /></a>
			 <a href="https://www.youtube.com/" target="_blank"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/you_s.png" alt="you_s" /></a>
			 <a href="<?php echo ($getMyFriendDetails['usl_gplus'])?$objCommon->html2text($getMyFriendDetails['usl_gplus']):'https://plus.google.com/'?>" target="_blank"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/g_plus_s.png" alt="g_plus_s" /></a>
			 <a href="<?php echo ($getMyFriendDetails['usl_instagram'])?$objCommon->html2text($getMyFriendDetails['usl_instagram']):'https://www.instagram.com/'?>" target="_blank"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/in_s.png" alt="in_s" /></a>
			 <a href="<?php echo ($getMyFriendDetails['usl_others'])?$objCommon->html2text($getMyFriendDetails['usl_others']):'https://www.pinterest.com/'?>" target="_blank"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/pin_s.png" alt="pin_s" /></a>
		</div>
		<?php
		if($cssName != 'add-booking'){
			include_once(DIR_ROOT."class/worked_together.php");
			$objWorkedTogether				=	new worked_together();
			$getWorkedDetails				 =	$objWorkedTogether->listQuery("SELECT social.usl_fameuz,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url
FROM worked_together AS worked 
LEFT JOIN users AS user ON CASE WHEN worked.send_to=".$getMyFriendDetails['user_id']." THEN worked.send_from = user.user_id ELSE worked.send_to = user.user_id END
LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
WHERE worked.from_status=1 AND worked.to_status=1 AND (worked.send_to ='".$getMyFriendDetails['user_id']."' OR worked.send_from='".$getMyFriendDetails['user_id']."') AND user.user_id !='".$getMyFriendDetails['user_id']."' AND user.email_validation=1 AND user.status=1 
GROUP BY user.user_id 
ORDER BY worked.worked_date DESC LIMIT 3");
			if(count($getWorkedDetails) >0 ){
		?>
		<div class="visitors_box ">
			<h2 class="heading with_border">
				<span><?php echo $displayNameFriendSmall;?> Worked With</span>
				<span class="seemore"><a href="<?php echo SITE_ROOT.'user/'.$user_url.'/worked-with'?>">See More <i class="fa fa-play "></i></a></span>
			</h2>
			<div class="visitor_images">
				<?php
				foreach($getWorkedDetails as $allWorkedTogether){
					if($allWorkedTogether['display_name']){
						$displayNameWorked	 =	$objCommon->html2text($allWorkedTogether['display_name']);
					}else if($allWorkedTogether['first_name']){
						$displayNameWorked	 =	$objCommon->html2text($allWorkedTogether['first_name']);
					}else{
						$exploEmailFriend	   =	explode("@",$objCommon->html2text($allWorkedTogether['email']));
						$displayNameWorked	  =	$exploEmailFriend[0];
					}
					$displayNameWorkedSmall	 =	$objCommon->smallWords($displayNameWorked,12);
				?>
				<a href="<?php echo SITE_ROOT.$objCommon->html2text($allWorkedTogether['usl_fameuz'])?>">
					<span class="thumb"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($allWorkedTogether['upi_img_url'])?$objCommon->html2text($allWorkedTogether['upi_img_url']):'profile_pic.jpg'?>" /></span>
					<span class="title"><?php echo $displayNameWorkedSmall;?></span>
				</a>
				<?php
				}
				?>
			</div>
		</div>
		<?php
			}
		}
		if($cssName == 'add-booking'){
			include_once(DIR_ROOT."widget/models_in_location.php");
		}
		?>
		<?php
		if(count($getMyReviews)>0 && $cssName !='add-review' && $cssName !='user_reviews' && $cssName != 'add-booking'){
		?>
		<div class="profile_border_box reviews">
			<h2 class="heading"><?php echo $displayNameFriend?>'s Reviews</h2>
			<?php
			foreach($getMyReviews as $allMyReviews){
				$getrate			=	(int) $allMyReviews['review_rate'];
				$getRateStyle	   =	'style="width:'.($getrate*20).'%;"';
			?>
			<div class="review_box">
				<div class="thumb"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($allMyReviews['upi_img_url'])?$allMyReviews['upi_img_url']:'profile_pic.jpg'?>" alt="review_thumb" /></div>
				<div class="text">
					<p class="review">
						<?php echo (strlen($objCommon->html2textarea($allMyReviews['review_msg']))>130)?mb_substr($objCommon->html2textarea($allMyReviews['review_msg']),0,130,'utf-8').'...':$objCommon->html2textarea($allMyReviews['review_msg']);?>
						</p>
					<p class="posted">Posted by
						<span><?php echo ($allMyReviews['display_name'])?$objCommon->html2text($allMyReviews['display_name']):$objCommon->html2text($allMyReviews['first_name']);?></span>
					</p>
				</div>
				<div class="clr"></div>
				<div class="rating_box"><div class="rating_yellow" <?php echo $getRateStyle?>></div></div>
				<div class="time"><i class="fa fa-clock-o"></i><?php echo date("d M , Y",strtotime($objCommon->local_time($allMyReviews['review_date'])));?></div>
			</div>
			<?php
			}
			?>
			<p class="margin_btm0"><a href="<?php echo SITE_ROOT.'user/'.$user_url.'/reviews'?>" class="seemore">See More Reviews<i class="fa fa-chevron-right"></i></a></p>
		</div>
		<?php
		}
		?>
	</div> 
</div>
<div class="clr"></div> 
<!--------modal request message------>
<div class="modal fade requestAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content request-back">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Contact <?php echo $displayNameFriend?></h4>
      </div>	
	  <form action="<?php echo SITE_ROOT?>access/send_contact_message.php" id="requestFormMsg" method="post">
      <div class="modal-body">
	  	<div id="preview"></div>
      	<div class="user-post-info-ad">
        	<div class="img-section">
            	<img class="img-responsive" src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo $getMyFriendDetails['upi_img_url']?>" />
            </div>
            <div class="pot-user-info">
            	<p><?php echo $objCommon->displayName($getMyFriendDetails);?></p>
                <p class="profesn"><span class="proffesion"></span><?php echo $getMyFriendDetails['c_name']?></p>
            </div>
        </div>
      		<input type="hidden" name="user_id"  value="<?php echo $getMyFriendDetails['user_id']?>" />
            <div class="form-group">
                <textarea class="form-control" rows="6" placeholder="Write Your Message" name="contact_msg" id="contact_msg"></textarea>
            </div>
        
      </div>
      <div class="modal-footer">
	  	<span class="pull-left" id="showMsgContact"></span>
        <button type="submit" class="btn btn-primary sendRequestMsg">Send</button>
      </div>
	  </form>
    </div>
  </div>
</div>
<!--------End modal request message------>
<script type="application/javascript">
$( "#requestFormMsg" ).submit(function( event ) {
 	var contact_msg	=	$("#contact_msg").val();
	if(contact_msg.length >2){
		return true;
	}
	return false;
  event.preventDefault();
});
		
/*function successCallMsg(){
	var contact_msg	=	$("#contact_msg").val();
	if(contact_msg.length >2){
		  $("#contact_msg").val('');
		  $("#showMsgContact").html('<div class="alert alertClose alert-success alert-dismissible" role="alert" style=" margin-bottom:0px; padding:6px;"> Message has been sent</div>');
		  $( ".alertClose" ).show().delay( 4000 ).slideUp( 400 );
	}
	  
}*/
</script>