<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
$objCommon				   =	new common();
include_once(DIR_ROOT."class/album.php");
$objAlbum					=	new album();
$friendId					=	$objCommon->esc($_GET['friendId']);
$getAlbumDetails			 =	$objAlbum->listQuery("SELECT tabPhotos.* FROM((SELECT photo_url AS img_url,photo_id AS img_id,photo_descr as img_descr,photo_created AS img_created,IF(photo_id != '', 1, '') AS img_type,photo_like_count AS likeCount,photo_comment_count AS commentCount
	FROM user_photos 
	WHERE user_id=".$friendId." 
	ORDER BY photo_created DESC,photo_id DESC)
	UNION ALL
	(SELECT ai_images AS img_url,ai_id AS img_id,ai_caption AS img_descr,ai_created AS img_created,IF(ai_id != '', 2, '') AS img_type,ai_like_count AS likeCount,ai_comment_count AS commentCount
	FROM album_images 
	WHERE user_id=".$friendId."
	ORDER BY ai_created desc ,ai_id desc)) AS tabPhotos ORDER BY tabPhotos.img_created DESC");
?>
<div class="album-head inner"> <img src="<?php echo SITE_ROOT?>images/gallery.png"><span class="head-album">Photos</span> </div>
	<?php
	if(count($getAlbumDetails)>0){
	?>
	<div class="row albListShow">
	<div class="col-md-12">
	<div class="add_photo_album_list">
		<ul class="album_list_ul">
			<?php
			foreach($getAlbumDetails as $allAlbumPhotos){
				?>
			<li><div class="pic"><a href="javascript:;" class="lightBoxs" data-contentId="<?php echo $allAlbumPhotos['img_id']?>" data-contentType="<?php echo $allAlbumPhotos['img_type']?>" data-contentAlbum="-1" ><img src="<?php echo SITE_ROOT?>uploads/albums/<?php echo $objCommon->getThumb($allAlbumPhotos['img_url'])?>" class="img-responsive"  /></a></div></li>
			<?php
			}
			?>
		</ul>
	</div>
	</div>
	</div>
	<?php
	}else{
		echo '<div class="row albListShow"><div class="col-md-12"><p class="no_img_album text-center">There are no photos in this album</p></div></div>';
	}
	?>
<script type="text/javascript">
$(document).ready(function(){
	 $(".lightBoxs").lightBox({
	  videoSkin: '<?php echo SITE_ROOT?>jw_player/six/six.xml',
	  photosObj: true,
	  videoObj :  true,
	  defaults : true,
	  album   : false,
	  deleteCommentUrl : '<?php echo SITE_ROOT ?>access/delete_comment.php',
	  skins : {
	   loader   : '<?php echo SITE_ROOT?>images/ajax-loader.gif',
	   next      : '<img src="<?php echo SITE_ROOT?>fameuz_lightbox/images/next.png">',
	   prev   : '<img src="<?php echo SITE_ROOT?>fameuz_lightbox/images/prev.png">',
	   dismiss     : '<img src="<?php echo SITE_ROOT?>fameuz_lightbox/images/close.png" width="15">',
	   reviewIcon  : '<i class="fa fa-chevron-right"></i>',
	  },
	  photos : {
	   fetchUrl: '<?php echo SITE_ROOT.'ajax/popUpImageUser.php'?>',
	   ajaxUrl : '<?php echo SITE_ROOT.'widget/pop_image_comment_box.php' ?>',
	   comment : {
		CommentFormAction : '<?php echo SITE_ROOT?>access/post_comment.php?action=add_comment',
		  commentLikeUrl   : '<?php echo SITE_ROOT?>ajax/like_comment.php',
	   },
	   shareItems : {
		likeAjaxUrl  : '<?php echo SITE_ROOT?>ajax/like.php',
		shareAjaxUrl : '<?php echo SITE_ROOT?>ajax/share.php',
		workedAjaxUrl: '<?php echo SITE_ROOT?>ajax/worked_together.php',
	   },
	  },
	  video : {
	   url  : '<?php echo SITE_ROOT?>uploads/testuploads/1.mp4',
	   fetchUrl: '<?php echo SITE_ROOT.'ajax/popUpVideoUser.php'?>',
	   ajaxUrl : '<?php echo SITE_ROOT.'widget/pop_video_comment_box.php' ?>',
	   comment : {
		CommentFormAction : '<?php echo SITE_ROOT?>access/post_video_comment.php?action=add_comment',
		commentLikeUrl   : '<?php echo SITE_ROOT?>ajax/like_video_comment_pop.php',
	   },
	   shareItems : {
		likeAjaxUrl  : '<?php echo SITE_ROOT?>ajax/like_video.php',
		shareAjaxUrl : '<?php echo SITE_ROOT?>ajax/share_video.php',
	   },
	  },
	  shareItems : {
	   likeAjaxUrl : '<?php echo SITE_ROOT?>ajax/like.php',
	   shareAjaxUrl: '<?php echo SITE_ROOT?>ajax/share.php',
	  }
	 });
});
</script>