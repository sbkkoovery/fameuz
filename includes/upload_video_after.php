<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.validate.js"></script>
<div class="row">
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-sp-3" id="video_ajax_preview">
						  <div class="upload-preview"><img src="<?php echo SITE_ROOT.'images/1.gif'?>" width="30"  class="center-block" style="padding-top:40px;"/></div>
						   <div class="upload-status">
						   	<p><strong>Upload Status</strong></p>
						   	<p class="process">Processing your video</p>
						   </div>
						   
            </div>
            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-sp-9">
				<div class="row">
                <div class="col-sm-12">
                <div class="progressbar1 progress">
               		<div class="progression1 bar"></div>
					 <div class="percent">0%</div>
                </div>
                <div class="upload-info">
               <!-- <p><span class="vid-name">Home.mp4/</span><span class="vid-size">4.50MB of 4.50MB/</span><span class="vid-time">00:00 Remaining </span></p>-->
                </div>
                </div>
                </div>
                <div class="row">
                <div class="col-sm-12 vid-info-title">
                <p><strong>Basic Info</strong></p>
                <form class="video-upload" id="uploadvideo" method="post" action="<?php echo SITE_ROOT.'access/add_video_details.php'?>">
                	 <div class="form-group upload-help">
                        <label for="exampleInputEmail1">Title</label>
                        <div class="help-text">Videos are more interesting when they have creative titles. We know you can do better than "My Video."</div>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="img_title">
                      </div>
                      <div class="form-group upload-help">
                        <label for="exampleInputPassword1">Description</label>
                         <div class="help-text"></div>
                        <textarea class="form-control" id="exampleInputPassword1" name="img_descr"></textarea>
                      </div>
                      <div class="row">
                      <div class="col-sm-6">
                      <div class="form-group upload-help">
                        <label for="exampleInputEmail1">Tags</label>
                        <div class="help-text">Add related tags (separated by commas, please!).</div>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="video_tags">
                      </div>
                      </div>
                      <div class="col-sm-6">
                      <div class="form-group upload-help">
                        <label for="exampleInputEmail1">Privacy Settings</label>
                        <div class="help-text">&nbsp;</div>
                        <select class="form-control" name="privacy">
                          <option value="1">Public</option>
						  <option value="2">Friends</option>
						  <option value="4">Only for me</option>
                         </select>
                      </div>
                      </div>
					  <div class="row pubVidDet">
					  </div>
					  
                      </div>
                      <button type="submit" class="btn btn-default">Publish</button>
                </form>
                </div>
                </div>
            </div>
            </div>
<script type="text/javascript">
$("#uploadvideo").validate({
			rules: {
				img_title: "required",
			},
			messages: {
				img_title: '<p style="color:#F30;">Please add a title</p>',
			}
    });
</script>