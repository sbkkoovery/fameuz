<?php
include_once(DIR_ROOT."class/notifications.php");
$objNotifications		  =	new notifications();
?>
<link href="<?php echo SITE_ROOT?>css/user-profile.css" rel="stylesheet" type="text/css">
<link href="<?php echo SITE_ROOT?>fameuz_lightbox/fameuz_lightbox.css" rel="stylesheet">
<link href="<?php echo SITE_ROOT?>css/image_popup.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.css" />
<script src="<?php echo SITE_ROOT?>jw_player/jwplayer.js"></script>
<link href="<?php echo SITE_ROOT?>jw_player/jw_player.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery-ui.js"></script>
<script src="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>fameuz_lightbox/famuez_lightbox.js"></script>
<div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="user_profile">
						<?php echo $objCommon->checkEmailverification();?>
                            <div class="row">
                            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                            <div class="content">
                            	<div class="pagination_box">
                                	<a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
									<a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                	<a href="#" class="active"> My Notifications  </a>
                                 <?php
									include_once(DIR_ROOT."widget/notification_head.php");
								?>
                                </div>
                                 <div class="row">
                                <div class="col-lg-sp-4">
                            	 <?php
								include_once(DIR_ROOT."includes/profile_left.php");
								?>
                                </div>
                            	<div class="col-lg-sp-8"><div class="profile_content">
									<?php
									include_once(DIR_ROOT."widget/my_page_rank_head.php");
									?>
                                    <div class="profile_border_box wrap_review">
                                    	<h2 class="heading with_border"><span class="white_bg">Notifications</span></h2>
										<div class="notiList">
									
										</div>
                                    </div>
                                </div></div></div>
                            	<div class="clr"></div>            
                            </div></div> 
                          <?php
							include_once(DIR_ROOT.'includes/my_page_right.php');
							?> </div>            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<?php
	include_once(DIR_ROOT.'js/include/my_page.php');
	?>
<script>
$(document).ready(function(){
	$(".notiList").load('<?php echo SITE_ROOT?>ajax/notification_list.php');
});
</script>