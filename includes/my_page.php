<?php
include_once(DIR_ROOT."js/include/fameuz_lightbox_library.php");
?>
<link href="<?php echo SITE_ROOT?>css/user-profile.css" rel="stylesheet" type="text/css">
<link href="<?php echo SITE_ROOT?>css/user_update.css" rel="stylesheet">
<div class="inner_content_section">
    	<div class="container">
                    <div class="inner_top_border">
                    	<div class="user_profile">
						<?php echo $objCommon->checkEmailverification();?>
                        <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                            <div class="content">
                            <div class="tab-menu">
  							  <img src="<?php echo SITE_ROOT?>images/menu_icon.png" />
    							</div>
                            	<div class="pagination_box">
                                	<a href="<?php echo SITE_ROOT.'user/home'?>">Home <i class="fa fa-caret-right"></i></a>
                                    <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                    <a href="<?php echo SITE_ROOT.$user_url?>" class="active"> My profile </a>
                                    <?php
								include_once(DIR_ROOT."widget/notification_head.php");
								?>
                                </div>
                                <div class="row">
                                <div class="col-lg-sp-4">
                             	<?php
								include_once(DIR_ROOT."includes/profile_left.php");
								?>
                                </div>
                            	<div class="col-lg-sp-8"><div class="profile_content inside_profile">
									<?php
									include_once(DIR_ROOT."widget/my_page_rank_head.php");
									if($profileComplStatus !=100){
									include_once(DIR_ROOT."widget/my_page_profile_complete.php");
									}
									include_once(DIR_ROOT."widget/add_post_widget.php");
									include_once(DIR_ROOT."widget/my_page_latest_jobs.php");
									include_once(DIR_ROOT."widget/my_worked_together.php");
									include_once(DIR_ROOT."widget/my_page_latest_posts.php");
									echo '<div id="timelineFeeds"></div>';
									?>
                                </div></div></div>
                            	<div class="clr"></div>            
                            </div> 
                           </div>
                            <?php
							include_once(DIR_ROOT.'includes/my_page_right.php');
							?>  
                            </div>                    
                        </div>
                    </div>
            </div>
        </div>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/fixed_sidebar.js"></script>
<script type="text/javascript">

		$(window).load(function(e) {
			$('.sidebar').fixedSidebar();
			$('.profile_box').fixedSidebar();
		});
		//$(window).load(function(e) {
		$(document).ready(function(e) {
			$('body').fameuzLightbox({
				class : 'lightBoxs',
				sidebar: 'default',
				photos:{
					imageLoadUrl: '<?php echo SITE_ROOT.'ajax/popUpImageUser.php'?>',
					data_attr	: ['data-contentId','data-contentType','data-contentAlbum'],
					likeUrl	: '<?php echo SITE_ROOT?>ajax/like.php',
					shareUrl: '<?php echo SITE_ROOT?>ajax/share.php',
					workedUrl: '<?php echo SITE_ROOT?>ajax/worked_together.php',
					commentBoxUrl: '<?php echo SITE_ROOT.'widget/pop_image_comment_box.php' ?>',
					comments :{
						commentActionUrl:'<?php echo SITE_ROOT?>access/post_comment.php?action=add_comment',
						commentDelete: '<?php echo SITE_ROOT?>access/delete_comment.php',
					}
				},
				videos : {
					videoLoadUrl: '<?php echo SITE_ROOT.'ajax/popUpVideoUser.php'?>',
					data_attr	: ['data-vidEncrId'],
					likeUrl		: '<?php echo SITE_ROOT?>ajax/like_video.php',
					shareUrl	: '<?php echo SITE_ROOT?>ajax/share_video.php',
					commentBoxUrl:'<?php echo SITE_ROOT.'widget/pop_video_comment_box.php' ?>',
					comments :{
						commentActionUrl:'<?php echo SITE_ROOT?>access/post_video_comment.php?action=add_comment',
					}
				},
				skin: {
					next	: '<img src="<?php echo SITE_ROOT ?>fameuz_lightbox/images/next.png">',
					prev	: '<img src="<?php echo SITE_ROOT ?>fameuz_lightbox/images/prev.png">',
					reset	: '<i class="fa fa-refresh"></i>',
					close	: '<img src="<?php echo SITE_ROOT ?>fameuz_lightbox/images/close.png" width="15">',
					loader	: '<?php echo SITE_ROOT ?>images/ajax-loader.gif',
					review	: '<i class="fa fa-chevron-right"></i>',
					video	: '<?php echo SITE_ROOT ?>jw_player/six/six.xml',
				}
			});
		$('.dropMe_down').click(function(){
			$('.drop_down_delete').hide();
			$(this).children('.drop_down_delete').toggle();
		});
		$("body").mouseup(function(e){
        var subject = $(".dropMe_down"); 
		if(e.target.id != subject.attr('id') && !subject.has(e.target).length) {
             $(".drop_down_delete").fadeOut();
        }
});
			$('.comment_descr_input2').elastic();
			$("body").mouseup(function(e){
				var subject = $(".dropdown_option"); 
				if(e.target.id != subject.attr('id') && !subject.has(e.target).length) {
					 $(".dropdown_option").hide();
					 $(".user_thumb img").fadeIn();
				}
			});
			$(".review_message, .comment").click(function(){
				//var slidee = $(this).parent(".count-share-i").parent(".col-sm-5").parent(".row").parent(".botom-menus").parent(".desck").parent(".home_update").parent(".home_update_margin").children(".comment-sec");
				var slidee = $(this).parents(".home_update_margin").children(".comment-sec");
				var commentContent	=	$(this).data("comment");
				var commentCat		=	$(this).data("category");
				var noti_whome		=	$(this).data("for");
				$(".comment-sec").not(slidee).hide();
				$(".comment-sec textarea").not(slidee).val('');
				$(".home_update").toggleClass("no-border-sl");
				slidee.toggle();
				$(".loadComment_"+commentContent+"_"+commentCat).load('<?php echo SITE_ROOT?>widget/load_home_comment.php?commentContent='+commentContent+'&commentCat='+commentCat+'&noti_whome='+noti_whome);
			});
			/*$(".comments textarea").blur(function(){
				var texts= $(this).val().trim();
				if(texts.length === 0 || texts == ' '){
				$(".comment-sec").slideUp();
				$(".home_update").removeClass("no-border-sl");	
				}
			});*/
		 });
		 $(".like").on("click",".like_btn",function(){
			var likeId				=	$(this).data("like");
			var likeCat			   =	$(this).data("category");
			var noti_whome			=	$(this).data("userby");
			var that				  =	$(this);
			if(likeId != '' && likeCat != ''){
				$.ajax({
					url:'<?php echo SITE_ROOT?>ajax/like_home.php',
					data:{likeId:likeId,likeCat:likeCat,noti_whome:noti_whome},
					type:"POST",
					success: function(data2){
						$(that).parent().html(data2);
						$.get('<?php echo SITE_ROOT?>ajax/like_count_home.php',{likeId:likeId,likeCat:likeCat},function(data3){
							$(".likeCountDisplay_"+likeId+"_"+likeCat).html(data3);
						});
						return false;
					}
				})
			}
		 });
		 $(".share").on("click",".share_btn",function(){
			var shareId			   =	$(this).data("share");
			var shareCat			  =	$(this).data("category");
			var noti_whome			=	$(this).data("userby");
			var that				  =	$(this);
			if(shareId != '' && shareCat != ''){
				$.ajax({
					url:'<?php echo SITE_ROOT?>ajax/share_home.php',
					data:{shareId:shareId,shareCat:shareCat,noti_whome:noti_whome},
					type:"POST",
					success: function(data4){
						$(that).parent().html(data4);
						return false;
					}
				})
			}
		 });
		  $('.comment_descr_input1').keydown(function(event){			
			  if (event.keyCode == 13 && !event.shiftKey) {
				$(this).closest("form").find('.ajaxloader').show();
				var commentContent		   =	$(this).closest("form").data("comment");
				var commentCat			   =	$(this).closest("form").data("category");
				var noti_whome			   =	$(this).closest("form").data("for");
				$(this).closest("form").ajaxForm(
					{
						success:function(){
							$(".loadComment_"+commentContent+"_"+commentCat).load('<?php echo SITE_ROOT?>widget/load_home_comment.php?commentContent='+commentContent+'&commentCat='+commentCat+'&noti_whome='+noti_whome,function(){
								$('.ajaxloader').hide();
								$(".comment_descr_input1").val('');
								$(".comment_descr_input2").val('');
								$(".commentCountDisplay_"+commentContent+"_"+commentCat).load('<?php echo SITE_ROOT?>ajax/comment_count_home.php?commentContent='+commentContent+'&commentCat='+commentCat);
								
							});
						}
					}).submit();
				return false;
			  }
		});
		var sH	=	$(window).height();
		var sW	=	$(window).width();

		$(".likedPopUp").on("click",function(){
			var postid				=	$(this).data("postid");
				posttype			  =	$(this).data("posttype");
			if(postid != '' && posttype != ''){
				$(".loadLikedUserName").load('<?php echo SITE_ROOT?>widget/liked_users_name.php?postid='+postid+'&posttype='+posttype);
			}
		});
	</script>

<script type="text/javascript">

/*--------------------Delete Post---------------------*/
function deletePost(a){
	$.confirm({
		'title'		: 'Delete Confirmation',
		'message'	: 'You are about to delete this post ?',
		'buttons'	: {
		'Yes'	: {
		'class'	: 'blue',
		'action': function(){
			var catId	=	$(a).parent('ul').parent('a').data("category");
			var dataId	=	$(a).parent('ul').parent('a').data("id");
			$.ajax({
				url:'http://localhost/fameuz/ajax/delete_timeline.php',
				data:{dataID:dataId,dataCat:catId},
				type:"GET",
				success: function(result){
					if(result=='success'){
						$(a).parent().parent().parent('div').parent('div').remove();
					}
				}
			});
		}
		},
		'No'	: {
		'class'	: 'gray',
		'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
		}
		}
	});
}
/*------------------Delete Post End------------------*/
 /*$(window).scroll(function(){
		if ($(window).scrollTop() == $(document).height() - $(window).height()){
			if(parseInt($(".pagenum:last").val()) <= parseInt($(".total-page").val())) {
				var pagenum = parseInt($(".pagenum:last").val()) + 1;
				getresult('<?php echo SITE_ROOT?>widget/my_page_latest_posts.php?page='+pagenum);
			}
		}
	}); */
</script>
<script type="text/javascript">
 var preloader		=	'<div class="feedLoader"><div class="feedContainer"><div class="row"><div class="col-sm-1"><div class="img-s"></div></div><div class="col-sm-11"><div class="rectangle-md"><div class="rectangle"></div><div class="rectangle-sm pull-right"></div></div><div class="rectangle-lg"><div class="rectangle-sm"></div><div class="rectangle-sm"></div><div class="rectangle-sm"></div></div><div class="rectangle-lg"><div class="rectangle-full"></div></div></div></div></div><div class="line"></div><div class="rectangle-wd"><div class="rectangle-sm-down"></div><div class="rectangle-sm-down"></div><div class="rectangle-sm-down"></div></div></div>';
function getresult(url) {
	$('#timelineFeeds').append(preloader);
	$.ajax({
		url: url,
		type: "GET",
		data:  {rowcount:$("#rowcount").val()},
		success: function(data){
			 $('#timelineFeeds').append(data);  
			 $('.feedLoader').hide(); 
			 if($("#total-count").val()>0){
			  }else{
				$('#timelineFeeds').html('<div class="dummy_box default_no_image"></div>');
			  }
		},
		error: function(){} 	        
   });
}
 $(window).scroll(function(){
		if ($(window).scrollTop() == $(document).height() - $(window).height()){
			if(parseInt($(".pagenum:last").val()) <= parseInt($(".total-page").val())) {
				var pagenum = parseInt($(".pagenum:last").val()) + 1;
				getresult('<?php echo SITE_ROOT?>widget/my_page_latest_posts.php?page='+pagenum);
			}
		}
	}); 
</script>

	<?php
	include_once(DIR_ROOT.'js/include/my_page.php');
	?>