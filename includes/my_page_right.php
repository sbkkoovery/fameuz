<div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3">
 <div class="sidebar">
	<div class="search_box"><form action="<?php echo SITE_ROOT.'user/search-result'?>" method="get"><input type="text" placeholder="Search "   name="keywordSearch"/></form></div>
    <div class="background-white">
	<!--------------ad start----------->
		<script type='text/javascript'>
			  var googletag = googletag || {};
			  googletag.cmd = googletag.cmd || [];
			  (function() {
				var gads = document.createElement('script');
				gads.async = true;
				gads.type = 'text/javascript';
				var useSSL = 'https:' == document.location.protocol;
				gads.src = (useSSL ? 'https:' : 'http:') +
				  '//www.googletagservices.com/tag/js/gpt.js';
				var node = document.getElementsByTagName('script')[0];
				node.parentNode.insertBefore(gads, node);
			  })();
			</script>
			<script type='text/javascript'>
			  googletag.cmd.push(function() {
				googletag.defineSlot('/194259912/fameuz_burjalsafa_300x233', [300, 233], 'div-gpt-ad-1454585006799-0').addService(googletag.pubads());
			  <!--  googletag.pubads().enableSingleRequest();-->
				googletag.enableServices();
			  });
			</script>
				<div id='div-gpt-ad-1454585006799-0' style='height:233px; width:300px;'>
					<script type='text/javascript'>
					googletag.cmd.push(function() { googletag.display('div-gpt-ad-1454585006799-0'); });
					</script>
				</div>
				<!--------------ad end----------->
    </div>
   
	<?php
	include_once(DIR_ROOT."class/profile_visitors.php");
	$objProfileVisitors		=	new profile_visitors();
	$getProfileVisitors		=	$objProfileVisitors->listQuery("select user.first_name,user.last_name,user.display_name,profileImg.upi_img_url,user.email,social.usl_fameuz from profile_visitors as visitors  left join users as user on visitors.visited_by=user.user_id LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1  where visitors.visited_me='".$_SESSION['userId']."' order by visitors.visited_time desc limit 3");
	if(count($getProfileVisitors)>0){
	?>
	 <div class="background-white">
	<div class="visitors_box margin_btm14">
		<h2 class="heading with_border no-color">
			<span>Profile Visitors </span>
			<span class="seemore"><a href="<?php echo SITE_ROOT?>user/visitors">See More <i class="fa fa-play "></i></a></span>
		</h2>
		<div class="visitor_images">
			<?php
			foreach($getProfileVisitors as $allProfileVisitors){
				$allProfileVisitorsImg	=	SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.(($allProfileVisitors['upi_img_url'])?$allProfileVisitors['upi_img_url']:"profile_pic.jpg");
				$allProfileVisitorsName		= $objCommon->displayNameSmall($allProfileVisitors,10);
			?>
			<a href="<?php echo SITE_ROOT.$objCommon->html2text($allProfileVisitors['usl_fameuz'])?>"  title="<?php echo $allProfileVisitorsName?>">
				<span class="thumb"><img src="<?php echo $allProfileVisitorsImg?>"  width="99"  alt="<?php echo $allProfileVisitorsName?>" title="<?php echo $allProfileVisitorsName?>"/></span>
                <span class="shasow-up"></span>
				<span class="title"><?php echo $allProfileVisitorsName?></span>
			</a>
			<?php
			}
			?>
		</div>
	</div>
	</div>
	<?php
	}
	?>
    
    
	<?php
	if(count($getMyReviews)>0){
	?>
	<div class="background-white">
	<div class="profile_border_box reviews no_pd_l_r">
		<h2 class="heading">My Recent Reviews</h2>
		<?php
		foreach($getMyReviews as $allMyReviews){
			$getrate			=	(int) $allMyReviews['review_rate'];
			$getRateStyle	   =	'style="width:'.($getrate*20).'%;"';
	    ?>
		<div class="review_box">
			<?php
			$objCommon->user_info_widget(($allMyReviews['display_name'])?$objCommon->html2text($allMyReviews['display_name']):$objCommon->html2text($allMyReviews['first_name']),$allMyReviews['upi_img_url'],date("d M , Y",strtotime($objCommon->local_time($allMyReviews['review_date']))),'videography','');
			?>
			<div class="text">
				<p class="review"><?php echo (strlen($objCommon->html2textarea($allMyReviews['review_msg']))>130)?mb_substr($objCommon->html2textarea($allMyReviews['review_msg']),0,130,'utf-8').'...':$objCommon->html2textarea($allMyReviews['review_msg']);?></p>
				
			</div>
			<div class="clr"></div>
		</div>
		<?php
		}
		?>
		<p class="margin_btm0"><a href="<?php echo SITE_ROOT.'/user/my-reviews'?>" class="seemore">See More <i class="fa fa-play "></i></a></p>
	</div>
	 </div>
	<?php
	}
	?>
   
    <div class="background-white">
	<!--------------ad start----------->
		<script type='text/javascript'>
		  googletag.cmd.push(function() {
			googletag.defineSlot('/194259912/fameuz_burjalsafa2_300x233', [300, 233], 'div-gpt-ad-1454585737202-0').addService(googletag.pubads());
			googletag.pubads().enableSingleRequest();
			googletag.enableServices();
		  });
		</script>
		<div id='div-gpt-ad-1454585737202-0' style='height:233px; width:300px;'>
			<script type='text/javascript'>
			googletag.cmd.push(function() { googletag.display('div-gpt-ad-1454585737202-0'); });
			</script>
		</div>
				<!--------------ad end----------->
    </div>
</div>
</div> 
<div class="clr"></div>  