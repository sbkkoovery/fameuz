<?php
if (preg_match('%^https?://[^\s]+$%',$getVideoDetails['video_thumb'])) {
	$imgUrlshow			=	$getVideoDetails['video_thumb'];
} else {
	$imgUrlshow			=	SITE_ROOT.'uploads/videos/'.$getVideoDetails['video_thumb'];
}
?>
<div class="row">
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-sp-3">		  
				<div class="upload-preview" style="background-image:url('<?php echo $imgUrlshow?>');"></div>
				<div class="upload-link">
					<p>Your video will be live at : </p>
					<a href="<?php echo SITE_ROOT.'video/watch/'.$vidId?>"><?php echo SITE_ROOT.'video/watch/ '.$vidId?></a>
				</div>
						   
            </div>
            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-sp-9">
                <div class="row">
                <div class="col-sm-12 vid-info-title">
                <p><strong>Basic Info</strong></p>
                <form class="video-upload" method="post" action="<?php echo SITE_ROOT.'access/add_video_details.php'?>">
                	 <div class="form-group upload-help">
                        <label for="exampleInputEmail1">Title</label>
                        <div class="help-text">Videos are more interesting when they have creative titles. We know you can do better than "My Video."</div>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="img_title" value="<?php echo $objCommon->html2text($getVideoDetails['video_title'])?>">
                      </div>
                      <div class="form-group upload-help">
                        <label for="exampleInputPassword1">Description</label>
                         <div class="help-text"></div>
                        <textarea class="form-control" id="exampleInputPassword1" name="img_descr"><?php echo $objCommon->html2text($getVideoDetails['video_descr'])?></textarea>
                      </div>
                      <div class="row">
                      <div class="col-sm-6">
                      <div class="form-group upload-help">
                        <label for="exampleInputEmail1">Tags</label>
                        <div class="help-text">Add related tags (separated by commas, please!).</div>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="video_tags" value="<?php echo $objCommon->html2text($getVideoDetails['video_tags'])?>">
                      </div>
                      </div>
                      <div class="col-sm-6">
                      <div class="form-group upload-help">
                        <label for="exampleInputEmail1">Privacy Settings</label>
                        <div class="help-text">&nbsp;</div>
                        <select class="form-control" name="privacy">
                          <option value="1" <?php echo ($getVideoDetails['video_privacy']==='1,0,0,0')?'selected="selected"':''?>>Public</option>
						  <option value="2" <?php echo ($getVideoDetails['video_privacy']==='0,1,0,0')?'selected="selected"':''?>>Friends</option>
						  <option value="4" <?php echo ($getVideoDetails['video_privacy']==='0,0,0,1')?'selected="selected"':''?>>Only for me</option>
                         </select>
                      </div>
                      </div>
					  <div class="row pubVidDet">
					  </div>
					  
                      </div>
					  <input type="hidden" name="videoId" value="<?php echo $getVideoDetails['video_id']?>" />
                      <button type="submit" class="btn btn-default">Publish</button>
                </form>
                </div>
                </div>
            </div>
            </div>