<div class="profile_box">
                                	<div class="profile_pic">
                                    	<div class="pro_pic">
										<?php
										if($getMyFriendDetails['upi_id']){
											?>
											<a href="javascript:;" data-contentId="<?php echo $getMyFriendDetails['upi_id']?>" data-contentType="6" data-contentAlbum="0" class="propic_a ovr <?php echo($getMyFriendDetails['upi_img_url'])?'lightBoxs':''?>"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($getMyFriendDetails['upi_img_url'])?$getMyFriendDetails['upi_img_url']:'profile_pic.jpg'?>" width="200" alt="pro_pic" /></a>
											<?php
										}else{
											?>
											<a href="javascript:;"  class="propic_a ovr "><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($getMyFriendDetails['upi_img_url'])?$getMyFriendDetails['upi_img_url']:'profile_pic.jpg'?>" width="200" alt="pro_pic" /></a>
											<?php
										}
										if($friendStatus=='1'){
											echo '<a href="#" class="follow_btn follow following" id="follow_btn"><i class="fa fa-check"></i><span class="text">Following</span></a>';
										}else{
											echo '<a href="#" class="follow_btn follow" id="follow_btn"><i class="fa fa-plus"></i><span class="text">Follow</span></a>';
										}
										?>
                                        </div>
                                        <h2 class="name"><?php echo $displayNameFriend?></h2>
                                        <p class="username"><?php echo $objCommon->html2text($getMyFriendDetails['c_name'])?></p>
										<div class="friedChatStatus"></div>
										<?php
										//include_once(DIR_ROOT."ajax/friend_chat_status.php");
										?>
                                        <div class="raning"><?php echo $getMyFriendDetails['main_cat_rank']?><br><span>Ranking</span></div>
                                        <div class="follows_no_box">
										<?php
										$getFollowingCount	=	$objFollowing->count("(follow_user1='".$getMyFriendDetails['user_id']."') or (follow_user2='".$getMyFriendDetails['user_id']."' and follow_status='2')");
										$getFollowerCount	 =	$objFollowing->count("(follow_user2='".$getMyFriendDetails['user_id']."') or (follow_user1='".$getMyFriendDetails['user_id']."' and follow_status='2')");
										?>
                                        	<div class="follows_no">
                                            	<?php echo $getFollowingCount?><br> <span>Following</span>
                                            </div>
                                        	<div class="follows_no">
                                            	<?php echo $getFollowerCount?><br> <span>Followers</span>
                                            </div>
                                        	<div class="clr"></div>
                                        </div>
                                        <div class="profile_about">
                                        	<h2 class="about"><a href="<?php echo SITE_ROOT.'user/'.$user_url.'/about-me'?>">About Me</a> </h2>
											<?php
											if($getMyFriendDetails['n_name']){
											echo '<h2>Nationality</h2><div class="national"><span class="flag"><i class="fa fa-flag-checkered"></i></span>'.$objCommon->html2text(ucfirst($getMyFriendDetails['n_name'])).'</div>';
											}
											if($getMyFriendDetails['p_languages'] && is_array($disply_see_arr) && in_array("language",$disply_see_arr)){
												$friendLang	=	explode(",",$getMyFriendDetails['p_languages']);
												$friendLang	=	array_filter($friendLang);
												echo '<h2>Languages </h2><ul class="list_border">';
												foreach($friendLang as $allFriendLang){
													echo '<li>'.$objCommon->html2text($allFriendLang).'</li>';
												}
												echo '</ul>';
											}
											if($getMyFriendDetails['uc_s_id']){
												include_once(DIR_ROOT."class/sub_category.php");
												$objSubCategory		=	new sub_category();
												$getFriendInterest	 =	$objSubCategory->getAll("s_id IN (".trim($getMyFriendDetails['uc_s_id'],',').")");
												echo '<h2>Interested in </h2><ul class="list_border">';
												foreach($getFriendInterest as $allFriendInterest){
													echo '<li><a href="#"><i class="fa fa-chevron-right"></i>'.$objCommon->html2text($allFriendInterest['s_name']).'</a></li>';
												}
												echo '</ul>';
											}
											?>
											<?php
											if($getMyFriendDetails['uc_m_id']==2 || $getMyFriendDetails['vi_url'] !=''){
											?>
                                             <h2>Gallery</h2>
                                             <ul class="list_border">
											 	<?php
												if($getMyFriendDetails['uc_m_id']==2){
												?>
                                             	<li><a href="<?php echo SITE_ROOT.'user/'.$user_url.'/composite-card'?>"><i class="fa fa-chevron-right"></i>Composite Card</a></li>
												
												<li><a href="<?php echo SITE_ROOT?>user/<?php echo $user_url?>/polaroids"><i class="fa fa-camera"></i>Polaroids</a></li>
												<?php
												}
												if($getMyFriendDetails['vi_url'] !=''){
												?>
												<li><a href="javascript:;" data-toggle="modal" data-target="#videoIntroduction" class="callDisplayIntoduction"><i class="fa fa-video-camera"></i>Video Introduction</a></li>
												<?php
												}
												?>
                                             </ul>
											 <?php
											}
											?>
                                        	<!--<h2 class="about blog"><a href="#">Blogs</a> </h2>-->
                                        </div>
                                    </div>
									<?php
									if($getModelDetails['model_dis_id']){
										include_once(DIR_ROOT."class/model_disciplines.php");
										$objModelDisiplines		=	new model_disciplines();
										$getFriendDesci			=	$objModelDisiplines->getAll("model_dis_id IN (".trim($getModelDetails['model_dis_id'],',').")");
										echo '<div class="profile_tabs"><h2>Disciplines </h2><ul class="list_border">';
										foreach($getFriendDesci as $allFriendDesci){
										echo '<li><a href="#"><i class="fa fa-chevron-right"></i>'.$objCommon->html2text($allFriendDesci['model_dis_name']).'</a></li>';
										}
										echo '</ul></div>';
									}
									?>
                                    <div class="profile_tabs list_bold">
                                        <ul>
                                        	<?php /*?><li>
                                                <a href="<?php echo SITE_ROOT.'games/user/'.$user_url?>">
                                                    <i class="fa fa-gamepad icons"></i>
                                                    Games
                                                    <i class="fa fa-play arrow_list"></i>
                                                </a>
                                            </li><?php */?>
                                        	<li>
                                                <a href="<?php echo SITE_ROOT.'user/'.$user_url.'/music-list'?>">
                                                    <i class="fa fa-music icons"></i>
                                                     Music
                                                    <i class="fa fa-play arrow_list"></i>
                                                </a>
                                            </li>
                                        	<li>
                                                <a href="<?php echo SITE_ROOT.'user/'.$user_url.'/albums#videos'?>">
                                                    <i class="fa fa-video-camera icons"></i>
                                                     Videos
                                                    <i class="fa fa-play arrow_list"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    
                                </div>
                                	<?php
								include_once(DIR_ROOT."includes/my_profile_ipad.php");
								?>
								
<script language="javascript" type="application/javascript">
$(document).ready(function(){
	/*$('.following').hover(function(){
		$('.folow_option').css({"visibility":"visible"});
					
	});
	$('.following').hover(function(){
		$('.folow_option').css({"visibility":"hidden"});
					
	});
	$('.following').click(function(){
		$(this).next(".unfollow_pop").fadeToggle();
	});
	$('.unfollow').click(function(){
			$.ajax({
				url: '<?php echo SITE_ROOT?>ajax/unfollow_friend.php?friendId=<?php echo $getMyFriendDetails['user_id']?>', 
				success: function(result){
					$("#follow_btn").removeClass('following');
					$("#follow_btn").addClass('follow');
					var icon = $("#follow_btn").find('i')
					icon.addClass('fa-plus');
					icon.removeClass('fa-check');
					$("#follow_btn").find('.text').html("Follow");
					$(".folow_option").remove();
					$(".unfollow_pop").fadeToggle();
			}});
	});*/
});
</script>
<div class="modal fade" id="videoIntroduction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog videoIntroModal" role="document">
		<div class="modal-content">
			<div class="modal-header vidIntroHead">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><?php echo $objCommon->displayName($getMyFriendDetails);?></h4>
			</div>
			<div class="modalDisplayIntoduction"></div>
		</div>
	</div>
</div>
<script>
$(".callDisplayIntoduction").on("click",function(){
	$(".modalDisplayIntoduction").load('<?php echo SITE_ROOT?>widget/display_video_introduction.php?user_id=<?php echo $getMyFriendDetails['user_id']?>');
});
</script>