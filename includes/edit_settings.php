<?php
@session_start();
include_once("site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/privacy_settings.php");
$objCommon		   				 =	new common();
$objPrivacySettings				=	new privacy_settings();
$getPrivacyDetails				 =	$objPrivacySettings->getRow("user_id=".$_SESSION['userId']);
if($getPrivacyDetails['user_id']){
	$privacySettingArr			 =	unserialize($getPrivacyDetails['ps_value']);
}
?>
  <div class="privacy_settings">
    <p class="pic_dis">Privacy Settings&nbsp;&nbsp;<i class="fa fa-lock"></i></p>
    <h2 class="bg_heading">Privacy Settings And Tools</h2>
  </div>
  <form method="post" action="<?php echo SITE_ROOT.'access/edit_setting.php'?>">
    <div class="form-group">
      <div class="label-div">
        <label class="control-label">Who can see my profile ?</label>
      </div>
      <div class="selectbox"> 
      <span class="form-control input-sm slideon">
      <span class="drop-arrow"><img src="<?php echo SITE_ROOT?>images/select_arrow_1.png" /></span>
      	<span class="changeme">Select One Option</span>
       </span>
        <div class="option">
		  <?php
		  $profile_see_arr			=	$privacySettingArr['profile_see'];
		  ?>
          <div>
		  	<label>
            <input type="checkbox" name="profile_see[]" value="1" <?php echo (is_array($profile_see_arr) && in_array("1",$profile_see_arr)?'checked':''); ?> />
            <span class="option-list">Members</span>
			</label>
		  </div>
          <div>
		  	<label>
            <input type="checkbox" name="profile_see[]" value="2" <?php echo (is_array($profile_see_arr) && in_array("2",$profile_see_arr)?'checked':''); ?> />
            <span class="option-list">Friends</span>
			</label>
		  </div>
          <div>
		  	<label>
            <input type="checkbox" name="profile_see[]" value="5" <?php echo (is_array($profile_see_arr) && in_array("5",$profile_see_arr)?'checked':''); ?> />
            <span class="option-list">Friends of friends</span>
			</label>
		  </div>
          <div>
		  	<label>
            <input type="checkbox" name="profile_see[]" value="3" <?php echo (is_array($profile_see_arr) && in_array("3",$profile_see_arr)?'checked':''); ?> />
            <span class="option-list">Professionals</span>
			</label>
		 </div>
          <div>
		  	<label>
            <input type="checkbox" name="profile_see[]" value="4" <?php echo (is_array($profile_see_arr) && in_array("4",$profile_see_arr)?'checked':''); ?> />
            <span class="option-list">Only me</span>
			</label>
		  </div>
         <!-- <div>
          <fieldset>
            <input type="checkbox" class="popup"/>
            <span class="option-list">Custom</span>
		  </fieldset>
        </div>-->
      </div>
    </div>
	</div>
    <div class="form-group">
      <div class="label-div">
        <label class="control-label">Who can see my personal details ?</label>
      </div>
      <div class="selectbox"> 
    	<span class="form-control input-sm slideon">
         <span class="drop-arrow"><img src="<?php echo SITE_ROOT?>images/select_arrow_1.png" /></span>
      	<span class="changeme">Select One Option</span>
       </span>
	    <?php
		  $personal_see_arr			=	$privacySettingArr['personal_see'];
		?>
        <div class="option">
          <div>
		  	<label>
            <input type="checkbox" name="personal_see[]" value="1" <?php echo (is_array($personal_see_arr) && in_array("1",$personal_see_arr)?'checked':''); ?> />
            <span class="option-list">Members</span>
			</label>
		  </div>
          <div>
		  	<label>
            <input type="checkbox" name="personal_see[]" value="2" <?php echo (is_array($personal_see_arr) && in_array("2",$personal_see_arr)?'checked':''); ?> />
            <span class="option-list">Friends</span>
			</label>
		  </div>
          <div>
		  	<label>
            <input type="checkbox" name="personal_see[]" value="5"  <?php echo (is_array($personal_see_arr) && in_array("5",$personal_see_arr)?'checked':''); ?> />
            <span class="option-list">Friends of friends</span>
		  	</label>
		  </div>
          <div>
		  	<label>
            <input type="checkbox" name="personal_see[]" value="3" <?php echo (is_array($personal_see_arr) && in_array("3",$personal_see_arr)?'checked':''); ?> />
            <span class="option-list">Professionals</span>
			</label>
		 </div>
          <div>
		  	<label>
            <input type="checkbox" name="personal_see[]" value="4" <?php echo (is_array($personal_see_arr) && in_array("4",$personal_see_arr)?'checked':''); ?> />
            <span class="option-list">Only me</span>
			</label>
		  </div>
         <!-- <div>
          <fieldset>
            <input type="checkbox" class="popup"/>
            <span class="option-list">Custom</span>
		  </fieldset>
        </div>-->
      </div>
      </div>
    </div>
    <div class="form-group">
      <div class="label-div">
        <label class="control-label">Who can see my photos ?</label>
      </div>
      <div class="selectbox"> 
      <span class="form-control input-sm slideon">
       <span class="drop-arrow"><img src="<?php echo SITE_ROOT?>images/select_arrow_1.png" /></span>
      	<span class="changeme">Select One Option</span>
       </span>
        <div class="option">
		<?php
		 $photos_see_arr			=	$privacySettingArr['photos_see'];
		?>
          <div>
		  	<label>
            <input type="checkbox" name="photos_see[]" value="1" <?php echo (is_array($photos_see_arr) && is_array($photos_see_arr) && in_array("1",$photos_see_arr)?'checked':''); ?> />
            <span class="option-list">Members</span>
			</label>
		  </div>
          <div>
		  	<label>
            <input type="checkbox" name="photos_see[]" value="2" <?php echo (is_array($photos_see_arr) && in_array("2",$photos_see_arr)?'checked':''); ?> />
            <span class="option-list">Friends</span>
			</label>
		  </div>
          <div>
		  	<label>
            <input type="checkbox" name="photos_see[]" value="5" <?php echo (is_array($photos_see_arr) && in_array("5",$photos_see_arr)?'checked':''); ?> />
            <span class="option-list">Friends of friends</span>
			</label>
		  </div>
          <div>
		  	<label>
            <input type="checkbox" name="photos_see[]" value="3" <?php echo (is_array($photos_see_arr) && in_array("3",$photos_see_arr)?'checked':''); ?> />
            <span class="option-list">Professionals</span>
			</label>
		 </div>
          <div>
		  	<label>
            <input type="checkbox" name="photos_see[]" value="4" <?php echo (is_array($photos_see_arr) && in_array("4",$photos_see_arr)?'checked':''); ?> />
            <span class="option-list">Only me</span>
			</label>
		  </div>
         <!-- <div>
          <fieldset>
            <input type="checkbox" class="popup"/>
            <span class="option-list">Custom</span>
		  </fieldset>
        </div>-->
      </div>
      </div>
    </div>
    <div class="form-group">
      <div class="label-div">
        <label class="control-label">Who can see my reviews ? </label>
      </div>
      <div class="selectbox"> 
      <span class="form-control input-sm slideon">
       <span class="drop-arrow"><img src="<?php echo SITE_ROOT?>images/select_arrow_1.png" /></span>
      	<span class="changeme">Select One Option</span>
       </span>
        <div class="option">
		<?php
		 $review_see_arr			=	$privacySettingArr['review_see'];
		?>
          <div>
		  	<label>
            <input type="checkbox" name="review_see[]" value="1" <?php echo (is_array($review_see_arr) && in_array("1",$review_see_arr)?'checked':''); ?> />
            <span class="option-list">Members</span>
			</label>
		  </div>
          <div>
		  	<label>
            <input type="checkbox" name="review_see[]" value="2"  <?php echo (is_array($review_see_arr) && in_array("2",$review_see_arr)?'checked':''); ?> />
            <span class="option-list">Friends</span>
			</label>
		  </div>
          <div>
		  	<label>
            <input type="checkbox" name="review_see[]" value="5"  <?php echo (is_array($review_see_arr) && in_array("5",$review_see_arr)?'checked':''); ?> />
            <span class="option-list">Friends of friends</span>
			</label>
		  </div>
          <div>
		  	<label>
            <input type="checkbox" name="review_see[]" value="3"  <?php echo (is_array($review_see_arr) && in_array("3",$review_see_arr)?'checked':''); ?> />
            <span class="option-list">Professionals</span>
			</label>
		 </div>
          <div>
		  	<label>
            <input type="checkbox" name="review_see[]" value="4"  <?php echo (is_array($review_see_arr) && in_array("4",$review_see_arr)?'checked':''); ?> />
            <span class="option-list">Only me</span>
			</label>
		  </div>
         <!-- <div>
          <fieldset>
            <input type="checkbox" class="popup"/>
            <span class="option-list">Custom</span>
		  </fieldset>
        </div>-->
      </div>
      </div>
    </div>
    <div class="form-group">
      <div class="label-div">
        <label class="control-label">Who can see members whom I worked with ?</label>
      </div>
      <div class="selectbox"> 
      <span class="form-control input-sm slideon">
      <span class="drop-arrow"><img src="<?php echo SITE_ROOT?>images/select_arrow_1.png" /></span>
      	<span class="changeme">Select One Option</span>
       </span>
        <div class="option">
		<?php
		 $worked_see_arr			=	$privacySettingArr['worked_see'];
		?>
          <div>
		  	<label>
            <input type="checkbox" name="worked_see[]" value="1" <?php echo (is_array($worked_see_arr) && in_array("1",$worked_see_arr)?'checked':''); ?> />
            <span class="option-list">Members</span>
			</label>
		  </div>
          <div>
		  	<label>
            <input type="checkbox" name="worked_see[]" value="2" <?php echo (is_array($worked_see_arr) && in_array("2",$worked_see_arr)?'checked':''); ?> />
            <span class="option-list">Friends</span>
			</label>
		  </div>
          <div>
		  	<label>
            <input type="checkbox" name="worked_see[]" value="5" <?php echo (is_array($worked_see_arr) && in_array("5",$worked_see_arr)?'checked':''); ?> />
            <span class="option-list">Friends of friends</span>
			</label>
		  </div>
          <div>
		  	<label>
            <input type="checkbox" name="worked_see[]" value="3" <?php echo (is_array($worked_see_arr) && in_array("3",$worked_see_arr)?'checked':''); ?> />
            <span class="option-list">Professionals</span>
			</label>
		 </div>
          <div>
		  	<label>
            <input type="checkbox" name="worked_see[]" value="4" <?php echo (is_array($worked_see_arr) && in_array("4",$worked_see_arr)?'checked':''); ?> />
            <span class="option-list">Only me</span>
			</label>
		  </div>
         <!-- <div>
          <fieldset>
            <input type="checkbox" class="popup"/>
            <span class="option-list">Custom</span>
		  </fieldset>
        </div>-->
      </div>
      </div>
    </div>
  <h2 class="bg_heading">Display My</h2>
  <div class="display_my">
  	<?php
	 $disply_see_arr			=	$privacySettingArr['disply_see'];
	?>
    <div class="chkedbox">
	  <label>
      <input type="checkbox"  name="disply_see[]" value="language" <?php echo (is_array($disply_see_arr) && in_array("language",$disply_see_arr)?'checked':''); ?> />
      <span class="option-listd">Language</span></label>
	</div>
    <div class="chkedbox">
	  <label>
      <input type="checkbox" name="disply_see[]" value="location" <?php echo (is_array($disply_see_arr) && in_array("location",$disply_see_arr)?'checked':''); ?> />
      <span class="option-listd">Location</span>
	  </label>
	</div>
    <div class="chkedbox">
	  <label>
      <input type="checkbox"  name="disply_see[]" value="gender" <?php echo (is_array($disply_see_arr) && in_array("gender",$disply_see_arr)?'checked':''); ?> />
      <span class="option-listd">Gender</span>
	  </label>
	</div>
    <div class="chkedbox">
	  <label>
      <input type="checkbox" name="disply_see[]" value="ethnicity" <?php echo (is_array($disply_see_arr) && in_array("ethnicity",$disply_see_arr)?'checked':''); ?> />
      <span class="option-listd">Ethnicity</span>
	  </label>
	</div>
    <div class="chkedbox">
	  <label>
      <input type="checkbox" name="disply_see[]" value="height" <?php echo (is_array($disply_see_arr) && in_array("height",$disply_see_arr)?'checked':''); ?> />
      <span class="option-listd">Height</span>
	  </label>
	</div>
    <div class="chkedbox">
	  <label>
      <input type="checkbox" name="disply_see[]" value="eye_color" <?php echo (is_array($disply_see_arr) && in_array("eye_color",$disply_see_arr)?'checked':''); ?> />
      <span class="option-listd">Eye Color</span>
	  </label>
	</div>
    <div class="chkedbox">
	  <label>
      <input type="checkbox" name="disply_see[]" value="hair" <?php echo (is_array($disply_see_arr) && in_array("hair",$disply_see_arr)?'checked':''); ?> />
      <span class="option-listd">Hair</span>
	  </label>
	 </div>
    <div class="chkedbox">
	  <label>
      <input type="checkbox" name="disply_see[]" value="chest" <?php echo (is_array($disply_see_arr) && in_array("chest",$disply_see_arr)?'checked':''); ?> />
      <span class="option-listd">Bust/Chest</span>
	  </label>
	</div>
    <div class="chkedbox">
	  <label>
      <input type="checkbox" name="disply_see[]" value="waist" <?php echo (is_array($disply_see_arr) && in_array("waist",$disply_see_arr)?'checked':''); ?> />
      <span class="option-listd">Waist</span>
	  </label>
	</div>
    <div class="chkedbox">
	  <label>
      <input type="checkbox" name="disply_see[]" value="hips" <?php echo (is_array($disply_see_arr) && in_array("hips",$disply_see_arr)?'checked':''); ?> />
      <span class="option-listd">Hips</span>
	  </label>
	</div>
    <div class="chkedbox">
	  <label>
      <input type="checkbox" name="disply_see[]" value="dress" <?php echo (is_array($disply_see_arr) && in_array("dress",$disply_see_arr)?'checked':''); ?> />
      <span class="option-listd">Dress size</span>
	  </label>
	</div>
    <div class="chkedbox">
	  <label>
      <input type="checkbox" name="disply_see[]" value="shoe" <?php echo (is_array($disply_see_arr) && in_array("shoe",$disply_see_arr)?'checked':''); ?> />
      <span class="option-list">Shoe size</span>
	  </label>
	</div>
  </div>
<!--  <h2 class="bg_heading">Casting Settings</h2>
  <div class="display_my">
    <div class="chkedbox">
      <input type="checkbox" />
      <span class="option-listd">Allow casting invitations</span></div>
  </div>-->
  <h2 class="bg_heading">Booking Settings</h2>
  <?php
	 $book_me_arr			=	$privacySettingArr['book_me'];
  ?>
  <div class="display_my">
    <div class="row">
      <div class="col-sm-6">
        <div class="chkedbox">
		  <label>
          <input type="checkbox" name="book_me[]" value="1" <?php echo (is_array($book_me_arr) && in_array("1",$book_me_arr)?'checked':''); ?> />
          <span class="option-listd">All members can book me</span>
		  </label>
		</div>
        <div class="chkedbox">
		  <label>
          <input type="checkbox" name="book_me[]" value="5" <?php echo (is_array($book_me_arr) && in_array("5",$book_me_arr)?'checked':''); ?>  />
          <span class="option-listd">Friends of friends can book me</span>
		  </label>
		  </div>
        <div class="chkedbox">
          <label>
		  <input type="checkbox" name="book_me[]" value="4" <?php echo (is_array($book_me_arr) && in_array("4",$book_me_arr)?'checked':''); ?> />
          <span class="option-listd">Nobody can book me</span>
		  </label>
		  </div>
      </div>
      <div class="col-sm-6">
        <div class="chkedbox">
          <label>
		  <input type="checkbox"  name="book_me[]" value="2" <?php echo (is_array($book_me_arr) && in_array("2",$book_me_arr)?'checked':''); ?> />
          <span class="option-listd">Friends can book me</span>
		  </label>
		  </div>
        <div class="chkedbox">
		  <label>
          <input type="checkbox" name="book_me[]" value="3" <?php echo (is_array($book_me_arr) && in_array("3",$book_me_arr)?'checked':''); ?> />
          <span class="option-listd">Professionals can book me</span>
		  </label>
		</div>
        <!--<div class="chkedbox">
          <input type="checkbox" class="popup"/>
          <span class="option-listd">Custom</span></div>-->
      </div>
    </div>
  </div>
  <h2 class="bg_heading">Email Notifications When</h2>
   <?php
	 $email_notification_arr			=	$privacySettingArr['email_notification'];
  ?>
  <div class="display_my">
    <div class="chkedbox">
	  <label>
      <input type="checkbox"  name="email_notification[]" value="about_friends" <?php echo (is_array($email_notification_arr) && in_array("about_friends",$email_notification_arr)?'checked':''); ?> />
      <span class="option-listd">Updates about friends</span>
	  </label>
	  </div>
    <div class="chkedbox">
	  <label>
      <input type="checkbox" name="email_notification[]" value="booked_me" <?php echo (is_array($email_notification_arr) && in_array("booked_me",$email_notification_arr)?'checked':''); ?>  />
      <span class="option-listd">Someone booked me</span>
	  </label>
	  </div>
    <div class="chkedbox">
      <label>
	  <input type="checkbox" name="email_notification[]" value="messaged_me" <?php echo (is_array($email_notification_arr) && in_array("messaged_me",$email_notification_arr)?'checked':''); ?>   />
      <span class="option-listd">Someone messaged me</span>
	  </label>
	  </div>
    <div class="chkedbox">
      <label>
	  <input type="checkbox" name="email_notification[]" value="liked_photo" <?php echo (is_array($email_notification_arr) && in_array("liked_photo",$email_notification_arr)?'checked':''); ?>   />
      <span class="option-listd">Someone liked my photo</span>
	  </label>
	</div>
    <div class="chkedbox">
      <label>
	  <input type="checkbox" name="email_notification[]" value="liked_profile" <?php echo (is_array($email_notification_arr) && in_array("liked_profile",$email_notification_arr)?'checked':''); ?>  />
      <span class="option-listd">Someone liked my profile</span>
	  </label>
	  </div>
    <div class="chkedbox">
      <label>
	  <input type="checkbox" name="email_notification[]" value="liked_post" <?php echo (is_array($email_notification_arr) && in_array("liked_post",$email_notification_arr)?'checked':''); ?>   />
      <span class="option-listd">Someone liked my post</span>
	  </label>
	  </div>
    <div class="chkedbox">
	  <label>
      <input type="checkbox" name="email_notification[]" value="post_comment" <?php echo (is_array($email_notification_arr) && in_array("post_comment",$email_notification_arr)?'checked':''); ?>   />
      <span class="option-listd">Someone commented on my post</span>
	  </label>
	</div>
    <div class="chkedbox">
      <label>
	  <input type="checkbox"  name="email_notification[]" value="photo_comment" <?php echo (is_array($email_notification_arr) && in_array("photo_comment",$email_notification_arr)?'checked':''); ?>  />
      <span class="option-listd">Someone commented on my picture</span>
	  </label>
	</div>
    <div class="chkedbox">
	  <label>
      <input type="checkbox" name="email_notification[]" value="worked_me" <?php echo (is_array($email_notification_arr) && in_array("worked_me",$email_notification_arr)?'checked':''); ?>   />
      <span class="option-listd">Someone worked with me</span>
	  </label>
	 </div>
    <div class="chkedbox">
      <label>
	  <input type="checkbox" name="email_notification[]" value="new_blog" <?php echo (is_array($email_notification_arr) && in_array("new_blog",$email_notification_arr)?'checked':''); ?>   />
      <span class="option-listd">When there is a new blog</span>
	  </label>
	</div>
    <div class="chkedbox">
      <label>
	  <input type="checkbox" name="email_notification[]" value="new_casting" <?php echo (is_array($email_notification_arr) && in_array("new_casting",$email_notification_arr)?'checked':''); ?>   />
      <span class="option-listd">When there is a new casting</span>
	  </label>
	</div>
    <div class="chkedbox">
      <label>
	  <input type="checkbox" name="email_notification[]" value="new_review" <?php echo (is_array($email_notification_arr) && in_array("new_review",$email_notification_arr)?'checked':''); ?>   />
      <span class="option-listd">When there is a new review about me</span>
	  </label>
	</div>
    <div class="chkedbox">
      <label>
	  <input type="checkbox" name="email_notification[]" value="profile_visitor" <?php echo (is_array($email_notification_arr) && in_array("profile_visitor",$email_notification_arr)?'checked':''); ?>   />
      <span class="option-listd">When there is a new profile visitor</span>
	  </label>
	</div>
    <div class="chkedbox">
	  <label>
      <input type="checkbox" name="email_notification[]" value="market_place" <?php echo (is_array($email_notification_arr) && in_array("market_place",$email_notification_arr)?'checked':''); ?>   />
      <span class="option-listd">When there is a new marketplace update</span>
	  </label>
	</div>
    <div class="chkedbox">
	  <label>
      <input type="checkbox" name="email_notification[]" value="new_follower" <?php echo (is_array($email_notification_arr) && in_array("new_follower",$email_notification_arr)?'checked':''); ?> />
      <span class="option-listd">When there is a new follower</span>
	  </label>
	</div>
    <div class="chkedbox">
	  <label>
      <input type="checkbox" name="email_notification[]" value="ranking" <?php echo (is_array($email_notification_arr) && in_array("ranking",$email_notification_arr)?'checked':''); ?>  />
      <span class="option-listd">Updates about my ranking</span>
	  </label>
	</div>
    <div class="chkedbox">
      <div class="row">
        <div class="col-sm-4">
          <label>
		  <input type="checkbox" name="email_notification[]" value="profile_status" <?php echo (is_array($email_notification_arr) && in_array("profile_status",$email_notification_arr)?'checked':''); ?>  />
          <span class="option-listd">updates about my profile status</span>
		  </label>
		</div>
		<?php
		 $profile_status_select_arr			=	$privacySettingArr['profile_status_select'];
	  ?>
        <div class="col-sm-2"> 
          <select class="select-box-inline" name="profile_status_select[]">
            <option value="daily" <?php echo (is_array($profile_status_select_arr) && in_array("daily",$profile_status_select_arr)?'selected':''); ?>>Daily</option>
            <option value="weekly" <?php echo (is_array($profile_status_select_arr) && in_array("weekly",$profile_status_select_arr)?'selected':''); ?>>weekly</option>
            <option value="monthly" <?php echo (is_array($profile_status_select_arr) && in_array("monthly",$profile_status_select_arr)?'selected':''); ?>>Monthly</option>
            <option value="yearly" <?php echo (is_array($profile_status_select_arr) && in_array("yearly",$profile_status_select_arr)?'selected':''); ?>>Yearly</option>
          </select>
        </div>
      </div>
    </div>
    <div class="chkedbox">
      <input type="checkbox" name="email_notification[]" value="newsletter_subscribe" <?php echo (is_array($email_notification_arr) && in_array("newsletter_subscribe",$email_notification_arr)?'checked':''); ?> />
      <span class="option-listd">newsletter subscription for Fameuz</span></div>
  </div>
  <h2 class="bg_heading">Email Notifications When</h2>
  <div class="display_my">
    <div class="chkedbox">
      <input type="checkbox" name="email_notification[]" value="login_attempt" <?php echo (is_array($email_notification_arr) && in_array("login_attempt",$email_notification_arr)?'checked':''); ?> />
      <span class="option-listd">Get email alert when there are more than three login attempts</span></div>
    <div class="chkedbox">
      <input type="checkbox" name="email_notification[]" value="recover_password" <?php echo (is_array($email_notification_arr) && in_array("recover_password",$email_notification_arr)?'checked':''); ?> />
      <span class="option-listd">Use alternative email address or phone number to recover password</span></div>
  </div>
  <p class="pic_dis">* By default your primary email address and primary phone number will be used for recovering password</p>
  <div class="field_box_section"><div><input type="submit" value="Save Changes"></div></div>
   </form>
<div class="bg-popup">
  <div class="custom-box">
    <div class="pop-box-head">
      <p>Custom Settings<i class="fa fa-close pull-right closepop"></i></p>
    </div>
    <div class="main-content-popup">
      <p><i class="fa fa-plus"></i> &nbsp;Share These With</p>
      <form>
        <div class="form-group">
          <div class="input-group"> <span class="input-group-addon" id="basic-addon1"><i class="fa fa-plus"></i></span>
            <input type="email" class="form-control">
          </div>
        </div>
        <a href="javascript:;" class="btn btn-default closepop">Cancel</a>
        <button class="btn btn-default" type="submit">Save</button>
      </form>
    </div>
  </div>
</div>
</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".slideon").click(function(){
		var slideoption = $(this).parent().children(".option");
			$(".option").removeClass("toggleme");
			slideoption.addClass("toggleme");
			$(".option:not(.toggleme)").hide();
			$(".toggleme").slideToggle();
		});
		$(".option").children("div").addClass("chkval");
		$(".option").children("div").children("input").addClass("chke");
		var defaulttext = "Select One Option";
		
	$(".chkval").click(function(){
		var chkvalue = $(this).children(".option-list").html();
		var chngeme = $(this).parent().parent().find(".changeme");
		if($(this).children(".chke").prop('checked') == true){
			var insertinto = $(this).parent().parent().find(".changeme");
		$("<span></span>").addClass("selected").html(chkvalue).insertAfter(insertinto);
		}else if($(this).children(".chke").prop('checked') == false){
			var m = $(this).parent().parent(".selectbox").find(".selected");
			for(var i=0; i<m.length; i++){
					var htmlof = $(m[i]).html();
					if( htmlof== chkvalue){
						$(m[i]).remove();
					}
				}
				var x = $(this).parent().parent().find(".selected").size();
				if(x == 0){
					$(chngeme).show();
				}
			}
			//var winwidth = $(window).width();
//			if(winwidth < 1024){
//				
//				
//			}
		});
		
	var winheight = $( window ).height()/2;
	var boxheight = $(".custom-box").height()/2;
	var totalheight = winheight - boxheight;
		$(".custom-box").css("margin-top", totalheight);
		$('.popup').click(function() {
		$(".bg-popup").fadeToggle(this.checked);
		$(".option").fadeOut(this.checked);
		});
		$(".closepop").click(function(){
		$(".bg-popup").fadeOut();
		$(".popup").prop('checked', false).uniform();
		});
	});
	$(document).mouseup(function (e)
	{
		var container = $('.toggleme');
	
		if (!container.is(e.target)
			&& container.has(e.target).length === 0) {
			container.hide();
		}
	});
</script> 

