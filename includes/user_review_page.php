<link href="<?php echo SITE_ROOT?>css/user-view.css" rel="stylesheet" type="text/css">
<link href="<?php echo SITE_ROOT?>css/jquery_scroll.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery-ias.min.js"></script>
<?php
include_once(DIR_ROOT."class/user_reviews.php");
$objUserReviews						=	new user_reviews();
$limit 								 =	6;
$page 								  =	(int) (!isset($_GET['p'])) ? 1 : $_GET['p'];
$sql_reviews						   =	"SELECT review.review_id,review.review_msg,review.review_rate,review.review_date,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url from user_reviews AS review LEFT JOIN users AS user ON review.user_id_by=user.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 WHERE review.review_status=1 AND review.review_user_to_status=1  AND review.user_id_to='".$getMyFriendDetails['user_id']."' ORDER BY review.review_date desc ";
$start 								 =	($page * $limit) - $limit;
if(($objUserReviews->countRows($sql_reviews)) > ($page * $limit) ){
	$next = ++$page;
}
$getMyAllReviews						=	$objUserReviews->listQuery($sql_reviews. " LIMIT ".$start.", ".$limit);
?>
    <div class="inner_content_section">
    	<div class="container">
        	<div class="inner_top_border">
                    	<div class="user_profile">
						<?php echo $objCommon->checkEmailverification();?>
                            <div class="row"><div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9"><div class="content">
                            	<div class="pagination_box">
                                	<a href="<?php echo SITE_ROOT?>user/home">Home <i class="fa fa-caret-right"></i></a>
                                	<a href="#"><?php echo $displayNameFriend?><i class="fa fa-caret-right"></i></a>
                                	<a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                    <a href="javascript:;" class="active">Reviews</a>
                                    <?php
								include_once(DIR_ROOT."widget/notification_head.php");
								?>
                                </div>
                            	<?php 
								include_once(DIR_ROOT."includes/my_friend_page_left.php");
								?>
                            	<div class="profile_content">
									<div class="add_review_list">
										<div class="row">
											<div class="col-md-12 wrap_review">
												<h5>
													<span><?php echo $displayNameFriend?>'s</span> Reviews 
													<a href="<?php echo SITE_ROOT.'user/'.$user_url.'/add-review'?>" class="pull-right add_rev_btn">Add Review</a>
												</h5>
												<?php
												if(count($getMyAllReviews)>0){
													foreach($getMyAllReviews as $allMyReviews){
														$getrate			=	(int) $allMyReviews['review_rate'];
														$getRateStyle	   =	'style="width:'.($getrate*20).'%;"';
													?>
												<div class="review_box item_review" id="item_review-<?php echo $allMyReviews['review_id']?>">
													<div class="thumb"><img alt="review_thumb" src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($allMyReviews['upi_img_url'])?$allMyReviews['upi_img_url']:'profile_pic.jpg'?>"></div>
													<div class="text">
														<p class="review"><?php echo $objCommon->html2textarea($allMyReviews['review_msg']);?></p>
													</div>
													<div class="clr"></div>
													<div class="rating_box"><div <?php echo $getRateStyle?> class="rating_yellow"></div></div>
													<div class="time"><div class="posted">Posted by <span><?php echo ($allMyReviews['display_name'])?$objCommon->html2text($allMyReviews['display_name']):$objCommon->html2text($allMyReviews['first_name']);?>  </span> </div>  <i class="fa fa-clock-o"></i> <?php echo date("d M , Y",strtotime($objCommon->local_time($allMyReviews['review_date'])));?></div>
												</div>
												<?php
													}
													if (isset($next)){ ?>
														<div class="nav">
														<a href="<?php echo SITE_ROOT.'user/'.$user_url.'/reviews/'.$next?>">Next</a>
														</div>
														<?php
													}
													
												}else{
													echo '<p>No reviews found....</p>';
												}
												?>
											</div>
										</div>
									</div>
                                </div>
                            	<div class="clr"></div>            
                            </div></div> 
                               <?php include_once(DIR_ROOT."includes/my_friend_page_right.php");?>                    
                        </div></div>
                    </div>
                 </div>
    </div>
  <script type="text/javascript">
		$(document).ready(function(e) {
			$('.follow').click(function(e) {
				$.get('<?php echo SITE_ROOT?>ajax/sent_friend_request.php',{"friendId":'<?php echo $getMyFriendDetails['user_id']?>'},function(data){

				});
                $(this).addClass('following');
				var icon = $(this).find('i')
				icon.removeClass('fa-plus');
				icon.addClass('fa-check');
				$(this).find('.text').html("Following");
				return false;
            });
			$('.friedChatStatus').load('<?php echo SITE_ROOT?>ajax/friend_chat_status.php?fid=<?php echo $getMyFriendDetails['user_id']?>');
			var refreshId = setInterval(function()
			{
			$('.friedChatStatus').load('<?php echo SITE_ROOT?>ajax/friend_chat_status.php?fid=<?php echo $getMyFriendDetails['user_id']?>');
			}, 1000);
			 $('.rate-btn').hover(function(){
                $('.rate-btn').removeClass('rate-btn-hover');
                var therate = $(this).attr('id');
                for (var i = therate; i >= 0; i--) {
                    $('.rate-btn-'+i).addClass('rate-btn-hover');
                };
            });
			$(".review_relation").click(function(){
				var relVal	=	$(this).val();
				if(relVal==3){
					$(".review_relation_other").show();
				}else{
					$(".review_relation_other").hide();
				}
			});
			 $('.rate-btn').click(function(){
                var therate = $(this).attr('id');
				$("#rate_val").val(therate);
			 });
			  jQuery.ias({
                container : '.wrap_review', // main container where data goes to append
                item: '.item_review', // single items
                pagination: '.nav', // page navigation
                next: '.nav a', // next page selector
                loader: '<img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/ajax-loader.gif"/>', // loading gif
                triggerPageThreshold: 3 // show load more if scroll more than this
            });
        });
	</script>