<?php
@session_start();
include_once(DIR_ROOT."class/album_images.php");
include_once(DIR_ROOT."class/user_photos.php");
$objAlbumImages					=	new album_images();
$objUserPhotos					 =	new user_photos();
$getCoverImg1					  =	$objUserPhotos->getRowSql("select photo_url,photo_id,likes.like_id,likes.like_status from user_photos left join likes on user_photos.photo_id = likes.like_content and likes.like_user_id = ".$_SESSION['userId']." where user_id=".$getMyFriendDetails['user_id']." and photo_set_main=1 order by photo_created desc");
$getCoverimage					 =	$getCoverImg1['photo_url'];
$getCoverImageId				   =	$getCoverImg1['photo_id'];
$getCoverCat					   =	1;
$likeStatus						=	($getCoverImg1['like_id'] != '' && $getCoverImg1['like_status']==1)?1:0;
if($getCoverimage ==''){
	$getCoverImg1				  =	$objAlbumImages->getRowSql("select ai_images,ai_id,likes.like_id,likes.like_status from album_images left join likes on album_images.ai_id = likes.like_content and likes.like_user_id = ".$_SESSION['userId']." where user_id=".$getMyFriendDetails['user_id']." and ai_set_main=1 order by ai_created desc");
	$getCoverimage				 =	$getCoverImg1['ai_images'];
	$getCoverImageId			   =	$getCoverImg1['ai_id'];
	$getCoverCat				   =	2;
	$likeStatus					=	($getCoverImg1['like_id'] != '' && $getCoverImg1['like_status']==1)?1:0;
}
$_SESSION['friendId']	=	$getMyFriendDetails['user_id'];
include_once(DIR_ROOT."js/include/fameuz_lightbox_library.php");
?>
<link href="<?php echo SITE_ROOT?>css/user-view.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.elastic.source.js"></script>
    <div class="inner_content_section">
    	<div class="container">
                    <div class="inner_top_border">
                    	<div class="user_profile">
						<?php echo $objCommon->checkEmailverification();?>
                        <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                            <div class="content">
                            <div class="tab-menu">
  							  <img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/menu_icon.png" />
    							</div>
                            	<div class="pagination_box">
                                	<a href="<?php echo SITE_ROOT?>user/home">Home <i class="fa fa-caret-right"></i></a>
                                	<a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                    <a href="javascript:;" class="active"><?php echo $displayNameFriend?></a>
                                    <?php
								include_once(DIR_ROOT."widget/notification_head.php");
								?>
                                </div>
                            	<?php
								if($showPage==0){
									echo '<div class="" style="background-color:#fff; padding:15px;">Acces Denied</div>';
									exit;
								}
								include_once(DIR_ROOT."includes/my_friend_page_left.php");
								?>
                            	<div class="profile_content">
									<?php
									if($getCoverimage){
									?>
                                	<div class="profile_pic_big">
                                    	<div class="image"><a  data-contentalbum="-1" data-contenttype="<?php echo $getCoverCat?>" data-contentid="<?php echo $getCoverImageId?>" href="javascript:;" class="lightBoxs"><img src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/albums/'.$getCoverimage?>" alt="<?php echo $displayNameFriend?>" /></a></div>
                                        <div class="share_like_box">
                                        	<div class="review_message">
                                            	<a href="<?php echo SITE_ROOT.'user/'.$user_url.'/add-review'?>">Add Review <i class="fa fa-chevron-right"></i></a>
                                            	<a href="<?php echo SITE_ROOT.'user/single-image?type='.$getCoverCat.'&id='.$getCoverImageId?>">Comments <i class="fa fa-chevron-right"></i></a>
                                            </div>
                                            <div class="like_dup">
												<?php
												if($likeStatus==1){
													?>
													<a href="javascript:;" id="like_btn" data-like="<?php echo $getCoverImageId?>" data-likecat="<?php echo $getCoverCat?>" class="like_btn liked"><i class="fa fa-heart"></i> <span class="text">Liked</span> </a>
													<?php
												}else if($likeStatus==0){
													?>
													<a href="javascript:;" id="like_btn" data-like="<?php echo $getCoverImageId?>" data-likecat="<?php echo $getCoverCat?>" class="like_btn"><i class="fa fa-heart"></i> <span class="text">Like</span> </a>
													<?php
												}
												?>
                                            </div>
                                            <?php /*?><div class="share">
                                            	<a href="javascript:;" data-share="<?php echo $getCoverImageId?>" data-sharecat="<?php echo $getCoverCat?>" class="share_btn"><i class="fa fa-share-alt"></i>Share </a>
                                            </div><?php */?>
											<div class="worked">
											<?php
											include_once(DIR_ROOT."class/worked_together.php");
											$objWorkedTogether			=	new worked_together();
											$getWorkedDetails			 =	$objWorkedTogether->getRowSql("SELECT worked_id,from_status,to_status FROM worked_together WHERE img_cat=".$getCoverCat." and img_id =".$getCoverImageId." and send_from=".$_SESSION['userId']." and send_to=".$getMyFriendDetails['user_id']);
											if($getWorkedDetails['from_status']==1 && $getWorkedDetails['to_status']==1){
												?>
												<a href="javascript:;" class="worked_request_sent"><i class="fa fa-star-o"></i>Worked Together</a> 
											<?php
											}else if($getWorkedDetails['from_status']==1){
												?>
												<a href="javascript:;" class="worked_request_sent"><i class="fa fa-check"></i>Worked Together</a> 
											<?php
											}else{
											?>
												<a href="javascript:;" class="worked_request has-spinner" data-like="<?php echo $getCoverImageId?>" data-likecat="<?php echo $getCoverCat?>">
													<span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span>
													<i class="fa fa-users"></i>Worked Together 
												</a>
											<?php
											}
											?>
											</div>
                                        </div>
                                         <div class="shadow_provided"></div>
                                    </div>
									<?php
									}else{
										?>
										<div class="profile_pic_big">
											<div class="image"><img src="<?php echo SITE_ROOT_AWS_IMAGES.'images/cover.jpg'?>" alt="<?php echo $displayNameFriend?>" /></div>
											<div class="share_like_box">
												<div class="review_message">
													<a href="<?php echo SITE_ROOT.'user/'.$user_url.'/add-review'?>">Add Review <i class="fa fa-chevron-right"></i></a>
												</div>
											</div>
										</div>
										<?php
									}
									?>
									<div class="my_profile_tabs">
										<!-- Nav tabs -->
										  <ul class="nav nav-tabs" role="tablist">
                                          	<li role="presentation" class="active"><a href="#post" aria-controls="post" role="tab" data-toggle="tab">Updates</a></li>
											<li role="presentation"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Photos</a></li>
											<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Videos</a></li>
											<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Friends</a></li>
										  </ul>
										
										  <!-- Tab panes -->
										  <div class="tab-content">
											<div role="tabpanel" class="tab-pane" id="home">
                                            	 <?php
												 include_once("widget/my_page_photo_widget.php");
												 ?>
                                            </div>
											<div role="tabpanel" class="tab-pane" id="profile">
                                            	<?php
												include_once("widget/my_page_video_widget.php");
												 ?>
                                            </div>
											<div role="tabpanel" class="tab-pane" id="messages">
                                            	<?php
                                                include_once("widget/my_page_friends_widget.php");
                                                ?>
                                            </div>
                                            <div role="tabpanel" class="tab-pane active" id="post">
                                            	<?php
                                                include_once(DIR_ROOT."widget/my_friend_page_latest_posts.php");
												echo '<div id="friendTimelineFeeds"></div>';
                                                ?>
                                            </div>
										  </div>
									
									</div>
                                </div>
                            	<div class="clr"></div>            
                            </div>
                            </div> 
                               <?php include_once(DIR_ROOT."includes/my_friend_page_right.php");?>      
                               </div>              
                        </div>
                    </div>
        </div>
    </div>
    <?php
	include_once(DIR_ROOT.'js/include/my_page.php');
	?>
<script type="text/javascript" src="<?php echo SITE_ROOT ?>js/fixed_sidebar.js"></script>
  <script type="text/javascript">
  		$(window).load(function(){
			$('.sidebar').fixedSidebar();
			$('.profile_box').fixedSidebar();
		});
		$(document).ready(function(e) {
            $('#change_status').click(function(e) {
                $('#status_option').fadeToggle();
				return false;
            });
			$('.follow').click(function(e) {
				$.get('<?php echo SITE_ROOT?>ajax/sent_friend_request.php',{"friendId":'<?php echo $getMyFriendDetails['user_id']?>'},function(data){
				});
                $(this).addClass('following');
				var icon = $(this).find('i')
				icon.removeClass('fa-plus');
				icon.addClass('fa-check');
				$(this).find('.text').html("Following");
				return false;
            });
			$('.like_btn_photo').click(function(e) {
                 $(this).toggleClass('liked');
				return false;
            });
			$('.friedChatStatus').load('<?php echo SITE_ROOT?>ajax/friend_chat_status.php?fid=<?php echo $getMyFriendDetails['user_id']?>');
			var refreshId = setInterval(function()
			{
			$('.friedChatStatus').load('<?php echo SITE_ROOT?>ajax/friend_chat_status.php?fid=<?php echo $getMyFriendDetails['user_id']?>');
			}, 1000);
			$('body').fameuzLightbox({
				class : 'lightBoxs',
				sidebar: 'default',
				photos:{
					imageLoadUrl: '<?php echo SITE_ROOT.'ajax/popUpImageUser.php'?>',
					data_attr	: ['data-contentId','data-contentType','data-contentAlbum'],
					likeUrl	: '<?php echo SITE_ROOT?>ajax/like.php',
					shareUrl: '<?php echo SITE_ROOT?>ajax/share.php',
					workedUrl: '<?php echo SITE_ROOT?>ajax/worked_together.php',
					commentBoxUrl: '<?php echo SITE_ROOT.'widget/pop_image_comment_box.php' ?>',
					comments :{
						commentActionUrl:'<?php echo SITE_ROOT?>access/post_comment.php?action=add_comment',
						commentDelete: '<?php echo SITE_ROOT?>access/delete_comment.php',
					}
				},
				videos : {
					videoLoadUrl: '<?php echo SITE_ROOT.'ajax/popUpVideoUser.php'?>',
					data_attr	: ['data-vidEncrId'],
					likeUrl		: '<?php echo SITE_ROOT?>ajax/like_video.php',
					shareUrl	: '<?php echo SITE_ROOT?>ajax/share_video.php',
					commentBoxUrl:'<?php echo SITE_ROOT.'widget/pop_video_comment_box.php' ?>',
					comments :{
						commentActionUrl:'<?php echo SITE_ROOT?>access/post_video_comment.php?action=add_comment',
					}
				}
			});
        });
		$('.like_dup').on('click','.like_btn',function(){
			var likeId		=	$(this).data('like');
			var likecat	   =	$(this).data('likecat');
			var imgUserId	 =	'<?php echo $getMyFriendDetails['user_id']?>';
			var that		  =	$(this);
			if(likeId !='' && likecat != ''){
				$.ajax({
					url:"<?php echo SITE_ROOT?>ajax/like.php",
					method: "POST",
					data:{likeId:likeId,likecat:likecat,imgUserId:imgUserId},
					success:function(result){
						if(result == 'liked'){
							$(that).addClass('liked');
							$(that).children('.text').html('Liked');
						}else if(result == 'like'){
							$(that).removeClass('liked');
							$(that).children('.text').html('Like');
						}
				}});
			}
		});
		$(".worked").on("click",".worked_request",function(){
			$(this).addClass('active');
			var likeId		=	$(this).data('like');
			var likecat	   =	$(this).data('likecat');
			var imgUserId	 =	'<?php echo $getMyFriendDetails['user_id']?>';
			var that		  =	this;
			if(likeId !='' && likecat != ''){
				$.ajax({
					url:"<?php echo SITE_ROOT?>ajax/worked_together.php",
					method: "POST",
					data:{img_id:likeId,img_cat:likecat,send_to:imgUserId},
					success:function(result){
						if(result == 'workedTogether'){
							$(that).removeClass('active');
							$(that).html('<i class="fa fa-check"></i> Worked Together');
						}
				}});
			}
		});
	</script>
<script type="text/javascript">
 var preloader		=	'<div class="feedLoader"><div class="feedContainer"><div class="row"><div class="col-sm-1"><div class="img-s"></div></div><div class="col-sm-11"><div class="rectangle-md"><div class="rectangle"></div><div class="rectangle-sm pull-right"></div></div><div class="rectangle-lg"><div class="rectangle-sm"></div><div class="rectangle-sm"></div><div class="rectangle-sm"></div></div><div class="rectangle-lg"><div class="rectangle-full"></div></div></div></div></div><div class="line"></div><div class="rectangle-wd"><div class="rectangle-sm-down"></div><div class="rectangle-sm-down"></div><div class="rectangle-sm-down"></div></div></div>';
function getresult(url) {
	$('#friendTimelineFeeds').append(preloader);
	$.ajax({
		url: url,
		type: "GET",
		data:  {rowcount:$("#rowcount").val()},
		success: function(data){
			 $('#friendTimelineFeeds').append(data);
			 $('.feedLoader').hide();
			 if($("#total-count").val()>0){
			  }else{
				$('#friendTimelineFeeds').html('<div class="dummy_box default_no_image"></div>');
			  }
		},
		error: function(){} 	        
   });
}
 $(window).scroll(function(){
		if ($(window).scrollTop() == $(document).height() - $(window).height()){
			if(parseInt($(".pagenum:last").val()) <= parseInt($(".total-page").val())) {
				var pagenum = parseInt($(".pagenum:last").val()) + 1;
				getresult('<?php echo SITE_ROOT?>widget/my_page_latest_posts.php?friendId=<?php echo $getMyFriendDetails['user_id'];?>&page='+pagenum);
			}
		}
	}); 
</script>