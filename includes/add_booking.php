<link href="<?php echo SITE_ROOT?>css/user-view.css" rel="stylesheet" type="text/css">
<link href="<?php echo SITE_ROOT?>css/jquery_scroll.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery-ias.min.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery-ui.js"></script>
<link href="<?php echo SITE_ROOT?>css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link href="<?php echo SITE_ROOT?>css/cmxform.css" rel="stylesheet" type="text/css" />
<?php
include_once(DIR_ROOT."js/include/fameuz_lightbox_library.php");
if($editId){
	include_once(DIR_ROOT."class/book_model.php");
	$objBookModel					  =	new book_model();
	$getEditbooking					=	$objBookModel->getRow("bm_id='".$editId."' and agent_id=".$_SESSION['userId']);
	$editBookFrom					  =	date("m/d/Y",strtotime($getEditbooking['book_from']));
	$editBookTo					    =	date("m/d/Y",strtotime($getEditbooking['book_to']));
}
include_once(DIR_ROOT."class/user_reviews.php");
$objUserReviews						=	new user_reviews();
$limit 								 =	6;
$page 								  =	(int) (!isset($_GET['p'])) ? 1 : $_GET['p'];
$sql_reviews						   =	"SELECT review.review_id,review.review_msg,review.review_rate,review.review_date,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url from user_reviews AS review LEFT JOIN users AS user ON review.user_id_by=user.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 WHERE review.review_status=1 AND review.review_user_to_status=1 AND review.user_id_to='".$getMyFriendDetails['user_id']."' ORDER BY review.review_date desc ";
$start 								 =	($page * $limit) - $limit;
if(($objUserReviews->countRows($sql_reviews)) > ($page * $limit) ){
	$next = ++$page;
}
$getMyAllReviews						=	$objUserReviews->listQuery($sql_reviews. " LIMIT ".$start.", ".$limit);
?>
    <div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="user_profile">
						<?php echo $objCommon->checkEmailverification();?>
                            <div class="row"><div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9"><div class="content">
                            	<div class="pagination_box">
                                	<a href="<?php echo SITE_ROOT?>user/home">Home <i class="fa fa-caret-right"></i></a>
									<a href="javascript:;"><?php echo $displayNameFriend?> <i class="fa fa-caret-right"></i></a>
           							 <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
									<a href="javascript:;" class="active">Book</a>
								   <?php
                                    include_once(DIR_ROOT."widget/notification_head.php");
                                    ?>
                                </div>
                            	<?php
								include_once(DIR_ROOT."includes/my_friend_page_left.php");
								?>
                            	<div class="profile_content">
									<div class="row">
										<div class="col-md-12">
											<div class="book_form_outer">
												<div class="book_form_inner">
													<h2>Book <?php echo $displayNameFriend?></h2>
													<div class="row">
														<div class="col-md-8">
															<form method="post" action="<?php echo SITE_ROOT.'access/add_booking.php?action=add_booking'?>" enctype="multipart/form-data" id="add-booking-form">
																<div class="row">
																	<div class="col-sm-6">
																		<div class="form-group">
																			<div class="input-group">
																				<input type="text" class="form-control" id="book_from_date" name="book_from_date" placeholder="From" value="<?php echo ($editBookFrom)?$editBookFrom:''?>" >
																				<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																			</div>
																		</div>
																	</div>
																	<div class="col-sm-6">
																		<div class="form-group">
																			<div class="input-group">
																				<input type="text" class="form-control" id="book_to_date" name="book_to_date" placeholder="To" value="<?php echo ($editBookTo)?$editBookTo:''?>" >
																				<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control" id="book_title" name="book_title" placeholder="Title" value="<?php echo($getEditbooking['bm_title'])?$objCommon->html2text($getEditbooking['bm_title']):'';?>" >
																		<span class="input-group-addon"><i class="fa fa-tag"></i></span>
																	</div>
																</div>
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control" id="book_company" name="book_company" placeholder="Name/Company"  value="<?php echo($getEditbooking['bm_company'])?$objCommon->html2text($getEditbooking['bm_company']):'';?>" >
																		<span class="input-group-addon"><i class="fa fa-tag"></i></span>
																	</div>
																</div>
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control" id="book_work" name="book_work" placeholder="Work" value="<?php echo($getEditbooking['bm_work'])?$objCommon->html2text($getEditbooking['bm_work']):'';?>" >
																		<span class="input-group-addon"><i class="fa fa-briefcase"></i></span>
																	</div>
																</div>
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control" id="book_country" name="book_country" placeholder="Country" value="<?php echo($getEditbooking['bm_country'])?$objCommon->html2text($getEditbooking['bm_country']):'';?>" >
																		<span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
																	</div>
																</div>
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control" id="book_budget" name="book_budget" placeholder="Budget  (eg :$ 1,000)" value="<?php echo($getEditbooking['bm_budget'])?$objCommon->html2text($getEditbooking['bm_budget']):'';?>" >
																		<span class="input-group-addon"><i class="fa fa-usd"></i></span>
																	</div>
																</div>
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control" id="book_email" name="book_email" placeholder="Email" value="<?php echo($getEditbooking['bm_email'])?$objCommon->html2text($getEditbooking['bm_email']):'';?>" >
																		<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
																	</div>
																</div>
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control" id="book_phone" name="book_phone" placeholder="Phone" value="<?php echo($getEditbooking['bm_phone'])?$objCommon->html2text($getEditbooking['bm_phone']):'';?>" >
																		<span class="input-group-addon"><i class="fa fa-phone"></i></span>
																	</div>
																</div>
																<div class="form-group">
																	<div class="input-group">
																		<!--<input type="text" class="form-control" id="book_attachement" name="book_attachement" placeholder="Attachement">-->
																		<div class="attach_book"><?php echo($getEditbooking['bm_attachement'])?'<i class="fa fa-file-o"></i> '.$objCommon->html2text($getEditbooking['bm_attachement']):'Attachement';?></div>
																		<input type="file" name="book_attach" class="book_attach_file" />
																		<span class="input-group-addon"><i class="fa fa-paperclip"></i></span>
																	</div>
																	
																</div>
																<strong>Company Allowance</strong>
																<div class="checkbox">
																	<label>
																		<input type="checkbox" name="book_allowance" value="1" <?php echo ($getEditbooking['bm_allowance']==1)?'checked="checked"':''?>> Air Ticket
																	</label>
																</div>
																<div class="form-group">
																	<textarea class="form-control" rows="3" placeholder="Brief Details" name="book_brief_details"><?php echo($getEditbooking['bm_descr'])?$objCommon->html2textarea($getEditbooking['bm_descr']):'';?></textarea>
																</div>
																<input type="hidden" name="hid_user_to" value="<?php echo $getMyFriendDetails['user_id']?>" />
																<?php if($getEditbooking['bm_id']){
																echo '<input type="hidden" name="hid_edit_bookId" value="'.$getEditbooking['bm_id'].'" />';
																}
																?>
																<button type="submit" class="btn btn-default pull-right">Book Now</button>
															</form>
														</div>
														<div class="col-md-2">
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
                                    <div class="review_section wrap_review">
                                        <div class="review_heading">
                                        	Reviews
                                        	<span class="review_arrow"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/photo_arrow.png" alt="photo_arrow" /></span>
                                        </div>
<?php
if(count($getMyAllReviews)>0){
foreach($getMyAllReviews as $allMyReviews){
$getrate			=	(int) $allMyReviews['review_rate'];
$getRateStyle	   =	'style="width:'.($getrate*20).'%;"';
?>
<div class="review_box item_review" id="item_review-<?php echo $allMyReviews['review_id']?>">
<div class="thumb"><img alt="review_thumb" src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($allMyReviews['upi_img_url'])?$allMyReviews['upi_img_url']:'profile_pic.jpg'?>"></div>
<div class="text">
<p class="review"><?php echo $objCommon->html2textarea($allMyReviews['review_msg']);?></p>
</div>
<div class="clr"></div>
	<div class="row rew_post">
	<div class="col-sm-7">
	<div class="post_user"><div class="posted">Posted by <span><?php echo ($allMyReviews['display_name'])?$objCommon->html2text($allMyReviews['display_name']):$objCommon->html2text($allMyReviews['first_name']);?>  </span> </div>  </div>
	</div>
	<div class="col-sm-5 text-right">
	<div class="rating_box"><div <?php echo $getRateStyle?> class="rating_yellow"></div></div>
	<div class="time"><i class="fa fa-clock-o"></i> <?php echo date("d M , Y",strtotime($objCommon->local_time($allMyReviews['review_date'])));?></div>
	</div>
	</div>
</div>
<?php
} 
if (isset($next)){ ?>
<div class="nav">
<a href="<?php echo SITE_ROOT.'user/'.$user_url.'/add-book/'.$next?>">Next</a>
</div>
<?php
}

}else{
echo '<p>No reviews found....</p>';
}
?>
                                    </div>
                                </div>
                            	<div class="clr"></div>            
                            </div></div> 
                               <?php include_once(DIR_ROOT."includes/my_friend_page_right.php");?>    </div>                
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
include_once(DIR_ROOT."js/include/add_booking.php");
?>