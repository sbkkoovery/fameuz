<?php
include_once(DIR_ROOT."class/worked_together.php");
$objWorkedTogether		 =	new worked_together();
$limit 					 =	10;
$page 					  =	(int) (!isset($_GET['p'])) ? 1 : $_GET['p'];
$sqlProfileVisitors		= "SELECT social.usl_fameuz,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,following.follow_user1,following.follow_user2,following.follow_status,case when (TIMESTAMPDIFF(MINUTE,chat.ucs_last_seen,'".date("Y-m-d H:i:s")."') >15 or chat.ucs_status=0) then '0' else '1' END AS chatStatus,personal.p_country,personal.p_city
FROM worked_together AS worked 
LEFT JOIN users AS user ON CASE WHEN worked.send_to=".$getMyFriendDetails['user_id']." THEN worked.send_from = user.user_id ELSE worked.send_to = user.user_id END
LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
LEFT JOIN user_chat_status AS chat ON   CASE WHEN worked.send_to=".$getMyFriendDetails['user_id']." THEN  worked.send_from = chat.user_id  ELSE worked.send_to = chat.user_id  END
LEFT JOIN personal_details AS personal ON user.user_id=personal.user_id 
LEFT JOIN following ON (worked.send_from = following.follow_user1 OR worked.send_from = following.follow_user2) AND (following.follow_user1='".$_SESSION['userId']."' OR following.follow_user2='".$_SESSION['userId']."') 
WHERE worked.from_status=1 AND worked.to_status=1 AND (worked.send_to ='".$getMyFriendDetails['user_id']."' OR worked.send_from='".$getMyFriendDetails['user_id']."') AND user.user_id !='".$getMyFriendDetails['user_id']."' AND user.email_validation=1 AND user.status=1 
GROUP BY user.user_id 
ORDER BY worked.worked_date DESC"; 

$start 					 =	($page * $limit) - $limit;
if(($objWorkedTogether->countRows($sqlProfileVisitors)) > ($page * $limit) ){
	$next = ++$page;
}
$getProfileVisitors		=	$objWorkedTogether->listQuery($sqlProfileVisitors. " LIMIT ".$start.", ".$limit);
?>
<link href="<?php echo SITE_ROOT?>css/jquery_scroll.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery-ias.min.js"></script>
<div class="inner_content_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="inner_top_border">
					<div class="user_profile">
					<?php echo $objCommon->checkEmailverification();?>
						<div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                        <div class="content">
                        <div class="pagination_box">
								<a href="<?php echo SITE_ROOT?>user/home">Home <i class="fa fa-caret-right"></i></a>
                                <a href="<?php echo SITE_ROOT.$getMyFriendDetails['usl_fameuz']?>"> <?php echo $displayNameprofile;?> <i class="fa fa-caret-right"></i>  </a>
          					    <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                <a href="javascript:;" class="active">Worked with</a>
							<?php
                            include_once(DIR_ROOT."widget/notification_head.php");
                            ?>
							</div>
                            <div class="tab_white_search">
                                        <div class="section_search">
                                            <ul>
                                                <li class="visitor_head_box"><h5><?php echo $displayNameprofile;?> worked with</h5><span class="point-it"></span></li>
                                            </ul>
                                            <span>
                                                <!--<input type="text" placeholder="Start typing friend's name" id="invite_search" name="invite_search">-->
                                            </span>
                                            <span class="point-it"></span>
                                        </div>
                                    </div>
                            
							<!--<div class="row">
								<div class="col-md-12">
									<div class="visitor_head_box"><h5><?php echo $displayNameprofile;?> worked with</h5></div>
								</div>
							</div>-->
							<div class="visitor_list_box wrap_review">
								<ul class="row">
									<?php
									if(count($getProfileVisitors)>0){
										foreach($getProfileVisitors as $allProfileVisitors){
											$allProfileVisitorsImg	=	SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.(($allProfileVisitors['upi_img_url'])?$allProfileVisitors['upi_img_url']:"profile_pic.jpg");
											if($allProfileVisitors['display_name']){
												$allProfileVisitorsName	 =	$objCommon->html2text($allProfileVisitors['display_name']);
											}else if($getUserDetails['first_name']){
												$allProfileVisitorsName	 =	$objCommon->html2text($allProfileVisitors['first_name']);
											}else{
												$exploEmailVisi	  		 =	explode("@",$objCommon->html2text($allProfileVisitors['email']));
												$allProfileVisitorsName	 =	$exploEmailVisi[0];
											}
											if($allProfileVisitors['follow_status']==2){ 
												$friendStatus =2; 
											}else if($allProfileVisitors['follow_status']==1 && $allProfileVisitors['follow_user1']==$_SESSION['userId']){ 
												$friendStatus =1; 
											}else { 
												$friendStatus=0; 
											}
											if($allProfileVisitors['p_city'] != '' || $allProfileVisitors['p_country'] != ''){
												$friendPcity	=	($allProfileVisitors['p_city'])?$objCommon->html2text($allProfileVisitors['p_city'])." ,":"";
											}
									?>
									<li class="col-md-6 item_review" id="item_review-<?php echo $allProfileVisitors['visitor_id']?>">
										<div class="visitor_inner_box">
											<div class="image_thumb pull-left">
												 <a href="<?php echo SITE_ROOT.$objCommon->html2text($allProfileVisitors['usl_fameuz'])?>" title="<?php echo $allProfileVisitorsName?>"><img src="<?php echo $allProfileVisitorsImg?>" class="img-responsive" alt="<?php echo $allProfileVisitorsName?>" title="<?php echo $allProfileVisitorsName?>" /></a>
											</div>
											<div class="visitor_box">
												<p class="name_head"><a href="<?php echo SITE_ROOT.$objCommon->html2text($allProfileVisitors['usl_fameuz'])?>" title="<?php echo $allProfileVisitorsName?>"><?php echo $allProfileVisitorsName?></a></p>
												<p><i class="fa fa-globe"></i> <?php echo $friendPcity.' '.$objCommon->html2text($allProfileVisitors['p_country'])?></p>
												<?php
												if($allProfileVisitors['chatStatus']==1){
													echo '<p class="online_status">online</p>';
												}else{
													echo '<p class="offline_status">offline</p>';
												}
												?>
												<?php
												if($allProfileVisitors['user_id']!= $_SESSION['userId']){
												?>
												<div class="follow_status_box">
													<div class="follow_status">
														<?php
														if($friendStatus==2){
														?>
														<a href="javascript:;" class="friend"><i class="fa fa-users"></i> Friend</a>
														<?php
														}else if($friendStatus==1){
														?>
														<a href="javascript:;" class="following"><i class="fa fa-check"></i> Following</a>
														<?php
														}else if($friendStatus==0){
														?>
														<a href="javascript:;" class="follow has-spinner" data-friendid="<?php echo $objCommon->html2text($allProfileVisitors['user_id'])?>">
															<span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span>
															<i class="fa fa-plus"></i> Follow
														</a>
														<?php
														}
														?>
													</div>
												</div>
												<?php
												}
												?>
											</div>
                                            <div class="side_mark"></div>
                                             <div class="clearfix"></div>
										</div>
									</li>
									<?php
										}
										
										if (isset($next)){ ?>
											<div class="nav">
											<a href="<?php echo SITE_ROOT.'user/'.$getMyFriendDetails['usl_fameuz'].'/worked-with/'.$next?>">Next</a>
											</div>
											<?php
										}
									}
									else{
										echo '<li class="col-md-12">No results found...</li>';
									}
									?>
								</ul>
							</div>
						</div></div>
						<?php
						include_once(DIR_ROOT."widget/right_static_ad_bar.php");
						?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	 jQuery.ias({
		container : '.wrap_review', // main container where data goes to append
		item: '.item_review', // single items
		pagination: '.nav', // page navigation
		next: '.nav a', // next page selector
		loader: '<img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/ajax-loader.gif"/>', // loading gif
		triggerPageThreshold: 6 // show load more if scroll more than this
	});
	$('.follow').click(function(e) {
			var friendid		=	$(this).data('friendid');
			$(this).addClass('active');
			$(this).find('.fa-plus').hide();
			that	=	this;
			$.get('<?php echo SITE_ROOT?>ajax/sent_friend_request.php',{"friendId":friendid},function(data){
			  setTimeout(function () {
       				$(that).removeClass('active');
					$(that).parent().html('<a href="javascript:;" class="following"><i class="fa fa-check"></i> Following</a>');
					
    			},3000);
			});
		});

});
</script>