<div class="row">
  <div class="col-sm-12">
  	<div class="msgAlert" style="display:none;"></div>
	<div class="upload-file center-block custom-upload">
	<form class="dropzone" id="video_form" action='<?php echo SITE_ROOT?>access/music_upload.php?path=<?php echo DIR_ROOT?>uploads/music/' method="post" enctype="multipart/form-data" >
	  <div class="drag-upload center-block text-center"><img class="left-shadow" src="<?php echo SITE_ROOT?>images/shadow-left.png" /> <img class="right-shadow" src="<?php echo SITE_ROOT?>images/shadow-right.png" /> <img src="<?php echo SITE_ROOT?>images/cloud.png" class="cloud center-block" />
		<div class="upload-title">
		  <p><strong>Select file to upload</strong></p>
			<input type="file" name="file" id="file_browse" />
		</div>
		<!--<div class="or">
		  <p><strong>Or</strong></p>
		</div>-->
	  </div>
	  <div class="input-field ">
		<div class="form-inline">
		  <div class="form-group youlink">
			<!--<div class="input-group"> <span class="input-group-addon" id="basic-addon1"><i class="fa fa-globe"></i></span>
			  <input type="text" class="form-control" id="youtube_link" placeholder="Paste a music link (URL)" name="youtube_link">
			</div>-->
		  </div>
		  <button class="btn btn-default upoad-btn"  type="submit">Continue</button>
		  </div>
		</form>
	  </div>
	</div>
  </div>
</div>