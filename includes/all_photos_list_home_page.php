<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
$objCommon				   =	new common();
include_once(DIR_ROOT."class/album.php");
include_once(DIR_ROOT."class/user_photos.php");
include_once(DIR_ROOT."class/user_profile_image.php");
include_once(DIR_ROOT."class/videos.php");
$objAlbum					=	new album();
$objUserPhotos			   =	new user_photos();
$objUserProfileImg		   =	new user_profile_image();
$objVideos		   		   =	new videos();
$userId					  =	$_SESSION['userId'];
$getAlbumDetails			 =	$objAlbum->listQuery("select tab.* from (select album.a_name,img.ai_images,album.a_id,img.ai_created,album.a_created from album inner join album_images as img on album.a_id=img.a_id where album.user_id=".$userId."  order by img.ai_created desc ) as tab group by tab.a_id order by tab.a_created desc ");
$getOnceUserPhoto			=	$objUserPhotos->listQuery("select photo_id,photo_url from user_photos where user_id=".$userId." order by photo_created desc limit 0,2");
$getProfileImg			   =	$objUserProfileImg->listQuery("(SELECT upi_img_url ,upi_status from user_profile_image where upi_status=1 and user_id=".$userId.")
union all
(SELECT upi_img_url AS imgUrls,upi_status from user_profile_image where user_id=".$userId ." and upi_status !=1 order by upi_created desc limit 1) ");
?>

<div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class=" col-sm-12 col-md-12 col-lg-6" >
        <div class="album-head"> <img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/gallery.png" /><span class="head-album">My Profile images</span> </div>
        <div class="image-gallery">
          <div class="row">
            <div class="col-sm-6">
              <div class="album_maso_album_maso_box masonry-brick" > <a href="javascript:;" onclick="showProfileImg()">
                <?php
					if($getProfileImg[0]['upi_img_url']){
						echo '<img src="'.SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.$getProfileImg[0]['upi_img_url'].'"   />';
						
					}else{
						echo '<div class="no_album_img"></div>';
					}
					?>
                </a> </div>
            </div>
            <div class="col-sm-6">
              <div class="album_maso_album_maso_box masonry-brick" > <a href="javascript:;" onclick="showProfileImg()">
                <?php
					if($getProfileImg[1]['upi_img_url']){
						echo '<img src="'.SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.$getProfileImg[1]['upi_img_url'].'"   />';
						
					}else{
						echo '<div class="no_album_img"></div>';
					}
					?>
                </a> </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-12 col-md-12 col-lg-6">
        <div class="album-head"> <img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/gallery.png" /><span class="head-album">My Wall Images</span> </div>
        <div class="image-gallery">
          <div class="row">
            <div class="col-sm-6">
              <div class="album_maso_album_maso_box masonry-brick" > <a href="javascript:;" onclick="showWallImg()">
                <?php
					if($getOnceUserPhoto[0]['photo_id'] !='' && $getOnceUserPhoto[0]['photo_url']!=''){
						echo '<img src="'.SITE_ROOT_AWS_IMAGES.'uploads/albums/'.$objCommon->getThumb($objCommon->html2text($getOnceUserPhoto[0]['photo_url'])).'"   />';
						
					}else{
						echo '<div class="no_album_img"></div>';
					}
					?>
                </a> </div>
            </div>
            <div class="col-sm-6">
              <div class="album_maso_album_maso_box masonry-brick" > <a href="javascript:;" onclick="showWallImg()">
                <?php
					if($getOnceUserPhoto[1]['photo_id'] !='' && $getOnceUserPhoto[1]['photo_url']!=''){
						echo '<img src="'.SITE_ROOT_AWS_IMAGES.'uploads/albums/'.$objCommon->getThumb($objCommon->html2text($getOnceUserPhoto[1]['photo_url'])).'"   />';
						
					}else{
						echo '<div class="no_album_img"></div>';
					}
					?>
                </a> </div>
            </div>
          </div>
        </div>
      </div>
      
      <?php
			if(count($getAlbumDetails)>0){
				foreach($getAlbumDetails as $allAlbumDetails){
					$albImg			=	($allAlbumDetails['ai_images'])?'<img src="'.SITE_ROOT_AWS_IMAGES.'uploads/albums/'.$objCommon->getThumb($objCommon->html2text($allAlbumDetails['ai_images'])).'"   />':''
					
			?>
      <div class="col-sm-12 col-md-12 col-lg-6">
        <div class="album-head"> <img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/gallery.png" /><span class="head-album"><?php echo $objCommon->html2text($allAlbumDetails['a_name'])?></span> </div>
        <div class="image-gallery">
          <div class="row">
            <div class="col-sm-6">
              <div class="add_album_img"><a href="javascript:;" onclick="showAlbum('<?php echo $allAlbumDetails['a_id']?>')" class="pull-left"></a></div>
            </div>
            <div class="col-sm-6">
              <div class="album_maso_album_maso_box masonry-brick" > <a href="javascript:;" onclick="showAlbum('<?php echo $allAlbumDetails['a_id']?>')">
                <?php 
				if($allAlbumDetails['ai_images']){
					echo $albImg;
				}else{
					echo '<div class="no_album_img"></div>';
				}
				?>
                </a> </div>
            </div>
          </div>
        </div>
      </div>
      <?php
				}
			}
			?>
    </div>
  </div>
</div>
</div>
<?php /*?><script type="text/javascript">
		$('.album_maso').masonry();
</script><?php */?>
