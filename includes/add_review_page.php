<?php
$reviewEdit						=	$objCommon->esc($_GET['reviewEdit']);
if($reviewEdit){
	$getEditReviews				=	$objUserReviews->getRow("user_id_by='".$_SESSION['userId']."' AND 	review_status=1 AND review_user_to_status = 1 AND review_id='".$reviewEdit."'");
}
?>
<link href="<?php echo SITE_ROOT?>css/user-view.css" rel="stylesheet" type="text/css">
<link href="<?php echo SITE_ROOT?>css/rating.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/livevalidation.js"></script>
<link href="<?php echo SITE_ROOT?>css/livevalidation.css" rel="stylesheet" type="text/css" />
    <div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="user_profile">
						<?php echo $objCommon->checkEmailverification();?>
                            <div class="row"><div class="colxs-12 col-sm-8 col-md-8 col-lg-sp-9"><div class="content">
                            	<div class="pagination_box">
                                	<a href="<?php echo SITE_ROOT?>user/home">Home <i class="fa fa-caret-right"></i></a>
                                	<a href="#"><?php echo $displayNameFriend?><i class="fa fa-caret-right"></i></a>
                                	<a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                    <a href="#" class="active">Add Review</a>
									<?php
									include_once(DIR_ROOT."widget/notification_head.php");
								?>
                                </div>
                                 
                            	<?php
								include_once(DIR_ROOT."includes/my_friend_page_left.php");
								?>
                            	<div class="profile_content">
                                	<div class="tab_white_search">
                                        <div class="section_search">
                                            <ul>
                                                <li class="visitor_head_box"><h5>Review for <?php echo $displayNameFriend?></h5><span class="point-it"></span></li>
                                            </ul>
                                        </div>
                                    </div>
									<div class="add_review_content">
										<div class="row">
											<div class="col-md-12">
                                            <div class="wrapper">
											<?php echo $objCommon->displayMsg();?>
											<?php
											if($getUserDetails['email_validation']==1){
											?>
												<form class="form-horizontal" id="rate_form" action="<?php echo SITE_ROOT?>access/add_review.php?action=add" method="post">
													<div class="form-group">
														<label class="col-sm-3 control-label">My Rating</label>
														<div class="col-sm-9">
															<div class="rate-ex2-cnt">
																<div id="1" class="rate-btn-1 rate-btn <?php echo ($getEditReviews['review_rate']>=1)?'rate-btn-hover':''?>"></div>
																<div id="2" class="rate-btn-2 rate-btn <?php echo ($getEditReviews['review_rate']>=2)?'rate-btn-hover':''?>"></div>
																<div id="3" class="rate-btn-3 rate-btn <?php echo ($getEditReviews['review_rate']>=3)?'rate-btn-hover':''?>"></div>
																<div id="4" class="rate-btn-4 rate-btn <?php echo ($getEditReviews['review_rate']>=4)?'rate-btn-hover':''?>"></div>
																<div id="5" class="rate-btn-5 rate-btn <?php echo ($getEditReviews['review_rate']==5)?'rate-btn-hover':''?>"></div>
																<input type="text" value="<?php echo $getEditReviews['review_rate']?>" name="rate_val" id="rate_val" style="display:none;" />
															<script>var rate_val = new LiveValidation('rate_val');rate_val.add( Validate.Presence );</script>
															</div>
															
														</div>
													</div>
													<div class="form-group">
														<label  class="col-sm-3 control-label">My Review</label>
														<div class="col-sm-9">
															<textarea class="form-control" rows="3" name="review_descr" id="review_descr"><?php echo $objCommon->html2textarea($getEditReviews['review_msg']);?></textarea>
															<script>var review_descr = new LiveValidation('review_descr');review_descr.add( Validate.Presence );</script>
														</div>
													</div>
													<?php
													if($getEditReviews['review_id'] ==''){
													?>
													<div class="form-group">
														<label class="col-sm-3 control-label">My Relationship to  <?php echo $displayNameFriend?></label>
														<div class="col-sm-9">
															<ul>
																<li><label><input type="radio" name="review_relation"  value="1" class="review_relation" <?php echo ($getEditReviews['review_relation']==1)?'checked="checked"':'';?>/> i have worked with <?php echo $displayNameFriend?></label></li>
																<li><label><input type="radio" name="review_relation" value="2" class="review_relation" <?php echo ($getEditReviews['review_relation']==2)?'checked="checked"':'';?>/> i am following/follower of  <?php echo $displayNameFriend?></label></li>
																<li>
																	<label><input type="radio" name="review_relation" value="3" class="review_relation"/> other</label>
																	<input type="text" name="review_relation_other" class="review_relation_other" placeholder="Write your relationship.." />
																</li>
															</ul>
														</div>
													</div>
													<?php
													}
													?>
													<div class="form-group">
														<div class="col-sm-offset-3 col-sm-9">
															<input type="hidden" value="<?php echo $getMyFriendDetails['user_id']?>"  name="friend_id" />
															<input type="hidden" value="<?php echo $getEditReviews['review_id']?>"  name="reviewEdit" />
															<button type="submit" class="btn btn-default">Rate</button>
														</div>
													</div>
												</form>
												<?php
												}else{
													echo '<div role="alert" class="alert alert-warning"><strong>Warning ! </strong> You do not have sufficient permissions to Post Review.</div>';
												}
												?>
											</div>
											</div>
										</div>
									</div>
                                    <div class="tab_white_search">
                                        <div class="section_search">
                                            <ul>
                                                <li class="visitor_head_box"><h5><?php echo $displayNameFriend?>'s Recent Reviews</h5><span class="point-it"></span></li>
                                            </ul>
                                        </div>
                                    </div>
									<div class="add_review_list">
										<div class="row">
											<div class="col-md-12">
                                            	<?php
												if(count($getMyReviews)>0){
													foreach($getMyReviews as $allMyReviews){
														$getrate			=	(int) $allMyReviews['review_rate'];
														$getRateStyle	   =	'style="width:'.($getrate*20).'%;"';
													?>
											
												<div class="review_box">
													<div class="thumb"><img alt="review_thumb" src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($allMyReviews['upi_img_url'])?$allMyReviews['upi_img_url']:'profile_pic.jpg'?>"></div>
													<div class="text">
														<p class="review"><?php echo $objCommon->html2textarea($allMyReviews['review_msg']);?></p>
													</div>
													<div class="clr"></div>
													<div class="rating_box"><div <?php echo $getRateStyle?> class="rating_yellow"></div></div>
													<div class="time"><div class="posted">Posted by <span><?php echo ($allMyReviews['display_name'])?$objCommon->html2text($allMyReviews['display_name']):$objCommon->html2text($allMyReviews['first_name']);?>  </span> </div>  <i class="fa fa-clock-o"></i> <?php echo date("d M , Y",strtotime($objCommon->local_time($allMyReviews['review_date'])));?></div>
												</div>
												<?php
													}
													?>
													<p class="margin_btm0 text-right"><a href="<?php echo SITE_ROOT.'user/'.$user_url.'/reviews'?>" class="seemore">See More Reviews <i class="fa fa-chevron-right"></i></a></p>
													<?php
												}else{
													echo '<p>No reviews found....</p>';
												}
												?>
												
											</div>
										</div>
									</div>
                                </div>
                            	<div class="clr"></div>            
                            </div></div> 
                               <?php include_once(DIR_ROOT."includes/my_friend_page_right.php");?>  </div>                  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <script type="text/javascript">
		$(document).ready(function(e) {
			$('.follow').click(function(e) {
				$.get('<?php echo SITE_ROOT?>ajax/sent_friend_request.php',{"friendId":'<?php echo $getMyFriendDetails['user_id']?>'},function(data){

				});
                $(this).addClass('following');
				var icon = $(this).find('i')
				icon.removeClass('fa-plus');
				icon.addClass('fa-check');
				$(this).find('.text').html("Following");
				return false;
            });
			$('.friedChatStatus').load('<?php echo SITE_ROOT?>ajax/friend_chat_status.php?fid=<?php echo $getMyFriendDetails['user_id']?>');
			var refreshId = setInterval(function()
			{
			$('.friedChatStatus').load('<?php echo SITE_ROOT?>ajax/friend_chat_status.php?fid=<?php echo $getMyFriendDetails['user_id']?>');
			}, 1000);
			 $('.rate-btn').hover(function(){
                $('.rate-btn').removeClass('rate-btn-hover');
                var therate = $(this).attr('id');
                for (var i = therate; i >= 0; i--) {
                    $('.rate-btn-'+i).addClass('rate-btn-hover');
                };
            });
			$(".review_relation").click(function(){
				var relVal	=	$(this).val();
				if(relVal==3){
					$(".review_relation_other").show();
				}else{
					$(".review_relation_other").hide();
				}
			});
			 $('.rate-btn').click(function(){
                var therate = $(this).attr('id');
				$("#rate_val").val(therate);
			 });
        });
	</script>