
<?php
include_once(DIR_ROOT."class/user_chat_status.php");
$objUserChatStatus		   =	new user_chat_status();
$getUserChatStatus	   =	$objUserChatStatus->getRow("user_id=".$_SESSION['userId']);
?>
<div class="profile_box">
	<div class="profile_pic"><a href="javascript:;" data-contentId="<?php echo $getUserDetails['upi_id']?>" data-contentType="6" data-contentAlbum="0" class="<?php echo($getUserDetails['upi_img_url'])?'lightBoxs':''?>"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($getUserDetails['upi_img_url'])?$getUserDetails['upi_img_url']:'profile_pic.jpg'?>" alt="profile_pic" /></a><a href="<?php echo SITE_ROOT.'user/edit-profile?active=profile_photo'?>"><span class="edit_my_profile"><i class="fa fa-camera"></i></span></a></div>
    <div class="profile_tabs">
		<h1><?php echo $displayName?></h1>
		<p class="proffesion username"> <?php echo $objCommon->html2text($getUserDetails['c_name'])?></p>
        <div class="status_box">
			<?php
			$statusOnlineDis		 =	'';
			$statusBsyDis			=	'';
			$statusInvDis			=	'';
			if($getUserChatStatus['ucs_online_status']==1){
				$statusClass		 =	'status_online';
				$statusText		  =	'Online';
				$statusOnlineDis	 =	'status_hide';
			}else if($getUserChatStatus['ucs_online_status']==2){
				$statusClass		 =	'status_busy';
				$statusText		  =	'Busy';
				$statusBsyDis	 	=	'status_hide';
			}else if($getUserChatStatus['ucs_online_status']==0){
				$statusClass		 =	'status_invisible';
				$statusText		  =	'Invisible';
				$statusInvDis	 	=	'status_hide';
			}
			?>
			<a href="#" class="<?php echo $statusClass?> current_status" id="change_status"><?php echo $statusText?><i class="fa fa-sort-desc"></i></a>
			<div class="status_option" id="status_option">
				<div class="status_option_arrow"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/status_option_arrow.png" alt="status_option_arrow" /></div>
				<a href="#"  class="status_online check_Status <?php echo $statusOnlineDis?>" data-value="1">Online</a> 
				<a href="#"  class="status_busy check_Status <?php echo $statusBsyDis?>" data-value="2">Busy</a>     
				<a href="#" class="status_invisible check_Status <?php echo $statusInvDis?>" data-value="0">Invisible</a>
				<a href="<?php echo SITE_ROOT?>user/logout"  class="status_invisible">Sign out </a>
			</div>
		</div>
	</div>
	<div class="profile_tabs">
		<ul>
			<li>
				<a href="<?php echo SITE_ROOT?>user/edit-profile">
					<i class="fa fa-edit icons"></i>
					Edit My Profile
					<i class="fa fa-play arrow_list"></i>
				</a>
			</li>
			<li>
				<a href="<?php echo SITE_ROOT.'user/my-profile'?>">
					<i class="fa fa-search icons"></i>
					View My Profile
					<i class="fa fa-play arrow_list"></i>
				</a>
			</li>
			<li>
				<a href="<?php echo SITE_ROOT.'user/my-reviews'?>">
					<i class="fa fa-comment icons"></i>
					Reviews<!--<sup>New</sup>-->
					<i class="fa fa-play arrow_list"></i>
				</a>
			</li>
			<?php /*?><li>
				<a href="<?php echo SITE_ROOT.'user/my-blogs'?>">
					<i class="fa fa-dribbble icons"></i>
					My Blogs
					<i class="fa fa-play arrow_list"></i>
				</a>
			</li><?php */?>
			<li>
				<a href="<?php echo SITE_ROOT.'user/my-messages'?>">
					<i class="fa fa-envelope icons"></i>
					My Messages<!--<sup>30</sup>-->
					<i class="fa fa-play arrow_list"></i>
				</a>
			</li>
			<?php /*?><li>
				<a href="<?php echo SITE_ROOT?>user/my-market-place">
					<i class="fa fa-cart-arrow-down icons"></i>
					My Marketplace
					<i class="fa fa-play arrow_list"></i>
				</a>
			</li><?php */?>
			<li>
				<a href="<?php echo SITE_ROOT?>user/my-jobs">
					<i class="fa fa-briefcase icons"></i>
					My Jobs
					<i class="fa fa-play arrow_list"></i>
				</a>
			</li>
			<li>
				<a href="<?php echo SITE_ROOT.'user/user-calendar'?>">
					<i class="fa fa-calendar icons"></i>
					 My Calendar
					<i class="fa fa-play arrow_list"></i>
				</a>
			</li>
			<li>
				<a href="<?php echo SITE_ROOT.'user/my-promotions'?>">
					<i class="fa fa-bullhorn icons"></i>
					 My Promotions
					<i class="fa fa-play arrow_list"></i>
				</a>
			</li>
			<?php
			if($getUserDetails['c_book_me']!=1){
			?>
			<li>
				<a href="<?php echo SITE_ROOT.'user/my-contact-messages'?>">
					<i class="fa fa-envelope icons"></i>
					My Contact Message<!--<sup>30</sup>-->
					<i class="fa fa-play arrow_list"></i>
				</a>
			</li>
			<?php
			}
			?>
		</ul>
	</div>
	<div class="profile_tabs">
		<h2>My Bookings</h2>
		<ul>
			<?php
			if($book_me==1){
			?>
			<li><a href="<?php echo SITE_ROOT.'user/my-booking'?>"><i class="fa fa-thumb-tack icons"></i>Received Bookings<!--<sup>New</sup>--><i class="fa fa-play arrow_list"></i></a></li>
			<?php
			}
			if($book_by_me==1){
			?>
			<li><a href="<?php echo SITE_ROOT.'user/booking-requests'?>"><i class="fa fa-book icons"></i>Sent Booking Requests<i class="fa fa-play arrow_list"></i></a></li>
			<?php
			}
			?>
		</ul>
	</div>
	<div class="profile_tabs">
		<a href="<?php echo SITE_ROOT.'user/promote/profile'?>" class="promote">Promote Your Profile</a>
	</div>
	<div class="profile_tabs">
		<h2>My Gallery</h2>
		<ul>
			<li>
				<a href="<?php echo SITE_ROOT.'user/my-album'?>">
					<i class="fa fa-youtube-play icons"></i>
					 Albums
					<i class="fa fa-play arrow_list"></i>
				</a>
			</li>
			<?php
			if($getUserDetails['uc_m_id']==2){
			?>
			<li>
				<a href="<?php echo SITE_ROOT.'user/my-polaroids'?>">
					<i class="fa fa-camera icons"></i>
					 Polaroids
					<i class="fa fa-play arrow_list"></i>
				</a>
			</li>
			<li>
				<a href="<?php echo SITE_ROOT.'user/'.$objCommon->html2text($getUserDetails['usl_fameuz']).'/composite-card'?>">
					<i class="fa fa-th-large icons"></i>
					 Composite Card
					 <i class="fa fa-play arrow_list"></i>
				</a>
			</li>
			<?php
			}
			//if($getUserDetails['vi_url'] !=''){
			?>
			<li>
				<a href="javascript:;" data-toggle="modal" data-target="#videoIntroduction" class="callDisplayIntoduction">
					<i class="fa fa-file-video-o icons"></i>
					 Video Introduction
					 <i class="fa fa-play arrow_list"></i>
				</a>
			</li>
			<?php
			//}
			?>
			
		</ul>
	</div>
	<div class="profile_tabs list_bold">
		<ul>
			<?php /*?><li>
				<a href="<?php echo SITE_ROOT.'games/search?category=my-games'?>">
					<i class="fa fa-gamepad icons"></i>
					My Games
					<i class="fa fa-play arrow_list"></i>
				</a>
			</li><?php */?>
			<li>
				<a href="<?php echo SITE_ROOT.'user/my-music'?>">
					<i class="fa fa-music icons"></i>
					 My Music
					<i class="fa fa-play arrow_list"></i>
				</a>
			</li>
			<li>
				<a href="<?php echo SITE_ROOT.'user/my-album'?>">
					<i class="fa fa-video-camera icons"></i>
					 My Videos
					<i class="fa fa-play arrow_list"></i>
				</a>
			</li>
		</ul>
	</div>
	<div class="profile_tabs ">
		<h2>My Area</h2>
		<ul>
			<li>
				<a href="<?php echo SITE_ROOT?>user/invite-friends">
					<i class="fa fa-users icons"></i>
					Invite People
					<i class="fa fa-play arrow_list"></i>
				</a>
			</li>
			<li>
				<a href="<?php echo SITE_ROOT?>user/following">
					<i class="fa fa-cog icons"></i>
					 Manage Friends
					<i class="fa fa-play arrow_list"></i>
				</a>
			</li>
		</ul>
	</div>
</div>
<?php
include_once(DIR_ROOT."widget/ipad_menu.php");
?>
<div class="modal fade" id="videoIntroduction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog videoIntroModal" role="document">
		<div class="modal-content">
			<div class="modal-header vidIntroHead">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><?php echo $displayName?></h4>
			</div>
			<div class="modalDisplayIntoduction"></div>
		</div>
	</div>
</div>
<?php /*?><div class="modal fade" id="videoIntroduction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog videoIntroModal" role="document">
		<div class="modal-content">
			<div class="modal-header vidIntroHead">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><?php echo $displayName?></h4>
			</div>
			<div class="modalDisplayIntoduction"></div>
		</div>
	</div>
</div><?php */?>
<script>
$(".callDisplayIntoduction").on("click",function(){
	$(".modalDisplayIntoduction").load('<?php echo SITE_ROOT?>widget/display_video_introduction.php');
});
</script>