<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/pagination-class-front.php");
$book_status				 =	$objCommon->esc($_GET['book_status']);
$num_results_per_page		= 	20;
$num_page_links_per_page 	 = 	5;
$pg_param 					= 	"";
$pagesection				 = 	'';
$show						=	$objCommon->esc($_GET['show']);
$actAllStatus=$actUpcoming=$actPending=$actComplted=$actExpired	=	'';
$getMyJobsSql				=	"SELECT b.*,bc.bc_title FROM blogs AS b LEFT JOIN blog_category AS bc ON b.bc_id = bc.bc_id  WHERE b.user_id=".$_SESSION['userId']." order by b.created_time desc";
pagination($getMyJobsSql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$getMyJobs			   	   =	$objUsers->listQuery($pg_result);
?>
<link href="<?php echo SITE_ROOT?>css/my-booking.css" rel="stylesheet" type="text/css">
<link href="<?php echo SITE_ROOT?>css/pagination.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.css" />
<script src="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.js"></script>
    <div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="user_profile">
						<?php echo $objCommon->checkEmailverification();?>
						<div class="content">
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                                    <div class="pagination_box">
                                		<a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
                                        <a href="<?php echo SITE_ROOT.$getUserDetails['usl_fameuz']?>"> My profile </a><i class="fa fa-caret-right"></i></a>
                                         <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                        <a href="javascript:;" class="active">My Blogs  </a>
									   <?php
                                        include_once(DIR_ROOT."widget/notification_head.php");
                                        ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-8 col-lg-sp-3">
                                    <div class="sidebar no-border">
                                        <div class="search_box">
											<form action="<?php echo SITE_ROOT.'user/search-result'?>" method="get">
                                            <input type="text" placeholder="Search"  name="keywordSearch" /></form></div>
                                        </div>
                                </div>
                            </div>
                                  <div class="row">
                                    <div class="col-lg-sp-2">
                            <?php
							include_once(DIR_ROOT."includes/profile_left.php");
							?>
							</div>
                            <div class="col-lg-sp-10">
                            <div class="tab_white_search">
                                <div class="section_search">
                                    <ul>
                                        <li class="visitor_head_box"><h5>My Blogs</h5><span class="point-it"></span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="sidebar">
                                <div class="clr"></div>
                                <div class="booking">
									<?php echo $objCommon->displayMsg();?>
                                    <div class="tab">
                                    	<ul class="c">
                                        	<li class="active"><a href="javascript:;">All Blogs</a></li>
                                        </ul>
                                    </div>
                                    <div class="table">
                                    	<div class="table_hed c">
                                            <div class="hash left17">&nbsp;</div>
                                            <div class="name left50">Title</div>
                                            <div class="e_date left13"><i class="fa fa-calendar-o"></i> Created Date</div>
                                            <div class="status left12"><i class="fa fa-info"></i> Status</div>
                                            <div class="delete left8">&nbsp;</div>
                                        </div>
										<?php
										 if(count($getMyJobs)>0){
											 $page_id			 	=	($_GET['page_id'])? ($num_results_per_page*($_GET['page_id']-1)):'';
											 foreach($getMyJobs as $keyJobs=>$allJobs){
											 	$siNo				=	$page_id+($keyJobs+1);
												$blog_status	 	 =	($allJobs['blog_status']==1)?'Enable':'Disable';;
												$todayDate		   =	date("Y-m-d");
												$editStatus		  =	1;
												?>
                                        <div class="t_data show<?php echo $objCommon->html2text($allJobs['blog_id']);?>">
                                        	<div class="data_hed">
                                            	<div class="hash left17">
                                                	<span class="count"><?php echo $siNo?></span>
                                                    <div class="details" data-booking="<?php echo $objCommon->html2text($allJobs['blog_id']);?>"  data-read="<?php echo $objCommon->html2text($allJobs['bm_read_status']);?>">Details <span class="fa fa-angle-down"></span></div>                                              
                                                </div>
                                                <div class="name left50"><?php echo $objCommon->html2text($allJobs['blog_title']);?></div>
                                                <div class="e_date left13"><?php echo date("d M Y",strtotime($allJobs['created_time']));?></div>
                                                <div class="status left12"><?php echo $blog_status?></div>
                                                <div class="delete left8">
													<a data-delid="<?php echo $objCommon->html2text($allJobs['blog_id'])?>" class="delete_booking" href="javascript:;"><i class="fa fa-trash"></i></a>
													<?php
													if($editStatus==1){
													?>
													<a href="<?php echo SITE_ROOT.'user/add-blogs/edit'.$objCommon->html2text($allJobs['blog_id'])?>"><i class="fa fa-pencil"></i></a>
													<?php
													}
													?>
												</div>
                                                <div class="clr"></div>
                                            </div>
                                            <div class="data_content">
                                            	<div class="main_content">
                                                	<h5><?php echo $objCommon->html2text($allJobs['blog_title']);?></h5>
													<div class="left100">
														<div class="left70">
                                                    		<p><?php echo $objCommon->html2textarea($allJobs['blog_descr']);?></p>
														</div>
														<div class="left30 descrImage">
														<?php
														if($allJobs['blog_img']){
														?>
														<img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/blogs/<?php echo $allJobs['blog_img']?>" />
														<?php
														}
														?>
														</div>
													</div>
                                                    <h3><span>Details</span></h3>
                                                    <div class="w_details">
														<?php 
														echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-calendar"></i>Created Date</div><div class="detail_colon">:</div><div class="detail_con">'.date("Y-m-d",strtotime($allJobs['created_time'])).'</div> </div>';
														echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-tags"></i>Category</div><div class="detail_colon">:</div><div class="detail_con">'.$objCommon->html2text($allJobs['bc_title']).'</div> </div>';
														?>
                                                    </div>
													<div class="clr"></div>
                                                </div>
                                                
                                                <div class="clr"></div>
                                            </div>
                                        </div>
										<?php
										}
										echo '<div class="paginationDiv">'.$pagination_output.'</div>';
										 }else{
											 echo '<p> No Results found... </p>';
										 }
										 ?>
                                    </div>
                                </div>
                            </div>
                            </div>
                            </div> 
                            <div class="clr"></div>                        
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
		$(document).ready(function(e) {
			var	showDiv		=	'<?php echo $show?>';
			if(showDiv){
				$(".show"+showDiv).toggleClass('active');
			}
			$('.details').click(function(e) {
			var t_data = $(this).parent().parent().parent();
			t_data.toggleClass('active');
			t_data.siblings().removeClass('active');
			return false;
            });
			$('.user_profile .sidebar .booking .t_data .data_hed .hash .detail_setting').click(function(e) {
				$(this).find('.setting_option').fadeToggle();
            });
			
			$('.delete_booking').click(function(){
				var delid = $(this).data("delid");
				var elem = $(this).closest('.item');
				var that			=	this;
				$.confirm({
					'title'		: 'Delete Confirmation',
					'message'	: 'You are about to delete this item ?',
					'buttons'	: {
					'Yes'	: {
					'class'	: 'blue',
					'action': function(){
					elem.slideUp();
					window.location.href = '<?php echo SITE_ROOT?>access/delete_blogs.php?action=del_job&jobId='+delid;
					}
					},
					'No'	: {
					'class'	: 'gray',
					'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
					}
					}
				});
			
			});
        });
	</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  