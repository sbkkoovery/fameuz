<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
?>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery.form.new.js"></script>
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
    <div class="inner_top_border">
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
          <div class="row">
            <div class="col-sm-12">
              <div class="pagination_box"> 
              	<a href="<?php echo SITE_ROOT?>">Home <i class="fa fa-caret-right"></i></a> <a href="<?php echo SITE_ROOT.'videos'?>"> video <i class="fa fa-caret-right"></i></a>
           		<a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                <a href="javascript:;" class="active"> upload </a>
                <?php
				include_once(DIR_ROOT."widget/notification_head.php");
				?>
              </div>
            </div>
          </div>
          <div class="main-background-white">
            <div class="row">
              <div class="col-sm-12">
                <div class="upload-sec">
                  <div class="upload-warning">
                    <div class="upload-warning-head">
                      <p>Please follow this rules :-</p>
                    </div>
                    <div class="upload-warning-points">
                      <ol>
                        <li> Lorem Ipsum is printing and typesetting industry. </li>
                        <li>Lorem Ipsum is simply dummy text of typesetting industry. </li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting ind. </li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </li>
                      </ol>
                    </div>
                    <div class="upload-warning-footer">
                      <p>Still have questions? Read the full <a href="#"><strong>Fameuz Guidelines.</strong></a> </p>
                      <img class="down-arrow" src="<?php echo SITE_ROOT?>images/down-arrow.png" /> </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="loadit">
            <?php
			include_once(DIR_ROOT."includes/upload_video_before.php");
			?>
            </div>
			<div class="uploading-section" id="uploading-section">
			<?php
			include_once(DIR_ROOT."includes/upload_video_after.php");
			?>
			</div>
          </div>
        </div>
        <?php
		include_once(DIR_ROOT."widget/right_static_ad_bar.php");
		?>
      </div>
    </div>
  </div>
</div>
<script>
/*$(document).ready(function(){
	$(".upoad-btn").click(function(){
		//$("#loadit").load("video-upload-1.php #uploading-section");
		var youtube_link	=	$("#youtube_link").val();
		if(youtube_link){
			$("#video_form").ajaxForm(
			{
			target: '#loadit',
			success:successCall
			}).submit();
		}
	});
	$('#file_browse').on('change', function(){ 
	alert("asd");
		$("#video_form").ajaxForm(
		{
		target: '#loadit',
		success:successCall
		}).submit();
	});
});
function successCall(){
	alert("success");
	
}*/
</script>
<script>
$('#file_browse').on('change', function(){ 
	var fName	=	$(this).val();
	if(fName){
		$(".or").hide();
		$(".youlink").html('<font style="color:#afb420;">'+fName+'</font>');
	}
});
(function() {
	var bar = $('.bar');
	var percent = $('.percent');
	var status = $('#video_ajax_preview');
	
	$('#video_form').ajaxForm({
			beforeSend: function() {
				var fileN	=	$('input[type=file]').val();
				var yLn	  =	$('#youtube_link').val();
				if(fileN =='' && yLn ==''){
					return false;
				}
				$("#loadit").hide();
				$("#uploading-section").show();
				var percentVal = '0%';
				bar.width(percentVal)
				percent.html(percentVal);
			},
			uploadProgress: function(event, position, total, percentComplete) {
				var percentVal = percentComplete + '%';
				bar.width(percentVal)
				percent.html(percentVal);
			},
			success: function() {
				var percentVal = '100%';
				bar.width(percentVal)
				percent.html(percentVal);
			},
			complete: function(xhr) {
				var $response=$(xhr.responseText);
				var oneval = $response.filter('#one').html();
				var twoval = $response.filter('#two').html();
				status.html(oneval);
				$(".pubVidDet").html(twoval);
				//status.html(xhr.responseText);
			}
	}); 

})();       
</script>
<?php
//
include_once(DIR_ROOT."includes/footer.php");
?>
