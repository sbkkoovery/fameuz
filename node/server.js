var app = require('express')();
var mysql   =   require("mysql");
var http = require('http').Server(app);
var io 		= 	require('socket.io')(http);
//var memwatch = require('memwatch');
var users	=	{};
var room	=	[];

var pool = mysql.createPool({
	connectionLimit : 10,
    host			: 'localhost',
    user			: 'fameuzco_olesya',
    password		: 'LwuQGVb1wr=D',
	database		: 'fameuzco_fameuz'
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});

app.use('/', function(req, res){
  res.sendFile(__dirname + '/chat.php');
});

io.on('connection', function (socket) {
	
	//when a user enter to socket it will update the users{};
	socket.on('enter', function(initData){
		if(initData.id in users){
			console.log(initData.id+'already exist in socket');
		}else{
			socket.username	=	initData.id;
			users[socket.username]	=	socket;
		}
		onlineUsers(initData);
	});
	// when user leave socket it will remove the users{};
	socket.on('disconnect', function(data){
		if(!socket.username) return;
		leaveRoom(socket.username);
		delete users[socket.username];
	});
	
	//fetch online friends list
	function onlineUsers(userData){
		var sql	=	"select user.user_id,user.first_name,user.last_name,user.display_name,profileImg.upi_img_url,user.email,social.usl_fameuz,case when (TIMESTAMPDIFF(MINUTE,chat.ucs_last_seen,'"+userData.D_T+"') >15 or chat.ucs_status=0) then '0' else '1' END AS chatStatus from following as follow left join users as user on (follow.follow_user1 = user.user_id and follow.follow_user1 !='"+userData.id+"') or (follow.follow_user2 = user.user_id and follow.follow_user2 !='"+userData.id+"') left join user_chat_status as chat on user.user_id = chat.user_id LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN personal_details AS personal ON user.user_id=personal.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 where (follow.follow_user1='"+userData.id+"' or follow.follow_user2='"+userData.id+"')  and follow.follow_status='2'  order by chatStatus DESC,user.first_name asc";
		
		eventConnection(sql, function(callback, rows){
			if(callback){
				if(userData.id in users){
					var onlineFriends	=	[];
					room.push('room'+userData.id);	
					socket.room	=	room;
					users[userData.id].emit('Release Online', rows);
					
					var dynamo	=	'user'+userData.id;
					var dynamo	=	new Array();
					
					Object.keys(rows).forEach(function(keys){
						var values	=	rows[keys].user_id;
						dynamo.push(values);
					});
					
					for(var i=0; i<dynamo.length; i++){
						if(dynamo[i] in users){
							onlineFriends.push(dynamo[i]);
						}
					}
					joinRoom(onlineFriends, room, userData);
				}
			}
		});
	}
	function findArrayIndex(Arr,Val){
		return Arr.indexOf(Val);
	}
	function joinRoom(friendsArray, room, userData){
		var existance	=	room.indexOf('room'+userData.id);
		for(var i=0; i<friendsArray.length; i++){
			if(friendsArray[i] in users){
				users[friendsArray[i]].join(room[existance]);
			}
		}
		socket.to(room[existance]).emit('update online', userData.id);
		
	}
	function leaveRoom(userLeave){
		var existance	=	room.indexOf('room'+userLeave);
		socket.to(room[existance]).emit('update left', userLeave);
		users[userLeave].leave(room['room'+userLeave]);
		room.splice(existance,1);
	}
	
	//fetching messages
	function fetchMessages(data){
		var sql	= "SELECT `msg_descr`,DATE_ADD(msg_created_date, INTERVAL 240 MINUTE) AS date, `msg_to` FROM `message` WHERE (msg_to="+data[0].iChatUserId+" AND msg_from="+data[1]+") OR (msg_to="+data[1]+" AND msg_from="+data[0].iChatUserId+") ORDER BY msg_created_date DESC LIMIT 0,20";
		eventConnection(sql, function(callback, rows){
			if(callback){
				if(data[1] in users){
					users[data[1]].emit('Release Msg', {messages:rows, selfId:data[0].iChatUserId});
				}
				
			}
		});
	}
	
	//send message and save into database
	function insertMessage(inserData){
		var sql	=	"INSERT INTO `message` (`msg_from`,`msg_to`,`msg_descr`,`msg_created_date`) VALUES ('"+inserData.from+"','"+inserData.to+"','"+inserData.message+"','"+inserData.dateT+"')";
		eventConnection(sql, function(callback){
			if(callback){
				if(inserData.to in users) {
					users[inserData.to].emit('Push Message', inserData);
				}
			}
		});
	}
	socket.on('Send Message', function(messageData){
		insertMessage(messageData);
	});
	
	//when a user is actibe
	socket.on('load Message', function(data, callback){
		var repsondMsg = fetchMessages(data);
	});
	
	socket.on('typing', function(typingData){
		if(typingData.to in users){
			users[typingData.to].emit('User Typing', typingData);
		}
	});
	socket.on('stop typing', function(StpTypingData){
		if(StpTypingData.to in users){
			users[StpTypingData.to].emit('Stop typing', StpTypingData);
		}
	});
	//databse connection for exrecuting query
	var eventConnection	=	function(sql, callback){
		pool.query(sql, function(err,rows){
				if(!err){
					callback(true, rows);
				}else{
					console.log(err+ 'erroe');
				}
		});
	}
	
});