<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
?>
<link href="<?php echo SITE_ROOT?>css/auto_search.css" rel="stylesheet" type="text/css" />
<link href="<?php echo SITE_ROOT?>css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>css/jquery.tokenize.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT_AWS_CDN?>js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
<script src="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.js"></script>
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
  <div class="inner_top_border">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-sp-9">
      <div class="jobs-head">
        <div class="pagination_box_jobs pagination_box">
        <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
        <a href="<?php echo SITE_ROOT.$getUserDetails['usl_fameuz']?>"> My profile </a><i class="fa fa-caret-right"></i></a>
         <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
        <a href="javascript:;" class="active">Message</a>
        <?php
		if(isset($_SESSION['userId'])){
        	include_once(DIR_ROOT."widget/notification_head.php");
		}
        ?>
        </div>
       </div>
       <div class="white-compo">
       		<div class="message-container">
            	
				<?php
				include_once(DIR_ROOT."widget/email_message_header.php");
				//include_once(DIR_ROOT."widget/email_message.php");
				?>
				<div id="loadEmail"><p class="emailLoading text-center">Loading.....</p><!--Email message load here--></div>
            </div>
       </div>
   </div>
      <?php
	include_once(DIR_ROOT."widget/right_online_memebers.php");
	?>
    </div>
    </div>
  </div>
</div>
<?php include_once(DIR_ROOT."js/include/message.php");?>
<!--<script src="<?php echo SITE_ROOT ?>js/message.js" type="text/javascript"></script>-->
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery.tokenize.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.wysihtml5').wysihtml5();
	$('#tokenize').tokenize();
	doShowMsgList(1);
});
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  