<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
?>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.form.new.js"></script>
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
    <div class="inner_top_border">
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
          <div class="row">
            <div class="col-sm-12">
              <div class="pagination_box"> 
                    <a href="<?php echo SITE_ROOT?>">Home <i class="fa fa-caret-right"></i></a> <a href="<?php echo SITE_ROOT.'music'?>"> music <i class="fa fa-caret-right"></i></a>
                    <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                    <a href="javascript:;" class="active"> upload </a>
                <?php
				include_once(DIR_ROOT."widget/notification_head.php");
				?>
              </div>
            </div>
          </div>
          <div class="main-background-white">
            <div class="row">
              <div class="col-sm-12">
			 <?php  echo $objCommon->displayMsg();?>
                <div class="upload-sec">
                  <div class="upload-warning">
                    <div class="upload-warning-head">
                      <p>Please follow this rules :-</p>
                    </div>
                    <div class="upload-warning-points">
                      <ol>
                        <li> Lorem Ipsum is printing and typesetting industry. </li>
                        <li>Lorem Ipsum is simply dummy text of typesetting industry. </li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting ind. </li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </li>
                      </ol>
                    </div>
                    <div class="upload-warning-footer">
                      <p>Still have questions? Read the full <a href="#"><strong>Fameuz Guidelines.</strong></a> </p>
                      <img class="down-arrow" src="<?php echo SITE_ROOT?>images/down-arrow.png" /> </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="loadit">
            <?php
			include_once(DIR_ROOT."includes/upload_music_before.php");
			?>
            </div>
			<div class="uploading-section" id="uploading-section">
			<?php
			include_once(DIR_ROOT."includes/upload_music_after.php");
			?>
			</div>
          </div>
        </div>
        <?php
		include_once(DIR_ROOT."widget/right_static_ad_bar.php");
		?>
      </div>
    </div>
  </div>
</div>
<script>
$('#file_browse').on('change', function(){ 
	var fName	=	$(this).val();
	if(fName){
		$(".or").hide();
		$(".youlink").html('<font style="color:#afb420;">'+fName+'</font>');
	}
});
(function() {
	var bar = $('.bar');
	var percent = $('.percent');
	
	$('#video_form').ajaxForm({
			beforeSend: function() {
				var fileN	=	$('input[type=file]').val();
				var yLn	  =	$('#youtube_link').val();
				if(fileN =='' && yLn ==''){
					return false;
				}
				$("#loadit").hide();
				$("#uploading-section").show();
				var percentVal = '0%';
				bar.width(percentVal)
				percent.html(percentVal);
			},
			uploadProgress: function(event, position, total, percentComplete) {
				var percentVal = percentComplete + '%';
				bar.width(percentVal)
				percent.html(percentVal);
				if(percentVal=='100%'){
					percent.html('Please be patient');
				}
			},
			success: function() {
			},
			complete: function(xhr) {
				console.log(xhr.responseText.length);
				console.log(xhr.responseText);
				if(xhr.responseText.length > 2){
					var percentVal = '100%';
					bar.width(percentVal)
					percent.html(percentVal);
					var $response=$(xhr.responseText);
					$(".publishBtn").show();
					$(".progressbar1").hide();
					$(".pubVidDet").html(xhr.responseText);
					//status.html(xhr.responseText);
				}else{
					$("#uploading-section").hide();
					$("#loadit").show();
					$(".msgAlert").show().html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Warning</strong> Invalid file format</div>');
				}
			}
	}); 

})();       
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
