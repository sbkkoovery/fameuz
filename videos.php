<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."class/videos.php");
include_once(DIR_ROOT."includes/session_check.php");
$objVideos						=	new videos();
?>
<script src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery.easing-1.3.js"></script>
<script src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery.mousewheel-3.1.12.js"></script>
<script src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery.jcarousellite.js"></script>
<link rel="stylesheet" media="all" type="text/css" href="<?php echo SITE_ROOT?>css/jcarousellite.css">
<div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="user_profile video-border">
                            <div class="row">
                            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                            <div class="pagination_box">
                                <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
                                <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                 <a href="#" class="active"> Videos </a>
                                     <?php
									 if(isset($_SESSION['userId'])){
										include_once(DIR_ROOT."widget/notification_head.php");
									 }
									?>
							</div>
                            	<div class="upper-video-sec">
                                	<?php
									include_once(DIR_ROOT."widget/video_serach_widget.php");
									?>
                                    <div class="col-sm-1">
                                </div>
                                </div>
                                    </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3">
                            	     <div class="sidebar no-border">
                            	<div class="search_box"><form action="<?php echo SITE_ROOT.'user/search-result'?>" method="get"><input type="text" placeholder="Search"  name="keywordSearch" /></form></div>
                            </div>
                            </div> 
                            </div> 
                            <div class="videos">
                            <div class="row">
                            	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-sp-9">
                            	<div class="video_left">
									<?php
									$getTopVideos				=	$objVideos->listQuery("SELECT vid.video_id,vid.video_url,video_encr_id,vid.video_thumb,vid.video_duration,vid.video_type,vid.video_title,vid.video_visits,vid.video_created,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz,SUM(vote.vv_vote) as voteSum FROM videos AS vid  LEFT JOIN users AS user ON vid.user_id = user.user_id LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
LEFT JOIN videos_vote AS vote ON vid.video_id	=	vote.video_id WHERE video_status=1 AND user.status=1 AND user.email_validation =1 GROUP BY vid.video_id ORDER BY voteSum DESC,video_created DESC LIMIT 0,24");
									?>
                                	<div class="top_videos custom-container mouseWheelButtons1">
                                	<h1 class="pull-left">Top Videos</h1><div class="slideNavig"><a href="#" class="prev">&nbsp;</a><a href="#" class="next">&nbsp;</a></div>
									
                                     <div class="videos_slider_section">
                                        
										<div class="carousel1">
											<ul>
												<li>
											<?php
											foreach($getTopVideos as $keytopVideos=>$allTopVideos){
												if (preg_match('%^https?://[^\s]+$%',$allTopVideos['video_thumb'])) {
													$thumbPath			=	$allTopVideos['video_thumb'];
												} else {
													$thumbPath			=	SITE_ROOT_AWS_VIDEOS.'uploads/videos/'.$objCommon->html2text($allTopVideos['video_thumb']);
												}
												$displayNameFriendSmall	=	$objCommon->displayNameSmall($allTopVideos,12);
												$vidcreatedTime			=	$objCommon->time_elapsed_string(strtotime($objCommon->local_time($allTopVideos['video_created'])));
											?>
													 <div class="item">
														<a href="<?php echo SITE_ROOT.'video/watch/'.$objCommon->html2text($allTopVideos['video_encr_id'])?>"><img src="<?php echo $thumbPath?>" alt="video" width="195"  /><div class="video_duration"><?php echo $allTopVideos['video_duration']?></div></a>
														<div class="video_main">
															<div class="title"><a href="<?php echo SITE_ROOT.'video/watch/'.$objCommon->html2text($allTopVideos['video_encr_id'])?>"><?php echo $objCommon->limitWords(ucfirst(strtolower($allTopVideos['video_title'])),50);?></a></div>
															<div class="produce">by <a href="<?php echo SITE_ROOT.$objCommon->html2text($allTopVideos['usl_fameuz'])?>"><?php echo $displayNameFriendSmall?></a></div> 
															<div class="left50 video_views"><span class="fa_eye"></span> <?php echo number_format($allTopVideos['video_visits']);?> views</div><div class="left50 time"> 
															<span class="fa_globe"></span> <?php echo $vidcreatedTime?></div>
															<div class="clr"></div>
														</div>
													</div>
												
												<?php
												if(($keytopVideos+1)%2==0 && ($keytopVideos+1)!= 24 ){
													echo '</li><li>';
												}
											}
											?>
												</li>
											</ul>
										</div>
										
                                    </div>
                                </div>
									
                                    
                                </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-sp-3">
                                 <?php include_once(DIR_ROOT.'widget/ad/right_ad_two_block.php');?>
                                </div>
                                </div>
                                <div class="clr"></div> 
								
								<?php /*?><?php
									$getPromoVideos				=	$objVideos->listQuery("SELECT vid.video_url,video_encr_id,vid.video_thumb,vid.video_duration,vid.video_type,vid.video_title,vid.video_visits,vid.video_created,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz 
										FROM videos AS vid 
										LEFT JOIN users AS user ON  vid.user_id = user.user_id
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN promotion AS promo ON vid.video_id = promo.promo_content_id AND promo.promo_type='video'
										WHERE video_status=1 AND user.status=1 AND user.email_validation =1 AND 
										((promo.promo_country LIKE '%,".$_SESSION['country_user']['country_id'].",%' AND promo_category LIKE '%".$getUserDetails['uc_c_id']."%' )  
										OR promo.promo_country !=''
										) AND   DATE(NOW()) between promo.promo_start_date and promo.promo_end_date AND promo.promo_admin_status=1 AND promo.promo_status=1
										 LIMIT 0,24");
									if(count($getPromoVideos)>0){
									?>
									<div class="top_videos custom-container mouseWheelButtons4">
										<h1 class="pull-left">Featured Videos</h1><div class="slideNavig"><a href="#" class="prev">&nbsp;</a><a href="#" class="next">&nbsp;</a></div>
										
										 <div class="videos_slider_section">
											
											<div class="carousel4">
												<ul>
												<?php
												foreach($getPromoVideos as $keyPromoVideos=>$allPromoVideos){
												?>
												<li>
												<?php
													if (preg_match('%^https?://[^\s]+$%',$allPromoVideos['video_thumb'])) {
														$thumbPath			=	$allPromoVideos['video_thumb'];
													} else {
														$thumbPath			=	SITE_ROOT_AWS_VIDEOS.'uploads/videos/'.$objCommon->html2text($allPromoVideos['video_thumb']);
													}
													$displayNameFriendSmall	=	$objCommon->displayNameSmall($allPromoVideos,12);
													$vidcreatedTime			=	$objCommon->time_elapsed_string(strtotime($objCommon->local_time($allPromoVideos['video_created'])));
												?>
														 <div class="item">
															<a href="<?php echo SITE_ROOT.'video/watch/'.$objCommon->html2text($allPromoVideos['video_encr_id'])?>"><img src="<?php echo $thumbPath?>" alt="video" width="214"  /><div class="video_duration"><?php echo $allPromoVideos['video_duration']?></div></a>
															<div class="video_main">
																<div class="title"><a href="<?php echo SITE_ROOT.'video/watch/'.$objCommon->html2text($allPromoVideos['video_encr_id'])?>">
																	<?php echo $objCommon->limitWords(ucfirst(strtolower($allPromoVideos['video_title'])),60);?></a>
																</div>
																<div class="produce">by <a href="<?php echo SITE_ROOT.$objCommon->html2text($allPromoVideos['usl_fameuz'])?>"><?php echo $displayNameFriendSmall?></a></div> 
																<div class="left50 video_views"><span class="fa_eye"></span> <?php echo number_format($allPromoVideos['video_visits']);?> views</div><div class="left50 time"><span class="fa_globe"></span> <?php echo $vidcreatedTime?></div>
																<div class="clr"></div>
															</div>
														</div>
													</li>
													<?php
												}
												?>
													
												</ul>
											</div>
											
										</div>
									</div>
									<?php
									}
									?><?php */?>
								
								
								
								<div class="clr"></div>  
								<?php
									$getNewVideos				=	$objVideos->listQuery("SELECT vid.video_url,video_encr_id,vid.video_thumb,vid.video_duration,vid.video_type,vid.video_title,vid.video_visits,vid.video_created,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz 
										FROM videos AS vid 
										LEFT JOIN users AS user ON  vid.user_id = user.user_id
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id  
										WHERE video_status=1 AND user.status=1 AND user.email_validation =1 ORDER BY video_created DESC LIMIT 0,24");
									?>
                               <div class="top_videos custom-container mouseWheelButtons2">
                                	<h1 class="pull-left">New Videos</h1><div class="slideNavig"><a href="#" class="prev">&nbsp;</a><a href="#" class="next">&nbsp;</a></div>
									
                                     <div class="videos_slider_section">
                                        
										<div class="carousel2">
											<ul>
											<?php
											foreach($getNewVideos as $keyNewVideos=>$allNewVideos){
											?>
											<li>
											<?php
												if (preg_match('%^https?://[^\s]+$%',$allNewVideos['video_thumb'])) {
													$thumbPath			=	$allNewVideos['video_thumb'];
												} else {
													$thumbPath			=	SITE_ROOT_AWS_VIDEOS.'uploads/videos/'.$objCommon->html2text($allNewVideos['video_thumb']);
												}
												$displayNameFriendSmall	=	$objCommon->displayNameSmall($allNewVideos,12);
												$vidcreatedTime			=	$objCommon->time_elapsed_string(strtotime($objCommon->local_time($allNewVideos['video_created'])));
											?>
													 <div class="item">
														<a href="<?php echo SITE_ROOT.'video/watch/'.$objCommon->html2text($allNewVideos['video_encr_id'])?>"><img src="<?php echo $thumbPath?>" alt="video" width="214"  /><div class="video_duration"><?php echo $allNewVideos['video_duration']?></div></a>
														<div class="video_main">
															<div class="title"><a href="<?php echo SITE_ROOT.'video/watch/'.$objCommon->html2text($allNewVideos['video_encr_id'])?>">
																<?php echo $objCommon->limitWords(ucfirst(strtolower($allNewVideos['video_title'])),60);?></a>
															</div>
															<div class="produce">by <a href="<?php echo SITE_ROOT.$objCommon->html2text($allNewVideos['usl_fameuz'])?>"><?php echo $displayNameFriendSmall?></a></div> 
															<div class="left50 video_views"><span class="fa_eye"></span> <?php echo number_format($allNewVideos['video_visits']);?> views</div><div class="left50 time"><span class="fa_globe"></span> <?php echo $vidcreatedTime?></div>
															<div class="clr"></div>
														</div>
													</div>
												</li>
												<?php
											}
											?>
												
											</ul>
										</div>
										
                                    </div>
                                </div>
                            	<div class="clr"></div> 
								<?php
								$getPopularVideos				=	$objVideos->listQuery("SELECT vid.video_url,video_encr_id,vid.video_thumb,vid.video_duration,vid.video_type,vid.video_title,vid.video_visits,vid.video_created,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz 
									FROM videos AS vid 
									LEFT JOIN users AS user ON  vid.user_id = user.user_id
									LEFT JOIN user_social_links AS social ON user.user_id=social.user_id  
									WHERE vid.video_status=1 AND user.status=1 AND user.email_validation =1 ORDER BY vid.video_visits DESC LIMIT 0,24");
								?> 
                               <div class="top_videos custom-container mouseWheelButtons3">
                                	<h1 class="pull-left">Popular Videos</h1><div class="slideNavig"><a href="#" class="prev">&nbsp;</a><a href="#" class="next">&nbsp;</a></div>
                                     <div class="videos_slider_section">
										<div class="carousel3">
											<ul>
											
											<?php
											foreach($getPopularVideos as $keyPopularVideos=>$allPopularVideos){
											?>
											<li>
											<?php
												if (preg_match('%^https?://[^\s]+$%',$allPopularVideos['video_thumb'])) {
													$thumbPath			=	$allPopularVideos['video_thumb'];
												} else {
													$thumbPath			=	SITE_ROOT_AWS_VIDEOS.'uploads/videos/'.$objCommon->html2text($allPopularVideos['video_thumb']);
												}
												$displayNameFriendSmall	=	$objCommon->displayNameSmall($allPopularVideos,12);
												$vidcreatedTime			=	$objCommon->time_elapsed_string(strtotime($objCommon->local_time($allPopularVideos['video_created'])));
											?>
													 <div class="item">
														<a href="<?php echo SITE_ROOT.'video/watch/'.$objCommon->html2text($allPopularVideos['video_encr_id'])?>"><img src="<?php echo $thumbPath?>" alt="video" width="214"  /><div class="video_duration"><?php echo $allPopularVideos['video_duration']?></div></a>
														<div class="video_main">
															<div class="title"><a href="<?php echo SITE_ROOT.'video/watch/'.$objCommon->html2text($allPopularVideos['video_encr_id'])?>"><?php echo $objCommon->limitWords(ucfirst(strtolower($allPopularVideos['video_title'])),60);?></a></div>
															<div class="produce">by <a href="<?php echo SITE_ROOT.$objCommon->html2text($allPopularVideos['usl_fameuz'])?>"><?php echo $displayNameFriendSmall?></a></div> 
															<div class="left50 video_views"><span class="fa_eye"></span> <?php echo number_format($allPopularVideos['video_visits']);?> views</div><div class="left50 time"><span class="fa_globe"></span> <?php echo $vidcreatedTime?></div>
															<div class="clr"></div>
														</div>
													</div>
												</li>
												<?php	
											}
											?>
											</ul>
										</div>
										
                                    </div>
                                </div>
                            </div>
                            <div class="clr"></div>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
$(window).load(function(e) {
	$(".mouseWheelButtons1 .carousel1").jCarouselLite({
		btnNext: ".mouseWheelButtons1 .next",
		btnPrev: ".mouseWheelButtons1 .prev",
		mouseWheel: true,
		visible: 4
	});
	$(".mouseWheelButtons2 .carousel2").jCarouselLite({
		btnNext: ".mouseWheelButtons2 .next",
		btnPrev: ".mouseWheelButtons2 .prev",
		mouseWheel: true,
		visible: 5
	});
	$(".mouseWheelButtons3 .carousel3").jCarouselLite({
		btnNext: ".mouseWheelButtons3 .next",
		btnPrev: ".mouseWheelButtons3 .prev",
		mouseWheel: true,
		visible: 5
	});
});
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
