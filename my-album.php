<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
@$show						=	$objCommon->esc($_GET['show']);
?>
<link href="<?php echo SITE_ROOT?>css/user-profile.css" rel="stylesheet" type="text/css">
<link href="<?php echo SITE_ROOT?>fameuz_lightbox/fameuz_lightbox.css" rel="stylesheet">
<link href="<?php echo SITE_ROOT?>css/image_popup.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.css" />
<script src="<?php echo SITE_ROOT?>jw_player/jwplayer.js"></script>
<link href="<?php echo SITE_ROOT?>jw_player/jw_player.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery-ui.js"></script>
<script src="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>fameuz_lightbox/famuez_lightbox.js"></script>
<div class="inner_content_section">
<!-------------------------------------------------------------------------------------------------------------->
<link href="<?php echo SITE_ROOT?>dropzone/css/dropzone.css" type="text/css" rel="stylesheet" />
<script src="<?php echo SITE_ROOT?>dropzone/dropzone.js"></script>
<!------------------------------------------------------------------------------------------------------------->

  <div class="container">
    <div class="inner_top_border">
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
          <div class="user_profile"> <?php echo $objCommon->checkEmailverification();?>
            <div class="content">
              <div class="pagination_box"> 
                <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
                <a href="<?php echo SITE_ROOT.$getUserDetails['usl_fameuz']?>"> My profile </a><i class="fa fa-caret-right"></i></a>
                <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
              <a href="javascript:;" class="active"> My Album </a>
                <div class="upload_photo_head">
                  <ul>
                    <li id="create_album_li"><a href="#create_album_pop" name="create" ><i class="fa fa-plus"></i><span class="hide-sm"> Create Album</span></a></li>
                    <li id="edit_album_li"><a href="javascript:;" name="create" onclick="editAlbumOpen();"><i class="fa fa-pencil-square-o"></i><span class="hide-sm"> Edit Album</span></a></li>
                    <li id="add_photo_li"><a href="javascript:;" id="add_timeline_photos"><i class="fa fa-camera"></i> <span class="hide-sm">Add Photo</span></a></li>
                    <li id="add_videos_li"><a href="javascript:;" id="add_album_videos"><i class="fa fa-video-camera"></i><span class="hide-sm"> Add Video</span></a></li>
                    <li id="all_photos_li"><a href="javascript:;" class="all_photos"><i class="fa fa-undo"></i><span class="hide-sm">All Albums</span></a></li>
                 	</ul>
                </div>
                <?php
				include(DIR_ROOT."widget/notification_head.php");
			  ?>
              </div>
              <div class="profile_content album-content">
                <div class="top-album-section">
                <div class="album">
                <a href="javascript:;" class="all_photos hike_color">Albums</a>
                <img class="arrow_down_tab" src="<?php echo SITE_ROOT?>images/arrow-down-grey.png" />
                </div>
                <div class="album">
                 <a href="javascript:;" onclick="showAllImages()">Photos</a>
                  <img class="arrow_down_tab" src="<?php echo SITE_ROOT?>images/arrow-down-grey.png" />
                </div>
                <div class="album">
                 <a href="javascript:;" onclick="showAlbumVideo()">Videos</a>
                  <img class="arrow_down_tab" src="<?php echo SITE_ROOT?>images/arrow-down-grey.png" />
                </div>
				<?php
				if($getUserDetails['uc_m_id']==2){
				?>
				<div class="album">
                 <a href="javascript:;" onclick="showPolaroids()">Polaroids</a>
                  <img class="arrow_down_tab" src="<?php echo SITE_ROOT?>images/arrow-down-grey.png" />
                </div>
				<?php
				}
				?>
                 </div>
                <div class="profile_border_box">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="load_album_content">
                        <?php //include_once(DIR_ROOT.'includes/album_list_home_page.php');?>
                      </div>
					   <!-- POPUP STARTS -->
						<link href="<?php echo SITE_ROOT?>css/image_popup.css" rel="stylesheet">
						<div id="pop_up" class="cd-popup">
							
						</div>
						<!-- POPUP ENDS -->
                    </div>
                  </div>
                </div>
              </div>
              <div class="clr"></div>
              </div>
          </div>
        </div>
        <?php
		include_once(DIR_ROOT.'includes/my_page_right.php');
		?>
      </div>
    </div>
  </div>
</div>
<div class="remodal create_album_pop_box" data-remodal-id="create_album_pop">
  <div class="pop_new_album">
    <div class="pop_new_album_head"> New Album </div>
    <div class="pop_new_album_form">
      <div class="form-group">
        <label  class="fontGray">Album Name</label>
        <div class="input-group">
  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-tag"></i></span>
        <input type="text" class="form-control" id="album_name" placeholder="Enter album name here....">
        </div>
      </div>
      <div class="form-group">
        <label class="fontGray">Album Description</label>
        <textarea class="form-control" rows="3" id="album_descr"></textarea>
      </div>
      <div class="form-group">
        <button type="button" class="btn btn-default" id="create_alb">Create</button>
      </div>
    </div>
  </div>
</div>
<input type="hidden" id="edit_album_id" value="" />
<div class="remodal create_album_pop_box" data-remodal-id="edit_album_pop">
  <div class="pop_new_album">
    <div class="pop_new_album_head"> Edit Album </div>
    <div class="pop_new_album_form ret_edit_val"> </div>
  </div>
</div>
<div class="remodal album_pre_loader" data-remodal-id="album_pre_loader">
  <div class="album_pre_load"><img src="<?php echo SITE_ROOT?>images/1.gif" width="32" height="32"/></div>
</div>
<div class="remodal create_album_pop_box" data-remodal-id="upload_video">
  <div class="pop_new_album">
    <div class="pop_new_album_head"> New Album </div>
    <div class="pop_new_album_form">
      <div class="form-group">
        <label  class="fontGray">Album Name</label>
        <input type="text" class="form-control" id="album_name" placeholder="Enter album name here....">
      </div>
      <div class="form-group">
        <label class="fontGray">Album Description</label>
        <textarea class="form-control" rows="3" id="album_descr"></textarea>
      </div>
      <div class="form-group">
        <button type="button" class="btn btn-default" id="create_alb">Create</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(e) {
	$('body').fameuzLightbox({
		class : 'lightBoxs',
		sidebar: 'album',
		photos:{
			imageLoadUrl: '<?php echo SITE_ROOT.'ajax/popUpImageUser.php'?>',
			data_attr	: ['data-contentId','data-contentType','data-contentAlbum'],
			likeUrl	: '<?php echo SITE_ROOT?>ajax/like.php',
			shareUrl: '<?php echo SITE_ROOT?>ajax/share.php',
			workedUrl: '<?php echo SITE_ROOT?>ajax/worked_together.php',
			commentBoxUrl: '<?php echo SITE_ROOT.'ajax/popUpImageMe.php' ?>',
		},
		videos : {
			videoLoadUrl: '<?php echo SITE_ROOT.'ajax/popUpVideoUser.php'?>',
			data_attr	: ['data-vidEncrId'],
			likeUrl		: '<?php echo SITE_ROOT?>ajax/like_video.php',
			shareUrl	: '<?php echo SITE_ROOT?>ajax/share_video.php',
			commentBoxUrl:'<?php echo SITE_ROOT.'ajax/popUpVideoMe.php' ?>',
		},
		skin: {
			next	: '<img src="<?php echo SITE_ROOT ?>fameuz_lightbox/images/next.png">',
			prev	: '<img src="<?php echo SITE_ROOT ?>fameuz_lightbox/images/prev.png">',
			reset	: '<i class="fa fa-refresh"></i>',
			close	: '<img src="<?php echo SITE_ROOT ?>fameuz_lightbox/images/close.png" width="15">',
			loader	: '<?php echo SITE_ROOT ?>images/ajax-loader.gif',
			review	: '<i class="fa fa-chevron-right"></i>',
			video	: '<?php echo SITE_ROOT ?>jw_player/six/six.xml',
		}
	});
});
	
var show		=	'<?php echo $show?>';
if(show = 'polaroids' && show !=''){
	showPolaroids();
}else{
	$(".load_album_content").load('<?php echo SITE_ROOT?>includes/album_list_home_page.php');
}
window.location.hash ='list';
$(document).ready(function(e) {
	$(".all_photos").click(function(){
		$("#edit_album_id").val('');
		$("#edit_album_li").css("display", "none");$("#all_photos_li").css("display", "none");$("#add_videos_li").css("display", "inline-block");$("#add_photo_li").css("display", "inline-block");  $("#create_album_li").css("display", "inline-block");
		$(".load_album_content").load('<?php echo SITE_ROOT?>includes/album_list_home_page.php');
	});
	$("#add_timeline_photos").click(function(){
		$("#edit_album_id").val('');
		$("#edit_album_li").css("display", "none");$("#add_photo_li").css("display", "none");$("#create_album_li").css("display", "inline-block");;$("#add_videos_li").css("display", "inline-block");$("#all_photos_li").css("display", "inline-block");
		$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/timeline_photo_page.php');
	});
	$("#add_album_videos").click(function(){
		$("#edit_album_id").val('');
		$("#edit_album_li").css("display", "none");$("#create_album_li").css("display", "inline-block");$("#add_videos_li").css("display", "none");$("#add_photo_li").css("display", "inline-block"); $("#all_photos_li").css("display", "inline-block");
		$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/album_video_list.php');
	});
	$("#create_alb").click(function(e){
		var album_name	=	$("#album_name").val();
		var album_descr   =	$("#album_descr").val();
		if(album_name){
			var inst = $.remodal.lookup[$('[data-remodal-id=create_album_pop]').data('remodal')];
			inst.close();
			$(".load_album_content").html('<div class="load_album_preloader"></div>');
			$.ajax({
				url:'<?php echo SITE_ROOT?>ajax/create_album.php',
				data:{album_name:album_name,album_descr:album_descr},
				type:"GET",
				success: function(data){
					$("#album_name").val('');
					$("#album_descr").val('');
					$("#create_album_li").css("display", "none");
					$("#add_videos_li").css("display", "none");
					$("#edit_album_li").css("display", "inline-block");
					$("#all_photos_li").css("display", "inline-block"); 
					$("#edit_album_id").val(data);
					$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/album_photo_page.php?albumId='+data);
					$(".load_album_preloader").hide();
				}
			});
		}else{
			 $( "#album_name" ).focus();
			 return false;
		}
	});
	$(".album:first-child").find(".arrow_down_tab").show();
	$(".album").click(function(){
		$(".arrow_down_tab").hide();
		$('.album a').removeClass('hike_color');
		$(this).find(".arrow_down_tab").show();
		$(this).children('a').addClass('hike_color')
	});
});
function showAlbum(aId){
	$(".load_album_content").html('<div class="load_album_preloader"></div>');
	$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/album_photo_page.php?albumId='+aId,function(){
		$(".load_album_preloader").hide();
		$("#create_album_li").css("display", "none");
		$("#add_videos_li").css("display", "none");
		$("#edit_album_li").css("display", "inline-block");
		$("#all_photos_li").css("display", "inline-block"); 
		$("#edit_album_id").val(aId);
	});
}
function showWallImg(){
	$(".load_album_content").html('<div class="load_album_preloader"></div>');
	$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/timeline_photo_page.php',function(){
		$(".load_album_preloader").hide();
		$("#edit_album_li").css("display", "none");$("#add_photo_li").css("display", "none");$("#create_album_li").css("display", "inline-block");;$("#add_videos_li").css("display", "inline-block");$("#all_photos_li").css("display", "inline-block");
	});
}
function showAlbumVideo(){
	$(".load_album_content").html('<div class="load_album_preloader"></div>');
	$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/album_video_list.php',function(){
		$(".load_album_preloader").hide();
		$("#edit_album_li").css("display", "none");$("#create_album_li").css("display", "inline-block");$("#add_videos_li").css("display", "none");$("#add_photo_li").css("display", "inline-block"); $("#all_photos_li").css("display", "inline-block");
	});
}
function showAllImages(){
	$(".load_album_content").html('<div class="load_album_preloader"></div>');
	$(".load_album_content").load('<?php echo SITE_ROOT?>includes/all_photos_list_home_page.php',function(){
		$(".load_album_preloader").hide();
		$("#edit_album_li").css("display", "none");$("#create_album_li").css("display", "inline-block");$("#add_videos_li").css("display", "none");$("#add_photo_li").css("display", "inline-block"); $("#all_photos_li").css("display", "inline-block");
	});
}
function showPolaroids(){
	$("#edit_album_li").css("display", "none");$("#add_photo_li").css("display", "none");$("#create_album_li").css("display", "inline-block");;$("#add_videos_li").css("display", "inline-block");$("#all_photos_li").css("display", "inline-block");
	$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/polaroids_photo_page.php');
}
function showProfileImg(){
	$(".load_album_content").html('<div class="load_album_preloader"></div>');
	$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/profile_photo_page.php',function(){
		$(".load_album_preloader").hide();
	});
}
function editAlbumOpen(){
	var inst_open 		= $.remodal.lookup[$('[data-remodal-id=edit_album_pop]').data('remodal')];
	var edit_album_id	= $("#edit_album_id").val();
	$.get('<?php echo SITE_ROOT?>ajax/album_edit_details.php',{'edit_album_id':edit_album_id},function(data){
		$(".ret_edit_val").html(data);
	});
	inst_open.open();
}
function update_form_album(){
		var album_name		=	$("#album_name_edit").val();
		var album_descr   	   =	$("#album_descr_edit").val();
		var update_album_id   =	$("#update_album_id").val();
		if(album_name){
			window.location.hash ='album_pre_loader';
			var inst = $.remodal.lookup[$('[data-remodal-id=album_pre_loader]').data('remodal')];
			$.get('<?php echo SITE_ROOT?>ajax/edit_album.php',{"album_name":album_name,"album_descr":album_descr,"update_album_id":update_album_id},function(data){
				$("#album_name_edit").val('');
				$("#album_descr_edit").val('');
				$("#update_album_id").val('');
				setTimeout(function () { $("#create_album_li").css("display", "none");$("#edit_album_li").css("display", "inline-block"); $("#edit_album_id").val(update_album_id);
				$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/album_photo_page.php?albumId='+update_album_id);inst.close();
				window.location.hash ='list';}, 3000);
			});
		}else{
			 $( "#album_name_edit" ).focus();
			 return false;
		}
}
function delAlbum(albumId){
	var inst1 = $.remodal.lookup[$('[data-remodal-id=edit_album_pop]').data('remodal')];
	$.confirm({
		'title'		: 'Delete Confirmation',
		'message'	: 'You are about to delete this item ?',
		'buttons'	: {
		'Yes'	: {
		'class'	: 'blue',
		'action': function(){
			inst1.close();
			$(".load_album_content").html('<div class="load_album_preloader"></div>');
			$.ajax({
				url:'<?php echo SITE_ROOT?>ajax/delete_album.php',
				data:{albumId:albumId},
				type:"GET",
				success: function(){
					$("#create_album_li").css("display", "inline-block");$("#edit_album_li").css("display", "none"); $("#edit_album_id").val('');
					$(".load_album_content").load('<?php echo SITE_ROOT?>includes/album_list_home_page.php');
					$(".load_album_preloader").hide();
				}
			});
		}
		},
		'No'	: {
		'class'	: 'gray',
		'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
		}
		}
	});
}
$('.status_box').on("click",'#change_status',function(e) {
                $('#status_option').fadeToggle();
				return false;
});
$('#status_option').on("click",'.check_Status',function(e) {
		var dataVal	=	$(this).attr('data-value');
		if(dataVal){
			$.get('<?php echo SITE_ROOT?>ajax/change_online_chat_status.php',{"dataVal":dataVal},function(data1){
				$(".status_box").html(data1);
			});
		}
	});
	 $(function () {
 	 $('[data-toggle="tooltip"]').tooltip();
	});
$("body").on("click", ".makeCover_btn",function(){
	var imgId			=	$(this).data("id");
		imgCat		   =	$(this).data("imgcat");
		that			 =	this;
	if(imgId != '' && imgCat != ''){
		$.ajax({
			url:'<?php echo SITE_ROOT?>ajax/set_cover_image.php',
			data:{imgId:imgId,imgCat:imgCat},
			type:"POST",
			success: function(data){
				$(that).html('<i class="fa fa-check"></i>Make Cover Photo ');
			}
		});
	}
});			
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>