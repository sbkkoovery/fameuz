<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."class/main_category.php");
include_once(DIR_ROOT."class/nationality.php");
include_once(DIR_ROOT."class/country.php");
include_once(DIR_ROOT."class/personal_details.php");
$objPersonalDetails		   		=	new personal_details();
$objMainCategory				   =	new main_category();
$objNationality					=	new nationality();
$objCountry						=	new country();
$getMainCategoryAll				=	$objMainCategory->getAll();
$getPersonalDetails				=	$objPersonalDetails->getRowSql("select p_gender from personal_details where user_id=".$_SESSION['userId']);
//echo $_SESSION['step'];
include_once(DIR_ROOT."includes/session_check.php");
?>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/bootstrap-checkbox.js"></script>
<link href="<?php echo SITE_ROOT?>css/cmxform.css" rel="stylesheet" type="text/css" />
<link href="<?php echo SITE_ROOT?>css/cmxformTemplate.css" rel="stylesheet" type="text/css" />
<link href="<?php echo SITE_ROOT?>css/bootstrap-checkbox.css" rel="stylesheet" type="text/css" />

<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
<script>
window.onload=function(){initialize();};
var placeSearch, autocomplete;
var componentForm = {
  locality: 'long_name',
  country: 'long_name'
};

function initialize() {
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('locality')),
      { types: ['geocode'] });
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    fillInAddress();
  });
}
function fillInAddress() {
  var place = autocomplete.getPlace();
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
// [END region_geolocation]

    </script>
	<script src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery.Jcrop.js"></script>

<script type="text/javascript">
function cropImage(){
	if (parseInt($('#w').val())){
		$(".preview-container").html('<div class="text-center"><img src="<?php echo SITE_ROOT?>images/1.gif" height="32" width="32"/></div>');
		var targetFile	=	$("#target").attr("src");
		
		var xvalue		=	$('#x').val();
		var yvalue		=	$('#y').val();
		var wvalue		=	$('#w').val();
		var hvalue		=	$('#h').val();
		var resWidth	  =	$("#target").width();
		var resHeight	  =	$("#target").height();
		$.get("<?php echo SITE_ROOT?>ajax/imageCrope.php",{x:xvalue,y:yvalue,w:wvalue,h:hvalue,src:targetFile,resWidth:resWidth,resHeight:resHeight},
		function(data){
			if(data!=""){
				d = new Date();
				var imageFile	=	'<img src="<?php echo SITE_ROOT?>uploads/profile_images/'+data+'?id='+d.getTime()+'" class="jcrop-preview" alt="Preview" />';
				var imageprofile =	'<img src="<?php echo SITE_ROOT?>uploads/profile_images/'+data+'" alt="profile_pic" />';
				//$(".preview-container").html(imageFile);
				$(".profile_pic").html(imageprofile);
				$(".user_thumb").html(imageprofile)
				$(".dp_show").hide();
				$(".alertPic").show();
				$( ".alertClose" ).delay( 4000 ).slideUp( 400 );
			}
		});
	}else{
		alert('Please select a crop region then press submit.');
	}
}
</script>
<link rel="stylesheet" href="<?php echo SITE_ROOT?>css/jquery.Jcrop.css" type="text/css" />
<script src="<?php echo SITE_ROOT?>js/jquery.upload-1.0.2.js"></script>
<script>
$(document).ready(function(){
$('input[type=file]').change(function() {
    $(this).upload('<?php echo SITE_ROOT?>access/upload-profile-image.php?ID=1', function(res) {
		if(res!=""){
			$(".dp_show").show();
			d = new Date();
			var uploadedContent	=	'<img src="<?php echo SITE_ROOT?>uploads/profile_images/'+res+'?'+d.getTime()+'" id="target" />';
			var uploadedPreview	=	'<img src="<?php echo SITE_ROOT?>uploads/profile_images/'+res+'?'+d.getTime()+'" class="jcrop-preview" alt="Preview" />';
			$(".uploadedImage").html(uploadedContent);
			$(".preview-container").html(uploadedPreview);
			jQuery(function($){

    // Create variables (in this scope) to hold the API and image size
    var jcrop_api,
        boundx,
        boundy,

        // Grab some information about the preview pane
        $preview = $('#preview-pane'),
        $pcnt = $('#preview-pane .preview-container'),
        $pimg = $('#preview-pane .preview-container img'),

        xsize = $pcnt.width(),
        ysize = $pcnt.height();
    
    console.log('init',[xsize,ysize]);
    $('#target').Jcrop({
      onChange: updatePreview,
      onSelect: updateCoords,
      aspectRatio: xsize / ysize
    },function(){
      // Use the API to get the real image size
      var bounds = this.getBounds();
      boundx = bounds[0];
      boundy = bounds[1];
      // Store the API in the jcrop_api variable
      jcrop_api = this;
      jcrop_api.setSelect([70,20,70+350,65+385]);
      jcrop_api.setOptions({ bgFade: true });
      jcrop_api.ui.selection.addClass('jcrop-selection');

      // Move the preview into the jcrop container for css positioning
      //$preview.appendTo(jcrop_api.ui.holder);
    });

    function updatePreview(c){
      if (parseInt(c.w) > 0)
      {
		$("#preview-pane").show();
        var rx = xsize / c.w;
        var ry = ysize / c.h;

        $pimg.css({
          width: Math.round(rx * boundx) + 'px',
          height: Math.round(ry * boundy) + 'px',
          marginLeft: '-' + Math.round(rx * c.x) + 'px',
          marginTop: '-' + Math.round(ry * c.y) + 'px'
        });
      }
    };
	function updateCoords(c){
		$('#x').val(c.x);
		$('#y').val(c.y);
		$('#w').val(c.w);
		$('#h').val(c.h);
	 };
	function checkCoords(){
		if (parseInt($('#w').val())) return true;
		alert('Please select a crop region then press submit.');
		return false;
	  };

  });
		}
    });
});
});
</script>
<div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="register_page">
                        	<div class="register_heading">
                            	<h2>Your Primary Details</h2>
                                <p>These details are available inside your profile and it can be updated later</p>
                            </div>
                            <div class="registration_tabs">
                            	<div class="tab_label register_tab1"><!-- add this class ""-->
                                	<div class="text"  <?php echo ($_SESSION['step']!=2 && $_SESSION['step']!=3  )?' onclick="registerTab(1);"':''; ?>>
                                    Primary<br>Details
                                    </div>
                                </div>
                            	<div class="tab_label register_tab2  <?php echo ($_SESSION['step'] >=2)?' select':''; ?>"><!-- add this class " select complete"-->
                                	<div class="text" <?php echo ($_SESSION['step']==2)?' onclick="registerTab(2);"':''; ?>>
                                    Personal<br>Details
                                    </div>
                                </div>
                            	<div class="tab_label register_tab3 <?php echo ($_SESSION['step']==3)?' select':''; ?>"><!-- add this class "select"-->
                                	<div class="text" <?php echo ($_SESSION['step']==3)?' onclick="registerTab(3);"':''; ?>>
                                    Upload<br>Photo
                                    </div>
                                
                                </div>
                            	<div class="clr"></div>
                            </div>
                            <div class="register_box">
                            	<div class="first_box commonBox" id="box1" <?php echo ($_SESSION['step']==2 || $_SESSION['step']==3 )?'style="display:none;"':'style="display:block;"'; ?>>
                                	<form name="multiform" id="multiform" class="regForm" action="<?php echo SITE_ROOT?>ajax/register-step.php" method="POST" enctype="multipart/form-data">
                                    <h2 class="bg_heading">Basic Details</h2>
                                    <div class="row edit-select">
                                    	<div class="col-sm-4">
                                        	<div class="form-group">
                                            	<label>First Name</label>
                                                <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-tag"></i></span>
                                                <input type="text" name="first_name" class="form-control" placeholder="First Name" value="<?php echo $getUserDetails['first_name']; ?>" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                        	<div class="form-group">
                                            	<label>Last Name</label>
                                                <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-tag"></i></span>
                                                <input type="text" name="last_name" class="form-control" placeholder="Last Name" value="<?php echo $getUserDetails['last_name']; ?>" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                        	<div class="form-group">
                                            	<label>Display Name</label>
                                                <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                <input type="text" name="display_name" class="form-control" placeholder="Display Name" value="<?php echo $getUserDetails['display_name']; ?>" >
                                                </div>
                                            </div>
                                        </div>
                                    	<div class="col-sm-4">
                                        	<div class="form-group Privacy_settingz">
                                            	<label>Phone Number</label>
                                                <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                <input type="text" name="p_phone" class="form-control" placeholder="Phone Number" value="<?php echo $getUserDetails['phone']; ?>" >
                                                </div>
                                            </div>
                                            <div class="settings">
                                            	<i class="fa fa-cog"></i>
                                                <div class="dropdown_option">
													<div class="arrow"><img alt="status_option_arrow" src="<?php echo SITE_ROOT?>images/status_option_arrow.png"></div>
													<label><input type="checkbox" name="per_phone_privacy1" value="1"/>Public</label>
													<label><input type="checkbox" name="per_phone_privacy2" value="1" />Friends</label>
													<label><input type="checkbox"  name="per_phone_privacy3" value="1" />Professionals</label>
													<label><input type="checkbox"  name="per_phone_privacy4" value="1" />Only for me</label>
												</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                        	<div class="form-group">
                                            	<label>Email Address</label>
                                                <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                <input type="text" class="form-control" placeholder="<?php echo $objCommon->html2text($getUserDetails['email']); ?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                        	<div class="form-group">
                                            	<label>Alternative Email Address</label>
                                                <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                <input type="email" class="form-control" placeholder="Email Address" name="p_alt_email" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                        	<div class="form-group">
                                            	<label>Gender</label>
                                                <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-mars"></i></span>
                                                    <select name="p_gender" class="form-control selectBox selectexcl">
                                                        <option value="">Select Gender</option>
                                                        <option value="1">Male</option>
                                                        <option value="2">Female</option>
                                                    </select>
                                                
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                        	<div class="form-group Privacy_settingz_alter">
                                            	<label>Date Of Birth</label>
                                                <div class="input-group adjustSize bigResize">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <select name="dobD" class="form-control selectBox">
                                                            <option value="">DD</option>
                                                            <?php for($i=1;$i<32;$i++){?>
                                                            <option value="<?php echo sprintf('%02d',$i); ?>"><?php echo sprintf('%02d',$i); ?></option>
                                                            <?php }?>
                                                        </select>
                                                
                                                </div>
                                                <div class="adjustSize smallResize">
                                                    <select name="dobM" class="form-control selectBox">
                                                        <option value="">MM</option>
                                                        <?php for($i=1;$i<13;$i++){?>
                                                        <option value="<?php echo sprintf('%02d',$i); ?>"><?php echo sprintf('%02d',$i); ?></option>
                                                        <?php }?>
                                                    </select>
                                                
                                                </div>
                                                <div class=" adjustSize">
                                                    <select  name="dobY" class="form-control selectBox">
                                                        <option value="">YYYY</option>
                                                        <?php for($i=date("Y");$i>(date("Y")-100);$i--){?>
                                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                        <?php }?>
                                                    </select>
                                                
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="settings">
												<i class="fa fa-cog"></i>
												<div class="dropdown_option">
													<div class="arrow"><img src="<?php echo SITE_ROOT?>images/status_option_arrow.png" alt="status_option_arrow" /></div>
													<label><input type="checkbox" name="per_dob_privacy1" value="1" />Public</label>
													<label><input type="checkbox" name="per_dob_privacy2" value="1" />Friends</label>
													<label><input type="checkbox"  name="per_dob_privacy3" value="1"  />Professionals</label>
													<label><input type="checkbox"  name="per_dob_privacy4" value="1" />Only for me</label>
												</div>
											</div>
                                        </div>
                                    </div>
                                  
                                    <h2 class="bg_heading">Location Details</h2>
                                    <div class="row">
                                    	<div class="col-sm-4">
                                        	<div class="form-group">
                                            	<label>Nationality</label>
                                                <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-mars"></i></span>
                                                    <select name="nationality" class="form-control selectBox">
                                                        <option value="">Please Select</option>
                                                        <?php 
                                                        $nationalityList	=	$objNationality->getAll();
                                                        foreach($nationalityList as $nationality){?>
                                                        <option value="<?php echo $nationality['n_id']; ?>"><?php echo $nationality['n_name']; ?></option>
                                                        <?php }?>
                                                    </select>
                                                
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                        	<div class="form-group">
                                            	<label>City</label>
                                                <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-building"></i></span>
                                                <input type="text" name="city" onFocus="geolocate()" class="form-control" autocomplete="off"  id="locality"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                        	<div class="form-group">
                                            	<label>Country</label>
                                                <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                                                <input type="text" name="country" autocomplete="off" class="form-control"  id="country"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="field_box_section">
                                        <p class="agree"><label><input type="checkbox" name="agree">I Agree</label><a href="#"> Terms and Condition</a></p>
                                        <div><input type="submit" name="primary_details" value="Continue" ></div>
                                    </div>
                                  </form>
                                  </div>
                                   
                                
                                <!--second_box-->
<?php
include_once(DIR_ROOT."class/model_chest.php");
include_once(DIR_ROOT."class/model_collar.php");
include_once(DIR_ROOT."class/model_dress.php");
include_once(DIR_ROOT."class/model_eyes.php");
include_once(DIR_ROOT."class/model_height.php");
include_once(DIR_ROOT."class/model_hips.php");
include_once(DIR_ROOT."class/model_jacket.php");
include_once(DIR_ROOT."class/model_shoes.php");
include_once(DIR_ROOT."class/model_trousers.php");
include_once(DIR_ROOT."class/model_waist.php");
include_once(DIR_ROOT."class/model_hair.php");
include_once(DIR_ROOT."class/model_details.php");
include_once(DIR_ROOT."class/model_disciplines.php");
$objModelChest				=	new model_chest();
$objModelCollar			   =	new model_collar();
$objModelDress				=	new model_dress();
$objModelEyes				 =	new model_eyes();
$objModelHeight			   =	new model_height();
$objModelHips				 =	new model_hips();
$objModelJacket			   =	new model_jacket();
$objModelShoes				=	new model_shoes();
$objModelTrousers			 =	new model_trousers();
$objModelWaist				=	new model_waist();
$objModelDetails			  =	new model_details();
$objModelHair				 =	new model_hair();
$objModelDisciplines		  =	new model_disciplines();
$getAllModelChest			 =	$objModelChest->getAll("");
$getAllModelCollar			=	$objModelCollar->getAll("");
$getAllModelDress			 =	$objModelDress->getAll("");
$getAllModelEyes			  =	$objModelEyes->getAll("","me_name");
$getAllModelHeight			=	$objModelHeight->getAll("");
$getAllModelHips			  =	$objModelHips->getAll("");
$getAllModelJacket			=	$objModelJacket->getAll("");
$getAllModelShoes			 =	$objModelShoes->getAll("");
$getAllModelTrousers		  =	$objModelTrousers->getAll("");
$getAllModelwaist			 =	$objModelWaist->getAll("");
$getModelDetails			  =	$objModelDetails->getRow("user_id=".$_SESSION['userId']);
$getAllModelHair			  =	$objModelHair->getAll("","mhair_name");
$getAllModelDesciplines	   =	$objModelDisciplines->getAll("","model_dis_name");
?>
                                <div class="second_box commonBox edit-select edit-select2" id="box2" <?php echo ($_SESSION['step']==2)?' style="display:block"':'style="display:none"'; ?>>
                                	<form action="<?php echo SITE_ROOT?>ajax/register-step.php" method="post" enctype="multipart/form-data">
                                    <h2 class="bg_heading">Your Details</h2>
                                    <div class="row">
                                    	<div class="col-sm-4">
                                        	<div class="form-group">
                                            	<label>Eyes</label>
                                                <div >
                                                    <select name="pub_model_eyes" id="pub_model_eyes" class="form-control selectBox">
                                                            <option value="0">Please Select</option>
                                                            <?php
                                                            foreach($getAllModelEyes as $allModelEyes){
                                                            ?>
                                                            <option value="<?php echo $allModelEyes['me_id']?>" <?php echo ($getModelDetails['me_id']==$allModelEyes['me_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelEyes['me_name'])?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                        	<div class="form-group">
                                            	<label>Hair</label>
                                                <div >
                                                    <select name="pub_model_hair" id="pub_model_hair" class="form-control selectBox">
                                                            <option value="0">Please Select</option>
                                                            <?php
                                                            foreach($getAllModelHair as $allModelHair){
                                                            ?>
                                                            <option value="<?php echo $allModelHair['mhair_id']?>" <?php echo ($getModelDetails['mhair_id']==$allModelHair['mhair_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelHair['mhair_name'])?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="row">
                                        <div class="col-sm-4">
                                        	<div class="form-group">
                                            	<label>Height</label>
                                                <div >
                                                    <select name="pub_model_height" id="pub_model_height" class="form-control selectBox">
                                                            <option value="0">Please Select</option>
                                                            <?php
                                                            foreach($getAllModelHeight as $allModelHeight){
                                                            ?>
                                                            <option value="<?php echo $allModelHeight['mh_id']?>" <?php echo ($getModelDetails['mh_id']==$allModelHeight['mh_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelHeight['mh_name'])?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                        	<div class="form-group">
                                            	<label>Waist</label>
                                                <div >
                                                    <select name="pub_model_waist" id="pub_model_waist" class="form-control selectBox">
                                                            <option value="0">Please Select</option>
                                                            <?php
                                                            foreach($getAllModelwaist as $allModelwaist){
                                                            ?>
                                                            <option value="<?php echo $allModelwaist['mw_id']?>" <?php echo ($getModelDetails['mw_id']==$allModelwaist['mw_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelwaist['mw_name'])?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                        	<div class="form-group">
                                            	<label>Hips</label>
                                                <div >
                                                    <select name="pub_model_hips" id="pub_model_hips" class="form-control selectBox">
                                                            <option value="0">Please Select</option>
                                                            <?php
                                                            foreach($getAllModelHips as $allModelHips){
                                                            ?>
                                                            <option value="<?php echo $allModelHips['mhp_id']?>" <?php echo ($getModelDetails['mhp_id']==$allModelHips['mhp_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelHips['mhp_name'])?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                        	<div class="form-group">
                                            	<label><?php echo ($getPersonalDetails['p_gender']==1)?'Chest':'Bust';?></label>
                                                <div >
                                                    <select name="pub_model_chest" id="pub_model_chest" class="form-control selectBox">
                                                            <option value="0">Please Select</option>
                                                            <?php
                                                            foreach($getAllModelChest as $allModelChest){
                                                            ?>
                                                            <option value="<?php echo $allModelChest['mc_id']?>" <?php echo ($getModelDetails['mc_id']==$allModelChest['mc_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelChest['mc_name'])?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                        	<div class="form-group">
                                            	<label>Shoe</label>
                                                <div >
                                                    <select name="pub_model_shoe" id="pub_model_shoe" class="form-control selectBox">
                                                            <option value="0">Please Select</option>
                                                            <?php
                                                            foreach($getAllModelShoes as $allModelShoes){
                                                            ?>
                                                            <option value="<?php echo $allModelShoes['ms_id']?>" <?php echo ($getModelDetails['ms_id']==$allModelShoes['ms_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelShoes['ms_name'])?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
									<?php
                                    if($getPersonalDetails['p_gender']==2){
                                    ?>
                                    <div class="row profile_female">
                                        <div class="col-sm-4">
                                        	<div class="form-group">
                                            	<label>Dress</label>
                                                <div >
                                                        <select name="pub_model_dress" id="pub_model_dress" class="form-control selectBox">
                                                            <option value="0">Please Select</option>
                                                            <?php
                                                            foreach($getAllModelDress as $allModelDress){
                                                            ?>
                                                            <option value="<?php echo $allModelDress['mdress_id']?>" <?php echo ($getModelDetails['mdress_id']==$allModelDress['mdress_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelDress['mdress_name'])?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
									<?php }?>
                                        
									<?php
									if($getPersonalDetails['p_gender']==1){
									?>
                                    <div class="row profile_male">
                                        <div class="col-sm-4">
                                        	<div class="form-group">
                                            	<label>Jacket</label>
                                                <div >
                                                        <select name="pub_model_jacket" id="pub_model_jacket" class="form-control selectBox">
                                                            <option value="0">Please Select</option>
                                                            <?php
                                                            foreach($getAllModelJacket as $allModelJacket){
                                                            ?>
                                                            <option value="<?php echo $allModelJacket['mj_id']?>" <?php echo ($getModelDetails['mj_id']==$allModelJacket['mj_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelJacket['mj_name'])?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                        	<div class="form-group">
                                            	<label>Trousers</label>
                                                <div >
                                                        <select name="pub_model_trousers" id="pub_model_trousers" class="form-control selectBox">
                                                            <option value="0">Please Select</option>
                                                            <?php
                                                            foreach($getAllModelTrousers as $allModelTrousers){
                                                            ?>
                                                            <option value="<?php echo $allModelTrousers['mt_id']?>" <?php echo ($getModelDetails['mt_id']==$allModelTrousers['mt_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelTrousers['mt_name'])?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                        	<div class="form-group">
                                            	<label>Collar</label>
                                                <div >
                                                        <select name="pub_model_collar" id="pub_model_collar" class="form-control selectBox">
                                                            <option value="0">Please Select</option>
                                                            <?php
                                                            foreach($getAllModelCollar as $allModelCollar){
                                                            ?>
                                                            <option value="<?php echo $allModelCollar['mcollar_id']?>" <?php echo ($getModelDetails['mcollar_id']==$allModelCollar['mcollar_id'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allModelCollar['mcollar_name'])?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        <?php } ?>
                
                                    <div class="field_box_section">
                                        <div class="submit"><input type="submit" name="personal_details" value="Continue" ></div>
                                    </div>
                                    </form>
                                </div>
                                <!--third_box-->
                                <div class="third_box commonBox" id="box3"  <?php echo ($_SESSION['step']==3)?' style="display:block"':'style="display:none"'; ?>>
                                    <div class="field_box_section">
									<?php /*?><div class="alert alertClose alert-success alert-dismissible alertPic" role="alert">
										<button type="button" class="close" data-dismiss="alertClose" aria-label="Close"><span aria-hidden="true">&times;</span></button>Your profile image has been updated..
									</div><?php */?>
									<div class="dp_show">
									
                                        <h2 class="bg_heading">Upload your photo</h2>
                                        <p class="pic_dis">* Select the area of picture you wish to display</p>
                                        <div class="pic_frame">
											<div class="jc-demo-box">
												<div class="uploadedImage"></div>
												<div class="hiddenFields">
													<input type="hidden" id="x" name="x" />
													<input type="hidden" id="y" name="y" />
													<input type="hidden" id="w" name="w" />
													<input type="hidden" id="h" name="h" />
												</div>
												<div id="preview-pane">
													<div class="preview-container"></div>
													<input type="button" onClick="cropImage();" value="Save" class="btn btn-small btn-inverse btn-crop" />
												</div>    
												
												<div class="clearfix"></div>
											</div>
										</div>
                                    </div>
									</div>
                                    <div class="field_box_section">
                                        <h2 class="bg_heading">Upload Photo </h2>
                                        <div class="field_box">
                                            <div class="fiel_border">
                                                <div class="text">
                                                   <div class="file_up"> <input type="file" name="photoimg"/> <span class="file_browse">Browse</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="field_box_section">
                                        <p class="agree"><label><input type="checkbox" checked="checked">I certify that this image contains no nudity or other material, which could be deemed offensive by some users. </label><a href="#"> Terms of use</a></p>
                                        <!--<div><input type="submit" value="Continue"></div>-->
										<div><a href="<?php echo SITE_ROOT.$objCommon->html2text($getUserDetails['usl_fameuz'])?>" class="go_to_profile">Go to profile</a></div>
                                    </div>
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
//$('input[type="checkbox"]').checkbox();
$("#multiform").validate({

     /*   submitHandler: function(form) {

            var data = $("#multiform").serialize();

            $.ajax({
                type: "POST",
                url: "<?php echo SITE_ROOT?>ajax/register-step.php",
                data: data,
                cache: false,
                success: function(result) {
                   registerTab(result);
				   //$(".register_tab"+result+" .text").click(registerTab(result));
				   $(".register_tab"+result+" .text").attr('onclick','registerTab('+result+')');
                },
                error: function (xhr, ajaxOptions, thrownError) {
					alert(xhr.status);
					alert(thrownError);
                }
            });
        },*/
			rules: {
				first_name: "required",
				last_name: "required",
				display_name: "required",
				p_alt_email: {email: true},
				p_gender: "required",
				city: "required",
				agree: "required",
				dobD: "required",
				dobM: "required",
				dobY: "required",
				nationality:"required",
				country:"required"
			},
			messages: {
				first_name: '<i class="fa fa-times"></i>',
				last_name: '<i class="fa fa-times"></i>',
				display_name: '<i class="fa fa-times"></i>',
				p_gender: '<i class="fa fa-times"></i>',
				p_alt_email: '<i class="fa fa-times"></i>',
				city: '<i class="fa fa-times"></i>',
				agree: '<i class="fa fa-times"></i>',
				dobD: '<i class="fa fa-times"></i>',
				dobM: '<i class="fa fa-times"></i>',
				dobY: '<i class="fa fa-times"></i>',
				nationality: '<i class="fa fa-times"></i>',
				country: '<i class="fa fa-times"></i>'
			}

    });


/*-------------------------------*/
$(document).ready(function(e) {
$(".close_btn").click(function(e) {
	$(this).parent('.language').remove();
});
$('.settings').click(function(e) {
	$(this).find('.dropdown_option').toggle();
});
});
function registerTab(tabId){
	$(".commonBox").hide();
	$("#box"+tabId).show();
	for(var i=tabId;i<=3;i++){
		$(".register_tab"+i).removeClass("select");
	}
	$(".register_tab"+tabId).addClass("select");
}
</script> 
<script language="javascript" type="text/javascript">
$(".pic").on("click","a",function(){
	$(".pic").removeClass("active");
	$(this).parent().addClass("active");
});
</script>  
<?php
//
//include_once(DIR_ROOT."includes/login_box.php");
include_once(DIR_ROOT."includes/footer.php");
?>