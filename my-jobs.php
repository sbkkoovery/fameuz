<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/pagination-class-front.php");
$job_status				  =	$objCommon->esc($_GET['job_status']);
$activeJobId				 =	$objCommon->esc($_GET['active']);
$num_results_per_page		= 	20;
$num_page_links_per_page 	 = 	5;
$pg_param 					= 	"";
$pagesection				 = 	'';
$actAllStatus=$actApplied	=	'';
if($job_status=='applied'){
	$actApplied			  =	'class="active"';
	$getMyJobsSql			=	"SELECT jobs.*,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz,applied.ja_created FROM job_applied AS applied  LEFT JOIN jobs ON applied.job_id = jobs.job_id LEFT JOIN users AS user ON jobs.user_id = user.user_id  LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE applied.ja_applied_by = ".$_SESSION['userId']."  AND jobs.job_status=1  order by applied.ja_created desc";
}else{
	$actAllStatus			=	'class="active"';
	$getMyJobsSql			=	"SELECT job.*,applied.countApplied
									FROM jobs  AS job
									LEFT JOIN (select count(ja_id) AS countApplied,job_id FROM job_applied WHERE ja_view_status=0 GROUP BY job_id) AS applied ON job.job_id = applied.job_id 
									WHERE job.user_id = ".$_SESSION['userId']." AND job.job_status=1 order by job.job_edited desc";
}
pagination($getMyJobsSql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$getMyJobs			   =	$objUsers->listQuery($pg_result);
?>
<link href="<?php echo SITE_ROOT?>css/my-booking.css" rel="stylesheet" type="text/css">
<link href="<?php echo SITE_ROOT?>css/pagination.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.css" />
<script src="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.js"></script>
    <div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="user_profile">
						<?php echo $objCommon->checkEmailverification();?>
						<div class="content">
                        	<div class="row">
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                                    <div class="pagination_box">
                                        <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
                                        <a href="<?php echo SITE_ROOT.$getUserDetails['usl_fameuz']?>"> My profile </a><i class="fa fa-caret-right"></i></a>
                                         <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                        <a href="javascript:;" class="active"> Manage My Jobs  </a>
									   <?php
                                        include_once(DIR_ROOT."widget/notification_head.php");
                                        ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-8 col-lg-sp-3">
                                    <div class="sidebar no-border">
                                        <div class="search_box">
											<form action="<?php echo SITE_ROOT.'user/search-result'?>" method="get">
                                            <input type="text" placeholder="Search"  name="keywordSearch" />
											</form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            	<div class="row">
                                    <div class="col-lg-sp-2">
                            <?php
							include_once(DIR_ROOT."includes/profile_left.php");
							?>
							</div>
                            <div class="col-lg-sp-10">
                            <div class="tab_white_search">
                                <div class="section_search">
                                    <ul>
                                        <li class="visitor_head_box"><h5>Manage My Jobs</h5><span class="point-it"></span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="sidebar">
                            	<div class="clr"></div>
                                <div class="booking">
                                	<?php echo $objCommon->displayMsg();?>
                                    <div class="tab">
                                    	<ul class="c">
                                        	<li <?php echo $actAllStatus?>><a href="<?php echo SITE_ROOT.'user/my-jobs'?>">Posted Jobs</a></li>
											<li <?php echo $actApplied?>><a href="<?php echo SITE_ROOT.'user/my-jobs/applied'?>">Applied Jobs</a></li>
                                        </ul>
                                    </div>
                                    <div class="table">
                                    	<div class="table_hed c">
                                            <div class="hash left17">&nbsp;</div>
                                            <div class="name left39">Job Title</div>
                                            <div class="s_date left12"><i class="fa fa-calendar-o"></i> Start Date</div>
                                            <div class="e_date left12"><i class="fa fa-calendar-o"></i> End Date</div>
                                            <div class="status left12"><i class="fa fa-info"></i> Status</div>
                                            <div class="delete left8">&nbsp;</div>
                                        </div>
										<?php
										 if(count($getMyJobs)>0){
											 $page_id			 	=	($_GET['page_id'])? ($num_results_per_page*($_GET['page_id']-1)):'';
											 foreach($getMyJobs as $keyJobs=>$allJobs){
											 	$siNo				=	$page_id+($keyJobs+1);
												$jobStart			=	($allJobs['job_start'])?date("Y-m-d",strtotime($allJobs['job_start'])):'&nbsp;';
												$jobEnd			  =	($allJobs['job_end'])?date("Y-m-d",strtotime($allJobs['job_end'])):'&nbsp;';
												$job_status	 	  =	$objCommon->html2text($allJobs['job_status']);
												$todayDate		   =	date("Y-m-d");
												$editStatus		  =	1;
												if(($jobStart > $todayDate) && ($job_status==1))
												{
													$modelStatusStr  =	'<span class="squrae upcoming"></span>Upcoming';
													$editStatus	  =	1;
												}else if($job_status==0)
												{
													$modelStatusStr  =	'<span class="squrae expired"></span>Inactive';
												}else if(($jobStart <= $todayDate)  && ($jobEnd >= $todayDate) )
												{
													$modelStatusStr  =	'<span class="squrae completed"></span>Active';
												}else
												{
													$modelStatusStr  =	'<span class="squrae pending"></span>Expired';
												}
										?>
                                        <div class="t_data <?php echo ($activeJobId==$allJobs['job_id'])?'active':''?> <?php echo ($allJobs['countApplied'] >0)?'unread':''?>">
                                        	<div class="data_hed">
                                            	<div class="hash left17">
                                                	<span class="count"><?php echo $siNo?></span>
                                                    <div class="details" data-jobid="<?php echo $objCommon->html2text($allJobs['job_id']);?>">Details <span class="fa fa-angle-down"></span></div>                                              
                                                </div>
                                                <div class="name left39"><?php echo $objCommon->html2text($allJobs['job_title']);?></div>
                                                <div class="s_date left12"><?php echo $jobStart?></div>
                                                <div class="e_date left12"><?php echo $jobEnd?></div>
                                                <div class="status left12"><?php echo $modelStatusStr?></div>
                                                <div class="delete left8">
													<a data-delid="<?php echo $objCommon->html2text($allJobs['job_id'])?>" class="delete_booking" href="javascript:;"><i class="fa fa-trash"></i></a>
													<?php
													if($editStatus==1){
													?>
													<a href="<?php echo SITE_ROOT.'user/post-jobs/edit'.$objCommon->html2text($allJobs['job_id'])?>"><i class="fa fa-pencil"></i></a>
													<?php
													}
													?>
												</div>
                                                <div class="clr"></div>
                                            </div>
                                            <div class="data_content">
                                            	<div class="main_content">
                                                	<h5><?php echo $objCommon->html2text($allJobs['job_title']);?></h5>
													<div class="left100">
														<div class="left70">
                                                    		<p><?php echo $objCommon->html2text($allJobs['job_descr']);?></p>
														</div>
														<div class="left30 descrImage">
														<?php
														if($allJobs['job_image']){
														?>
														<img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/jobs/<?php echo $allJobs['job_image']?>" />
														<?php
														}
														?>
														</div>
													</div>
                                                    <h3><span>Job Details</span></h3>
                                                    <div class="w_details">
														<?php 
														if($allJobs['job_company']){ 
															echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-building"></i> Companies</div><div class="detail_colon">:</div><div class="detail_con">'.$objCommon->html2text($allJobs['job_company']).'</div> </div>'; 
														}
														if($allJobs['job_country']){ 
															echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-map-marker"></i> Country</div><div class="detail_colon">:</div><div class="detail_con">'.$objCommon->html2text($allJobs['job_country']).'</div> </div>'; 
														}
														if($allJobs['job_state']){ 
															echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-map-marker"></i> State</div><div class="detail_colon">:</div><div class="detail_con">'.$objCommon->html2text($allJobs['job_state']).'</div> </div>'; 
														}
														if($allJobs['job_city']){ 
															echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-map-marker"></i> City</div><div class="detail_colon">:</div><div class="detail_con">'.$objCommon->html2text($allJobs['job_city']).'</div> </div>'; 
														}
														if($allJobs['job_skill']){ 
															echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-graduation-cap"></i> Skills</div><div class="detail_colon">:</div><div class="detail_con">'.$objCommon->html2text($allJobs['job_skill']).'</div> </div>'; 
														}
														if($allJobs['job_payment']){ 
															$paymentjob			=	$objCommon->html2text($allJobs['job_payment']);
															$explpaymentjob		=	str_replace("###"," ",$paymentjob);
															echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-usd"></i> Payment</div><div class="detail_colon">:</div><div class="detail_con">'.$explpaymentjob.'</div> </div>'; 
														}
														echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-calendar"></i> Job Created Date</div><div class="detail_colon">:</div><div class="detail_con">'.date("Y-m-d",strtotime($allJobs['job_created'])).'</div> </div>';
														if($_GET['job_status'] == 'applied'){
														echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-user"></i> Job Created By</div><div class="detail_colon">:</div><div class="detail_con"><a class="view" target="_blank" href="'.SITE_ROOT.$objCommon->html2text($allJobs['usl_fameuz']).'">'.$objCommon->displayName($allJobs).'</a></div> </div>';
														echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-calendar"></i> Applied Date</div><div class="detail_colon">:</div><div class="detail_con"><a class="view" target="_blank" href="'.SITE_ROOT.$objCommon->html2text($allJobs['usl_fameuz']).'">'.date("Y-m-d",strtotime($allJobs['ja_created'])).'</a></div> </div>';
														}
														?>
                                                    </div>
                                                    <div class="duration">
                                                    	<h4>Duration</h4>
                                                        <div class="duration_dates">
                                                        	<div class="date round5">
                                                            	<div class="from">From</div>
                                                                <div class="day"><?php echo date('d',strtotime($jobStart));?></div>
                                                                <div class="month"><?php echo date('F Y',strtotime($jobStart));?></div>
                                                            </div>
                                                            <div class="date round5">
                                                            	<div class="from">To</div>
                                                                <div class="day"><?php echo date('d',strtotime($jobEnd));?></div>
                                                                <div class="month"><?php echo date('F Y',strtotime($jobEnd));?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clr"></div>
												<?php
												if($_GET['job_status'] != 'applied'){
													$getJobApplied		=	$objUsers->listQuery("select applied.ja_created,applied.ja_view_status,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz 
																							FROM job_applied AS applied
																							LEFT JOIN users AS user ON applied.ja_applied_by = user.user_id 
																							LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
																							LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
																							LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
																							LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
																							WHERE applied.job_id = ".$allJobs['job_id']." AND  user.status=1 AND user.email_validation=1 ORDER BY applied.ja_created DESC");
													if(count($getJobApplied)>0){
													?>
													<h3 class="applied"><span>Received applications</span></h3>
													<div class="bookReq">
													   <div class="w_details"> 
															<table class="table table-striped">
																<thead>
																	<tr>
																		<th>#</th>
																		<th>Name</th>
																		<th>Email</th>
																		<th>Applied Date</th>
																		<th>View</th>
																	</tr>
																</thead>
																<tbody>
																	<?php
																	foreach($getJobApplied as $keyAllApplied=>$allAppliedStatus){
																	?>
																	<tr <?php echo ($allAppliedStatus['ja_view_status']==0)?'class="bold"':''?>>
																		<th scope="row"><?php echo $keyAllApplied+1?></th>
																		<td><?php echo $objCommon->displayName($allAppliedStatus)?></td>
																		<td><?php echo $objCommon->html2text($allAppliedStatus['email'])?></td>
																		<td><?php echo date("d M Y",strtotime($allAppliedStatus['ja_created']))?></td>
																		<td>
																			<a href="<?php echo SITE_ROOT.$allAppliedStatus['usl_fameuz']?>" class="view" title="View Profile"><i class="fa fa-user"></i></a>&nbsp;&nbsp;
																			<a href="<?php echo SITE_ROOT.'user/'.$allAppliedStatus['usl_fameuz'].'/composite-card'?>" class="view" title="View Composite card"><i class="fa fa-envelope-o"></i></a>&nbsp;&nbsp;
																			<a href="<?php echo SITE_ROOT.'user/'.$allAppliedStatus['usl_fameuz'].'/polaroids'?>" class="view" title="View Polaroids"><i class="fa fa-camera-retro"></i></a>
																		</td>
																	</tr>
																	<?php
																	}
																	?>
																</tbody>
															</table>
													</div>
													</div>
													<?php
													}
													}
													?>
                                                </div>
                                                
                                                <div class="clr"></div>
                                            </div>
                                        </div>
										<?php
										}
										echo '<div class="paginationDiv">'.$pagination_output.'</div>';
										 }else{
											 echo '<p> No Results found... </p>';
										 }
										 ?>
                                    </div>
                                </div>
                            </div>
                            </div>
                            </div> 
                            <div class="clr"></div>                        
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
<?php
if($activeJobId){
	?>
	<script language="javascript" type="application/javascript">
		$.get("<?php echo SITE_ROOT?>ajax/change_job_applied_read_status.php",{"jobid":'<?php echo $activeJobId?>'},function(){
			//change read applied job user list 	
		});
	</script>
	<?php
}
?>
    <script type="text/javascript">
		$(document).ready(function(e) {
			$('.details').click(function(e) {
				var jobid		=	$(this).data("jobid");
				if(jobid !=''){
					$.get("<?php echo SITE_ROOT?>ajax/change_job_applied_read_status.php",{"jobid":jobid},function(){
						//change read applied job user list 	
					});
				}
				var t_data = $(this).parent().parent().parent();
                t_data.toggleClass('active');
				t_data.siblings().removeClass('active');
				t_data.removeClass('unread');
				return false;
            });
			$('.user_profile .sidebar .booking .t_data .data_hed .hash .detail_setting').click(function(e) {
				$(this).find('.setting_option').fadeToggle();
            });
			
			$('.delete_booking').click(function(){
				var delid = $(this).data("delid");
				var elem = $(this).closest('.item');
				var that			=	this;
				$.confirm({
					'title'		: 'Delete Confirmation',
					'message'	: 'You are about to delete this item ?',
					'buttons'	: {
					'Yes'	: {
					'class'	: 'blue',
					'action': function(){
					elem.slideUp();
					window.location.href = '<?php echo SITE_ROOT?>access/delete_job.php?action=del_job&jobId='+delid;
					}
					},
					'No'	: {
					'class'	: 'gray',
					'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
					}
					}
				});
			
			});
        });
	</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  