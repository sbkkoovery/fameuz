<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/is_session.php");
include_once(DIR_ROOT."class/main_category.php");
$objMainCategory				=	new main_category();
$getMainCategoryAll			 =	$objMainCategory->getAll();
$mainCat						=	$objCommon->esc($_GET['main-cat']);								
?>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.validate.js"></script>
<link href="<?php echo SITE_ROOT?>css/cmxform.css" rel="stylesheet" type="text/css" />
    <div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="register_heading">
                        	<h1>Join the world's largest Fashion models network.</h1>
                            <p>Professional Model, New face, Photographers, Agency, Hair and makeup, Artist, Fashion stylist, Spectator, Industry Professional</p>
                        </div>
                        <div class="register_content">
                        	<?php 
							include_once(DIR_ROOT."widget/register_user_images.php");
							?>
                        	<div class="register_box">
                            	<div class="register_form_box" id="register_form_box">
                                    <h1>Change your password</h1>
                                    <h6>Let’s find your account</h6>
									<p>First, help us identify you by providing the email address you used for your famuez.com account.</p>
                                    <?php
									echo $objCommon->displayMsg();
									?>
                                    <form action="<?php echo SITE_ROOT?>access/forgot_password_request.php" method="post" id="register_home_frm" role="form">
                                    <div class="register_form">
                                        <div class="tr form-group">
                                            <div class="icon"><img src="<?php echo SITE_ROOT?>images/register_icon2.png" alt="register_icon2" /></div>
                                            <div class="field"><input type="text" placeholder="Your email address" name="forgot_email" id="forgot_email"   /></div>
                                            <div class="clr"></div>
                                        </div>
                                        
                                        <div class="tr submit">
                                            <input type="submit" value="Continue" name="register_home" />
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script language="javascript" type="text/javascript">
$(document).ready(function(e){
	$("#register_home_frm").validate({
			rules: {
				forgot_email:{required:true,email: true}
			},
			messages: {
				forgot_email: {required:'Can\'t be empty',email:'Please enter valid email address'}
			}

    });
	var reg_main_cat			=	$("#reg_main_cat").val();
	if(reg_main_cat){
		get_category_fun(reg_main_cat);
	}
	$("#reg_main_cat").change(function(){
		var reg_main_cat_ch	=	$(this).val();
		if(reg_main_cat_ch){
			get_category_fun(reg_main_cat_ch);
		}
	});
});
</script>
<?php

include_once(DIR_ROOT."includes/login_box.php");
include_once(DIR_ROOT."includes/footer.php");
?>