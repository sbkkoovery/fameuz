<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/pagination-class-front.php");
$book_status				 =	$objCommon->esc($_GET['book_status']);
$num_results_per_page		= 	20;
$num_page_links_per_page 	 = 	5;
$pg_param 					= 	"";
$pagesection				 = 	'';
$show						=	$objCommon->esc($_GET['show']);
$actAllStatus=$actApplied	=	'';
if($book_status =='sent-request'){
	$actApplied			  =	'class="active"';
	$getMyJobsSql			=	"SELECT mp.*,mpc.mpc_name,booked.mpb_date,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM market_place_booked AS booked LEFT JOIN market_place AS mp ON booked.mp_id = mp.mp_id LEFT JOIN market_place_category AS mpc ON mp.mp_type =  mpc.mpc_id  LEFT JOIN users AS user ON mp.user_id = user.user_id  LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE booked.user_id = ".$_SESSION['userId']." order by booked.mpb_date desc";
}else{
	$actAllStatus			=	'class="active"';
	$getMyJobsSql			=	"SELECT mp.*,mpc.mpc_name FROM market_place AS mp LEFT JOIN market_place_category AS mpc ON mp.mp_type =  mpc.mpc_id WHERE mp.user_id = ".$_SESSION['userId']." order by mp.mp_created_date desc";
}
pagination($getMyJobsSql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$getMyJobs			   	   =	$objUsers->listQuery($pg_result);
?>
<link href="<?php echo SITE_ROOT?>css/my-booking.css" rel="stylesheet" type="text/css">
<link href="<?php echo SITE_ROOT?>css/pagination.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.css" />
<script src="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.js"></script>
    <div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="user_profile">
						<?php echo $objCommon->checkEmailverification();?>
						<div class="content">
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                                    <div class="pagination_box">
                                        <a href="#">Home <i class="fa fa-caret-right"></i></a>
                                         <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                        <a href="#" class="active">My Market Place  </a>
									   <?php
                                        include_once(DIR_ROOT."widget/notification_head.php");
                                        ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-8 col-lg-sp-3">
                                    <div class="sidebar no-border">
                                        <div class="search_box">
											<form action="<?php echo SITE_ROOT.'user/search-result'?>" method="get">
                                            <input type="text" placeholder="Search"  name="keywordSearch" />
											</form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                  <div class="row">
                                    <div class="col-lg-sp-2">
                            <?php
							include_once(DIR_ROOT."includes/profile_left.php");
							?>
							</div>
                            <div class="col-lg-sp-10">
                            <div class="tab_white_search">
                                <div class="section_search">
                                    <ul>
                                        <li class="visitor_head_box"><h5>My Market Place</h5><span class="point-it"></span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="sidebar">
                                <div class="clr"></div>
                                <div class="booking">
                                	<?php echo $objCommon->displayMsg();?>
                                    <div class="tab">
                                    	<ul class="c">
                                        	<li <?php echo $actAllStatus?>><a href="<?php echo SITE_ROOT?>user/my-market-place">Posted ads</a></li>
											<li <?php echo $actApplied?>><a href="<?php echo SITE_ROOT?>user/my-market-place/sent-request">Sent requests</a></li>
                                        </ul>
                                    </div>
                                    <div class="table">
                                    	<div class="table_hed c">
                                            <div class="hash left17">&nbsp;</div>
                                            <div class="name left39">Title</div>
                                            <div class="s_date left12"><i class="fa fa-calendar-o"></i> Start Date</div>
                                            <div class="e_date left12"><i class="fa fa-calendar-o"></i> End Date</div>
                                            <div class="status left12"><i class="fa fa-info"></i> Status</div>
                                            <div class="delete left8">&nbsp;</div>
                                        </div>
										<?php
										 if(count($getMyJobs)>0){
											 $page_id			 	=	($_GET['page_id'])? ($num_results_per_page*($_GET['page_id']-1)):'';
											 foreach($getMyJobs as $keyJobs=>$allJobs){
											 	$siNo				=	$page_id+($keyJobs+1);
												$jobStart			=	($allJobs['mp_show_from'])?date("Y-m-d",strtotime($allJobs['mp_show_from'])):'&nbsp;';
												$jobEnd			  =	($allJobs['mp_show_to'])?date("Y-m-d",strtotime($allJobs['mp_show_to'])):'&nbsp;';
												$mp_status	 	   =	$objCommon->html2text($allJobs['mp_status']);
												$todayDate		   =	date("Y-m-d");
												$editStatus		  =	1;
												if(($jobStart > $todayDate) && ($mp_status==1))
												{
													$modelStatusStr  =	'<span class="squrae upcoming"></span>Upcoming';
												}else if($mp_status==0)
												{
													$modelStatusStr  =	'<span class="squrae expired"></span>Inactive';
												}else if(($jobStart <= $todayDate)  && ($jobEnd >= $todayDate) )
												{
													$modelStatusStr  =	'<span class="squrae completed"></span>Active';
												}else
												{
													$modelStatusStr  =	'<span class="squrae pending"></span>Expired';
												}
										?>
                                        <div class="t_data show<?php echo $objCommon->html2text($allJobs['mp_id']);?>">
                                        	<div class="data_hed">
                                            	<div class="hash left17">
                                                	<span class="count"><?php echo $siNo?></span>
                                                    <div class="details " data-booking="<?php echo $objCommon->html2text($allJobs['mp_id']);?>"  data-read="<?php echo $objCommon->html2text($allJobs['bm_read_status']);?>">Details <span class="fa fa-angle-down"></span></div>                                              
                                                </div>
                                                <div class="name left39"><?php echo $objCommon->html2text($allJobs['mp_title']);?></div>
                                                <div class="s_date left12"><?php echo $jobStart?></div>
                                                <div class="e_date left12"><?php echo $jobEnd?></div>
                                                <div class="status left12"><?php echo $modelStatusStr?></div>
                                                <div class="delete left8">
													<a data-delid="<?php echo $objCommon->html2text($allJobs['mp_id'])?>" class="delete_booking" href="javascript:;"><i class="fa fa-trash"></i></a>
													<?php
													if($editStatus==1){
													?>
													<a href="<?php echo SITE_ROOT.'user/post-market-place/edit'.$objCommon->html2text($allJobs['mp_id'])?>"><i class="fa fa-pencil"></i></a>
													<?php
													}
													?>
												</div>
                                                <div class="clr"></div>
                                            </div>
                                            <div class="data_content">
                                            	<div class="main_content">
                                                	<h5><?php echo $objCommon->html2text($allJobs['mp_title']);?></h5>
													<div class="left100">
														<div class="left70">
                                                    		<p><?php echo $objCommon->html2text($allJobs['mp_description']);?></p>
														</div>
														<div class="left30 descrImage">
														<?php
														if($allJobs['mp_image']){
														?>
														<img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/market_place/<?php echo $allJobs['mp_image']?>" />
														<?php
														}
														?>
														</div>
													</div>
                                                    <h3><span>Details</span></h3>
                                                    <div class="w_details">
														<?php 
														if($allJobs['mpc_name']){ 
															echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-building"></i> Type</div><div class="detail_colon">:</div><div class="detail_con">'.$objCommon->html2text($allJobs['mpc_name']).'</div> </div>'; 
														}
														if($allJobs['mp_locations']){ 
															echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-map-marker"></i> Locations</div><div class="detail_colon">:</div><div class="detail_con">'.$objCommon->html2text($allJobs['mp_locations']).'</div> </div>'; 
														}
														if($allJobs['mp_duration']){ 
															echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-clock-o"></i> Duration</div><div class="detail_colon">:</div><div class="detail_con">'.$objCommon->html2text($allJobs['mp_duration']).'</div> </div>'; 
														}
														if($allJobs['mp_actual_price']){ 
															echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-money"></i> Actual Price</div><div class="detail_colon">:</div><div class="detail_con">'.$objCommon->html2text(str_replace('###',' ',$allJobs['mp_actual_price'])).'</div> </div>'; 
														}
														if($allJobs['mp_dis_price']){ 
															echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-money"></i> Discount Price</div><div class="detail_colon">:</div><div class="detail_con">'.$objCommon->html2text(str_replace('###',' ',$allJobs['mp_dis_price'])).'</div> </div>'; 
														}
														echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-calendar"></i> Created Date</div><div class="detail_colon">:</div><div class="detail_con">'.date("Y-m-d",strtotime($allJobs['mp_created_date'])).'</div> </div>';
														if($book_status =='sent-request'){
															echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-user"></i> Job Created By</div><div class="detail_colon">:</div><div class="detail_con"><a class="view" target="_blank" href="'.SITE_ROOT.$objCommon->html2text($allJobs['usl_fameuz']).'">'.$objCommon->displayName($allJobs).'</a></div> </div>';
														echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-calendar"></i> Applied Date</div><div class="detail_colon">:</div><div class="detail_con">'.date("Y-m-d",strtotime($allJobs['mpb_date'])).'</div> </div>';
														}
														?>
                                                    </div>
													
                                                    <div class="duration">
                                                    	<h4>Show Ad Duration</h4>
                                                        <div class="duration_dates">
                                                        	<div class="date round5">
                                                            	<div class="from">From</div>
                                                                <div class="day"><?php echo date('d',strtotime($jobStart));?></div>
                                                                <div class="month"><?php echo date('F Y',strtotime($jobStart));?></div>
                                                            </div>
                                                            <div class="date round5">
                                                            	<div class="from">To</div>
                                                                <div class="day"><?php echo date('d',strtotime($jobEnd));?></div>
                                                                <div class="month"><?php echo date('F Y',strtotime($jobEnd));?></div>
                                                            </div>
                                                        </div>
                                                    </div>
													
                                                    <div class="clr"></div>
													<?php
													if($book_status !='sent-request'){
													$getBookStatus		=	$objUsers->listQuery("select mpb.mpb_id,mpb.mpb_date,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz 
																							FROM market_place_booked AS mpb
																							LEFT JOIN users AS user ON mpb.user_id = user.user_id 
																							LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
																							LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
																							LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
																							LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
																							WHERE mpb.mp_id = ".$allJobs['mp_id']." AND  user.status=1 AND user.email_validation=1");
													if(count($getBookStatus)>0){
													?>
													<h3 class="applied"><span>Received requests</span></h3>
													<div class="bookReq">
													   <div class="w_details"> 
															<table class="table table-striped">
																<thead>
																	<tr>
																		<th>#</th>
																		<th>Name</th>
																		<th>Email</th>
																		<th>Request Date</th>
																		<th>&nbsp;</th>
																	</tr>
																</thead>
																<tbody>
																	<?php
																	foreach($getBookStatus as $keyAllBook=>$allBookStatus){
																	?>
																	<tr>
																		<th scope="row"><?php echo $keyAllBook+1?></th>
																		<td><a href="<?php echo SITE_ROOT.$allBookStatus['usl_fameuz']?>" class="view"><?php echo $objCommon->displayName($allBookStatus)?></a></td>
																		<td><?php echo $objCommon->html2text($allBookStatus['email'])?></td>
																		<td><?php echo date("d M Y",strtotime($allBookStatus['mpb_date']))?></td>
																		<td><a href="javascript:;" data-toggle="modal" data-target=".requestAdd" data-marketplace="<?php echo $allBookStatus['mpb_id']?>" class="view viewBookedRequest">View</a></td>
																	</tr>
																	<?php
																	}
																	?>
																</tbody>
															</table>
													</div>
													</div>
													<?php
													}
													}
													?>
                                                </div>
                                                
                                                <div class="clr"></div>
                                            </div>
                                        </div>
										<?php
										}
										echo '<div class="paginationDiv">'.$pagination_output.'</div>';
										 }else{
											 echo '<p> No Results found... </p>';
										 }
										 ?>
                                    </div>
                                </div>
                            </div>
                            </div>
                            </div> 
                            <div class="clr"></div>                        
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
		$(document).ready(function(e) {
			var	showDiv		=	'<?php echo $show?>';
			if(showDiv){
				$(".show"+showDiv).toggleClass('active');
			}
			$('.details').click(function(e) {
			var t_data = $(this).parent().parent().parent();
			t_data.toggleClass('active');
			t_data.siblings().removeClass('active');
			return false;
            });
			$('.user_profile .sidebar .booking .t_data .data_hed .hash .detail_setting').click(function(e) {
				$(this).find('.setting_option').fadeToggle();
            });
			
			$('.delete_booking').click(function(){
				var delid = $(this).data("delid");
				var elem = $(this).closest('.item');
				var that			=	this;
				$.confirm({
					'title'		: 'Delete Confirmation',
					'message'	: 'You are about to delete this item ?',
					'buttons'	: {
					'Yes'	: {
					'class'	: 'blue',
					'action': function(){
					elem.slideUp();
					window.location.href = '<?php echo SITE_ROOT?>access/delete_market_place.php?action=del_job&jobId='+delid;
					}
					},
					'No'	: {
					'class'	: 'gray',
					'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
					}
					}
				});
			
			});
        });
		$(".viewBookedRequest").on("click",function(){
			var marketplace		=	$(this).data("marketplace");
			if(marketplace){
				$(".load_Market_book_request").load('<?php echo SITE_ROOT?>ajax/show_my_market_request.php?marketplace='+marketplace);
			}
		});
	</script>
<!--------modal request message------>
<div class="modal fade requestAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content request-back">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Booked Request</h4>
      </div>
      <div class="modal-body">
	  	<div class="load_Market_book_request"></div>
      </div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div>
<!--------End modal request message------>	
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  