<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."class/videos.php");
$objVideos				=	new videos();
$vidId					=	$objCommon->esc($_GET['id']);
$getVideoDetails		  =	$objVideos->getRowSql("SELECT vid.video_id,vid.video_url,video_encr_id,vid.video_thumb,vid.video_duration,vid.video_type,vid.video_title,vid.video_visits,vid.video_created,vid.video_privacy,user.first_name,user.user_id,user.last_name,user.display_name,user.email,social.usl_fameuz,cat.c_name,profileImg.upi_img_url
										FROM videos AS vid
										LEFT JOIN users AS user ON  vid.user_id = user.user_id
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
										LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
										WHERE vid.video_status=1 AND user.status=1 AND user.email_validation =1 AND vid.video_encr_id='".$vidId."'");
if($getVideoDetails['video_url'] ==''){
	header("location:".SITE_ROOT.'videos');
}
$getFriendStatus		   =	$objVideos->getRowSql("select * from following where (follow_user1=".$getUserDetails['user_id']." OR follow_user2=".$getUserDetails['user_id'].") AND (follow_user1=".$getVideoDetails['user_id']." OR follow_user2=".$getVideoDetails['user_id'].")");
if(($getFriendStatus['follow_status']==1 && $getFriendStatus['follow_user1']==$_SESSION['userId']) || $getFriendStatus['follow_status']==2){ 
	$friendStatus =1; 
}else { 
	$friendStatus=0; 
}
?>
<link href="<?php echo SITE_ROOT?>css/rating.css" rel="stylesheet" type="text/css">
<script src="<?php echo SITE_ROOT?>jw_player/jwplayer.js"></script>
<link href="<?php echo SITE_ROOT?>jw_player/jw_player.css" rel="stylesheet" type="text/css">
<div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="user_profile">
                            <div class="row">
                            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                            	<div class="upper-video-sec">
                                	<?php
									include_once(DIR_ROOT."widget/video_serach_widget.php");
									?>
                                    <div class="col-sm-1">
                                     <?php
									  if(isset($_SESSION['userId'])){
									include_once(DIR_ROOT."widget/notification_head.php");
									  }
								?>
                                </div>
                                </div>
                                    </div>
                                 </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3">
                            	     <div class="sidebar no-border">
                            	<div class="search_box"><form action="<?php echo SITE_ROOT.'user/search-result'?>" method="get"><input type="text" placeholder="Search"  name="keywordSearch" /></form></div>
                            </div>
                            </div> 
                            </div> 
                            <div class="videos">
                            <div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-sp-9 no-padding">
								<?php
								if((($getVideoDetails['video_privacy']=='0,0,1,0' || $getVideoDetails['video_privacy']=='0,0,0,1') && $getVideoDetails['user_id'] != $_SESSION['userId'] ) || ($getVideoDetails['video_privacy']=='0,1,0,0' && $friendStatus ==0 && $getVideoDetails['user_id'] != $_SESSION['userId']) ){
									echo '<div class="video_left_sing left100">';
									echo "oops ! sorry , this video hasn't shared with you.";
									echo '</div>';
									
								}else{
									if($getVideoDetails['video_type']==1){
										$getAiImages				=	SITE_ROOT_AWS_VIDEOS.'uploads/videos/'.$getVideoDetails['video_thumb'];
										$vidUrl					 =	SITE_ROOT.'uploads/videos/'.$getVideoDetails['video_url'];
									}else if($getVideoDetails['video_type']==2){
										if (preg_match('%^https?://[^\s]+$%',$getVideoDetails['video_thumb'])) {
											$getAiImages			=	$getVideoDetails['video_thumb'];
										} else {
											$getAiImages			=	SITE_ROOT_AWS_VIDEOS.'uploads/videos/'.$getVideoDetails['video_thumb'];
										}
										$vidUrl					 =	$objCommon->html2text($getVideoDetails['video_url']);
									}
									$displayNameFriend			  =	$objCommon->displayName($getVideoDetails);
									if($_SESSION['userId']){
										include_once(DIR_ROOT."class/videos_views.php");
										include_once(DIR_ROOT."class/videos_vote.php");
										$objVideoViews			  =	new videos_views();
										$objVideoVote			   =	new videos_vote();
										$getVideoVote			   =	$objVideoVote->getRowSql("select group_concat(user_id) as user_ids,avg(vv_vote) as avg_rate from videos_vote where video_id =".$getVideoDetails['video_id']);
										$getVideoViewCount		  =	$objVideoViews->count("user_id=".$_SESSION['userId']." AND video_id=".$getVideoDetails['video_id']);
										if($getVideoViewCount==0){
											$_POST['video_id']	  =	$getVideoDetails['video_id'];
											$_POST['user_id']	   =	$_SESSION['userId'];
											$_POST['vvs_visit']	 =	1;
											$_POST['vvs_created']   =	date('Y-m-d H:i:s');
											$objVideoViews->insert($_POST);
											$objVideos->build_result_insert("UPDATE videos  SET video_visits = video_visits+1 WHERE video_id=".$getVideoDetails['video_id']);
										}else{
											$vvs_created_update	 =	date('Y-m-d H:i:s');
											$objVideoViews->updateField(array("vvs_created"=>$vvs_created_update),"video_id=".$getVideoDetails['video_id']." AND user_id=".$_SESSION['userId']);
										}
									}
								?>
								
									<div class="video_left_sing pull-left">
										<div class="video_left video_left_single">
										<div class="video" id="thePlayer">
										</div>
										<script type="text/javascript">
											jwplayer("thePlayer").setup({
												flashplayer: "player.swf",
												 image: "<?php echo $getAiImages?>",
												file: "<?php echo $vidUrl?>",
												skin: "<?php echo SITE_ROOT?>jw_player/six/six.xml",
												height: "450",
												width: "100%",
												autostart: true,
												stretching:"exactfit"
											});
										</script>
										<div class="video_desc">
											<div class="desc_sec">
												<div class="user">
													<div class="thump"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($getVideoDetails['upi_img_url'])?$getVideoDetails['upi_img_url']:'profile_pic.jpg'?>" alt="thump"></div>
													<div class="user_info">
														<h5><a href="<?php echo SITE_ROOT.$objCommon->html2text($getVideoDetails['usl_fameuz'])?>"><?php echo $displayNameFriend?></a></h5>
														<span class="proffesion"><?php echo $objCommon->html2text($getVideoDetails['c_name'])?></span>
														<span class="star_rate"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/star_rate.png" alt="star" /></span>
													</div>
												</div>
												<?php
											$getLikeCount	 =	$objVideos->getRowSql("SELECT count(like_id) AS likeCount,group_concat(like_user_id) AS youLike FROM likes WHERE like_content =".$getVideoDetails['video_id']." AND like_cat =4 AND like_status=1");
											$likeCount		=	$getLikeCount['likeCount'];
													$youLikeArr	   =	explode(",",$getLikeCount['youLike']);
													if(count($youLikeArr)>0){
														if(in_array($_SESSION['userId'],$youLikeArr)){
															$youLike	  =	1;
														}else{
															$youLike	  =	0;
														}
													}
													if($youLike ==1 && $likeCount	==1){
														$likeStr	  =	'You like this.';
													}else if($youLike ==1 && $likeCount>1){
														$likeStr	  =	'You and other '.($likeCount-1).' people like this.';
													}else if($youLike ==0 && $likeCount>0){
														$likeStr	  =	$likeCount.' people like this.';
													}else{
														$likeStr	  =	'0';
													}
											 $getShareCount		   =	$objVideos->getRowSql("SELECT count(share_id) AS shareCount,group_concat(user_by) AS youShare FROM share WHERE share_content =".$getVideoDetails['video_id']." AND share_category =3 AND share_status=1");
											 $shareCount			  =	$getShareCount['shareCount'];
											 $youShareArr	   		 =	explode(",",$getShareCount['youShare']);
											 if(count($youShareArr)>0){
												if(in_array($_SESSION['userId'],$youShareArr)){
													$youShare	  =	1;
												}else{
													$youShare	  =	0;
												}
											 }
												?>
												<div class="video_info">
													<div class="published_date"><i class="fa fa-globe"></i> Published on : <?php echo date("F d , Y",strtotime($getVideoDetails['video_created']));?></div>
													<h1><?php echo $objCommon->html2text($getVideoDetails['video_title'])?></h1>
													<div class="row views">
														<div class="col-sm-5 likeStrCount"><i class="fa fa-heart"></i> <?php echo $likeStr;?></div>
														<div class="col-sm-2 shareStrCount"><i class="fa fa-share-alt"></i> <?php echo $shareCount?></div>
														<div class="col-sm-3 rateStrCount">
															<?php
															$vidRateCound			=	$objCommon->round_to_nearest_half($getVideoVote['avg_rate']);
															$vidRateCoundStyle	   =	'style="width:'.($vidRateCound*20).'%;"';
															?>
															<div class="rating_box"><div class="rating_yellow" <?php echo $vidRateCoundStyle?>></div></div> &nbsp;<?php echo $vidRateCound?>
														</div>
														<div class="col-sm-2 a_right"> <i class="fa fa-eye"></i> <span class="vidVisits"><?php echo number_format($getVideoDetails['video_visits']);?></span></div>
													</div>
												</div>
												<div class="clr"></div>
											</div>
											<?php
											if($_SESSION['userId']){
											?>
											<div class="share">
												<div class="share_coment">
													<div class="likeVideoBtn"><a href="javascript:;" class="share_heart1 likeVideo <?php echo ($youLike==1)?'liked':''?>" data-like="<?php echo $getVideoDetails['video_id']?>"><i class="fa fa-heart"></i><?php echo ($youLike==1)?'Liked':'Like'?></a></div>
													<div class="shareVideoBtn"><a href="javascript:;" class="share_heart2 shareVideo <?php echo ($youShare==1)?'shared':''?>" data-share="<?php echo $getVideoDetails['video_id']?>"><i class="fa fa-share-alt"></i><?php echo ($youShare==1)?'Shared':'Share'?></a></div>
													<?php
													$votedUserArr				=	explode(",",$getVideoVote['user_ids']);
													if(in_array($_SESSION['userId'],$votedUserArr)){
													?>
													<a href="javascript:;" id="rateVideo" class="share_heart3 voted"><i class="fa fa-thumbs-up"></i>Voted</a>
													<?php
													}else{
													?>
													 <a href="#rate-video" id="rateVideo" class="share_heart3"><i class="fa fa-thumbs-up"></i>Vote</a>
													<?php
													}
													?>
												</div>
												<div class="share_icns">
													<div class="share_icon_content">
														<div class="share_social">
															<a href="#"><i class="fa fa-facebook"></i></a>
															<a href="#"><i class="fa fa-twitter"></i></a>
															<a href="#"><i class="fa fa-google-plus"></i></a>
															<a href="#"><i class="fa fa-pinterest-p"></i></a>
														</div>
														<div class="round_section">
															<a href="#"><i class="fa fa-circle"></i></a>
															<a href="#"><i class="fa fa-circle"></i></a>
															<a href="#"><i class="fa fa-circle"></i></a>
														</div>
														<div class="share_last">
															<a href="#"><i class="fa fa-flag-o"></i></a>
														</div>
														<div class="clr"></div>
													</div>
												</div>
												<div class="clearfix"></div>
											</div>
											<?php
											}
											?>
										</div>
									</div>
									</div>
									<div class="clr"></div>
									<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery.form.js"></script>
									<script src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery.elastic.source.js" type="text/javascript" charset="utf-8"></script>
									<div id="preview"></div>
									<div class="video_comment_related">
									<div class="row">
									<?php
									if($_SESSION['userId']){
									?>
										
                                        <div class="col-sm-7 no-padding bg_white">
											<div class="comment_box_outer">
												<div class="comment_head"><h1>All Comments </h1></div>
												<div class="working_comments">
													<div class="thumb"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($getUserDetails['upi_img_url'])?$getUserDetails['upi_img_url']:'profile_pic.jpg'?>" alt="thumb"></div>
													<div class="comments">
														<div class="comments_arrow"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/comments_arrow.png" alt="comments_arrow"></div>
														<form id="comment_form" method="post" action="<?php echo SITE_ROOT?>access/post_video_comment.php?action=add_comment&video_id=<?php echo $getVideoDetails['video_encr_id']?>&userto=<?php echo $getVideoDetails['user_id']?>">
															<textarea placeholder="Your comment...."  name="comment_descr_input" class="comment_descr_input1" rows="1"></textarea>
															<div class="ajaxloader text-center"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/ajax-loader.gif" alt="comments_arrow"></div> 
														</form>
														
														<div class="clr"></div>
													</div>
												</div>
												<div class="loadCommentBox"></div>
											</div>
										</div>
										 <div class="col-sm-5 no-padding">
											<div class="loadRelatedVideos">
												
											</div>
										</div>
											<?php
											}
											?>
										
                                        </div>
									</div>
									<?php
									}
									?>
								</div>
									<div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3 hidden-sm hidden-md">
										<?php include_once(DIR_ROOT.'widget/ad/right_ad_two_block_video.php');?>
									</div>
                                    </div>
									
                            </div>
                            <div class="clr"></div>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	 <!---------------------------------------->
    <div class="remodal popup_box" data-remodal-id="rate-video">
       <div class="video_rate_table login_table">
		<div>
		Rate this video
		</div>
       	<div class="tr">
			<div class="rate-ex2-cnt">
				<div id="1" class="rate-btn-1 rate-btn"></div>
				<div id="2" class="rate-btn-2 rate-btn"></div>
				<div id="3" class="rate-btn-3 rate-btn"></div>
				<div id="4" class="rate-btn-4 rate-btn"></div>
				<div id="5" class="rate-btn-5 rate-btn"></div>
				<input type="text" name="rate_val" id="rate_val" style="display:none;" />
			</div><input type="button" value="Rate" class="rate_submit">
		</div>
		<div class="tr submit">
			
		</div> 
       </div>
    </div>
    <!---------------------------------------->
<?php
include_once(DIR_ROOT."js/include/videos_single_js.php");
include_once(DIR_ROOT."includes/footer.php");
?>
