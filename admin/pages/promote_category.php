<?php
include_once("../class/promotion_based_category.php");
include_once("../class/common_class.php");
$objCommon		 		   =	new common();
$objpromocat	=	new promotion_based_category();
$getRowDetails	   =	$objpromocat->getRow("pbc_id=1");
?>
<div class="page-heading">
	<h3>Promotion Based on Categories</h3>
	<ul class="breadcrumb">
		<li><a href="#">Promotion</a></li>
		<li class="active"> Promotion Based on Categories </li>
	</ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
           <div class="col-lg-8">
                <section class="panel">
                    <header class="panel-heading">Promotion packages based on categories of users</header>
                    <div class="panel-body">
                    <form role="form" id="promote_loc" method="post" action="access/add-promo-cat.php">
                    	<div class="table-responsive">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="25%"></th>
                                        <th width="25%">Profile Promotion</th>
                                        <th width="25%">Video Promotion</th>
                                        <th width="25%">Music Promotion</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Cost per one category</td>
                                         <td> <input type="text" name="prom_pro" id="prom_pro" class="form-control"  value="<?php echo ($getRowDetails['pbc_profile'])?$objCommon->html2text($getRowDetails['pbc_profile']):''?>" required ></td>
                                        <td> <input type="text" name="prom_vid" id="prom_vid" class="form-control"  value="<?php echo ($getRowDetails['pbc_video'])?$objCommon->html2text($getRowDetails['pbc_video']):''?>" required ></td>
                                        <td> <input type="text" name="prom_mus" id="prom_mus" class="form-control"  value="<?php echo ($getRowDetails['pbc_music'])?$objCommon->html2text($getRowDetails['pbc_music']):''?>" required ></td>            	
                                    </tr>
                                </tbody>
                            </table>
                             <button type="submit" class="btn btn-primary">Update</button>
                            </form>
           </div>
                    </div>
                </section>

            </div>
        </div>
<script language="javascript" type="application/javascript">

</script>