<?php
include_once("../class/pages.php");
include_once("../class/common_class.php");
$pages			  		   =	new pages();
$objCommon		 		   =	new common();
$nId			  			 =	$objCommon->esc($_GET['nId']);
if($nId){
		$getRowDetails	   =	$pages->getRow("page_id=".$nId);
}
?>
<link rel="stylesheet" type="text/css" href="js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
<script type="text/javascript" src="js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<div class="page-heading">
	<h3>Pages</h3>
	<ul class="breadcrumb">
		<li><a href="#">Pages</a></li>
		<li class="active"> Add Pages </li>
	</ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">Add Pages</header>
                    <div class="panel-body">
                        <form role="form" id="add_collar" method="post" action="access/add-pages.php">
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" name="page_title" id="page_title" class="form-control"  value="<?php echo ($getRowDetails['page_title'])?$objCommon->html2text($getRowDetails['page_title']):''?>" placeholder="Enter Title" required >
                            </div>							 
							<div class="form-group">
                                <label>Description</label>
                                <textarea class="wysihtml5 form-control" rows="9" name="page_descr"><?php echo $objCommon->html2text($getRowDetails['page_descr']);?></textarea>
                            </div>
							<div class="form-group">
                                <label>Status</label>
                                <select name="page_status">
									<option value="1" <?php echo ($getRowDetails['page_status']==1)?'selected="selected"':''?>>Enable</option>
									<option value="0" <?php echo ($getRowDetails['page_status']==0)?'selected="selected"':''?>>Disable</option>
								</select>
                            </div>
                            <input type="hidden" name="editId" value="<?php echo $nId?>" />
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </section>
            </div>
            
        </div>
<script language="javascript" type="application/javascript">

jQuery(document).ready(function(){
         $('.wysihtml5').wysihtml5();
    });
</script>