<?php
include_once("../class/sub_category.php");
include_once("../class/category.php");
include_once("../class/common_class.php");
$objSubCategory			  =	new sub_category();
$objCategory			 	 =	new category();
$objCommon		 		   =	new common();
$nId			  			 =	$objCommon->esc($_GET['nId']);
$dId			   			 =	$objCommon->esc($_GET['dId']);
$search					  =	$objCommon->esc($_GET['search']);
$cat_search				  =	$objCommon->esc($_GET['cat_search']);
$getMainCategory			 =	$objCategory->getAll("","c_name");
if($nId){
		$getRowDetails	   =	$objSubCategory->getRow("s_id=".$nId);
}
if($dId){
		$objSubCategory->delete("s_id=".$dId);
		$objCommon->addMsg("Selected item has been deleted successfully.",1);
		header("location:".$_SERVER['HTTP_REFERER']);
		exit;
}
$sql						 .= "SELECT sub.*,cat.c_name FROM sub_category AS sub LEFT JOIN category AS cat ON sub.c_id=cat.c_id WHERE 1 ";
if($search){
	$sql					.= " AND (s_name LIKE '%".$search."%' OR s_id LIKE '%".$search."%')";	
}
if($cat_search){
	$sql					.= " AND (cat.c_id = ".$cat_search." )";
}
$sql						 .= " ORDER by s_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objSubCategory->listQuery($paginationQuery);
?>
<div class="page-heading">
	<h3>Sub Category</h3>
	<ul class="breadcrumb">
		<li><a href="#">Categories</a></li>
		<li class="active">Sub Category </li>
	</ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
            <div class="col-lg-4">
                <section class="panel">
                    <header class="panel-heading">Add Sub Category</header>
                    <div class="panel-body">
                        <form role="form" id="add_sub_category" method="post" action="access/add-sub-category.php">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Category Title</label>
                                <input type="text" name="s_name" id="s_name" class="form-control" id="exampleInputEmail1" value="<?php echo ($getRowDetails['s_name'])?$objCommon->html2text($getRowDetails['s_name']):''?>" placeholder="Enter Title" required >
                            </div>
                             <div class="form-group">
                                <label for="exampleInputEmail1">Category</label>
                                <select name="c_id" id="c_id" class="form-control"  required >
                                	<option value="">Select Category</option>
                                    <?php
									foreach($getMainCategory as $allMainCategory){
									?>
                                    <option value="<?php echo $allMainCategory['c_id']?>" <?php echo ($getRowDetails['c_id']==$allMainCategory['c_id'])?'selected':'';?>><?php echo $objCommon->html2text($allMainCategory['c_name'])?></option>
                                    <?php
									}
									?>
                                </select>
                     
                            </div>
                            <input type="hidden" name="editId" value="<?php echo $nId?>" />
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </section>
            </div>
            <div class="col-lg-8">
                <section class="panel">
                    <header class="panel-heading">Sub Category List</header>
                    <div class="panel-body">
                    	<div class="row">
                        	<div class="col-md-6 col-md-offset-6 ">
                                <form class="form-search pull-right" method="get" action="">
                                <select name="cat_search" class="select_large">
                                	<option value="">Category</option>
                                     <?php
									foreach($getMainCategory as $allMainCategorySearch){
									?>
                                    <option value="<?php echo $allMainCategorySearch['c_id']?>" <?php echo ($cat_search==$allMainCategorySearch['c_id'])?'selected':'';?>><?php echo $objCommon->html2text($allMainCategorySearch['c_name'])?></option>
                                    <?php
									}
									?>
                                </select>
                                	<input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                                    <input type="hidden" name="page" value="sub-category" />
                               		 <button class="btn btn-primary search_submit" type="submit">Search</button>
                                </form>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="50%">Sub Category Name</th>
                                        <th width="20%">Category</th>
                                        <th width="10%">ID</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count($contentList)>0){
                                    $i=1;
                                    foreach($contentList as $list){?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $objCommon->html2text($list['s_name']); ?></td></td>
                                         <td><?php echo $objCommon->html2text($list['c_name']); ?></td></td>
                                         <td><?php echo $list['c_id']; ?></td></td>
                                        
                                        <td>
                                    <a href="?page=sub-category&nId=<?php echo $list['s_id']?> " class="actionLink " title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                    <a href="?page=sub-category&dId=<?php echo $list['s_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>
                                        </td>                	
                                    </tr>
                                    <?php $i++;}
                                    }else{?>
                                    <tr>
                                        <td colspan="5">There is no results found.. </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
        <div class="paginationDiv"><?php echo $pagination_output;?></div>
        </div>
                    </div>
                </section>

            </div>
        </div>
<script language="javascript" type="application/javascript">
var Script = function () {
    $.validator.setDefaults({
        submitHandler: function() { alert("submitted!"); }
    });

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#add_sub_category").validate();
    });
}();
</script>