<?php
include_once("../class/face_month_category.php");
include_once("../class/category.php");
include_once("../class/common_class.php");
$objFaceCategory			 =	new face_month_category();
$objCategory			 	 =	new category();
$objCommon		 		   =	new common();
$getMainCategory			 =	$objCategory->getAll("","c_name");
$getRowDetails	   		   =	$objFaceCategory->getRow("fmc_id=1");
$cIdArr					  =	explode(",",$getRowDetails['fmc_category']);
?>
<div class="page-heading">
	<h3>Face of the month</h3>
	<ul class="breadcrumb">
		<li><a href="#">Categories</a></li>
	</ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
            <div class="col-lg-6">
                <section class="panel">
                    <header class="panel-heading">Add  Categories</header>
                    <div class="panel-body">
                        <form role="form" id="add_sub_category" method="post" action="access/add-face-month-category.php">
                             <div class="form-group">
                                <label for="exampleInputEmail1">Category <span style="color:#A7A7A7; font-size:10px; margin-left:15px;">(Select only 3 categories)</span></label>
                                <select name="c_id[]" id="c_id" class="form-control"  multiple="multiple" style=" min-height:250px;" >
                                	<option value="">Select Category</option>
                                    <?php
									foreach($getMainCategory as $allMainCategory){
									?>
                                    <option value="<?php echo $allMainCategory['c_id']?>" <?php echo (in_array($allMainCategory['c_id'],$cIdArr))?'selected':'';?>><?php echo $objCommon->html2text($allMainCategory['c_name'])?></option>
                                    <?php
									}
									?>
                                </select>                    
                            </div>
                            <input type="hidden" name="editId" value="<?php echo $nId?>" />
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </section>
            </div>          
        </div>
<script language="javascript" type="application/javascript">
var Script = function () {
    $.validator.setDefaults({
        submitHandler: function() { alert("submitted!"); }
    });

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#add_sub_category").validate();
    });
}();
</script>