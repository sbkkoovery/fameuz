<?php
include_once("../class/job_category.php");
include_once("../class/common_class.php");
$objJobCategory			  =	new job_category();
$objCommon		 		   =	new common();
$nId			  			 =	$objCommon->esc($_GET['nId']);
$dId			   			 =	$objCommon->esc($_GET['dId']);
$search					  =	$objCommon->esc($_GET['search']);
if($nId){
		$getRowDetails	   =	$objJobCategory->getRow("jc_id=".$nId);
}
if($dId){
		$objJobCategory->delete("jc_id=".$dId);
		$objCommon->addMsg("Selected item has been deleted successfully.",1);
		header("location:".$_SERVER['HTTP_REFERER']);
		exit;
}
$sql						.= "SELECT * FROM job_category WHERE 1 ";
if($search){
	$sql					.= " AND (jc_name LIKE '%".$search."%' OR jc_id LIKE '%".$search."%')";	
}
$sql						.= " ORDER by jc_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objJobCategory->listQuery($paginationQuery);
?>
<div class="page-heading">
	<h3>Job Category</h3>
	<ul class="breadcrumb">
		<li><a href="#">Job</a></li>
		<li class="active"> Category </li>
	</ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
            <div class="col-lg-4">
                <section class="panel">
                    <header class="panel-heading">Add Job Category</header>
                    <div class="panel-body">
                        <form role="form" id="add_collar" method="post" action="access/add-job-category.php">
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" name="jc_name" id="jc_name" class="form-control"  value="<?php echo ($getRowDetails['jc_name'])?$objCommon->html2text($getRowDetails['jc_name']):''?>" placeholder="Enter Title" required >
                            </div>
							 <div class="form-group">
                                <label>Order</label>
                                <input type="text" name="jc_order" id="jc_order" class="form-control"  value="<?php echo ($getRowDetails['jc_order'])?$objCommon->html2text($getRowDetails['jc_order']):''?>" placeholder="Enter order" >
                            </div>
                            <input type="hidden" name="editId" value="<?php echo $nId?>" />
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </section>
            </div>
            <div class="col-lg-8">
                <section class="panel">
                    <header class="panel-heading">List</header>
                    <div class="panel-body">
                    	<div class="row">
                        	<div class="col-md-6 col-md-offset-6 ">
                                <form class="form-search pull-right" method="get" action="">
                                	<input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                                    <input type="hidden" name="page" value="job_category" />
                               		 <button class="btn btn-primary search_submit" type="submit">Submit</button>
                                </form>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="70%">Name</th>
                                        <th width="10%">Order</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count($contentList)>0){
                                    $i=1;
                                    foreach($contentList as $list){?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $objCommon->html2text($list['jc_name']); ?></td></td>
                                         <td><?php echo $list['jc_order']; ?></td></td>
                                        
                                        <td>
                                    <a href="?page=job_category&nId=<?php echo $list['jc_id']?> " class="actionLink " title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                    <a href="?page=job_category&dId=<?php echo $list['jc_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>
                                        </td>                	
                                    </tr>
                                    <?php $i++;}
                                    }else{?>
                                    <tr>
                                        <td colspan="4">There is no results found.. </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
        <div class="paginationDiv"><?php echo $pagination_output;?></div>
        </div>
                    </div>
                </section>

            </div>
        </div>
<script language="javascript" type="application/javascript">
var Script = function () {
    $.validator.setDefaults({
        submitHandler: function() { alert("submitted!"); }
    });

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#add_collar").validate();
    });
}();
</script>