<?php
include_once("../includes/site_root.php");
include_once("../class/invite_games.php");
include_once("../class/notifications.php");
$objInviteGames			  =	new invite_games();
$objNotifications			=	new notifications();
$sqlInvite				   =	"SELECT invite.*,game.g_name,game.g_alias,user_from.first_name AS first_name_from,user_from.last_name AS last_name_from,user_from.display_name AS display_name_from,user_from.email AS email_from,profileImg_from.upi_img_url AS upi_img_url_from,user_to.first_name AS first_name_to,user_to.last_name AS last_name_to,user_to.display_name AS display_name_to,user_to.email AS email_to,profileImg.upi_img_url AS upi_img_url_to
									FROM invite_games AS invite 
									LEFT JOIN users AS user_to ON  invite.ig_to = user_to.user_id
									LEFT JOIN user_social_links AS social ON user_to.user_id=social.user_id
									LEFT JOIN user_profile_image as profileImg ON user_to.user_id=profileImg.user_id AND profileImg.upi_status=1
									LEFT JOIN users AS user_from ON  invite.ig_from = user_from.user_id
									LEFT JOIN user_social_links AS social_from ON user_from.user_id=social_from.user_id
									LEFT JOIN user_profile_image as profileImg_from ON user_from.user_id=profileImg_from.user_id AND profileImg_from.upi_status=1
									LEFT JOIN games AS game ON invite.g_id = game.g_id
									WHERE invite.ig_noti_status=0 ORDER BY ig_created";
$getAllInvite				=	$objInviteGames->listQuery($sqlInvite);
foreach($getAllInvite as $allInvite){
	//----notification table------------------------------------
	if($allInvite['display_name_from']){
		$displayName					 =	$objCommon->html2text($allInvite['display_name_from']);
	}else if($allInvite['first_name_from']){
		$displayName	 				 =	$objCommon->html2text($allInvite['first_name_from']);
	}else{
		$exploEmail	  				  =	explode("@",$objCommon->html2text($allInvite['email_from']));
		$displayName	 				 =	$exploEmail[0];
	}
	$_POST['notification_to']			=	$allInvite['ig_to'];
	$_POST['notification_from']		  =	$allInvite['ig_from'];
	$_POST['notification_type']		  =	'game_invite_friends';
	$_POST['notification_time']		  =	date('Y-m-d H:i:s');
	$_POST['notification_read_status']   =	0;
	$_POST['notification_descr']   	 	 =	'<b>'.$displayName.'</b> invited you to <b>Play '.$objCommon->html2text($allInvite['g_name']).'</b>';
	$_POST['notification_redirect_url']  =	SITE_ROOT.'games/show/'.$objCommon->html2text($allInvite['g_alias']).'-'.$allInvite['g_id'];
	if($_POST['notification_to'] !== $_POST['notification_from']){
		$objNotifications->insert($_POST);
	}
	$objInviteGames->updateField(array('ig_noti_status'=>1),"ig_id=".$allInvite['ig_id']);
	//----------------------------------------------------------
}
$objCommon->addMsg("games  invited successfully",1);
echo $objCommon->displayMsg();
?>