<?php
include_once("../class/other_categories.php");
include_once("../class/main_category.php");
include_once("../class/common_class.php");
$objOtherCategory			     =	new other_categories();
$objMainCategory			 =	new main_category();
$objCommon		 		   =	new common();
$nId			  			 =	$objCommon->esc($_GET['nId']);
$dId			   			 =	$objCommon->esc($_GET['dId']);
$search					  =	$objCommon->esc($_GET['search']);
$cat_search				  =	$objCommon->esc($_GET['cat_search']);
$getMainCategory			 =	$objMainCategory->getAll("","mc_name");
if($nId){
    $getRowDetails	   =	$objOtherCategory->getRow("other_id=".$nId);
}
if($dId){
    $objOtherCategory->delete("c_id=".$dId);
    $objCommon->addMsg("Selected item has been deleted successfully.",1);
    header("location:".$_SERVER['HTTP_REFERER']);
    exit;
}
$sql						 .= "SELECT cat.*,main.mc_name FROM other_categories as cat LEFT JOIN main_category AS main ON cat.other_parent=main.mc_id WHERE 1 ";
if($search){
    $sql					.= " AND (cat.other_name LIKE '%".$search."%' OR cat.other_id LIKE '%".$search."%')";
}
if($cat_search){
    $sql					.= " AND (cat.other_parent = ".$cat_search." )";
}
$sql						 .= " ORDER by cat.other_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objOtherCategory->listQuery($paginationQuery);
?>
<div class="page-heading">
    <h3>Other Categories</h3>
    <ul class="breadcrumb">
        <li><a href="#">Categories</a></li>
        <li class="active"> Other Categories </li>
    </ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
    <div class="col-lg-4">
        <section class="panel">
            <header class="panel-heading">Approve Category</header>
            <div class="panel-body">
                <form role="form" id="add_category" method="post" action="access/add-other-category.php">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Category Title</label>
                        <input type="text" name="c_name" id="c_name" class="form-control" value="<?php echo ($getRowDetails['other_name'])?$objCommon->html2text($getRowDetails['other_name']):''?>" placeholder="Enter Title" required >
                    </div>
                    <div class="form-group">
                        <label >Main Category</label>
                        <select name="mc_id" id="mc_id" class="form-control"  required >
                            <option value="">Select Main Category</option>
                            <?php
                            foreach($getMainCategory as $allMainCategory){
                                ?>
                                <option value="<?php echo $allMainCategory['mc_id']?>" <?php echo ($getRowDetails['other_parent']==$allMainCategory['mc_id'])?'selected':'';?>><?php echo $objCommon->html2text($allMainCategory['mc_name'])?></option>
                                <?php
                            }
                            ?>
                        </select>

                    </div>
                    <div class="form-group">
                        <label>i can book others</label>
                    </div>
                    <div class="form-group">
                        <label><input type="radio" name="book_by_me" value="1" required  /> Yes </label>
                        <label><input type="radio" name="book_by_me" value="0" required /> No </label>
                    </div>
                    <div class="form-group">
                        <label>Other can book me</label>
                    </div>
                    <div class="form-group">
                        <label><input type="radio" name="book_me" value="1" required /> Yes </label>
                        <label><input type="radio" name="book_me" value="0" required  /> No </label>
                    </div>
                    <div class="form-group">
                        <label>Show model form</label>
                    </div>
                    <div class="form-group">
                        <label><input type="checkbox" value="1" name="c_model_details_yes" /> Yes </label>
                    </div>
                    <input type="hidden" name="otherId" value="<?php echo $nId?>" />
                    <button type="submit" class="btn btn-primary">Approve</button>
                </form>

            </div>
        </section>
    </div>
    <div class="col-lg-8">
        <section class="panel">
            <header class="panel-heading">Category List</header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-6 ">
                        <form class="form-search pull-right" method="get" action="">
                            <select name="cat_search" class="select_large">
                                <option value="">Main Category</option>
                                <?php
                                foreach($getMainCategory as $allMainCategorySearch){
                                    ?>
                                    <option value="<?php echo $allMainCategorySearch['mc_id']?>" <?php echo ($cat_search==$allMainCategorySearch['mc_id'])?'selected':'';?>><?php echo $objCommon->html2text($allMainCategorySearch['mc_name'])?></option>
                                    <?php
                                }
                                ?>
                            </select>
                            <input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                            <input type="hidden" name="page" value="category" />
                            <button class="btn btn-primary search_submit" type="submit">Search</button>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th width="30%">Name</th>
                            <th width="25%">Main Category</th>
                            <th width="10%">ID</th>
                            <th width="10%">User ID</th>
                            <th width="15%">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($contentList)>0){
                            $i=1;
                            foreach($contentList as $list){?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $objCommon->html2text($list['other_name']); ?></td></td>
                                    <td><?php echo $objCommon->html2text($list['mc_name']); ?></td></td>
                                    <td><?php echo $list['other_id']; ?></td></td>
                                    <td><?php echo $list['user_id']; ?></td></td>

                                    <td>
                                        <?php if(!$list['other_status']){?>
                                        <a href="?page=other-category&nId=<?php echo $list['other_id']?> " class="actionLink " title="Edit">Approve</a>
                                        <?php }else{?><span>Approved </span> &nbsp;<?php }?>
                                        <a href="?page=other-category&dId=<?php echo $list['other_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php $i++;}
                        }else{?>
                            <tr>
                                <td colspan="6">There is no results found.. </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <div class="paginationDiv"><?php echo $pagination_output;?></div>
                </div>
            </div>
        </section>

    </div>
</div>
<script language="javascript" type="application/javascript">
    var Script = function () {
        $.validator.setDefaults({
            submitHandler: function() { alert("submitted!"); }
        });

        $().ready(function() {
            // validate the comment form when it is submitted
            $("#add_category").validate();
        });
    }();
</script>