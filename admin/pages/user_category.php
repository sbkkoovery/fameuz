<?php
include_once("../class/users.php");
include_once("../class/common_class.php");
$objUsers			  =	new users();
$objCommon		 		   =	new common();
$search					  =	$objCommon->esc($_GET['search']);
$pageid					  =	$objCommon->esc($_GET['page_id']);
if(isset($pageid)&&$pageid!=""){
	$i = (20*($pageid-1))+1;
}
else{
$i=1;
}
$sql						.= "SELECT * FROM users WHERE 1 ";
if($search){
	$sql					.= " AND (first_name LIKE '%".$search."%' OR user_id LIKE '%".$search."%')";	
}
$sql						.= " ORDER by user_id ASC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objUsers->listQuery($paginationQuery);

?>
<div class="page-heading">
	<h3>Users</h3>
	<ul class="breadcrumb">
		<li><a href="#">Users</a></li>
		<li class="active">All Users </li>
	</ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
              <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">List</header>
                    <div class="panel-body">
                    	<div class="row">
                        	<div class="col-md-6 col-md-offset-6 ">
                                <form class="form-search pull-right" method="get" action="">
                                	<input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                                    <input type="hidden" name="page" value="user_category" />
                               		 <button class="btn btn-primary search_submit" type="submit">Submit</button>
                                </form>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th width="15%">First Name</th>
                                        <th width="15%">Last Name</th>
                                        <th width="15%">Display Name</th>
                                        <th width="15%">Email</th>
                                        <th width="10%">Login Type</th>
                                        <th width="15%">Email Validation</th>
                                        <th width="10%">Status</th>
                                                                             
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count($contentList)>0){
                                    
                                    foreach($contentList as $list){?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $objCommon->html2text($list['first_name']); ?></td>
                                        <td><?php echo $objCommon->html2text($list['last_name']); ?></td>
                                        <td><?php echo $objCommon->html2text($list['display_name']); ?></td>
                                        <td><?php echo $objCommon->html2text($list['email']); ?></td>
                                        <td><?php if($objCommon->html2text($list['login_type'])==1){?> Site Login
                                            <?php }else{ ?> Facebook Login<?php } ?></td>  
                                        <td><?php if($objCommon->html2text($list['email_validation'])==1){?> Validated
                                            <?php }else{ ?> Not Validated<?php } ?></td>     
                                        <td><?php if($objCommon->html2text($list['status'])==0){?> Not Activated
                                            <?php }else if($objCommon->html2text($list['status'])==1){?> Activated
                                            <?php }else if($objCommon->html2text($list['status'])==2){?> Deactivated 
                                            <?php }else{ ?> Deleted<?php } ?></td>    
                                                    	
                                    </tr>
                                    
									<?php $i++;}
                                    }else{?>
                                    <tr>
                                        <td colspan="4">There is no results found.. </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
        <div class="paginationDiv"><?php echo $pagination_output;?></div>
        </div>
                    </div>
                </section>

            </div>
        </div>
        <?php  ?>
<script language="javascript" type="application/javascript">
var Script = function () {
    $.validator.setDefaults({
        submitHandler: function() { alert("submitted!"); }
    });

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#add_collar").validate();
    });
}();
</script>