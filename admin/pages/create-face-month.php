<?php
include_once("../class/users.php");
include_once("../class/face_month_members.php");
include_once("../class/common_class.php");
$objUsers			  		=	new users();
$objFaceMonthMember		  =	new face_month_members();
$objCommon		 		   =	new common();
$totalLikeCountSQl		   =	$objUsers->getRowSql("SELECT COUNT(like_id) AS likeCount FROM likes WHERE like_status=1");
$totalLikeCount			  =	$totalLikeCountSQl['likeCount'];
$totalbookingAcceptedSql	 =	$objUsers->getRowSql("SELECT COUNT(bm_id) AS bookedCount FROM book_model WHERE bm_model_status=1");
$totalbookingAccepted		=	$totalbookingAcceptedSql['bookedCount'];
$getCategories			   =	$objUsers->listQuery("SELECT c_id,mc_id,c_name FROM category");
$objFaceMonthMember->delete_new("TRUNCATE face_month_members");
foreach($getCategories as $allCat){
	if($allCat['mc_id']==1){
	
		$sqlMain1			=	"SELECT tab.user_id,COALESCE((((tab.countLike*100)/".$totalLikeCount.")+tab.reviewAvg+((tab.countBook*100)/".$totalbookingAccepted.")),0) AS finalCount FROM(
		SELECT ucat.user_id,COALESCE((AVG(reviews.review_rate)*20),0) AS reviewAvg,COALESCE(lk.likeCount,0) AS countLike,COALESCE(bk.bookCount,0)  As countBook
		FROM user_categories AS ucat
		LEFT JOIN user_reviews AS reviews ON ucat.user_id = reviews.user_id_to  AND reviews.review_date BETWEEN SUBDATE(CURDATE(), INTERVAL 1 MONTH) AND NOW()
		LEFT JOIN user_profile_image as profileImg ON ucat.user_id = profileImg.user_id AND profileImg.upi_status=1
		LEFT JOIN (SELECT likes.like_img_user_by,count(likes.like_id) AS likeCount FROM user_categories AS ucat  LEFT JOIN likes  ON ucat.user_id = likes.like_img_user_by WHERE likes.like_status=1 AND likes.like_time BETWEEN SUBDATE(CURDATE(), INTERVAL 1 MONTH) AND NOW() GROUP BY ucat.user_id) AS lk
		ON ucat.user_id = lk.like_img_user_by 
		LEFT JOIN (SELECT book.agent_id,count(book.bm_id) AS bookCount FROM user_categories AS ucat  LEFT JOIN book_model AS book  ON ucat.user_id = book.agent_id WHERE book.bm_model_status=1 AND book.bm_created BETWEEN SUBDATE(CURDATE(), INTERVAL 1 MONTH) AND NOW() GROUP BY ucat.user_id) AS bk
		ON ucat.user_id = bk.agent_id 
		WHERE ucat.uc_m_id =1  AND ucat.uc_c_id = ".$allCat['c_id']." 
		GROUP BY ucat.user_id
		) AS tab ORDER BY finalCount DESC LIMIT 3 ";
		$faceMaincat1		=	$objUsers->listQuery($sqlMain1);
		$_POST['fmm_cat']	=	$allCat['c_id'];
		$membStr1			=	'';
		foreach($faceMaincat1 as $keyMaincat1=>$allMaincat1){
			if($keyMaincat1==0){
				$membStr1		.=	",".$allMaincat1['user_id'].",";
			}else{
				$membStr1		.=	$allMaincat1['user_id'].",";
			}
		}
		$_POST['fmm_members']	=	$membStr1;
		$objFaceMonthMember->insert($_POST);	
	}elseif($allCat['mc_id']==2){
		$sqlMain2			=	"SELECT tab.user_id,COALESCE((((tab.countLike*100)/".$totalLikeCount.")+tab.reviewAvg),0) AS finalCount FROM(
		SELECT ucat.user_id,COALESCE((AVG(reviews.review_rate)*20),0) AS reviewAvg,COALESCE(lk.likeCount,0) AS countLike
		FROM user_categories AS ucat
		LEFT JOIN user_reviews AS reviews ON ucat.user_id = reviews.user_id_to  AND reviews.review_date BETWEEN SUBDATE(CURDATE(), INTERVAL 1 MONTH) AND NOW()
		LEFT JOIN user_profile_image as profileImg ON ucat.user_id = profileImg.user_id AND profileImg.upi_status=1
		LEFT JOIN (SELECT likes.like_img_user_by,count(likes.like_id) AS likeCount FROM user_categories AS ucat  LEFT JOIN likes  ON ucat.user_id = likes.like_img_user_by WHERE likes.like_status=1 AND likes.like_time BETWEEN SUBDATE(CURDATE(), INTERVAL 1 MONTH) AND NOW() GROUP BY ucat.user_id) AS lk
		ON ucat.user_id = lk.like_img_user_by
		WHERE ucat.uc_m_id =2  AND ucat.uc_c_id = ".$allCat['c_id']." 
		GROUP BY ucat.user_id
		) AS tab ORDER BY finalCount DESC LIMIT 3 ";
		$faceMaincat2		=	$objUsers->listQuery($sqlMain2);
		$_POST['fmm_cat']	=	$allCat['c_id'];
		$membStr2			=	'';
		foreach($faceMaincat2 as $keyMaincat2=>$allMaincat2){
			if($keyMaincat2==0){
				$membStr2		.=	",".$allMaincat2['user_id'].",";
			}else{
				$membStr2		.=	$allMaincat2['user_id'].",";
			}
		}
		$_POST['fmm_members']	=	$membStr2;
		$objFaceMonthMember->insert($_POST);
	}
}
//--------------------------------------------------------------------------------------------------
$sql						 .=	"SELECT cat.c_name,face.fmm_members FROM face_month_members AS face LEFT JOIN category AS cat ON face.fmm_cat = cat.c_id WHERE 1 ";
$sql						 .=	" ORDER by fmm_id";
$contentList				  =	$objUsers->listQuery($sql);

?>
<div class="page-heading">
	<h3>Face Of the Month</h3>
	<ul class="breadcrumb">
		<li><a href="#">User</a></li>
		<li class="active"> Create face of the month </li>
	</ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">Face of the month List</header>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th width="30%">Category</th>
                                        <th>Face of the month</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                    <?php 
                                    if(count($contentList)>0){
                                    $i=1;
                                    foreach($contentList as $list){
										if($list['fmm_members']){
											$trim_fmm_members		=	trim($list['fmm_members'],",");
											$memFaceList			 =	$objUsers->listQuery("SELECT user.first_name,user.last_name,user.display_name,user.email FROM users AS user WHERE user.user_id IN (".$trim_fmm_members.")" );
											$memfa				   =	'';
											foreach($memFaceList as $all_memFaceList){
												$memfa			   .=	$objCommon->displayName($all_memFaceList)."<br/>";
											}
										}
										else{
											$memfa				   =	'';
										}
										
									?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $objCommon->html2text($list['c_name']); ?></td>
                                        <td><?php echo $memfa; ?></td>                	
                                    </tr>
                                    <?php $i++;}
                                    }else{?>
                                    <tr>
                                        <td colspan="4">There is no results found.. </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
        <div class="paginationDiv"><?php echo $pagination_output;?></div>
        </div>
                    </div>
                </section>

            </div>
        </div>
<script language="javascript" type="application/javascript">
var Script = function () {
    $.validator.setDefaults({
        submitHandler: function() { alert("submitted!"); }
    });

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#add_main_category").validate();
    });
}();
</script>