<?php
include_once("../class/country.php");
include_once("../class/common_class.php");
$objCountry			  =	new country();
$objCommon		 		   =	new common();
$nId			  			 =	$objCommon->esc($_GET['nId']);
$dId			   			 =	$objCommon->esc($_GET['dId']);
$search					  =	$objCommon->esc($_GET['search']);
if($nId){
		$getRowDetails	   =	$objCountry->getRow("country_id=".$nId);
}
if($dId){
		$objCountry->delete("country_id=".$dId);
		$objCommon->addMsg("Selected item has been deleted successfully.",1);
		header("location:".$_SERVER['HTTP_REFERER']);
		exit;
}
$sql						 .= "SELECT * FROM country WHERE 1 ";
if($search){
	$sql					.= " AND (country_name LIKE '%".$search."%' OR country_id LIKE '%".$search."%')";	
}
$sql						 .= " ORDER by country_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objCountry->listQuery($paginationQuery);
?>
<div class="page-heading">
	<h3>Country</h3>
	<ul class="breadcrumb">
		<li><a href="#">Extra pages</a></li>
		<li class="active"> Country </li>
	</ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
            <div class="col-lg-4">
                <section class="panel">
                    <header class="panel-heading">Add Country</header>
                    <div class="panel-body">
                        <form role="form" id="add_country" method="post" action="access/add-country.php">
							<div class="form-group">
                                <label for="exampleInputEmail1">Country Code</label>
                                <input type="text" name="country_code" id="country_code" class="form-control" value="<?php echo ($getRowDetails['country_code'])?$objCommon->html2text($getRowDetails['country_code']):''?>" placeholder="Enter Code" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Country Name</label>
                                <input type="text" name="country_name" id="country_name" class="form-control" value="<?php echo ($getRowDetails['country_name'])?$objCommon->html2text($getRowDetails['country_name']):''?>" placeholder="Enter Name" required >
                            </div>
                            <input type="hidden" name="editId" value="<?php echo $nId?>" />
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </section>
            </div>
            <div class="col-lg-8">
                <section class="panel">
                    <header class="panel-heading">Country List</header>
                    <div class="panel-body">
                    <div class="row">
                        	<div class="col-md-6 col-md-offset-6 ">
                                <form class="form-search pull-right" method="get" action="">
                                	<input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                                    <input type="hidden" name="page" value="country" />
                               		 <button class="btn btn-primary search_submit" type="submit">Submit</button>
                                </form>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="60%">Name</th>
										<th width="10%">Code</th>
                                        <th width="10%">ID</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count($contentList)>0){
                                    $i=1;
                                    foreach($contentList as $list){?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $objCommon->html2text($list['country_name']); ?></td></td>
										<td><?php echo $objCommon->html2text($list['country_code']); ?></td></td>
                                        <td><?php echo $list['country_id']; ?></td></td>
                                        <td>
                                    <a href="?page=country&nId=<?php echo $list['country_id']?> " class="actionLink " title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                    <a href="?page=country&dId=<?php echo $list['country_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>
                                        </td>                	
                                    </tr>
                                    <?php $i++;}
                                    }else{?>
                                    <tr>
                                        <td colspan="4">There is no results found.. </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
        <div class="paginationDiv"><?php echo $pagination_output;?></div>
        </div>
                    </div>
                </section>

            </div>
        </div>
<script language="javascript" type="application/javascript">
var Script = function () {
    $.validator.setDefaults({
        submitHandler: function() { alert("submitted!"); }
    });

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#add_country").validate();
    });
}();
</script>