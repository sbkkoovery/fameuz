
            <div class="row states-info">
            <div class="col-md-3">
                <div class="panel yellow-bg">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-eye"></i>
                            </div>
                            <div class="col-xs-8">
                                <span class="state-title">  Visitors  </span>
                                <h4>10,000</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel red-bg">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-user"></i>
                            </div>
                            <div class="col-xs-8">
                                <span class="state-title">New Registered Users</span>
                                <h4>232</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel blue-bg">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-bullhorn"></i>
                            </div>
                            <div class="col-xs-8">
                                <span class="state-title">  Add Visitors</span>
                                <h4>2,980</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel green-bg">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-comments"></i>
                            </div>
                            <div class="col-xs-8">
                                <span class="state-title">  Community Reviews  </span>
                                <h4>5980</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>