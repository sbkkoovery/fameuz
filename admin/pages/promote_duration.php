<?php
include_once("../class/promotion_based_duration.php");
include_once("../class/common_class.php");
$objPromotionDuration	 	=	new promotion_based_duration();
$objCommon		 		   =	new common();
$nId			  			 =	$objCommon->esc($_GET['nId']);
$dId			   			 =	$objCommon->esc($_GET['dId']);
$search					  =	$objCommon->esc($_GET['search']);
if($nId){
		$getRowDetails	   =	$objPromotionDuration->getRow("pbd_id=".$nId);
}
if($dId){
		$objPromotionDuration->delete("pbd_id=".$dId);
		$objCommon->addMsg("Selected item has been deleted successfully.",1);
		header("location:".$_SERVER['HTTP_REFERER']);
		exit;
}
$sql						= "SELECT * FROM promotion_based_duration ORDER by pbd_id DESC ";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objPromotionDuration->listQuery($paginationQuery);
?>
<div class="page-heading">
	<h3>Duration</h3>
	<ul class="breadcrumb">
		<li><a href="#">Promotion</a></li>
		<li class="active"> Duration </li>
	</ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
            <div class="col-lg-4">
                <section class="panel">
                    <header class="panel-heading">Add Promotion Duration</header>
                    <div class="panel-body">
                        <form role="form" id="add_collar" method="post" action="access/add-promote-duration.php">
                            <div class="form-group">
                                <label>Package Name</label>
                                <input type="text" name="pbd_name" id="pbd_name" class="form-control"  value="<?php echo ($getRowDetails['pbd_name'])?$objCommon->html2text($getRowDetails['pbd_name']):''?>" placeholder="Enter Title" required >
                            </div>
							<div class="form-group">
                                <label>Package Duration(in Days)</label>
                                <input type="text" name="pbd_days" id="pbd_days" class="form-control"  value="<?php echo ($getRowDetails['pbd_days'])?$objCommon->html2text($getRowDetails['pbd_days']):''?>" placeholder="Enter Days" required >
                            </div>
							<div class="form-group">
                                <label>Cost for profile</label>
                                <input type="text" name="pbd_profile" id="pbd_profile" class="form-control"  value="<?php echo ($getRowDetails['pbd_profile'])?$objCommon->html2text($getRowDetails['pbd_profile']):''?>" placeholder="Enter Cost in USD"  >
                            </div>
							<div class="form-group">
                                <label>Cost for Video</label>
                                <input type="text" name="pbd_video" id="pbd_video" class="form-control"  value="<?php echo ($getRowDetails['pbd_video'])?$objCommon->html2text($getRowDetails['pbd_video']):''?>" placeholder="Enter Cost in USD"  >
                            </div>
							<div class="form-group">
                                <label>Cost for Video</label>
                                <input type="text" name="pbd_music" id="pbd_music" class="form-control"  value="<?php echo ($getRowDetails['pbd_music'])?$objCommon->html2text($getRowDetails['pbd_music']):''?>" placeholder="Enter Cost in USD"  >
                            </div>
                            <input type="hidden" name="editId" value="<?php echo $nId?>" />
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </section>
            </div>
            <div class="col-lg-8">
                <section class="panel">
                    <header class="panel-heading">List</header>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="30%">Name</th>
                                        <th width="10%">Days</th>
										<th width="10%">Profile</th>
										<th width="10%">Video</th>
										<th width="10%">Music</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count($contentList)>0){
                                    $i=1;
                                    foreach($contentList as $list){?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $objCommon->html2text($list['pbd_name']); ?></td></td>
                                        <td><?php echo $list['pbd_days']; ?></td></td>
                                        <td><?php echo $list['pbd_profile']; ?></td></td>
										<td><?php echo $list['pbd_video']; ?></td></td>
										<td><?php echo $list['pbd_music']; ?></td></td>
                                        <td>
                                    <a href="?page=promote_duration&nId=<?php echo $list['pbd_id']?> " class="actionLink " title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                    <a href="?page=promote_duration&dId=<?php echo $list['pbd_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>
                                        </td>                	
                                    </tr>
                                    <?php $i++;}
                                    }else{?>
                                    <tr>
                                        <td colspan="4">There is no results found.. </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
        <div class="paginationDiv"><?php echo $pagination_output;?></div>
        </div>
                    </div>
                </section>

            </div>
        </div>
<script language="javascript" type="application/javascript">
var Script = function () {
    $.validator.setDefaults({
        submitHandler: function() { alert("submitted!"); }
    });

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#add_collar").validate();
    });
}();
</script>