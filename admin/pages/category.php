<?php
include_once("../class/category.php");
include_once("../class/main_category.php");
include_once("../class/common_class.php");
$objCategory			     =	new category();
$objMainCategory			 =	new main_category();
$objCommon		 		   =	new common();
$nId			  			 =	$objCommon->esc($_GET['nId']);
$dId			   			 =	$objCommon->esc($_GET['dId']);
$search					  =	$objCommon->esc($_GET['search']);
$cat_search				  =	$objCommon->esc($_GET['cat_search']);
$getMainCategory			 =	$objMainCategory->getAll("","mc_name");
if($nId){
		$getRowDetails	   =	$objCategory->getRow("c_id=".$nId);
}
if($dId){
		$objCategory->delete("c_id=".$dId);
		$objCommon->addMsg("Selected item has been deleted successfully.",1);
		header("location:".$_SERVER['HTTP_REFERER']);
		exit;
}
$sql						 .= "SELECT category.*,main.mc_name FROM category LEFT JOIN main_category AS main ON category.mc_id=main.mc_id WHERE 1 ";
if($search){
	$sql					.= " AND (c_name LIKE '%".$search."%' OR c_id LIKE '%".$search."%')";	
}
if($cat_search){
	$sql					.= " AND (category.mc_id = ".$cat_search." )";
}
$sql						 .= " ORDER by c_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objCategory->listQuery($paginationQuery);
?>
<div class="page-heading">
	<h3>Category</h3>
	<ul class="breadcrumb">
		<li><a href="#">Categories</a></li>
		<li class="active"> Category </li>
	</ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
            <div class="col-lg-4">
                <section class="panel">
                    <header class="panel-heading">Add Category</header>
                    <div class="panel-body">
                        <form role="form" id="add_category" method="post" action="access/add-category.php">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Category Title</label>
                                <input type="text" name="c_name" id="c_name" class="form-control" value="<?php echo ($getRowDetails['c_name'])?$objCommon->html2text($getRowDetails['c_name']):''?>" placeholder="Enter Title" required >
                            </div>
                             <div class="form-group">
                                <label >Main Category</label>
                                <select name="mc_id" id="mc_id" class="form-control"  required >
                                	<option value="">Select Main Category</option>
                                    <?php
									foreach($getMainCategory as $allMainCategory){
									?>
                                    <option value="<?php echo $allMainCategory['mc_id']?>" <?php echo ($getRowDetails['mc_id']==$allMainCategory['mc_id'])?'selected':'';?>><?php echo $objCommon->html2text($allMainCategory['mc_name'])?></option>
                                    <?php
									}
									?>
                                </select>
                     
                            </div>
							<div class="form-group">
								<label>i can book others</label>
							</div>
							<div class="form-group">
                                <label><input type="radio" name="book_by_me" value="1" required <?php echo ($getRowDetails['c_book_by_me']==1 && $nId !='')?'checked="checked"':''?> /> Yes </label>
								<label><input type="radio" name="book_by_me" value="0" required <?php echo ($getRowDetails['c_book_by_me']==0 && $nId !='')?'checked="checked"':''?> /> No </label>
                            </div>
							<div class="form-group">
								<label>Other can book me</label>
							</div>
							<div class="form-group">
                                <label><input type="radio" name="book_me" value="1" required <?php echo ($getRowDetails['c_book_me']==1 && $nId !='')?'checked="checked"':''?> /> Yes </label>
								<label><input type="radio" name="book_me" value="0" required <?php echo ($getRowDetails['c_book_me']==0 && $nId !='')?'checked="checked"':''?>  /> No </label>
                            </div>
							<div class="form-group">
								<label>Show model form</label>
							</div>
							<div class="form-group">
								<label><input type="checkbox" value="1" name="c_model_details_yes" <?php echo ($getRowDetails['c_model_details_yes']==1)?'checked="checked"':''?> /> Yes </label>
                            </div>
                            <input type="hidden" name="editId" value="<?php echo $nId?>" />
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </section>
            </div>
            <div class="col-lg-8">
                <section class="panel">
                    <header class="panel-heading">Category List</header>
                    <div class="panel-body">
                    	<div class="row">
                        	<div class="col-md-6 col-md-offset-6 ">
                                <form class="form-search pull-right" method="get" action="">
                                <select name="cat_search" class="select_large">
                                	<option value="">Main Category</option>
                                     <?php
									foreach($getMainCategory as $allMainCategorySearch){
									?>
                                    <option value="<?php echo $allMainCategorySearch['mc_id']?>" <?php echo ($cat_search==$allMainCategorySearch['mc_id'])?'selected':'';?>><?php echo $objCommon->html2text($allMainCategorySearch['mc_name'])?></option>
                                    <?php
									}
									?>
                                </select>
                                	<input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                                    <input type="hidden" name="page" value="category" />
                               		 <button class="btn btn-primary search_submit" type="submit">Search</button>
                                </form>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="50%">Name</th>
                                        <th width="20%">Main Category</th>
                                        <th width="10%">ID</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count($contentList)>0){
                                    $i=1;
                                    foreach($contentList as $list){?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $objCommon->html2text($list['c_name']); ?></td></td>
                                         <td><?php echo $objCommon->html2text($list['mc_name']); ?></td></td>
                                         <td><?php echo $list['c_id']; ?></td></td>
                                        
                                        <td>
                                    <a href="?page=category&nId=<?php echo $list['c_id']?> " class="actionLink " title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                    <a href="?page=category&dId=<?php echo $list['c_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>
                                        </td>                	
                                    </tr>
                                    <?php $i++;}
                                    }else{?>
                                    <tr>
                                        <td colspan="5">There is no results found.. </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
        <div class="paginationDiv"><?php echo $pagination_output;?></div>
        </div>
                    </div>
                </section>

            </div>
        </div>
<script language="javascript" type="application/javascript">
var Script = function () {
    $.validator.setDefaults({
        submitHandler: function() { alert("submitted!"); }
    });

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#add_category").validate();
    });
}();
</script>