<?php
include_once("../class/promotion_based_location.php");
include_once("../class/common_class.php");
$objCommon		 		   =	new common();
$objpromolocation	=	new promotion_based_location();
$getRowDetails	   =	$objpromolocation->getRow("pbl_id=1");
?>
<div class="page-heading">
	<h3>Promotion Based on Location</h3>
	<ul class="breadcrumb">
		<li><a href="#">Promotion</a></li>
		<li class="active"> Promotion Based on Location </li>
	</ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
           <div class="col-lg-8">
                <section class="panel">
                    <header class="panel-heading">Promotion packages based on location of users</header>
                    <div class="panel-body">
                    <form role="form" id="promote_loc" method="post" action="access/add-promo-loc.php">
                    	<div class="table-responsive">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="25%"></th>
                                        <th width="25%">Profile Promotion</th>
                                        <th width="25%">Video Promotion</th>
                                        <th width="25%">Music Promotion</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Cost per one country</td>
                                        <td> <input type="text" name="prom_pro" id="prom_pro" class="form-control"  value="<?php echo ($getRowDetails['pbl_profile'])?$objCommon->html2text($getRowDetails['pbl_profile']):''?>" required ></td>
                                        <td> <input type="text" name="prom_vid" id="prom_vid" class="form-control"  value="<?php echo ($getRowDetails['pbl_video'])?$objCommon->html2text($getRowDetails['pbl_video']):''?>" required ></td>
                                        <td> <input type="text" name="prom_mus" id="prom_mus" class="form-control"  value="<?php echo ($getRowDetails['pbl_music'])?$objCommon->html2text($getRowDetails['pbl_music']):''?>" required ></td>                	
                                    </tr>
                                </tbody>
                            </table>
                            <button type="submit" class="btn btn-primary">Update</button>
                            </form>
           </div>
                    </div>
                </section>

            </div>
        </div>
<script language="javascript" type="application/javascript">

</script>