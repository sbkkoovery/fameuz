<?php
include_once("../class/games.php");
include_once("../class/game_category.php");
include_once("../class/common_class.php");
include_once("../class/game_descr_images.php");
$objGames			    	=	new games();
$objGameCategory			 =	new game_category();
$objCommon		 		   =	new common();
$objDescrImages			  =	new game_descr_images();
$nId			  			 =	$objCommon->esc($_GET['nId']);
$dId			   			 =	$objCommon->esc($_GET['dId']);
$search					  =	$objCommon->esc($_GET['search']);
$cat_search				  =	$objCommon->esc($_GET['cat_search']);
$getMainCategory			 =	$objGameCategory->getAll("","gc_name");
if($nId){
		$getRowDetails	   =	$objGames->getRow("g_id=".$nId);
		$getDescrImages	  =	$objDescrImages->getAll("g_id=".$nId);
}
if($dId){
	$getDelDetails	   	   =	$objGames->getRow("g_id=".$dId);
	$explAiImages	   		=	explode("/",$getDelDetails['g_img']);
	$getAiImag1		  	  =	$explAiImages[0].'/thumb/'.$explAiImages[1];
	$getAiImag2		  	  =	$explAiImages[0].'/original/'.$explAiImages[1];
	$imgPath				 =	'../uploads/games/img/'.$getDelDetails['g_img'];
	$imgPath1				=	'../uploads/games/img/'.$getAiImag1;
	$imgPath2				=	'../uploads/games/img/'.$getAiImag2;
	$fileLink				=	'../uploads/games/file/'.$getDelDetails['g_file'];
	unlink($imgPath);
	unlink($imgPath1);
	unlink($imgPath2);
	unlink($fileLink);
	$objGames->delete("g_id=".$dId);
	$objCommon->addMsg("Selected item has been deleted successfully.",1);
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}
$sql						.= "SELECT games.*,cat.gc_name FROM games LEFT JOIN game_category AS cat ON games.gc_id=cat.gc_id WHERE 1 ";
if($search){
	$sql					.= " AND (g_name LIKE '%".$search."%' OR g_id LIKE '%".$search."%')";	
}
if($cat_search){
	$sql					.= " AND (cat.gc_id = ".$cat_search." )";
}
$sql						.= " ORDER by g_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objGames->listQuery($paginationQuery);
?>
<div class="page-heading">
	<h3>Games</h3>
	<ul class="breadcrumb">
		<li><a href="#">Games</a></li>
		<li class="active"> Games </li>
	</ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
            <div class="col-lg-4">
                <section class="panel">
                    <header class="panel-heading">Add Games</header>
                    <div class="panel-body">
                        <form role="form" id="add_category" method="post" action="access/add-games.php" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Game Name</label>
                                <input type="text" name="g_name" id="g_name" class="form-control" value="<?php echo ($getRowDetails['g_name'])?$objCommon->html2text($getRowDetails['g_name']):''?>" placeholder="Enter Title" required >
                            </div>
                             <div class="form-group">
                                <label >Game Category</label>
                                <select name="gc_id" id="gc_id" class="form-control"  required >
                                	<option value="">Select Game Category</option>
                                    <?php
									foreach($getMainCategory as $allMainCategory){
									?>
                                    <option value="<?php echo $allMainCategory['gc_id']?>" <?php echo ($getRowDetails['gc_id']==$allMainCategory['gc_id'])?'selected':'';?>><?php echo $objCommon->html2text($allMainCategory['gc_name'])?></option>
                                    <?php
									}
									?>
                                </select>
                            </div>
							<div class="form-group">
								<label for="">Game Description</label>
							</div>
							<div class="form-group">
								<textarea name="g_descr" class="form-control" ><?php echo ($getRowDetails['g_descr'])?$objCommon->html2text($getRowDetails['g_descr']):''?></textarea>
                            </div>
							<div class="form-group">
                                <label for="">Game File</label>
                                <input type="file" name="g_file" id="g_file">
                            </div>
							<div class="form-group">
                                <label for="">Game Image</label>
                                <input type="file" name="g_img" id="g_img">
                            </div>
							<?php
							if($getRowDetails['g_img']){
								?>
							<div class="form-group">
                                <img src="../uploads/games/img/<?php echo $objCommon->html2text($getRowDetails['g_img'])?>" width="100" height="60" />
								<a href="access/delete_game_image.php?type=g_img&g_id=<?php echo $objCommon->html2text($getRowDetails['g_id'])?>&imgName=<?php echo $objCommon->html2text($getRowDetails['g_img'])?>">Delete</a>
                            </div>
							<?php
							}
							?>
							<div class="form-group">
                                <label for="">Game Banner</label>
                                <input type="file" name="g_banner" id="g_banner">
                            </div>
							<?php
							if($getRowDetails['g_banner']){
								?>
							<div class="form-group">
                                <img src="../uploads/games/banner/<?php echo $objCommon->html2text($getRowDetails['g_banner'])?>" width="100" height="60" />
								<a href="access/delete_game_image.php?type=g_banner&g_id=<?php echo $objCommon->html2text($getRowDetails['g_id'])?>&imgName=<?php echo $objCommon->html2text($getRowDetails['g_banner'])?>">Delete</a>
                            </div>
							<?php
							}
							?>
							<div class="form-group">
                                <label for="">Game Description Images</label>
                                <input type="file" name="g_descr_imgs[]" id="g_descr_imgs" multiple="multiple">
                            </div>
							<?php
							if(count($getDescrImages)>0){
								foreach($getDescrImages as $allDescrImgs){
									?>
									<div class="form-group">
										<img src="../uploads/games/descr_images/<?php echo $objCommon->html2text($allDescrImgs['gdi_url'])?>" width="100" height="60" />
										<a href="access/delete_game_image.php?type=g_descr_img&g_id=<?php echo $objCommon->html2text($allDescrImgs['gdi_id'])?>&imgName=<?php echo $objCommon->html2text($allDescrImgs['gdi_url'])?>">Delete</a>
									</div>
									<?php
								}
							}
							?>
                            <input type="hidden" name="editId" value="<?php echo $nId?>" />
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </section>
            </div>
            <div class="col-lg-8">
                <section class="panel">
                    <header class="panel-heading">Game List</header>
                    <div class="panel-body">
                    	<div class="row">
                        	<div class="col-md-6 col-md-offset-6 ">
                                <form class="form-search pull-right" method="get" action="">
                                <select name="cat_search" class="select_large">
                                	<option value="">Category</option>
                                     <?php
									foreach($getMainCategory as $allMainCategorySearch){
									?>
                                    <option value="<?php echo $allMainCategorySearch['gc_id']?>" <?php echo ($cat_search==$allMainCategorySearch['gc_id'])?'selected':'';?>><?php echo $objCommon->html2text($allMainCategorySearch['gc_name'])?></option>
                                    <?php
									}
									?>
                                </select>
                                	<input type="text" class="input-large search-query" name="search" value="<?php echo $search?>">
                                    <input type="hidden" name="page" value="games" />
                               		 <button class="btn btn-primary search_submit" type="submit">Search</button>
                                </form>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="50%">Name</th>
                                        <th width="20%">Category</th>
                                        <th width="10%">ID</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count($contentList)>0){
                                    $i=1;
                                    foreach($contentList as $list){?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $objCommon->html2text($list['g_name']); ?></td></td>
                                         <td><?php echo $objCommon->html2text($list['gc_name']); ?></td></td>
                                         <td><?php echo $list['g_id']; ?></td></td>
                                        
                                        <td>
                                    <a href="?page=games&nId=<?php echo $list['g_id']?> " class="actionLink " title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                    <a href="?page=games&dId=<?php echo $list['g_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>
                                        </td>                	
                                    </tr>
                                    <?php $i++;}
                                    }else{?>
                                    <tr>
                                        <td colspan="5">There is no results found.. </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
        <div class="paginationDiv"><?php echo $pagination_output;?></div>
        </div>
                    </div>
                </section>

            </div>
        </div>
<script language="javascript" type="application/javascript">
var Script = function () {
    $.validator.setDefaults({
        submitHandler: function() { alert("submitted!"); }
    });

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#add_category").validate();
    });
}();
</script>