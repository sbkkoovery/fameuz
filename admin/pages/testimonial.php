<?php
include_once("../class/testimonial.php");
include_once("../class/common_class.php");
$objTestonial			   	=	new testimonial();
$objCommon		 		   =	new common();
$nId			  			 =	$objCommon->esc($_GET['nId']);
$dId			   			 =	$objCommon->esc($_GET['dId']);
$search					  =	$objCommon->esc($_GET['search']);
if($nId){
		$getRowDetails	   =	$objTestonial->getRow("testi_id=".$nId);		
}
if($dId){
	$getDelDetails	   	   =	$objTestonial->getRow("testi_id=".$dId);
	$explAiImages	   		=	explode("/",$getDelDetails['t_image']);
	$getAiImag1		  	  =	$explAiImages[0].'/thumb/'.$explAiImages[1];
	$imgPath				 =	'../uploads/testimonial/imgs/'.$getDelDetails['t_image'];
	$imgPath1				=	'../uploads/testimonial/imgs/'.$getAiImag1;
	
	unlink($imgPath);
	unlink($imgPath1);
		
	$objTestonial->delete("testi_id=".$dId);
	$objCommon->addMsg("Selected item has been deleted successfully.",1);
	header("location:".$_SERVER['HTTP_REFERER']);
	exit;
}
$sql						.= "SELECT testimonial.* FROM testimonial ";
if($search){
	$sql					.= " where (fullname LIKE '%".$search."%' OR testi_id LIKE '%".$search."%')";	
}
$sql						.= " ORDER by testi_id DESC";
$num_results_per_page		= 20;
$num_page_links_per_page 	 = 5;
$pg_param 					= "";
$pagesection				 = '';
pagination($sql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$contentList				 =	$objTestonial->listQuery($paginationQuery);
?>
<div class="page-heading">
	<h3>Testimonial</h3>
	<ul class="breadcrumb">
		<li><a href="#">Testimonial</a></li>
		<li class="active"> Testimonial </li>
	</ul>
</div>
<?php echo $objCommon->displayMsg(); ?>
<div class="row">
            <div class="col-lg-4">
                <section class="panel">
                    <header class="panel-heading">Add Testimonial</header>
                    <div class="panel-body">
                        <form role="form" id="add_testimonial" method="post" action="access/add_testimonial.php" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="eInputEmail1">Name</label>
                                <input type="text" name="t_name" id="t_name" class="form-control" value="<?php echo ($getRowDetails['fullname'])?$objCommon->html2text($getRowDetails['fullname']):''?>" placeholder="Enter Name" required >
                            </div>
                             <div class="form-group">
                                <label for="">Profession</label>
                                <input type="text" name="t_prof" id="t_prof" class="form-control" value="<?php echo ($getRowDetails['profession'])?$objCommon->html2text($getRowDetails['profession']):''?>" placeholder="Enter Profession" required >
                            </div>
                             <div class="form-group">
                                <label for="">Place</label>
                                <input type="text" name="t_place" id="t_place" class="form-control" value="<?php echo ($getRowDetails['place'])?$objCommon->html2text($getRowDetails['place']):''?>" placeholder="Enter Place" required >
                            </div>
                                    
							<div class="form-group">
								<label for="">Text</label>
							</div>
							<div class="form-group">
								<textarea name="t_descr" class="form-control" required ><?php echo ($getRowDetails['text'])?$objCommon->html2text($getRowDetails['text']):''?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Image</label>
                                <input type="file" name="t_image" id="t_image" > 
                            </div>
												
                            <input type="hidden" name="editId" value="<?php echo $nId?>" />
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                    </div>
                </section>
            </div>
            <div class="col-lg-8">
                <section class="panel">
                    <header class="panel-heading">Testimonials</header>
                    <div class="panel-body">
                    	<div class="row">
                        	<div class="col-md-6 col-md-offset-6 ">
                                <form class="form-search pull-right" method="get" action="">
                                   	<input type="text" class="input-large search-query" name="search" value="">
                                    <input type="hidden" name="page" value="testimonial" />
                               		 <button class="btn btn-primary search_submit" type="submit">Search</button>
                                </form>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="10%">No</th>
                                        <th width="30%">Name</th>
                                        <th width="30%">Profession</th>
                                        <th width="20%">Place</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    if(count($contentList)>0){
                                    $i=1;
                                    foreach($contentList as $list){?>
                                     <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $objCommon->html2text($list['fullname']); ?></td></td>
                                         <td><?php echo $objCommon->html2text($list['profession']); ?></td></td>
                                         <td><?php echo $objCommon->html2text($list['place']); ?></td></td>
                                        
                                        <td>
                                    <a href="?page=testimonial&nId=<?php echo $list['testi_id']?>" class="actionLink " title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                    <a href="?page=testimonial&dId=<?php echo $list['testi_id']?>" onclick="return confirm('You want to delete..?');" class="actionLink" title="Delete"><i class="fa fa-trash-o"></i></a>
                                        </td>                	
                                    </tr>
                                    <?php $i++;}
                                    }else{?>
                                    <tr>
                                        <td colspan="5">There is no results found.. </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                                
                            </table>
        <div class="paginationDiv"></div>
        </div>
                    </div>
                </section>

            </div>
        </div>
<script language="javascript" type="application/javascript">
var Script = function () {
    $.validator.setDefaults({
        submitHandler: function() { alert("submitted!"); }
    });

    $().ready(function() {
        // validate the comment form when it is submitted
        $("#add_testimonial").validate();
    });
}();
</script>