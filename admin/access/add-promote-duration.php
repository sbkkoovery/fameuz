<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/promotion_based_duration.php");
include_once("../../class/common_class.php");
$objPromotionDuration	   =	new promotion_based_duration();
$objCommon		 		  =	new common();
$objCommon->adminCheck();
$editId			 		 =	$objCommon->esc($_POST['editId']);
if(isset($_POST['pbd_name'],$_POST['pbd_days'])&& $_POST['pbd_name']!="" && $_POST['pbd_days']!="" ){
	if($editId){
		$objPromotionDuration->update($_POST,"pbd_id=".$editId);
		$objCommon->addMsg("Promotion duration updated successfully",1);
	}else{
		$objPromotionDuration->insert($_POST);
		$objCommon->addMsg("Promotion duration  added successfully",1);
	}
	header("location:../index.php?page=promote_duration");
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>