<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/model_jacket.php");
include_once("../../class/common_class.php");
$objModelJacket		  =	new model_jacket();
$objCommon			  =	new common();
$objCommon->adminCheck();
$editId				 =	$objCommon->esc($_POST['editId']);
if(isset($_POST['mj_name'])&&$_POST['mj_name']!=""){
	$_POST['mj_name']    =	$objCommon->esc($_POST['mj_name']);
	if($editId){
		$objModelJacket->update($_POST,"mj_id=".$editId);
		$objCommon->addMsg("Content  updated successfully",1);
	}else{
		$objModelJacket->insert($_POST);
		$objCommon->addMsg("Content  added successfully",1);
	}
	header("location:../index.php?page=model_jacket");
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>