<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/pages.php");
include_once("../../class/common_class.php");
$objPages		  =	new pages();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['page_title'])&&$_POST['page_title']!=""){
	$_POST['page_title']	=	$objCommon->esc($_POST['page_title']);
	$_POST['page_descr']	=	$objCommon->esc($_POST['page_descr']);
	$_POST['page_createdon']=	date("Y-m-d H:i:s");
	$_POST['page_status']   =	$objCommon->esc($_POST['page_status']);
	if($editId){
		$objPages->update($_POST,"page_id=".$editId);
		$objCommon->addMsg("page  updated successfully",1);
	}else{
		$_POST['page_alias']	=	$objCommon->getAlias($_POST['page_title']);
		$objPages->insert($_POST);
		$objCommon->addMsg("page  added successfully",1);
	}
	header("location:../index.php?page=page-list");
	exit();
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>