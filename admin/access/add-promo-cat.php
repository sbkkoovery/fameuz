<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/promotion_based_category.php");
include_once("../../class/common_class.php");
$objpromocat	=	new promotion_based_category();
$objCommon			  =	new common();
$objCommon->adminCheck();
if(isset($_POST['prom_pro'])&&$_POST['prom_pro']!=""){
	$_POST['pbc_profile']	=	$objCommon->esc($_POST['prom_pro']);
	$_POST['pbc_video']	=	$objCommon->esc($_POST['prom_vid']);
	$_POST['pbc_music']	  =	$objCommon->esc($_POST['prom_mus']);
	
	$objpromocat->update($_POST,"pbc_id=1");
	$objCommon->addMsg("Changes updated successfully",1);
	header("location:../index.php?page=promote_category");
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>