<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/model_shoes.php");
include_once("../../class/common_class.php");
$objModelShoes		 =	new model_shoes();
$objCommon			  =	new common();
$objCommon->adminCheck();
$editId				 =	$objCommon->esc($_POST['editId']);
if(isset($_POST['ms_name'])&&$_POST['ms_name']!=""){
	$_POST['ms_name']    =	$objCommon->esc($_POST['ms_name']);
	if($editId){
		$objModelShoes->update($_POST,"ms_id=".$editId);
		$objCommon->addMsg("Content  updated successfully",1);
	}else{
		$objModelShoes->insert($_POST);
		$objCommon->addMsg("Content  added successfully",1);
	}
	header("location:../index.php?page=model_shoes");
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>