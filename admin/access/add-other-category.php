<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/category.php");
include_once("../../class/other_categories.php");
include_once("../../class/user_categories.php");
include_once("../../class/common_class.php");
$objCategory		=	new category();
$objOtherCategory	=	new other_categories();
$objUserCategory	=	new user_categories();
$objCommon		 =	new common();
$objCommon->adminCheck();
$otherId			=	$objCommon->esc($_POST['otherId']);
$catDetails			=	$objOtherCategory->getRow("other_id=$otherId");
if(isset($_POST['c_name'],$_POST['mc_id'],$_POST['book_by_me'],$_POST['book_me'])&& $_POST['c_name']!="" && $_POST['mc_id']!="" && $_POST['book_by_me'] !='' && $_POST['book_me'] !=''){
	$_POST['c_name']	  		  =	$objCommon->esc($_POST['c_name']);
	$_POST['c_alias']	 		 =	$objCommon->getAlias($_POST['c_name']);
	$_POST['mc_id']	   		   =	$objCommon->esc($_POST['mc_id']);
	$_POST['c_book_by_me']		=	$objCommon->esc($_POST['book_by_me']);
	$_POST['c_book_me']   		   =	$objCommon->esc($_POST['book_me']);
	$_POST['c_model_details_yes'] =	($_POST['c_model_details_yes'])?$objCommon->esc($_POST['c_model_details_yes']):0;
	if($editId){
		$objCategory->update($_POST,"c_id=".$editId);
		$objCommon->addMsg("Category  updated successfully",1);
	}
	$existCat	=	$objCategory->getRow("c_alias='".$_POST['c_alias']."'");
	if(count($existCat)==1) {
		$objCategory->insert($_POST);
		$catId		=	$objCategory->insertId();
		$objUserCategory->updateField(array("uc_c_id"=>$catId),"user_id=".$catDetails['user_id']);
		$objOtherCategory->updateField(array("other_status"=>1),"other_id=$otherId");
		$objCommon->addMsg("Category  approved and added successfully", 1);
		header("location:../index.php?page=category");
		exit();
	}else{
		$objCommon->addMsg("This Category is already exist",0);
	}
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>