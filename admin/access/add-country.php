<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/country.php");
include_once("../../class/common_class.php");
$objCountry		=	new country();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['country_name'])&&$_POST['country_name']!=""){
	$_POST['country_code']	=	$objCommon->esc($_POST['country_code']);
	$_POST['country_name']	=	$objCommon->esc($_POST['country_name']);
	if($editId){
		$objCountry->update($_POST,"country_id=".$editId);
		$objCommon->addMsg("Country  updated successfully",1);
	}else{
		$objCountry->insert($_POST);
		$objCommon->addMsg("Country  added successfully",1);
	}
	header("location:../index.php?page=country");
	exit();
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>