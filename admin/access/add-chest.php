<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/model_chest.php");
include_once("../../class/common_class.php");
$objModelChest		  =	new model_chest();
$objCommon			  =	new common();
$objCommon->adminCheck();
$editId				 =	$objCommon->esc($_POST['editId']);
if(isset($_POST['mc_name'])&&$_POST['mc_name']!=""){
	$_POST['mc_name']   =	$objCommon->esc($_POST['mc_name']);
	if($editId){
		$objModelChest->update($_POST,"mc_id=".$editId);
		$objCommon->addMsg("Content  updated successfully",1);
	}else{
		$objModelChest->insert($_POST);
		$objCommon->addMsg("Content  added successfully",1);
	}
	header("location:../index.php?page=model_chest");
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>