<?php 
@session_start();
include_once("../../class/promote_profile.php");
include_once("../../class/common_class.php");
$objPromoteprofile		  =	new promote_profile();
$objCommon			   =	new common();
$objCommon->adminCheck();
if(isset($_POST['pro-bud'])&&$_POST['pro-bud']!=""){
	$_POST['type'] = 'profile';
	$_POST['budget']    =	$objCommon->esc($_POST['pro-bud']);
	$_POST['location']	=	$objCommon->esc($_POST['pro-loc']);
	$_POST['categories']	=	$objCommon->esc($_POST['pro-cat']);
	$_POST['promotefrom']	=	$objCommon->esc($_POST['pro-from']);
	$_POST['promoteto']	=	$objCommon->esc($_POST['pro-to']);
	/*if($editId){
		$objPromoteprofile->update($_POST,"promote_id=".$editId);
		$objCommon->addMsg("Profile updated successfully",1);
	}else{*/
		$objPromoteprofile->insert($_POST);
		$objCommon->addMsg("Profile added successfully",1);
	/*}*/
	header("location:../index.php?page=promote_profile");
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>