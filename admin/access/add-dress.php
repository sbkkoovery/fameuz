<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/model_dress.php");
include_once("../../class/common_class.php");
$objModelDress		  =	new model_dress();
$objCommon			  =	new common();
$objCommon->adminCheck();
$editId				 =	$objCommon->esc($_POST['editId']);
if(isset($_POST['mdress_name'])&&$_POST['mdress_name']!=""){
	$_POST['mdress_name']    =	$objCommon->esc($_POST['mdress_name']);
	if($editId){
		$objModelDress->update($_POST,"mdress_id=".$editId);
		$objCommon->addMsg("Content  updated successfully",1);
	}else{
		$objModelDress->insert($_POST);
		$objCommon->addMsg("Content  added successfully",1);
	}
	header("location:../index.php?page=model_dress");
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>