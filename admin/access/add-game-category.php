<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/game_category.php");
include_once("../../class/common_class.php");
$objGameCategory	=	new game_category();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['gc_name'])&&$_POST['gc_name']!=""){
	$_POST['gc_name']	=	$objCommon->esc($_POST['gc_name']);
	$_POST['gc_alias']   =	$objCommon->getAlias($_POST['gc_name']);
	if($editId){
		$objGameCategory->update($_POST,"gc_id=".$editId);
		$objCommon->addMsg("Category  updated successfully",1);
	}else{
		$objGameCategory->insert($_POST);
		$objCommon->addMsg("Category  added successfully",1);
	}
	header("location:../index.php?page=game-category");
	exit();
}else{
	$objCommon->addMsg("Please enter Category",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>