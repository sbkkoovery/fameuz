<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/most_wated_category.php");
include_once("../../class/common_class.php");
$objMostCategory	=	new most_wated_category();
$objCommon		  =	new common();
$objCommon->adminCheck();
if(count($_POST['c_id']) >0){
	$cId					=	$_POST['c_id'];
	$cId					=	array_filter($cId);
	$_POST['mwc_category']  =	implode(",",$cId);
	$objMostCategory->update($_POST,"mwc_id=1");
	$objCommon->addMsg("Category  added successfully",1);
	header("location:../index.php?page=most-wanted-category");
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>