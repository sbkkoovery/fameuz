<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/promotion_based_location.php");
include_once("../../class/common_class.php");
$objpromolocation	=	new promotion_based_location();
$objCommon			  =	new common();
$objCommon->adminCheck();
if(isset($_POST['prom_pro'])&&$_POST['prom_pro']!=""){
	$_POST['pbl_profile']	=	$objCommon->esc($_POST['prom_pro']);
	$_POST['pbl_video']	=	$objCommon->esc($_POST['prom_vid']);
	$_POST['pbl_music']	  =	$objCommon->esc($_POST['prom_mus']);
	
	$objpromolocation->update($_POST,"pbl_id=1");
	$objCommon->addMsg("Changes updated successfully",1);
	header("location:../index.php?page=promote_location");
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>