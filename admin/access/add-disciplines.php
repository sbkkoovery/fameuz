<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/model_disciplines.php");
include_once("../../class/common_class.php");
$objModelDisciplines	=	new model_disciplines();
$objCommon			  =	new common();
$objCommon->adminCheck();
$editId				 =	$objCommon->esc($_POST['editId']);
if(isset($_POST['model_dis_name'])&&$_POST['model_dis_name']!=""){
	$_POST['model_dis_name']	=	$objCommon->esc($_POST['model_dis_name']);
	if($editId){
		$objModelDisciplines->update($_POST,"model_dis_id=".$editId);
		$objCommon->addMsg("Disciplines  updated successfully",1);
	}else{
		$objModelDisciplines->insert($_POST);
		$objCommon->addMsg("Disciplines  added successfully",1);
	}
	header("location:../index.php?page=model_disciplines");
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>