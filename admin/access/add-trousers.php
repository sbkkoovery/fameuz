<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/model_trousers.php");
include_once("../../class/common_class.php");
$objModelTrousers	   =	new model_trousers();
$objCommon			  =	new common();
$objCommon->adminCheck();
$editId				 =	$objCommon->esc($_POST['editId']);
if(isset($_POST['mt_name'])&&$_POST['mt_name']!=""){
	$_POST['mt_name']    =	$objCommon->esc($_POST['mt_name']);
	if($editId){
		$objModelTrousers->update($_POST,"mt_id=".$editId);
		$objCommon->addMsg("Content  updated successfully",1);
	}else{
		$objModelTrousers->insert($_POST);
		$objCommon->addMsg("Content  added successfully",1);
	}
	header("location:../index.php?page=model_trousers");
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>