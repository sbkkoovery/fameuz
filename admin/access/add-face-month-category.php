<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/face_month_category.php");
include_once("../../class/common_class.php");
$objMostCategory	=	new face_month_category();
$objCommon		  =	new common();
$objCommon->adminCheck();
if(count($_POST['c_id']) >0){
	$cId					=	$_POST['c_id'];
	$cId					=	array_filter($cId);
	$_POST['fmc_category']  =	implode(",",$cId);
	$objMostCategory->update($_POST,"fmc_id=1");
	$objCommon->addMsg("Category  added successfully",1);
	header("location:../index.php?page=face-month-category");
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>