<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/sub_category.php");
include_once("../../class/common_class.php");
$objSubCategory	=	new sub_category();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['s_name'],$_POST['c_id'])&&$_POST['s_name']!="" &&$_POST['c_id']!=""){
	$_POST['s_name']	=	$objCommon->esc($_POST['s_name']);
	$_POST['c_id']	  =	$objCommon->esc($_POST['c_id']);
	if($editId){
		$objSubCategory->update($_POST,"s_id=".$editId);
		$objCommon->addMsg("Category  updated successfully",1);
	}else{
		$objSubCategory->insert($_POST);
		$objCommon->addMsg("Category  added successfully",1);
	}
	header("location:../index.php?page=sub-category");
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>