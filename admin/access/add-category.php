<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/category.php");
include_once("../../class/common_class.php");
$objCategory	=	new category();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['c_name'],$_POST['mc_id'],$_POST['book_by_me'],$_POST['book_me'])&& $_POST['c_name']!="" && $_POST['mc_id']!="" && $_POST['book_by_me'] !='' && $_POST['book_me'] !=''){
	$_POST['c_name']	  		  =	$objCommon->esc($_POST['c_name']);
	$_POST['c_alias']	 		 =	$objCommon->getAlias($_POST['c_name']);
	$_POST['mc_id']	   		   =	$objCommon->esc($_POST['mc_id']);
	$_POST['c_book_by_me']		=	$objCommon->esc($_POST['book_by_me']);
	$_POST['c_book_me']   		   =	$objCommon->esc($_POST['book_me']);
	$_POST['c_model_details_yes'] =	($_POST['c_model_details_yes'])?$objCommon->esc($_POST['c_model_details_yes']):0;
	if($editId){
		$objCategory->update($_POST,"c_id=".$editId);
		$objCommon->addMsg("Category  updated successfully",1);
	}else{
		$objCategory->insert($_POST);
		$objCommon->addMsg("Category  added successfully",1);
	}
	header("location:../index.php?page=category");
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>