<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/model_waist.php");
include_once("../../class/common_class.php");
$objModelWaist	   	  =	new model_waist();
$objCommon			  =	new common();
$objCommon->adminCheck();
$editId				 =	$objCommon->esc($_POST['editId']);
if(isset($_POST['mw_name'])&&$_POST['mw_name']!=""){
	$_POST['mw_name']    =	$objCommon->esc($_POST['mw_name']);
	if($editId){
		$objModelWaist->update($_POST,"mw_id=".$editId);
		$objCommon->addMsg("Content  updated successfully",1);
	}else{
		$objModelWaist->insert($_POST);
		$objCommon->addMsg("Content  added successfully",1);
	}
	header("location:../index.php?page=model_waist");
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>