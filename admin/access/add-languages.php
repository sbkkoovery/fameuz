<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/languages.php");
include_once("../../class/common_class.php");
$objLanguages	  =	new languages();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['languages_name'])&&$_POST['languages_name']!=""){
	$_POST['languages_name']	=	$objCommon->esc($_POST['languages_name']);
	if($editId){
		$objLanguages->update($_POST,"languages_id=".$editId);
		$objCommon->addMsg("Content  updated successfully",1);
	}else{
		$objLanguages->insert($_POST);
		$objCommon->addMsg("Content  added successfully",1);
	}
	header("location:../index.php?page=languages");
	exit();
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>