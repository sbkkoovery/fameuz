<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/model_collar.php");
include_once("../../class/common_class.php");
$objModelCollar		  =	new model_collar();
$objCommon			   =	new common();
$objCommon->adminCheck();
$editId				  =	$objCommon->esc($_POST['editId']);
if(isset($_POST['mcollar_name'])&&$_POST['mcollar_name']!=""){
	$_POST['mcollar_name']    =	$objCommon->esc($_POST['mcollar_name']);
	if($editId){
		$objModelCollar->update($_POST,"mcollar_id=".$editId);
		$objCommon->addMsg("Content  updated successfully",1);
	}else{
		$objModelCollar->insert($_POST);
		$objCommon->addMsg("Content  added successfully",1);
	}
	header("location:../index.php?page=model_collar");
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>