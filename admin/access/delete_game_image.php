<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/games.php");
include_once("../../class/common_class.php");
include_once("../../class/game_descr_images.php");
$objGames	      =	new games();
$objCommon		 =	new common();
$objDescrImages	=	new game_descr_images();
$objCommon->adminCheck();
$type			  =	$objCommon->esc($_GET['type']);
$g_id			  =	$objCommon->esc($_GET['g_id']);
$imgName		   =	$objCommon->esc($_GET['imgName']);
if($type =='g_img'){
	$imgPath			=	'../../uploads/games/img/'.$imgName;
	unlink($imgPath);
	$objGames->updateField(array('g_img'=>' '),"g_id=".$g_id);
}else if($type =='g_banner'){
	$imgPath			=	'../../uploads/games/banner/'.$imgName;
	unlink($imgPath);
	$objGames->updateField(array('g_banner'=>' '),"g_id=".$g_id);
}else if($type =='g_descr_img'){
	$imgPath			=	'../../uploads/games/descr_images/'.$imgName;
	unlink($imgPath);
	$objDescrImages->delete('gdi_id='.$g_id);
}
$objCommon->addMsg("Game images deleted successfully",1);
header("location:".$_SERVER['HTTP_REFERER']);
?>