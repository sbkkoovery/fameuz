<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/ethnicity.php");
include_once("../../class/common_class.php");
$objEthnicity	  =	new ethnicity();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['ethnicity_name'])&&$_POST['ethnicity_name']!=""){
	$_POST['ethnicity_name']	=	$objCommon->esc($_POST['ethnicity_name']);
	if($editId){
		$objEthnicity->update($_POST,"ethnicity_id=".$editId);
		$objCommon->addMsg("Content  updated successfully",1);
	}else{
		$objEthnicity->insert($_POST);
		$objCommon->addMsg("Content  added successfully",1);
	}
	header("location:../index.php?page=ethnicity");
	exit();
}else{
	$objCommon->addMsg("Please enter Fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>