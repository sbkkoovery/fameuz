<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/games.php");
include_once("../../class/common_class.php");
include_once("../../class/game_descr_images.php");
include("resize-class.php");
$objGames	      =	new games();
$objCommon		 =	new common();
$objDescrImages	=	new game_descr_images();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
$path			  =	'../../uploads/games/';
$todayDate		 =	date('Y-m-d');
$time			  =	time();
include_once("../../amazone_s3/S3.php");
$s3			=	new S3(AWS_ID,AWS_KEY);
if(isset($_POST['g_name'],$_POST['gc_id'])&& $_POST['g_name']!="" && $_POST['gc_id']!=""){
	$_POST['g_name']	  =	$objCommon->esc($_POST['g_name']);
	$_POST['g_alias']     =	$objCommon->getAlias($_POST['g_name']);
	$_POST['gc_id']	   =	$objCommon->esc($_POST['gc_id']);
	$_POST['g_descr']	 =	$objCommon->esc($_POST['g_descr']);
	if($editId){
			if($_FILES['g_file']['tmp_name']){
			if(!file_exists($path.'file/'.$todayDate)){
				mkdir($path.'file/'.$todayDate);
			}
			$pathFile	=	$path.'file/'.$todayDate.'/';
			$ext	 	  =	pathinfo($_FILES['g_file']['name'], PATHINFO_EXTENSION);
			$actual_image_name 		   = 	$time.".".$ext;
			if(move_uploaded_file($_FILES['g_file']['tmp_name'], $pathFile.$actual_image_name)){
					//----------------start upload to AWS----------------------------------------
					S3::putObjectFile($pathFile.$actual_image_name,AWS_IMG_BUCKET,'uploads/games/file/'.$todayDate.'/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
					//----------------end upload to AWS-------------------------------------------
				$_POST['g_file']		 =	$todayDate."/".$actual_image_name;
			}
		}
		if($_FILES['g_img']['tmp_name']){
			if(!file_exists($path.'img/'.$todayDate)){
				mkdir($path.'img/'.$todayDate);
			}
			$pathImg					 =	$path.'img/'.$todayDate.'/';
			if(!file_exists($pathImg."thumb")){
				mkdir($pathImg."thumb");
			}
			$extImg	 				  =	pathinfo($_FILES['g_img']['name'], PATHINFO_EXTENSION);
			$validImages				  =	array("jpg","jpeg","gif","png");
			if(in_array($extImg,$validImages)){
				$actual_image_nameImg 		= 	$time;
				$g_imgnew			        =	$objCommon->addIMG($_FILES['g_img'],$pathImg,$actual_image_nameImg,800,800,false);
				$_POST['g_img']		  	  =	$todayDate."/".$g_imgnew;
				$objCommon->addIMG($_FILES['g_img'],$pathImg.'thumb/',$actual_image_nameImg,200,200,true);
				S3::putObjectFile($pathImg.$actual_image_nameImg.".".$extImg,AWS_IMG_BUCKET,'uploads/games/img/'.$todayDate.'/'.$actual_image_nameImg.".".$extImg,S3::ACL_PUBLIC_READ,array());
				S3::putObjectFile($pathImg.'thumb/'.$actual_image_nameImg.".".$extImg,AWS_IMG_BUCKET,'uploads/games/img/'.$todayDate.'/thumb/'.$actual_image_nameImg.".".$extImg,S3::ACL_PUBLIC_READ,array());
			}
		}
		if($_FILES['g_banner']['tmp_name']){
			if(!file_exists($path.'banner/'.$todayDate)){
				mkdir($path.'banner/'.$todayDate);
			}
			$pathImg					 =	$path.'banner/'.$todayDate.'/';
			if(!file_exists($pathImg."thumb")){
				mkdir($pathImg."thumb");
			}
			$extImg	 				  =	pathinfo($_FILES['g_banner']['name'], PATHINFO_EXTENSION);
			$validImages				  =	array("jpg","jpeg","gif","png");
			if(in_array($extImg,$validImages)){
				$actual_image_nameImg 		= 	$time;
				$g_imgnew			        =	$objCommon->addIMG($_FILES['g_banner'],$pathImg,$actual_image_nameImg,820,820,false);
				$_POST['g_banner']		   =	$todayDate."/".$g_imgnew;
				$objCommon->addIMG($_FILES['g_banner'],$pathImg.'thumb/',$actual_image_nameImg,200,200,true);
				S3::putObjectFile($pathImg.$actual_image_nameImg.".".$extImg,AWS_IMG_BUCKET,'uploads/games/banner/'.$todayDate.'/'.$actual_image_nameImg.".".$extImg,S3::ACL_PUBLIC_READ,array());
				S3::putObjectFile($pathImg.'thumb/'.$actual_image_nameImg.".".$extImg,AWS_IMG_BUCKET,'uploads/games/banner/'.$todayDate.'/thumb/'.$actual_image_nameImg.".".$extImg,S3::ACL_PUBLIC_READ,array());
			}
		}
		//---------------------------------------------------------------------------------------------------
		if(count($_FILES['g_descr_imgs']['name'])>0){	
			$todayDate		  =	date('Y-m-d');
			if(!file_exists($path.'descr_images/'.$todayDate)){
				mkdir($path.'descr_images/'.$todayDate);
			}
			$pathNew			=	$path.'descr_images/'.$todayDate.'/';
			if(!file_exists($pathNew."original")){
				mkdir($pathNew."original");
			}
			$valid_img_formats 	  =	array("jpg", "png", "gif", "bmp","jpeg","JPG", "PNG", "GIF", "BMP","JPEG");
			$countIter			  =	0;
			$_POST['g_id']		  =	$editId;
			foreach($_FILES['g_descr_imgs']['tmp_name'] as $i => $tmp_name ){ 
				if($countIter <= $i){
				$ext 				= 	pathinfo($_FILES['g_descr_imgs']['name'][$i], PATHINFO_EXTENSION);
			if(in_array($ext,$valid_img_formats)){
				$time		   		   =	time();
				$actual_image_name 	  = 	$time.'_'.$i.".".$ext;
				if(move_uploaded_file($_FILES['g_descr_imgs']['tmp_name'][$i], $pathNew.'original/'.$actual_image_name)){
					$resizeObj 		  =	new resize($pathNew.'original/'.$actual_image_name);
					$resizeObj -> resizeImage(300, 195, 'auto');
					$resizeObj -> saveImage($pathNew.$actual_image_name, 100);
					S3::putObjectFile($pathNew.$actual_image_name,AWS_IMG_BUCKET,'uploads/games/descr_images/'.$todayDate.'/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
					$_POST['gdi_url']	=	$todayDate."/".$actual_image_name;
					$objDescrImages->insert($_POST);
				}
			}
			$countIter++;
			}
		}
	
		}
//----------------------------------------------------------------------------------------------------------
		$objGames->update($_POST,"g_id=".$editId);
		$objCommon->addMsg("Category  updated successfully",1);
	}else{
		$_POST['g_status']	=	1;
		$_POST['g_created']   =	date("Y-m-d H:i:s");
		if($_FILES['g_file']['tmp_name']){
			if(!file_exists($path.'file/'.$todayDate)){
				mkdir($path.'file/'.$todayDate);
			}
			$pathFile	=	$path.'file/'.$todayDate.'/';
			$ext	 	  =	pathinfo($_FILES['g_file']['name'], PATHINFO_EXTENSION);
			$actual_image_name 		   = 	$time.".".$ext;
			if(move_uploaded_file($_FILES['g_file']['tmp_name'], $pathFile.$actual_image_name)){
				S3::putObjectFile($pathFile.$actual_image_name,AWS_IMG_BUCKET,'uploads/games/file/'.$todayDate.'/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
				$_POST['g_file']		 =	$todayDate."/".$actual_image_name;
			}
		}
		if($_FILES['g_img']['tmp_name']){
			if(!file_exists($path.'img/'.$todayDate)){
				mkdir($path.'img/'.$todayDate);
			}
			$pathImg					 =	$path.'img/'.$todayDate.'/';
			if(!file_exists($pathImg."thumb")){
				mkdir($pathImg."thumb");
			}
			$extImg	 				  =	pathinfo($_FILES['g_img']['name'], PATHINFO_EXTENSION);
			$validImages				  =	array("jpg","jpeg","gif","png");
			if(in_array($extImg,$validImages)){
				$actual_image_nameImg 		= 	$time;
				$g_imgnew			        =	$objCommon->addIMG($_FILES['g_img'],$pathImg,$actual_image_nameImg,800,800,false);
				$_POST['g_img']		  	  =	$todayDate."/".$g_imgnew;
				$objCommon->addIMG($_FILES['g_img'],$pathImg.'thumb/',$actual_image_nameImg,200,200,true);
				S3::putObjectFile($pathImg.$actual_image_nameImg.".".$extImg,AWS_IMG_BUCKET,'uploads/games/img/'.$todayDate.'/'.$actual_image_nameImg.".".$extImg,S3::ACL_PUBLIC_READ,array());
				S3::putObjectFile($pathImg.'thumb/'.$actual_image_nameImg.".".$extImg,AWS_IMG_BUCKET,'uploads/games/img/'.$todayDate.'/thumb/'.$actual_image_nameImg.".".$extImg,S3::ACL_PUBLIC_READ,array());
			}
		}
		if($_FILES['g_banner']['tmp_name']){
			if(!file_exists($path.'banner/'.$todayDate)){
				mkdir($path.'banner/'.$todayDate);
			}
			$pathImg					 =	$path.'banner/'.$todayDate.'/';
			if(!file_exists($pathImg."thumb")){
				mkdir($pathImg."thumb");
			}
			$extImg	 				  =	pathinfo($_FILES['g_banner']['name'], PATHINFO_EXTENSION);
			$validImages				  =	array("jpg","jpeg","gif","png");
			if(in_array($extImg,$validImages)){
				$actual_image_nameImg 		= 	$time;
				$g_imgnew			        =	$objCommon->addIMG($_FILES['g_banner'],$pathImg,$actual_image_nameImg,820,820,false);
				$_POST['g_banner']		   =	$todayDate."/".$g_imgnew;
				$objCommon->addIMG($_FILES['g_banner'],$pathImg.'thumb/',$actual_image_nameImg,200,200,true);
				S3::putObjectFile($pathImg.$actual_image_nameImg.".".$extImg,AWS_IMG_BUCKET,'uploads/games/banner/'.$todayDate.'/'.$actual_image_nameImg.".".$extImg,S3::ACL_PUBLIC_READ,array());
				S3::putObjectFile($pathImg.'thumb/'.$actual_image_nameImg.".".$extImg,AWS_IMG_BUCKET,'uploads/games/banner/'.$todayDate.'/thumb/'.$actual_image_nameImg.".".$extImg,S3::ACL_PUBLIC_READ,array());
			}
		}
		$objGames->insert($_POST);
		$lastGameId						  =	$objGames->insertId();
//---------------------------------------------------------------------------------------------------
		if(count($_FILES['g_descr_imgs']['name'])>0){	
			$todayDate		  =	date('Y-m-d');
			if(!file_exists($path.'descr_images/'.$todayDate)){
				mkdir($path.'descr_images/'.$todayDate);
			}
			$pathNew			=	$path.'descr_images/'.$todayDate.'/';
			if(!file_exists($pathNew."original")){
				mkdir($pathNew."original");
			}
			$valid_img_formats 	  =	array("jpg", "png", "gif", "bmp","jpeg","JPG", "PNG", "GIF", "BMP","JPEG");
			$countIter			  =	0;
			$_POST['g_id']		  =	$lastGameId;
			foreach($_FILES['g_descr_imgs']['tmp_name'] as $i => $tmp_name ){ 
				if($countIter <= $i){
				$ext 				= 	pathinfo($_FILES['g_descr_imgs']['name'][$i], PATHINFO_EXTENSION);
			if(in_array($ext,$valid_img_formats)){
				$time		   		   =	time();
				$actual_image_name 	  = 	$time.'_'.$i.".".$ext;
				if(move_uploaded_file($_FILES['g_descr_imgs']['tmp_name'][$i], $pathNew.'original/'.$actual_image_name)){
					$resizeObj 		  =	new resize($pathNew.'original/'.$actual_image_name);
					$resizeObj -> resizeImage(300, 195, 'auto');
					$resizeObj -> saveImage($pathNew.$actual_image_name, 100);
					S3::putObjectFile($pathNew.$actual_image_name,AWS_IMG_BUCKET,'uploads/games/descr_images/'.$todayDate.'/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
					$_POST['gdi_url']	=	$todayDate."/".$actual_image_name;
					$objDescrImages->insert($_POST);
				}
			}
			$countIter++;
			}
		}
	
		}
//----------------------------------------------------------------------------------------------------------
		$objCommon->addMsg("game  added successfully",1);
	}
	header("location:".$_SERVER['HTTP_REFERER']);
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>