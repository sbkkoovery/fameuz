<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/testimonial.php");
include_once("../../class/common_class.php");
$objCategory	=	new testimonial();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
$path			  =	'../../uploads/testimonial/';
$todayDate		 =	date('Y-m-d');
$time			  =	time();
if(isset($_POST['t_name'],$_POST['t_prof'],$_POST['t_place'],$_POST['t_descr'])&& $_POST['t_name']!="" && $_POST['t_prof']!="" && $_POST['t_place'] !='' && $_POST['t_descr'] !=''){
	$_POST['fullname']	  		  =	$objCommon->esc($_POST['t_name']);
	$_POST['profession']	 		 =	$objCommon->esc($_POST['t_prof']);
	$_POST['place']	   		   =	$objCommon->esc($_POST['t_place']);
	$_POST['text']		=	$objCommon->esc($_POST['t_descr']);
	
	if($editId){
		if($_FILES['t_image']['tmp_name']){
			if(!file_exists($path.'img/'.$todayDate)){
				mkdir($path.'imgs/'.$todayDate);
			}
			$pathImg					 =	$path.'imgs/'.$todayDate.'/';
			if(!file_exists($pathImg."thumb")){
				mkdir($pathImg."thumb");
			}
			$extImg	 				  =	pathinfo($_FILES['t_image']['name'], PATHINFO_EXTENSION);
			$validImages				  =	array("jpg","jpeg","gif","png");
			if(in_array($extImg,$validImages)){
				$actual_image_nameImg 		= 	$time;
				$g_imgnew			        =	$objCommon->addIMG($_FILES['t_image'],$pathImg,$actual_image_nameImg,800,800,false);
				$_POST['t_image']		  	  =	$todayDate."/".$g_imgnew;
				$objCommon->addIMG($_FILES['t_image'],$pathImg.'thumb/',$actual_image_nameImg,200,200,true);
			}
		}
		$objCategory->update($_POST,"testi_id=".$editId);
		$objCommon->addMsg("Testimonial updated successfully",1);
		
	}else{
		if($_FILES['t_image']['tmp_name']){
			if(!file_exists($path.'imgs/'.$todayDate)){
				mkdir($path.'imgs/'.$todayDate);
			}
			$pathImg					 =	$path.'imgs/'.$todayDate.'/';
			if(!file_exists($pathImg."thumb")){
				mkdir($pathImg."thumb");
			}
			$extImg	 				  =	pathinfo($_FILES['t_image']['name'], PATHINFO_EXTENSION);
			$validImages				  =	array("jpg","jpeg","gif","png");
			if(in_array($extImg,$validImages)){
				$actual_image_nameImg 		= 	$time;
				$g_imgnew			        =	$objCommon->addIMG($_FILES['t_image'],$pathImg,$actual_image_nameImg,800,800,false);
				$_POST['t_image']		  	  =	$todayDate."/".$g_imgnew;
				$objCommon->addIMG($_FILES['t_image'],$pathImg.'thumb/',$actual_image_nameImg,200,200,true);
			}
		}
		$objCategory->insert($_POST);
		$objCommon->addMsg("Testimonial added successfully",1);
	}
	header("location:../index.php?page=testimonial");
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>