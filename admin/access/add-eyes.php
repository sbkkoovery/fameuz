<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/model_eyes.php");
include_once("../../class/common_class.php");
$objModelEyes		  =	new model_eyes();
$objCommon			  =	new common();
$objCommon->adminCheck();
$editId				 =	$objCommon->esc($_POST['editId']);
if(isset($_POST['me_name'])&&$_POST['me_name']!=""){
	$_POST['me_name']    =	$objCommon->esc($_POST['me_name']);
	if($editId){
		$objModelEyes->update($_POST,"me_id=".$editId);
		$objCommon->addMsg("Content  updated successfully",1);
	}else{
		$objModelEyes->insert($_POST);
		$objCommon->addMsg("Content  added successfully",1);
	}
	header("location:../index.php?page=model_eyes");
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>