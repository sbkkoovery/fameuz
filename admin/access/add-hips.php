<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/model_hips.php");
include_once("../../class/common_class.php");
$objModelHips		  =	new model_hips();
$objCommon			  =	new common();
$objCommon->adminCheck();
$editId				 =	$objCommon->esc($_POST['editId']);
if(isset($_POST['mhp_name'])&&$_POST['mhp_name']!=""){
	$_POST['mhp_name']    =	$objCommon->esc($_POST['mhp_name']);
	if($editId){
		$objModelHips->update($_POST,"mhp_id=".$editId);
		$objCommon->addMsg("Content  updated successfully",1);
	}else{
		$objModelHips->insert($_POST);
		$objCommon->addMsg("Content  added successfully",1);
	}
	header("location:../index.php?page=model_hips");
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>