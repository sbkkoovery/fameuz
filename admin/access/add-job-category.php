<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/job_category.php");
include_once("../../class/common_class.php");
$objJobCategory		  =	new job_category();
$objCommon			   =	new common();
$objCommon->adminCheck();
$editId				  =	$objCommon->esc($_POST['editId']);
if(isset($_POST['jc_name'])&&$_POST['jc_name']!=""){
	$_POST['jc_name']    =	$objCommon->esc($_POST['jc_name']);
	$_POST['jc_order']	=	($_POST['jc_order'])?$_POST['jc_order']:1;
	if($editId){
		$objJobCategory->update($_POST,"jc_id=".$editId);
		$objCommon->addMsg("Content  updated successfully",1);
	}else{
		$objJobCategory->insert($_POST);
		$objCommon->addMsg("Content  added successfully",1);
	}
	header("location:../index.php?page=job_category");
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>