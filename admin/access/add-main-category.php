<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/main_category.php");
include_once("../../class/common_class.php");
$objMainCategory	=	new main_category();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['mc_name'])&&$_POST['mc_name']!=""){
	$_POST['mc_name']	=	$objCommon->esc($_POST['mc_name']);
	if($editId){
		$objMainCategory->update($_POST,"mc_id=".$editId);
		$objCommon->addMsg("Category  updated successfully",1);
	}else{
		$objMainCategory->insert($_POST);
		$objCommon->addMsg("Category  added successfully",1);
	}
	header("location:../index.php?page=main-category");
	exit();
}else{
	$objCommon->addMsg("Please enter Category",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>