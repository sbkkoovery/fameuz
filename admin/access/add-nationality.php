<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/nationality.php");
include_once("../../class/common_class.php");
$objNationality	=	new nationality();
$objCommon		 =	new common();
$objCommon->adminCheck();
$editId			=	$objCommon->esc($_POST['editId']);
if(isset($_POST['n_name'])&&$_POST['n_name']!=""){
	$_POST['n_name']	=	$objCommon->esc($_POST['n_name']);
	if($editId){
		$objNationality->update($_POST,"n_id=".$editId);
		$objCommon->addMsg("Nationality  updated successfully",1);
	}else{
		$objNationality->insert($_POST);
		$objCommon->addMsg("Nationality  added successfully",1);
	}
	header("location:../index.php?page=nationality");
	exit();
}else{
	$objCommon->addMsg("Please enter Nationality",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>