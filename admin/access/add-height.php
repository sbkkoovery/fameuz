<?php 
@session_start();
include_once("../../includes/site_root.php");
include_once("../../class/model_hair.php");
include_once("../../class/common_class.php");
$objModelHair		  =	new model_hair();
$objCommon			  =	new common();
$objCommon->adminCheck();
$editId				 =	$objCommon->esc($_POST['editId']);
if(isset($_POST['mhair_name'])&&$_POST['mhair_name']!=""){
	$_POST['mhair_name']    =	$objCommon->esc($_POST['mhair_name']);
	if($editId){
		$objModelHair->update($_POST,"mhair_id=".$editId);
		$objCommon->addMsg("Content  updated successfully",1);
	}else{
		$objModelHair->insert($_POST);
		$objCommon->addMsg("Content  added successfully",1);
	}
	header("location:../index.php?page=model_hair");
	exit();
}else{
	$objCommon->addMsg("Please fill the fields",0);
}
header("location:".$_SERVER['HTTP_REFERER']);
?>