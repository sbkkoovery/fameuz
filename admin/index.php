<?php 
@session_start();
ob_start();
include_once("../includes/site_root.php");
include_once("../class/common_class.php");
include_once("../class/pagination-class.php");
$objCommon	=	new common();
if(!$objCommon->adminLogin()){
	header("location:login.php");
	exit();
}
$page	=	"dashboard.php";
if(isset($_GET['page'])&&$_GET['page']!=""){
	$page	=	$_GET['page'];
}
$pageName	=	"pages/".$page.".php";
if(!file_exists($pageName)){
	$pageName	=	"pages/dashboard.php";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">
  <link rel="shortcut icon" href="#" type="image/png">
  <title>Admin Fameuz</title>

  <!--icheck-->
  <link href="js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
  <link href="js/iCheck/skins/square/square.css" rel="stylesheet">
  <link href="js/iCheck/skins/square/red.css" rel="stylesheet">
  <link href="js/iCheck/skins/square/blue.css" rel="stylesheet">

  <!--dashboard calendar-->
  <link href="css/clndr.css" rel="stylesheet">


  <!--common-->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">




  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
  <script src="js/jquery-1.10.2.min.js"></script>
  <script src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/jquery.nicescroll.js"></script>
</head>

<body class="sticky-header">
<section>
    <!-- left side start-->
    <div class="left-side sticky-left-side">
        <!--logo and iconic logo start-->
        <div class="logo">
            <a href="index.html"><img src="images/logo.png" alt="" width="110"></a>
        </div>
        <div class="logo-icon text-center">
            <a href="index.html"><img src="images/logo_icon.png" alt=""></a>
        </div>
        <!--logo and iconic logo end-->
        <div class="left-side-inner">
            <!-- visible to small devices only -->
            <div class="visible-xs hidden-sm hidden-md hidden-lg">
                <div class="media logged-user">
                    <img alt="" src="images/photos/user-avatar.png" class="media-object">
                    <div class="media-body">
                        <h4><a href="#">Olesya</a></h4>
                        <span>"Hello There..."</span>
                    </div>
                </div>
                <h5 class="left-nav-title">Account Information</h5>
                <ul class="nav nav-pills nav-stacked custom-nav">
                  <li><a href="#"><i class="fa fa-user"></i> <span>Profile</span></a></li>
                  <li><a href="#"><i class="fa fa-cog"></i> <span>Settings</span></a></li>
                  <li><a href="#"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
                </ul>
            </div>
            <!--sidebar nav start-->
            <ul class="nav nav-pills nav-stacked custom-nav">
                <li class="active"><a href=""><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

                <li><a href=""><i class="fa fa-home"></i> <span>Visit Site</span></a></li>
                
                 <!--   <li class="menu-list"><a href=""><i class="fa fa-envelope"></i> <span>Mail</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="mail.html"> Inbox</a></li>
                        <li><a href="mail_compose.html"> Compose Mail</a></li>
                        <li><a href="mail_view.html"> View Mail</a></li>
                    </ul>
                </li>

                <li class="menu-list"><a href=""><i class="fa fa-file-text"></i> <span>Pages</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="blank_page.html"> All Pages</a></li>
                        <li><a href="boxed_view.html"> Add Page</a></li>
                    </ul>
                </li>
                
            <li class="menu-list"><a href=""><i class="fa fa-users"></i> <span>Users</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="general.html"> All Users</a></li>
                        <li><a href="general.html"> Unverified Users</a></li>
                        <li><a href="buttons.html"> User Categories</a></li>
                        <li><a href="tabs-accordions.html"> Add Category</a></li>
                    </ul>
                </li>
                <li class="menu-list"><a href=""><i class="fa fa-picture-o"></i> <span>Media Library</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="grids.html"> Images</a></li>
                        <li><a href="gallery.html"> Videos</a></li>
                        <li><a href="mail_compose.html"> Abused Medeas</a></li>
                    </ul>
                </li>
                <li class="menu-list <?php echo ($page=='create-ranking' || $page=='create-face-month' || $page == 'invite-games')?'nav-active':'';?>"><a href=""><i class="fa fa-thumb-tack"></i> <span>Cron Job</span></a>
                    <ul class="sub-menu-list">
                        <li <?php echo ($page=='create-ranking')?'class="active"':'';?>><a href="index.php?page=create-ranking"> Create Ranking</a></li>
						<li <?php echo ($page=='create-face-month')?'class="active"':'';?>><a href="index.php?page=create-face-month"> Create face of the Month</a></li>
						<li <?php echo ($page=='invite-games')?'class="active"':'';?>><a href="index.php?page=invite-games"> Invite Games</a></li>
                    </ul>
                </li>-->

               <!-- <li class="menu-list"><a href=""><i class="fa fa-comments"></i> <span>Testimonials</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="form_layouts.html"> Vew Testimonials</a></li>
                        <li><a href="form_advanced_components.html"> Add Testimonials</a></li>
                    </ul>
                </li>-->
                <li class="menu-list <?php echo ($page=='main-category' || $page=='category' || $page=='sub-category' || $page=='other-category')?'nav-active':'';?>"><a href=""><i class="fa fa-tags"></i> <span>Categories</span></a>
                    <ul class="sub-menu-list">
                        <li <?php echo ($page=='main-category')?'class="active"':'';?>><a href="index.php?page=main-category"> Main Category</a></li>
                        <li <?php echo ($page=='category')?'class="active"':'';?>><a href="index.php?page=category"> Category</a></li>
                        <li <?php echo ($page=='sub-category')?'class="active"':'';?>><a href="index.php?page=sub-category">Sub Category</a></li>
                        <li <?php echo ($page=='other-category')?'class="active"':'';?>><a href="index.php?page=other-category">Other Categories</a></li>
                    </ul>
                </li>
                 <li class="menu-list <?php echo ($page=='user_category')?'nav-active':'';?>"><a href=""><i class="fa fa-file-text"></i> <span>Users</span></a>
                    <ul class="sub-menu-list">
                        <li <?php echo ($page=='user_category')?'class="active"':'';?>><a href="index.php?page=user_category"> Users</a></li>
                    </ul>
                </li>
                <li class="menu-list <?php echo ($page=='job_category')?'nav-active':'';?>"><a href=""><i class="fa fa-file-text"></i> <span>Jobs</span></a>
                    <ul class="sub-menu-list">
                        <li <?php echo ($page=='job_category')?'class="active"':'';?>><a href="index.php?page=job_category"> Job Category</a></li>
                    </ul>
                </li>
                   <li class="menu-list <?php echo ($page=='promote_duration' || $page=='promote_location' || $page=='promote_category')?'nav-active':'';?>"><a href=""><i class="fa fa-file-text"></i> <span>Promotion</span></a>
                    <ul class="sub-menu-list">
                        <li <?php echo ($page=='promote_duration')?'class="active"':'';?>><a href="index.php?page=promote_duration"> Based ON Duration</a></li>
                        <li <?php echo ($page=='promote_location')?'class="active"':'';?>><a href="index.php?page=promote_location"> Based ON Location</a></li>
                        <li <?php echo ($page=='promote_category')?'class="active"':'';?>><a href="index.php?page=promote_category"> Based ON Category</a></li>
                    </ul>
                </li>
                 <li class="menu-list <?php echo ($page=='testimonial' || $page=='testimonial' || $page=='testimonial')?'nav-active':'';?>"><a href=""><i class="fa fa-file-text"></i> <span>Testimonial</span></a>
                    <ul class="sub-menu-list">
                        <li <?php echo ($page=='testimonial')?'class="active"':'';?>><a href="index.php?page=testimonial"> Testimonials</a></li>
                    </ul>
                </li>
				<li class="menu-list <?php echo ($page=='model_disciplines' || $page=='model_chest' || $page=='model_collar' || $page=='model_dress' || $page=='model_eyes' || $page=='model_shoes' || $page=='model_hair' || $page=='model_hips' || $page=='model_jacket' || $page=='model_trousers' || $page=='model_waist')?'nav-active':'';?>"><a href=""><i class="fa fa-file-text"></i> <span>Model Settings</span></a>
                    <ul class="sub-menu-list">
                        <li <?php echo ($page=='model_disciplines')?'class="active"':'';?>><a href="index.php?page=model_disciplines"> Model Disciplines</a></li>
						<li <?php echo ($page=='model_chest')?'class="active"':'';?>><a href="index.php?page=model_chest"> Model Chest/Bust</a></li>
						<li <?php echo ($page=='model_collar')?'class="active"':'';?>><a href="index.php?page=model_collar"> Model Collar</a></li>
						<li <?php echo ($page=='model_dress')?'class="active"':'';?>><a href="index.php?page=model_dress"> Model Dress</a></li>
						<li <?php echo ($page=='model_eyes')?'class="active"':'';?>><a href="index.php?page=model_eyes"> Model Eyes</a></li>
						<li <?php echo ($page=='model_hair')?'class="active"':'';?>><a href="index.php?page=model_hair"> Model Hair</a></li>
						<li <?php echo ($page=='model_hips')?'class="active"':'';?>><a href="index.php?page=model_hips"> Model Hips</a></li>
						<li <?php echo ($page=='model_jacket')?'class="active"':'';?>><a href="index.php?page=model_jacket"> Model Jacket</a></li>
						<li <?php echo ($page=='model_shoes')?'class="active"':'';?>><a href="index.php?page=model_shoes"> Model Shoes</a></li>
						<li <?php echo ($page=='model_trousers')?'class="active"':'';?>><a href="index.php?page=model_trousers"> Model Trousers</a></li>
						<li <?php echo ($page=='model_waist')?'class="active"':'';?>><a href="index.php?page=model_waist"> Model Waist</a></li>
                    </ul>
                </li>
				  <li class="menu-list <?php echo ($page=='most-wanted-category' || $page=='face-month-category')?'nav-active':'';?>"><a href=""><i class="fa fa-shield"></i> <span>Most Wanted</span></a>
                    <ul class="sub-menu-list">
                          <li <?php echo ($page=='most-wanted-category')?'class="active"':'';?>><a href="index.php?page=most-wanted-category">Category</a></li>
						  <li <?php echo ($page=='face-month-category')?'class="active"':'';?>><a href="index.php?page=face-month-category">Face Of Month Category</a></li>
                    </ul>
                </li>
				<li class="menu-list <?php echo ($page=='game-category' || $page=='games')?'nav-active':'';?>"><a href=""><i class="fa fa-tags"></i> <span>Games</span></a>
                    <ul class="sub-menu-list">
                        <li <?php echo ($page=='games')?'class="active"':'';?>><a href="index.php?page=games"> Games</a></li>
                        <li <?php echo ($page=='game-category')?'class="active"':'';?>><a href="index.php?page=game-category"> Games Category</a></li>
                    </ul>
                </li>
				<li class="menu-list <?php echo ($page=='add-page' || $page=='page-list')?'nav-active':'';?>"><a href=""><i class="fa fa-file-text"></i> <span>Pages</span></a>
                    <ul class="sub-menu-list">
                        <li <?php echo ($page=='add-page')?'class="active"':'';?>><a href="index.php?page=add-page"> Add Page</a></li>
						<li <?php echo ($page=='page-list')?'class="active"':'';?>><a href="index.php?page=page-list">Page List</a></li>
                    </ul>
                </li>
                <li class="menu-list <?php echo ($page=='nationality' || $page=='country' || $page=='ethnicity' || $page=='languages')?'nav-active':'';?>"><a href=""><i class="fa fa-file-text"></i> <span>Extra Pages</span></a>
                    <ul class="sub-menu-list">
                       <!-- <li><a href="profile.html"> Profile</a></li>
                        <li><a href="invoice.html"> Invoice</a></li>-->
                        <li <?php echo ($page=='nationality')?'class="active"':'';?>><a href="index.php?page=nationality"> Nationality</a></li>
                        <li <?php echo ($page=='country')?'class="active"':'';?>><a href="index.php?page=country"> Country</a></li>
                        <li <?php echo ($page=='ethnicity')?'class="active"':'';?>><a href="index.php?page=ethnicity"> Ethnicity</a></li>
                        <li <?php echo ($page=='languages')?'class="active"':'';?>><a href="index.php?page=languages"> Languages</a></li>
                    </ul>
                </li>
            </ul>
            <!--sidebar nav end-->

        </div>
    </div>
    <!-- left side end-->
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

            <!--toggle button start-->
            <a class="toggle-btn"><i class="fa fa-bars"></i></a>
            <!--toggle button end-->
            <!--notification menu start -->
            <div class="menu-right">
                <ul class="notification-menu">
                    <!--<li>
                        <a href="#" class="btn btn-default dropdown-toggle info-number bgNone" data-toggle="dropdown">
                            <i class="fa fa-tasks"></i>
                            <span class="badge">8</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-head pull-right">
                            <h5 class="title">You have 8 pending task</h5>
                            <ul class="dropdown-list user-list">
                                <li class="new">
                                    <a href="#">
                                        <div class="task-info">
                                            <div>Database update</div>
                                        </div>
                                        <div class="progress progress-striped">
                                            <div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar progress-bar-warning">
                                                <span class="">40%</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="new">
                                    <a href="#">
                                        <div class="task-info">
                                            <div>Dashboard done</div>
                                        </div>
                                        <div class="progress progress-striped">
                                            <div style="width: 90%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="90" role="progressbar" class="progress-bar progress-bar-success">
                                                <span class="">90%</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="task-info">
                                            <div>Web Development</div>
                                        </div>
                                        <div class="progress progress-striped">
                                            <div style="width: 66%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="66" role="progressbar" class="progress-bar progress-bar-info">
                                                <span class="">66% </span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="task-info">
                                            <div>Mobile App</div>
                                        </div>
                                        <div class="progress progress-striped">
                                            <div style="width: 33%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="33" role="progressbar" class="progress-bar progress-bar-danger">
                                                <span class="">33% </span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="task-info">
                                            <div>Issues fixed</div>
                                        </div>
                                        <div class="progress progress-striped">
                                            <div style="width: 80%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="80" role="progressbar" class="progress-bar">
                                                <span class="">80% </span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="new"><a href="">See All Pending Task</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="#" class="btn btn-default dropdown-toggle info-number bgNone" data-toggle="dropdown">
                            <i class="fa fa-envelope-o"></i>
                            <span class="badge">5</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-head pull-right">
                            <h5 class="title">You have 5 Mails </h5>
                            <ul class="dropdown-list normal-list">
                                <li class="new">
                                    <a href="">
                                        <span class="thumb"><img src="images/photos/user1.png" alt="" /></span>
                                        <span class="desc">
                                          <span class="name">John Doe <span class="badge badge-success">new</span></span>
                                          <span class="msg">Lorem ipsum dolor sit amet...</span>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <span class="thumb"><img src="images/photos/user2.png" alt="" /></span>
                                        <span class="desc">
                                          <span class="name">Jonathan Smith</span>
                                          <span class="msg">Lorem ipsum dolor sit amet...</span>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <span class="thumb"><img src="images/photos/user3.png" alt="" /></span>
                                        <span class="desc">
                                          <span class="name">Jane Doe</span>
                                          <span class="msg">Lorem ipsum dolor sit amet...</span>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <span class="thumb"><img src="images/photos/user4.png" alt="" /></span>
                                        <span class="desc">
                                          <span class="name">Mark Henry</span>
                                          <span class="msg">Lorem ipsum dolor sit amet...</span>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <span class="thumb"><img src="images/photos/user5.png" alt="" /></span>
                                        <span class="desc">
                                          <span class="name">Jim Doe</span>
                                          <span class="msg">Lorem ipsum dolor sit amet...</span>
                                        </span>
                                    </a>
                                </li>
                                <li class="new"><a href="">Read All Mails</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="#" class="btn btn-default dropdown-toggle info-number bgNone" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="badge">4</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-head pull-right">
                            <h5 class="title">Notifications</h5>
                            <ul class="dropdown-list normal-list">
                                <li class="new">
                                    <a href="">
                                        <span class="label label-danger"><i class="fa fa-bolt"></i></span>
                                        <span class="name">Server #1 overloaded.  </span>
                                        <em class="small">34 mins</em>
                                    </a>
                                </li>
                                <li class="new">
                                    <a href="">
                                        <span class="label label-danger"><i class="fa fa-bolt"></i></span>
                                        <span class="name">Server #3 overloaded.  </span>
                                        <em class="small">1 hrs</em>
                                    </a>
                                </li>
                                <li class="new">
                                    <a href="">
                                        <span class="label label-danger"><i class="fa fa-bolt"></i></span>
                                        <span class="name">Server #5 overloaded.  </span>
                                        <em class="small">4 hrs</em>
                                    </a>
                                </li>
                                <li class="new">
                                    <a href="">
                                        <span class="label label-danger"><i class="fa fa-bolt"></i></span>
                                        <span class="name">Server #31 overloaded.  </span>
                                        <em class="small">4 hrs</em>
                                    </a>
                                </li>
                                <li class="new"><a href="">See All Notifications</a></li>
                            </ul>
                        </div>
                    </li>-->
                    <li class="paddRight10">
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="images/photos/user-avatar.png" alt="" />
                            Olesya
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                            <li><a href="#"><i class="fa fa-user"></i>  Profile</a></li>
                            <li><a href="#"><i class="fa fa-cog"></i>  Settings</a></li>
                            <li><a href="index.php?page=log-out"><i class="fa fa-sign-out"></i> Log Out</a></li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!--notification menu end -->

        </div>
        <!-- header section end-->



        <!--body wrapper start-->
        <div class="wrapper">
        <?php include_once($pageName); ?>
        </div>
        <!--body wrapper end-->

        <!--footer section start-->
        <footer class="sticky-footer">
            <?php echo date("Y");?> &copy; Fameuz.com
        </footer>
        <!--footer section end-->


    </div>
    <!-- main content end-->
</section>

<!-- Placed js at the end of the document so the pages load faster -->



<!--easy pie chart-->
<script src="js/easypiechart/jquery.easypiechart.js"></script>
<script src="js/easypiechart/easypiechart-init.js"></script>

<!--Sparkline Chart-->
<script src="js/sparkline/jquery.sparkline.js"></script>
<script src="js/sparkline/sparkline-init.js"></script>

<!--icheck -->
<script src="js/iCheck/jquery.icheck.js"></script>
<script src="js/icheck-init.js"></script>

<!-- jQuery Flot Chart-->
<script src="js/flot-chart/jquery.flot.js"></script>
<script src="js/flot-chart/jquery.flot.tooltip.js"></script>
<script src="js/flot-chart/jquery.flot.resize.js"></script>
<script src="js/flot-chart/jquery.flot.pie.resize.js"></script>
<script src="js/flot-chart/jquery.flot.selection.js"></script>
<script src="js/flot-chart/jquery.flot.stack.js"></script>
<script src="js/flot-chart/jquery.flot.time.js"></script>
<script src="js/main-chart.js"></script>

<!--common scripts for all pages-->
<script src="js/scripts.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>

</body>
</html>
