<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
$userId							   =	$_SESSION['userId'];
$keywordSearch						=	$objCommon->esc($_GET['keywordSearch']);
$searchCount						  =	0;
if($keywordSearch ==''){
	header("location:".SITE_ROOT);
	exit;
}
?>
<div class="inner_content_section">
	<div class="container">
        <div class="inner_top_border">
            <div class="user_profile">
            <?php echo $objCommon->checkEmailverification();?>
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                    <div class="content">
                        <div class="pagination_box">
                            <a href="<?php echo SITE_ROOT.'user/home'?>">Home <i class="fa fa-caret-right"></i></a>
                            <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                            <a href="javascript:;" class="active"> Search Result</a>
                            <?php
                            include_once(DIR_ROOT."widget/notification_head.php");
                            ?>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div>
                                    <div class="visitor_head_box seperateIt">
                                        <h5>Search Result for '<?php echo $keywordSearch?>'</h5>
                                        <span class="arw-point"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
						<?php
						$getpeopleArr				=	$objUsers->listQuery("select user.user_id,user.first_name,user.last_name,user.display_name,profileImg.upi_img_url,user.email,social.usl_fameuz,follow.follow_user1,follow.follow_user2,follow.follow_status,case when (TIMESTAMPDIFF(MINUTE,chat.ucs_last_seen,'".date("Y-m-d H:i:s")."') >15 or chat.ucs_status=0) then '0' else '1' END AS chatStatus,personal.p_country,personal.p_city,cat.c_name
from following as follow 
left join users as user on (follow.follow_user1 = user.user_id and follow.follow_user1 !='".$userId."') or (follow.follow_user2 = user.user_id and follow.follow_user2 !='".$userId."')  
left join user_chat_status as chat on user.user_id = chat.user_id
LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN personal_details AS personal ON user.user_id=personal.user_id 
LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
where ( user.first_name LIKE '%".$keywordSearch."%' or  user.display_name LIKE '%".$keywordSearch."%' or  user.email LIKE '%".$keywordSearch."%' or  social.usl_fameuz LIKE '%".$keywordSearch."%') group by user.user_id order by user.first_name asc limit 6");
						if(count($getpeopleArr)>0){
							$searchCount				=	1;
						?>
                        <div class="visitor_list_box">
							<div class="search_result_tag">
								<h3>People</h3>
							</div>
                           <div class="row">
                           <?php 
						   foreach($getpeopleArr as $getpeople){ 
						   	$allPeopleImg		  =	SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.(($getpeople['upi_img_url'])?$getpeople['upi_img_url']:"profile_pic.jpg");
							$allPeopleName		 =	$objCommon->displayName($getpeople);
							if($getpeople['follow_status']==2){ 
								$friendStatus =2; 
							}else if($getpeople['follow_status']==1 && $getpeople['follow_user1']==$_SESSION['userId']){ 
								$friendStatus =1; 
							}else { 
								$friendStatus=0; 
							}
							if($getpeople['p_city'] != '' || $getpeople['p_country'] != ''){
								$friendPcity	=	($getpeople['p_city'])?$objCommon->html2text($getpeople['p_city'])." ,":"";
							}
						   ?>
                               <div class="col-sm-12 col-md-12 col-lg-6 border-support">
                                   <div class="row">
                                       <div class="col-sm-4 new-width-search">
                                           <div class="search-img">
                                             <a href="<?php echo SITE_ROOT.$objCommon->html2text($getpeople['usl_fameuz'])?>" title="<?php echo $allPeopleName?>"><img src="<?php echo $allPeopleImg?>" class="img-responsive" alt="<?php echo $allPeopleName?>" title="<?php echo $allPeopleName?>" /></a>
                                           </div>
                                       </div>
                                       <div class="col-sm-8">
                                            <div class="info-job search-main"><div class="job-head">
                                                <p><a href="<?php echo SITE_ROOT.$objCommon->html2text($getpeople['usl_fameuz'])?>" title="<?php echo $allPeopleName?>"><?php echo $allPeopleName?></a></p><!--<img class="verify" src="images/verify.png" />-->
                                            </div>
                                            <div class="post-info">
                                                <p class="proffesion"><?php echo $objCommon->html2text($getpeople['c_name'])?></p>
                                                <p class="dim-me"><i class="fa_globe"></i>&nbsp; <?php echo $friendPcity.' '.$objCommon->limitWords($allFollowers['p_country'],15)?></p>
                                            </div>
                                            <div class="status-user">
												<?php
												if($getpeople['chatStatus']==1){
													echo '<p class="online_status">online</p>';
												}else{
													echo '<p class="offline_status">offline</p>';
												}
												?>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-7">
                                                </div>
                                                <div class="col-sm-5 less-padding">
                                                    <div class="follow-btn pull-right">
													<?php
														if($friendStatus==2){
														?>
														<a href="javascript:;" class="friend"><i class="fa fa-star-o"></i> Friend</a>
														<?php
														}else if($friendStatus==1){
														?>
														<a href="javascript:;" class="following"><i class="fa fa-check"></i> Following</a>
														<?php
														}else if($friendStatus==0){
														?>
														
														<?php
														}
														?>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                       </div>
                                    </div>
                               <div class="border-me-under"> </div>
                               </div>
                           <?php } ?>
                            </div>
                        </div>
						<?php
						}
						if(strlen($keywordSearch)>1){
						include_once(DIR_ROOT."class/search_functions.php");
						$obj_search_functions		=	new search_functions();
						$sqlVideos		  	   	   =	$obj_search_functions->search($keywordSearch);
						$getVideoArr				 =	$objUsers->listQuery($sqlVideos.' limit 6');
						if(count($getVideoArr)>0){
							$searchCount				=	1;
						?>
							<div class="search_result_tag gap_div">
								<h3>Videos</h3>
							</div>
							<?php
							foreach($getVideoArr as $getVideo){
								if($getVideo['video_type']==1){
									$getAiImages				=	SITE_ROOT_AWS_VIDEOS.'uploads/videos/'.$getVideo['video_thumb'];
									$vidUrl					 =	SITE_ROOT.'uploads/videos/'.$getVideo['video_url'];
								}else if($getVideo['video_type']==2){
									if (preg_match('%^https?://[^\s]+$%',$getVideo['video_thumb'])) {
										$getAiImages			=	$getVideo['video_thumb'];
									} else {
										$getAiImages			=	SITE_ROOT_AWS_VIDEOS.'uploads/videos/'.$getVideo['video_thumb'];
									}
									$vidUrl					 =	$objCommon->html2text($getVideo['video_url']);
								}
								$displayNameFriend			  =	$objCommon->displayName($getVideo);
								$vidcreatedTime				 =	$objCommon->time_elapsed_string(strtotime($objCommon->local_time($getVideo['video_created'])));
								$vidRateCound				   =	$objCommon->round_to_nearest_half($getVideo['voteAvg']);
								$vidRateCoundStyle	   		  =	'style="width:'.($vidRateCound*20).'%;"';
							?>
								<div class="row">
							   <div class="col-sm-12 videoSearch">
									<div class="item">
									<div class="row">	
										<div class="col-xs-12 col-sm-5 col-md-3">
											<a href="<?php echo SITE_ROOT.'video/watch/'.$objCommon->html2text($getVideo['video_encr_id'])?>" class="vidimg pull-left"><img src="<?php echo $getAiImages?>" alt="video" width="100%" class="img-responsive"/>
												<div class="video_duration"><?php echo $getVideo['video_duration']?></div>
											</a>
										</div>
										<div class="col-xs-12 col-sm-7 col-md-9 vidDescr">
											<h2><a href="<?php echo SITE_ROOT.'video/watch/'.$objCommon->html2text($getVideo['video_encr_id'])?>"><?php echo $objCommon->html2text($getVideo['video_title'])?></a></h2>
											<div class="row">
											<div class="col-sm-5"><p class="publishedBy"><i class="fa_publish"></i> <span class="publishedBySpan">Published By:</span> <a href="<?php echo SITE_ROOT.$objCommon->html2text($getVideo['usl_fameuz'])?>"><?php echo $displayNameFriend?></a></p></div><div class="col-sm-4"><p class="publishedBy"><i class="fa_clock"></i> <?php echo $vidcreatedTime?></p></div></div>
											
											<p><?php echo $objCommon->limitWords($getVideo['video_descr'],120)?></p>
											<div class="row"></div>
											<div class="likeWidget">
												<div class="row">
													<div class="col-sm-2 col-md-2 col-lg-2"><span><i class="fa_heart_gery"></i> <?php echo number_format($getVideo['video_likes']);?> Likes</span></div>
													<div class="col-sm-2 col-md-2 col-lg-2"><span><div class="rating_box"><div <?php echo $vidRateCoundStyle?> class="rating_yellow"></div></div></span></div>
													<div class="col-sm-5"><span><i class="fa_eye"></i> <span class="vidVisits"><?php echo number_format($getVideo['video_visits']);?> Views </span></span></div>
												</div>
											</div>
										</div>
										</div>
									</div>
								</div>
							</div>
							<?php
							}
						}
						}
						$getBlogArr							=	$objUsers->listQuery("SELECT b.*,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,group_concat(l.user_id) AS likeUser,count(comment_id) AS commentCount,bc.bc_title
										FROM blogs AS b 
										LEFT JOIN blog_category AS bc ON b.bc_id = bc.bc_id 
										LEFT JOIN users AS user ON b.user_id = user.user_id
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN blogs_like AS l ON b.blog_id = l.blog_id AND l.bl_status = 1
										LEFT JOIN  comments ON b.blog_id=comments.comment_content AND comments.comment_cat=7
										WHERE user.status=1 AND user.email_validation=1  AND b.blog_status=1 AND (b.blog_title LIKE '%".$keywordSearch."%' OR user.first_name LIKE '%".$keywordSearch."%' or  user.display_name LIKE '%".$keywordSearch."%') GROUP BY b.blog_id  ORDER BY b.created_time desc");
						if(count($getBlogArr)>0){
							$searchCount				=	1;
						?>
                        <div class="search_result_tag gap_div">
                        	<h3>Blogs</h3>
                        </div>
                        <div class="row">
                           <?php 
						   foreach($getBlogArr as $getBlog){ 
						   	$likeUser					=	$getBlog['likeUser'];
							$likeUserArr				 =	'';
							if($likeUser){
								$likeUserArr			 =	explode(",",$likeUser);
								$likeUserArr			 =	array_filter($likeUserArr);
								$likeCounts			  =	count($likeUserArr).' Likes';
							}else{
								$likeCounts			  =	 '0 Likes';
							}
						   ?>
                           <div class="col-sm-12 blogSearch">
                                <div class="item">
                                <div class="likeWidget">
                                            <div class="row">
                                                <div class="col-md-2"><span><i class="fa_heart_gery"></i> <?php echo $likeCounts?> </span></div>
                                                <div class="col-md-9"><span><i class="fa_comment_alt"></i> <?php echo $objCommon->html2text($getBlog['commentCount'])?> Comments</span></div>
                                            </div>
                                        </div>
                                <div class="row">
                                        <div class="col-xs-12 col-sm-5 col-md-3">
                                        <a href="<?php echo SITE_ROOT.'blog/show/'.$objCommon->html2text($getBlog['blog_alias']).'-'.$getBlog['blog_id']?>" class="blogImg" style="background-image:url('<?php echo SITE_ROOT_AWS_IMAGES?>uploads/blogs/<?php echo $objCommon->html2text($getBlog['blog_img'])?>')">
                                        </a>
                                        </div>
                                        <div class="col-xs-12 col-sm-7 col-md-9 BlogDescr">
                                    <h2><a href="<?php echo SITE_ROOT.'blog/show/'.$objCommon->html2text($getBlog['blog_alias']).'-'.$getBlog['blog_id']?>"><?php echo $objCommon->html2text($getBlog['blog_title'])?></a></h2>
                                    <div class="row">
                                            <div class="col-sm-12 col-md-5">
                                                <p class="publishedBy"><i class="fa_publish"></i> <span class="publishedBySpan">Published By:</span> <a href="<?php echo SITE_ROOT.$objCommon->html2text($getBlog['usl_fameuz'])?>"><?php echo $objCommon->displayName($getBlog)?></a></p>
                                            </div>
                                            <div class="col-sm-12 col-md-4">
                                                <p class="publishedBy"><i class="fa fa-tag"></i> <span class="publishedBySpan">Category : </span> <?php echo $objCommon->html2text($getBlog['bc_title'])?></p>
                                            </div>
                                            <div class="col-sm-3">
                                                <p class="publishedBy"><i class="fa_clock"></i>  <?php echo date("M, d, Y",strtotime($getBlog['created_time']))?></p>
                                            </div>
                                        </div>
                                        <p class="desc-blog-s"><?php echo strip_tags($objCommon->limitWords($getBlog['blog_descr'],150))?></p>
                                     </div>
                                    </div>
                                </div>
                            </div>
                           <?php } ?>
                        </div>
                       	<?php
						}
						if($searchCount==0){
						?>
						<div class="visitor_list_box">No results found</div>
						<?php
						}
						?>
                    </div>
					 
                </div>
                <div class="col-sm-3 col-lg-sp-3">
                    <?php
                    include_once(DIR_ROOT."widget/right_ads_sidebar.php");
                    ?>
                </div>
                </div>
            </div>
        </div>
	</div>
</div>
<script type="text/javascript" src="<?php echo SITE_ROOT ?>js/fixed_sidebar.js"></script>
<script type="text/javascript">
$(window).load(function(){
	$('.fixer').fixedSidebar();
});
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  