<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/email_message.php");
include_once(DIR_ROOT."class/email_message_users.php");
include_once(DIR_ROOT."class/common_class.php");
$objCommon				   =	new common();

$objEmailMsg					=	new email_message();
$objEmailMsgUser				=	new email_message_users();
$user_url					   =	$objCommon->esc($_GET['user_url']);
$msg_id					   	 =	$objCommon->esc($_GET['msg_id']);
$getMessageSingleSql			=	"SELECT msg.*,msg_user.emu_from,msg_user.emu_to,msg_user.emu_read_status,msg_user.emu_delete_to,user_from.user_id,user_from.first_name,user_from.last_name,user_from.display_name,user_from.email,profileImg.upi_img_url,profileImg.upi_id,social.usl_fameuz,cat.c_name,group_concat(msg_attach.ema_file) AS attach_files
											  FROM email_message AS msg
											  LEFT JOIN email_message_users AS msg_user ON msg.em_id = msg_user.em_id
											  LEFT JOIN email_message_attachments AS msg_attach	ON msg.em_id = msg_attach.em_id
											  LEFT JOIN users AS user_from ON msg_user.emu_from = user_from.user_id 
											  LEFT JOIN user_social_links AS social ON user_from.user_id=social.user_id 
											  LEFT JOIN user_profile_image as profileImg ON user_from.user_id=profileImg.user_id AND profileImg.upi_status=1
											  LEFT JOIN user_categories AS ucat ON user_from.user_id=ucat.user_id
											  LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
											  WHERE ((msg_user.emu_to=".$_SESSION['userId']." AND msg_user.emu_delete_to=1) OR (msg_user.emu_from=".$_SESSION['userId']." AND msg_user.emu_delete_from=1))  AND user_from.email_validation=1 AND user_from.status=1 AND msg.em_id =".$msg_id;
$getMessageSingle			=	$objEmailMsg->getRowSql($getMessageSingleSql);
if($user_url =='' || $msg_id == '' || $getMessageSingle['em_id'] =='' || ($getMessageSingle['emu_to'] !=$_SESSION['userId'] && $getMessageSingle['emu_from'] !=$_SESSION['userId'])){
	header("location:".SITE_ROOT."user/my-messages");
	exit;
}
if($getMessageSingle['emu_to']==$_SESSION['userId']){
	$objEmailMsgUser->updateField(array("emu_read_status"=>1),"em_id=".$getMessageSingle['em_id']);
}
$img_formats = array("jpg", "png", "gif", "bmp","jpeg","JPG", "PNG", "GIF", "BMP","JPEG");
?>
<link href="<?php echo SITE_ROOT?>css/message.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>css/jquery.tokenize.css" />
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
  <div class="inner_top_border">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-sp-9">
      <div class="jobs-head">
        <div class="pagination_box_jobs pagination_box">
        <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
        <a href="<?php echo SITE_ROOT.$getUserDetails['usl_fameuz']?>"> My profile </a><i class="fa fa-caret-right"></i></a>
         <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
        <a href="<?php echo SITE_ROOT ?>user/my-messages" class="active">Message</a>
			<?php
				if(isset($_SESSION['userId'])){
					include_once(DIR_ROOT."widget/notification_head.php");
				}
            ?>
        </div>
       </div>
       <div class="white-compo">
       	<div class="message-container">
                <?php
				include_once(DIR_ROOT."widget/email_message_header.php");
				$getMessageSingleReplySql			=	"SELECT msg.*,msg_user.emu_from,msg_user.emu_to,msg_user.emu_read_status,msg_user.emu_delete_to,user_from.user_id,user_from.first_name,user_from.last_name,user_from.display_name,user_from.email,profileImg.upi_img_url,profileImg.upi_id,social.usl_fameuz,cat.c_name
											  FROM email_message AS msg
											  LEFT JOIN email_message_users AS msg_user ON msg.em_id = msg_user.em_id
											  LEFT JOIN users AS user_from ON msg_user.emu_from = user_from.user_id 
											  LEFT JOIN user_social_links AS social ON user_from.user_id=social.user_id 
											  LEFT JOIN user_profile_image as profileImg ON user_from.user_id=profileImg.user_id AND profileImg.upi_status=1
											  LEFT JOIN user_categories AS ucat ON user_from.user_id=ucat.user_id
											  LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
											  WHERE  msg_user.emu_delete_to=1 AND user_from.email_validation=1 AND user_from.status=1 AND msg.em_replyof =".$getMessageSingle['em_id']." ORDER BY msg.em_createdon DESC";
					$getMessageSingleReplyArr			=	$objEmailMsg->listQuery($getMessageSingleReplySql);
					if(count($getMessageSingleReplyArr)>0){
						foreach($getMessageSingleReplyArr as $getMessageSingleReply){
							$emailUserImgReply		   =	($getMessageSingleReply['upi_img_url'])?$getMessageSingleReply['upi_img_url']:'profile_pic.jpg';
							if($getMessageSingleReply['emu_to']==$_SESSION['userId']){
								$objEmailMsgUser->updateField(array("emu_read_status"=>1),"em_id=".$getMessageSingleReply['em_id']);
							}
							
				?>
				<div class="inbox_msges">
                    <div class="sub_message">
                        	<h3><?php echo 'Re : '.$objCommon->html2text($getMessageSingleReply['em_subject'])?></h3>
                        </div>
                    <div class="sender_details">
                    	<div class="img_user inside_mesgae">
                            <img src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.$emailUserImgReply?>" />
                        </div>
                        <div class="userInfo_online_chat">
                        	<h4><?php echo $objCommon->displayName($getMessageSingleReply);?></h4>
                            <p class="proffesion"><?php echo $objCommon->html2text($getMessageSingleReply['c_name'])?></p>
                        </div>
                        <div class="time_date pull-right text-right">
                        	<p><?php echo date("d/m/Y",strtotime($getMessageSingleReply['em_createdon']))?> (<?php echo $objCommon->time_elapsed_string(strtotime($objCommon->local_time($getMessageSingleReply['em_createdon'])))?>)</p>
                            <p><?php echo date("H:i",strtotime($objCommon->local_time($getMessageSingleReply['em_createdon'])))?></p>
                        </div>
                    </div>
                    <div class="recived_message">
                    	<p><?php echo $objCommon->html2text($getMessageSingleReply['em_message'])?></p>
                    </div>
                    <!--<div class="attachments_msg">
                        <div class="head_attachment">
                        	<h4> 5 Attachments <i class="fa fa-paperclip"></i></h4>
                            <a href="#" class="pull-right" title="Download All Attachments"><i class="fa fa-download"></i></a>
                    	<ul>
                        	<li style="background-image:url('http://localhost/fameuz/uploads/profile_images/2015-03-24/1427197935_13.jpg')">
                            	<div class="overlay_download text-center">
                                	<a href="#"><i class="fa fa-download"></i></a>
                                </div>
                            </li>
                        	<li style="background-image:url('http://localhost/fameuz/uploads/market_place/2015-06-10/1433925563_4.jpg')">
                            	<div class="overlay_download text-center">
                                	<a href="#"><i class="fa fa-download"></i></a>
                                </div>
                            </li>
                        	<li style="background-image:url('http://localhost/fameuz/uploads/blogs/2015-06-14/1434272073_8.png')">
                            	<div class="overlay_download text-center">
                                	<a href="#"><i class="fa fa-download"></i></a>
                                </div>
                            </li>
                        	<li style="background-image:url('http://localhost/fameuz/uploads/blogs/2015-06-14/1434271700_8.png')">
                            	<div class="overlay_download text-center">
                                	<a href="#"><i class="fa fa-download"></i></a>
                                </div>
                            </li>
                        	<li style="background-image:url('http://localhost/fameuz/uploads/blogs/2015-06-14/1434272324_8.jpg')">
                            	<div class="overlay_download text-center">
                                	<a href="#"><i class="fa fa-download"></i></a>
                                </div>
                            </li>
                        </ul>
                       </div>
                    </div>-->
                </div>
				<?php
					}
				}
				$emailUserImg			=	($getMessageSingle['upi_img_url'])?$getMessageSingle['upi_img_url']:'profile_pic.jpg';
				?>
                <div class="inbox_msges">
                    <div class="sub_message">
                        	<h3><?php echo $objCommon->html2text($getMessageSingle['em_subject'])?></h3>
                        </div>
                    <div class="sender_details">
                    	<div class="img_user inside_mesgae">
                            <img src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.$emailUserImg?>" />
                        </div>
                        <div class="userInfo_online_chat">
                        	<h4><?php echo $objCommon->displayName($getMessageSingle);?></h4>
                            <p class="proffesion"><?php echo $objCommon->html2text($getMessageSingle['c_name'])?></p>
                        </div>
                        <div class="time_date pull-right text-right">
                        	<p><?php echo date("d/m/Y",strtotime($getMessageSingle['em_createdon']))?> (<?php echo $objCommon->time_elapsed_string(strtotime($objCommon->local_time($getMessageSingle['em_createdon'])))?>)</p>
                            <p><?php echo date("H:i",strtotime($objCommon->local_time($getMessageSingle['em_createdon'])))?></p>
                        </div>
                    </div>
                    <div class="recived_message">
                    	<p><?php echo $objCommon->html2text($getMessageSingle['em_message'])?></p>
                    </div>
					<?php
					$attachFiles					=	$getMessageSingle['attach_files'];
					if($attachFiles !=''){
						$attachFilesExpl			=	explode(",",$attachFiles);
						$attachFilesExpl			=	array_filter($attachFilesExpl);
					?>
                    <div class="attachments_msg">
                        <div class="head_attachment">
                        	<h4> <?php echo count($attachFilesExpl)?> Attachments <i class="fa fa-paperclip"></i></h4>
                            <a href="<?php echo SITE_ROOT?>download_file/download_email_attach_zip.php?attach_id=<?php echo $getMessageSingle['em_id']?>" class="pull-right" title="Download All Attachments"><i class="fa fa-download"></i></a>
                    	<ul>
							<?php
							foreach($attachFilesExpl as $attachFilesAll){
								$ext	=	strtolower(pathinfo($attachFilesAll, PATHINFO_EXTENSION));
								if(in_array($ext,$img_formats)){
									$attachImg		=	SITE_ROOT.'uploads/email_attachment/'.$attachFilesAll;
								}else{
									$attachImg		=	SITE_ROOT.'uploads/email_attachment/text-file.png';
								}
							?>
                        	<li style="background-image:url('<?php echo $attachImg?>')">
                            	<div class="overlay_download text-center">
                                	<a href="<?php echo SITE_ROOT?>download_file/download_email_attach.php?attach_file=<?php echo $attachFilesAll?>"><i class="fa fa-download"></i></a>
                                </div>
                            </li>
							<?php
								}
							?>
                        </ul>
                       </div>
                    </div>
					<?php
					}
					?>
					<form action="<?php echo SITE_ROOT.'access/sent_msg_reply.php'?>" method="post" id="sent_msg_reply">
                    	<input type="hidden" name="replyof" value="<?php echo $getMessageSingle['em_id']?>"/>
						<input type="hidden" name="replyto" value="<?php echo ($getMessageSingleReply['emu_from']==$_SESSION['userId'])?$getMessageSingleReply['emu_to']:$getMessageSingleReply['emu_from']?>"/>
                    	<div class="user_me">
                        	<img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($getUserDetails['upi_img_url'])?$getUserDetails['upi_img_url']:'profile_pic.jpg'?>" />
                        </div>
						<div class="replay_message">
							<div class="form-group">
								 <textarea class="wysihtml5 form-control" rows="6" name="email_message" placeholder="Reply" id="email_message"></textarea>
							 </div>
						</div>
						<div class="submitBtn pull-right">
							<button class="btn SendBtn" type="submit">Send</button>
						 </div>
					</form>
                </div>
            </div>
       </div>
   </div>
     <div class="hidden-xs hidden-sm hidden-md"> 
	 <?php
		include_once(DIR_ROOT."widget/right_online_memebers.php");
	 ?>
	</div>
    </div>
    </div>
  </div>
</div>
<script src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery-ui.js" type="text/javascript"></script>
<!--<script src="<?php echo SITE_ROOT ?>js/message.js" type="text/javascript"></script>-->
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery.tokenize.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.wysihtml5').wysihtml5();
	$('#tokenize').tokenize();
});
</script>
<?php
include_once(DIR_ROOT."js/include/message.php");
include_once(DIR_ROOT."includes/footer.php");

?>
  