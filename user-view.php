<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
?>
    <div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="user_profile">
                            <div class="content">
                            	<div class="ad a_right "><img src="<?php echo SITE_ROOT?>images/ad_img2.png" alt="ad_img2" /></div>
                            	<div class="pagination_box">
                                	<a href="#">Home <i class="fa fa-caret-right"></i></a>
                                	<a href="#" class="active"> My profile  </a>
                                </div>
                            	<div class="profile_box">
                                	<div class="profile_pic">
                                    	<div class="pro_pic">
                                        	<a href="#"><img src="<?php echo SITE_ROOT?>images/pro_pic.jpg" alt="pro_pic" /></a>
                                            <a href="#" class="follow_btn follow " id="follow_btn">
                                            	<i class="fa fa-plus"></i><span class="text">Follow</span>
                                            </a>
                                        </div>
                                        <h2 class="name">Olesya Leuenberger</h2>
                                        <p class="username">@olesyaleuenberger</p>
                                        <div class="status offline"><i class="fa fa-circle"></i>Offline</div>
                                        <p class="time"><i class="fa fa-clock-o"></i>Last seen today at 9:52 am </p>
                                        <div class="raning">1235<br><span>Ranking</span></div>
                                        <div class="follows_no_box">
                                        	<div class="follows_no">
                                            	677070<br> <span>Following</span>
                                            </div>
                                        	<div class="follows_no">
                                            	10000<br> <span>Followers</span>
                                            </div>
                                        	<div class="clr"></div>
                                        </div>
                                        <div class="profile_about">
                                        	<h2 class="about"><a href="#">About</a> </h2>
                                            <h2>Nationality</h2>
                                            <div class="national">
                                            	<span class="flag"><img src="<?php echo SITE_ROOT?>images/flag.png" alt="flag" /></span>
                                                United Arab Emirates
                                             </div>
                                             <h2>Languages </h2>
                                             <ul class="list_border">
                                             	<li>English</li>
                                                <li>French</li>
                                                <li>Russian</li>
                                             </ul>
                                             <h2>Interested in </h2>
                                             <ul class="list_border">
                                             	<li><a href="#"><i class="fa fa-chevron-right"></i>Fashion</a></li>
                                                <li><a href="#"><i class="fa fa-chevron-right"></i>Real Life & People</a></li>
                                             </ul>
                                             <h2>Gallery</h2>
                                             <ul class="list_border">
                                             	<li><a href="#"><i class="fa fa-chevron-right"></i>Composite Card</a></li>
                                             </ul>
                                        	<h2 class="about blog"><a href="#">Blogs</a> </h2>
                                        </div>
                                    </div>
                                    <div class="profile_tabs">
                                         <h2>Disciplines </h2>
                                         <ul class="list_border">
                                            <li><a href="#"><i class="fa fa-chevron-right"></i>Fashion/High Fashion</a></li>
                                            <li><a href="#"><i class="fa fa-chevron-right"></i>Beach Fashion</a></li>
                                            <li><a href="#"><i class="fa fa-chevron-right"></i>Sport</a></li>
                                            <li><a href="#"><i class="fa fa-chevron-right"></i>Abaya</a></li>
                                            <li><a href="#"><i class="fa fa-chevron-right"></i>Lingerie</a></li>
                                            <li><a href="#"><i class="fa fa-chevron-right"></i>Print/Editorial</a></li>
                                            <li><a href="#"><i class="fa fa-chevron-right"></i>Runway</a></li>
                                            <li><a href="#"><i class="fa fa-chevron-right"></i>Beauty</a></li>
                                            <li><a href="#"><i class="fa fa-chevron-right"></i>Hairs</a></li>
                                            <li><a href="#"><i class="fa fa-chevron-right"></i>Hands</a></li>
                                            <li><a href="#"><i class="fa fa-chevron-right"></i>Feet</a></li>
                                            <li><a href="#"><i class="fa fa-chevron-right"></i>Other</a> </li>
                                         </ul>
                                    </div>
                                    <div class="profile_tabs list_bold">
                                        <ul>
                                        	<li>
                                                <a href="#">
                                                    <i class="fa fa-gamepad icons"></i>
                                                    Games
                                                    <i class="fa fa-play arrow_list"></i>
                                                </a>
                                            </li>
                                        	<li>
                                                <a href="#">
                                                    <i class="fa fa-music icons"></i>
                                                     Music
                                                    <i class="fa fa-play arrow_list"></i>
                                                </a>
                                            </li>
                                        	<li>
                                                <a href="#">
                                                    <i class="fa fa-video-camera icons"></i>
                                                     Videos
                                                    <i class="fa fa-play arrow_list"></i>
                                                </a>
                                            </li>
                                        	<li>
                                                <a href="#">
                                                    <i class="fa fa-gift icons"></i>
                                                     Gifts
                                                    <i class="fa fa-play arrow_list"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    
                                </div>
                            	<div class="profile_content">
                                	<div class="profile_pic_big">
                                    	<div class="image"><img src="<?php echo SITE_ROOT?>images/profile_big.png" alt="profile_big" /></div>
                                        <div class="share_like_box">
                                        	<div class="review_message">
                                            	<a href="#">Add Review <i class="fa fa-chevron-right"></i></a>
                                            	<a href="#">Message <i class="fa fa-chevron-right"></i></a>
                                            </div>
                                            <div class="like">
                                            	<a href="#" id="like_btn"><i class="fa fa-heart"></i> <span class="text">Like</span> </a>
                                            </div>
                                            <div class="share">
                                            	<a href="#"><i class="fa fa-share-alt"></i>Share </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="photos_section">
                                        <div class="photo_heading">
                                        	Photos 
                                        	<span class="photo_arrow"><img src="<?php echo SITE_ROOT?>images/photo_arrow.png" alt="photo_arrow" /></span>
                                        </div>
                                        <div class="photos_box">
                                        	<div class="photos">
                                            	<div class="pic"><img src="<?php echo SITE_ROOT?>images/photos.png" alt="photos" /></div>
                                                <div class="like_cmt_share">
                                                	<a href="#" class="like like_btn_photo" ><i class="fa fa-heart"></i>44</a>
                                                    <a href="#" class="cmt"><i class="fa fa-comments"></i>100</a>
                                                    <a href="#" class="share"><i class="fa fa-share-alt"></i></a>
                                                </div>
                                            </div>
                                        	<div class="photos">
                                            	<div class="pic"><img src="<?php echo SITE_ROOT?>images/photos.png" alt="photos" /></div>
                                                <div class="like_cmt_share">
                                                	<a href="#" class="like like_btn_photo" ><i class="fa fa-heart"></i>44</a>
                                                    <a href="#" class="cmt"><i class="fa fa-comments"></i>100</a>
                                                    <a href="#" class="share"><i class="fa fa-share-alt"></i></a>
                                                </div>
                                            </div>
                                        	<div class="photos">
                                            	<div class="pic"><img src="<?php echo SITE_ROOT?>images/photos.png" alt="photos" /></div>
                                                <div class="like_cmt_share">
                                                	<a href="#" class="like like_btn_photo" ><i class="fa fa-heart"></i>44</a>
                                                    <a href="#" class="cmt"><i class="fa fa-comments"></i>100</a>
                                                    <a href="#" class="share"><i class="fa fa-share-alt"></i></a>
                                                </div>
                                            </div>
                                        	<div class="photos">
                                            	<div class="pic"><img src="<?php echo SITE_ROOT?>images/photos.png" alt="photos" /></div>
                                                <div class="like_cmt_share">
                                                	<a href="#" class="like like_btn_photo" ><i class="fa fa-heart"></i>44</a>
                                                    <a href="#" class="cmt"><i class="fa fa-comments"></i>100</a>
                                                    <a href="#" class="share"><i class="fa fa-share-alt"></i></a>
                                                </div>
                                            </div>
                                        	<div class="photos">
                                            	<div class="pic"><img src="<?php echo SITE_ROOT?>images/photos.png" alt="photos" /></div>
                                                <div class="like_cmt_share">
                                                	<a href="#" class="like like_btn_photo" ><i class="fa fa-heart"></i>44</a>
                                                    <a href="#" class="cmt"><i class="fa fa-comments"></i>100</a>
                                                    <a href="#" class="share"><i class="fa fa-share-alt"></i></a>
                                                </div>
                                            </div>
                                        	<div class="photos">
                                            	<div class="pic"><img src="<?php echo SITE_ROOT?>images/photos.png" alt="photos" /></div>
                                                <div class="like_cmt_share">
                                                	<a href="#" class="like like_btn_photo" ><i class="fa fa-heart"></i>44</a>
                                                    <a href="#" class="cmt"><i class="fa fa-comments"></i>100</a>
                                                    <a href="#" class="share"><i class="fa fa-share-alt"></i></a>
                                                </div>
                                            </div>
                                        	<div class="photos">
                                            	<div class="pic"><img src="<?php echo SITE_ROOT?>images/photos.png" alt="photos" /></div>
                                                <div class="like_cmt_share">
                                                	<a href="#" class="like like_btn_photo" ><i class="fa fa-heart"></i>44</a>
                                                    <a href="#" class="cmt"><i class="fa fa-comments"></i>100</a>
                                                    <a href="#" class="share"><i class="fa fa-share-alt"></i></a>
                                                </div>
                                            </div>
                                        	<div class="photos">
                                            	<div class="pic"><img src="<?php echo SITE_ROOT?>images/photos.png" alt="photos" /></div>
                                                <div class="like_cmt_share">
                                                	<a href="#" class="like like_btn_photo" ><i class="fa fa-heart"></i>44</a>
                                                    <a href="#" class="cmt"><i class="fa fa-comments"></i>100</a>
                                                    <a href="#" class="share"><i class="fa fa-share-alt"></i></a>
                                                </div>
                                            </div>
                                        	<div class="photos">
                                            	<div class="pic"><img src="<?php echo SITE_ROOT?>images/photos.png" alt="photos" /></div>
                                                <div class="like_cmt_share">
                                                	<a href="#" class="like like_btn_photo" ><i class="fa fa-heart"></i>44</a>
                                                    <a href="#" class="cmt"><i class="fa fa-comments"></i>100</a>
                                                    <a href="#" class="share"><i class="fa fa-share-alt"></i></a>
                                                </div>
                                            </div>
                                            <div class="clr"></div>
                                        </div>
                                    </div>
                                </div>
                            	<div class="clr"></div>            
                            </div> 
                            <div class="sidebar">
                            	<div class="search_box"><form action="<?php echo SITE_ROOT.'user/search-result'?>" method="get"><input type="text" placeholder="Search "  name="keywordSearch" /></form></div>
                                <div class="advertisement_box">
                                	<div class="adlink">
                                    	<span>Advertise with us!</span>
                                    	<span><a href="#">Create Ad</a></span>
                                    </div>
                                    <div class="ad_img"><img src="<?php echo SITE_ROOT?>images/ad_img3.png" alt="ad_img3" /></div>
                                    <div class="ad_text">
                                    	<h6>Men's NIKE Shoes -80%</h6>
                                        <p class="site">xxxxxxxxx.com</p>
                                        <p class="disc">See how this unique UAE website can get you 
Top Brand Shoes & Clothing at up to 80% off</p>
                                    </div>
                                </div>
                                <div class="profile_border_box information">
                               		<h2 class="heading"><i class="fa fa-exclamation"></i>Information </h2>
                                    <ul>
                                    	<li>
                                        	<span>Location </span>
                                        	<span>Dubai, United Arab Emirates</span>
                                        </li>
                                    	<li>
                                        	<span>Age </span>
                                        	<span>28</span>
                                        </li>
                                    	<li>
                                        	<span>Ethnicity </span>
                                        	<span>White/Caucasic</span>
                                        </li>
                                    	<li>
                                        	<span>Gender </span>
                                        	<span>Female</span>
                                        </li>
                                    	<li>
                                        	<span>Height </span>
                                        	<span>176cm / 5´9”</span>
                                        </li>
                                    	<li>
                                        	<span>Height </span>
                                        	<span>176cm / 5´9”</span>
                                        </li>
                                    	<li>
                                        	<span>Eyes </span>
                                        	<span>Brown</span>
                                        </li>
                                    	<li>
                                        	<span>Bust </span>
                                        	<span>86cm / 34"</span>
                                        </li>
                                    	<li>
                                        	<span>Waist </span>
                                        	<span>63cm / 25"</span>
                                        </li>
                                    	<li>
                                        	<span>Hips </span>
                                        	<span>88cm / 35"</span>
                                        </li>
                                    	<li>
                                        	<span>Dress </span>
                                        	<span>34 EU, 6 UK, 4 US</span>
                                        </li>
                                    	<li>
                                        	<span>Shoes </span>
                                        	<span>39 EU, 6 UK, 6 US</span>
                                        </li>
                                    </ul>
                                    <p class="about_more"><a href="#">Know more about measurements <i class="fa fa-play "></i></a></p>
                                    <p class="booking margin_btm0"><a href="#">Book Olesya</a></p>
                                </div>
                                <div class="social_icons margin_btm14">
                                	 <h2 class="heading">Connect socially with us</h2>
                                     <a href="#"><img src="<?php echo SITE_ROOT?>images/fb_s.png" alt="fb_s" /></a>
                                     <a href="#"><img src="<?php echo SITE_ROOT?>images/tw_s.png" alt="tw_s" /></a>
                                     <a href="#"><img src="<?php echo SITE_ROOT?>images/you_s.png" alt="you_s" /></a>
                                     <a href="#"><img src="<?php echo SITE_ROOT?>images/g_plus_s.png" alt="g_plus_s" /></a>
                                     <a href="#"><img src="<?php echo SITE_ROOT?>images/in_s.png" alt="in_s" /></a>
                                     <a href="#"><img src="<?php echo SITE_ROOT?>images/pin_s.png" alt="pin_s" /></a>
                                </div>
                                <div class="visitors_box margin_btm14">
                                    <h2 class="heading with_border">
                                        <span>Olesya Worked With</span>
                                        <span class="seemore"><a href="#">See More <i class="fa fa-play "></i></a></span>
                                    </h2>
                                    <div class="visitor_images">
                                    	<a href="#">
                                        	<span class="thumb"><img src="<?php echo SITE_ROOT?>images/visitor_img.png" /></span>
                                            <span class="title">Monica</span>
                                        </a>
                                    	<a href="#">
                                        	<span class="thumb"><img src="<?php echo SITE_ROOT?>images/visitor_img.png" /></span>
                                            <span class="title">Monica</span>
                                        </a>
                                    	<a href="#">
                                        	<span class="thumb"><img src="<?php echo SITE_ROOT?>images/visitor_img.png" /></span>
                                            <span class="title">Monica</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="profile_border_box reviews">
                                	<h2 class="heading">Olesya's Reviews</h2>
                                    <div class="review_box">
                                    	<div class="thumb"><img src="<?php echo SITE_ROOT?>images/review_thumb.png" alt="review_thumb" /></div>
                                    	<div class="text">
                                        	<p class="review">Check my out everyone !!!!! :-) it's me transform into 
The male version of mystique from x-men ! :-) :-) :-) </p>
											<p class="posted">Posted by <span>zerilin machman</span></p>
                                        </div>
                                    	<div class="clr"></div>
                                        <div class="rating_box"><div class="rating_yellow" style="width:78%;"></div></div>
                                        <div class="time"><i class="fa fa-clock-o"></i> 3 hours ago</div>
                                    </div>
                                    <div class="review_box">
                                    	<div class="thumb"><img src="<?php echo SITE_ROOT?>images/review_thumb.png" alt="review_thumb" /></div>
                                    	<div class="text">
                                        	<p class="review">Check my out everyone !!!!! :-) it's me transform into 
The male version of mystique from x-men ! :-) :-) :-) </p>
											<p class="posted">Posted by <span>zerilin machman</span></p>
                                        </div>
                                    	<div class="clr"></div>
                                        <div class="rating_box"><div class="rating_yellow" style="width:78%;"></div></div>
                                        <div class="time"><i class="fa fa-clock-o"></i> 3 hours ago</div>
                                    </div>
                                    <div class="review_box">
                                    	<div class="thumb"><img src="<?php echo SITE_ROOT?>images/review_thumb.png" alt="review_thumb" /></div>
                                    	<div class="text">
                                        	<p class="review">Check my out everyone !!!!! :-) it's me transform into 
The male version of mystique from x-men ! :-) :-) :-) </p>
											<p class="posted">Posted by <span>zerilin machman</span></p>
                                        </div>
                                    	<div class="clr"></div>
                                        <div class="rating_box"><div class="rating_yellow" style="width:78%;"></div></div>
                                        <div class="time"><i class="fa fa-clock-o"></i> 3 hours ago</div>
                                    </div>
                                    <p class="margin_btm0"><a href="#" class="seemore">See More Reviews<i class="fa fa-chevron-right"></i></a></p>
                                </div>
                            </div> 
                            <div class="clr"></div>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <script type="text/javascript">
		$(document).ready(function(e) {
            $('#change_status').click(function(e) {
                $('#status_option').fadeToggle();
				return false;
            });
			$('.follow').click(function(e) {
                $(this).addClass('following');
                var icon = $(this).find('i')
				icon.removeClass('fa-plus');
				icon.addClass('fa-check');
                $(this).find('.text').html("Following")
				return false;
            });
			$('#like_btn').click(function(e) {
                 $(this).toggleClass('liked');
				return false;
            });
			$('.like_btn_photo').click(function(e) {
                 $(this).toggleClass('liked');
				return false;
            });
        });
	</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  