<?php
header('Cache-Control: no-cache, no-store, must-revalidate');
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
require_once DIR_ROOT.'bigpipe/include/classes/class.bigpipe.php';
require_once DIR_ROOT.'bigpipe/include/classes/class.pagelet.php';
require_once DIR_ROOT.'bigpipe/include/functions.php';
$PageletRed = new Pagelet();
$preloaderframe = '';
for($i=0; $i<2; $i++){
	$preloaderframe .= '
	<section id="newsfeed">
		<div class="feedLoader">
			<div class="feedContainer">
				<div class="row">
					<div class="col-sm-2 xtra-sm">
						<div class="img-s"></div>
					</div>
					<div class="col-sm-11">
						<div class="rectangle-md">
							<div class="rectangle"></div>
							<div class="rectangle-sm pull-right"></div>
						</div>
						<div class="rectangle-lg">
							<div class="rectangle-sm"></div>
							<div class="rectangle-sm"></div>
							<div class="rectangle-sm"></div>
						</div>
						<div class="rectangle-lg">
							<div class="rectangle-full"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="line"></div>
			<div class="rectangle-wd">
				<div class="rectangle-sm-down"></div>
				<div class="rectangle-sm-down"></div>
				<div class="rectangle-sm-down"></div>
			</div>
		</div>
	</section>';
}
include_once(DIR_ROOT.'js/include/fameuz_lightbox_library.php');
$PageletRed->addHTML($preloaderframe);
$PageletRed->addCSS(SITE_ROOT.'bigpipe/static/red.php?cachebuster='.getRandomValue());
$PageletRed->addJS(SITE_ROOT.'bigpipe/static/delayJS.php?cachebuster='.getRandomValue());
//$PageletRed->addJSCode("$('#newsfeed').load('".SITE_ROOT."widget/news_feed_home.php',function(){ $('.feedLoader').hide(); }); ");
$PageletRed->addJSCode("getresult('".SITE_ROOT."widget/news_feed_home.php');");
?>
<script src="<?php echo SITE_ROOT?>bigpipe/static/bigpipe.js" type="text/javascript"></script>
<script src="<?php echo SITE_ROOT?>bigpipe/static/bigpipe-callbacks.js" type="text/javascript"></script>
<script type="text/javascript">
	var globalExecution = function globalExecution(code) {
		window.execScript ? window.execScript(code) : window.eval.call(window, code);
	};
</script>
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
  <div class="border-top-s"></div>
  <div class="inner_top_border">
   <div class="row">
      <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9 chk">
      	<div class="row fixxxeee">
  	<div class="col-sm-9 col-lg-sp-9">
    	<?php /*?><div class="search top-fix form-group">
        	<input type="text" class="form-control" placeholder="Search" />
            <input type="submit" hidden="" />
        </div><?php */?>
        <?php include_once(DIR_ROOT."widget/add_post_widget.php"); ?>
    </div>
    <div class="col-sm-3 col-lg-sp-3">
            <div class="invitefrind pull-right">
              <div class="request_sec">
                <div class="request"> <a href="<?php echo SITE_ROOT?>user/invite-friends" class="request_btn"><i class="fa_invite"></i>Invite Friends</a> </div>
              </div>
                <?php
				include(DIR_ROOT."widget/notification_head.php");
				?>
            </div>
    </div>
  </div>
        <div class="row">
        	<div class="col-sm-4">
            	<div class="left_fixedSidebar">
					<?php
					include(DIR_ROOT."widget/user_home_profile_promotion_widget.php");
					?>
               </div>
            </div>
        	<div class="col-sm-8">
				<?php echo $PageletRed;?>
            </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3 widcalculator">
        <div>
            <?php
			include_once('widget/right_ads_sidebar.php');
			?>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>
<!------------share post modal---------->
	<div class="modal fade shared" id="shared" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-body">
			<p>Your share posted to your news feed</p>
		  </div>
		</div>
	  </div>
	</div>
<!-----------end share modal----------->
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/fixed_sidebar.js"></script>
<?php
include_once(DIR_ROOT.'js/include/user_home.php');
BigPipe::render();
include_once(DIR_ROOT."widget/like_view_popup.php");
include_once(DIR_ROOT."includes/footer-home.php");
?>  