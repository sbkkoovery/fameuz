<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/album.php");
include_once(DIR_ROOT."class/album_images.php");
include_once(DIR_ROOT."class/album_images_vote.php");
include_once(DIR_ROOT."class/share.php");
$objAlbums		 =	new album();
$objAlbumImages	=	new album_images();
$objImageVote	  =	new album_images_vote();
$objShare	      =	new share();

$type			  =	'new-photos';
if(isset($_GET['search'])&&$_GET['search']!=''){
	$type			  =	$objCommon->esc($_GET['search']);
}
$limit 					 =	20;
$page 					  =	(int) (!isset($_GET['p'])) ? 1 : $_GET['p'];
if($type=='new-photos'){
	/*$sqlImagesList	=	"SELECT @c:=@c+1 AS serialNumber,tab.* 
		FROM (SELECT * FROM((
			SELECT a.ai_id AS imgId,a.ai_images AS imageUrl,a.user_id,a.ai_like_count AS likeCount,a.ai_comment_count AS commentCount,a.ai_caption AS imageCaption ,u.display_name,u.first_name,u.email,a.ai_created AS createdOn,'album' AS type
			FROM album_images AS a 
			LEFT JOIN users AS u ON a.user_id=u.user_id 
			WHERE u.email_validation=1 AND u.status=1)
			UNION
			(SELECT a.photo_id AS imgId,a.photo_url AS imageUrl,a.user_id,a.photo_like_count AS likeCount,a.photo_comment_count AS commentCount,a.photo_descr AS imageCaption ,u.display_name,u.first_name,u.email,a.photo_created AS createdOn,'wall' AS type 
			FROM user_photos AS a 
			LEFT JOIN users AS u ON a.user_id=u.user_id 
			WHERE u.email_validation=1 AND u.status=1 ))
		AS t GROUP BY user_id)
	AS tab,(SELECT @c:=0) AS c ORDER BY tab.createdOn DESC";*/
	$sqlImagesList	=	"SELECT tab2.* FROM (
		 SELECT @c:=@c+1 AS serialNumber,tab.* 
		  FROM (SELECT * FROM((
		   SELECT a.ai_id AS imgId,a.ai_images AS imageUrl,a.user_id,a.ai_like_count AS likeCount,a.ai_comment_count AS commentCount,a.ai_caption AS imageCaption ,u.display_name,u.first_name,u.email,a.ai_created AS createdOn,'2' AS type
		   FROM album_images AS a 
		   LEFT JOIN users AS u ON a.user_id=u.user_id 
		   WHERE u.email_validation=1 AND u.status=1 ORDER BY createdOn DESC)
		   UNION
		   (SELECT a.photo_id AS imgId,a.photo_url AS imageUrl,a.user_id,a.photo_like_count AS likeCount,a.photo_comment_count AS commentCount,a.photo_descr AS imageCaption ,u.display_name,u.first_name,u.email,a.photo_created AS createdOn,'1' AS type 
		   FROM user_photos AS a 
		   LEFT JOIN users AS u ON a.user_id=u.user_id 
		   WHERE u.email_validation=1 AND u.status=1 ORDER BY createdOn DESC))
		  AS t )
		 AS tab,(SELECT @c:=0) AS c ORDER BY tab.createdOn DESC
		 
		) AS tab2 GROUP BY tab2.user_id";
	
}else if($type=='popular'){
	$sqlImagesList	=	"SELECT @c:=@c+1 AS serialNumber,tab.* 
		FROM ((
			SELECT a.ai_id AS imgId,a.ai_images AS imageUrl,a.user_id,a.ai_like_count AS likeCount,a.ai_comment_count AS commentCount,a.ai_caption AS imageCaption ,u.display_name,u.first_name,u.email,a.ai_created AS createdOn,'2' AS type
			FROM album_images AS a 
			LEFT JOIN users AS u ON a.user_id=u.user_id 
			WHERE u.email_validation=1 AND u.status=1 GROUP BY imgId)
		UNION
			(SELECT a.photo_id AS imgId,a.photo_url AS imageUrl,a.user_id,a.photo_like_count AS likeCount,a.photo_comment_count AS commentCount,a.photo_descr AS imageCaption ,u.display_name,u.first_name,u.email,a.photo_created AS createdOn,'1' AS type 
			FROM user_photos AS a 
			LEFT JOIN users AS u ON a.user_id=u.user_id 
			WHERE u.email_validation=1 AND u.status=1 GROUP BY imgId))
		AS tab,(SELECT @c:=0) AS c ORDER BY likeCount DESC";
}else if($type=='top'){
	$sqlImagesList	=	"SELECT @c:=@c+1 AS serialNumber,tab.* 
		FROM ((
			SELECT a.ai_id AS imgId,a.ai_images AS imageUrl,a.user_id,a.ai_like_count AS likeCount,a.ai_comment_count AS commentCount,a.ai_caption AS imageCaption ,u.display_name,u.first_name,u.email,a.ai_created AS createdOn,'2' AS type,AVG(vote.aiv_vote) AS voteRate 
			FROM album_images_vote AS vote 
			LEFT JOIN album_images AS a ON a.ai_id=vote.ai_id 
			LEFT JOIN users AS u ON a.user_id=u.user_id 
			WHERE u.email_validation=1 AND u.status=1 GROUP BY imgId)
		UNION
			(SELECT a.photo_id AS imgId,a.photo_url AS imageUrl,a.user_id,a.photo_like_count AS likeCount,a.photo_comment_count AS commentCount,a.photo_descr AS imageCaption ,u.display_name,u.first_name,u.email,a.photo_created AS createdOn,'1' AS type,AVG(vote.up_vote) AS voteRate
			FROM user_photos_vote AS vote 
			LEFT JOIN user_photos AS a ON a.photo_id=vote.photo_id 
			LEFT JOIN users AS u ON a.user_id=u.user_id 
			WHERE u.email_validation=1 AND u.status=1 GROUP BY imgId))
		AS tab,(SELECT @c:=0) AS c ORDER BY voteRate DESC";
}else if($type=='last-visited'&&isset($_SESSION['userId'])){
	$sqlImagesList	=	"SELECT @c:=@c+1 AS serialNumber,tab.* 
		FROM ((
			SELECT a.ai_id AS imgId,a.ai_images AS imageUrl,a.user_id,a.ai_like_count AS likeCount,a.ai_comment_count AS commentCount,a.ai_caption AS imageCaption ,u.display_name,u.first_name,u.email,a.ai_created AS createdOn,'2' AS type,iv.iv_created AS visited
			FROM images_view AS iv 
			LEFT JOIN album_images AS a ON iv.iv_content=a.ai_id 
			LEFT JOIN users AS u ON a.user_id=u.user_id 
			WHERE u.email_validation=1 AND u.status=1 AND iv.iv_cat=2 AND iv.iv_user_id=".$_SESSION['userId'].")
		UNION
			(SELECT a.photo_id AS imgId,a.photo_url AS imageUrl,a.user_id,a.photo_like_count AS likeCount,a.photo_comment_count AS commentCount,a.photo_descr AS imageCaption ,u.display_name,u.first_name,u.email,a.photo_created AS createdOn,'1' AS type,iv.iv_created AS visited
			FROM images_view AS iv
			LEFT JOIN user_photos AS a ON iv.iv_content=a.photo_id 
			LEFT JOIN users AS u ON a.user_id=u.user_id 
			WHERE u.email_validation=1 AND u.status=1 AND iv.iv_cat=2 AND iv.iv_user_id=".$_SESSION['userId']."))
		AS tab,(SELECT @c:=0) AS c ORDER BY visited DESC";
}else if($type=='all'){
	$sqlImagesList	=	"SELECT @c:=@c+1 AS serialNumber,tab.* 
		FROM (SELECT * 
			FROM((
				SELECT a.ai_id AS imgId,a.ai_images AS imageUrl,a.user_id,a.ai_like_count AS likeCount,a.ai_comment_count AS commentCount,a.ai_caption AS imageCaption ,u.display_name,u.first_name,u.email,a.ai_created AS createdOn,'2' AS type 
				FROM album_images AS a 
				LEFT JOIN users AS u ON a.user_id=u.user_id 
				WHERE u.email_validation=1 AND u.status=1)
			UNION
				(SELECT a.photo_id AS imgId,a.photo_url AS imageUrl,a.user_id,a.photo_like_count AS likeCount,a.photo_comment_count AS commentCount,a.photo_descr AS imageCaption ,u.display_name,u.first_name,u.email,a.photo_created AS createdOn,'1' AS type
				FROM user_photos AS a
				LEFT JOIN users AS u ON a.user_id=u.user_id 
				WHERE u.email_validation=1 AND u.status=1))
			AS t ORDER BY createdOn DESC)
		AS tab,(SELECT @c:=0) AS c";
}else{
	$searchKey		=	$type;
	$sqlImagesList	=	"SELECT @c:=@c+1 AS serialNumber,tab.* 
	FROM (SELECT * FROM((
		SELECT a.ai_id AS imgId,a.ai_images AS imageUrl,a.user_id,a.ai_like_count AS likeCount,a.ai_comment_count AS commentCount,a.ai_caption AS imageCaption ,u.display_name,u.first_name,u.email,a.ai_created AS createdOn,'2' AS type
		FROM album_images AS a 
		LEFT JOIN users AS u ON a.user_id=u.user_id 
		WHERE u.email_validation=1 AND u.status=1 AND (u.first_name LIKE '%$searchKey%' OR u.last_name LIKE '%$searchKey%' OR u.display_name LIKE '%$searchKey%' OR a.ai_caption LIKE '%$searchKey%'))
	UNION
		(SELECT a.photo_id AS imgId,a.photo_url AS imageUrl,a.user_id,a.photo_like_count AS likeCount,a.photo_comment_count AS commentCount,a.photo_descr AS imageCaption ,u.display_name,u.first_name,u.email,a.photo_created AS createdOn,'1' AS type
		FROM user_photos AS a
		LEFT JOIN users AS u ON a.user_id=u.user_id 
		WHERE u.email_validation=1 AND u.status=1 AND (u.first_name LIKE '%$searchKey%' OR u.last_name LIKE '%$searchKey%' OR u.display_name LIKE '%$searchKey%' OR a.photo_descr LIKE '%$searchKey%')))
	AS t ORDER BY createdOn DESC)AS tab,(SELECT @c:=0) AS c ORDER BY createdOn DESC";
}
$imageCount	=	$objAlbumImages->countRows("SELECT ai_id FROM album_images WHERE 1");
$start 					 =	($page * $limit) - $limit;
if(($objAlbums->countRows($sqlImagesList)) > ($page * $limit) ){
	$next = ++$page;
}
if($page==2){
	$i	=	0;
	$first	=	3;
	$adCount	=	0;
}
$newImagesList	=	$objAlbums->listQuery($sqlImagesList. " LIMIT ".$start.", ".$limit);
?>
<?php /*?><script src="<?php echo SITE_ROOT; ?>js/jquery.mosaicflow.js"></script><?php */?>
<link href="<?php echo SITE_ROOT?>css/rating.css" rel="stylesheet" type="text/css">
<?php /*?><script src="<?php echo SITE_ROOT?>js/jquery-1.7.1.min.js"></script><?php */?>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/masonry.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery-ias.js"></script>
<?php /*?><script type="text/javascript" src="<?php echo SITE_ROOT?>perfect-scrollbar/perfect-scrollbar.js"></script><?php */?>
<?php /*?><link href="<?php echo SITE_ROOT?>css/jquery_scroll.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery-ias.min.js"></script><?php */?>
<link href="<?php echo SITE_ROOT?>fameuz_lightbox/fameuz_lightbox.css" rel="stylesheet">
<link href="<?php echo SITE_ROOT?>css/image_popup.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.css" />
<script src="<?php echo SITE_ROOT?>jw_player/jwplayer.js"></script>
<link href="<?php echo SITE_ROOT?>jw_player/jw_player.css" rel="stylesheet" type="text/css">
<script src="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>fameuz_lightbox/famuez_lightbox.js"></script>
<style type="text/css">
/*.item_review{
	padding:inherit;
}
.infscr-loading{
	bottom:0;
}*/
</style>
<div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="upload_section">
                        	<img src="<?php echo SITE_ROOT_AWS_IMAGES; ?>images/upload_bg.png" alt="upload bg">
                            <div class="fameuz_photos_section">
                            	<div class="fameuz_photo">
                                    <p class="fameuz_nomber"><?php echo number_format($imageCount); ?></p>
                                    <p class="fameuz_photos">Fameuz Photos</p>
                                </div>
                                <div class="clr"></div>
                                <div class="photo_section">
                                    <div class="photo_nav">
                                    	<ul>
                                        	<li<?php echo ($type=='new-photos')?' class="select"':''; ?>><a href="<?php echo SITE_ROOT; ?>photos/?search=new-photos">New Photos</a></li>
                                            <li<?php echo ($type=='popular')?' class="select"':''; ?>><a href="<?php echo SITE_ROOT; ?>photos/?search=popular">Popular Photos</a></li>
                                            <li<?php echo ($type=='top')?' class="select"':''; ?>><a href="<?php echo SITE_ROOT; ?>photos/?search=top">Top Photos</a></li>
                                            <?php if(isset($_SESSION['userId'])){?><li<?php echo ($type=='last-visited')?' class="select"':''; ?>><a href="<?php echo SITE_ROOT; ?>photos/?search=last-visited">Last Visited</a></li><?php }?>
                                            <li<?php echo ($type=='all')?' class="select"':''; ?>><a href="<?php echo SITE_ROOT; ?>photos/?search=all">All Photos</a></li>
                                        </ul>
                                        <!--<div class="new_tab select">New Photos</div>
                                        <div class="new_tab">Popular Photos</div>
                                        <div class="new_tab">Top Photos</div>
                                        <div class="new_tab">Last Visited</div>
                                        <div class="new_tab">All Photos</div>-->
                                    </div>
                                    <div class="upload_set">
                                     	<div class="upload_photo_section">
                                        	<div class="ring_styles" style="z-index:999 !important;">
                                            <?php if(isset($_SESSION['userId'])){
												include_once(DIR_ROOT."widget/notification_head.php");
											 }?>
                                                
                                            </div>
                                            <?php if(isset($_SESSION['userId'])){?>
                                            <div class="tic_upload_photos" style="z-index:999 !important;">
                                            	<a href="javascript:;" data-toggle="modal" data-target="#uploadPhotos"><i class="fa fa-camera"></i>UploadPhoto</a>
                                            </div>
                                            <?php }?>
                                            <?php /*?><div class="round_section">
                                            	<a href="#"><i class="fa fa-circle"></i></a>
                                                <a href="#"><i class="fa fa-circle"></i></a>
                                                <a href="#"><i class="fa fa-circle"></i></a>
                                            </div><?php */?>
                                            <div class="zoom_photos">
                                            	<form action="<?php echo SITE_ROOT; ?>photos/" method="get"><input type="text" id="searchKey" name="search" placeholder="Search Photos" /></form><a href="#"><i class="fa fa-search"></i></a>
                                            </div>
                                        </div>   
                                    </div>
                                    <div class="clearfix"></div>
                                </div>                            
                            </div>
                            <div class="clearfix"></div>
                            
                        </div>
                    	<div class="register_page">
                        	<div class="image_section">
                                <div class="mosaicflow" data-item-height-calculation="attribute">
                                	<?php
									//print_r($newImagesList); die;
									if(count($newImagesList)>0){
									$adArray		 =	array("adOne.jpg","adTwo.jpg");
									foreach($newImagesList as $userImage){
										if($i==3&&$first==3){
									?>
                                    <div class="mosaicflow__item">
                                        <div class="potoImageFrame text-center"><img src="<?php echo SITE_ROOT_AWS_IMAGES."images/".$adArray[0]; ?>" class="img-resposive" /></div>
                                    </div>
                                    <?php 
										$first=0;
										}else{
									if($userImage['serialNumber']%11==10){
									?>
                                    <div class="mosaicflow__item">
                                        <div class="potoImageFrame text-center"><img src="<?php echo SITE_ROOT_AWS_IMAGES."images/".$adArray[0]; ?>" class="img-resposive" /></div>
                                    </div>
                                    <?php }}?>
                                   <?php $getImgsrc = SITE_ROOT_AWS_IMAGES.'uploads/albums/'.$objCommon->html2text($userImage['imageUrl']);
                                         if(@getimagesize($getImgsrc)) { ?>
 
                                     <div class="mosaicflow__item <?php echo $i; ?>" <?php /*?>id="item_review-<?php echo $userImage['user_id']?>"<?php */?>>
                                        <div class="potoImageFrame <?php echo $classPattern[$i]; ?>"><img src="<?php echo SITE_ROOT_AWS_IMAGES."uploads/albums/".$objCommon->html2text($userImage['imageUrl']); ?>" class="lightBoxsPhoto" data-contentId="<?php echo $userImage['imgId']; ?>" data-contentType="<?php echo $userImage['type']; ?>" data-typeOf="<?php echo $type; ?>"  alt=""></div>
                                        <div class="upload_shadow">by <?php echo $objCommon->displayName($userImage); ?></div>
                                        <?php if(isset($_SESSION['userId'])){?>
                                        <div class="vote" id="imageVote<?php echo $userImage['imgId']; ?>">
                                        	<?php 
											$voteExists	=	$objImageVote->countRows("SELECT aiv_id FROM album_images_vote WHERE user_id=".$_SESSION['userId']." AND ai_id=".$userImage['imgId']);
											if(!$voteExists){
											?>
                                            <a href="#vote-image" data-id="<?php echo $userImage['imgId']; ?>" onclick="voteThis(<?php echo $userImage['imgId']; ?>);" class="share_heart3"><i class="fa fa-thumbs-up"></i> Vote</a>
                                            <?php }else{?>
                                            <a id="voteImage" class="share_heart3 voted"><i class="fa fa-thumbs-up"></i> Voted</a>
                                            <?php }?>
                                        </div>
                                        <?php }?>
                                        <div class="clearfix"></div>
                                        <div class="dessi_bottom">
                                            <div class="the_bird">
                                                <h1><?php //echo $objCommon->html2text($userImage['ai_caption']); ?></h1>
                                                <a href="javascript:;" class="lightBoxsPhoto" data-contentId="<?php echo $userImage['imgId']; ?>" data-contentType="<?php echo $userImage['user_id']; ?>" data-typeOf="<?php echo $type; ?>"><i class="fa fa-expand"></i></a>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="by_bottom">
                                                <div class="by">by <?php echo $objCommon->displayName($userImage); ?></div>
                                                <div class="rate">
                                                    <a><i class="fa fa-heart"></i><?php echo $userImage['likeCount']; ?></a>
                                                    <a><i class="fa fa-comments"></i> <?php echo $userImage['commentCount']; ?></a>
                                                 <?php $imageShare =	$objShare->getRowSql("SELECT count(DISTINCT share_id) AS shareCount FROM share WHERE share_content =".$userImage['imgId']." AND share_category =".$userImage['type']." AND share.share_status=1"); ?>
                                                    <a><i class="fa fa-share-alt"></i><?php echo $imageShare['shareCount']; ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?> 
                            		<?php $i++;
									}
									if (isset($next)){ 
									?>
                                        <div id="page-nav">
                                        	<a href="<?php echo SITE_ROOT.'photos/?search='.$type.'&p='.$next?>"></a>
                                        </div>
                                        <?php
                                    }
									}else{
										echo "No images found in this section..!";
									}?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 <!---------------------------------------->
    <div class="remodal popup_box" data-remodal-id="vote-image">
       <div class="rate_table login_table">
		<div>
		Rate this Photo
		</div>
       	<div class="tr ratingSec">
			<div class="rate-ex2-cnt">
				<div id="1" class="rate-btn-1 rate-btn"></div>
				<div id="2" class="rate-btn-2 rate-btn"></div>
				<div id="3" class="rate-btn-3 rate-btn"></div>
				<div id="4" class="rate-btn-4 rate-btn"></div>
				<div id="5" class="rate-btn-5 rate-btn"></div>
                <input type="hidden" name="imageId" id="imgId" />
				<input type="text" name="rate_val" id="rate_val" style="display:none;" />
			</div><input type="button" value="Rate" class="rate_submit">
		</div>
		<div class="tr submit">
			
		</div> 
       </div>
    </div>
    <!---------------------------------------->
<!-- POPUP STARTS -->
<div id="uploadPhotos" class="modal fade" role="dialog">
  <div class="modal-dialog uploadWidth">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload Photos</h4>
      </div>
      <div class="modal-body">
      	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div> 	
<!-- POPUP ENDS -->
<script>

$(document).ready(function(){
	$('body').fameuzLightbox({
		class : 'lightBoxsPhoto',
		sidebar: 'photo',
		photos:{
			imageLoadUrl: '<?php echo SITE_ROOT.'ajax/popUpBox.php'?>',
			data_attr	: ['data-contentId','data-contentType','data-typeOf'],
			likeUrl	: '<?php echo SITE_ROOT?>ajax/like.php',
			shareUrl: '<?php echo SITE_ROOT?>ajax/share.php',
			workedUrl: '<?php echo SITE_ROOT?>ajax/worked_together.php',
			commentBoxUrl: '<?php echo SITE_ROOT.'widget/pop_image_comment_box.php' ?>',
			limitCount: 6,
			comments :{
				commentActionUrl:'<?php echo SITE_ROOT?>access/post_comment.php?action=add_comment',
				commentDelete: '<?php echo SITE_ROOT?>access/delete_comment.php',
			}
		},
		videos : {
			videoLoadUrl: '<?php echo SITE_ROOT.'ajax/popUpVideoUser.php'?>',
			data_attr	: ['data-vidEncrId'],
			likeUrl		: '<?php echo SITE_ROOT?>ajax/like_video.php',
			shareUrl	: '<?php echo SITE_ROOT?>ajax/share_video.php',
			commentBoxUrl:'<?php echo SITE_ROOT.'widget/pop_video_comment_box.php' ?>',
			comments :{
				commentActionUrl:'<?php echo SITE_ROOT?>access/post_video_comment.php?action=add_comment',
			}
		},
		skin: {
			next	: '<img src="<?php echo SITE_ROOT ?>fameuz_lightbox/images/next.png">',
			prev	: '<img src="<?php echo SITE_ROOT ?>fameuz_lightbox/images/prev.png">',
			reset	: '<i class="fa fa-refresh"></i>',
			close	: '<img src="<?php echo SITE_ROOT ?>fameuz_lightbox/images/close.png" width="15">',
			loader	: '<?php echo SITE_ROOT_AWS_IMAGES ?>images/ajax-loader.gif',
			review	: '<i class="fa fa-chevron-right"></i>',
			video	: '<?php echo SITE_ROOT ?>jw_player/six/six.xml',
		}
	});
});
</script>

<script type="text/javascript">
function voteThis(curr){
	$("#imgId").val(curr);
}
$(".vote").on("click","a",function(e){
	var imgId	=	$(this).data("id");
	$("#imgId").val(imgId);
});
$(".rate_table").on("click",".rate_submit",function(){
	var inst = $.remodal.lookup[$('[data-remodal-id=vote-image]').data('remodal')];
	var rate_val				=	$("#rate_val").val();
	var imageId				 =	$("#imgId").val();
	var that		  			=	this;
	if(rate_val&&imageId){
		$(".remodal .ratingSec").html("Rating...");
		$.ajax({
			url:"<?php echo SITE_ROOT;?>ajax/rateImage.php",
			method:"GET",
			data:{rate_val:rate_val,ai_id:imageId},
			success:function(rtnData){
				if(rtnData=='success'){
					$("#imageVote"+imageId).html('<a id="voteImage" class="share_heart3 voted"><i class="fa fa-thumbs-up"></i> Voted</a>');
					var therate = $("#rate_val").val();
					therate++;
					$(".remodal .ratingSec").html('<div class="rate-ex2-cnt"><div id="1" class="rate-btn-1 rate-btn"></div><div id="2" class="rate-btn-2 rate-btn"></div><div id="3" class="rate-btn-3 rate-btn"></div><div id="4" class="rate-btn-4 rate-btn"></div><div id="5" class="rate-btn-5 rate-btn"></div><input type="hidden" name="imageId" id="imgId" /><input type="text" name="rate_val" id="rate_val" style="display:none;" /></div><input type="button" value="Rate" class="rate_submit">');
					for (var i = therate; i <= 5; i++) {
						$('.rate-btn-'+i).removeClass('rate-btn-hover');
					};
					inst.close();
				}
		}});
	}else{
		var therate = $("#rate_val").val();
		therate++;
		for (var i = therate; i <= 5; i++) {
			$('.rate-btn-'+i).removeClass('rate-btn-hover');
		};
		inst.close();
	}
});
$('[data-target="#uploadPhotos"]').click(function(e) {
  e.preventDefault();
  var url = "<?php echo SITE_ROOT.'includes/'; ?>photoUpload.php";
  //var modal_id = $(this).attr('data-target');
  $.get(url, function(data) {
	  //alert(data);
      $('.modal-body').html(data);
		/*$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/timeline_photo_page.php',function(){
			$(".load_album_preloader").hide();
			$("#edit_album_li").css("display", "none");$("#add_photo_li").css("display", "none");$("#create_album_li").css("display", "inline-block");;$("#add_videos_li").css("display", "inline-block");$("#all_photos_li").css("display", "inline-block");
		});*/
	  
  });
});
function showAlbum(aId){
	$(".load_album_content").html('<div class="load_album_preloader"></div>');
	$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/photo_album_photo_page.php?albumId='+aId,function(){
		$(".load_album_preloader").hide();
		$(".albumList").hide();
		$(".load_album_content").prepend('<div class="backTolist"><a href="javascript:;" onclick="loadFolder();">Back to Folder</a></div>');
		$("#create_album_li").css("display", "none");
		$("#add_videos_li").css("display", "none");
		$("#edit_album_li").css("display", "inline-block");
		$("#all_photos_li").css("display", "inline-block"); 
		$("#edit_album_id").val(aId);
	});
}
function showWallImg(){
	$(".load_album_content").html('<div class="load_album_preloader"></div>');
	$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/photo_timeline_photo_page.php',function(){
		$(".albumList").hide();
		$(".load_album_content").prepend('<div class="backTolist"><a href="javascript:;" onclick="loadFolder();">Back to Folder</a></div>');
		$(".load_album_preloader").hide();
		$("#edit_album_li").css("display", "none");$("#add_photo_li").css("display", "none");$("#create_album_li").css("display", "inline-block");;$("#add_videos_li").css("display", "inline-block");$("#all_photos_li").css("display", "inline-block");
	});
	
}
function loadFolder(){
	$(".modal-body").load('<?php echo SITE_ROOT.'includes/'; ?>photoUpload.php');
}
$('#uploadPhotos').on('hidden.bs.modal', function () {
    location.reload();
})
</script>
<script>
	$(document).ready(function(e) {
        $(".zoom_photos a").click(function(){
			$(".zoom_photos form, .ring_styles, .tic_upload_photos").toggleClass("slideSearch");
		});
    });

$(window).load(function(e) {
 var container = document.querySelector('.mosaicflow');
    var msnry = new Masonry( container, {
      // options
      itemSelector: '.mosaicflow__item',
      gutter: 0
    });

    var ias = $.ias({
      container: ".mosaicflow",
      item: ".mosaicflow__item",
      pagination: "#page-nav",
      next: "#page-nav a",
      delay: 1200
    });

    ias.on('render', function(items) {
      $(items).css({ opacity: 0 });
    });

    ias.on('rendered', function(items) {
      msnry.appended(items);
    });
    ias.extension(new IASSpinnerExtension());
    ias.extension(new IASNoneLeftExtension({html: '<div class="ias-noneleft" style="text-align:center"><p><b>No more images to load..!</b></p></div>'}));
    
});


  /*$(function(){
    
    var $container = $('.mosaicflow');
    
    $container.imagesLoaded(function(){
      $container.masonry({
        itemSelector: '.mosaicflow__item'
      });
    });
    
    //$container.infinitescroll({
//      navSelector  : '#page-nav',    // selector for the paged navigation 
//      nextSelector : '#page-nav a',  // selector for the NEXT link (to page 2)
//      itemSelector : '.mosaicflow__item',     // selector for all items you'll retrieve
//      loading: {
//          finishedMsg: 'No more images to load.',
//          img: '<?php echo SITE_ROOT.'images/album_preloader.GIF'; ?>',
//        }
//      },
      // trigger Masonry as a callback
     // function( newElements ) {
//        // hide new items while they are loading
//        var $newElems = $( newElements ).css({ opacity: 0 });
//        // ensure that images load before adding to masonry layout
//        $newElems.imagesLoaded(function(){
//          // show elems now they're ready
//          $newElems.animate({ opacity: 1 });
//          $container.masonry( 'appended', $newElems, true ); 
//        });
//      }
   // );
    
  });*/
</script>
<?php
include_once(DIR_ROOT."js/include/rating_js.php");
include_once(DIR_ROOT."includes/footer.php");
?>