<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/is_session.php");
include_once(DIR_ROOT."class/main_category.php");
$objMainCategory				=	new main_category();
$getMainCategoryAll			 =	$objMainCategory->getAll();
$mainCat						=	$objCommon->esc($_GET['main-cat']);
$cat							=	$objCommon->esc($_GET['cat']);
$emuser 						 =	$_GET['emuser'];
?>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery.validate.js"></script>
<link href="<?php echo SITE_ROOT?>css/cmxform.css" rel="stylesheet" type="text/css" />
    <div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="register_heading">
                        	<h1>Join the world's largest Fashion models network.</h1>
                            <p>Professional Model, New face, Photographers, Agency, Hair and makeup, Artist, Fashion stylist, Spectator, Industry Professional</p>
                        </div>
                        <div class="register_content">
                        	<?php 
							include_once(DIR_ROOT."widget/register_user_images.php");
							?>
                        	<div class="register_box">
                            	<div class="register_form_box" id="register_form_box">
                                    <h1>Get started – it’s free.</h1>
                                    <h6>Registration takes less than 2 minutes.</h6>
                                    <?php
									echo $objCommon->displayMsg();
									?>
                                    <form action="<?php echo SITE_ROOT?>access/add_register_home.php?action=add" method="post" id="register_home_frm" role="form">
                                    <div class="register_form">
                                    	<div class="tr form-group">
                                            <div class="icon"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/register_icon1.png" alt="register_icon1" /></div>
                                            <div class="field">
                                                <div class="select_arrow">
                                                    <select name="reg_main_cat" id="reg_main_cat" >
                                                    	<?php
														foreach($getMainCategoryAll as $allMainCategoryAll){
														?>
                                                        <option value="<?php echo $objCommon->html2text($allMainCategoryAll['mc_id']);?>" <?php echo ($mainCat==$allMainCategoryAll['mc_id'])?'selected':''?>><?php echo $objCommon->html2text($allMainCategoryAll['mc_name']);?></option>
                                                        <?php
														}
														?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="clr"></div>
                                        </div>
                                        <div class="tr form-group">
                                            <div class="icon"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/who.png" alt="register_icon1" /></div>
                                            <div class="field">
                                                <div class="select_arrow">
                                                    <select name="reg_cat" id="reg_cat" >
                                                        <option value="">Tell us who you are:</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="clr"></div>
                                        </div>
										 <div class="tr form-group otherCat">
                                            <div class="field"><input type="text" placeholder="Type your Profession" name="other_prof" id="other_prof"   /></div>
                                            <div class="clr"></div>
                                        </div>
                                        <div class="tr form-group">
                                            <div class="icon"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/register_icon2.png" alt="register_icon2" /></div>
                                            <div class="field"><input type="text" placeholder="Your email address" name="reg_email" id="reg_email"   /></div>
                                            <div class="clr"></div>
                                        </div>
                                        <div class="tr form-group">
                                            <div class="icon"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/register_icon3.png" alt="register_icon3" /></div>
                                            <div class="field"><input type="password" placeholder="Password ( 6 or more characters)" name="reg_pwd" id="reg_pwd"  /></div>
                                            <div class="clr"></div>
                                        </div>
                                        <div class="tr submit">
                                            <input type="submit" value="Register Now" name="register_home" />
                                        </div>
                                        <div class="tr submit clicking">
                                            <p>By clicking Register Now, you agree to <a href="#">Fameuz</a> <br>
    <a href="#">User Agreement</a>, <a href="#">Privacy Policy</a> and <a href="#">Cookie Policy</a>. </p>
                                        </div>
                                        <div class="tr submit facebook_login">
                                            <a href="#" id="fb_login"><span>Sign Up with Facebook</span></a>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                                <div class="register_fb_box" id="register_fb_box">
                                	<div id="close_btn" class="close_btn"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/popup_close.png" alt="popup_close" /></div>
                                	<div class="fb_logo"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/fb_logo.png" alt="fb_logo" /></div>
                                    <h3>Are you sure that you don’t want
 to connect your account to Facebook?</h3>
 									<p>Just think of all the cool things you will be missing out on!</p>
                                    <div class="hr"></div>
                                    <h2>Faster access!</h2>
                                    <p>Log in with one click! No need to type your email or password!</p>
                                    <div class="hr hr_bg"></div>
                                    <h2>Keep your network informed!</h2>
                                    <p>Your Facebook friends will get your latest news and successes! 
Find more industry contacts through your network!</p>
                                    <div class="hr hr_bg"></div>
                                    <h2>Manage your permissions!</h2>
                                    <p>You can decide whether you share or not, so you’re in full control!</p>
                                    <div class="fb_login_arrow"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/fb_login_arrow.png" alt="fb_login_arrow" /></div>
                                    <div class="facebook_login">
                                        <a href="<?php echo SITE_ROOT?>fb/fbconfig.php"><span>Sign Up with Facebook</span></a>
                                    </div>
                                </div>
                                <!-- For facebook login -->
                          <div style="display:none;" class="facebook_loginreg" id="facebook_loginreg" >
                          <h2>
                            <img src="https://graph.facebook.com/<?php echo $_SESSION['FBID']; ?>/picture?type=small">  <?php echo $_SESSION['FULLNAME']; ?>
                          </h2>
                          <h6 style="color:#0C0;">
                                Dear <?php echo $_SESSION['FULLNAME']; ?> Please Fill out the following fields before you continue.
                          </h6>
                          <?php
							echo $objCommon->displayMsg();
						  ?>
                          <form action="<?php echo SITE_ROOT?>access/add_fb_register_home.php?action=add" method="post" id="register_home_frmfb" role="form">
                               <div class="register_form">
                               <?php if($emuser=='Noemail'){ ?>
                               <div class="tr form-group">
                                       <div class="icon"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/register_icon2.png" alt="register_icon2" /></div>
                                       <div class="field"><input type="text" placeholder="Your email address" name="reg_emailfb" id="reg_emailfb"   /></div>
                                       <div class="clr"></div>
                               </div>
                               <?php } else { ?>
                               <div class="tr form-group">
                               <div class="icon"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/register_icon2.png" alt="register_icon2" /></div>
                               <div class="field">
                                    <input type="text" placeholder="Your email address" value="<?php echo $emuser;?>" name="reg_emailfb" id="reg_emailfb"   />
                               </div>
                               <div class="clr"></div>
                               </div>
                               <?php } ?>
                               <div class="tr form-group">
                               <div class="icon"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/register_icon1.png" alt="register_icon1" /></div>
                               <div class="field">
                               <div class="select_arrow">
                                     <select name="reg_main_catfb" id="reg_main_catfb"   >
                                   	 <?php
														foreach($getMainCategoryAll as $allMainCategoryAll){
														?>
                                                        <option value="<?php echo $objCommon->html2text($allMainCategoryAll['mc_id']);?>" <?php echo ($mainCat==$allMainCategoryAll['mc_id'])?'selected':''?>><?php echo $objCommon->html2text($allMainCategoryAll['mc_name']);?></option>
                                                        <?php
														}
														?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="clr"></div>
                                        </div>
                                        <div class="tr form-group">
                                            <div class="icon"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/who.png" alt="register_icon1" /></div>
                                            <div class="field">
                                                <div class="select_arrow">
                                                    <select name="reg_catfb" id="reg_catfb" >
                                                        <option value="">Tell us who you are:</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="clr"></div>
                                        </div>
                                        <div class="ermssg" id="ermssg" style="display:none;">
                                        <h6>Please Fill all the fields.</h6>
                                        </div>
                                      <!-- <div class="tr submit clicking">
                                            <p>By clicking Register Now, you agree to <a href="#">Fameuz</a> <br>
    <a href="#">User Agreement</a>, <a href="#">Privacy Policy</a> and <a href="#">Cookie Policy</a>. </p>
                                        </div>-->
                                        <div class="tr submit facebook_login">
                                         <input type="submit" value="Sign Up with Facebook" name="register_home" />
                                            
                                        </div>
                                    </div>
                                    </form>
                                </div>
                                
                                
                                
                                
                                
                                
                            </div>
                            <div class="clr"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php  if($emuser!=''){ 
echo "<script type=\"text/javascript\">
       	$(document).ready(function(){
	    $('#register_form_box').fadeOut(function(){
		$('#register_fb_box').fadeOut();
		document.getElementById('facebook_loginreg').style.display = 'block'
		});
		return false;		
	    });
        </script>
        ";
}?>      
    
<script language="javascript" type="text/javascript">
$(document).ready(function(e){
	$("#register_home_frm").validate({
			rules: {
				reg_main_cat: "required",
				reg_cat: "required",
				reg_email:{required:true,email: true},
				reg_pwd: {required:true,minlength: 6},
			},
			messages: {
				reg_main_cat: 'Can\'t be empty',
				reg_cat: 'Can\'t be empty',
				reg_email: {required:'Can\'t be empty',email:'Please enter valid email address'},
				reg_pwd: {required:'Can\'t be empty',minlength:'Password must be at least 6 characters long'},
			}
	});
	var reg_main_cat			=	$("#reg_main_cat").val();
	if(reg_main_cat){
		get_category_fun(reg_main_cat);
	}
	$("#reg_main_cat").change(function(){
		var reg_main_cat_ch	=	$(this).val();
		if(reg_main_cat_ch){
			get_category_fun(reg_main_cat_ch);
		}
	});
	function get_category_fun(main_cat){
		var site_root		 =	'<?php echo SITE_ROOT?>';
		var cat			   =	'<?php echo $cat?>';
		if(main_cat){
			$.get(site_root+"ajax/get_category.php",{'main_cat':main_cat,'cat':cat},function(data){
				$("#reg_cat").html(data);
			});
		}
	}
	$("#register_home_frmfb").validate({
			rules: {
				reg_main_catfb: "required",
				reg_catfb: "required",
				reg_emailfb:{required:true,email: true},
				
			},
			messages: {
				reg_main_catfb: 'Can\'t be empty',
				reg_catfb: 'Can\'t be empty',
				reg_emailfb: {required:'Can\'t be empty',email:'Please enter valid email address'},
				
			}
	
	});
	var reg_main_catfb			=	$("#reg_main_catfb").val();
	if(reg_main_catfb){
		get_category_funb(reg_main_catfb);
	}
	$("#reg_main_catfb").change(function(){
		var reg_main_cat_ch	=	$(this).val();
		if(reg_main_cat_ch){
			get_category_funb(reg_main_cat_ch);
		}
	});
	function get_category_funb(main_cat){
		var site_root		 =	'<?php echo SITE_ROOT?>';
		if(main_cat){
			$.get(site_root+"ajax/get_category.php",{'main_cat':main_cat},function(data){
				$("#reg_catfb").html(data);
			});
		}
	}
});
$(".field").on("change","#reg_cat",function(){
	var otherVal		=	$('#reg_cat option:selected').text();
	if(otherVal=='Others'){
		$(".otherCat").show();
	}else{
		$(".otherCat").hide();
	}
});

</script>
<?php
include_once(DIR_ROOT."includes/login_box.php");
include_once(DIR_ROOT."includes/footer.php");
?>