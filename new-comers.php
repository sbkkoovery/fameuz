<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
?>
<div class="inner_content_section">
	<div class="container">
		<div class="row">
				<div class="inner_top_border">
					<div class="user_profile">
					<?php echo $objCommon->checkEmailverification();?>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
						<div class="content">
							<div class="pagination_box">
                                <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
                                <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                 <a href="#" class="active"> New Comers </a>
                                <?php
								include_once(DIR_ROOT."widget/notification_head.php");
								?>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div>
										<div class="visitor_head_box seperateIt">
                                            <h5>New Comers</h5>
                                            <span class="arw-point"></span>
                                        </div>
									</div>
								</div>
							</div>
							<div class="visitor_list_box">
								<ul class="row"></ul>
							</div>
						</div>
                        </div>
						<?php
						include_once(DIR_ROOT."widget/right_ads_sidebar.php");
						?>
					</div>
				</div>
			</div>
	</div>
</div>
<script>
$(document).ready(function(){
	getresult('<?php echo SITE_ROOT?>ajax/new_comer_list.php');
	 function getresult(url) {
		$.ajax({
			url: url,
			type: "GET",
			data:  {rowcount:$("#rowcount").val()},
			beforeSend: function(){
			$('#loader-icon').show();
			},
			complete: function(){
			$('#loader-icon').hide();
			},
			success: function(data){
				$(".visitor_list_box ul").append(data);
				if($("#total-count").val()>0){
				}else{
					var imageUrl	=	'images/no_user.png';
						dummyBox	=	$('.dummy_box');
					$(".visitor_list_box ul").html('<div class="dummy_box"></div>');
					$('.dummy_box').css('background-image', 'url("' + imageUrl + '")');
					$('.dummy_box').css('border-bottom', '0px');
				}
			},
			error: function(){} 	        
	   });
	}
	$(window).scroll(function(){
		if ($(window).scrollTop() == $(document).height() - $(window).height()){
			if($(".pagenum:last").val() <= $(".total-page").val()) {
				var pagenum = parseInt($(".pagenum:last").val()) + 1;
				getresult('<?php echo SITE_ROOT?>ajax/new_comer_list.php?page='+pagenum);
			}
		}
	}); 
	$('#follower_search').keyup(function(){
		var keyVal		=	$(this).val();
			getSearchresult('<?php echo SITE_ROOT?>ajax/new_comer_list.php?search='+keyVal+'&page=1');
	});
	function getSearchresult(url) {
		$.ajax({
			url: url,
			type: "GET",
			data:  {rowcount:$("#rowcount").val()},
			beforeSend: function(){
			$('#loader-icon').show();
			},
			complete: function(){
			$('#loader-icon').hide();
			},
			success: function(data){
				if(data!=""){
					$(".visitor_list_box ul").html(data);
				}else{
					$(".visitor_list_box ul").html('<li class="col-md-6"><p>No New comers found....</p></li>');
				}
			},
			error: function(){} 	        
	   });
	}

});
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  