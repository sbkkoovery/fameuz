<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."class/users.php");
include_once(DIR_ROOT."class/user_verification.php");
$objUsers				 =	new users();
$objUserVerification	  =	new user_verification();
$confirm				  =	$objCommon->esc($_GET['confirm']);
$explConfirm			  =	explode("_fameuz",$confirm);
$userId				   =	$explConfirm[1];
$confirmation    		 =	$explConfirm[0];
if(isset($userId,$confirmation)){
		$getConDetails	=	$objUserVerification->getRow("uv_verification='".$confirmation."' and user_id=".$userId);
		if($getConDetails['user_id']!=''){
			$getMyDet	 =	$objUsers->getRow("user_id=".$getConDetails['user_id']);
			$objUsers->updateField(array("email_validation"=>1),"user_id=".$getMyDet['user_id']);
		}else{
			header("location:".SITE_ROOT."user/login");
		}
}else{
	header("location:".SITE_ROOT."user/login");
}
?>
    <div class="inner_content_section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="inner_top_border">
                        <div class="user_profile">
								<div class="row">
                                	<div class="col-sm-8 col-sm-offset-2">
                                    	<div class="success_main">
                                            <div class="jumbotron">
                                                <h1>Hello <?php echo $objCommon->html2text($getMyDet['email'])?> ,</h1>
                                                <p>Welcome to FAMEUZ.COM.You are now part of the fastest growing Professional Social Network in the World.</p>
                                                <p><a class="btn btn-primary btn-sm" href="<?php echo SITE_ROOT?>user/login" role="button">View Your profile</a></p>
                                                </div>
                                        </div>
                                    </div> 
                                </div>
                            	<div class="clr"></div>                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php

include_once(DIR_ROOT."includes/login_box.php");
include_once(DIR_ROOT."includes/footer.php");
?>
