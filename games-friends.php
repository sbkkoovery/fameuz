<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/games.php");
$objGames								=	new games();
$user_url								=	$objCommon->esc($_GET['user_url']);
if($user_url ==''){
	header("location:".SITE_ROOT."games");
	exit;
}else{
	$getMyFriendDetails			  	=	$objUsers->getRowSql("SELECT user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url
															FROM users AS user 
															LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
															LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
															WHERE user.status=1 AND user.email_validation=1 AND social.usl_fameuz='".$user_url."'");
}
$gameSearchSql					   	   =	"SELECT games.*,cat.gc_name
												FROM games 
												LEFT JOIN game_category AS cat ON games.gc_id = cat.gc_id 
												LEFT JOIN game_player AS player ON games.g_id = player.g_id
												WHERE  games.g_status=1 AND player.user_id='".$getMyFriendDetails['user_id']."'
												ORDER BY player.gp_created desc LIMIT 32";
$getGamesList							=	$objGames->listQuery($gameSearchSql);
?>
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
  <div class="inner_top_border">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-sp-9">
      <div class="jobs-head">
        <div class="pagination_box_jobs pagination_box">
        <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
		<a href="<?php echo SITE_ROOT.$user_url?>"> <?php echo $objCommon->displayName($getMyFriendDetails)?> </a><i class="fa fa-caret-right"></i></a>
        <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
 	     <a href="javascript:;" class="active"> Games</a>
       <?php
        include_once(DIR_ROOT."widget/notification_head.php");
        ?>
        </div>
       </div>
        <div class="tab_white_most_wanted">
			     
		</div>
        <div class="games_categories">
            <div class="top-category">
				<p><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/game-info.png" /><?php echo $objCommon->displayName($getMyFriendDetails)?>'s Games</p>
			</div>
            <div class="row">
            	<div class="col-sm-12">
                	<div class="owl-carousel_2 ovr">
                    <?php
					if(count($getGamesList) >0){
						foreach($getGamesList as $allGetGames){
						?>
							<div class="col-sm-3">
							<div class="item_1">
								 <a href="<?php echo SITE_ROOT.'games/show/'.$objCommon->html2text($allGetGames['g_alias']).'-'.$objCommon->html2text($allGetGames['g_id'])?>">
									<img src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/games/img/'.$objCommon->getThumb($allGetGames['g_img'])?>" />
								 </a>
									 <span class="game-info">
										<p><a href="<?php echo SITE_ROOT.'games/show/'.$objCommon->html2text($allGetGames['g_alias']).'-'.$objCommon->html2text($allGetGames['g_id'])?>"><?php echo $objCommon->html2text($allGetGames['g_name'])?></a></p>
										<span class="rating_box">
											<span class="rating_yellow" style="width:40%"></span>
										</span>
										<span class="game-cat"><p><?php echo $objCommon->html2text($allGetGames['gc_name'])?></p></span>
									</span>
								</div>
								</div>
							   <?php 
								}
						 }else{
							 echo '<p>No games found....</p>';
						 }
						?>
						   </div>
                   
                </div>
            </div>
        </div>
      </div>
	  <?php
	  include_once(DIR_ROOT."widget/games_right_widget.php");
	  ?>
    </div>
    </div>
  </div>
</div>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>