<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
$cat					=	$objCommon->esc($_GET['cat']);
$queryString			=	$_SERVER['QUERY_STRING'];
$myCategoryId		   =	$getUserDetails['uc_c_id'];
$countryId			  =	$_SESSION['country_user']['country_id'];
?>
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
  <div class="inner_top_border">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
		  <div class="jobs-head">
			<div class="pagination_box">
            <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
            <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
             <a href="javascript:;" class="active"> Search </a>
		   <?php
			include_once(DIR_ROOT."widget/notification_head.php");
			?>
			</div>
		   </div>
		   <div class="tab_white_search">
			   <div class="section_search">
			   <ul>
                    <li <?php echo ($cat=='members')?'class="hactive"':''?>><a href="<?php echo SITE_ROOT.'search?cat=members'?>">Members</a><?php echo ($cat=='members')?'<span class="point-it"></span>':''?></li>
                    <li <?php echo ($cat=='companies')?'class="hactive"':''?>><a href="<?php echo SITE_ROOT.'search?cat=companies'?>">Companies</a><?php echo ($cat=='companies')?'<span class="point-it"></span>':''?></li>
                    <li <?php echo ($cat=='blogs')?'class="hactive"':''?>><a href="<?php echo SITE_ROOT.'search?cat=blogs'?>">Blogs</a><?php echo ($cat=='blogs')?'<span class="point-it"></span>':''?></li>
                    <li <?php echo ($cat=='music')?'class="hactive"':''?>><a href="<?php echo SITE_ROOT.'search?cat=music'?>">Music</a><?php echo ($cat=='music')?'<span class="point-it"></span>':''?></li>
                    <li <?php echo ($cat=='video')?'class="hactive"':''?>><a href="<?php echo SITE_ROOT.'search?cat=video'?>">Video</a><?php echo ($cat=='video')?'<span class="point-it"></span>':''?></li>
                    <li <?php echo ($cat=='photos')?'class="hactive"':''?>><a href="<?php echo SITE_ROOT.'search?cat=photos'?>">Photos</a><?php echo ($cat=='photos')?'<span class="point-it"></span>':''?></li>
               </ul>
               <span>
               		<?php /*?><form action="" method="get">
                    	<input type="hidden" name="cat" value="<?php echo $cat;?>" />
                        <input type="text" name="searchKey" value="<?php echo $_GET['searchKey']; ?>" placeholder="Search <?php echo $cat?>" />
                    </form><?php */?>
               </span>
			   </div>
		   </div>
		   <div class="search-place">
				<?php
					if($cat=='members'){
						echo '<div class="newBaground">';
							include_once(DIR_ROOT."ajax/search_members.php");
						echo '</div>';
					}else if($cat=='video'){
						 echo '<div class="newBaground no-padding_no_bg">';
							include_once(DIR_ROOT."ajax/search_videos.php");
						 echo '</div>';
					}else if($cat=='blogs'){
						 echo '<div class="newBaground no-padding_no_bg">';
							include_once(DIR_ROOT."ajax/search_blogs.php");
						 echo '</div>';
					}else if($cat=='companies'){
						 echo '<div class="newBaground">';
							include_once(DIR_ROOT."ajax/search_companies.php");
						 echo '</div>';
					}else if($cat=='music'){
						 echo '<div class="newBaground no-padding_no_bg">';
							include_once(DIR_ROOT."ajax/search_music.php");
						 echo '</div>';
					}else if($cat=='photos'){
						 echo '<div class="newBaground">';
							include_once(DIR_ROOT."ajax/search_photos.php");
						 echo '</div>';
					}
				?>
		   </div>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3">
        <div class="fixer">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <?php include_once(DIR_ROOT."widget/right_search_main.php");?>
            </div>
          </div>
		   <?php include_once(DIR_ROOT."widget/ad/right_ad_single_block_search.php");?>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>
<script>

$(window).load(function(e) {
	$('.fixer').fixedSidebar();
});
$(document).ready(function(){
	var cat			=	'<?php echo $cat?>';
	var loadDiv		=	$('.newBaground');
	 function getresult(url) {
		$.ajax({
			url: url,
			type: "GET",
			data:  {rowcount:$("#rowcount").val()},
			beforeSend: function(){
			$('#loader-icon').show();
			},
			complete: function(){
				<?php if($cat=='photos'){?>
				initialiceMasonry();
					<?php }?>
			$('#loader-icon').hide();
			},
			success: function(data){
				$(".search-place .newBaground").append(data);
				if($("#total-count").val()>0){
				<?php if($cat=='photos'){?>
					initialiceMasonry();
					
					 $(".lightBoxs").lightBox({
					  videoSkin: '<?php echo SITE_ROOT?>jw_player/six/six.xml',
					  photosObj: true,
					  videoObj :  true,
					  photoPage: true,
					  deleteCommentUrl : '<?php echo SITE_ROOT ?>access/delete_comment.php',
					  skins : {
					   loader   : '<?php echo SITE_ROOT?>images/ajax-loader.gif',
					   next      : '<img src="<?php echo SITE_ROOT?>fameuz_lightbox/images/next.png">',
					   prev   : '<img src="<?php echo SITE_ROOT?>fameuz_lightbox/images/prev.png">',
					   dismiss     : '<img src="<?php echo SITE_ROOT?>fameuz_lightbox/images/close.png" width="15">',
					   reviewIcon  : '<i class="fa fa-chevron-right"></i>',
					  },
					  photos : {
					   fetchUrl: '<?php echo SITE_ROOT.'ajax/popUpBox.php'?>',
					   ajaxUrl : '<?php echo SITE_ROOT.'widget/pop_image_comment_box.php' ?>',
					   comment : {
						CommentFormAction : '<?php echo SITE_ROOT?>access/post_comment.php?action=add_comment',
						  commentLikeUrl   : '<?php echo SITE_ROOT?>ajax/like_comment.php',
					   },
					   shareItems : {
						likeAjaxUrl  : '<?php echo SITE_ROOT?>ajax/like.php',
						shareAjaxUrl : '<?php echo SITE_ROOT?>ajax/share.php',
						workedAjaxUrl: '<?php echo SITE_ROOT?>ajax/worked_together.php',
					   },
					  }
					 });
				<?php }?>
				}else{
					$(".search-place .newBaground").html('<div class="row"><div class="col-md-6"><p>No results found....</p></div></div>');
				}
			},
			error: function(){} 	        
	   });
	}
	var cat	=	'<?php echo $cat?>';
	if(cat != 'photos'){
		$(window).scroll(function(){
			if ($(window).scrollTop() == $(document).height() - $(window).height()){
				if($(".pagenum:last").val() <= $(".total-page").val()) {
					var pagenum = parseInt($(".pagenum:last").val()) + 1;
					var cat			=	'<?php echo $cat?>';
					if(cat=='members'){
						getresult('<?php echo SITE_ROOT?>ajax/search_members_load.php?page='+pagenum+'&myCategoryId=<?php echo $myCategoryId?>&countryId=<?php echo $countryId?>&<?php echo $queryString?>');
					}else if(cat=='video'){
						getresult('<?php echo SITE_ROOT?>ajax/search_videos_load.php?page='+pagenum+'&myCategoryId=<?php echo $myCategoryId?>&countryId=<?php echo $countryId?>&<?php echo $queryString?>');
					}else if(cat=='blogs'){
						getresult('<?php echo SITE_ROOT?>ajax/search_blogs_load.php?page='+pagenum+'&<?php echo $queryString?>');
					}else if(cat=='companies'){
						getresult('<?php echo SITE_ROOT?>ajax/search_companies_load.php?page='+pagenum+'&<?php echo $queryString?>');
					}else if(cat=='music'){
						getresult('<?php echo SITE_ROOT?>ajax/search_music_load.php?page='+pagenum+'&myCategoryId=<?php echo $myCategoryId?>&countryId=<?php echo $countryId?>&<?php echo $queryString?>');
					}
				}
			}
		});
	}
});
</script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/fixed_sidebar.js"></script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>