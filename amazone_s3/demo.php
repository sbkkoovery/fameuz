<?php
require 'aws/aws-autoloader.php';
use Aws\S3\S3Client;

$bucket = 'test-fameuz';
$keyname = 'my-files/sandeep.jpg';
// $filepath should be absolute path to a file on disk						
$filepath = '45.jpg';
$s3 = new S3Client([
				'version' => 'latest',
				'region'  => 'us-west-2',
				'credentials' => [
									'key'    => AWS_ID,
									'secret' => AWS_KEY
								  ]
		]);					
try {
    $s3->putObject([
        'Bucket' => $bucket,
        'Key'    => $keyname,
        'Body'   => fopen($filepath, 'r'),
        'ACL'    => 'public-read',
    ]);
} catch (Aws\Exception\S3Exception $e) {
    echo "There was an error uploading the file.\n";
}
?>