<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/is_session.php");
?>
<link href="<?php echo SITE_ROOT?>css/register.css" rel="stylesheet" type="text/css">
    <div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="register_heading">
                        	<h1>Join the world's largest Fashion models network.</h1>
                            <p>Professional Model, New face, Photographers, Agency, Hair and makeup, Artist, Fashion stylist, Spectator, Industry Professional</p>
                        </div>
                        <div class="register_content login-session">
                        	<?php 
							include_once(DIR_ROOT."widget/register_user_images.php");
							?>
                        	<div class="register_box">
                            	<div class="register_form_box loginBx" id="register_form_box">
                                    <h1>Fameuz.com Login</h1>
                                    <?php
									echo $objCommon->displayMsg();
									?>
                                    <form action="<?php echo SITE_ROOT?>access/login.php?action=login" method="post" role="form">
                                    <div class="register_form">
                                        <div class="tr form-group">
                                            <div class="icon"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/register_icon2.png" alt="register_icon2" /></div>
                                            <div class="field"><input type="text" placeholder="Your email address" name="login_email" id="login_email" autocomplete="off" /></div>
                                            <div class="clr"></div>
                                        </div>
                                        <div class="tr form-group">
                                            <div class="icon"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/register_icon3.png" alt="register_icon3" /></div>
                                            <div class="field"><input type="password" placeholder="Password" name="login_passoword" id="login_passoword" autocomplete="off"  /></div>
                                            <div class="clr"></div>
                                        </div>
                                        <div class="tr submit">
                                            <input type="submit" value="Log in" name="register_home" />
                                        </div>
                                        
                                        <div class="tr submit facebook_login">
                                            <a href="#" id="fb_login"><span>Sign Up with Facebook</span></a>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                                <div class="register_fb_box" id="register_fb_box">
                                	<div id="close_btn" class="close_btn"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/popup_close.png" alt="popup_close" /></div>
                                	<div class="fb_logo"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/fb_logo.png" alt="fb_logo" /></div>
                                    <h3>Are you sure that you don’t want
 to connect your account to Facebook?</h3>
 									<p>Just think of all the cool things you will be missing out on!</p>
                                    <div class="hr"></div>
                                    <h2>Faster access!</h2>
                                    <p>Log in with one click! No need to type your email or password!</p>
                                    <div class="hr hr_bg"></div>
                                    <h2>Keep your network informed!</h2>
                                    <p>Your Facebook friends will get your latest news and successes! 
Find more industry contacts through your network!</p>
                                    <div class="hr hr_bg"></div>
                                    <h2>Manage your permissions!</h2>
                                    <p>You can decide whether you share or not, so you’re in full control!</p>
                                    <div class="fb_login_arrow"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/fb_login_arrow.png" alt="fb_login_arrow" /></div>
                                    <div class="facebook_login">
                                        <a href="<?php echo SITE_ROOT?>fb/fbconfig.php"><span>Sign Up with Facebook</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
include_once(DIR_ROOT."includes/login_box.php");
include_once(DIR_ROOT."includes/footer.php");
?>