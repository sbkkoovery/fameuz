<?php
echo "here";
exit;
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
?>
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
    <div class="inner_top_border">
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
          <div class="row">
            <div class="col-sm-12">
              <div class="pagination_box"> <a href="#">Home <i class="fa fa-caret-right"></i></a> <a href="#" class="active"> My profile </a>
                <?php
				include_once(DIR_ROOT."widget/notification_head.php");
				?>
              </div>
            </div>
          </div>
          <div class="main-background-white">
            <div class="row">
              <div class="col-sm-12">
                <div class="upload-sec">
                  <div class="upload-warning">
                    <div class="upload-warning-head">
                      <p>Please follow this rules :-</p>
                    </div>
                    <div class="upload-warning-points">
                      <ol>
                        <li> Lorem Ipsum is printing and typesetting industry. </li>
                        <li>Lorem Ipsum is simply dummy text of typesetting industry. </li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting ind. </li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </li>
                      </ol>
                    </div>
                    <div class="upload-warning-footer">
                      <p>Still have questions? Read the full <a href="#"><strong>Fameuz Guidelines.</strong></a> </p>
                      <img class="down-arrow" src="images/down-arrow.png" /> </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="uploading-section" id="uploading-section">
            <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-sp-3">
           <div class="upload-preview" style="background-image:url('images/prev.jpg');">
           </div>
           <div class="upload-status">
           <p><strong>Upload Status</strong></p>
           <p class="process">Processing your video</p>
           </div>
           <div class="upload-link">
           <p>Your video will be live at : </p>
           <a href="#">https://www.Fameuz.com/watch?v=
rMltoD1jCGI&index=27&list=RDrj
BsQ9SygnE</a>
           </div>
            </div>
            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-sp-9">
				<div class="row">
                <div class="col-sm-12">
                <div class="progressbar1">
                <div class="progression1" style="width:20%;">
                </div>
                </div>
                <div class="upload-info">
                <p><span class="vid-name">Home.mp4/</span><span class="vid-size">4.50MB of 4.50MB/</span><span class="vid-time">00:00 Remaining </span></p>
                </div>
                </div>
                </div>
                <div class="row">
                <div class="col-sm-12 vid-info-title">
                <p><strong>Basic Info</strong></p>
                <form class="video-upload">
                	 <div class="form-group upload-help">
                        <label for="exampleInputEmail1">Title</label>
                        <div class="help-text">Videos are more interesting when they have creative titles. We know you can do better than "My Video."</div>
                        <input type="text" class="form-control" id="exampleInputEmail1">
                      </div>
                      <div class="form-group upload-help">
                        <label for="exampleInputPassword1">Description</label>
                         <div class="help-text">Videos are more interesting when they have creative titles. We know you can do better than "My Video."</div>
                        <textarea class="form-control" id="exampleInputPassword1"></textarea>
                      </div>
                      <div class="row">
                      <div class="col-sm-6">
                      <div class="form-group upload-help">
                        <label for="exampleInputEmail1">Title</label>
                        <div class="help-text">Add related tags (separated by commas, please!).</div>
                        <input type="text" class="form-control" id="exampleInputEmail1">
                      </div>
                      </div>
                      <div class="col-sm-6">
                      <div class="form-group upload-help">
                        <label for="exampleInputEmail1">Privacy Settings</label>
                        <div class="help-text">Add related tags (separated by commas, please!).</div>
                        <select class="form-control">
                          <option>Public</option>
                          <option>Only me</option>
                          <option>Friends</option>
                         </select>
                      </div>
                      </div>
                      </div>
                      <button type="submit" class="btn btn-default">Publish</button>
                </form>
                </div>
                </div>
            </div>
            </div>
            </div>
            
          </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3">
          <div class="fixer">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="sidebar">
                  <div class="search_box">
                    <input type="text" placeholder="Search " />
                  </div>
                </div>
              </div>
            </div>
            <div class="video_right">
              <div class="left50">
                <h6>Sponsored <i class="fa fa-bullhorn"></i></h6>
              </div>
              <div class="left50 a_right"><a href="#" class="create">Create Ad</a></div>
              <div class="clr"></div>
              <div class="row">
                <div class="ads">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="ad"> <a href="#"><img src="<?php echo SITE_ROOT?>images/ad1.jpg" alt="ad" /></a>
                      <h4>Men's NIKE Shoes -80%</h4>
                      <h6>xxxxxxxxx.com</h6>
                      <p>See how this unique UAE website can get you 
                        Top Brand Shoes &amp; Clothing at up to 80% off</p>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="ad"> <a href="#"><img src="<?php echo SITE_ROOT?>images/ad1.jpg" alt="ad" /></a>
                      <h4>Men's NIKE Shoes -80%</h4>
                      <h6>xxxxxxxxx.com</h6>
                      <p>See how this unique UAE website can get you 
                        Top Brand Shoes &amp; Clothing at up to 80% off</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
