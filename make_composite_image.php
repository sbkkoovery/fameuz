<?php
@session_start();
include_once("includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/composite_card_img.php");
include(DIR_ROOT."ajax/resize-class.php");
$objCommon				 	   =	new common();
$objCompoCardImg				 =	new composite_card_img();
if($_SESSION['userId']==''){
	//header("location:".SITE_ROOT.'composite-card');
	exit;
}
$compColor					   =	$objCommon->esc($_GET['compColor']);
if($compColor){
	if($compColor == 'color1'){
		$compColorStr			=	'#FFFFFF';
	}else if($compColor == 'color2'){
		$compColorStr			=	'#E9E9EA';
	}else if($compColor == 'color3'){
		$compColorStr			=	'#C7C7C8';
	}else if($compColor == 'color4'){
		$compColorStr			=	'#A4A5A7';
	}else if($compColor == 'color5'){
		$compColorStr			=	'#828285';
	}else if($compColor == 'color6'){
		$compColorStr			=	'#5F6063';
	}else if($compColor == 'color7'){
		$compColorStr			=	'#4E4F53';
	}
}else{
	$compColorStr				=	'#4E4F53';
}
$getUserDetails	  			  =	$objCompoCardImg->getRowSql("SELECT user.*,profileImg.upi_img_url,social.usl_fameuz,ucat.uc_m_id,ucat.uc_c_id,ucat.uc_s_id,cat.c_book_by_me,cat.c_book_me,personal.p_gender FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id  LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 LEFT JOIN personal_details as personal ON user.user_id = personal.user_id WHERE user.status=1 AND user.user_id=".$_SESSION['userId']);
$getCompoImgDetails			  =	$objCompoCardImg->getRow("user_id=".$_SESSION['userId']);
$posImg1						 =	($getCompoImgDetails['cci_pos1'])?SITE_ROOT.'uploads/albums/'.$objCommon->html2text($getCompoImgDetails['cci_pos1']):SITE_ROOT.'images/composite-img.jpg';
$posImg2						 =	($getCompoImgDetails['cci_pos2'])?SITE_ROOT.'uploads/albums/'.$objCommon->html2text($getCompoImgDetails['cci_pos2']):SITE_ROOT.'images/compo-img1.jpg';
$posImg3						 =	($getCompoImgDetails['cci_pos3'])?SITE_ROOT.'uploads/albums/'.$objCommon->html2text($getCompoImgDetails['cci_pos3']):SITE_ROOT.'images/compo-img-6.jpg';
$posImg4						 =	($getCompoImgDetails['cci_pos4'])?SITE_ROOT.'uploads/albums/'.$objCommon->html2text($getCompoImgDetails['cci_pos4']):SITE_ROOT.'images/compo-img-3.jpg';
$getDataTable								=	$objCompoCardImg->getRowSql("SELECT ccd_data_table FROM composite_card_data WHERE user_id=".$_SESSION['userId']);
$getDataTableArr						 	 =	explode(",",$getDataTable['ccd_data_table']);
$getDataTableArr						 	 =	array_filter($getDataTableArr);
if(count($getDataTableArr)>0){
	$sqlCompModelDetails					 =	"";
	$selectSql							   =	"";
	$leftSql								 =	"";
	if(in_array('model_chest',$getDataTableArr)){
		$selectSql						  .=	",chest.mc_name";
		$leftSql							.=	" LEFT JOIN model_chest AS chest ON det.mc_id = chest.mc_id ";
	}
	if(in_array('model_collar',$getDataTableArr)){
		$selectSql						  .=	",collar.mcollar_name";
		$leftSql							.=	" LEFT JOIN model_collar AS collar ON det.mcollar_id = collar.mcollar_id ";
	}
	if(in_array('model_dress',$getDataTableArr)){
		$selectSql						  .=	",dress.mdress_name";
		$leftSql							.=	" LEFT JOIN model_dress AS dress ON det.mdress_id = dress.mdress_id ";
	}
	if(in_array('model_eyes',$getDataTableArr)){
		$selectSql						  .=	",eyes.me_name";
		$leftSql							.=	" LEFT JOIN model_eyes AS eyes ON det.me_id = eyes.me_id ";
	}
	if(in_array('model_hair',$getDataTableArr)){
		$selectSql						  .=	",hair.mhair_name";
		$leftSql							.=	" LEFT JOIN model_hair AS hair ON det.mhair_id = hair.mhair_id ";
	}
	if(in_array('model_height',$getDataTableArr)){
		$selectSql						  .=	",height.mh_name";
		$leftSql							.=	" LEFT JOIN model_height AS height ON det.mh_id = height.mh_id ";
	}
	if(in_array('model_hips',$getDataTableArr)){
		$selectSql						  .=	",hips.mhp_name";
		$leftSql							.=	" LEFT JOIN model_hips AS hips ON det.mhp_id = hips.mhp_id ";
	}
	if(in_array('model_jacket',$getDataTableArr)){
		$selectSql						  .=	",jacket.mj_name";
		$leftSql							.=	" LEFT JOIN model_jacket AS jacket ON det.mj_id = jacket.mj_id ";
	}
	if(in_array('model_shoes',$getDataTableArr)){
		$selectSql						  .=	",shoes.ms_name";
		$leftSql							.=	" LEFT JOIN model_shoes AS shoes ON det.ms_id = shoes.ms_id ";
	}
	if(in_array('model_trousers',$getDataTableArr)){
		$selectSql						  .=	",trousers.mt_name";
		$leftSql							.=	" LEFT JOIN model_trousers AS trousers ON det.mt_id = trousers.mt_id ";
	}
	if(in_array('model_waist',$getDataTableArr)){
		$selectSql						  .=	",waist.mw_name";
		$leftSql							.=	" LEFT JOIN model_waist AS waist ON det.mw_id = waist.mw_id ";
	}
	$datSql							  	 .=	"SELECT det.model_id ".$selectSql." FROM model_details AS det ".$leftSql." WHERE det.user_id=".$_SESSION['userId'];
}else{
	$datSql								  =	"SELECT 														det.model_dis_id,chest.mc_name,collar.mcollar_name,disc.model_dis_name,dress.mdress_name,eyes.me_name,hair.mhair_name,height.mh_name,hips.mhp_name,jacket.mj_name,shoes.ms_name,trousers.mt_name,waist.mw_name
																FROM model_details AS det
																LEFT JOIN model_chest AS chest ON det.mc_id=chest.mc_id
																LEFT JOIN model_collar AS collar ON det.mcollar_id=collar.mcollar_id
																LEFT JOIN model_dress AS dress ON det.mdress_id=dress.mdress_id
																LEFT JOIN model_disciplines AS disc ON det.model_dis_id=disc.model_dis_id
																LEFT JOIN model_eyes AS eyes ON det.me_id=eyes.me_id
																LEFT JOIN model_hair AS hair ON det.mhair_id=hair.mhair_id
																LEFT JOIN model_height AS height ON det.mh_id=height.mh_id
																LEFT JOIN model_hips AS hips ON det.mhp_id=hips.mhp_id
																LEFT JOIN model_jacket AS jacket ON det.mj_id=jacket.mj_id
																LEFT JOIN model_shoes AS shoes ON det.ms_id=shoes.ms_id
																LEFT JOIN model_trousers AS trousers ON det.mt_id=trousers.mt_id
																LEFT JOIN model_waist AS waist ON det.mw_id=waist.mw_id
																WHERE det.user_id=".$_SESSION['userId'];
}
$getDataSql							  	  =	$objCompoCardImg->getRowSql($datSql);

$html='

<link href="http://fonts.googleapis.com/css?family=Roboto:500,900,100,300,700,400" rel="stylesheet" type="text/css">
<style>
	body{
		font-family: "Arial", sans-serif;
	}
	.main-container{
		width:976px;
		height:690px;
		overflow:hidden;
		margin:0px auto;
		background:#f4f4f4;
		font-family: "Arial", sans-serif;
	}
	.left-section, .right-section{
		width:450.5px;
		height:650px;
		overflow:hidden;
		float:left;
		padding:15px;
		border-top:5px solid '.$compColorStr.';
		border-bottom:5px solid '.$compColorStr.';
	}
	.left-section{
		border-left:5px solid '.$compColorStr.';
		border-right:2.5px solid '.$compColorStr.';
	}
	.right-section{
		background:#fff;
		border-right:5px solid '.$compColorStr.';
		border-left:2.5px solid '.$compColorStr.';
		margin-left:-0.5px;
	}
	.image-sections{
		width:450.5px;
		height:650px;
		overflow:hidden;
		position:relative;
	}
	.image-sections img{
		width:450.5px;
		height:650px;
	}
	.top-right-section, .right-bottom-section{
		width:450px;
		height:300px;
	}
	.top-right-section{
		padding-bottom:7.5px
	}
	.right-bottom-section{
		padding-top:7.5px
	}
	.top-left, .top-right{
		width:215px;
		height:317.5px;
		float:left;
		overflow:hidden;
	}
	.top-left{
		padding-right:7.5px;
	}
	.top-right{
		padding-left:7.5px;
	}
	.top-left img, .top-right img{
		width:215px;
		height:317.5px;
	}
	.log-section{
		width:102px;
		margin:0 auto;
		height:45px;
	}
	
	.info-user-section ul li{
		list-style:none;
		font-size:12px;
		margin-bottom:5px;
	}
	.info-user-section ul li span{
		font-weight:bold;
	}
	
	
	.clearfix{
	  float:none;
	  clear:both;
	 }
</style>
<body>
	<div class="main-container">
    	<div class="left-section" style="width:450.5px; height:650px;">
        	<div class="image-sections">
            	<img src="'.$posImg1.'" style="width:450px; height:650px;"  />



				<p class="user-info" style="margin-top:-50px; text-align:center; color:#fff;"></p>
    			<span><img src="'.SITE_ROOT.'images/shadow-compo.png" style="width:450.5px; height:150px; margin-top:-119px;" />
                <p class="user-info" style="margin-top:-50px; text-align:center; color:#fff;">'.$objCommon->displayName($getUserDetails).'</p>
             </div>
        </div>
        <div class="right-section" style="width:450.5px; height:650px;">
        	<div class="top-right-section">
            	<div class="top-left" style="width:217.5px; height:317.5px; ">
                	<img src="'.$posImg2.'" style="width:217.5px; height:317.5px;" />
                </div>
                <div class="top-right" style="width:217.5px; height:317.5px;">
                	<img src="'.$posImg3.'" style="width:217.5px; height:317.5px;" />
                </div>
				<div class="clearfix"></div>
            </div>
            <div class="right-bottom-section">
            	<div class="top-left" style="width:217.5px; height:317.5px">
                	<div class="info-user-section">
                    	<div class="log-section">
                        	<img src="'.SITE_ROOT.'images/log-compo.png" style="width:102px; height:45px;" />
                        </div>
                        <div class="info-user-section">
                        	<ul>';
if($getDataSql['mh_name']){
	$html .= '<li>Height : <b>'.$objCommon->html2text($getDataSql['mh_name']).'</b></li>';
}
if($getDataSql['mc_name']){
	$chestCheck	=	($getUserDetails['p_gender']==2)?'Bust':'Chest';
	$html .= '<li>'.$chestCheck.' : <b>'.$objCommon->html2text($getDataSql['mc_name']).'</b></li>';
}
if($getDataSql['me_name']){
	$html .= '<li>Eyes : <b>'.$objCommon->html2text($getDataSql['me_name']).'</b></li>';
}
if($getDataSql['mhair_name']){
	$html .= '<li>Hair : <b>'.$objCommon->html2text($getDataSql['mhair_name']).'</b></li>';
}
if($getDataSql['mw_name']){
	$html .= '<li>Waist : <b>'.$objCommon->html2text($getDataSql['mw_name']).'</b></li>';
}
if($getDataSql['mhp_name']){
	$html .= '<li>Hips : <b>'.$objCommon->html2text($getDataSql['mhp_name']).'</b></li>';
}
if($getDataSql['mcollar_name']){
	$html .= '<li>Collar : <b>'.$objCommon->html2text($getDataSql['mcollar_name']).'</b></li>';
}
if($getDataSql['mdress_name']){
	$html .= '<li>Dress : <b>'.$objCommon->html2text($getDataSql['mdress_name']).'</b></li>';
}
if($getDataSql['ms_name']){
	$html .= '<li>Shoes : <b>'.$objCommon->html2text($getDataSql['ms_name']).'</b></li>';
}
if($getDataSql['mj_name']){
	$html .= '<li>Jacket : <b>'.$objCommon->html2text($getDataSql['mj_name']).'</b></li>';
}
if($getDataSql['mt_name']){
	$html .= '<li>Trousers : <b>'.$objCommon->html2text($getDataSql['mt_name']).'</b></li>';
}
                            $html .='</ul>
                        </div>
                    </div>
                </div>
                <div class="top-right" style="width:217.5px; height:317.5px;">
                	<img src="'.$posImg4.'" style="width:217.5px; height:317.5px;" />
                </div>
				<div class="clearfix"></div>
            </div>
        </div>
		<div class="clearfix"></div>
    </div></body>';
//echo $html;

	$compocard	=	DIR_ROOT."uploads/composite_card/".$_SESSION['userId'].'.pdf';
	include(DIR_ROOT."phptoPDF/mpdf.php");
	$mpdf=new mPDF('utf-8', 'A4-L',0,0,0,0,11);
	$mpdf->WriteHTML($html);
	$mpdf->Output($compocard,'F');
	$im = new Imagick();
	$im->setResolution(1000,800);
	$im->readimage('uploads/composite_card/'.$_SESSION['userId'].'.pdf'); 
	//$im->cropThumbnailImage( 976, 690 );
	//$im->scaleImage(800,0);
	$im->setImageFormat('jpg');    
	$im->writeImage('uploads/composite_card/'.$_SESSION['userId'].'.jpg'); 
	$im->clear(); 
	$im->destroy();
	$basename = DIR_ROOT.'uploads/composite_card/'.$_SESSION['userId'].'.jpg';
	$resizeObj = new resize($basename);
	$resizeObj -> resizeImage('1000','', 'auto');
	$resizeObj -> saveImage($basename, 100);
	$filename = $basename; // don't accept other directories
	
	$mime = ($mime = getimagesize($filename)) ? $mime['mime'] : $mime;
	$size = filesize($filename);
	$fp   = fopen($filename, "rb");
	if (!($mime && $size && $fp)) {
	// Error.
	return;
	}
	
	header("Content-type: " . $mime);
	header("Content-Length: " . $size);
	// NOTE: Possible header injection via $basename
	header("Content-Disposition: attachment; filename=" . $objCommon->displayName($getUserDetails).'_'.$_SESSION['userId'].'.jpg');
	header('Content-Transfer-Encoding: binary');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	fpassthru($fp);
	exit;
?>