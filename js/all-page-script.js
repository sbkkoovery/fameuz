$(document).ready(function(e) {
            $('#mob_btn').click(function(e) {
                $('.top_section .main_nav .main_manu').slideToggle();
				return false;
            });
            $('#mob_btn1').click(function(e) {
                $('#mob_menu1').slideToggle();
				return false;
            });
           /*------------------*/
			var owl = $("#featured_members_slider");
			owl.owlCarousel({
				loop:true,
			    autoplay:true,
				autoplayTimeout:1000,
				autoplayHoverPause:true,
				items : 3, //10 items above 1000px browser width
				itemsDesktop : [1000,3], //5 items between 1000px and 901px
				itemsDesktopSmall : [900,3], // betweem 900px and 601px
				itemsTablet: [600,2], //2 items between 600 and 0
				itemsMobile : [400,1] // itemsMobile disabled - inherit from itemsTablet option
			});
			owl.trigger('owl.play',3000);
			//////////////////////////////
			var owl2 = $("#client_slider");
			owl2.owlCarousel({
				loop:true,
			    autoplay:true,
				autoplayTimeout:1000,
				autoplayHoverPause:true,
				responsiveClass:true,
				responsive:{
					0:{
						items:2,
						
					},
					600:{
						items:8,
						
					},
					1000:{
						items:18	,
						
					}
				}
			});
			owl2.trigger('owl.play',3000);
			 
			/*------------------*/
			$(".mob_menu_btn,#close").click(function(e){
				e.preventDefault();
				$(".mob_menu").toggleClass("show_menu");
			});
			$(".mob_menu ul li a").click(function(event){
				if($(this).next('ul').length){
					$(this).next().toggle('fast');
				}
			});

		});