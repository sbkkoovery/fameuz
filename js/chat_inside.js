// JavaScript Document
	var cachedObjs	=	{
		checkBox	:	$('.chk_delt input[type="checkbox"]'),
	}
$(document).ready(function(e) {
	$('#more_optn').click(function(){
		$('.drop_down_settings').toggle();
	});
	$('.del_conv').click(function(){
		$('.chk_delt').removeClass('block_inline');
		deleteMsg('all');
	});
	$('.del_msg').click(function(){
		$('.drop_down_settings').hide();
		$('.options_onCheck').addClass('block_inline');
		$('.chk_delt').addClass('block_inline');
	});
	$('#cancelThis').click(function(){
		$('.options_onCheck,.chk_delt').removeClass('block_inline');
	})
	$('#deleteThis').click(function(){
		detectCheckbox();
	});
});
function detectCheckbox(){
	if(cachedObjs.checkBox.prop('checked') == true){
		deleteMsg('one');
	}else{
		notifyWarning();
	}
}
function notifyWarning(){
	Lobibox.notify('warning', {
		msg: 'Please select any message',
		icon: false,
		 delay: 1500,
	});
}
function deleteMsg(optn){
	Lobibox.confirm({
		msg: "Are you sure that you want to permanently delete selected messages ?",
		title: "Delete Confirmation",
		buttonsAlign: 'right',
		closeButton: false,
		callback: function ($this, type, ev) {
			if (type === 'yes') {
				if(optn == 'all'){
					$('.inbox_msges').remove();
					window.location = 'http://localhost:8888/fameuz/user/my-messages';
				}else if(optn == 'one'){
					$('.checkEd:checked').parents('.delM').remove();
				}
					Lobibox.notify('success', {
						msg: 'Conversation deleted',
						icon: false,
						 delay: 1500,
					});
			} else if (type === 'no') {
			}
		}
	});
}