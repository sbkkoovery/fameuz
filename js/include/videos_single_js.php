<script type="text/javascript" >
$(document).ready(function(e) {
  $("#video_slider").owlCarousel({
	lazyLoad : true,
	navigation : true
  }); 
   $("#video_slider1").owlCarousel({
	lazyLoad : true,
	navigation : true
  }); 
   $("#video_slider2").owlCarousel({
	lazyLoad : true,
	navigation : true
  }); 
   $("#video_slider3").owlCarousel({
	lazyLoad : true,
	navigation : true
  }); 
 $('.rate-btn').hover(function(){
		$('.rate-btn').removeClass('rate-btn-hover');
		var therate = $(this).attr('id');
		for (var i = therate; i >= 0; i--) {
			$('.rate-btn-'+i).addClass('rate-btn-hover');
		};
	});
 $('.rate-btn').click(function(){
		var therate = $(this).attr('id');
		$("#rate_val").val(therate);
 });
  $('.rate-btn').mouseout(function(){
		var therate = $("#rate_val").val();
		therate++;
		for (var i = therate; i <= 5; i++) {
			$('.rate-btn-'+i).removeClass('rate-btn-hover');
		};
 });
 $('.comment_descr_input1').elastic();
	$('.comment_descr_input1').keydown(function(event){				
		  if (event.keyCode == 13 && !event.shiftKey) {
			$(this).closest("form").find('.ajaxloader').show();
			$(this).parents("form").ajaxForm(
				{
					target: '#preview',
					success:successCommentCall1
				}).submit();
		  }
	});
	$(".loadCommentBox").load('<?php echo SITE_ROOT?>widget/load_video_comment.php?videoEncrId=<?php echo $vidId ?>');
	$(".loadRelatedVideos").load("<?php echo SITE_ROOT?>widget/load_related_video.php?videoEncrId=<?php echo $vidId?>");
});
$(".likeVideoBtn").on("click",".likeVideo",function(){
	var likeId		=	$(this).data('like');
	var likecat	   =	4;
	var imgUserId	 =	'<?php echo $getVideoDetails['user_id']?>';
	var that		  =	this;
	if(likeId !='' && likecat != ''){
		$.ajax({
			url:"<?php echo SITE_ROOT?>ajax/like_video.php",
			method: "POST",
			data:{likeId:likeId,likecat:likecat,imgUserId:imgUserId},
			success:function(result){
				if(result == 'liked'){
					$(that).addClass('liked');
				}else if(result == 'like'){
					$(that).removeClass('liked');
				}
				//$(that).parent().html(result);
				$(".likeStrCount").load('<?php echo SITE_ROOT?>ajax/like_count.php?type='+likecat+'&id='+likeId);
		}});
	}
});
$(".shareVideoBtn").on("click",".shareVideo",function(){
	var shareId		=	$(this).data('share');
	var shareCat	   =	3;
	var shareWhoseId   =	'<?php echo $getVideoDetails['user_id']?>';
	var that		   =	this;
	if(shareId !='' && shareCat != '' && shareWhoseId != ''){
		$.ajax({
			url:"<?php echo SITE_ROOT?>ajax/share_video.php",
			method: "POST",
			data:{shareId:shareId,shareCat:shareCat,shareWhoseId:shareWhoseId},
			success:function(result){
				if(result == 'shared'){
					$(that).addClass('shared').html('<i class="fa fa-share-alt"></i>Shared');
				}else if(result == 'share'){
					$(that).removeClass('shared').html('<i class="fa fa-share-alt"></i> Share');
				}
				//$(that).parent().html(result);
				$(".shareStrCount").load('<?php echo SITE_ROOT?>ajax/share_count.php?type='+shareCat+'&id='+shareId);
		}});
	}
});
$(".video_rate_table").on("click",".rate_submit",function(){
	var inst = $.remodal.lookup[$('[data-remodal-id=rate-video]').data('remodal')];
	var rate_val				=	$("#rate_val").val();
	var vidId				    =	'<?php echo $vidId?>';
	var that		  			=	this;
	if(rate_val){
		$.ajax({
			url:"<?php echo SITE_ROOT?>ajax/rate_video.php",
			method:"POST",
			data:{rate_val:rate_val,vidId:vidId},
			success:function(result){
				$("#rateVideo").addClass("voted");
				$("#rateVideo").html('<i class="fa fa-thumbs-up"></i>Voted');
				$("#rateVideo").attr("href",'javascript:;');
				$(".rateStrCount").html(result);
				inst.close();
		}});
	}
});
function successCommentCall1(){
	$('.ajaxloader').hide();
	$(".comment_descr_input1").val('');
	$(".comment_descr_input2").val('');
	$(".loadCommentBox").load('<?php echo SITE_ROOT?>widget/load_video_comment.php?videoEncrId=<?php echo $vidId ?>');
}
</script>