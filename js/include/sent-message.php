<script>
// JavaScript Document

var MessageContainer	=	$('.message-container');
$(document).ready(function(e) {
	doShowMsgList(1);
});

$('.settings_msg').on('click', '.refreshMessageList', function(){
	doShowMsgList(1);	
});
$('.search_bar_msg').on('click', '.searchMsgBtn', function(){
	doShowMsgList(1);
});
$('input[name="searchMsg"]').on('keypress', function(e) {
    var code = e.keyCode || e.which;
    if(code==13){
        doShowMsgList(1);
    }
});
function doShowMsgList(page){
	var searchMsg			=	$("#searchMsg").val();
	$("#loadEmail").html('<div class="new_msg_unread"><p class="emailLoading text-center">Loading.....</p></div>');
	$("#loadEmail").load('<?php echo SITE_ROOT?>widget/email_sent_message.php?page='+page+'&search='+searchMsg,function(){
		init();
	});
}
function init(){
	var messageCount = $('.inbox_msges').find('input[type="checkbox"]');	
	var cachedObj	=	{
		MessageContainer	: $('.message-container'),
		inboxContainer	  : $('.inbox_msges'),
		checkBoxs		   : $('.check'),
		countMsg			: $('.count_msg'),
		settingsBox		 : $('.options_onCheck'),
		selectAll		   : $('.checkAll input[type="checkbox"]'),
		messagesBar		 : $('.new_msg_unread'),
		undo				: $('.undo'),
		delete			  : $('#deleteThis'),
		newMsg			  : $('#new_msg'),
		newMesgContain	  : $('.new_msg'),
	}
	triggerEvents(cachedObj,messageCount);
}
function triggerEvents(cachedObj,messageCount){
	cachedObj.selectAll.change(function(){
			changeProp(cachedObj.selectAll, messageCount, cachedObj.messagesBar);
			applyChanges(cachedObj);
		});
		cachedObj.undo.click(function(){
			cachedObj.settingsBox.hide();
			cachedObj.messagesBar.removeClass('selected_msg');
			messageCount.prop('checked', false);
			cachedObj.selectAll.prop('checked', false);
		});
		cachedObj.delete.click(function(){
			trash(messageCount,cachedObj);
		});
		cachedObj.checkBoxs.change(function(){
			var _this	=	$(this);
			var mainElem	=	$(this).parents('.new_msg_unread');
			changeProp(_this,_this,mainElem);
			applyChanges(cachedObj);
		});
		cachedObj.newMsg.click(function(){
			cachedObj.inboxContainer.add(cachedObj.newMesgContain).toggle();
		});
		cachedObj.checkBoxs.click(function(){
			var _this	=	$(this);
			var mainElem	=	$(this).parents('.new_msg_unread');
			changeProp(_this,_this,mainElem);
			applyChanges(cachedObj);
		});
		$('#file_name').change(function(){
			var _this	=	$(this);
			console.log(_this.prop('files'));
			var getName = getFileName(_this);
			buildPreviwFrag(getName);
		});
	
		$('.message_attachments').on('click', '.removeFile', function(){
			var _this	=	$(this);
			removeFiles(_this);
		});
}
function applyChanges(cachedObj){
	var count	=	getSlectedCount(cachedObj);
	var fetchedCheckbox	=	cachedObj.inboxContainer.find('input[type="checkbox"]');
	var allMessageCount	=	parseInt(fetchedCheckbox.length);
	(count == 0)?cachedObj.settingsBox.hide():cachedObj.settingsBox.show();
	if(count == allMessageCount){
		cachedObj.selectAll.prop('checked', true);
	}else if(count < allMessageCount){
		cachedObj.selectAll.prop('checked', false);
	}
	cachedObj.countMsg.html(count);
}
function changeProp(elem, messageCount, messageBar){
	if(elem.prop('checked') == true){
		messageCount.prop('checked', true);
		messageBar.addClass('selected_msg');
	}else if(elem.prop('checked') == false){
		messageCount.prop('checked', false);
		messageBar.removeClass('selected_msg');
	}
}
function getSlectedCount(cachedObj){
	var allCheckbox	=	cachedObj.inboxContainer.find('input[type="checkbox"]:checked');
	return allCheckbox.length;
}
function trash(elem,cachedObj){
	var checkedValues = $('input[name="msgId"]:checked').map(function() {
		return this.value;
	}).get();
	Lobibox.confirm({
		msg: "Are you sure that you want to permanently delete selected messages ?",
		title: "Delete Confirmation",
		buttonsAlign: 'right',
		closeButton: false,
		callback: function ($this, type, ev) {
			if (type === 'yes') {
					$.ajax({
						url:"<?php echo SITE_ROOT?>access/delete_email_message.php",
						type:"POST",
						data:{checkedValues:checkedValues},
						success: function(data){
							$('.check:checked').parents('.new_msg_unread').remove();
							cachedObj.settingsBox.hide();
							cachedObj.selectAll.prop('checked', false);
							Lobibox.notify('success', {
								msg: 'Comment has been removed.',
								icon: false,
								 delay: 1500,
							});
						}
					});
			} else if (type === 'no') {
			}
		}
	});
}
function getFileName(elem){
	var files	=	elem.prop('files');
	var names	=	$.map(files, function(val){
		return val.name;
	});
	return names;
}
function buildPreviwFrag(files){
	$('.attachments_field').prepend('<div class="cont_files"></div>');
	for(var i=0; i<files.length; i++){
		$('.cont_files').append('<div class="prevNames"><div class="namesFile">'+files[i]+'</div><i class="fa fa-close removeFile"></i></div>');
	}
}
function removeFiles(file){
	var selectedFile	=	file.prev('.namesFile').html();
	var fileSelect	=	$('#file_name');
	var fetchedFiles = getFileName(fileSelect);
	var realArray	=	fileSelect.prop('files');
	if($.inArray(selectedFile,fetchedFiles) !== -1){
	var result	=	$.grep(realArray, function(e,i){
			return e.name	!= selectedFile;
		});
	result.slice($.inArray(result,realArray),1);
	}else{
	}
}
</script>