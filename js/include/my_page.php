<script type="text/javascript" >
$('.status_box').on("click",'#change_status',function(e) {
	$('#status_option').fadeToggle();
	return false;
});
$('#status_option').on("click",'.check_Status',function(e) {
	var dataVal	=	$(this).attr('data-value');
	if(dataVal){
		$.get('<?php echo SITE_ROOT?>ajax/change_online_chat_status.php',{"dataVal":dataVal},function(data1){
			$(".status_box").html(data1);
		});
	}
});
$(".tab-menu img").click(function(){
	$(".ipad_menu, .my_profile_ipad").toggleClass("moveitup");
	$(".top_section, .inner_content_section, .footer_section").toggleClass("moveit");
});
$('.dropMe_down').click(function(){
			$('.drop_down_delete').hide();
			$(this).children('.drop_down_delete').toggle();
});
$("body").mouseup(function(e){
        var subject = $(".dropMe_down"); 
		if(e.target.id != subject.attr('id') && !subject.has(e.target).length) {
             $(".drop_down_delete").fadeOut();
        }
});
$("body").on("click", ".makeCover_btn",function(){
	var imgId			=	$(this).data("coverid");
		imgCat		  	=	$(this).data("imgcat");
		that			=	this;
	if(imgId != '' && imgCat != ''){
		$.ajax({
			url:'<?php echo SITE_ROOT?>ajax/set_cover_image.php',
			data:{imgId:imgId,imgCat:imgCat},
			type:"POST",
			success: function(data){
				alert(data);
				$(that).html('<i class="fa fa-check"></i> Cover Photo ');
			}
		});
	}
});
</script>