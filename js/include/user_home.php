<script type="text/javascript">
$(window).load(function(e) {
	$('.fixer').fixedSidebar();
	$('.left_fixedSidebar').fixedSidebar();
});
$(document).ready(function(e) {
	$('body').fameuzLightbox({
		class : 'lightBoxs',
		sidebar: 'default',
		photos:{
			imageLoadUrl: '<?php echo SITE_ROOT.'ajax/popUpImageUser.php'?>',
			data_attr	: ['data-contentId','data-contentType','data-contentAlbum'],
			likeUrl	: '<?php echo SITE_ROOT?>ajax/like.php',
			shareUrl: '<?php echo SITE_ROOT?>ajax/share.php',
			workedUrl: '<?php echo SITE_ROOT?>ajax/worked_together.php',
			commentBoxUrl: '<?php echo SITE_ROOT.'widget/pop_image_comment_box.php' ?>',
			comments :{
				commentActionUrl:'<?php echo SITE_ROOT?>access/post_comment.php?action=add_comment',
			}
		},
		videos : {
			videoLoadUrl: '<?php echo SITE_ROOT.'ajax/popUpVideoUser.php'?>',
			data_attr	: ['data-vidEncrId','data-posttype'],
			likeUrl		: '<?php echo SITE_ROOT?>ajax/like_video.php',
			shareUrl	: '<?php echo SITE_ROOT?>ajax/share_video.php',
			commentBoxUrl:'<?php echo SITE_ROOT.'widget/pop_video_comment_box.php' ?>',
			comments :{
				commentActionUrl:'<?php echo SITE_ROOT?>access/post_video_comment.php?action=add_comment',
			}
		},
		skin: {
			next	: '<img src="<?php echo SITE_ROOT ?>fameuz_lightbox/images/next.png">',
			prev	: '<img src="<?php echo SITE_ROOT ?>fameuz_lightbox/images/prev.png">',
			reset	: '<i class="fa fa-refresh"></i>',
			close	: '<img src="<?php echo SITE_ROOT ?>fameuz_lightbox/images/close.png" width="15">',
			loader	: '<?php echo SITE_ROOT ?>images/ajax-loader.gif',
			review	: '<i class="fa fa-chevron-right"></i>',
			video	: '<?php echo SITE_ROOT ?>jw_player/six/six.xml',
		}
	});
	
});
 var preloader		=	'<div class="feedLoader"><div class="feedContainer"><div class="row"><div class="col-sm-1"><div class="img-s"></div></div><div class="col-sm-11"><div class="rectangle-md"><div class="rectangle"></div><div class="rectangle-sm pull-right"></div></div><div class="rectangle-lg"><div class="rectangle-sm"></div><div class="rectangle-sm"></div><div class="rectangle-sm"></div></div><div class="rectangle-lg"><div class="rectangle-full"></div></div></div></div></div><div class="line"></div><div class="rectangle-wd"><div class="rectangle-sm-down"></div><div class="rectangle-sm-down"></div><div class="rectangle-sm-down"></div></div></div>';
function getresult(url) {
	$('#newsfeed').append(preloader);
	$.ajax({
		url: url,
		type: "GET",
		data:  {rowcount:$("#rowcount").val()},
		success: function(data){
			 $('.feedLoader').hide(); 
			 $('#newsfeed').append(data);  
			 if($("#total-count").val()>0){
			  }else{
				$('#newsfeed').load('<?php echo SITE_ROOT ?>ajax/newUser_home.php');
			  }
		},
		error: function(){} 	        
   });
}
$(window).scroll(function(){
	if ($(window).scrollTop() == $(document).height() - $(window).height()){
		if(parseInt($(".pagenum:last").val()) <= parseInt($(".total-page").val())) {
			var pagenum = parseInt($(".pagenum:last").val()) + 1;
			alert(pagenum);
			getresult('<?php echo SITE_ROOT?>widget/news_feed_home.php?page='+pagenum);
		}
	}
}); 
$('body').on("click",".review_message,.comment",function(){
	var slidee = $(this).parents(".home_update_margin").children(".comment-sec");
	var commentContent	=	$(this).data("comment");
	var commentCat		=	$(this).data("category");
	var noti_whome		=	$(this).data("for");
	$(".comment-sec").not(slidee).hide();
	$(".comment-sec textarea").not(slidee).val('');
	$(".home_update").toggleClass("no-border-sl");
	slidee.toggle();
	$(".loadComment_"+commentContent+"_"+commentCat).load('<?php echo SITE_ROOT?>widget/load_home_comment.php?commentContent='+commentContent+'&commentCat='+commentCat+'&noti_whome='+noti_whome);
});
function deletePost(a){
	$.confirm({
		'title'		: 'Delete Confirmation',
		'message'	: 'You are about to delete this post ?',
		'buttons'	: {
		'Yes'	: {
		'class'	: 'blue',
		'action': function(){
			var catId	=	$(a).parent('ul').parent('a').data("category");
			var dataId	=	$(a).parent('ul').parent('a').data("id");
			$.ajax({
				url:'<?php echo SITE_ROOT?>ajax/delete_timeline.php',
				data:{dataID:dataId,dataCat:catId},
				type:"GET",
				success: function(result){
					if(result=='success'){
						$(a).parent().parent().parent('div').parent('div').remove();
					}
				}
			});
		}
		},
		'No'	: {
		'class'	: 'gray',
		'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
		}
		}
	});
}
$("body").on("click",".share_btn",function(){
	var shareId			   =	$(this).data("share");
	var shareCat			  =	$(this).data("category");
	var noti_whome			=	$(this).data("userby");
	var that				  =	$(this);
	if(shareId != '' && shareCat != ''){
		$.ajax({
			url:'<?php echo SITE_ROOT?>ajax/share_home.php',
			data:{shareId:shareId,shareCat:shareCat,noti_whome:noti_whome},
			type:"POST",
			success: function(data4){
				$('.shared').modal('show');
				setTimeout(function(){
					$('.shared').modal('hide');
				},4000)
				$(that).parent().html(data4);
				return false;
			}
		})
	}
 });
 $("body").on("click",".like_btn",function(){
	var likeId				=	$(this).data("like");
	var likeCat			   =	$(this).data("category");
	var noti_whome			=	$(this).data("userby");
	var that				  =	$(this);
	if(likeId != '' && likeCat != ''){
		$.ajax({
			url:'<?php echo SITE_ROOT?>ajax/like_home.php',
			data:{likeId:likeId,likeCat:likeCat,noti_whome:noti_whome},
			type:"POST",
			success: function(data2){
				$(that).parent().html(data2);
				$.get('<?php echo SITE_ROOT?>ajax/like_count_home.php',{likeId:likeId,likeCat:likeCat},function(data3){
					$(".likeCountDisplay_"+likeId+"_"+likeCat).html(data3);
				});
				return false;
			}
		})
	}
 });
$('body').on("keydown",".comment_descr_input1",function(event){			
	  if (event.keyCode == 13 && !event.shiftKey) {
		$(this).closest("form").find('.ajaxloader').show();
		var commentContent		   =	$(this).closest("form").data("comment");
		var commentCat			   =	$(this).closest("form").data("category");
		var noti_whome			   =	$(this).closest("form").data("for");
		$(this).closest("form").ajaxForm(
			{
				success:function(){
					$(".loadComment_"+commentContent+"_"+commentCat).load('<?php echo SITE_ROOT?>widget/load_home_comment.php?commentContent='+commentContent+'&commentCat='+commentCat+'&noti_whome='+noti_whome,function(){
						$('.ajaxloader').hide();
						$(".comment_descr_input1").val('');
						$(".comment_descr_input2").val('');
						$(".commentCountDisplay_"+commentContent+"_"+commentCat).load('<?php echo SITE_ROOT?>ajax/comment_count_home.php?commentContent='+commentContent+'&commentCat='+commentCat);
						
					});
				}
			}).submit();
		return false;
	  }
});
</script>