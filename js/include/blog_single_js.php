<script type="text/javascript" >
$(window).load(function(){
	$('.fixer').fixedSidebar();
});
$(document).ready(function(e) {
 $('.comment_descr_input1').elastic();
 $('[data-toggle="tooltip"]').tooltip();	
	$('.comment_descr_input1').keydown(function(event){				
		  if (event.keyCode == 13 && !event.shiftKey) {
			$(this).closest("form").find('.ajaxloader').show();
			$(this).parents("form").ajaxForm(
				{
					target: '#preview',
					success:successCommentCall1
				}).submit();
		  }
	});
	$(".loadCommentBox").load('<?php echo SITE_ROOT?>widget/load_blog_comment.php?blogId=<?php echo $blogId ?>');
});
function successCommentCall1(){
	$('.ajaxloader').hide();
	$(".comment_descr_input1").val('');
	$(".comment_descr_input2").val('');
	$(".loadCommentBox").load('<?php echo SITE_ROOT?>widget/load_blog_comment.php?blogId=<?php echo $blogId ?>');
}
$(".like-blog").on("click",".shareBlog",function(){
	var share		=	$(this).data("share");
		blogby	   =	$(this).data("blogby");
		that		 =	this;
	if(share){
		$.ajax({
			url:"<?php echo SITE_ROOT?>ajax/share_blog.php",
			data:{share:share,blogby:blogby},
			type:"POST",
			success: function(dat1){
				$(that).parent().html(dat1);
			}
		});
	}
});
$(".like-blog").on("click",".like",function(){
	var like		=	$(this).data("like");
	var that		=	this;
	if(like){
		$.ajax({
			url:"<?php echo SITE_ROOT?>ajax/like_blog.php",
			data:{like:like},
			type:"POST",
			success: function(dat1){
				$(that).parent().html(dat1);
				$(".likecount"+like).load('<?php echo SITE_ROOT?>ajax/like_blog_count.php?like='+like);
			}
		});
	}
});
</script>