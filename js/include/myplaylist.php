<?php
$getMyPlayList				=	$objMusic->listQuery("SELECT music.music_id,music.music_title,music.music_descr,music.music_artist,music.music_thumb,music.music_url ,music.music_created,play_list.myp_id ,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name,review.reviewAvg,ml.ml_status,mv.mv_id
																				FROM music 
																				LEFT JOIN my_playlist AS play_list ON music.music_id = play_list.music_id
																				lEFT JOIN users AS user ON music.user_id = user.user_id
																				LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
																				LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
																				LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id 
																				LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id  
																				LEFT JOIN (SELECT AVG(review_rate) AS reviewAvg,user_id_to FROM user_reviews GROUP BY user_id_to ) AS review ON user.user_id = review.user_id_to
																				LEFT JOIN music_like AS ml ON music.music_id = ml.music_id AND ml.user_id = ".$_SESSION['userId']."
																				LEFT JOIN music_vote AS mv ON music.music_id = mv.music_id AND mv.user_id = ".$_SESSION['userId']."
																				WHERE music.music_status=1 AND play_list.user_id = ".$_SESSION['userId']." ORDER BY music_created DESC");												
$playListString			   =	"";
if(count($getMyPlayList) >0){
foreach($getMyPlayList as $keyMyPlayList=>$allMyPlayList){
		$musicThumb				   =	($allMyPlayList['music_thumb'])?$allMyPlayList['music_thumb']:'audio-thump.jpg';
		$musicUser					=	$objCommon->displayName($allMyPlayList);
		$musicUserThumb			   =	($allMyPlayList['upi_img_url'])?$allMyPlayList['upi_img_url']:'profile_pic.jpg';
		$musicCreated				 =	date(" F d, Y ",strtotime($allMyPlayList['music_created']));
		$musicuserRate				=	round($allMyPlayList['reviewAvg'])*20;
		$musicLikeStatus			  =	($allMyPlayList['ml_status']==1)?1:0;
		$playListString	  .=	"{ ";
		$playListString	  .=	"mp3:'".SITE_ROOT_AWS_MUSIC."uploads/music/".$allMyPlayList['music_url']."',title:'".addslashes($objCommon->limitWords($allMyPlayList['music_title'],20))."',artist:'".trim(addslashes($objCommon->html2text($allMyPlayList['music_artist'])))."',dateCreated:'".$musicCreated."',rating_yellow:'".$musicuserRate."%',likei:'".$allMyPlayList['music_id']."',sharei:'".$allMyPlayList['music_id']."',proffesion:'".$objCommon->html2text($allMyPlayList['c_name'])."',userInfo:'".$musicUser."',artist1:'".$objCommon->html2text($allMyPlayList['music_title'])."',poster:'".SITE_ROOT_AWS_MUSIC."uploads/music/".$musicThumb."',userthumb:'".SITE_ROOT_AWS_IMAGES."uploads/profile_images/".$musicUserThumb."',oga:'".SITE_ROOT_AWS_MUSIC."uploads/music/".$allMyPlayList['music_url']."',musicid:'".$allMyPlayList['music_id']."',musicid:'".$allMyPlayList['music_id']."',musicdesc:'".addslashes($objCommon->limitWords($allMyPlayList['music_descr'],150))."',music_user:'".$allMyPlayList['user_id']."',like_status:'".$musicLikeStatus."',votei:'".$allMyPlayList['music_id']."',vote_status:'".$allMyPlayList['mv_id']."'";
		if((count($getMyPlayList)-1) == $keyMyPlayList){
			$playListString  .=	" } ";
		}else{
			$playListString  .=	" }, ";
		}
		if($keyMyPlayList == 0){
			$firstPlayListItem=	$playListString;
		}
	}
	//echo $playListString;
}else{
	$firstPlayListItem	=	"{ mp3:'".SITE_ROOT_AWS_MUSIC."uploads/music/sample.mp3',title:'Famez Sample Music',artist:'Famez.com',dateCreated:'".date(" F d, Y ")."',rating_yellow:'100%',likei:'1',sharei:'1',proffesion:'Fameuz',userInfo:'Fameuz',artist1:'faisal Darshan',poster:'".SITE_ROOT_AWS_MUSIC."uploads/music/audio-thump.jpg',userthumb:'".SITE_ROOT_AWS_IMAGES."uploads/profile_images/profile_pic.jpg',oga:'".SITE_ROOT_AWS_MUSIC."uploads/music/sample.mp3',musicid:'',musicid:'',musicdesc:'Sample music by fameuz.com',like_status:'0' }";
	$playListString	=	"{ mp3:'".SITE_ROOT_AWS_MUSIC."uploads/music/sample.mp3',title:'Famez Sample Music',artist:'Famez.com',dateCreated:'August 01, 2015',rating_yellow:'100%',likei:'1',sharei:'1',proffesion:'Fameuz',userInfo:'Fameuz',artist1:'faisal Darshan',poster:'".SITE_ROOT_AWS_MUSIC."uploads/music/audio-thump.jpg',userthumb:'".SITE_ROOT_AWS_IMAGES."uploads/profile_images/profile_pic.jpg',oga:'".SITE_ROOT_AWS_MUSIC."uploads/music/sample.mp3',musicid:'',musicid:'',musicdesc:'Sample music by fameuz.com',like_status:'0' }";
}
?>
