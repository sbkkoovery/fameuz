<script type="text/javascript" >

$(document).ready(function(){
	$('body').fameuzLightbox({
			class : 'lightBoxs',
			sidebar: 'default',
			photos:{
				imageLoadUrl: '<?php echo SITE_ROOT.'ajax/popUpImageUser.php'?>',
				data_attr	: ['data-contentId','data-contentType','data-contentAlbum'],
				likeUrl	: '<?php echo SITE_ROOT?>ajax/like.php',
				shareUrl: '<?php echo SITE_ROOT?>ajax/share.php',
				workedUrl: '<?php echo SITE_ROOT?>ajax/worked_together.php',
				commentBoxUrl: '<?php echo SITE_ROOT.'widget/pop_image_comment_box.php' ?>',
				comments :{
					commentActionUrl:'<?php echo SITE_ROOT?>access/post_comment.php?action=add_comment',
					commentDelete: '<?php echo SITE_ROOT?>access/delete_comment.php',
				}
			},
			videos : {
				videoLoadUrl: '<?php echo SITE_ROOT.'ajax/popUpVideoUser.php'?>',
				data_attr	: ['data-vidEncrId'],
				likeUrl		: '<?php echo SITE_ROOT?>ajax/like_video.php',
				shareUrl	: '<?php echo SITE_ROOT?>ajax/share_video.php',
				commentBoxUrl:'<?php echo SITE_ROOT.'widget/pop_video_comment_box.php' ?>',
				comments :{
					commentActionUrl:'<?php echo SITE_ROOT?>access/post_video_comment.php?action=add_comment',
				}
			},
			skin: 
					{
						next	: '<img src="<?php echo SITE_ROOT?>fameuz_lightbox/images/next.png">',
						prev	: '<img src="<?php echo SITE_ROOT?>fameuz_lightbox/images/prev.png">',
						reset	: '<i class="fa fa-refresh"></i>',
						close	: '<img src="<?php echo SITE_ROOT?>fameuz_lightbox/images/close.png" width="15">',
						loader	: '<?php echo SITE_ROOT?>images/ajax-loader.gif',
						review	: '<i class="fa fa-chevron-right"></i>'
					}
		});
});
  $("#add-booking-form").validate({
			rules: {
				book_from_date: "required",
				book_to_date: "required",
				book_title:"required",
				book_email:{required:true,email: true},
			},
			messages: {
				book_from_date: 'Can\'t be empty',
				book_to_date: 'Can\'t be empty',
				book_title: 'Can\'t be empty',
				book_email: {required:'Can\'t be empty',email:'Please enter valid email address'}
			}

    });
		$(document).ready(function(e) {
            $('#change_status').click(function(e) {
                $('#status_option').fadeToggle();
				return false;
            });
			$('.follow').click(function(e) {
				$.get('<?php echo SITE_ROOT?>ajax/sent_friend_request.php',{"friendId":'<?php echo $getMyFriendDetails['user_id']?>'},function(data){

				});
                $(this).addClass('following');
				var icon = $(this).find('i')
				icon.removeClass('fa-plus');
				icon.addClass('fa-check');
				$(this).find('.text').html("Following");
				return false;
            });
			$('#like_btn').click(function(e) {
                 $(this).toggleClass('liked');
				return false;
            });
			$('.like_btn_photo').click(function(e) {
                 $(this).toggleClass('liked');
				return false;
            });
			$('.friedChatStatus').load('<?php echo SITE_ROOT?>ajax/friend_chat_status.php?fid=<?php echo $getMyFriendDetails['user_id']?>');
			var refreshId = setInterval(function()
			{
			$('.friedChatStatus').load('<?php echo SITE_ROOT?>ajax/friend_chat_status.php?fid=<?php echo $getMyFriendDetails['user_id']?>');
			}, 1000);
			 jQuery.ias({
                container : '.wrap_review', // main container where data goes to append
                item: '.item_review', // single items
                pagination: '.nav', // page navigation
                next: '.nav a', // next page selector
                loader: '<img src="<?php echo SITE_ROOT?>images/ajax-loader.gif"/>', // loading gif
                triggerPageThreshold: 3 // show load more if scroll more than this
            });
			$( "#book_from_date").datepicker({
			});
			$( "#book_to_date").datepicker({
			});
			$('input[name="book_attach"]').change(function(){
					var fileName = $(this).val();
					$(".attach_book").html(fileName);
			});
			
        });
</script>