
/*
Fameuz Plugin 1.0
 |----------------------------------------------------|
 |				Fameuz Lightbox Plugin				  |
 |				Developed by Designdays		     	  |
 |													  |
 |----------------------------------------------------|
 */
;(function ( $, window, document, undefined ) {

	"use strict";

		// Create the defaults once
		var pluginName = "fixedSidebar",
				defaults = {
					distance : 250,
					container : '.profile_content '
					
		};

		// The actual plugin constructor
		function Plugin ( element, options ) {
				this.element = element;
				this.settings = $.extend( {}, defaults, options );
				this._defaults = defaults;
				this._name = pluginName;
				this.init();
		}

		// Avoid Plugin.prototype conflicts
		$.extend(Plugin.prototype, {
				init: function () {
					var _self	=	this;
					this.updateClac();
				},
				buildFrag: function(){
					var _self = this;
					var topBar	=	'<div class="topBar_fixed"></div>';
					$(_self._defaults.container).append(topBar);
				},
				updateClac: function () {
					var _self	=	this;
					//cache jquery objects
					_self.windowHeight	=	$(window).height();
					_self.elemHeight	=	$(this.element).outerHeight(true);
					_self.offsetTop		=	$(this.element).offset().top;
					_self.dist			=	parseInt(_self.windowHeight - _self._defaults.distance);
					_self.topDist		=	parseInt(_self.elemHeight - _self.dist) - _self.offsetTop;
					
					if(_self.elemHeight > parseInt(_self.windowHeight- _self.offsetTop)){
						this.scrollEvent();
					}else if(_self.elemHeight < parseInt(_self.windowHeight- _self.offsetTop)){
						this.scrollEvent('tiny');
					}
				},
				scrollEvent: function(type){
						var _self	=	this;
					$(window).scroll(function(){
						_self.windowScrollTop	=	$(this).scrollTop();
						if(type == 'tiny'){
							if(_self.windowScrollTop > _self.offsetTop){
								$(_self.element).css({'position': 'fixed', 'top': 3, right: 'auto', left: 'auto', bottom: 'auto'})
							}else{
								$(_self.element).css({'position': 'relative', 'top': 'auto'});
							}
						}else{
							if(_self.windowScrollTop > parseInt(_self.elemHeight - _self.dist)){
								$(_self.element).css({'position': 'fixed', 'top': -(_self.topDist), right: 'auto', left: 'auto', bottom: 'auto'});
							}else {
								$(_self.element).css({'position': 'relative', 'top': 'auto'});
							}
						}
					});
				}
		});

		$.fn[ pluginName ] = function ( options ) {
				return this.each(function() {
						if ( !$.data( this, "plugin_" + pluginName ) ) {
								$.data( this, "plugin_" + pluginName, new Plugin( this, options ) );
						}
				});
		};

})( jQuery, window, document );