<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
$active							=	$objCommon->esc($_GET['active']);
include_once(DIR_ROOT."js/include/fameuz_lightbox_library.php");
?>
<link rel="stylesheet" href="<?php echo SITE_ROOT?>css/jquery-ui.css">
<script src="<?php echo SITE_ROOT?>js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.validate.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
<link href="<?php echo SITE_ROOT?>css/cmxform.css" rel="stylesheet" type="text/css" />
    <div class="inner_content_section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="inner_top_border">
                        <div class="user_profile">
						<?php echo $objCommon->checkEmailverification();?>
                            <div class="content">
                            <div class="pagination_box">
                             <a href="<?php echo SITE_ROOT.'user/home'?>">Home <i class="fa fa-caret-right"></i></a>
                             <a href="<?php echo SITE_ROOT.$getUserDetails['usl_fameuz']?>"> My profile </a><i class="fa fa-caret-right"></i></a> 
        					 <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                             <a href="javascript:;" class="active"> Edit Profile</a>
                                     <?php
								include_once(DIR_ROOT."widget/notification_head.php");
								?>
                                </div>
                                <div class="row">
                                    <div class="col-lg-sp-2">
                                <?php
								include_once(DIR_ROOT."includes/profile_left.php");
								?>
                                </div>
                                <div class="col-lg-sp-10"><div class="profile_content">
                                	<?php
									echo $objCommon->displayMsg();
									?>
                                    <div class="edit_profile">
                                        <a class="edit_btn" id="edit_btn" href="#"><img alt="mob_btn" src="<?php echo SITE_ROOT?>images/edit_btn.png"></a>
                                        <div class="tabs" id="tabs">
                                            <div class="tab <?php echo ($active=='personal' || $active == '')?'active':''?>"><a href="#" data-label="personel" class="loadPersonal">Personal</a></div>
                                            <div class="tab <?php echo ($active == 'public')?'active':''?>"><a href="#" data-label="public" class="loadPublic">Public</a></div>
                                            <div class="tab <?php echo ($active == 'profile_photo')?'active':''?>"><a href="#" data-label="profile_photo" class="loadProfilePhoto">Profile Photo</a></div>
                                            <div class="tab <?php echo ($active == 'social_media')?'active':''?>"><a href="#" data-label="social_media" class="loadSocialMedia">Social media</a></div>
                                            <div class="tab <?php echo ($active == 'settings')?'active':''?>"><a href="#" data-label="settings" class="loadSettings">Settings</a></div>
                                            <div class="clr"></div>
                                        </div> 
										<div class="tab_content <?php echo ($active=='personal' || $active == '')?'':'hide'?>" id="personel">
											<div class="load_album_preloader"></div>
										</div>
										<div class="tab_content <?php echo ($active == 'public')?'':'hide'?>" id="public">
											<div class="load_album_preloader"></div>
										</div>
										<div class="tab_content <?php echo ($active == 'profile_photo')?'':'hide'?>" id="profile_photo">
											<div class="load_album_preloader"></div>
										</div>
										<div class="tab_content <?php echo ($active == 'social_media')?'':'hide'?>" id="social_media">
											<div class="load_album_preloader"></div>
										</div>
										<div class="tab_content <?php echo ($active == 'settings')?'':'hide'?>" id="settings">
											<div class="load_album_preloader"></div>
										</div>
                                       <?php
										//include_once(DIR_ROOT."includes/edit_personal_details.php");
										//include_once(DIR_ROOT."includes/edit_public_details.php");
										//include_once(DIR_ROOT."includes/edit_profile_photo.php");
										//include_once(DIR_ROOT."includes/edit_social_media.php");
										//include_once(DIR_ROOT."includes/edit_settings.php");
									   ?>
                                </div>
                                <div class="clr"></div>            
                            </div></div></div> 
                             
                            <div class="clr"></div>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
	$(window).load(function(e) {
		$('.profile_box').fixedSidebar();
	});
		$(document).ready(function(e) {
			var activeTab		=	'<?php echo $active?>';
			if(activeTab=='public'){
				$("#public").load('<?php echo SITE_ROOT?>includes/edit_public_details.php',function(){
					$(".load_album_preloader").hide();
					initialize();
				});
			}else if(activeTab=='profile_photo'){
				$("#profile_photo").load('<?php echo SITE_ROOT?>includes/edit_profile_photo.php',function(){
					$(".load_album_preloader").hide();
				});
			}else if(activeTab=='social_media'){
				$("#social_media").load('<?php echo SITE_ROOT?>includes/edit_social_media.php',function(){
					$(".load_album_preloader").hide();
				});
			}else if(activeTab=='settings'){
				$("#settings").load('<?php echo SITE_ROOT?>includes/edit_settings.php',function(){
					$(".load_album_preloader").hide();
				});
			}else{
				$("#personel").load('<?php echo SITE_ROOT?>includes/edit_personal_details.php',function(){
					$(".load_album_preloader").hide();
				});
			}
			
			$('#change_status').click(function(e) {
                $('#status_option').fadeToggle();
				return false;
            });
            $('#option_btn').click(function(e) {
                $('#option_box').fadeToggle();
				return false;
            });
            $('#working').click(function(e) {
                $('.upload_section').slideDown();
				return false;
            });
			$('#edit_btn').click(function(e) {
                $('#tabs .tab').slideToggle();
				return false;
            });
			$('#tabs .tab').click(function(e) {
				$('#tabs .tab').removeClass("active");
				//$('#tabs .tab').slideUp();
                $(this).addClass("active");
				var dta = $(this).find("a").data();
				$("#"+dta.label).removeClass("hide");
				$("#"+dta.label).siblings(".tab_content").addClass("hide");
				return false;
            });
			/*$('.edited-privacy').click(function(e) {
                $(this).find('.drop_down').slideToggle();
			 });
			$('.h_close a').click(function(e){
				$(this).parent().parent().parent().remove();
				return false;
			});*/
			//$('#option_btn').val(optionvalue);
        });
		
		
$(".loadPersonal").on("click",function(){
	$("#personel").children(".load_album_preloader").show();
	$("#personel").load('<?php echo SITE_ROOT?>includes/edit_personal_details.php',function(){
		$(".load_album_preloader").hide();
	});
});		
$(".loadPublic").on("click",function(){
	$("#public").children(".load_album_preloader").show();
	$("#public").load('<?php echo SITE_ROOT?>includes/edit_public_details.php',function(){
		$(".load_album_preloader").hide();
		initialize();
	});
});
$(".loadProfilePhoto").on("click",function(){
	$("#profile_photo").children(".load_album_preloader").show();
	$("#profile_photo").load('<?php echo SITE_ROOT?>includes/edit_profile_photo.php',function(){
		$(".load_album_preloader").hide();
	});
});
$(".loadSocialMedia").on("click",function(){
	$("#social_media").children(".load_album_preloader").show();
	$("#social_media").load('<?php echo SITE_ROOT?>includes/edit_social_media.php',function(){
		$(".load_album_preloader").hide();
	});
});
$(".loadSettings").on("click",function(){
	$("#settings").children(".load_album_preloader").show();
	$("#settings").load('<?php echo SITE_ROOT?>includes/edit_settings.php',function(){
		$(".load_album_preloader").hide();
	});
});
</script>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/fixed_sidebar.js"></script>
<?php
include_once(DIR_ROOT.'js/include/my_page.php');
include_once(DIR_ROOT."includes/footer.php");
?>