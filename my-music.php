<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/music.php");
$objMusic							=	new music();
?>
<script src="<?php echo SITE_ROOT?>jw_player/jwplayer.js"></script>
    <div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="user_profile">
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                                    <div class="content music-pagein">
                                        <div class="pagination_box">
                                            <div class="row">   
                                                <div class="col-sm-12 not">
													<div class="pagination_box">
                                                        <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
                                                        <a href="<?php echo SITE_ROOT.$getUserDetails['usl_fameuz']?>"> My profile </a><i class="fa fa-caret-right"></i></a>
                                                        <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
														<a href="javascript:;" class="active"> My Music  </a>
														<?php
														include_once(DIR_ROOT."widget/notification_head.php");
														?>
													</div>
                                                </div>
                                                </div>
                                      </div>
                                        <div class="clr"></div>            
                                    </div>
                                </div> 
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3 music-page">
                                    <div class="sidebar">
                                        <div class="search_box"><form action="<?php echo SITE_ROOT.'user/search-result'?>" method="get"><input type="text" placeholder="Search "   name="keywordSearch" /></form></div>
                                    </div>
                                </div> 
                            </div>
                            <div class="clr"></div>
                            <div class="videos">
                                <div class="row">
								<?php  echo $objCommon->displayMsg();?>
                                    <div class="clr"></div> 
                                    <div class="top_videos">
                                        <h1>My Music</h1>
										<div class="loadMusic"></div>
                            </div>
                            <div class="clr"></div>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
    <script type="text/javascript">
$(document).ready(function(e) {
	getresult('<?php echo SITE_ROOT?>widget/load_my_music.php');
});
function getresult(getUrl) {
	$.ajax({
		url: getUrl,
		type: "GET",
		data:  {rowcount:$("#rowcount").val()},
		beforeSend: function(){
		//$('#loader-icon').show();
		},
		complete: function(){
		//$('#loader-icon').hide();
		},
		success: function(data){
			$(".loadMusic").append(data);
			if($("#total-count").val()>0){
			}else{
				$(".loadMusic").html('<p>No music found....</p>');
			}
		},
		error: function(){} 	        
   });
}
$(window).scroll(function(){
		if ($(window).scrollTop() == $(document).height() - $(window).height()){
			if($(".pagenum:last").val() <= $(".total-page").val()) {
				var pagenum = parseInt($(".pagenum:last").val()) + 1;
				getresult('<?php echo SITE_ROOT?>widget/load_my_music.php?page='+pagenum);
			}
		}
	}); 
</script>
<?php
include_once(DIR_ROOT."includes/login_box.php");
include_once(DIR_ROOT."includes/footer.php");
?>
    