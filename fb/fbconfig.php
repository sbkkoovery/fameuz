<?php
session_start();
// added in v4.0.0
require_once 'autoload.php';
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/user_fb_details.php");
include_once(DIR_ROOT."class/users.php");
$objCommon				=	new common();
$objFbdetails		=	new user_fb_details();
$objUsers		=	new users();

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;
// init app with app id and secret

FacebookSession::setDefaultApplication( '444683995720527','b15561a4b2100510898573eaf96342ae' );
// login helper with redirect_uri

    $helper = new FacebookRedirectLoginHelper(SITE_ROOT.'fb/fbconfig.php' );
try {
  $session = $helper->getSessionFromRedirect();
} catch( FacebookRequestException $ex ) {
  // When Facebook returns an error
} catch( Exception $ex ) {
  // When validation fails or other local issues
}
// see if we have a session

if(isset($session)){
	
  // graph api request for user data
 //$request = new FacebookRequest( $session, 'GET', '/me?locale=en_US&fields=id,name,email' );
 
  $request = new FacebookRequest( $session, 'GET', '/me?scope=email' );
  $response = $request->execute();
  // get response
 
  $graphObject = $response->getGraphObject();
       $fbid = $graphObject->getProperty('id');              // To Get Facebook ID
 	   $fbfullname = $graphObject->getProperty('name'); // To Get Facebook full name
	   $femail = $graphObject->getProperty('email');    // To Get Facebook email ID 
	/* ---- Session Variables -----*/ 
	    $_SESSION['FBID'] = $fbid;           
        $_SESSION['FULLNAME'] = $fbfullname;
	    $_SESSION['EMAIL'] =  $femail; 
		$femail = $femail?:'Noemail';
    /* ---- header location after session ----*/ 
	
	
	$getFbIdRow	  =	$objFbdetails->getRow("fb_id='".$fbid."' AND status=1");
	if($getFbIdRow['fb_id']!=''){
		//$getEmailRow	  =	$objUsers->getRow("user_id='".$getFbIdRow['user_id']."'");
		//$emailUser        =    $getEmailRow['email'];
		$getvalidUser = $objUsers->getRow("user_id='".$getFbIdRow['user_id']."'");
		if($getvalidUser['status']=='1'){
		      $_SESSION['userId']		=	$getFbIdRow['user_id'];
		      if(isset($_SESSION['login_redirect_url'])){
			     	$login_redirect_url	=	$_SESSION['login_redirect_url'];
				    unset($_SESSION['login_redirect_url']);
				    header("location:".$login_redirect_url);
				    exit;
		      }
		      if(isset($_SESSION['userId'])&& $_SESSION['userId']!='NULL'){
		            header("location:".SITE_ROOT."user/home");
		            exit;
		      }
		      else
		     {
			      header("location:".SITE_ROOT."user/register?emuser=".$femail);
		     }
		}
		else
		{
		   header("location:".SITE_ROOT."login");
		}
	}
	else
	{
		 
	    $_POST['fb_id']	=	$fbid; 
		$_POST['Fullname']	=	$fbfullname;
		
		$objFbdetails->insert($_POST);
		header("Location: ../register.php?emuser=".$femail);
	}	
	
  
} else {
	$loginUrl = $helper->getLoginUrl(array('scope' => 'email, public_profile,user_friends'));
//  $loginUrl = $helper->getLoginUrl();
 header("Location: ".$loginUrl);
}
?>