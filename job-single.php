<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/jobs.php");
$objJobs					 =	new jobs();
$jobId					   =	$objCommon->esc($_GET['jobId']);
$userId					  =	$_SESSION['userId'];
if($jobId ==''){
	header("location:".SITE_ROOT."jobs");
	exit;
}
$getJobDetails		   =	$objJobs->getRowSql("SELECT job.*,applied.ja_id FROM jobs AS job LEFT JOIN job_applied AS applied ON (job.job_id = applied.job_id AND applied.ja_applied_by =".$userId.") WHERE ((job.job_start <= CURDATE() AND job.job_end >= CURDATE()) OR job.job_start > CURDATE())  AND job.job_status=1 AND  job.job_id=".$jobId);
if($getJobDetails['job_id'] ==''){
	header("location:".SITE_ROOT."jobs");
	exit;
}
$jobcategory			 =	$getJobDetails['job_category'];
$jobcategory			 =	trim($jobcategory,",");
$getJobcategory		  =	$objJobs->getRowSql("SELECT group_concat(jc_name) AS catJob FROM job_category WHERE jc_id IN(".$jobcategory.")");
?>
<div class="inner_content_section">
	<div class="container">
		<div class="row">
				<div class="inner_top_border">
					<div class="user_profile">
					<?php echo $objCommon->checkEmailverification();?>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
						<div class="content">
							<div class="pagination_box">
								<a href="#">Home <i class="fa fa-caret-right"></i> </a>
                                <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                <a href="<?php echo SITE_ROOT?>jobs" class="active">  Jobs </a>
                                <?php
								include_once(DIR_ROOT."widget/notification_head.php");
								?>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div>
										<div class="visitor_head_box">
                                            <h5><?php echo $objCommon->html2text($getJobDetails['job_title'])?></h5>
                                            <span class="arw-point"></span>
                                        </div>
									</div>
								</div>
							</div>
							<div class="applyjob-modal">
							<div class="visitor_list_box">
								<div class="row">
									<div class="col-md-3">
										<div class="job_desc_head">
											<h3>Location</h3>
										</div>
									</div>
									<div class="col-md-1">:</div>
									<div class="col-md-8"><?php echo ($getJobDetails['job_city'])?$objCommon->html2text($getJobDetails['job_city']).',':''?>
									<?php echo ($getJobDetails['job_state'])?$objCommon->html2text($getJobDetails['job_state']).',':''?><?php echo ($getJobDetails['job_country'])?$objCommon->html2text($getJobDetails['job_country']):''?>
									</div>
								</div>
								<?php
								if($getJobcategory['catJob']){
									?>
									<div class="row">
									<div class="col-md-3">
										<div class="job_desc_head">
											<h3>Category</h3>
										</div>
									</div>
									<div class="col-md-1">:</div>
									<div class="col-md-8"><?php echo $objCommon->html2text($getJobcategory['catJob'])?></div>
								</div>
									<?php
								}
								?>
								<div class="row">
									<div class="col-md-3">
										<div class="job_desc_head">
											<h3>Date</h3>
										</div>
									 </div>
									<div class="col-md-1">:</div>
									<div class="col-md-8"><?php echo ($getJobDetails['job_start'] !='' || $getJobDetails['job_start'] !=0000-00-00)?date("d M Y",strtotime($getJobDetails['job_start'])).' ':''?> <?php echo ($getJobDetails['job_start'] !='' || $getJobDetails['job_end'] !=0000-00-00)?' <span class="grey">to</span>  '.date("d M Y",strtotime($getJobDetails['job_end'])).' ':''?></div>
								</div> 
								<div class="row">
									<div class="col-md-3">
										<div class="job_desc_head">
											<h3>Company</h3>
										</div>
									</div>
									<div class="col-md-1">:</div>
									<div class="col-md-8"><?php echo $objCommon->html2text($getJobDetails['job_company'])?></div>
								</div>
								<div class="row">
									<div class="col-md-3">
										<div class="job_desc_head">
											<h3>Skills</h3>
										</div>
									</div>
									<div class="col-md-1">:</div>
									<div class="col-md-8"><?php echo $objCommon->html2text($getJobDetails['job_skill'])?></div>
								</div>
								<?php
								if($getJobDetails['job_payment']){
									$explpaymentjob		=	str_replace("###"," ",$objCommon->html2text($getJobDetails['job_payment']));
								?>
								<div class="row">
									<div class="col-md-3">
										<div class="job_desc_head">
											<h3>Payment</h3>
										</div>
									</div>
									<div class="col-md-1">:</div>
									<div class="col-md-8"><?php echo $explpaymentjob;?></div>
								</div>
								<?php
								}
								?>
								<div class="row">
									<div class="col-md-3">
										<div class="job_desc_head">
											<h3>Job Posted Date</h3>
										</div>
									</div>
									<div class="col-md-1">:</div>
									<div class="col-md-8"><?php echo date("d M Y",strtotime($getJobDetails['job_created']))?></div>
								</div>
								<div class="row">
									<div class="col-md-12">
									<?php echo $objCommon->html2text($getJobDetails['job_descr'])?>
									</div>
								</div>
								<?php
								if($userId != $getJobDetails['user_id']){
								?>
								<div class="row">
									<div class="col-sm-12 text-right">
										<?php
										if($getJobDetails['ja_id'] < 1){
										?>
										<button type="submit" class="btn btn-primary apply_for_job">Apply</button>
										<?php
										}else{
											echo '<span class="applied">You have already applied for this job !</span>';
										}
										?>
									</div>
								</div>
								<?php
								}
								?>
								</div>
							</div>
						</div>
                        </div>
						<?php
						include_once(DIR_ROOT."widget/right_static_ad_bar.php");
						?>
					</div>
				</div>
			</div>
	</div>
</div>
<script>
$(".apply_for_job").on("click",function(){
	var jobid				=	'<?php echo $jobId?>';
	var jobBy				=	'<?php echo $getJobDetails['user_id']?>';
	var that				 =	this;
	if(jobid){
		$.ajax({
				url:'<?php echo SITE_ROOT.'ajax/apply_for_job.php'?>',
				data:{jobid:jobid,jobBy:jobBy},
				type:"POST",
				success: function(result){
					$(that).parent().html('<span style="color:#afb420;">Applied</span>');
				}
			});
	}
});
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  