<?php
include_once("db_functions.php");
class user_social_links extends db_functions 
{
	var $tablename			= 	"user_social_links";
	var $primaryKey			= 	"usl_id";
	var $table_fields	  	=   array('usl_id'=>"",'user_id'=>"",'usl_fameuz' => "","usl_fb"=>'',"usl_twitter"=>'',"usl_gplus"=>'',"usl_instagram"=>'',"usl_others"=>'');
	function user_social_links()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>