<?php
include_once("db_functions.php");
class sub_category extends db_functions 
{
	var $tablename			= 	"sub_category";
	var $primaryKey			= 	"s_id";
	var $table_fields	  	=   array('s_id'=>"",'c_id' => "","s_name"=>'');
	function sub_category()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>