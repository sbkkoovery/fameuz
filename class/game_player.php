<?php
include_once("db_functions.php");
class game_player extends db_functions{
     var $tablename = "game_player";
     var $primaryKey = "gp_id";
     var $table_fields = array("gp_id"=>"","user_id"=>"","g_id"=>"","gp_created"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>