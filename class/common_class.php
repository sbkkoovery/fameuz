<?php
include_once(DIR_ROOT."class/users.php");
$objUsers			=	new users();
class common
{
	public function esc($s){
	   $s				= htmlentities($s);
	   $s				= str_replace("'","&#39;",$s);
	   $s				= mysql_real_escape_string($s);
	   return $s;
	}
	function adminLogin(){
		$admin	=	false;
		if(isset($_SESSION['master'])){
			$admin	=	true;
		}
		return $admin;
	}
	function adminCheck(){
		if(!isset($_SESSION['master'])){
			header("location:../login.php");
			exit();
		}
	}
	public function html2text($s){
		//htmlspecialchars($s, ENT_QUOTES, 'UTF-8');
		
		$s				= html_entity_decode($s);
		$s				= strip_tags($s,'<h1><h2><h3><h4><h5><h6><table><tr><td><th><p><ul><li><b><strong><i><u><div><a><img><br>');
		$s				= stripslashes($s);
		$s				= str_replace("\r\n","<br/>",$s);
		$s				= str_replace("\\r\\n","<br/>",$s);
		$s				= str_replace("\\n","<br/>",$s);
		$s    			= str_replace("'","&#39;",$s);
		$s				= nl2br($s);
		//$s 				= filter_var($s, FILTER_SANITIZE_STRING);
		
		return $s;
	}
	function html2textarea($s){
		$s				= html_entity_decode($s);
		//$s				= stripslashes($s);
		$s				= str_replace("\r\n","<br/>",$s);
		$s				= str_replace("\\r\\n","<br/>",$s);
		$s				= str_replace("<br/>","\n",$s);
		//$s				= nl2br($s);
		return $s;
	}
	function displayEditor($s){
		$s				= stripslashes($s);
		$s				= str_replace("rn","",$s);
		return $s;
	}
	function addMsg($message,$type){
		$messageArray	=	array("msg"=>$message,"type"=>$type);
		$_SESSION["message"]=$messageArray;
	}
	function local_time($dateStr){
		$minutes_to_add	=	0;
		$minutes_to_add	=	$_SESSION['localtime'];//+240 if mysql time zone is Arabian Standard Time
		$time = new DateTime(date($dateStr));
		if($minutes_to_add >0){
			$minutes_to_add	=	abs($minutes_to_add);
			$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
		}else{
			$minutes_to_add	=	abs($minutes_to_add);
			$time->sub(new DateInterval('PT' . $minutes_to_add . 'M'));
		}
		
		return $time->format('Y-m-d H:i:s');
	}
	function displayMsg(){
		if(isset($_SESSION['message'])){
			$message	=	$_SESSION['message']["msg"];
			$type		=	$_SESSION['message']["type"];
			$selectClass=	($type==1)?"success":"danger";
			$messageBox	=	 '<div class="alert alertClose alert-'.$selectClass.' alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alertClose" aria-label="Close"><span aria-hidden="true">&times;</span></button> '.$message.'</div><script>$( ".alertClose" ).delay( 4000 ).slideUp( 400 );</script>';
							unset($_SESSION['message']);
							if(isset($_SESSION['sect'])){
								unset($_SESSION['sect']);
							}
							return $messageBox;
		}
	}
	function displayMsg2(){
		if(isset($_SESSION['message'])){
			$message	=	$_SESSION['message']["msg"];
			$type		=	$_SESSION['message']["type"];
			$messageBox		=	"<script>Lobibox.notify('success', { msg: '".$message."',icon: false,delay: 1500 })</script>";
		}
		unset($_SESSION['message']);
		if(isset($_SESSION['sect'])){
			unset($_SESSION['sect']);
		}
		return $messageBox;
	}
	function get_client_ip() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
		   $ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}
	function randStrGen($len){
		$result = "";
		$chars = "abcdefghijklmnopqrstuvwxyz0123456789";
		$charArray = str_split($chars);
		for($i = 0; $i < $len; $i++){
			$randItem = array_rand($charArray);
			$result .= "".$charArray[$randItem];
		}
		return $result;
	}
	function randStrGenDigit($len){
		$result = "";
		$chars = "0123456789";
		$charArray = str_split($chars);
		for($i = 0; $i < $len; $i++){
			$randItem = array_rand($charArray);
			$result .= "".$charArray[$randItem];
		}
		return $result;
	}
	function randStrGenSpecial($len){
		$result = "";
		$chars = "abcdefghijklmnopqrstuvwxyz0123456789@$*_";
		$charArray = str_split($chars);
		for($i = 0; $i < $len; $i++){
			$randItem = array_rand($charArray);
			$result .= "".$charArray[$randItem];
		}
		return $result;
	}
function checkEmailverification(){
	global $getUserDetails;
	if($getUserDetails['email_validation'] !=1 ){
		echo '<div class="alert alert-warning" role="alert"><strong>Warning ! </strong> Your Account has not been verified yet..Please check your email and verify your account</div>';
	}
}
function checkPrivacy($str){
	if($str=='1,0,0,0' || $str=='1,0,0,1' || $str=='1,0,1,0' || $str=='1,0,1,1' || $str=='1,1,0,0' || $str=='1,1,0,1' || $str=='1,1,1,0' || $str=='1,1,1,1'){
		$retPriv		.=	1;
	}
	if($str=='0,1,0,0' || $str=='0,1,0,1' || $str=='0,1,1,0' || $str=='0,1,1,1' ){
		$retPriv		.=	2;
	}
	if($str=='0,1,1,0' || $str=='0,1,1,1' || $str=='0,0,1,0' || $str=='0,0,1,1' ){
		$retPriv		.=	3;
	}
	if($str=='0,0,0,1' ){
		$retPriv		.=	4;
	}
	return $retPriv;
	
}
	function addIMG($file,$folder,$name,$width,$height,$thump){
		$ret="";
		if(isset($file)){
			$ext=explode(".",$file['name']);
			$ext=(count($ext)!=0)?strtolower($ext[count($ext)-1]):"";
			$path="$folder$name.$ext";
			$name="$name.$ext";
			if(copy($file['tmp_name'],$path)){
				$proc=false;
				switch($ext){
					case "jpg": $proc=true; $im=imagecreatefromjpeg($path); break;
					case "jpeg": $proc=true; $im=imagecreatefromjpeg($path); break;
					case "gif": $proc=true; $im=imagecreatefromgif($path); break;
					case "png": $proc=true; $im=imagecreatefrompng($path); break;
				}
				if($proc){
					$ow=imagesx($im);
					$oh=imagesy($im);
					$bow=$ow;
					$boh=$oh;
					$posX=0;
					$posY=0;
					if($ow>$width||$oh>$height){
						if($thump){ 
							$cmp=1; 
							if($oh>$ow){ $cmp = $ow/$width; }
							if($ow>$oh){ $cmp = $oh/$height; }
							if($ow==$oh){ $cmp = $oh/$height; }
							$ow=round($ow/$cmp); 
							$oh=round($oh/$cmp);
							if($ow<$width){
								$cmp = $ow/$width;
								$ow=round($ow/$cmp); 
								$oh=round($oh/$cmp);
							}
							if($oh<$height){
								$cmp = $oh/$height;
								$ow=round($ow/$cmp); 
								$oh=round($oh/$cmp);
							}
						}else{
							$cmp=1; 
							if($ow>$oh){ $cmp = $ow/$width; }
							if($ow<$oh){ $cmp = $oh/$height; }
							if($ow==$oh){ $cmp = $oh/$height; }
							$ow=round($ow/$cmp); 
							$oh=round($oh/$cmp);
						}
					}
					if($thump){ 
						$posX=round(($width-$ow)/2); 
						$posY=round(($height-$oh)/2); 
					}
					$bow1=$ow; 
					$boh1=$oh; 
					if($thump){ 
						$ow=$width; 
						$oh=$height; 
					}
					$newImg = imagecreatetruecolor($ow,$oh);
					if($ext=="png"){
						imagealphablending($newImg, false);
						imagesavealpha($newImg,true);
						$transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
						imagefilledrectangle($newImg, 0, 0, $ow, $oh, $transparent);
					}
					imagecopyresampled($newImg, $im, $posX, $posY, 0, 0 , $bow1, $boh1, $bow, $boh);
					if($ext=="png"){ 
						imagepng($newImg,$path,9); 
					}else if($ext=="jpg"){ 
						imagejpeg($newImg,$path,90); 
					}else if($ext=="gif"){ 
						imagegif($newImg,$path); 
					}
					$ret= $name;
				}
			}
		}
		return $ret;
	}
	function getAlias($title){
		$alias	=	strtolower($title);		
		$alias	=	preg_replace("/[^a-z0-9]+/", "-", $alias);
		$alias	=	rtrim($alias, '-');
		$alias	=	ltrim($alias, '-');
		return $alias;
	}
	function getAge($birthDate){
		$age	=	floor((time()-strtotime($birthDate))/31556926);
		return $age;
	}
	function limitWords($pageContent,$limit){
		$resultWord		=	$this->html2text($pageContent);
		if(strlen($resultWord)>$limit){
			$resultWord	=	mb_substr($resultWord,0,$limit,'UTF-8')."...";
		}
		return $resultWord;
	}
	function smallWords($str,$count){
		if (strlen($str) > $count){
			$str = wordwrap($str, $count);
			$str = substr($str, 0, strpos($str, "\n"));
		}
		return $str;
	}
	function getYoutubeId($url){
		$submitID = preg_replace('/[^\w-_:?=.\/\\\\]|\s$/', '', $url);
		preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $submitID, $matches);
		if (isset($matches[1])){
			$videoID = $matches[1];
		}
		return $videoID;
	}
	function covtimeYoutubeTime($youtube_time) {
		preg_match_all('/(\d+)/',$youtube_time,$parts);
	
		// Put in zeros if we have less than 3 numbers.
		if (count($parts[0]) == 1) {
			array_unshift($parts[0], "0", "0");
		} elseif (count($parts[0]) == 2) {
			array_unshift($parts[0], "0");
		}
	
		$sec_init = $parts[0][2];
		$seconds = $sec_init%60;
		$seconds_overflow = floor($sec_init/60);
	
		$min_init = $parts[0][1] + $seconds_overflow;
		$minutes = ($min_init)%60;
		$minutes_overflow = floor(($min_init)/60);
	
		$hours = $parts[0][0] + $minutes_overflow;
	
		if($hours != 0)
			return sprintf("%02d",$hours).':'.sprintf("%02d",$minutes).':'.sprintf("%02d",$seconds);
		else
			return sprintf("%02d",$minutes).':'.sprintf("%02d",$seconds);
	}
	function getYoutubeDetails($link) {
		$videoID		 =	$this->getYoutubeId($link);
		$apiKey	 	  =	'AIzaSyAcG9KHZjelMM2DqNEhYdt59g42srDT50c';
		if($videoID){
			$url		 =	'https://www.googleapis.com/youtube/v3/videos?id='.$videoID.'&key='.$apiKey.'&part=contentDetails';
			$string 	  = file_get_contents($url);
			$json_a 	  = json_decode($string, true);
			$duration	=	$this->covtimeYoutubeTime($json_a['items'][0]['contentDetails']['duration']);
			$thumbNail   = 	'https://i.ytimg.com/vi/'.$videoID.'/hqdefault.jpg';
			$resultArr   =	array("duration"=>$duration,"thumbnail"=>$thumbNail);
		}
		return $resultArr;
	}
	/*function getYoutubeDetails($id) {
		$submitID = preg_replace('/[^\w-_:?=.\/\\\\]|\s$/', '', $id);
		if (strpos($submitID, '/') === false) {
			$videoID = $submitID;
		}else {
			preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $submitID, $matches);
			if (isset($matches[1])){
				$videoID = $matches[1];
			}else {
				$videoID = '';
			}
		}
		$videoID = preg_replace('/[^\w-_]+/', '', $videoID);
		if($videoID){
			$xml 				=	simplexml_load_file('https://gdata.youtube.com/feeds/api/videos/'.$videoID.'?v=2');
			$result 			 =	$xml->xpath('//yt:duration[@seconds]');
			$total_seconds 	  = 	(int) $result[0]->attributes()->seconds;
			$total_seconds	  =	gmdate("H:i:s", $total_seconds);
			$thumbNail	 	  = 	'https://i.ytimg.com/vi/'.$videoID.'/maxresdefault.jpg';
			$resultArr	 	  =	array("duration"=>$total_seconds,"thumbnail"=>$thumbNail);
			return $resultArr;
		}
	}*/
	function displayName($arrName){
		if($arrName['display_name']){
			$displayNameFriend	 =	$this->html2text($arrName['display_name']);
		}else if($arrName['first_name']){
			$displayNameFriend	 =	$this->html2text($arrName['first_name']);
		}else{
			$exploEmailFriend	  =	explode("@",$this->html2text($arrName['email']));
			$displayNameFriend	 =	$exploEmailFriend[0];
		}
		return $displayNameFriend;
	}
	function displayNameSmall($arrName,$count){
		if($arrName['display_name']){
			$displayNameFriend	 =	$this->html2text($arrName['display_name']);
		}else if($arrName['first_name']){
			$displayNameFriend	 =	$this->html2text($arrName['first_name']);
		}else{
			$exploEmailFriend	  =	explode("@",$this->html2text($arrName['email']));
			$displayNameFriend	 =	$exploEmailFriend[0];
		}
		$displayNameFriendSmall	=	$this->smallWords($displayNameFriend,$count);
		return $displayNameFriendSmall;
	}
	function displayNameFields($arrName,$fields){
		if($arrName[$fields['display_name']]){
			$displayNameFriend	 =	$this->html2text($arrName[$fields['display_name']]);
		}else if($arrName[$fields['first_name']]){
			$displayNameFriend	 =	$this->html2text($arrName[$fields['first_name']]);
		}else{
			$exploEmailFriend	  =	explode("@",$this->html2text($arrName[$fields['email']]));
			$displayNameFriend	 =	$exploEmailFriend[0];
		}
		return $displayNameFriend;
	}
	function displayNameFieldsSmall($arrName,$fields,$count){
		if($arrName[$fields['display_name']]){
			$displayNameFriend	 =	$this->html2text($arrName[$fields['display_name']]);
		}else if($arrName[$fields['first_name']]){
			$displayNameFriend	 =	$this->html2text($arrName[$fields['first_name']]);
		}else{
			$exploEmailFriend	  =	explode("@",$this->html2text($arrName[$fields['email']]));
			$displayNameFriend	 =	$exploEmailFriend[0];
		}
		return $this->smallWords($displayNameFriend,$count);
	}
	function time_elapsed_string($ptime)
	{
		$etime = (strtotime($this->local_time(date("Y-m-d H:i:s")))) - $ptime;
		if ($etime < 1){
			return '0 seconds';
		}
		$a = array( 365 * 24 * 60 * 60  =>  'year',
					 30 * 24 * 60 * 60  =>  'month',
						  24 * 60 * 60  =>  'day',
							   60 * 60  =>  'hour',
									60  =>  'minute',
									 1  =>  'second'
					);
		$a_plural = array( 'year'   => 'years',
						   'month'  => 'months',
						   'day'    => 'days',
						   'hour'   => 'hours',
						   'minute' => 'minutes',
						   'second' => 'seconds'
					);
	
		foreach ($a as $secs => $str)
		{
			$d = $etime / $secs;
			if ($d >= 1)
			{
				$r = round($d);
				return '<span class="five_moths">'.$r.'</span>' . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
			}
		}
	}
	function user_info_widget($profile_name=NULL,$profile_img=NULL,$date=NULL,$profession=NULL,$rating=NULL){
		$rating_style = 'style="width:'.($rating*20).'%;"';
		?>
		<div class="user">
            <div class="thump"><img class="img-responsive" src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($profile_img)?$profile_img:'profile_pic.jpg'?>" alt="thump"></div>
                <div class="user_info">
                  <h5><?php echo $profile_name?></h5>
                  <span class="date"><i class="fa fa-globe"></i><?php echo $date ?></span>&sdot; <span class="proffesion">Videography</span> 
                  <?php
				  if($rating){
					  echo '<div class="rating_box"><div class="rating_yellow" '.$rating_style.'></div></div>';
				  }else{
	
				  }
				  ?>
               	 
               </div>
        </div>
<?php
	}
	function user_info_home_widget($profile_name=NULL,$profile_img=NULL,$date=NULL,$profession=NULL,$rating=NULL,$hours=NULL){
		$rating_style = 'style="width:'.($rating*20).'%;"';
		?>
		<div class="user">
            <div class="thump no-radius"><?php echo $profile_img?></div>
                <div class="user_info">
                   <p class="postmsgs"><?php echo $profile_name?></p>
                  <?php
				  if($rating){
					  echo '<div class="rating_box"><div class="rating_yellow" '.$rating_style.'></div></div>';
				  }
				  ?>
                  <span class="date"><i class="fa_calendar"></i><?php echo $date ?></span>&sdot; <span class="proffesion"><?php echo $profession?></span> 
               	 
               </div>
               <div class="pull-right time-post">
                   <p><?php echo $hours?> . <i class="fa_globe"></i></p>
               </div>
        </div>
<?php
	}
	function round_to_nearest_half($number) {
    return round($number * 2) / 2;
}
	function getThumb($ar)
	{
		$arrExpl				=	explode("/",$ar);
		$thumbPath			  =	$arrExpl[0].'/thumb/'.$arrExpl[1];	
		return $thumbPath;			 				 
	}
	function getOriginal($ar)
	{
		$arrExpl				=	explode("/",$ar);
		$thumbPath			  =	$arrExpl[0].'/original/'.$arrExpl[1];	
		return $thumbPath;			 				 
	}
	function currencies(){
			$arry			   =	array('$'=>"USD",'EURO'=>"EURO",'AED'=>"AED");
			return $arry;
	}
	function format_number($number) {
		if($number >= 1000) {
		   $number = $number/1000;   // NB: you will want to round this
		   $number = sprintf("%0.2f",$number);
		   return ($number+0).' K';
		}
		else {
			return $number;
		}
	}
	function url_encryption($val){
		return base64_encode($val);
	}
	function url_decryption($val){
		return base64_decode($this->esc($val));
	}
	public function pushNotification($notiTo,$notiFrom,$notiType,$notiImg,$notiDescr,$notiUrl){
			global $objUsers;
			$notiTime					  =	date('Y-m-d H:i:s');
			if($notiTo !== $notiFrom){
				$objUsers->build_result_insert("INSERT INTO notifications (notification_id, notification_to, notification_from, notification_type, notification_descr, notification_img, notification_redirect_url, notification_time, notification_read_status) VALUES (NULL,'".$notiTo."', '".$notiFrom."', '".$notiType."', '".$notiDescr."', '".$notiImg."', '".$notiUrl."', '".$notiTime."', '0')");
				
			}
	}
}
?>
