<?php
include_once("db_functions.php");
class promotion_based_duration extends db_functions{
     var $tablename = "promotion_based_duration";
     var $primaryKey = "pbd_id";
     var $table_fields = array("pbd_id"=>"","pbd_name"=>"","pbd_days"=>"","pbd_profile"=>"","pbd_video"=>"","pbd_music"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>