<?php
include_once("db_functions.php");
class images_view extends db_functions
{
     var $tablename = "images_view";
     var $primaryKey = "iv_id";
     var $table_fields = array("iv_id"=>"","iv_cat"=>"","iv_content"=>"","iv_user_id"=>"","iv_visit"=>"","iv_created"=>"");

     function __construct()
     {
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>