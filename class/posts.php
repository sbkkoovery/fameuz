<?php
include_once("db_functions.php");
class posts extends db_functions 
{
	var $tablename			= 	"posts";
	var $primaryKey		   = 	"post_id";
	var $table_fields	  	 =   array('post_id'=>"",'user_id' => "","post_descr"=>'',"post_privacy"=>'',"post_created"=>'',"post_edited"=>'',"edited_status"=>'',"post_status"=>'');
	function posts()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>