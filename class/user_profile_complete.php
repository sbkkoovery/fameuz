<?php
include_once("db_functions.php");
class user_profile_complete extends db_functions{
     var $tablename = "user_profile_complete";
     var $primaryKey = "complete_id";
     var $table_fields = array("complete_id"=>"","user_id"=>"","complete_primary"=>"","complete_personal"=>"","complete_public"=>"","complete_profile_img"=>"","complete_social"=>'',"complete_created"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>