<?php
include_once("db_functions.php");
class country extends db_functions 
{
	var $tablename			= 	"country";
	var $primaryKey			= 	"country_id";
	var $table_fields	  	=   array('country_id' => "","country_code"=>'',"country_name"=>'');
	function country()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>