<?php
include_once("db_functions.php");
class following extends db_functions 
{
	var $tablename			= 	"following";
	var $primaryKey		   = 	"follow_id";
	var $table_fields	  	 =   array('follow_id'=>"",'follow_user1' => "","follow_user2"=>'',"follow_status"=>'',"follow_created"=>'');
	function following()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>