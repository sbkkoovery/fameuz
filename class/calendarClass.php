<?php
//$_SERVER[
@session_start();
/**
*@author  Xu Ding
*@email   thedilab@gmail.com
*@website http://www.StarTutorial.com
**/

include_once(DIR_ROOT."class/users.php");
include_once(DIR_ROOT."class/book_model.php");
$objUsers					=	new users();
$objBookmodel				=	new book_model();
class CalendarPlus {  
     
    /**
     * Constructor
     */
    public function __construct(){     
        $this->naviHref = SITE_ROOT."user/user-calendar";
    }
     
    /********************* PROPERTY ********************/  
    public $dayLabels = array("Mon","Tue","Wed","Thu","Fri","Sat","Sun");
     
    public $currentYear=0;
     
    public $currentMonth=0;
     
    public $currentDay=0;
     
    public $currentDate=null;
     
    public $daysInMonth=0;
     
    public $naviHref= null;
    public $selDay=0;
    /********************* PUBLIC **********************/  
        
    /**
    * print out the calendar
    */
    public function show() {
        $year  == null;
         
        $month == null;
         
        if(null==$year&&isset($_GET['year'])){
 
            $year = $_GET['year'];
         
        }else if(null==$year){
 
            $year = date("Y",time());  
         
        }          
         
        if(null==$month&&isset($_GET['month'])){
 
            $month = $_GET['month'];
         
        }else if(null==$month){
 
            $month = date("m",time());
         
        }                  
        if(null==$selDate&&isset($_GET['sel_date'])){
 
            $selDate = $_GET['sel_date'];
         
        }else if(null==$month){
 
            $selDate = date("d",time());
         
        }  
        $this->currentYear=$year;
         
        $this->currentMonth=$month;
         
        $this->daysInMonth=$this->_daysInMonth($month,$year);  
        $content='<div id="calendar">'.
                        '<div class="box">'.
                        $this->_createNavi().
                        '</div>'.
                        '<div class="box-content">'.
                                '<ul class="label">'.$this->_createLabels().'</ul>';   
                                $content.='<div class="clear"></div>';     
                                $content.='<ul class="dates">';    
                                 
                                $weeksInMonth = $this->_weeksInMonth($month,$year);
                                // Create weeks in a month
                                for( $i=0; $i<$weeksInMonth; $i++ ){
                                     
                                    //Create days in a week
                                    for($j=1;$j<=7;$j++){
                                        $content.=$this->_showDay($i*7+$j);
                                    }
                                }
                                 
                                $content.='</ul>';
                                 
                                $content.='<div class="clear"></div>';     
             
                        $content.='</div>';
                 
        $content.='</div>';
        return $content;   
    }
     
    /********************* PRIVATE **********************/ 
    /**
    * create the li element for ul
    */
    public function _showDay($cellNumber){
         
        if($this->currentDay==0){
             
            $firstDayOfTheWeek = date('N',strtotime($this->currentYear.'-'.$this->currentMonth.'-01'));
                     
            if(intval($cellNumber) == intval($firstDayOfTheWeek)){
                 
                $this->currentDay=1;
                 
            }
        }
         
        if( ($this->currentDay!=0)&&($this->currentDay<=$this->daysInMonth) ){
             
            $this->currentDate = date('Y-m-d',strtotime($this->currentYear.'-'.$this->currentMonth.'-'.($this->currentDay)));
             
            $cellContent = $this->currentDay;
             
            $this->currentDay++;   
             
        }else{
             
            $this->currentDate =null;
 
            $cellContent=null;
        }
        //$objCalendar	=	new calendar();
		//$calendarDetails	=	$objCalendar->listQuery("SELECT cr.*,c.c_title,c.c_alias FROM calendar AS cr LEFT JOIN courses AS c ON cr.c_id=c.c_id WHERE cr.cr_date='".$this->currentDate."'");
		$dateSel		 =	($this->currentDate==date("Y-m-d"))?"activeday ":" ";
         
        global $objUsers,$objCommon;
		if($this->currentDate!=""){
			$friendPermission	=	"'1,0,0,0','1,0,0,1','1,0,1,0','1,0,1,1','1,1,0,0','1,1,0,1','1,1,1,0','1,1,1,1','0,1,0,0','0,1,0,1','0,1,1,0','0,1,1,1'";
			$followPermission	=	"'1,0,0,0','1,0,0,1','1,0,1,0','1,0,1,1','1,1,0,0','1,1,0,1','1,1,1,0','1,1,1,1'";
			$calendarQuery	  =	"SELECT * FROM (
			SELECT
			user.user_id,user.first_name,user.last_name,user.display_name,book.bm_company,book.bm_work,book.book_to AS endDate, book.bm_country AS location,'booking' AS calCat
			FROM book_model AS book 
			LEFT JOIN users AS user ON book.agent_id=user.user_id 
			WHERE book.model_id=".$_SESSION['userId']." AND book.bm_model_status=1 AND DATE(book.book_from)='".$this->currentDate."'
			UNION
			SELECT
			r.remind_id AS user_id,u.first_name,u.last_name,u.display_name,r.remind_title AS bm_company ,r.remind_description AS bm_work,'NA' AS endDate,r.remind_place AS location,'reminder' AS calCat
			FROM reminders AS r
			LEFT JOIN users AS u ON r.user_id=u.user_id
			WHERE r.user_id=".$_SESSION['userId']." AND r.remind_date='".$this->currentDate."'
			UNION
			SELECT
			u.user_id,u.first_name,u.last_name,u.display_name,'NA' AS bm_company ,'NA' AS bm_work,up.p_dob AS endDate,'NA' AS location,'birthday' AS calCat
			FROM users AS u
			LEFT JOIN  personal_details as up ON u.user_id=up.user_id
			LEFT JOIN following AS flw ON u.user_id=flw.follow_user1 OR u.user_id=flw.follow_user2
			LEFT JOIN user_privacy as pr ON u.user_id=pr.user_id
			WHERE flw.follow_status=2 AND pr.uc_p_dob IN(".$friendPermission.") AND (flw.follow_user1=".$_SESSION['userId']." OR flw.follow_user2=".$_SESSION['userId'].") AND MONTH( up.p_dob)='".$this->currentMonth."' AND DAYOFMONTH( up.p_dob)='".$cellContent."'
			UNION
			SELECT
			u.user_id,u.first_name,u.last_name,u.display_name,'NA' AS bm_company ,'NA' AS bm_work,up.p_dob AS endDate,'NA' AS location,'birthday' AS calCat
			FROM users AS u
			LEFT JOIN  personal_details as up ON u.user_id=up.user_id
			LEFT JOIN following AS flw ON u.user_id=flw.follow_user1 OR u.user_id=flw.follow_user2
			LEFT JOIN user_privacy as pr ON u.user_id=pr.user_id
			WHERE flw.follow_status=1 AND pr.uc_p_dob IN(".$followPermission.") AND flw.follow_user1=".$_SESSION['userId']." AND MONTH( up.p_dob)='".$this->currentMonth."' AND DAYOFMONTH( up.p_dob)='".$cellContent."'
			
			) AS calTab WHERE 1
			";
			$calendarDetails	=	$objUsers->listQuery($calendarQuery." LIMIT 1");
		}
		$showDetailsFunction	=	(count($calendarDetails)>0)?' onclick="showDetails(\''.$this->currentDate.'\')"':'';
        $outPutDates	.=	'<li id="li-'.$this->currentDate.'" class="'.$dateSel.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
                ($cellContent==null?'mask':'').'"'.$showDetailsFunction.'><span class="calContent"><span class="courseDay">'.$cellContent.'</span></span>';
				if(count($calendarDetails)&&$this->currentDate!=""){
					foreach($calendarDetails as $calenderContent){
						$name	=	$objCommon->displayName($calenderContent);
						if($calenderContent['calCat']=='birthday'){
							$outPutDates	.=	'<span class="eventTitle"><b>'.$name.'</b> <br />Birthday <i style="color:#F99E2B" class="fa fa-birthday-cake"></i><br /></span>';
						}else if($calenderContent['calCat']=='booking'){
							$outPutDates	.=	'<span class="eventTitle"><b>'.$calenderContent['bm_work'].'</b> <br />Booking <i style="color:#F99E2B" class="fa fa-bullhorn"></i></span>';
						}else  if($calenderContent['calCat']=='reminder'){
							$outPutDates	.=	'<a href="javascript:;" class="editArrow" onclick="showOptions(this);"><i class="fa fa-angle-down"></i></a><span class="eventTitle"><b>'.$calenderContent['bm_company'].'</b> <br /><span style="font-size:11px;">Reminder</span> <i style="color:#F99E2B" class="fa fa-clock-o"></i></span>
							
							';
							/*$outPutDates	.=	'<a href="javascript:;" class="editArrow" onclick="showOptions(this);"><i class="fa fa-angle-down"></i></a><span class="eventTitle"><b>'.$calenderContent['bm_company'].'</b> <br /><span style="font-size:11px;">Reminder</span> <i style="color:#F99E2B" class="fa fa-clock-o"></i></span>
							<div class="dropdown_option_unfriend">
								<div class="menu-drop">
									<a href="javascript:void(0);" data-toggle="modal" data-target="#editReminder" class="unfriend has-spinner" data-friend="" onclick="editReminder('.$calenderContent['user_id'].');">Edit<span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span></a>
									<a href="'.SITE_ROOT.'access/delete_reminder.php?remindId='.$calenderContent['user_id'].'" class="cancel" onclick="return confirm(\'You want to delete..?\');">Delete</a>
								</div>
							</div>
							';*/
						}
					}
				}
				?>
                
				<?php
				/*if(count($calendarDetails)>0){
					foreach($calendarDetails as $currDeatil){
						$courseTitle	=	($currDeatil['cr_title']!='')?$currDeatil['cr_title']:$currDeatil['c_title'];
						$outPutDates	.=	'<span class="calendarCourse"><span class="courseTime">'.date("h:i A",strtotime($currDeatil['cr_fromtime'])).' to '.date("h:i A",strtotime($currDeatil['cr_totime'])).'</span><a class="courseName" href="'.SITE_ROOT.'register/course/'.$currDeatil['c_alias'].'/course_date/'.$this->currentDate.'">'.$courseTitle.'</a></span>';
					}
				}*/
		$outPutDates	.=	'</li>';
		return $outPutDates;
    }
     
    /**
    * create navigation
    */
    public function _createNavi(){
         
        $nextMonth = $this->currentMonth==12?1:intval($this->currentMonth)+1;
         
        $nextYear = $this->currentMonth==12?intval($this->currentYear)+1:$this->currentYear;
         
        $preMonth = $this->currentMonth==1?12:intval($this->currentMonth)-1;
         
        $preYear = $this->currentMonth==1?intval($this->currentYear)-1:$this->currentYear;
         
        return
            '<div class="header">'.
                '<a class="prev" href="'.$this->naviHref.'/'.sprintf('%02d',$preMonth).'/'.$preYear.'/"><i class="fa fa-angle-left"></i>
</a>'.
                    '<span class="title">'.date('F Y',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span>'.
                '<a class="next" href="'.$this->naviHref.'/'.sprintf("%02d", $nextMonth).'/'.$nextYear.'/"><i class="fa fa-angle-right"></i>
</a>'.
            '</div>';
    }
         
    /**
    * create calendar week labels
    */
    public function _createLabels(){  
                 
        $content='';
         
        foreach($this->dayLabels as $index=>$label){
             
            $content.='<li class="'.($label==6?'end title':'start title').' title">'.$label.'</li>';
 
        }
         
        return $content;
    }
     
     
     
    /**
    * calculate number of weeks in a particular month
    */
    public function _weeksInMonth($month=null,$year=null){
         
        if( null==($year) ) {
            $year =  date("Y",time()); 
        }
         
        if(null==($month)) {
            $month = date("m",time());
        }
         
        // find number of days in this month
        $daysInMonths = $this->_daysInMonth($month,$year);
         
        $numOfweeks = ($daysInMonths%7==0?0:1) + intval($daysInMonths/7);
         
        $monthEndingDay= date('N',strtotime($year.'-'.$month.'-'.$daysInMonths));
         
        $monthStartDay = date('N',strtotime($year.'-'.$month.'-01'));
         
        if($monthEndingDay<$monthStartDay){
             
            $numOfweeks++;
         
        }
         
        return $numOfweeks;
    }
 
    /**
    * calculate number of days in a particular month
    */
    public function _daysInMonth($month=null,$year=null){
         
        if(null==($year))
            $year =  date("Y",time()); 
 
        if(null==($month))
            $month = date("m",time());
             
        return date('t',strtotime($year.'-'.$month.'-01'));
    }
     
}
?>