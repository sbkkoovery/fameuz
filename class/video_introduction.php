<?php
include_once("db_functions.php");
class video_introduction extends db_functions{
     var $tablename = "video_introduction";
     var $primaryKey = "vi_id";
     var $table_fields = array("vi_id"=>"","user_id"=>"","vi_url"=>"","vi_thumb"=>"","vi_type"=>"","vi_created_date"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>