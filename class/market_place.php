<?php
include_once("db_functions.php");
class market_place extends db_functions 
{
	var $tablename			= 	"market_place";
	var $primaryKey		   = 	"mp_id";
	var $table_fields	  	 =    array('mp_id'=>"",'user_id' => "","mp_title"=>'','mp_alias'=>"","mp_type"=>'',"mp_image"=>'',"mp_duration"=>'',"mp_actual_price"=>'',"mp_dis_price"=>'',"mp_description"=>'',"mp_show_from"=>'',"mp_show_to"=>'',"mp_locations"=>'',"mp_created_date"=>'',"mp_edited_date"=>'',"mp_status"=>'');
	function market_place()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>