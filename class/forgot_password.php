<?php
include_once("db_functions.php");
class forgot_password extends db_functions{
     var $tablename = "forgot_password";
     var $primaryKey = "fp_id";
     var $table_fields = array("fp_id"=>"","user_id"=>"","fp_string"=>"");
     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>