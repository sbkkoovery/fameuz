<?php
include_once("db_functions.php");
class music_vote extends db_functions{
     var $tablename = "music_vote";
     var $primaryKey = "mv_id";
     var $table_fields = array("mv_id"=>"","music_id"=>"","user_id"=>"","mv_vote"=>"","mv_created"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>