<?php
include_once("db_functions.php");
class model_details extends db_functions 
{
	var $tablename			= 	"model_details";
	var $primaryKey			= 	"model_id";
	var $table_fields	  	=   array('model_id'=>"",'user_id' => "","mw_id"=>'',"mt_id"=>'',"ms_id"=>'',"mj_id"=>'',"mhp_id"=>'',"mh_id"=>'',"me_id"=>'',"mcollar_id"=>'',"mc_id"=>'',"mhair_id"=>'',"mdress_id"=>'',"model_dis_id"=>'');
	function model_details()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>