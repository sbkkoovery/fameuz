<?php
include_once("db_functions.php");
class game_descr_images extends db_functions{
     var $tablename = "game_descr_images";
     var $primaryKey = "gdi_id";
     var $table_fields = array("gdi_id"=>"","g_id"=>"","gdi_url"=>"");
     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>