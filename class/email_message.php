<?php
include_once("db_functions.php");
class email_message extends db_functions{
     var $tablename = "email_message";
     var $primaryKey = "em_id";
     var $table_fields = array("em_id"=>"","em_category"=>"","em_subject"=>"","em_message"=>"","em_replyof"=>"","em_createdon"=>"","em_status"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>