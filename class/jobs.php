<?php
include_once("db_functions.php");
class jobs extends db_functions 
{
	var $tablename			= 	"jobs";
	var $primaryKey		   = 	"job_id";
	var $table_fields	  	 =   array('job_id'=>"",'user_id' => "","job_title"=>'',"job_country"=>'',"job_state"=>'',"job_city"=>'',"job_company"=>'',"job_start"=>'',"job_end"=>'',"job_skill"=>'',"job_category"=>'',"job_descr"=>'',"job_image"=>'',"job_payment"=>'',"job_created"=>'',"job_edited"=>'',"job_status"=>'');
	function jobs()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>