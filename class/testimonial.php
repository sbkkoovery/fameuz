<?php
include_once("db_functions.php");
class testimonial extends db_functions 
{
	var $tablename			= 	"testimonial";
	var $primaryKey		   = 	"testi_id";
	var $table_fields	  	 =   array('testi_id'=>"","fullname"=>'',"profession"=>'',"place"=>'',"text"=>'',"t_image"=>'');
	function testimonial()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>