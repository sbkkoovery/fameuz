<?php
include_once("db_functions.php");
class promotion_based_category extends db_functions{
     var $tablename = "promotion_based_category";
     var $primaryKey = "pbc_id";
     var $table_fields = array("pbc_id"=>"","pbc_profile"=>"","pbc_video"=>"","pbc_music"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>