<?php
include_once("db_functions.php");
class face_month_members extends db_functions{
     var $tablename = "face_month_members";
     var $primaryKey = "fmm_id";
     var $table_fields = array("fmm_id"=>"","fmm_cat"=>"","fmm_members"=>"");
     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>