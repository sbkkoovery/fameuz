<?php
include_once("db_functions.php");
class email_message_users extends db_functions{
     var $tablename = "email_message_users";
     var $primaryKey = "emu_id";
     var $table_fields = array("emu_id"=>"","em_id"=>"","emu_from"=>"","emu_to"=>"","emu_read_status"=>"","emu_delete_from"=>"","emu_delete_to"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>