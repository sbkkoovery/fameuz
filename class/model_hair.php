<?php
include_once("db_functions.php");
class model_hair extends db_functions 
{
	var $tablename			= 	"model_hair";
	var $primaryKey		   = 	"mhair_id";
	var $table_fields	  	 =   array('mhair_id' => "","mhair_name"=>'');
	function model_hair()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>