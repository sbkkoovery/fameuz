<?php
include_once("db_functions.php");
class model_jacket extends db_functions 
{
	var $tablename			= 	"model_jacket";
	var $primaryKey		   = 	"mj_id";
	var $table_fields	  	 =   array('mj_id' => "","mj_name"=>'');
	function model_jacket()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>