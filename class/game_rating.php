<?php
include_once("db_functions.php");
class game_rating extends db_functions{
     var $tablename = "game_rating";
     var $primaryKey = "gr_id";
     var $table_fields = array("gr_id"=>"","user_id"=>"","g_id"=>"","gr_rating"=>"","gr_created"=>"");
     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>