<?php
include_once("db_functions.php");
class album_images extends db_functions 
{
	var $tablename			= 	"album_images";
	var $primaryKey		   = 	"ai_id";
	var $table_fields	  	 =   array('ai_id'=>"",'a_id' => "",'user_id'=>"",'ai_images'=>"",'ai_caption'=>"",'ai_set_main'=>"",'ai_like_count'=>"",'ai_comment_count'=>"",'ai_created'=>"",'ai_edited'=>"");
	function album_images()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>