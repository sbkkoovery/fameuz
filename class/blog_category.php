<?php
include_once("db_functions.php");
class blog_category extends db_functions 
{
	var $tablename			= 	"blog_category";
	var $primaryKey		   = 	"bc_id";
	var $table_fields	  	 =   array('bc_id'=>"",'bc_title' => "",'bc_alias'=>"",'bc_order'=>"");
	function blog_category()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>