<?php
include_once("db_functions.php");
class user_categories extends db_functions 
{
	var $todays_date;
	var $tablename			= 	"user_categories";
	var $primaryKey			= 	"uc_id";
	var $table_fields	  	=   array('uc_id' => "","user_id"=>'',"uc_m_id"=>'',"uc_c_id"=>'',"uc_s_id"=>'');
	function user_categories()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>