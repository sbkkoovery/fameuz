<?php
include_once("db_functions.php");
class category extends db_functions 
{
	var $tablename			= 	"category";
	var $primaryKey			= 	"c_id";
	var $table_fields	  	=   array('c_id'=>"",'mc_id' => "","c_name"=>'',"c_alias"=>'',"c_book_by_me"=>'',"c_book_me"=>'',"c_model_details_yes"=>'');
	function category()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>