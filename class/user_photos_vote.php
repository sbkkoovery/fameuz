<?php
include_once("db_functions.php");
class user_photos_vote extends db_functions 
{
	var $tablename			= 	"user_photos_vote";
	var $primaryKey		   = 	"up_id";
	var $table_fields	  	 =   array('up_id'=>"",'photo_id' => "","user_id"=>'',"up_vote"=>'',"up_created"=>'');
	function user_photos_vote()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>