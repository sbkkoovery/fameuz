<?php
include_once("db_functions.php");
class game_category extends db_functions 
{
	var $tablename			= 	"game_category";
	var $primaryKey		   = 	"gc_id";
	var $table_fields	  	 =   array('gc_id'=>"",'gc_name' => "","gc_alias"=>'');
	function game_category()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>