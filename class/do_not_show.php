<?php
include_once("db_functions.php");
class do_not_show extends db_functions{
     var $tablename = "do_not_show";
     var $primaryKey = "dns_id";
     var $table_fields = array("dns_id"=>"","dns_type"=>"","user_id"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>