<?php
include_once("db_functions.php");
class job_applied extends db_functions{
     var $tablename = "job_applied";
     var $primaryKey = "ja_id";
     var $table_fields = array("ja_id"=>"","job_id"=>"","ja_applied_by"=>"","ja_view_status"=>'',"ja_created"=>"");
     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>