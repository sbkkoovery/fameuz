<?php
include_once("db_functions.php");
class users extends db_functions 
{
	var $todays_date;
	var $tablename			= 	"users";
	var $primaryKey			= 	"user_id";
	var $table_fields	  	=   array('user_id' => "","first_name"=>'',"last_name"=>'',"display_name"=>'',"email"=>'',"password"=>'',"login_type"=>'',"profile_image"=>'',"created_on"=>'',"edited_on"=>'',"email_validation"=>'',"status"=>'');

	function users()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>