<?php
include_once("db_functions.php");
class album extends db_functions 
{
	var $tablename			= 	"album";
	var $primaryKey		   = 	"a_id";
	var $table_fields	  	 =   array('a_id'=>"",'user_id' => "","a_name"=>'',"a_caption"=>'',"a_created"=>'',"a_edited"=>'');
	function album()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>