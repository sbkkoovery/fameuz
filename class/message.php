<?php
include_once("db_functions.php");
class message extends db_functions{
     var $tablename = "message";
     var $primaryKey = "msg_id";
     var $table_fields = array("msg_id"=>"","msg_from"=>"","msg_to"=>"","msg_descr"=>"","msg_photos"=>"","msg_created_date"=>"","msg_edited_date"=>"","msg_send_status"=>"","msg_read_status"=>"","msg_trash_from"=>"","msg_trash_to"=>"","msg_status"=>"");
     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>