<?php
include_once("db_functions.php");
class face_month_category extends db_functions{
     var $tablename = "face_month_category";
     var $primaryKey = "fmc_id";
     var $table_fields = array("fmc_id"=>"","fmc_category"=>"");
     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>