<?php
include_once("db_functions.php");
class message_attachement extends db_functions{
     var $tablename = "message_attachement";
     var $primaryKey = "ma_id";
     var $table_fields = array("ma_id"=>"","msg_id"=>"","ma_attachement"=>"","ma_attach_type"=>"");
     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>