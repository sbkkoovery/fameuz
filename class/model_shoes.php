<?php
include_once("db_functions.php");
class model_shoes extends db_functions 
{
	var $tablename			= 	"model_shoes";
	var $primaryKey		   = 	"ms_id";
	var $table_fields	  	 =   array('ms_id' => "","ms_name"=>'');
	function model_shoes()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>