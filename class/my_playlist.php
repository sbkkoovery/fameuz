<?php
include_once("db_functions.php");
class my_playlist extends db_functions{
     var $tablename = "my_playlist";
     var $primaryKey = "myp_id";
     var $table_fields = array("myp_id"=>"","user_id"=>"","music_id"=>"","myp_created"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>