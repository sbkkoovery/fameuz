<?php
include_once("db_functions.php");
class model_collar extends db_functions 
{
	var $tablename			= 	"model_collar";
	var $primaryKey			= 	"mcollar_id";
	var $table_fields	  	=   array('mcollar_id' => "","mcollar_name"=>'');
	function model_collar()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>