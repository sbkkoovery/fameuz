<?php
include_once("db_functions.php");
class ethnicity extends db_functions 
{
	var $tablename			= 	"ethnicity";
	var $primaryKey		   = 	"ethnicity_id";
	var $table_fields	  	 =   array('ethnicity_id'=>"","ethnicity_name"=>'');
	function ethnicity()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>