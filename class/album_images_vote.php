<?php
include_once("db_functions.php");
class album_images_vote extends db_functions
{
     var $tablename = "album_images_vote";
     var $primaryKey = "aiv_id";
     var $table_fields = array("aiv_id"=>"","ai_id"=>"","user_id"=>"","aiv_vote"=>"","aiv_created"=>"");

     function __construct()
     {
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>