<?php
include_once("db_functions.php");
class main_category extends db_functions 
{
	var $tablename			= 	"main_category";
	var $primaryKey			= 	"mc_id";
	var $table_fields	  	=   array('mc_id' => "","mc_name"=>'');
	function main_category()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>