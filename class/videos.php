<?php
include_once("db_functions.php");
class videos extends db_functions 
{
	var $tablename			= 	"videos";
	var $primaryKey		   = 	"video_id";
	var $table_fields	  	 =    array('video_id'=>"",'video_encr_id'=>"",'user_id' => "","video_url"=>'',"video_thumb"=>'',"video_title"=>'',"video_descr"=>'',"video_tags"=>'',"video_type"=>'',"video_duration"=>'',"video_visits"=>'',"video_likes"=>'',"video_created"=>'',"video_updated"=>'',"video_privacy"=>'',"video_status"=>'');
	function videos()
	{
		 parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>