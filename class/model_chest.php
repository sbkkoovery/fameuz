<?php
include_once("db_functions.php");
class model_chest extends db_functions 
{
	var $tablename			= 	"model_chest";
	var $primaryKey			= 	"mc_id";
	var $table_fields	  	=   array('mc_id' => "","mc_name"=>'');
	function model_chest()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>