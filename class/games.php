<?php
include_once("db_functions.php");
class games extends db_functions 
{
	var $tablename			= 	"games";
	var $primaryKey		   = 	"g_id";
	var $table_fields	  	 =   array('g_id'=>"",'gc_id' => "","g_name"=>'','g_alias'=>"","g_descr"=>'',"g_file"=>'',"g_img"=>'',"g_banner"=>'',"g_created"=>'',"g_status"=>'');
	function games()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>