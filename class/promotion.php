<?php
include_once("db_functions.php");
class promotion extends db_functions{
     var $tablename = "promotion";
     var $primaryKey = "promo_id";
     var $table_fields = array("promo_id"=>"","user_id"=>"","promo_type"=>"","promo_duration"=>"","promo_country"=>"","promo_category"=>"","promo_content_id"=>"","promo_start_date"=>"","promo_end_date"=>"","promo_created_date"=>"","promo_admin_status"=>"","promo_status"=>"");
     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>