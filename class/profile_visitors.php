<?php
include_once("db_functions.php");
class profile_visitors extends db_functions 
{
	var $tablename			= 	"profile_visitors";
	var $primaryKey		   = 	"visitor_id";
	var $table_fields	  	 =   	array('visitor_id'=>"",'visited_me' => "","visited_by"=>'',"visited_time"=>'');
	function profile_visitors()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>