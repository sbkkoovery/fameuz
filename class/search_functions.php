<?php
class search_functions
{
	function filterSearchKeys($query){
		$query = trim(preg_replace("/(\s+)+/", " ", $query));
		$words = array();
		// expand this list with your words.
		$list = array("in","it","a","the","of","or","I","you","he","me","us","they","she","to","but","that","this","those","then","+");
		$c = 0;
		foreach(explode(" ", $query) as $key){
			if (in_array($key, $list)){
				continue;
			}
			$words[] = $key;
			if ($c >= 15){
				break;
			}
			$c++;
		}
		return $words;
	}
	// limit words number of characters
	function limitChars($query, $limit = 200){
		return substr($query, 0,$limit);
	}
	function search($query,$cond=NULL){
		$query = trim($query);
		if (mb_strlen($query)===0){
			// no need for empty search right?
			return false; 
		}
		$query = $this->limitChars($query);
		// Weighing scores
		$scoreFullTitle = 6;
		$scoreTitleKeyword = 5;
		$scoreFullSummary = 5;
		$scoreSummaryKeyword = 4;
		$scoreFullDocument = 4;
		$scoreDocumentUser = 3;
		$scoreCategoryKeyword = 2;
		$scoreUrlKeyword = 1;
		$keywords = $this->filterSearchKeys($query);
		$escQuery = $query; // see note above to get db object
		$titleSQL = array();
		$sumSQL = array();
		$userSQL = array();
		$categorySQL = array();
		$urlSQL = array();
		/** Matching full occurences **/
		if (count($keywords) > 1){
			$titleSQL[] = "if (video_title LIKE '%".$escQuery."%',{$scoreFullTitle},0)";
			//$sumSQL[] = "if (video_title LIKE '%".$escQuery."%',{$scoreFullSummary},0)";
			$userSQL[] = "if (user.first_name LIKE '%".$escQuery."%' OR user.email LIKE '%".$escQuery."%' OR social.usl_fameuz LIKE '%".$escQuery."%' OR user.display_name LIKE '%".$escQuery."%',{$scoreFullDocument},0)";
		}
		/** Matching Keywords **/
		foreach($keywords as $key){
			$titleSQL[] = "if (video_title LIKE '%".$key."%',{$scoreTitleKeyword},0)";
			//$sumSQL[] = "if (video_title LIKE '%".$key."%',{$scoreSummaryKeyword},0)";
			$userSQL[] = "if (video_title LIKE '%".$key."%',{$scoreDocumentUser},0)";
			$urlSQL[] = "if (user.first_name LIKE '%".$key."%' OR user.email LIKE '%".$key."%' OR social.usl_fameuz LIKE '%".$key."%' OR user.display_name LIKE '%".$key."%',{$scoreUrlKeyword},0)";
		   /* $categorySQL[] = "if ((
			SELECT count(category.tag_id)
			FROM category
			JOIN post_category ON post_category.tag_id = category.tag_id
			WHERE post_category.post_id = p.post_id
			AND category.name = '".$key."'
						) > 0,{$scoreCategoryKeyword},0)";*/
		}
		$sql = "SELECT vid.video_id,vid.video_url,video_encr_id,vid.video_thumb,vid.video_duration,vid.video_type,vid.video_title,vid.video_descr,vid.video_visits,vid.video_likes,vid.video_created,user.first_name,user.user_id,user.last_name,user.display_name,user.email,social.usl_fameuz,cat.c_name,profileImg.upi_img_url,avg(vote.vv_vote) as voteAvg,
				(
					(
					".implode(" + ", $titleSQL)."
					)+
					(
					".implode(" + ", $userSQL)."
					)+
					(
					".implode(" + ", $urlSQL)."
					)
				) as relevance
				FROM videos AS vid
				LEFT JOIN users AS user ON  vid.user_id = user.user_id
				LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
				LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
				LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
				LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id
				LEFT JOIN videos_vote AS vote ON vid.video_id	=	vote.video_id ";
		$sql	.=	" WHERE vid.video_status=1 AND user.status=1 AND user.email_validation =1 ";
		$sql	.=	($cond)?$cond:'';
		$sql	.=	" GROUP BY vid.video_id ";
		$sql	.=	" HAVING relevance > 0 ORDER BY relevance DESC,vid.video_created DESC";
		$results = $sql;
		return $results;
	}
}
?>