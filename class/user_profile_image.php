<?php
include_once("db_functions.php");
class user_profile_image extends db_functions 
{
	var $tablename			= 	"user_profile_image";
	var $primaryKey		   = 	"upi_id";
	var $table_fields	  	 =   array('upi_id'=>"",'user_id' => "","upi_img_url"=>'',"upi_like_count"=>'',"upi_comment_count"=>'',"upi_status"=>'',"upi_created"=>'',"upi_edited"=>'');
	function user_profile_image()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>