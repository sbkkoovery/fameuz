<?php
include_once("db_functions.php");
class model_trousers extends db_functions 
{
	var $tablename			= 	"model_trousers";
	var $primaryKey		   = 	"mt_id";
	var $table_fields	  	 =   array('mt_id' => "","mt_name"=>'');
	function model_trousers()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>