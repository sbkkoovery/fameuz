<?php
include_once("db_functions.php");
class comments extends db_functions 
{
	var $tablename			= 	"comments";
	var $primaryKey		   = 	"comment_id";
	var $table_fields	  	 =   array('comment_id'=>"",'comment_cat' => "","comment_content"=>'',"comment_descr"=>'',"comment_user_to"=>'',"comment_user_by"=>'',"comment_reply_id"=>'',"comment_time"=>'',"comment_status"=>'');
	function comments()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>