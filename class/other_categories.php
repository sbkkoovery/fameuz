<?php
include_once("db_functions.php");
class other_categories extends db_functions
{
	var $tablename = "other_categories";
	var $primaryKey = "other_id";
	var $table_fields = array("other_id"=>"","user_id"=>"","other_name"=>"","other_parent"=>"","other_created_on"=>"","other_status"=>"","other_status_desc"=>"");

	function __construct()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}
}
?>