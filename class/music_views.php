<?php
include_once("db_functions.php");
class music_views extends db_functions{
     var $tablename = "music_views";
     var $primaryKey = "mv_id";
     var $table_fields = array("mv_id"=>"","music_id"=>"","user_id"=>"","mv_visit"=>"","mv_created"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>