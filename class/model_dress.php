<?php
include_once("db_functions.php");
class model_dress extends db_functions 
{
	var $tablename			= 	"model_dress";
	var $primaryKey		   = 	"mdress_id";
	var $table_fields	  	 =    array('mdress_id' => "","mdress_name"=>'');
	function model_dress()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>