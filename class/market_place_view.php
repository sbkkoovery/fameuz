<?php
include_once("db_functions.php");
class market_place_view extends db_functions{
     var $tablename = "market_place_view";
     var $primaryKey = "mpv_id";
     var $table_fields = array("mpv_id"=>"","mp_id"=>"","user_id"=>"","mpv_created"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>