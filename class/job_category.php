<?php
include_once("db_functions.php");
class job_category extends db_functions 
{
	var $tablename			= 	"job_category";
	var $primaryKey		   = 	"jc_id";
	var $table_fields	  	 =   array('jc_id'=>"",'jc_name' => "",'jc_order'=>"");
	function job_category()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>