<?php
include_once("db_functions.php");
class promotion_payment extends db_functions{
     var $tablename = "promotion_payment";
     var $primaryKey = "payment_id";
     var $table_fields = array("payment_id"=>"","promo_id"=>"","txn_id"=>"","payment_amount"=>"","payment_currency"=>"","payment_created"=>"","payment_staus"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>