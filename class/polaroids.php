<?php
include_once("db_functions.php");
class polaroids extends db_functions{
     var $tablename = "polaroids";
     var $primaryKey = "polo_id";
     var $table_fields = array("polo_id"=>"","user_id"=>"","polo_url"=>"","polo_descr"=>"","polo_order"=>"","polo_like_count"=>"","polo_comment_count"=>"","polo_created"=>"","polo_edited"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>