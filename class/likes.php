<?php
include_once("db_functions.php");
class likes extends db_functions 
{
	var $tablename			= 	"likes";
	var $primaryKey		   = 	"like_id";
	var $table_fields	  	 =   array('like_id'=>"",'like_cat' => "","like_content"=>'',"like_user_id"=>'',"like_img_user_by"=>'',"like_time"=>'',"like_status"=>'');
	function likes()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>