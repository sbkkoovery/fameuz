<?php
include_once("db_functions.php");
class user_reviews extends db_functions 
{
	var $tablename			= 	"user_reviews";
	var $primaryKey		   = 	"review_id";
	var $table_fields	  	 =   array('review_id'=>"",'user_id_to' => "","user_id_by"=>'',"review_msg"=>'',"review_rate"=>'',"review_relation"=>'',"review_date"=>'',"review_status"=>'',"review_user_to_status"=>'');
	function user_reviews()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>