<?php
include_once("db_functions.php");
class languages extends db_functions 
{
	var $tablename			= 	"languages";
	var $primaryKey		   = 	"languages_id";
	var $table_fields	  	 =   array('languages_id'=>"",'languages_name'=>"");
	function languages()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>