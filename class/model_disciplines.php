<?php
include_once("db_functions.php");
class model_disciplines extends db_functions 
{
	var $tablename			= 	"model_disciplines";
	var $primaryKey		   = 	"model_dis_id";
	var $table_fields	  	 =    array('model_dis_id' => "","model_dis_name"=>'');
	function model_disciplines()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>