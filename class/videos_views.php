<?php
include_once("db_functions.php");
class videos_views extends db_functions 
{
	var $tablename			= 	"videos_views";
	var $primaryKey		   = 	"vvs_id";
	var $table_fields	  	 =   array('vvs_id'=>"",'video_id' => "","user_id"=>'',"vvs_visit"=>'',"vvs_created"=>'');
	function videos_views()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>