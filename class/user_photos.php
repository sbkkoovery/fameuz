<?php
include_once("db_functions.php");
class user_photos extends db_functions 
{
	var $tablename			= 	"user_photos";
	var $primaryKey		   = 	"photo_id";
	var $table_fields	  	 =   array('photo_id'=>"",'user_id' => "","photo_url"=>'',"photo_descr"=>'',"post_id"=>'',"photo_set_main"=>'',"photo_like_count"=>'',"photo_comment_count"=>'',"photo_created"=>'',"photo_edited"=>'');
	function user_photos()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>