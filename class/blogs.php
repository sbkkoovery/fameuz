<?php
include_once("db_functions.php");
class blogs extends db_functions 
{
	var $tablename			= 	"blogs";
	var $primaryKey		   = 	"blog_id";
	var $table_fields	  	 =   array('blog_id'=>"",'bc_id'=>"",'blog_title' => "","blog_alias"=>'',"user_id"=>'',"blog_descr"=>'',"blog_img"=>'',"created_time"=>'',"edited_time"=>'',"blog_status"=>'');
	function blogs()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>