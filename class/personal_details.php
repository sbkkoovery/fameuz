<?php
include_once("db_functions.php");
class personal_details extends db_functions 
{
	var $tablename			 = 	"personal_details";
	var $primaryKey			= 	"p_id";
	var $table_fields	  	  =    array('p_id'=>"",'user_id' => "","p_dob"=>'',"p_gender"=>'',"p_phone"=>'',"p_alt_phone"=>'',"p_alt_email"=>'',"p_about"=>'',"p_ethnicity"=>'',"p_languages"=>'',"n_id"=>'',"p_country"=>'',"p_city"=>'');
	function personal_details()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>