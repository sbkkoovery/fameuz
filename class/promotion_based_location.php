<?php
include_once("db_functions.php");
class promotion_based_location extends db_functions{
     var $tablename = "promotion_based_location";
     var $primaryKey = "pbl_id";
     var $table_fields = array("pbl_id"=>"","pbl_profile"=>"","pbl_video"=>"","pbl_music"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>