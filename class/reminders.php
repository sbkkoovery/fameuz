<?php
include_once("db_functions.php");
class reminders extends db_functions
{
     var $tablename = "reminders";
     var $primaryKey = "remind_id";
     var $table_fields = array("remind_id"=>"","user_id"=>"","remind_title"=>"","remind_description"=>"","remind_place"=>"","remind_date"=>"","remind_time"=>"","remind_status"=>"");

     function __construct()
     {
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>