<?php
include_once("db_functions.php");
class game_like extends db_functions{
     var $tablename = "game_like";
     var $primaryKey = "gl_id";
     var $table_fields = array("gl_id"=>"","user_id"=>"","g_id"=>"","gl_status"=>"","gl_created"=>"");
     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>