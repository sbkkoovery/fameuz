<?php
include_once("db_functions.php");
class music_like extends db_functions{
     var $tablename = "music_like";
     var $primaryKey = "ml_id";
     var $table_fields = array("ml_id"=>"","user_id"=>"","music_id"=>"","ml_status"=>"","ml_created"=>"");
     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>