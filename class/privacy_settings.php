<?php
include_once("db_functions.php");
class privacy_settings extends db_functions{
     var $tablename = "privacy_settings";
     var $primaryKey = "ps_id";
     var $table_fields = array("ps_id"=>"","user_id"=>"","ps_value"=>"");
     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>