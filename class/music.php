<?php
include_once("db_functions.php");
class music extends db_functions{
     var $tablename = "music";
     var $primaryKey = "music_id";
     var $table_fields = array("music_id"=>"","user_id"=>"","music_title"=>"","music_artist"=>"","music_descr"=>"","music_thumb"=>"","music_type"=>"","music_url"=>"","music_tags"=>"","music_privacy"=>"","music_created"=>"","music_edited"=>"","music_status"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>