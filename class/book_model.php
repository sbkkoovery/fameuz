<?php
include_once("db_functions.php");
class book_model extends db_functions 
{
	var $tablename			= 	"book_model";
	var $primaryKey		   = 	"bm_id";
	var $table_fields	  	 =   array('bm_id'=>"",'model_id' => "","agent_id"=>'',"book_from"=>'',"book_to"=>'',"bm_title"=>'',"bm_company"=>'',"bm_work"=>'',"bm_country"=>'',"bm_budget"=>'',"bm_email"=>'',"bm_phone"=>'',"bm_attachement"=>'',"bm_allowance"=>'',"bm_descr"=>'',"bm_created"=>'',"bm_model_status"=>'',"bm_agent_status"=>'',"bm_read_status"=>'');
	function book_model()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>