<?php
include_once("db_functions.php");
class user_fb_details extends db_functions 
{
	var $tablename			= 	"user_fb_details";
	var $primaryKey			= 	"fb_user_id";
	var $table_fields	  	=   array('fb_user_id' =>"",'fb_id'=>"",'user_id'=>"","Fullname"=>'');

	function user_fb_details()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>