<?php
include_once("db_functions.php");
class email_message_attachments extends db_functions{
     var $tablename = "email_message_attachments";
     var $primaryKey = "ema_id";
     var $table_fields = array("ema_id"=>"","em_id"=>"","ema_file"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>