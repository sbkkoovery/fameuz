<?php
include_once("db_functions.php");
class invite_games extends db_functions{
     var $tablename = "invite_games";
     var $primaryKey = "ig_id";
     var $table_fields = array("ig_id"=>"","g_id"=>"","ig_from"=>"","ig_to"=>"","ig_created"=>"","ig_noti_status"=>"");
     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>