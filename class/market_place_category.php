<?php
include_once("db_functions.php");
class market_place_category extends db_functions 
{
	var $tablename			= 	"market_place_category";
	var $primaryKey		   = 	"mpc_id";
	var $table_fields	  	 =   array('mpc_id'=>"",'mpc_name' => "","mpc_alias"=>'');
	function market_place_category()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>