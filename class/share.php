<?php
include_once("db_functions.php");
class share extends db_functions{
     var $tablename = "share";
     var $primaryKey = "share_id";
     var $table_fields = array("share_id"=>"","user_by"=>"","user_whose"=>"","share_content"=>"","share_category"=>"","share_created"=>"","share_status"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>