<?php
include_once("db_functions.php");
class user_chat_status extends db_functions 
{
	var $tablename			= 	"user_chat_status";
	var $primaryKey		   = 	"ucs_id";
	var $table_fields	  	 =   array('ucs_id'=>"",'user_id' => "","ucs_last_seen"=>'',"ucs_status"=>'',"ucs_online_status"=>'');
	function user_chat_status()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>