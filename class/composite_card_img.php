<?php
include_once("db_functions.php");
class composite_card_img extends db_functions{
     var $tablename = "composite_card_img";
     var $primaryKey = "cci_id";
     var $table_fields = array("cci_id"=>"","user_id"=>"","cci_pos1"=>"","cci_pos2"=>"","cci_pos3"=>"","cci_pos4"=>"","cci_created"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>