<?php
include_once("db_functions.php");
class composite_card_data extends db_functions{
     var $tablename = "composite_card_data";
     var $primaryKey = "ccd_id";
     var $table_fields = array("ccd_id"=>"","user_id"=>"","ccd_data_table"=>"");

     function __construct(){
          parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
     }
}
?>