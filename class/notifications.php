<?php
include_once("db_functions.php");
class notifications extends db_functions 
{
	var $tablename			= 	"notifications";
	var $primaryKey		   = 	"notification_id";
	var $table_fields	  	 =   array('notification_id'=>"",'notification_to' => "","notification_from"=>'',"notification_type"=>'',"notification_descr"=>'',"notification_img"=>'',"notification_redirect_url"=>'',"notification_time"=>'',"notification_read_status"=>'');
	function notifications()
	{
		parent::db_functions($this->tablename, $this->primaryKey, $this->table_fields);
	}	
}
?>