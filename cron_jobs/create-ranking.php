<?php
include_once("../class/ranking.php");
include_once("../class/users.php");
include_once("../class/common_class.php");
$objUsers			  		=	new users();
$objRanking				  =	new ranking();
$objCommon		 		   =	new common();
ini_set('max_execution_time', 300);
$getAllRankMembers		   =	$objRanking->getRowSql("SELECT GROUP_CONCAT(user_id) AS rank_user_id FROM ranking");
$rank_user_idArr			 =	array();
if($getAllRankMembers['rank_user_id']){
	$rank_user_idArr		 =	explode(",",$getAllRankMembers['rank_user_id']);
}
$totalLikeCountSQl		   =	$objUsers->getRowSql("SELECT COUNT(like_id) AS likeCount FROM likes WHERE like_status=1");
$totalLikeCount			  =	$totalLikeCountSQl['likeCount'];
$totalbookingAcceptedSql	 =	$objUsers->getRowSql("SELECT COUNT(bm_id) AS bookedCount FROM book_model WHERE bm_model_status=1");
$totalbookingAccepted		=	$totalbookingAcceptedSql['bookedCount'];
//-------------------------------------main cat rank for model--------------------------------------
$getMainCatRank			  =	$objUsers->listQuery("SELECT tab1.user_id,tab1.finalCount,COUNT(tab2.finalCount) AS Rank FROM 
(
	SELECT tab.user_id,COALESCE((((tab.countLike*100)/".$totalLikeCount.")+tab.reviewAvg),0) AS finalCount FROM(
	SELECT ucat.user_id,COALESCE((AVG(reviews.review_rate)*20),0) AS reviewAvg,COALESCE(lk.likeCount,0) AS countLike
	FROM user_categories AS ucat
	LEFT JOIN user_reviews AS reviews ON ucat.user_id = reviews.user_id_to
	LEFT JOIN user_profile_image as profileImg ON ucat.user_id = profileImg.user_id AND profileImg.upi_status=1
	LEFT JOIN (SELECT likes.like_img_user_by,count(likes.like_id) AS likeCount FROM user_categories AS ucat  LEFT JOIN likes  ON ucat.user_id = likes.like_img_user_by WHERE likes.like_status=1 GROUP BY ucat.user_id) AS lk
	ON ucat.user_id = lk.like_img_user_by 
	WHERE ucat.uc_m_id =2  
	GROUP BY ucat.user_id
	) AS tab ORDER BY finalCount DESC 
) AS tab1,(
	SELECT tab.user_id,COALESCE((((tab.countLike*100)/".$totalLikeCount.")+tab.reviewAvg),0) AS finalCount FROM(
	SELECT ucat.user_id,COALESCE((AVG(reviews.review_rate)*20),0) AS reviewAvg,COALESCE(lk.likeCount,0) AS countLike
	FROM user_categories AS ucat
	LEFT JOIN user_reviews AS reviews ON ucat.user_id = reviews.user_id_to
	LEFT JOIN user_profile_image as profileImg ON ucat.user_id = profileImg.user_id AND profileImg.upi_status=1
	LEFT JOIN (SELECT likes.like_img_user_by,count(likes.like_id) AS likeCount FROM user_categories AS ucat  LEFT JOIN likes  ON ucat.user_id = likes.like_img_user_by WHERE likes.like_status=1 GROUP BY ucat.user_id) AS lk
	ON ucat.user_id = lk.like_img_user_by 
	WHERE ucat.uc_m_id =2  
	GROUP BY ucat.user_id
	) AS tab ORDER BY finalCount DESC 
) AS tab2
WHERE tab1.finalCount < tab2.finalCount OR (tab1.finalCount=tab2.finalCount AND tab1.user_id = tab2.user_id) 
GROUP BY tab1.user_id, tab1.finalCount 
ORDER BY tab1.finalCount DESC, tab1.user_id DESC;
");
foreach($getMainCatRank as $allMainCatRank){
	if(in_array($allMainCatRank['user_id'],$rank_user_idArr)){
		$objRanking->updateField(array("main_cat_rank"=>$allMainCatRank['Rank']),"user_id=".$allMainCatRank['user_id']);
	}else{
		$_POST['main_cat_rank']			=	$allMainCatRank['Rank'];
		$_POST['user_id']				  =	$allMainCatRank['user_id'];
		$objRanking->insert($_POST);					
	}
}
//-------------------------------------main cat rank for agent--------------------------------------
$getMainCatRankAgents			  =	$objUsers->listQuery("SELECT tab1.user_id,tab1.finalCount,COUNT(tab2.finalCount) AS Rank FROM 
(
	SELECT tab.user_id,COALESCE((((tab.countLike*100)/".$totalLikeCount.")+tab.reviewAvg+((tab.countBook*100)/".$totalbookingAccepted.")),0) AS finalCount FROM(
	SELECT ucat.user_id,COALESCE((AVG(reviews.review_rate)*20),0) AS reviewAvg,COALESCE(lk.likeCount,0) AS countLike,COALESCE(bk.bookCount,0) As countBook
	FROM user_categories AS ucat
	LEFT JOIN user_reviews AS reviews ON ucat.user_id = reviews.user_id_to
	LEFT JOIN user_profile_image as profileImg ON ucat.user_id = profileImg.user_id AND profileImg.upi_status=1
	LEFT JOIN (SELECT likes.like_img_user_by,count(likes.like_id) AS likeCount FROM user_categories AS ucat  LEFT JOIN likes  ON ucat.user_id = likes.like_img_user_by WHERE likes.like_status=1 GROUP BY ucat.user_id) AS lk
	ON ucat.user_id = lk.like_img_user_by 
	LEFT JOIN (SELECT book.agent_id,count(book.bm_id) AS bookCount FROM user_categories AS ucat  LEFT JOIN book_model AS book  ON ucat.user_id = book.agent_id WHERE book.bm_model_status=1 GROUP BY ucat.user_id) AS bk
	ON ucat.user_id = bk.agent_id 
	WHERE ucat.uc_m_id =1  
	GROUP BY ucat.user_id
	) AS tab ORDER BY finalCount DESC 
) AS tab1,(
	SELECT tab.user_id,COALESCE((((tab.countLike*100)/".$totalLikeCount.")+tab.reviewAvg+((tab.countBook*100)/".$totalbookingAccepted.")),0) AS finalCount FROM(
	SELECT ucat.user_id,COALESCE((AVG(reviews.review_rate)*20),0) AS reviewAvg,COALESCE(lk.likeCount,0) AS countLike,COALESCE(bk.bookCount,0) As countBook
	FROM user_categories AS ucat
	LEFT JOIN user_reviews AS reviews ON ucat.user_id = reviews.user_id_to
	LEFT JOIN user_profile_image as profileImg ON ucat.user_id = profileImg.user_id AND profileImg.upi_status=1
	LEFT JOIN (SELECT likes.like_img_user_by,count(likes.like_id) AS likeCount FROM user_categories AS ucat  LEFT JOIN likes  ON ucat.user_id = likes.like_img_user_by WHERE likes.like_status=1 GROUP BY ucat.user_id) AS lk
	ON ucat.user_id = lk.like_img_user_by 
	LEFT JOIN (SELECT book.agent_id,count(book.bm_id) AS bookCount FROM user_categories AS ucat  LEFT JOIN book_model AS book  ON ucat.user_id = book.agent_id WHERE book.bm_model_status=1 GROUP BY ucat.user_id) AS bk
	ON ucat.user_id = bk.agent_id 
	WHERE ucat.uc_m_id =1  
	GROUP BY ucat.user_id
	) AS tab ORDER BY finalCount DESC 
) AS tab2
WHERE tab1.finalCount < tab2.finalCount OR (tab1.finalCount=tab2.finalCount AND tab1.user_id = tab2.user_id) 
GROUP BY tab1.user_id, tab1.finalCount 
ORDER BY tab1.finalCount DESC, tab1.user_id DESC
");
foreach($getMainCatRankAgents as $allMainCatRankAgents){
	if(in_array($allMainCatRankAgents['user_id'],$rank_user_idArr)){
		$objRanking->updateField(array("main_cat_rank"=>$allMainCatRankAgents['Rank']),"user_id=".$allMainCatRankAgents['user_id']);
	}else{
		$_POST['main_cat_rank']			=	$allMainCatRankAgents['Rank'];
		$_POST['user_id']				  =	$allMainCatRankAgents['user_id'];
		$objRanking->insert($_POST);					
	}
}
//--------------------------------------------------------------------------------------------------
?>