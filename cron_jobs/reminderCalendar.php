<?php
include_once("../includes/site_root.php");
include_once("../class/reminders.php");
include_once('../PHPMailer/class.phpmailer.php');
$objReminder	   =	new reminders();
$mail              = 	new PHPMailer();

$tomorrow = date("Y-m-d", strtotime('tomorrow'));

$sqlInvite				   =	"SELECT remind.*,user.*
								   FROM reminders AS remind
								   LEFT JOIN users AS user ON remind.user_id = user.user_id
								   WHERE remind_date='".$tomorrow."'";
$getAllreminders			 =	$objReminder->listQuery($sqlInvite);
foreach($getAllreminders as $allreminder){
	$reminderTitle					 =	$allreminder['remind_title'];
	$reminderDescrip				   =	$allreminder['remind_description'];
	$userEmail                         =	$allreminder['email'];
	$firstna                           =    $allreminder['first_name'];
	$remId                             =    $allreminder['remind_id'];
	$body			   =	'<table style="width:100%;border-collapse:collapse"><tbody><tr><td style="font:14px/1.4285714 Arial,sans-serif;padding:10px 10px 0;background:#f5f5f5">
            <table style="width:100%;border-collapse:collapse"><tbody><tr><td style="font:14px/1.4285714 Arial,sans-serif;padding:0;background-color:#ffffff;border-radius:5px">
    <div style="border:1px solid #cccccc;border-radius:5px;padding:20px">
      <table style="width:100%;border-collapse:collapse"><tbody><tr><td style="font:14px/1.4285714 Arial,sans-serif;padding:0">
              <p style="margin-bottom:0;margin-top:0">
               Dear '.$firstna.' <br>
				  You are receiving this email because you have a reminder in <strong>'.$tomorrow.'.</strong><br>
			      <strong>'.$reminderTitle.'</strong><br>
				  '.$reminderDescrip.'<br>	  
                
              </p>
            </td>
          </tr></tbody></table></div>
  </td>
</tr></tbody></table></td>
        </tr></tbody></table>'; 
	$mail->CharSet = 'UTF-8';
	$mail->AddReplyTo('info@fameuz.com','fameuz.com');
	$mail->SetFrom('info@fameuz.com','fameuz.com');
	$mail->AddAddress($userEmail,$firstna);
	
	$mail->Subject    = "Famous Reminder Email";
	
	$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
	
	$mail->MsgHTML($body);
	$mail->Send();
	//echo '<div class="alert alert-success" role="alert">Your query has been sent</div>';
	$objReminder->updateField(array('remind_status'=>1),"remind_id=".$remId);
	
	
	
}
?>