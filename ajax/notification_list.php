<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/notifications.php");
$objCommon				 =	new common();
$objNotifications		  =	new notifications();
$sqlNotificationList	   =	"SELECT noti.notification_id,noti.notification_type,noti.notification_descr,noti.notification_redirect_url,noti.notification_time,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz,profileImg.upi_img_url FROM notifications AS noti LEFT JOIN users AS user ON noti.notification_from = user.user_id LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 WHERE noti.notification_to='".$_SESSION['userId']."' AND notification_type != '' ORDER BY noti.notification_time desc LIMIT 0,20";
$getNotificationList	   =	$objNotifications->listQuery($sqlNotificationList);
if(count($getNotificationList)>0){
		foreach($getNotificationList as $allNotificationList){
			if($allNotificationList['notification_type']=='worked_together'){
				$notification_redirect_url			=	SITE_ROOT.'user/notifications-worked-together';
			}else{
				$notification_redirect_url			=	$objCommon->html2text($allNotificationList['notification_redirect_url']);
			}
?>
<div class="worked_box item_review" id="item_review-<?php echo $allNotificationList['notification_id']?>">
	<div class="row"><div class="col-sm-1"><div class="image">
	<a href="<?php echo SITE_ROOT.$objCommon->html2text($allNotificationList['usl_fameuz'])?>"><img src="<?php echo SITE_ROOT?>uploads/profile_images/<?php echo($allNotificationList['upi_img_url'])?$allNotificationList['upi_img_url']:'profile_pic.jpg'?>" alt="profile_pic" class="img-responsive" /></a>
	</div></div>
	<div class="col-sm-11"><div class="text">
		<p><a href="<?php echo $notification_redirect_url?>"><?php echo $objCommon->html2text($allNotificationList['notification_descr']);?></a></p>
		<p class="time"><span><i class="fa fa-clock-o"></i><?php echo date("d M , Y h:i a",strtotime($objCommon->local_time($allNotificationList['notification_time'])));?></span></p>
	</div></div>
	<div class="closeNoti"><a href="javascript:;" data-notid="<?php echo $allNotificationList['notification_id']?>"><i class="fa fa-times"></i></a></div></div>
	<div class="clr"></div>
</div>
<?php
	}
}else{
	echo "<p>No notification found...</p>";
}
?>
<script>
$(".worked_box").on("mouseover",function(){
	$(".closeNoti").hide();
	$(this).find(".closeNoti").show();
});
$(".worked_box").on("mouseout",function(){
	$(".closeNoti").hide();
});
$(".closeNoti").on("click","a",function(){
	var notid			=	$(this).data("notid");
	$.ajax({
		url:'<?php echo SITE_ROOT?>access/delete_notification.php',
		data:{notid:notid},
		method:"POST",
		success:function(result){
			$(".notiList").load('<?php echo SITE_ROOT?>ajax/notification_list.php');
	}});
});
</script>