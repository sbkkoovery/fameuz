<?php
@session_start();
include_once("../includes/site_root.php");
?>
<link href="<?php echo SITE_ROOT ?>css/newUser_home.css" type="text/css" rel="stylesheet" />
<div class="row">
	<div class="col-sm-12 home_update_margin">
    	<div class="home_update">
            <div class="user_greetings">
            	<div class="greeting_head text-center">
                	<h5>WELCOME TO</h5>
                    <h2>FAMEUZ</h2>
                </div>
                <div class="greeting_desc text-center">
                	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
                </div>
            </div>
    	</div>
    </div>
    <div class="col-sm-12 home_update_margin">
    	<div class="home_update">
            <div class="friends_suggest">
            	<div class="friends_suggest text-center">
                	<h2>People You May Know</h2>
                    <h5>See all friends recommendations</h5>
                </div>
                <div class="friends_suggest_users">
                	<?php
						include_once(DIR_ROOT."widget/friend_suggest.php");
					?>
                </div>
            </div>
    	</div>
    </div>
</div>
