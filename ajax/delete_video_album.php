<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/videos.php");
	$objCommon				   =	new common();
	$objVideos			   	   =	new videos();
	$imgid				  	   =	$objCommon->esc($_GET['imgid']);
	$userId					  =	$_SESSION['userId'];
	if($imgid !='' && $userId!=''){
		$getImageName			=	$objVideos->getRow("video_id=".$imgid);
		if($getImageName['video_type']==1){
			$imgPath			 =	DIR_ROOT.'uploads/videos/'.$getImageName['video_url'];
			@unlink($imgPath);
		}
			$imgPath1				=	DIR_ROOT.'uploads/videos/'.$getImageName['video_thumb'];
			@unlink($imgPath1);
		$objVideos->delete("video_id=".$imgid);
	}
}
?>