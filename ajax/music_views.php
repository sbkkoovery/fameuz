<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/music_views.php");
	$objCommon				   =	new common();
	$objMusicViews			   =	new music_views();
	$music_id					=	$objCommon->esc($_POST['musicId']);
	$userId					  =	$_SESSION['userId'];
	if($userId !='' && $music_id != ''){
		$getMusicViews		   =	$objMusicViews->getRow("user_id=".$userId." AND music_id=".$music_id);
		if($getMusicViews['mv_id'] == ''){
			$_POST['user_id']	=	$userId;
			$_POST['music_id']   =	$music_id;
			$_POST['mv_visit']   =	1;
			$_POST['mv_created'] =	date("Y-m-d H:i:s");
			$objMusicViews->insert($_POST);
		}else{
			$mv_created 		  =	date("Y-m-d H:i:s");
			$objMusicViews->updateField(array('mv_created'=>$mv_created),"user_id = ".$userId." AND music_id=".$music_id);
		}
	}
}
?>