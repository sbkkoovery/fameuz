<?php
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/category.php");
$objCommon				=	new common();
$objCategory			  =	new category();
$main_cat			 	 =	$objCommon->esc($_GET['main_cat']);
$cat					  =	$objCommon->esc($_GET['cat']);
if($main_cat){
	$data				 =	'<option value="">Tell us who you are:</option>';
	
	$getCategoryAll	   =	$objCategory->listQuery("SELECT *
												       FROM category
													   WHERE mc_id =".$main_cat);
	
	
	foreach($getCategoryAll as $allGetCategory){
		$seletcat		 =	($cat==$allGetCategory['c_id'])?'selected="selected"':'';
        $data			.=	'<option value="'.$objCommon->html2text($allGetCategory['c_id']).'" '.$seletcat.' data-catname="'.$objCommon->html2text($allGetCategory['c_name']).'">'.$objCommon->html2text($allGetCategory['c_name']).'</option>';
	}
	echo $data;
}
?>