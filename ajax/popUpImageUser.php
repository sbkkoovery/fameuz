<?php 
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/album_images.php");
include_once(DIR_ROOT."class/images_view.php");
$objCommon			   =	new common();
$objAlbums			   =	new album_images();
$objImageView	 		=	new images_view();
$getUserDetails	  	  =	$objAlbums->getRowSql("SELECT profileImg.upi_img_url,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 WHERE user.status=1 AND user.user_id=".$_SESSION['userId']);
	$imageId		 	 =	$objCommon->esc($_GET['contentId']);
	$im_type		 	 =	$objCommon->esc($_GET['contentType']);
	$albumId			 =	($_GET['contentAlbum'])?$objCommon->esc($_GET['contentAlbum']):0;
	$uploadPath		  =	SITE_ROOT_AWS_IMAGES."uploads/albums/";
	if($im_type==1 || $im_type==10){
		$imageDetails    =	$objAlbums->getRowSql("SELECT photos.user_id,photos.photo_url AS img_url,photos.photo_id AS img_id,photos.photo_descr as img_descr,photos.photo_created AS img_created,photos.photo_set_main AS coverImg,IF(photos.photo_id != '', 1, '') 		AS img_type,photos.photo_like_count AS likeCount,photos.photo_comment_count AS commentCount,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,count(DISTINCT likes.like_id) AS likeCount,group_concat(DISTINCT likes.like_user_id) AS youLike,count(DISTINCT share.share_id) AS shareCount,group_concat(DISTINCT share.user_by) AS youShare
	FROM user_photos as photos
	LEFT  JOIN users AS user ON photos.user_id = user.user_id
	LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
	LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
	LEFT JOIN likes ON photos.photo_id = likes.like_content AND likes.like_cat =".$im_type." AND likes.like_status=1
	LEFT JOIN share  ON  photos.photo_id = share.share_content AND share.share_category = ".$im_type." AND share.share_status=1
	WHERE photos.photo_id=".$imageId);	
	}else if($im_type==2){
		$imageDetails    =	$objAlbums->getRowSql("SELECT photos.user_id,photos.ai_images AS img_url,ai_id AS img_id,photos.ai_caption AS img_descr,photos.ai_created AS img_created,photos.ai_set_main AS coverImg,IF(photos.ai_id != '', 2, '') AS img_type,photos.ai_like_count AS likeCount,photos.ai_comment_count AS commentCount,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,count(DISTINCT likes.like_id) AS likeCount,group_concat(DISTINCT likes.like_user_id) AS youLike,count(DISTINCT share.share_id) AS shareCount,group_concat(DISTINCT share.user_by) AS youShare
		FROM album_images  AS photos
		LEFT  JOIN users AS user ON photos.user_id = user.user_id
		LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
		LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
		LEFT JOIN likes ON photos.ai_id = likes.like_content AND likes.like_cat =".$im_type." AND likes.like_status=1
		LEFT JOIN share  ON photos.ai_id = share.share_content AND share.share_category = ".$im_type." AND share.share_status=1
		WHERE ai_id=".$imageId);
	}
	else if($im_type==8){
		$imageDetails    =	$objAlbums->getRowSql("SELECT photos.user_id,photos.polo_url AS img_url,polo_id AS img_id,photos.polo_descr AS img_descr,photos.polo_created AS img_created,IF(photos.polo_id != '', 8, '') AS img_type,photos.polo_like_count AS likeCount,photos.polo_comment_count AS commentCount,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,count(DISTINCT likes.like_id) AS likeCount,group_concat(DISTINCT likes.like_user_id) AS youLike,count(DISTINCT share.share_id) AS shareCount,group_concat(DISTINCT share.user_by) AS youShare
		FROM polaroids  AS photos
		LEFT  JOIN users AS user ON photos.user_id = user.user_id
		LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
		LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
		LEFT JOIN likes ON photos.polo_id = likes.like_content AND likes.like_cat =".$im_type." AND likes.like_status=1
		LEFT JOIN share  ON photos.polo_id = share.share_content AND share.share_category = ".$im_type." AND share.share_status=1
		WHERE polo_id=".$imageId);
	}
	else if($im_type==6){
		$imageDetails    =	$objAlbums->getRowSql("SELECT photos.user_id,photos.upi_img_url AS img_url,photos.upi_id AS img_id,'' AS img_descr,photos.upi_created AS img_created,IF(photos.upi_id != '', 6, '') AS img_type,photos.upi_like_count AS likeCount,photos.upi_comment_count AS commentCount,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,count(DISTINCT likes.like_id) AS likeCount,group_concat(DISTINCT likes.like_user_id) AS youLike,count(DISTINCT share.share_id) AS shareCount,group_concat(DISTINCT share.user_by) AS youShare
		FROM user_profile_image  AS photos
		LEFT  JOIN users AS user ON photos.user_id = user.user_id
		LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
		LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
		LEFT JOIN likes ON photos.upi_id = likes.like_content AND likes.like_cat =".$im_type." AND likes.like_status=1
		LEFT JOIN share  ON photos.upi_id = share.share_content AND share.share_category = ".$im_type." AND share.share_status=1
		WHERE photos.upi_id=".$imageId);
		$uploadPath		  =	SITE_ROOT_AWS_IMAGES."uploads/profile_images/";
	}
//---------------------------------------get images for sliders--------------
if($albumId !='' && $albumId != 0 && $albumId != -1 && $im_type != 8 && $im_type != 6){
	$getOtherPhotosSql				 	=	"SELECT group_concat(tabPhotos.img_id ORDER BY tabPhotos.img_created DESC) AS Ids,group_concat(tabPhotos.img_type ORDER BY tabPhotos.img_created DESC) AS Typs FROM(SELECT ai_id AS img_id,IF(ai_id != '', 2, '') AS img_type,ai_created AS img_created
		FROM album_images 
		WHERE user_id=".$imageDetails['user_id']." AND a_id=".$albumId."
		ORDER BY ai_created desc) AS tabPhotos ORDER BY tabPhotos.img_created DESC";
		$nextAlbumId					  =	$preAlbumId		=	$albumId;
}else if($albumId !='' && $albumId != 0 && $albumId == -1 && $im_type != 8  && $im_type != 6){
	$getOtherPhotosSql				 	=	"SELECT group_concat(tabPhotos.img_id ORDER BY tabPhotos.img_created DESC) AS Ids,group_concat(tabPhotos.img_type ORDER BY tabPhotos.img_created DESC) AS Typs FROM(SELECT photo_id AS img_id,IF(photo_id != '', 1, '') AS img_type,photo_created AS img_created
		FROM user_photos 
		WHERE user_id=".$imageDetails['user_id']." 
		ORDER BY photo_created DESC) AS tabPhotos ORDER BY tabPhotos.img_created DESC";

}else if($im_type==8){
	$getOtherPhotosSql				 	=	"SELECT group_concat(tabPhotos.img_id ORDER BY tabPhotos.img_created DESC) AS Ids,group_concat(tabPhotos.img_type ORDER BY tabPhotos.img_created DESC) AS Typs FROM(SELECT polo_id AS img_id,IF(polo_id != '', 8, '') AS img_type,polo_created AS img_created,polo_order
		FROM polaroids 
		WHERE user_id=".$imageDetails['user_id']." 
		ORDER BY polo_order) AS tabPhotos ORDER BY tabPhotos.polo_order";
}else if($im_type==6){
	$getOtherPhotosSql				 	=	"SELECT group_concat(tabPhotos.img_id ORDER BY tabPhotos.img_created DESC) AS Ids,group_concat(tabPhotos.img_type ORDER BY tabPhotos.img_created DESC) AS Typs FROM(SELECT upi_id AS img_id,IF(upi_id != '', 6, '') AS img_type,upi_created AS img_created
		FROM user_profile_image 
		WHERE user_id=".$imageDetails['user_id']." 
		ORDER BY upi_created desc) AS tabPhotos ORDER BY tabPhotos.img_created desc";
}else{
	$getOtherPhotosSql				 	=	"SELECT group_concat(tabPhotos.img_id ORDER BY tabPhotos.img_created DESC) AS Ids,group_concat(tabPhotos.img_type ORDER BY tabPhotos.img_created DESC) AS Typs FROM((SELECT photo_id AS img_id,IF(photo_id != '', 1, '') AS img_type,photo_created AS img_created
		FROM user_photos 
		WHERE user_id=".$imageDetails['user_id']." 
		ORDER BY photo_created DESC)
		UNION ALL
		(SELECT ai_id AS img_id,IF(ai_id != '', 2, '') AS img_type,ai_created AS img_created
		FROM album_images 
		WHERE user_id=".$imageDetails['user_id']."
		ORDER BY ai_created desc)) AS tabPhotos ORDER BY tabPhotos.img_created DESC";
}
$getAllOtherPhotos					=	$objAlbums->getRowSql($getOtherPhotosSql);
$idsArr							   =	$getAllOtherPhotos['Ids'];
$typsArr							  =	$getAllOtherPhotos['Typs'];
$idsArrExpl						   =	explode(",",$idsArr);
$typsArrExpl						  =	explode(",",$typsArr);
if(count($idsArrExpl)>0){
	$ImagArr						  =	array();
	foreach($idsArrExpl as $keyExpl=>$allArrExpl){
		array_push($ImagArr,$allArrExpl.','.$typsArrExpl[$keyExpl]);
	}
	$keyCurrent					   =	array_search($imageId.','.$im_type,$ImagArr);
	$keyNext						  =	$keyCurrent+1;
	$keyPrev						  =	$keyCurrent-1;
	if(array_key_exists($keyNext,$ImagArr)){
		$nextValue					=	$ImagArr[$keyNext];
	}else{
		$nextValue					=	'';
	}
	if(array_key_exists($keyPrev,$ImagArr)){
		$prevValue					=	$ImagArr[$keyPrev];
	}else{
		$prevValue					=	'';
	}
}
if(isset($_SESSION['userId'])){
	$currentView	=	$objImageView->countRows("SELECT iv_id FROM images_view WHERE iv_content=$imageId AND iv_user_id=".$_SESSION['userId']);
	if($currentView){
		$objImageView->updateField(array("iv_created"=>date("Y-m-d H:i:s")),"iv_content=$imageId AND iv_user_id=".$_SESSION['userId']);
	}else{
		$objImageView->insert(array("iv_cat"=>$im_type,"iv_content"=>$imageId,"iv_user_id"=>$_SESSION['userId'],"iv_visit"=>1,"iv_created"=>date("Y-m-d H:i:s")));
	}
}
//--------------------------------------------------------------------------------
$imageUrl			    =	$uploadPath.$imageDetails['img_url'];
$userImageCon			=	($imageDetails['upi_img_url'])?$imageDetails['upi_img_url']:'profile_pic.jpg';
$userImage			   =	SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.$userImageCon;
$limit				   =	260;
$img_descr			   =	$objCommon->html2text($imageDetails['img_descr']);
if(strlen($img_descr) > $limit){
	$img_descrSmall	  =	mb_substr($img_descr,0,$limit,'UTF-8')."...";
	$img_descrSmall	 .=	$img_descrSmall.'<p class="readP text-right"><a href="javascript:;" class="readMore">Read more</a></p>';
	$img_descrFull	   =	$img_descr;
}else{
	$img_descrSmall	  =	$img_descr;
	$img_descrFull	   =	$img_descrFull;
}
$userName				=	$objCommon->displayName($imageDetails);
$myImageCon			  =	($getUserDetails['upi_img_url'])?$getUserDetails['upi_img_url']:'profile_pic.jpg';
$myImage				 =	SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.$myImageCon;	
$imageCreatedOn		  =	date("d M , Y h:i a",strtotime($objCommon->local_time($imageDetails['img_created'])));
$imageExt				=	strtolower(pathinfo($imageUrl, PATHINFO_EXTENSION));
switch ($imageExt) { 
	case 'gif' : 
		$imagetrueUrl   = imagecreatefromgif($imageUrl);
	break; 
	case 'jpeg' : 
		$imagetrueUrl   = imagecreatefromjpeg($imageUrl);
	break; 
	case 'jpg' : 
		$imagetrueUrl   = imagecreatefromjpeg($imageUrl);
	break; 
	case 'png' : 
		$imagetrueUrl   = imagecreatefrompng($imageUrl);
	break; 
}
$imageHeight		   =	imagesy($imagetrueUrl);
$imageWidth			=	imagesx($imagetrueUrl);
if($prevValue !=''){
	$prevValueExpl	   =	explode(",",$prevValue);
	$imagePrevId	     =	$prevValueExpl[0];
	$imagePrevType	   =	$prevValueExpl[1];
}
if($nextValue !=''){
	$nextValueExpl	   =	explode(",",$nextValue);
	$imageNextId	     =	$nextValueExpl[0];
	$imageNextType	   =	$nextValueExpl[1];
}
$likeCount			   =	$imageDetails['likeCount'];
$youLikeArr	   		  =	explode(",",$imageDetails['youLike']);
if(count($youLikeArr)>0){
	if(in_array($_SESSION['userId'],$youLikeArr)){
		$youLike	  	 =	1; // you liked
	}else{
		$youLike	  	 =	0;
	}
}
$shareCount	  		  =	$imageDetails['shareCount'];
$youShareArr	  		 =	explode(",",$imageDetails['youShare']);
if(count($youShareArr)>0){
	if(in_array($_SESSION['userId'],$youShareArr)){
		$youShare 	    =	1; // you shared this
	}else{
		$youShare 		=	0;
	}
}
if($imageDetails['user_id'] !=$_SESSION['userId']){
	$getWorkedDetails	=	$objAlbums->getRowSql("SELECT worked_id,from_status,to_status FROM worked_together WHERE img_cat=".$im_type." and img_id =".$imageId." and send_from=".$_SESSION['userId']." and send_to=".$imageDetails['user_id']);
	if($getWorkedDetails['from_status']==1 && $getWorkedDetails['to_status']==1){
		$workedTogether  =	3; //request sent and accepted
	}else if($getWorkedDetails['from_status']==1){
		$workedTogether  =	2; //request sent
	}else{
		$workedTogether  =	1; //request not sent
	}
}else{
	$workedTogether	  =	0; // worked together not applicable
}
$commentCount			=	number_format($imageDetails['commentCount']);
$jsonArr				 =	array('userMe'=>$_SESSION['userId'],'imageUrl'=>$imageUrl,'userImage'=>$userImage,'imgDescrSmall'=>$img_descrSmall,'imgDescrFull'=>$img_descrFull,'userName'=>$userName,'imageCreatedOn'=>$imageCreatedOn,'imagePrevId'=>$imagePrevId,'imagePrevType'=>$imagePrevType,'imageNextId'=>$imageNextId,'imageNextType'=>$imageNextType,'imageAlbumId'=>$albumId,'imageHeight'=>$imageHeight,'userto'=>$imageDetails['user_id'],'myimage'=>$myImage,'youLike'=>$youLike,'likeCount'=>$likeCount,'youShare'=>$youShare,'shareCount'=>$shareCount,'workedTogether'=>$workedTogether,'commentCount'=>$commentCount,'imageWidth'=>$imageWidth,'coverImg'=>$imageDetails['coverImg']);
//echo "<pre>"; print_r($jsonArr); echo "</pre>";
echo json_encode($jsonArr);
exit;
?>