<?php
@session_start();
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/users.php");
	$objCommon				   =	new common();
	$objUsers				    =	new users();	
	$getPolaroids				  =	$objUsers->listQuery("SELECT polo_id,polo_url FROM polaroids WHERE user_id=".$_SESSION['userId']." ORDER BY polo_order ASC");

?>
<link href="<?php echo SITE_ROOT?>css/master.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.masonry.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery-migrate-1.2.0.min.js"></script>
<script type="text/javascript">
	$(function(){
		$(window).load(function() {$('.album_maso').masonry();});
			$(window).resize(function() {$('.album_maso').masonry();});
	});
	</script>
	<div class="polarids">
	    		<div class="ploarids-sec">
            	<div class="row">
				<div id="masonry" class="album_maso masonry" >
                <?php 
				if(count($getPolaroids) >0){
				foreach($getPolaroids as $allPolaroids){
				$orgImg = $objCommon->html2text($allPolaroids['polo_url']);
				$thumbImg = $objCommon->getThumb($orgImg);
				?>
                     <div class="album_maso_album_maso_box masonry-brick" style="display:inline-block; position:relative; vertical-align: top;" >
                      <input type="checkbox" class="polaroidselecteight" name="poloroidselect[]" value="<?php echo $allPolaroids['polo_id']?>" style="margin-left:50%;"/>
                    	<div class="polarid-items">
                        	<div class="polarid-img ">
                            	<a href="javascript:;" class="lightBoxs" data-contentId="<?php echo $allPolaroids['polo_id']?>" data-contentType="8"><img src="<?php echo SITE_ROOT?>uploads/albums/<?php echo $thumbImg;?>" /></a>
                            </div>
                            
                        </div>
                    </div>
                    <?php 
					}?>
                  					
				<?php }else{
					echo $objCommon->displayNameSmall($getMyFriendDetails,12).' has no Polaroids yet';
				}
				?>
				</div>
                </div>
            </div>
       </div>
       
        <div class="custom-upload text">
       <a href="#create_pdf_pop" onclick="makePdffrom();"><span class="upload-btn pull"><img src="<?php echo SITE_ROOT?>images/pdf.png"><span class="">Make PDF</span></span></a><span id="errorpol" style="display:none; margin-left:2%; color:#F00;">Please select 8 polaroids to make pdf!</span></div>

<?php }?>
<script type="text/javascript">
var limit = 8;
$('input.polaroidselecteight').on('change', function(evt) {
	var check = document.getElementsByName('poloroidselect[]');
    var selectedRows = [];
    for (var i = 0, l = check.length; i < l; i++) {
    if (check[i].checked) {
        selectedRows.push(check[i].value);
          
	}
    }
    if(selectedRows.length > limit) {
       this.checked = false;
   }
});

function makePdffrom(){
	var check = document.getElementsByName('poloroidselect[]');
    var selectedRows = [];
    for (var i = 0, l = check.length; i < l; i++) {
    if (check[i].checked) {
        selectedRows.push(check[i].value);
          
	}
    }
	if(selectedRows.length==8){
window.location.href	=	'<?php echo SITE_ROOT?>poloroid-print.php?poloid='+selectedRows;
	document.getElementById('errorpol').style.display="none";
	
	//document.getElementsByName('remodal-overlay').style.display="none";
	//document.getElementsByName('remodal-wrapper').style.display="none";
	}
	else
	{
		document.getElementById('errorpol').style.display="inline-block";
	}
	
}
</script>