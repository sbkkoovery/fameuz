<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/share.php");
	$objCommon				   =	new common();
	$objShare				 	=	new share();
	$shareId					 =	$objCommon->esc($_POST['share']);
	$blogby					  =	$objCommon->esc($_POST['blogby']);
	$userId					  =	$_SESSION['userId'];
	if($shareId != '' && $userId != '' && $blogby != ''){
		$_POST['share_content']  =	$shareId;
		$_POST['user_by']   		=	$userId;
		$_POST['user_whose']	 =	$blogby;
		$_POST['share_category'] =	7;
		$_POST['share_created']  =	date("Y-m-d H:i:s");
		$_POST['share_status']   =	1;
		$getShare				=	$objShare->getRow("share_content=".$shareId." and share_category=7 and user_by =".$userId);
		if($getShare['share_id']==''){
			$objShare->insert($_POST);
			echo '<a href="javascript:;" data-share="'.$shareId.'" class="shareBlog colorGreen" data-blogby="'.$blogby.'"><i class="fa fa-share-alt"></i>Shared</a>';
		}else{
			$share_created  		=	date("Y-m-d H:i:s");
			$objShare->updateField(array("share_created"=>$share_created),"share_id=".$getShare['share_id']);
			echo '<a href="javascript:;" data-share="'.$shareId.'" class="shareBlog colorGreen" data-blogby="'.$blogby.'"><i class="fa fa-share-alt"></i>Shared</a>';
		}
	}
}
?>