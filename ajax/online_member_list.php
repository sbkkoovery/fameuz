<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/users.php");
$objCommon				 =	new common();
$objUsers				  =	new users();
$perPage 				   = 	10;
$page 					  = 	1;
if(!empty($_GET["page"])) {
$page = $_GET["page"];
}
$sqlMyFollowers			=	"SELECT tab.* FROM (SELECT user.user_id,user.first_name,user.last_name,user.display_name,user.created_on,profileImg.upi_img_url,user.email,social.usl_fameuz,follow.follow_user1,follow.follow_user2,follow.follow_status,cat.c_name,case when (TIMESTAMPDIFF(MINUTE,chat.ucs_last_seen,'".date("Y-m-d H:i:s")."') >15 or chat.ucs_status=0) then '0' else '1' END AS chatStatus,personal.p_country,personal.p_city,personal.p_gender,
case when ((follow.follow_user1=".$_SESSION['userId']." or follow.follow_user2 =".$_SESSION['userId'].") and follow.follow_status=2) then  'friends' when (follow.follow_user1=".$_SESSION['userId']." and follow.follow_status=1) then 'following' when (follow.follow_user2=".$_SESSION['userId']." and follow.follow_status=1) then 'follower' else 'none' end as friendStatus,case when ((follow.follow_user1=".$_SESSION['userId']." or follow.follow_user2 =".$_SESSION['userId'].") and follow.follow_status=2) then  '3' when (follow.follow_user1=".$_SESSION['userId']." and follow.follow_status=1) then '2' when (follow.follow_user2=".$_SESSION['userId']." and follow.follow_status=1) then '1' else '0' end as friendStatusOrder,ucat.uc_c_id
FROM users AS user
LEFT JOIN user_chat_status AS chat ON user.user_id = chat.user_id 
LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
LEFT JOIN personal_details AS personal ON user.user_id=personal.user_id 
LEFT JOIN user_categories AS ucat ON user.user_id = ucat.user_id
LEFT JOIN category AS cat ON ucat.uc_c_id = cat.c_id
LEFT JOIN user_profile_image AS profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
LEFT JOIN following as follow  ON (user.user_id = follow.follow_user1 and follow.follow_user1 !=".$_SESSION['userId']." ) or (user.user_id = follow.follow_user2 and follow.follow_user2 !=".$_SESSION['userId']." ) 
WHERE user.status= 1 AND user.email_validation=1 AND user.user_id != ".$_SESSION['userId']." ORDER BY friendStatusOrder DESC
) AS tab  WHERE chatStatus=1   GROUP BY tab.user_id ORDER BY  tab.first_name";
$start = ($page-1)*$perPage;
if($start < 0) { $start = 0; }
$queryMyFollowers =  $sqlMyFollowers . " limit " . $start . "," . $perPage; 
if(empty($_GET["rowcount"])) {
	$_GET["rowcount"] = $countFollowers	=	$objUsers->countRows($sqlMyFollowers);
}
$pages  = ceil($_GET["rowcount"]/$perPage);
$getMyFollowers		=	$objUsers->listQuery($queryMyFollowers);
if($start < $countFollowers){
?>
									<?php
									if(count($getMyFollowers)>0){
										echo '<input type="hidden" class="pagenum" value="' . $page . '" /><input type="hidden" class="total-page" value="'.$pages.'" /><input type="hidden" id="total-count" value="'.$countFollowers.'" />';
										foreach($getMyFollowers as $keyFollowers=>$allFollowers){
											$allFollowersImg	=	SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.(($allFollowers['upi_img_url'])?$allFollowers['upi_img_url']:"profile_pic.jpg");
											if($allFollowers['display_name']){
												$allFollowersName	 =	$objCommon->html2text($allFollowers['display_name']);
											}else if($getUserDetails['first_name']){
												$allFollowersName	 =	$objCommon->html2text($allFollowers['first_name']);
											}else{
												$exploEmailVisi	  		 =	explode("@",$objCommon->html2text($allFollowers['email']));
												$allFollowersName	 =	$exploEmailVisi[0];
											}
											if($allFollowers['follow_status']==2){ 
												$friendStatus =2; 
											}else if($allFollowers['follow_status']==1 && $allFollowers['follow_user1']==$_SESSION['userId']){ 
												$friendStatus =1; 
											}else { 
												$friendStatus=0; 
											}
											if($allFollowers['p_city'] != '' || $allFollowers['p_country'] != ''){
												$friendPcity	=	($allFollowers['p_city'])?$objCommon->html2text($allFollowers['p_city'])." ,":"";
											}
									?>
									<li class="col-md-6">
										<div class="visitor_inner_box">
											<div class="image_thumb pull-left">
												 <a href="<?php echo SITE_ROOT.$objCommon->html2text($allFollowers['usl_fameuz'])?>" title="<?php echo $allFollowersName?>"><img src="<?php echo $allFollowersImg?>" class="img-responsive" alt="<?php echo $allFollowersName?>" title="<?php echo $allFollowersName?>" /></a>
											</div>
											<div class="visitor_box">
												<p class="name_head"><a href="<?php echo SITE_ROOT.$objCommon->html2text($allFollowers['usl_fameuz'])?>" title="<?php echo $allFollowersName?>"><?php echo $allFollowersName?></a></p>
												<p><i class="fa fa-user"></i> <?php echo $objCommon->html2text($allFollowers['c_name'])?></p>
												<p><i class="fa fa-globe"></i> <?php echo $friendPcity.' '.$objCommon->html2text($allFollowers['p_country'])?></p>
												<?php
												if($allFollowers['chatStatus']==1){
													echo '<p class="online_status">online</p>';
												}else{
													echo '<p class="offline_status">offline</p>';
												}
												?>
												<div class="follow_status_box">
													<div class="follow_status">
														<?php
														if($friendStatus==2){
														?>
														<a href="javascript:;" class="friend"><i class="fa fa-star-o"></i> Friend</a>
														<?php
														}else if($friendStatus==1){
														?>
														<a href="javascript:;" class="following"><i class="fa fa-check"></i> Following</a>
														<?php
														}else if($friendStatus==0){
														?>
														<a href="javascript:;" class="follow has-spinner" data-friendid="<?php echo $objCommon->html2text($allFollowers['user_id'])?>">
															<span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span>&nbsp;&nbsp;
															<i class="fa fa-plus"></i> Follow
														</a>
														<?php
														}
														?>
													</div>
												</div>
											</div>
                                            <div class="side_mark"></div>
										</div>
									</li>
									<?php
									/*if(($keyFollowers+1)%2==0){
											echo '<div class="clearfix "></div>';
									}*/
										}
									}
									?>
<script>
$('.follow').click(function(e) {
	var friendid		=	$(this).data('friendid');
	$(this).addClass('active');
	$(this).find('.fa-plus').hide();
	that	=	this;
	$.get('<?php echo SITE_ROOT?>ajax/sent_friend_request.php',{"friendId":friendid},function(data){
	  setTimeout(function () {
			$(that).removeClass('active');
			$(that).parent().html('<a href="javascript:;" class="friend"><i class="fa fa-star-o"></i> Friend</a>');
			
		},3000);
	});
});
</script>
<?php
}
?>