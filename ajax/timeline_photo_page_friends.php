<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/user_photos.php");
$objCommon				   =	new common();
$objUserPhotos			   =	new user_photos();
$friendId					=	$objCommon->esc($_GET['friendId']);
if($friendId !=''){
	$getTimeLinePhotos	   =	$objUserPhotos->listQuery("select photos.* from user_photos as photos  where photos.user_id=".$friendId." order by photos.photo_created	desc");
?>
<div class="album-head inner"> <img src="<?php echo SITE_ROOT?>images/gallery.png"><span class="head-album">Wall images</span> </div>
<?php
if(count($getTimeLinePhotos)>0){
?>
	<div class="row albListShow">
	<div class="col-md-12">
	<div class="add_photo_album_list">
		<ul class="album_list_ul">
			<?php
			foreach($getTimeLinePhotos as $allTimeLinePhotos){
				$getAiImages		=	$allTimeLinePhotos['photo_url'];
				list($widthImg,$heightImg,$typeImg,$attrImg) = getimagesize(DIR_ROOT."uploads/albums/".$allTimeLinePhotos['photo_url']);
				?>
			<li><div class="pic"><a href="javascript:;" class="lightBoxs" data-contentId="<?php echo $allTimeLinePhotos['photo_id']?>" data-contentType="1" data-contentAlbum="-1"><img src="<?php echo SITE_ROOT?>uploads/albums/<?php echo $objCommon->getThumb($allTimeLinePhotos['photo_url'])?>" class="img-responsive"  /></a></div>
			</li>
			<?php
			}
			?>
		</ul>
	</div>
	</div>
	</div>
	<?php
	}else{
		echo '<div class="row albListShow"><div class="col-md-12"><p class="no_img_album text-center">There are no photos</p></div></div>';
	}
}
?>
<script type="text/javascript">
$(document).ready(function(){
	 $(".lightBoxs").lightBox({
	  videoSkin: '<?php echo SITE_ROOT?>jw_player/six/six.xml',
	  photosObj: true,
	  videoObj :  true,
	  defaults : true,
	  album   : false,
	  deleteCommentUrl : '<?php echo SITE_ROOT ?>access/delete_comment.php',
	  skins : {
	   loader   : '<?php echo SITE_ROOT?>images/ajax-loader.gif',
	   next      : '<img src="<?php echo SITE_ROOT?>fameuz_lightbox/images/next.png">',
	   prev   : '<img src="<?php echo SITE_ROOT?>fameuz_lightbox/images/prev.png">',
	   dismiss     : '<img src="<?php echo SITE_ROOT?>fameuz_lightbox/images/close.png" width="15">',
	   reviewIcon  : '<i class="fa fa-chevron-right"></i>',
	  },
	  photos : {
	   fetchUrl: '<?php echo SITE_ROOT.'ajax/popUpImageUser.php'?>',
	   ajaxUrl : '<?php echo SITE_ROOT.'widget/pop_image_comment_box.php' ?>',
	   comment : {
		CommentFormAction : '<?php echo SITE_ROOT?>access/post_comment.php?action=add_comment',
		  commentLikeUrl   : '<?php echo SITE_ROOT?>ajax/like_comment.php',
	   },
	   shareItems : {
		likeAjaxUrl  : '<?php echo SITE_ROOT?>ajax/like.php',
		shareAjaxUrl : '<?php echo SITE_ROOT?>ajax/share.php',
		workedAjaxUrl: '<?php echo SITE_ROOT?>ajax/worked_together.php',
	   },
	  },
	  video : {
	   url  : '<?php echo SITE_ROOT?>uploads/testuploads/1.mp4',
	   fetchUrl: '<?php echo SITE_ROOT.'ajax/popUpVideoUser.php'?>',
	   ajaxUrl : '<?php echo SITE_ROOT.'widget/pop_video_comment_box.php' ?>',
	   comment : {
		CommentFormAction : '<?php echo SITE_ROOT?>access/post_video_comment.php?action=add_comment',
		commentLikeUrl   : '<?php echo SITE_ROOT?>ajax/like_video_comment_pop.php',
	   },
	   shareItems : {
		likeAjaxUrl  : '<?php echo SITE_ROOT?>ajax/like_video.php',
		shareAjaxUrl : '<?php echo SITE_ROOT?>ajax/share_video.php',
	   },
	  },
	  shareItems : {
	   likeAjaxUrl : '<?php echo SITE_ROOT?>ajax/like.php',
	   shareAjaxUrl: '<?php echo SITE_ROOT?>ajax/share.php',
	  }
	 });
});
</script>