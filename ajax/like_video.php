<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/likes.php");
	$objCommon				   =	new common();
	$ObjLikes					=	new likes();
	$likeId					  =	$objCommon->esc($_POST['likeId']);
	$likecat					 =	$objCommon->esc($_POST['likecat']);
	$imgUserId				   =	$objCommon->esc($_POST['imgUserId']);
	$userId					  =	$_SESSION['userId'];
	if($likecat != '' && $likeId != '' && $userId != ''){
		$_POST['like_cat']	   =	$likecat;
		$_POST['like_content']   =	$likeId;
		$_POST['like_user_id']   =	$userId;
		$_POST['like_img_user_by']=   $imgUserId;
		$_POST['like_time']	  =	date("Y-m-d H:i:s");
		$getLikes				=	$ObjLikes->getRow("like_content=".$likeId." and like_user_id =".$userId." and like_cat=".$likecat);
		$getVideoId			  =	$ObjLikes->getRowSql("SELECT video_encr_id FROM videos WHERE video_id =".$likeId);
		if($getLikes['like_id']==''){
			$_POST['like_status']=	1;
			$ObjLikes->insert($_POST);
			$ObjLikes->build_result_insert("update videos SET video_likes=video_likes+1 where video_id=".$likeId);
			//echo '<a href="javascript:;" class="share_heart1 likeVideo liked"  data-like="'.$likeId.'"><i class="fa fa-heart"></i>Liked</a>';
			echo 'liked';
			//----notification table------------------------------------
			$myDetails				   		   	   =	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$userId);
			$displayName						  	 =	$objCommon->displayName($myDetails);
			$friend_id							   =	$imgUserId;
			$notiType								=	'video_like';
			$notiImg								 =	'';
			$notiDescr  	 	 				   	   =	'<b>'.$displayName.'</b> liked your video.';
			$notiUrl  								 =	SITE_ROOT.'video/watch/'.$getVideoId['video_encr_id'];
			$objCommon->pushNotification($friend_id,$userId,$notiType,$notiImg,$notiDescr,$notiUrl);
			//----------------------------------------------------------
		}else{
			if($getLikes['like_status']==1){
				$upStatus		=	0;
				$ObjLikes->build_result_insert("update videos SET video_likes=video_likes-1 where video_id=".$likeId);
				//echo '<a href="javascript:;" class="share_heart1 likeVideo"  data-like="'.$likeId.'"><i class="fa fa-heart"></i>Like</a>';
				echo 'like';
			}else{
				$upStatus		=	1;
				$ObjLikes->build_result_insert("update videos SET video_likes=video_likes+1 where video_id=".$likeId);
				//echo '<a href="javascript:;" class="share_heart1 likeVideo liked"  data-like="'.$likeId.'"><i class="fa fa-heart"></i>Liked</a>';
				echo 'liked';
			}
			$ObjLikes->updateField(array("like_status"=>$upStatus),"like_id=".$getLikes['like_id']);
		}
	}
}
?>