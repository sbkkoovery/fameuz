<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/users.php");
$objCommon				 =	new common();
$objUsers				  =	new users();
$userId				    =	$_SESSION['userId'];
$getPolaroids				  =	$objUsers->listQuery("SELECT polo_id,polo_url FROM polaroids WHERE user_id=".$_SESSION['userId']." ORDER BY polo_order ASC");	
?> 
<form action="" method="post">
<div class="modal-body">
	<div class="row">
			<?php
		  if(count($getPolaroids) >0){
				foreach($getPolaroids as $allPolaroids){?>
				<div class="album_maso_album_maso_box masonry-brick" >
                    	<div class="polarid-items">
                        	<div class="polarid-img">
                            	<a href="javascript:;" class="lightBoxs" data-contentId="<?php echo $allPolaroids['polo_id']?>" data-contentType="8"><img src="<?php echo SITE_ROOT?>uploads/albums/<?php echo $objCommon->html2text($allPolaroids['polo_url'])?>" /></a>
                            </div>
                            <div class="col-md-6">
			<label class="modelDataLabel"><input type="checkbox" name="modelDataArr[]" value="<?php echo $allPolaroids['polo_id']?>"  /></label>
		</div>
                        </div>
                    </div>
                    <?php 
					}
				}else{
					echo $objCommon->displayNameSmall($getMyFriendDetails,12).' has no Polaroids yet';
				}
				?>
	</div>
     <a href="javascript:;" onclick="makePdf();"><span class="upload-btn pull-right"><img src="<?php echo SITE_ROOT?>images/pdf.png"><span class="">Make PDF</span></span></a>
</div>
<div class="modal-footer">
	<button type="submit" class="btn btn-primary">Save</button>
</div>
</form>
