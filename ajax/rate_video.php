<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/videos_vote.php");
	include_once(DIR_ROOT."class/notifications.php");
	$objCommon				   =	new common();
	$ObjVideosVote			   =	new videos_vote();
	$objNotifications			=	new notifications();
	$rate_val					=	$objCommon->esc($_POST['rate_val']);
	$vidId					   =	$objCommon->esc($_POST['vidId']);
	$userId					  =	$_SESSION['userId'];
	$getVidDetails			   =	$ObjVideosVote->getRowSql("select video_id from videos where video_encr_id ='".$vidId."'");
	$videoId					 =	$getVidDetails['video_id'];
	if($rate_val != '' && $videoId != '' && $userId != ''){
		$_POST['video_id']   	   =	$videoId;
		$_POST['user_id']   		=	$userId;
		$_POST['vv_vote']   		=	$rate_val;
		$_POST['vv_created']	 =	date("Y-m-d H:i:s");
		$getVotes				=	$ObjVideosVote->getRow("video_id=".$videoId." and user_id =".$userId);
		if($getVotes['vv_id'] == ''){
			$ObjVideosVote->insert($_POST);
		}
	}
	$getAvg					 =	$ObjVideosVote->getRowSql("SELECT AVG(vv_vote) AS avg_rate FROM videos_vote WHERE video_id=".$videoId." ");
}
?>
<i class="fa fa-thumbs-o-up"></i>
<?php
$vidRateCound			=	$objCommon->round_to_nearest_half($getAvg['avg_rate']);
$vidRateCoundStyle	   =	'style="width:'.($vidRateCound*20).'%;"';
?>
<div class="rating_box"><div class="rating_yellow" <?php echo $vidRateCoundStyle?>></div></div> &nbsp;<?php echo $vidRateCound?>