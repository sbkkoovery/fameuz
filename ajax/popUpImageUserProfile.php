<?php 
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/user_profile_image.php");
$objCommon			   =	new common();
$objUserProfileImg	   =	new user_profile_image();
$getUserDetails	  	  =	$objUserProfileImg->getRowSql("SELECT profileImg.upi_img_url,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 WHERE user.status=1 AND user.user_id=".$_SESSION['userId']);
if(isset($_GET['screenHeight'],$_GET['screenWidth'],$_GET['imageId'])){
	$screenHeight		=	$objCommon->esc($_GET['screenHeight']);
	$screenWidth	 	 =	$objCommon->esc($_GET['screenWidth']);
	$imageId		 	 =	$objCommon->esc($_GET['imageId']);
	if($imageId){
		$imageDetails    =	$objUserProfileImg->getRow("upi_id=".$imageId);
		$friendDetails   =	$objUserProfileImg->getRowSql("SELECT user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz
	FROM users AS user
	LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
	LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
	WHERE user.user_id=".$imageDetails['user_id']);	
	}
//---------------------------------------get images for sliders--------------
	$getOtherPhotosSql				 	=	"SELECT group_concat(tabPhotos.img_id ORDER BY tabPhotos.img_created DESC) AS Ids FROM(SELECT upi_id AS img_id,upi_created AS img_created
		FROM user_profile_image 
		WHERE user_id=".$imageDetails['user_id']." 
		ORDER BY upi_created desc) AS tabPhotos ORDER BY tabPhotos.img_created DESC";
$getAllOtherPhotos					=	$objUserProfileImg->getRowSql($getOtherPhotosSql);
$idsArr							   =	$getAllOtherPhotos['Ids'];
$idsArrExpl						   =	explode(",",$idsArr);
if(count($idsArrExpl)>0){
	$ImagArr						  =	array();
	foreach($idsArrExpl as $keyExpl=>$allArrExpl){
		array_push($ImagArr,$allArrExpl);
	}
	$keyCurrent					   =	array_search($imageId,$ImagArr);
	$keyNext						  =	$keyCurrent+1;
	$keyPrev						  =	$keyCurrent-1;
	if(array_key_exists($keyNext,$ImagArr)){
		$nextValue					=	$ImagArr[$keyNext];
	}else{
		$nextValue					=	'';
	}
	if(array_key_exists($keyPrev,$ImagArr)){
		$prevValue					=	$ImagArr[$keyPrev];
	}else{
		$prevValue					=	'';
	}
}
//--------------------------------------------------------------------------------
$imageName			   =	SITE_ROOT."uploads/profile_images/".$imageDetails['upi_img_url'];
$imageExt				=	strtolower(pathinfo($imageName, PATHINFO_EXTENSION));
$imageExt;
switch ($imageExt) { 
	case 'gif' : 
		$imageExpand   = imagecreatefromgif($imageName);
	break; 
	case 'jpeg' : 
		$imageExpand   = imagecreatefromjpeg($imageName);
	break; 
	case 'jpg' : 
		$imageExpand   = imagecreatefromjpeg($imageName);
	break; 
	case 'png' : 
		$imageExpand   = imagecreatefrompng($imageName);
	break; 
}    
$imageWidth		 	=	imagesx($imageExpand);
$imageHeight		   =	imagesy($imageExpand);
$boxWidth		   	  =	$screenWidth-100;
$boxHeight		  	 =	$screenHeight-50;
$rightSide		     =	290;
$maxWidth		   	  =	($boxWidth-$rightSide);
$imageHeight		   =	($imageHeight>$screenHeight)?($boxHeight):$imageHeight;
$dummyHeight		   =	($imageHeight>545)?$imageHeight:545;
$marginTop		  	 =	($screenHeight-$dummyHeight)/2;
$imageMarginTop	 	=	0;
if($imageHeight<545){
	$imageMarginTop	=	(545-$imageHeight)/2;
}
if($imageWidth<$maxWidth){
	$maxWidth		  =	$imageWidth	=	($imageWidth>530)?$imageWidth:530;
	$boxWidth		  =	$maxWidth+$rightSide;
}else{
	
}
$imageBoxWidth		 =	$boxWidth-$rightSide;
$sideHalf			  =	(($imageHeight-300)<150)?150:($imageHeight-300);
$getLikeCount		  =	$objUserProfileImg->getRowSql("SELECT count(like_id) AS likeCount,group_concat(like_user_id) AS youLike FROM likes WHERE like_content =".$imageId." AND like_cat =6 AND like_status=1");
$getShareCount		 =	$objUserProfileImg->getRowSql("SELECT count(share_id) AS shareCount,group_concat(user_by) AS youShare FROM share WHERE share_content =".$imageId." AND share_category =6 AND share_status=1");
?>

        <div class="cd-popup-container" style="width:<?php echo $boxWidth; ?>px; margin-top:<?php echo $marginTop.'px'; ?>">
            <div class="right_side">
                <?php /*?><div class="logo"><img src="<?php echo SITE_ROOT?>images/logo.png" alt="logo"/></div><?php */?>
				<div class="imgUserBox">
					<span class="prof_pic"><img alt="pro-thumb" src="<?php echo SITE_ROOT?>uploads/profile_images/<?php echo($friendDetails['upi_img_url'])?$friendDetails['upi_img_url']:'profile_pic.jpg'?>"></span>
					<span class="commnt_content">
						<span class="name"><?php echo $objCommon->displayName($friendDetails);?></span>
						<span class="text">Added: <?php echo date("d M , Y h:i a",strtotime($objCommon->local_time($imageDetails['upi_created'])));?></span>
						<?php
							$likeCount		=	$getLikeCount['likeCount'];
							$youLikeArr	   =	explode(",",$getLikeCount['youLike']);
							if(count($youLikeArr)>0){
								if(in_array($_SESSION['userId'],$youLikeArr)){
									$youLike	  =	1;
								}else{
									$youLike	  =	0;
								}
							}
							if($youLike ==1 && $likeCount	==1){
								$likeStr	  =	'You like this.';
							}else if($youLike ==1 && $likeCount>1){
								$likeStr	  =	'You and other '.($likeCount-1).' people like this.';
							}else if($youLike ==0 && $likeCount>0){
								$likeStr	  =	$likeCount.' people like this.';
							}else{
								$likeStr	  =	'';
							}
							$shareCount		=	$getShareCount['shareCount'];
							$youShareArr	   =	explode(",",$getShareCount['youShare']);
							if(count($youShareArr)>0){
								if(in_array($_SESSION['userId'],$youShareArr)){
									$youShare	  =	1;
								}else{
									$youShare	  =	0;
								}
							}
						?>
						<span class="text likestr"><?php echo $likeStr;?></span>
						<span class="text shareStrCount"><i class="fa fa-share-alt"></i> <?php echo $shareCount;?></span>
					</span>
				</div>
				<div class="image_comment_load"></div>
            </div>
            
            <div class="left_side" style="width:<?php echo $maxWidth; ?>px">
                <div class="slider_big_box">
                    <div class="big_image">
                        <img src="<?php echo $imageName?>" style="max-height:<?php echo $imageHeight; ?>px; margin-top:<?php echo $imageMarginTop."px"; ?>" />
                        <div class="arrow_box">
							<?php
							if($prevValue !=''){
							?>
                            <a href="javascript:void(0);" class="left_arrow" onclick="photoPopUpProfile(<?php echo $prevValue?>);"><img src="<?php echo SITE_ROOT?>images/slider_arrow_left.png" /></a>
							<?php
							}
							if($nextValue !=''){
							?>
                            <a href="javascript:void(0);" class="right_arrow" onclick="photoPopUpProfile(<?php echo $nextValue?>);"><img src="<?php echo SITE_ROOT?>images/slider_arrow_right.png" /></a>
							<?php
							}
							?>
                        </div>
                    </div>
                </div>
                <div class="share_like_box">
                    <div class="review_message">
                        <a href="<?php echo SITE_ROOT.'user/'.$objCommon->html2text($friendDetails['usl_fameuz']).'/add-review'?>">Add Review <i class="fa fa-chevron-right"></i></a>
						<?php
						if($friendDetails['user_id'] != $_SESSION['userId']){
						?>
                        <a href="<?php echo SITE_ROOT.'user/'.$objCommon->html2text($friendDetails['usl_fameuz']).'/message'?>">Message <i class="fa fa-chevron-right"></i></a>
						<?php
						}
						?>
                    </div>
                    <div class="like">
						<a class="<?php echo ($youLike==1)?'liked':''?> like_btn" data-like="<?php echo $imageId?>" data-likecat="<?php echo $im_type?>" href="javascript:;"><i class="fa fa-heart"></i> <?php echo ($youLike==1)?'Liked':'Like'?></a>
                    </div>
                    <div class="share">
                        <a class="<?php echo ($youShare==1)?'shared':''?> share_btn" data-share="<?php echo $imageId?>"  href="javascript:;"><i class="fa fa-share-alt"></i><?php echo ($youShare==1)?'Shared':'Share'?> </a>
                    </div>
                </div>
            </div>
            <div class="clr"></div>
            <div class="close_popup"><img src="<?php echo SITE_ROOT?>images/close_new01.png" alt="close"></div>
        </div>
<script type="text/javascript">
$(".image_comment_load").load('<?php echo SITE_ROOT?>widget/pop_image_comment_box.php?sideHalf=<?php echo $sideHalf?>&imageId=<?php echo $imageId?>&im_type=6&userto=<?php echo $imageDetails['user_id']?>
&myimage=<?php echo $getUserDetails['upi_img_url']?>');
$('.like').on('click','.like_btn',function(){
	var likeId		=	$(this).data('like');
	var likecat	   =	6;
	var imgUserId	 =	'<?php echo $friendDetails['user_id']?>';
	var that		  =	this;
	if(likeId !='' && likecat != ''){
		$.ajax({
			url:"<?php echo SITE_ROOT?>ajax/like.php",
			method: "POST",
			data:{likeId:likeId,likecat:likecat,imgUserId:imgUserId},
			success:function(result){
				$(that).parent().html(result);
				$(".likestr").load('<?php echo SITE_ROOT?>ajax/like_count.php?type='+likecat+'&id='+likeId);
		}});
	}
});
$(".share").on("click","a",function(){
	var shareId		=	$(this).data('share');
	var shareCat	   =	6;
	var shareWhoseId   =	'<?php echo $friendDetails['user_id']?>';
	var that		   =	this;
	if(shareId !='' && shareCat != '' && shareWhoseId != ''){
		$.ajax({
			url:"<?php echo SITE_ROOT?>ajax/share.php",
			method: "POST",
			data:{shareId:shareId,shareCat:shareCat,shareWhoseId:shareWhoseId},
			success:function(result){
				$(that).parent().html(result);
				$(".shareStrCount").load('<?php echo SITE_ROOT?>ajax/share_count.php?type='+shareCat+'&id='+shareId);
		}});
	}
});
</script>
<?php }?>