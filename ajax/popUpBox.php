<?php 
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/album_images.php");
include_once(DIR_ROOT."class/images_view.php");
$objCommon		=	new common();
$objAlbums		=	new album_images();
$objImageView	 =	new images_view();
if(isset($_GET['contentId'],$_GET['type'])){
	$imageId	  =	$objCommon->esc($_GET['contentId']);
	$type		 =	$objCommon->esc($_GET['type']);
	$im_type	  =	$objCommon->esc($_GET['contentType']);
	$albumId			 =	($_GET['albumId'])?$objCommon->esc($_GET['albumId']):0;
	$uploadPath   =	SITE_ROOT_AWS_IMAGES."uploads/albums/";
		$imageDetails =	$objAlbums->getRowSql("SELECT photos.user_id,photos.ai_images AS img_url,ai_id AS img_id,photos.ai_caption AS img_descr,photos.ai_created AS img_created,IF(photos.ai_id != '', 2, '') AS img_type,photos.ai_like_count AS likeCount,photos.ai_comment_count AS commentCount,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,count(DISTINCT likes.like_id) AS likeCount,group_concat(DISTINCT likes.like_user_id) AS youLike,count(DISTINCT share.share_id) AS shareCount,group_concat(DISTINCT share.user_by) AS youShare
		FROM album_images  AS photos
		LEFT  JOIN users AS user ON photos.user_id = user.user_id
		LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
		LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
		LEFT JOIN likes ON photos.ai_id = likes.like_content AND likes.like_cat =".$im_type." AND likes.like_status=1
		LEFT JOIN share  ON photos.ai_id = share.share_content AND share.share_category =".$im_type." AND share.share_status=1
		WHERE ai_id=".$imageId);
	if($im_type==1){
		$imageDetails =	$objAlbums->getRowSql("SELECT photos.user_id,photos.photo_url AS img_url,photos.photo_id AS img_id,photos.photo_descr AS img_descr,photos.photo_created AS img_created,IF(photos.photo_id != '', 1, '') AS img_type,photos.photo_like_count AS likeCount,photos.photo_comment_count AS commentCount,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,count(DISTINCT likes.like_id) AS likeCount,group_concat(DISTINCT likes.like_user_id) AS youLike,count(DISTINCT share.share_id) AS shareCount,group_concat(DISTINCT share.user_by) AS youShare
		FROM user_photos  AS photos
		LEFT JOIN users AS user ON photos.user_id = user.user_id
		LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
		LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
		LEFT JOIN likes ON photos.photo_id = likes.like_content AND likes.like_cat =".$im_type." AND likes.like_status=1
		LEFT JOIN share  ON photos.photo_id = share.share_content AND share.share_category =".$im_type." AND share.share_status=1
		WHERE photo_id=".$imageId);
	}
	/*if($im_type==1){
		$imageDetails    =	$objAlbums->getRowSql("SELECT photos.user_id,photos.photo_url AS img_url,photos.photo_id AS img_id,photos.photo_descr as img_descr,photos.photo_created AS img_created,IF(photos.photo_id != '', 1, '') 		AS img_type,photos.photo_like_count AS likeCount,photos.photo_comment_count AS commentCount,photos.photo_set_main AS set_main,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz
	FROM user_photos as photos
	LEFT  JOIN users AS user ON photos.user_id = user.user_id
	LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
	LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
	WHERE photos.photo_id=".$imageId);	
	}else if($im_type==2){
		$imageDetails    =	$objAlbums->getRowSql("SELECT photos.user_id,photos.ai_images AS img_url,ai_id AS img_id,photos.ai_caption AS img_descr,photos.ai_created AS img_created,IF(photos.ai_id != '', 2, '') AS img_type,photos.ai_like_count AS likeCount,photos.ai_comment_count AS commentCount,photos.ai_set_main AS set_main,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz
		FROM album_images  AS photos
		LEFT  JOIN users AS user ON photos.user_id = user.user_id
		LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
		LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
		WHERE ai_id=".$imageId);
	}*/
	
	
	
$imageName	=	SITE_ROOT_AWS_IMAGES."uploads/albums/".$imageDetails['img_url'];
$imageExt	=	strtolower(pathinfo($imageName, PATHINFO_EXTENSION));
$imageExt;
switch ($imageExt) { 
	case 'gif' : 
		$imageExpand = imagecreatefromgif($imageName);
	break; 
	case 'jpeg' : 
		$imageExpand = imagecreatefromjpeg($imageName);
	break; 
	case 'jpg' : 
		$imageExpand = imagecreatefromjpeg($imageName);
	break; 
	case 'png' : 
		$imageExpand = imagecreatefrompng($imageName);
	break; 
} 
$imageHeight		  =	imagesy($imageExpand);
$imageWidth		   =	imagesx($imageExpand);
if($type=='new-photos'){
	$sqlImagesList	=	"SELECT tab2.* FROM (
		 SELECT @c:=@c+1 AS serialNumber,tab.* 
		  FROM (SELECT * FROM((
		   SELECT a.ai_id AS imgId,a.ai_images AS imageUrl,a.user_id,a.ai_like_count AS likeCount,a.ai_comment_count AS commentCount,a.ai_caption AS imageCaption ,u.display_name,u.first_name,u.email,a.ai_created AS createdOn,'2' AS type
		   FROM album_images AS a 
		   LEFT JOIN users AS u ON a.user_id=u.user_id 
		   WHERE u.email_validation=1 AND u.status=1 ORDER BY createdOn DESC)
		   UNION
		   (SELECT a.photo_id AS imgId,a.photo_url AS imageUrl,a.user_id,a.photo_like_count AS likeCount,a.photo_comment_count AS commentCount,a.photo_descr AS imageCaption ,u.display_name,u.first_name,u.email,a.photo_created AS createdOn,'1' AS type 
		   FROM user_photos AS a 
		   LEFT JOIN users AS u ON a.user_id=u.user_id 
		   WHERE u.email_validation=1 AND u.status=1 ORDER BY createdOn DESC))
		  AS t )
		 AS tab,(SELECT @c:=0) AS c ORDER BY tab.createdOn DESC
		 
		) AS tab2 GROUP BY tab2.user_id";	
	
}else if($type=='popular'){
	$sqlImagesList	=	"SELECT @c:=@c+1 AS serialNumber,tab.* 
		FROM ((
			SELECT a.ai_id AS imgId,a.ai_images AS imageUrl,a.user_id,a.ai_like_count AS likeCount,a.ai_comment_count AS commentCount,a.ai_caption AS imageCaption ,u.display_name,u.first_name,u.email,a.ai_created AS createdOn,'2' AS type
			FROM album_images AS a 
			LEFT JOIN users AS u ON a.user_id=u.user_id 
			WHERE u.email_validation=1 AND u.status=1 GROUP BY imgId)
		UNION
			(SELECT a.photo_id AS imgId,a.photo_url AS imageUrl,a.user_id,a.photo_like_count AS likeCount,a.photo_comment_count AS commentCount,a.photo_descr AS imageCaption ,u.display_name,u.first_name,u.email,a.photo_created AS createdOn,'1' AS type 
			FROM user_photos AS a 
			LEFT JOIN users AS u ON a.user_id=u.user_id 
			WHERE u.email_validation=1 AND u.status=1 GROUP BY imgId))
		AS tab,(SELECT @c:=0) AS c ORDER BY likeCount DESC";
}else if($type=='top'){
	$sqlImagesList	=	"SELECT @c:=@c+1 AS serialNumber,tab.* 
		FROM ((
			SELECT a.ai_id AS imgId,a.ai_images AS imageUrl,a.user_id,a.ai_like_count AS likeCount,a.ai_comment_count AS commentCount,a.ai_caption AS imageCaption ,u.display_name,u.first_name,u.email,a.ai_created AS createdOn,'2' AS type,AVG(vote.aiv_vote) AS voteRate 
			FROM album_images_vote AS vote 
			LEFT JOIN album_images AS a ON a.ai_id=vote.ai_id 
			LEFT JOIN users AS u ON a.user_id=u.user_id 
			WHERE u.email_validation=1 AND u.status=1 GROUP BY imgId)
		UNION
			(SELECT a.photo_id AS imgId,a.photo_url AS imageUrl,a.user_id,a.photo_like_count AS likeCount,a.photo_comment_count AS commentCount,a.photo_descr AS imageCaption ,u.display_name,u.first_name,u.email,a.photo_created AS createdOn,'1' AS type,AVG(vote.up_vote) AS voteRate
			FROM user_photos_vote AS vote 
			LEFT JOIN user_photos AS a ON a.photo_id=vote.photo_id 
			LEFT JOIN users AS u ON a.user_id=u.user_id 
			WHERE u.email_validation=1 AND u.status=1 GROUP BY imgId))
		AS tab,(SELECT @c:=0) AS c ORDER BY voteRate DESC";
}else if($type=='last-visited'&&isset($_SESSION['userId'])){
	$sqlImagesList	=	"SELECT @c:=@c+1 AS serialNumber,tab.* 
		FROM ((
			SELECT a.ai_id AS imgId,a.ai_images AS imageUrl,a.user_id,a.ai_like_count AS likeCount,a.ai_comment_count AS commentCount,a.ai_caption AS imageCaption ,u.display_name,u.first_name,u.email,a.ai_created AS createdOn,'2' AS type,iv.iv_created AS visited
			FROM images_view AS iv 
			LEFT JOIN album_images AS a ON iv.iv_content=a.ai_id 
			LEFT JOIN users AS u ON a.user_id=u.user_id 
			WHERE u.email_validation=1 AND u.status=1 AND iv.iv_cat=2 AND iv.iv_user_id=".$_SESSION['userId'].")
		UNION
			(SELECT a.photo_id AS imgId,a.photo_url AS imageUrl,a.user_id,a.photo_like_count AS likeCount,a.photo_comment_count AS commentCount,a.photo_descr AS imageCaption ,u.display_name,u.first_name,u.email,a.photo_created AS createdOn,'1' AS type,iv.iv_created AS visited
			FROM images_view AS iv
			LEFT JOIN user_photos AS a ON iv.iv_content=a.photo_id 
			LEFT JOIN users AS u ON a.user_id=u.user_id 
			WHERE u.email_validation=1 AND u.status=1 AND iv.iv_cat=2 AND iv.iv_user_id=".$_SESSION['userId']."))
		AS tab,(SELECT @c:=0) AS c ORDER BY visited DESC";
}else if($type=='all'){
	$sqlImagesList	=	"SELECT @c:=@c+1 AS serialNumber,tab.* 
		FROM (SELECT * 
			FROM((
				SELECT a.ai_id AS imgId,a.ai_images AS imageUrl,a.user_id,a.ai_like_count AS likeCount,a.ai_comment_count AS commentCount,a.ai_caption AS imageCaption ,u.display_name,u.first_name,u.email,a.ai_created AS createdOn,'2' AS type 
				FROM album_images AS a 
				LEFT JOIN users AS u ON a.user_id=u.user_id 
				WHERE u.email_validation=1 AND u.status=1)
			UNION
				(SELECT a.photo_id AS imgId,a.photo_url AS imageUrl,a.user_id,a.photo_like_count AS likeCount,a.photo_comment_count AS commentCount,a.photo_descr AS imageCaption ,u.display_name,u.first_name,u.email,a.photo_created AS createdOn,'1' AS type
				FROM user_photos AS a
				LEFT JOIN users AS u ON a.user_id=u.user_id 
				WHERE u.email_validation=1 AND u.status=1))
			AS t ORDER BY createdOn DESC)
		AS tab,(SELECT @c:=0) AS c";
}else{
	$searchKey		=	$type;
	$sqlImagesList	=	"SELECT @c:=@c+1 AS serialNumber,tab.* 
	FROM (SELECT * FROM((
		SELECT a.ai_id AS imgId,a.ai_images AS imageUrl,a.user_id,a.ai_like_count AS likeCount,a.ai_comment_count AS commentCount,a.ai_caption AS imageCaption ,u.display_name,u.first_name,u.email,a.ai_created AS createdOn,'2' AS type
		FROM album_images AS a 
		LEFT JOIN users AS u ON a.user_id=u.user_id 
		WHERE u.email_validation=1 AND u.status=1 AND (u.first_name LIKE '%$searchKey%' OR u.last_name LIKE '%$searchKey%' OR u.display_name LIKE '%$searchKey%' OR a.ai_caption LIKE '%$searchKey%'))
	UNION
		(SELECT a.photo_id AS imgId,a.photo_url AS imageUrl,a.user_id,a.photo_like_count AS likeCount,a.photo_comment_count AS commentCount,a.photo_descr AS imageCaption ,u.display_name,u.first_name,u.email,a.photo_created AS createdOn,'1' AS type
		FROM user_photos AS a
		LEFT JOIN users AS u ON a.user_id=u.user_id 
		WHERE u.email_validation=1 AND u.status=1 AND (u.first_name LIKE '%$searchKey%' OR u.last_name LIKE '%$searchKey%' OR u.display_name LIKE '%$searchKey%' OR a.photo_descr LIKE '%$searchKey%')))
	AS t ORDER BY createdOn DESC)AS tab,(SELECT @c:=0) AS c ORDER BY createdOn DESC";
}
$newImagesList	=	$objAlbums->listQuery($sqlImagesList);
if(count($newImagesList)>0){
	$ImagArr						  =	array();
	foreach($newImagesList as $allArrExpl){
		array_push($ImagArr,array("imageId"=>$allArrExpl['imgId'],"userId"=>$allArrExpl['user_id'],"catType"=>$allArrExpl['type']));
	}
	$keyCurrent					   =	array_search(array("imageId"=>$imageId,"catType"=>$im_type,"userId"=>$imageDetails['user_id']),$ImagArr);
	$keyCurrent;
	$keyNext						  =	$keyCurrent+1;
	$keyPrev						  =	$keyCurrent-1;
	if(array_key_exists($keyNext,$ImagArr)){
		$nextValue					=	$ImagArr[$keyNext];
	}else{
		$nextValue					=	'';
	}
	if(array_key_exists($keyPrev,$ImagArr)){
		$prevValue					=	$ImagArr[$keyPrev];
	}else{
		$prevValue					=	'';
	}
}
if(isset($_SESSION['userId'])){
	$currentView	=	$objImageView->countRows("SELECT iv_id FROM images_view WHERE iv_content=$imageId AND iv_user_id=".$_SESSION['userId']);
	if($currentView){
		$objImageView->updateField(array("iv_created"=>date("Y-m-d H:i:s")),"iv_content=$imageId AND iv_cat=$im_type AND iv_user_id=".$_SESSION['userId']);
	}else{
		$objImageView->insert(array("iv_cat"=>$im_type,"iv_content"=>$imageId,"iv_user_id"=>$_SESSION['userId'],"iv_visit"=>1,"iv_created"=>date("Y-m-d H:i:s")));
	}
	$getUserDetails	  	  =	$objAlbums->getRowSql("SELECT profileImg.upi_img_url,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 WHERE user.status=1 AND user.user_id=".$_SESSION['userId']);
}
$getLikeCount		  	=	$objAlbums->getRowSql("SELECT count(like_id) AS likeCount,group_concat(like_user_id) AS youLike FROM likes WHERE like_content =".$imageId." AND like_cat =$im_type AND like_status=1");
$relatedImageList	  	=	$objAlbums->getFields("ai_id,ai_images","user_id=".$imageDetails['user_id']);
$imageUrl			    =	$uploadPath.$imageDetails['img_url'];
$userImageCon			=	($imageDetails['upi_img_url'])?$imageDetails['upi_img_url']:'profile_pic.jpg';
$userImage			   =	SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.$userImageCon;
$limit				   =	70;
$img_descr			   =	$objCommon->html2text($imageDetails['img_descr']);
if(strlen($img_descr) > $limit){
	$img_descrSmall	  =	mb_substr($img_descr,0,$limit,'UTF-8')."...";
	$img_descrSmall	 .=	$img_descrSmall.'<p class="readP text-right"><a href="javascript:;" class="readMore">Read more</a></p>';
	$img_descrFull	   =	$img_descr;
}else{
	$img_descrSmall	  =	$img_descr;
	$img_descrFull	   =	$img_descrFull;
}
$userName				=	$objCommon->displayName($imageDetails);
$myImageCon			  =	($getUserDetails['upi_img_url'])?$getUserDetails['upi_img_url']:'profile_pic.jpg';
$myImage				 =	SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.$myImageCon;	
$imageCreatedOn		  =	date("d M , Y h:i a",strtotime($objCommon->local_time($imageDetails['img_created'])));
@$imagePrevId			=	$prevValue['imageId'];
@$imagePrevType		   =	$prevValue['catType'];
@$prevUserId			 =	$prevValue['userId'];
@$imageNextId			=	$nextValue['imageId'];
@$imageNextType		   =	$nextValue['catType'];
@$nextUserId			 =	$nextValue['userId'];
$likeCount			   =	$imageDetails['likeCount'];
$youLikeArr	   		  =	explode(",",$imageDetails['youLike']);
if(count($youLikeArr)>0){
	if(in_array($_SESSION['userId'],$youLikeArr)){
		$youLike	  	 =	1; // you liked
	}else{
		$youLike	  	 =	0;
	}
}
$shareCount	  		  =	$imageDetails['shareCount'];
$youShareArr	  		 =	explode(",",$imageDetails['youShare']);
if(count($youShareArr)>0){
	if(in_array($_SESSION['userId'],$youShareArr)){
		$youShare 	    =	1; // you shared this
	}else{
		$youShare 		=	0;
	}
}
if($imageDetails['user_id'] !=$_SESSION['userId']){
	$getWorkedDetails	=	$objAlbums->getRowSql("SELECT worked_id,from_status,to_status FROM worked_together WHERE img_cat=".$im_type." and img_id =".$imageId." and send_from=".$_SESSION['userId']." and send_to=".$imageDetails['user_id']);
	if($getWorkedDetails['from_status']==1 && $getWorkedDetails['to_status']==1){
		$workedTogether  =	3; //request sent and accepted
	}else if($getWorkedDetails['from_status']==1){
		$workedTogether  =	2; //request sent
	}else{
		$workedTogether  =	1; //request not sent
	}
}else{
	$workedTogether	  =	0; // worked together not applicable
}
$commentCount			=	number_format($imageDetails['commentCount']);
$relatedImages		   =	array();
foreach($relatedImageList as $images){//related images start
	if($images['ai_id']!=$imageId){
		array_push($relatedImages,array("contentId"=>$images['ai_id'],"type"=>$type,"contentType"=>2,"relatedImgUrl"=>$uploadPath.$objCommon->getThumb($images['ai_images'])));
	}
}//related images end

$jsonArr			   	 =	array('userMe'=>$_SESSION['userId'],'imageUrl'=>$imageUrl,'userImage'=>$userImage,'imgDescrSmall'=>$img_descrSmall,'imgDescrFull'=>$img_descrFull,'userName'=>$userName,'imageCreatedOn'=>$imageCreatedOn,'imagePrevId'=>$imagePrevId,'imagePrevType'=>$imagePrevType,'prevUserId'=>$prevUserId,'imageNextId'=>$imageNextId,'imageNextType'=>$imageNextType,'nextUserId'=>$nextUserId,'imageAlbumId'=>$albumId,'imageHeight'=>$imageHeight,'userto'=>$imageDetails['user_id'],'myimage'=>$myImage,'youLike'=>$youLike,'likeCount'=>$likeCount,'youShare'=>$youShare,'shareCount'=>$shareCount,'workedTogether'=>$workedTogether,'commentCount'=>$commentCount,'relatedImages'=>$relatedImages,'imageWidth'=>$imageWidth);

//echo "<pre>"; print_r($jsonArr); echo "</pre>";
echo json_encode($jsonArr);
exit;
}
?>
