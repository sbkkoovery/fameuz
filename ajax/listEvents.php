<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/reminders.php");
include_once (DIR_ROOT.'class/calendarClass.php');
$objCommon	  =	new common();
$objReminder	=	new reminders();
$calendar = new CalendarPlus();
$userId				   =	$_SESSION['userId'];
if(isset($_GET['eventDate'])){
$eventDate		=	date("Y-m-d",strtotime($_GET['eventDate']));
$eventYear		=	date("Y",strtotime($_GET['eventDate']));
$eventMonth		=	date("m",strtotime($_GET['eventDate']));
$eventDay		=	date("d",strtotime($_GET['eventDate']));
$friendPermission	=	"'1,0,0,0','1,0,0,1','1,0,1,0','1,0,1,1','1,1,0,0','1,1,0,1','1,1,1,0','1,1,1,1','0,1,0,0','0,1,0,1','0,1,1,0','0,1,1,1'";
$followPermission	=	"'1,0,0,0','1,0,0,1','1,0,1,0','1,0,1,1','1,1,0,0','1,1,0,1','1,1,1,0','1,1,1,1'";
$allEventSql	  =	"SELECT * FROM (
SELECT
user.user_id,user.first_name,user.last_name,user.display_name,book.bm_company,book.bm_descr,book.book_to AS endDate, book.bm_country AS location,'booking' AS calCat
FROM book_model AS book 
LEFT JOIN users AS user ON book.agent_id=user.user_id 
WHERE book.model_id=".$_SESSION['userId']." AND book.bm_model_status=1 AND DATE(book.book_from)='".$eventDate."'
UNION
SELECT
r.remind_id AS user_id,u.first_name,u.last_name,u.display_name,r.remind_title AS bm_company ,r.remind_description AS bm_descr,'NA' AS endDate,r.remind_place AS location,'reminder' AS calCat
FROM reminders AS r
LEFT JOIN users AS u ON r.user_id=u.user_id
WHERE r.user_id=".$_SESSION['userId']." AND r.remind_date='".$eventDate."'
UNION
SELECT
u.user_id,u.first_name,u.last_name,u.display_name,'NA' AS bm_company ,'NA' AS bm_descr,up.p_dob AS endDate,'NA' AS location,'birthday' AS calCat
FROM users AS u
LEFT JOIN  personal_details as up ON u.user_id=up.user_id
LEFT JOIN following AS flw ON u.user_id=flw.follow_user1 OR u.user_id=flw.follow_user2
LEFT JOIN user_privacy as pr ON u.user_id=pr.user_id
WHERE flw.follow_status=2 AND pr.uc_p_dob IN(".$friendPermission.") AND (flw.follow_user1=".$_SESSION['userId']." OR flw.follow_user2=".$_SESSION['userId'].") AND MONTH( up.p_dob)='".$eventMonth."' AND DAYOFMONTH( up.p_dob)='".$eventDay."'
UNION
SELECT
u.user_id,u.first_name,u.last_name,u.display_name,'NA' AS bm_company ,'NA' AS bm_descr,up.p_dob AS endDate,'NA' AS location,'birthday' AS calCat
FROM users AS u
LEFT JOIN  personal_details as up ON u.user_id=up.user_id
LEFT JOIN following AS flw ON u.user_id=flw.follow_user1 OR u.user_id=flw.follow_user2
LEFT JOIN user_privacy as pr ON u.user_id=pr.user_id
WHERE flw.follow_status=1 AND pr.uc_p_dob IN(".$followPermission.") AND flw.follow_user1=".$_SESSION['userId']." AND MONTH( up.p_dob)='".$eventMonth."' AND DAYOFMONTH( up.p_dob)='".$eventDay."'

) AS calTab WHERE 1
";
$remindDetails	=	$objReminder->listQuery($allEventSql);
$showDetailsFunction	=	'onclick="showDetails(\''.$eventYear.'-'.$eventMonth.'-'.($eventDay+1).'\')"';
$nextDay	=	date("d",strtotime($eventYear.'-'.$eventMonth.'-'.($eventDay+1)));
$prevDay	=	date("d",strtotime($eventYear.'-'.$eventMonth.'-'.($eventDay-1)));

$calendar->currentYear=$eventYear;
$calendar->currentMonth=$eventMonth;
$calendar->selDay=$eventDay;
$calendar->daysInMonth=$calendar->_daysInMonth($eventMonth,$eventYear);
$nextMonth = $calendar->currentMonth==12?1:intval($calendar->currentMonth)+1;
$nextYear = $calendar->currentMonth==12?intval($calendar->currentYear)+1:$calendar->currentYear;
$preMonth = $calendar->currentMonth==1?12:intval($calendar->currentMonth)-1;
$preYear = $calendar->currentMonth==1?intval($calendar->currentYear)-1:$calendar->currentYear;

if($eventDay+1>$calendar->daysInMonth){
	$showDetailsFunction	=	'onclick="window.location=\''.SITE_ROOT.'user/user-calendar/'.$nextMonth.'/'.$nextYear.'/\'"';
}else{
	$showDetailsFunction	=	'onclick="showDetails(\''.$eventYear.'-'.$eventMonth.'-'.($nextDay).'\')"';
}
if($eventDay-1<1){
	$showDetailsPreFunction	=	'onclick="window.location=\''.SITE_ROOT.'user/user-calendar/'.$preMonth.'/'.$preYear.'/'.$prevDay.'/\'"';
}else{
	$showDetailsPreFunction	=	'onclick="showDetails(\''.$eventYear.'-'.$eventMonth.'-'.($prevDay).'\')"';
}
?>
<div class="day-notifications">
    <div class="img-arw-left"><img src="<?php echo SITE_ROOT?>images/calendar-arw.png" <?php echo $showDetailsPreFunction; ?> /></div>
    <div class="date-clander"><p><?php echo date("d",strtotime($_GET['eventDate'])); ?> <span><?php echo date("l",strtotime($_GET['eventDate'])); ?></span></p></div>
    <div class="img-arw-right"><img src="<?php echo SITE_ROOT?>images/calendar-arw-right.png" <?php echo $showDetailsFunction; ?> /></div>
</div>
<div class="day-notification-content">
    <ul>
    
    <?php 
	if(count($remindDetails)>0){
		foreach($remindDetails as $reminder){
		if($reminder['calCat']=='reminder'){
	?>
        <li>
            <h5><?php echo $reminder['bm_company']; ?> <span class="events"><?php echo ucwords($reminder['calCat']); ?></span></h5>
            <p><?php echo $objCommon->html2text($reminder['bm_descr']); ?></p>
<!--            <p><img src="--><?php //echo SITE_ROOT?><!--images/calender-cal.png" /> Ends on 31, 2015</p>-->
            <?php if($reminder['bm_company']!='NA'){?>
            <p><img src="<?php echo SITE_ROOT?>images/marker-cal.png" /> <?php echo $reminder['location']; ?></p>
            <?php }?>
            <a href="javascript:;" class="editArrow" onclick="showOptions(this);"><i class="fa fa-angle-down"></i></a>
            <div class="dropdown_option_unfriend">
                <div class="menu-drop">
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#editReminder" class="unfriend has-spinner" data-friend="" onclick="editReminder(<?php echo $reminder['user_id']; ?>);">Edit<span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span></a>
                    <a href="<?php echo SITE_ROOT; ?>access/delete_reminder.php?remindId=<?php echo $reminder['user_id']; ?>" class="cancel" onclick="return confirm(\'You want to delete..?\');">Delete</a>
                </div>
            </div>
        </li>
    <?php }else if($reminder['calCat']=='birthday'){?>
        <li>
            <h5><?php echo ucwords($objCommon->displayName($reminder)); ?> <span><?php echo ucfirst($reminder['calCat']); ?></span></h5>
            <p><img src="<?php echo SITE_ROOT?>images/calender-cal.png" /> Date of Birth <?php echo date("dS F, Y",strtotime($reminder['endDate'])); ?></p>
            <?php if($reminder['bm_company']!='NA'){?>
            <p><img src="<?php echo SITE_ROOT?>images/marker-cal.png" /> <?php echo $reminder['location']; ?></p>
            <?php }?>
        </li>
    <?php }else if($reminder['calCat']=='booking'){?>
        <li>
            <h5><?php echo $reminder['bm_company']; ?> <span><?php echo ucfirst($reminder['calCat']); ?></span></h5>
            <p><?php echo $objCommon->html2text($reminder['bm_descr']); ?></p>
            <p><img src="<?php echo SITE_ROOT?>images/calender-cal.png" /> Ends on <?php echo date("dS F, Y",strtotime($reminder['endDate'])); ?></p>
            <?php if($reminder['bm_company']!='NA'){?>
            <p><img src="<?php echo SITE_ROOT?>images/marker-cal.png" /> <?php echo $reminder['location']; ?></p>
            <?php }?>
        </li>    
    <?php }} }else{?>
    <li style="background:none;">Nothing found in this date.</li>
    <?php }?>	
    </ul>
</div>
<?php }?>