<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/following.php");
	$objCommon				   =	new common();
	$objFollowing				=	new following();
	$unfriendId				  =	$objCommon->esc($_POST['unfriendId']);
	$userId					  =	$_SESSION['userId'];
	if($unfriendId != '' && $userId != ''){
		$getFollowList		   =	$objFollowing->getRow("(follow_user1=".$userId." AND follow_user2=".$unfriendId.") OR (follow_user1=".$unfriendId." AND follow_user2=".$userId.")");
		if($getFollowList['follow_id']){
			if($getFollowList['follow_status']==1){
				$objFollowing->delete("follow_id=".$getFollowList['follow_id']);
			}else{
				if($getFollowList['follow_user1']==$userId){
					$objFollowing->updateField(array("follow_user1"=>$unfriendId,"follow_user2"=>$userId,"follow_status"=>1),"follow_id=".$getFollowList['follow_id']);
				}else if($getFollowList['follow_user2']==$userId){
					$objFollowing->updateField(array("follow_status"=>1),"follow_id=".$getFollowList['follow_id']);
				}
			}
		}
	}
}
?>