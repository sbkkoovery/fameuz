<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/comments.php");
	$objCommon				   =	new common();
	$ObjComments				 =	new comments();
	$blogId					  =	$objCommon->esc($_GET['blogId']);
	$comment_cat				 =	7;
	$userId					  =	$_SESSION['userId'];
	if($blogId != '' && $userId != ''){
		$commentCount		  =	$ObjComments->count("comment_cat=".$comment_cat." AND comment_content =".$blogId." AND comment_status=1");
		echo $commentCount." Comments";
	}
}
?>