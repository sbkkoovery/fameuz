<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/album_images.php");
$objCommon				   =	new common();
$objAlbumImages			  =	new album_images();
$albumId					 =	$objCommon->esc($_GET['albumId']);
$userId					  =	$_SESSION['userId'];
if($albumId !='' && $userId !=''){
	$getAlbumPhotos		  =	$objAlbumImages->listQuery("select img.ai_images,img.ai_caption,album.a_name,img.ai_id,img.ai_created,img.ai_set_main from album_images as img left join album on img.a_id=album.a_id where   img.a_id=".$albumId." and img.user_id=".$userId." order by img.ai_created desc");
?>
<!-------------------------------------------------------------------------------------------------------------->
<div class="dropZoneSection">
    <div class="my_prev">
            <form action="<?php echo SITE_ROOT?>ajax/ajaximage.php?albumId=<?php echo $albumId?>&path=<?php echo DIR_ROOT?>uploads/albums/" class="dropzone" id="newUplaod" method="post"></form>
            <div id="drop_message">Drop Your File Here</div>
        </div>
    <div class="form_submit">
        	<button class="btn btn-default pull-right removeAttr media_form_submit" disabled="disabled">Submit</button>
        <div class="clearfix"></div>
    </div>
</div>
<script>
$("#newUplaod").dropzone({
	   paramName: "file[]",
	   addRemoveLinks: false,
       thumbnailWidth: 182,
       thumbnailHeight: 184,
	   success:function(file, response){
		   $('#drop_message').show();
		   $('.my_prev .title_upload_dz, .my_prev .desc_photo_dz').click(function(){
			   insertId($(this));
			});
			$('.dz-remove').click(function(){
				var deletId	=	$(this).parent('.dz-preview').attr('id');
				$(this).attr('data-deleteId',deletId );
				
			});
	  },
	  queuecomplete: function(){
			$('.my_prev .title_upload_dz, .my_prev .desc_photo_dz, .removeAttr').removeAttr('disabled');
	 }
});

function insertId(_this) {
	var primaryId	=	_this.parent('.form_section').parent('.dz-preview').attr('id');
	
		_this.parent('.form_section').children('.hiddenField').attr({'name': 'hiddenIdDz[]', 'value':primaryId});
		_this.parent('.form_section').children('.desc_photo_dz').attr('name', 'dZdesc_'+primaryId);
	
}
</script>
<!------------------------------------------------------------------------------------------------------------->

	<?php /*?><div class="row albListShow">
		<div class="col-md-12">
			<div class="add_photo_album text-center">
				<form action='<?php echo SITE_ROOT?>ajax/ajaximage.php?albumId=<?php echo $albumId?>&path=<?php echo DIR_ROOT?>uploads/albums/' method="post" enctype="multipart/form-data" id="media_form">
					<i class="fa fa-camera"></i> Add Photos to album<input type="file" class="album_uploader"  name="file[]" id="file_browse" multiple="multiple">
				</form>
			</div>
		</div>
	</div><?php */?>
	<?php
	if(count($getAlbumPhotos)>0){
	?>
	<div class="row albListShow">
	<div class="col-md-12">
	<div class="add_photo_album_list">
		<ul class="album_list_ul">
			<?php
			foreach($getAlbumPhotos as $allAlbumPhotos){
				$getAiImages		=	$objCommon->getThumb($allAlbumPhotos['ai_images']);
				?>
			<li><div class="pic"><a href="javascript:;" class="lightBoxs" data-contentId="<?php echo $allAlbumPhotos['ai_id']?>" data-contentType="2" data-contentAlbum="<?php echo $albumId?>"><img src="<?php echo SITE_ROOT?>uploads/albums/<?php echo $getAiImages?>" class="img-responsive"  /></a></div></li>
			<?php
			}
			?>
		</ul>
	</div>
	</div>
	</div>
	<?php
	}else{
		echo '<div class="row albListShow"><div class="col-md-12"><p class="no_img_album text-center">There are no photos in this album</p></div></div>';
	}
	?>
	<div class="row">
		<div class="col-md-12">
		<div id="preview"></div>
		</div>
	</div>
	<?php
}
?>

<script type="text/javascript">

$(document).ready(function(e) {
	$('.media_form_submit').on('click', function(){ 
		$("#preview").html('<div class="load_album_preloader"></div>');
		$("#newUplaod").ajaxForm(
				{
					target: '#preview',
					success:successCall
				}).submit();
				$(".albListShow").hide();
	});
});
function successCall(){
	$(".load_album_preloader").hide();
	$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/photo_album_photo_page.php?albumId=<?php echo $albumId?>',function(){
		$(".load_album_content").prepend('<div class="backTolist"><a href="javascript:;" onclick="loadFolder();">Back to Folder</a></div>');
	});

}
</script>