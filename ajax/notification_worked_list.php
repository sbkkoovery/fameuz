<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/notifications.php");
include_once(DIR_ROOT."class/worked_together.php");
$objCommon				 =	new common();
$objNotifications		  =	new notifications();
$objWorkedTogether		 =	new worked_together();
$sqlNotificationList	   =	"SELECT noti.notification_id,noti.notification_type,noti.notification_descr,noti.notification_redirect_url,noti.notification_time,noti.notification_from,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz,profileImg.upi_img_url FROM notifications AS noti LEFT JOIN users AS user ON noti.notification_from = user.user_id LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 WHERE noti.notification_to='".$_SESSION['userId']."' AND notification_type='worked_together' ORDER BY noti.notification_time desc LIMIT 0,20";
$getNotificationList	   =	$objNotifications->listQuery($sqlNotificationList);
if(count($getNotificationList)>0){
		foreach($getNotificationList as $allNotificationList){
			$notificationFrom			=	$allNotificationList['notification_from'];
			$notification_redirect_url   =	$objCommon->html2text($allNotificationList['notification_redirect_url']);
			$urlNotiArr				  =	parse_url($notification_redirect_url);
			$urlNotiQueryStr			 =	parse_str($urlNotiArr['query'],$outputQueryStr);
			$imgType					 =	$outputQueryStr['type']; 
			$imgId					   =	$outputQueryStr['id'];
			if($imgType==1 || $imgType==10){
				include_once(DIR_ROOT."class/user_photos.php");
				$objUserphotos		   =	new user_photos();
				$getNotiPhotos		   =	$objUserphotos->getRowSql("SELECT photo_url FROM user_photos WHERE photo_id=".$imgId);
				$workTogetherThoto	   =	$getNotiPhotos['photo_url'];
			}else if($imgType==2){
				include_once(DIR_ROOT."class/album_images.php");
				$objAlbumImages		  =	new album_images();
				$getNotiPhotos		   =	$objAlbumImages->getRowSql("SELECT ai_images FROM album_images WHERE ai_id=".$imgId);
				$workTogetherThoto	   =	$getNotiPhotos['ai_images'];
			}
			else if($imgType==6){
				include_once(DIR_ROOT."class/user_profile_image.php");
				$objProfileImages		=	new user_profile_image();
				$getNotiPhotos		   =	$objProfileImages->getRowSql("SELECT upi_img_url FROM user_profile_image WHERE upi_id=".$imgId);
				$workTogetherThoto	   =	$getNotiPhotos['upi_img_url'];
			}
			$workTogetherDetails		 =	$objWorkedTogether->getRow("img_cat=".$imgType." and img_id =".$imgId." and send_from=".$notificationFrom." and send_to=".$_SESSION['userId']);
?>
<div class="worked_box item_review" id="item_review-<?php echo $allNotificationList['notification_id']?>">
	<div class="image">
	<a href="<?php echo SITE_ROOT.$objCommon->html2text($allNotificationList['usl_fameuz'])?>"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($allNotificationList['upi_img_url'])?$allNotificationList['upi_img_url']:'profile_pic.jpg'?>" alt="profile_pic" class="img-responsive" /></a>
	</div>
	<div class="text worked_together_middle">
		<p><?php echo $objCommon->html2text($allNotificationList['notification_descr']);?></p>
		<p>
			<?php
			if($workTogetherDetails['to_status']==0){
			?>
			<a href="javascript:;" class="worked_confirm has-spinner agree" data-worked="<?php echo $workTogetherDetails['worked_id']?>" data-notid="<?php echo $allNotificationList['notification_id']?>"><span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span>Accept</a> 
			<a href="#" class="worked_confirm has-spinner refute" data-worked="<?php echo $workTogetherDetails['worked_id']?>" data-notid="<?php echo $allNotificationList['notification_id']?>"><span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span>Decline</a>
			<?php
			}else{
			?>
			<a href="javascript:;" class="worked_confirm agreed"><i class="fa fa-check"></i> Accepted</a>
			<?php
			}
			?>
		</p>
			<span class="time"><i class="fa fa-clock-o"></i><?php echo date("d M , Y h:i a",strtotime($objCommon->local_time($allNotificationList['notification_time'])));?></span>
	</div>
	<div class="worked_together_right_text">
		<div class="imgWorkedlink">
        <?php if($imgType==6){ ?>
			<a href="<?php echo $notification_redirect_url?>"><img src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.$workTogetherThoto?>" class="img-responsive" /></a>
            <?php } else {?>
            <a href="<?php echo $notification_redirect_url?>"><img src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/albums/'.$workTogetherThoto?>" class="img-responsive" /></a>
            <?php }?>
		</div>
	</div>
	<div class="closeNoti"><a href="javascript:;" data-notid="<?php echo $allNotificationList['notification_id']?>"><i class="fa fa-times"></i></a></div>
	<div class="clr"></div>
</div>
<?php
	}
}else{
	echo "<p>No notification found...</p>";
}
?>
<script>
$(".worked_box").on("mouseover",function(){
	$(".closeNoti").hide();
	$(this).find(".closeNoti").show();
});
$(".worked_box").on("mouseout",function(){
	$(".closeNoti").hide();
});
$(".closeNoti").on("click","a",function(){
	var notid			=	$(this).data("notid");
	$(this).addClass('active');
	var that			=	this;
	$.ajax({
		url:'<?php echo SITE_ROOT?>access/delete_notification.php',
		data:{notid:notid},
		method:"POST",
		success:function(result){
			$(that).removeClass('active');
			$(".notiList").load('<?php echo SITE_ROOT?>ajax/notification_worked_list.php');
	}});
});
$(".agree").on("click",function(){
	var workedId		=	$(this).data("worked");
	var confirmId	   =	1;
	$(this).addClass('active');
	var that			=	this;
	if(workedId){
		$.ajax({
		url:'<?php echo SITE_ROOT?>access/confirm_work_together.php',
		data:{workedId:workedId,confirmId:confirmId},
		method:"POST",
		success:function(result){
			$(that).removeClass('active');
			$(".notiList").load('<?php echo SITE_ROOT?>ajax/notification_worked_list.php');
		}});
	}
});
$(".refute").on("click",function(){
	var workedId		=	$(this).data("worked");
	var notid		   =	$(this).data("notid");
	var confirmId	   =	0;
	if(workedId){
		$.ajax({
		url:'<?php echo SITE_ROOT?>access/confirm_work_together.php',
		data:{workedId:workedId,confirmId:confirmId,notid:notid},
		method:"POST",
		success:function(result){
			$(".notiList").load('<?php echo SITE_ROOT?>ajax/notification_worked_list.php');
		}});
	}
});	
</script>