<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/promotion_based_duration.php");
	include_once(DIR_ROOT."class/promotion_based_category.php");
	include_once(DIR_ROOT."class/promotion_based_location.php");
	include_once(DIR_ROOT."class/promotion.php");
	$objPromoduration			=	new promotion_based_duration();
	$objPromocat				 =	new promotion_based_category();
	$objPromoloc				 =	new promotion_based_location();
	$objPromotion			    =	new promotion();
	$objCommon				   =	new common();
	
	
	$promoteloc = json_decode(stripslashes($_POST['promoteloc']));
	$promotecat = json_decode(stripslashes($_POST['promotecat']));
	$string1=implode(",",$promoteloc);
	$string2=implode(",",$promotecat);
	
	$totbudget				 =	$objCommon->esc($_POST['totbudget']);
	$datefrom				  =	$objCommon->esc($_POST['datefrom']);
	$days					  =	$objCommon->esc($_POST['days']); 
	
	$getDays				   =	$objPromoduration->getRow("pbd_id =".$days);	
	$totDays                   =    $getDays['pbd_days'];
	$fromdate                  = strtotime($datefrom);
	$todate                    = strtotime("+".$totDays."day", $fromdate);	
	$todatePromo               = date('m/d/Y', $todate);
	
	$date1=date("Y-m-d",strtotime($datefrom));
	$date2=date("Y-m-d",strtotime($todatePromo));
		
	if($promoteloc != '' && $promotecat != '' && $totbudget != '' && $datefrom != '' && $days != '')
	{
	   $_POST['user_id']				  =  $_SESSION['userId'];
	   $_POST['promo_type']			   =  'profile';
	   $_POST['promo_duration']		   =  $days;
	   $_POST['promo_country']		    =  ','.$string1.',';
	   $_POST['promo_category']		   =  ','.$string2.',';
	   $_POST['promo_start_date']         = $date1;
	   $_POST['promo_end_date']           = $date2;
	   
	   $_POST['promo_admin_status']       = '1';
	   $_POST['promo_status']             = 0;
	   $objPromotion->insert($_POST);
	   echo "1"; 
	   
		
	}
}
?>