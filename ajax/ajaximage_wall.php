<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/user_photos.php");
include(DIR_ROOT."ajax/resize-class.php");
include(DIR_ROOT."ajax/face_crop.php");
$objCommon				   =	new common();
$path 						= 	$_GET['path'];
$albumId					 =	$objCommon->esc($_GET['albumId']);
$userId					  =	$_SESSION['userId'];
$objUserPhotos			   =	new user_photos();
$valid_formats 			   = 	array("jpg", "png", "gif", "bmp","jpeg","JPG", "PNG", "GIF", "BMP","JPEG");
$uploadStatus		=	0;
$imgStr			  =	'';
if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST" && $_FILES['file']['tmp_name'] !='')
{
	$todayDate		=	date('Y-m-d');
	if(!file_exists($path.$todayDate)){
		mkdir($path.$todayDate);
	}
	$path	=	$path.$todayDate.'/';
foreach($_FILES['file']['tmp_name'] as $i => $tmp_name ){ 
$name = $_FILES['file']['name'][$i];
$size = $_FILES['file']['size'][$i];
if(strlen($name))
{
	$ext	=	strtolower(pathinfo($name, PATHINFO_EXTENSION));
	if(in_array($ext,$valid_formats))
	{
	if($size<(3072*10240))
	{
	$time						=	time();				
	$actual_image_name_ext_no	=	$time."_".$userId.'_'.$i;
	$actual_image_name 		   = 	$time."_".$userId.'_'.$i.".".$ext;
	
	if(!file_exists($path."original")){
		mkdir($path."original");
	}
	if(!file_exists($path."thumb")){
		mkdir($path."thumb");
	}
	$tmpName = $_FILES['file']['tmp_name'][$i];    
list($widthu, $heighut, $typeu, $attru) = getimagesize($tmpName); 
	if(move_uploaded_file($_FILES['file']['tmp_name'][$i], $path.'original/'.$actual_image_name)){
	
	$resizeObj = new resize($path.'original/'.$actual_image_name);
	//$resizeObj -> resizeImage(200, 200, 'exact');
	//$resizeObj -> saveImage($path.'thumb/'.$actual_image_name, 100);
	if($heighut >$widthu){
		if ($heighut > 700){
			$resizeObj -> resizeImage('', 700, 'auto');
			$resizeObj -> saveImage($path.$actual_image_name, 100);
		}else{
			$resizeObj -> resizeImage('', $heighut, 'auto');
			$resizeObj -> saveImage($path.$actual_image_name, 100);
		}
	}else{
		if ($widthu > 900){
			$resizeObj -> resizeImage(900, '', 'auto');
			$resizeObj -> saveImage($path.$actual_image_name, 100);
		}else{
			$resizeObj -> resizeImage($widthu,'', 'auto');
			$resizeObj -> saveImage($path.$actual_image_name, 100);
		}
	}
	//----------------face cropping----------------------------------------------
	cropImg($path.'original/'.$actual_image_name,$path.'thumb/',$actual_image_name_ext_no,'192','192');
	//----------------face cropping----------------------------------------------
	//----------------start upload to AWS----------------------------------------
	require(DIR_ROOT."amazone_s3/S3.php");
	$s3			=	new S3(AWS_ID,AWS_KEY);
	S3::putObjectFile($path.$actual_image_name,AWS_IMG_BUCKET,'uploads/albums/'.$todayDate.'/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
	S3::putObjectFile($path.'thumb/'.$actual_image_name,AWS_IMG_BUCKET,'uploads/albums/'.$todayDate.'/thumb/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
	//----------------end upload to AWS-------------------------------------------
	$uploadStatus				=	1;
	$_POST['user_id']			=	$userId;
	$_POST['photo_url']		  =	$todayDate.'/'.$actual_image_name;
	$_POST['photo_created']	  =	date("Y-m-d H:i:s");
	$_POST['photo_edited']	   =	date("Y-m-d H:i:s");
	$objUserPhotos->insert($_POST);
	$lastAlbImgId				=	$objUserPhotos->insertId();
	$arrayResponse			   =	array("imgId"=>$lastAlbImgId);		
	 }
}
//else
//echo '<font color="#CC0000">Image file size max 1 MB</font>'; 
}
//else
//echo '<font color="#CC0000">Invalid file format..</font>'; 
}
//else
//echo '<font color="#CC0000">Please select image..!</font>';
	}
	//exit;
	if($uploadStatus==1){
		echo $jsonResponse		=	json_encode($arrayResponse);
	}
}
$hiddenIdDz				=	$_POST['hiddenIdDz'];
if(count($hiddenIdDz)>0){
	foreach($hiddenIdDz as $allhid){
		$ai_caption	=	$objCommon->esc($_POST['dZdesc_'.$allhid]);
		$objUserPhotos->updateField(array('photo_descr'=>$ai_caption),"photo_id=".$allhid);
	}
}
?>