<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/album.php");
	$objCommon				   =	new common();
	$objAlbum					=	new album();
	$edit_album_id			   =	$objCommon->esc($_GET['edit_album_id']);
	$user_id					 =	$_SESSION['userId'];
	if($edit_album_id !='' && $user_id!=''){
		$getAlbumDetails		 =	$objAlbum->getRow("a_id=".$edit_album_id." and user_id=".$user_id);
		if($getAlbumDetails['a_id'] !=''){
			?>
			<div class="form-group">
				<label  class="fontGray">Album Name</label>
				<input type="text" class="form-control" id="album_name_edit" placeholder="Enter album name here...." value="<?php echo $objCommon->html2text($getAlbumDetails['a_name'])?>">
			</div>
			<div class="form-group">
				<label class="fontGray">Album Description</label>
				<textarea class="form-control" rows="3" id="album_descr_edit"><?php echo $objCommon->html2textarea($getAlbumDetails['a_caption'])?></textarea>
			</div>
			<div class="form-group">
			<input type="hidden" id="update_album_id" value="<?php echo $edit_album_id?>" />
				<button type="button" class="btn btn-default" id="edit_alb" onclick="update_form_album();">Update</button>
			</div>
			<div class="form-group">
				<a href="javascript:;" class="pull-right del_album"   onclick="delAlbum('<?php echo $edit_album_id?>');">Delete Album</a>
			</div>
			<?php
		}
	}
}
?>