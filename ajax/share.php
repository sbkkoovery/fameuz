<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/share.php");
	$objCommon				   =	new common();
	$objShare					=	new share();
	$shareId					 =	$objCommon->esc($_POST['shareId']);
	$sharecat					=	$objCommon->esc($_POST['shareCat']);
	$shareWhoseId				=	$objCommon->esc($_POST['shareWhoseId']);
	$userId					  =	$_SESSION['userId'];
	if($sharecat != '' && $shareId != '' && $userId != ''){
		$_POST['share_category'] =	$sharecat;
		$_POST['share_content']  =	$shareId;
		$_POST['user_by']   		=	$userId;
		$_POST['user_whose']	 =    $shareWhoseId;
		$_POST['share_created']  =	date("Y-m-d H:i:s");
		$getShare				=	$objShare->getRow("share_content=".$shareId." and user_by =".$userId." and share_category=".$sharecat);
		if($getShare['share_id']==''){
			$_POST['share_status']=	1;
			$objShare->insert($_POST);
			//echo '<a data-sharecat="'.$sharecat.'" data-share="'.$shareId.'" href="javascript:;" class="shared share_btn"><i class="fa fa-share-alt"></i> Shared </a>';
			echo 'shared';
			//----notification table------------------------------------
			if($shareWhoseId){
				$myDetails				   		   	   =	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$userId);
				$displayName						  	 =	$objCommon->displayName($myDetails);
				$friend_id							   =	$shareWhoseId;
				$notiType								=	'image_share';
				$notiImg								 =	'';
				$notiDescr  	 	 				   	   =	'<b>'.$displayName.'</b> shared your image.';
				$notiUrl  								 =	SITE_ROOT.'user/single-image?type='.$sharecat.'&id='.$shareId;
				$objCommon->pushNotification($friend_id,$userId,$notiType,$notiImg,$notiDescr,$notiUrl);
			}
			//----------------------------------------------------------
			
		}else{
			if($getShare['share_status']==1){
				$upStatus		=	0;
				//echo '<a data-sharecat="'.$sharecat.'" data-share="'.$shareId.'" href="javascript:;" class="share_btn"><i class="fa fa-share-alt"></i> Share </a>';
				echo 'share';
			}else{
				$upStatus		=	1;
				//echo '<a data-sharecat="'.$sharecat.'" data-share="'.$shareId.'" href="javascript:;" class="shared share_btn"><i class="fa fa-share-alt"></i> Shared </a>';
				echo 'shared';
			}
			$objShare->updateField(array("share_status"=>$upStatus),"share_id=".$getShare['share_id']);
		}
	}
}
?>