<script type="text/javascript" src="<?php echo SITE_ROOT?>js/masonry.pkgd.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	initialiceMasonry();
	$('body').fameuzLightbox({
	class : 'lightBoxs',
	sidebar: 'photo',
	photos:{
		imageLoadUrl: '<?php echo SITE_ROOT.'ajax/popUpBox.php'?>',
		data_attr	: ['data-contentId','data-contentType','data-typeOf'],
		likeUrl	: '<?php echo SITE_ROOT?>ajax/like.php',
		shareUrl: '<?php echo SITE_ROOT?>ajax/share.php',
		workedUrl: '<?php echo SITE_ROOT?>ajax/worked_together.php',
		commentBoxUrl: '<?php echo SITE_ROOT.'widget/pop_image_comment_box.php' ?>',
		limitCount: 6,
		comments :{
			commentActionUrl:'<?php echo SITE_ROOT?>access/post_comment.php?action=add_comment',
			commentDelete: '<?php echo SITE_ROOT?>access/delete_comment.php',
		}
	},
	videos : {
		videoLoadUrl: '<?php echo SITE_ROOT.'ajax/popUpVideoUser.php'?>',
		data_attr	: ['data-vidEncrId'],
		likeUrl		: '<?php echo SITE_ROOT?>ajax/like_video.php',
		shareUrl	: '<?php echo SITE_ROOT?>ajax/share_video.php',
		commentBoxUrl:'<?php echo SITE_ROOT.'widget/pop_video_comment_box.php' ?>',
		comments :{
			commentActionUrl:'<?php echo SITE_ROOT?>access/post_video_comment.php?action=add_comment',
		}
	},
	skin: {
		next	: '<img src="<?php echo SITE_ROOT ?>fameuz_lightbox/images/next.png">',
		prev	: '<img src="<?php echo SITE_ROOT ?>fameuz_lightbox/images/prev.png">',
		reset	: '<i class="fa fa-refresh"></i>',
		close	: '<img src="<?php echo SITE_ROOT ?>fameuz_lightbox/images/close.png" width="15">',
		loader	: '<?php echo SITE_ROOT_AWS_IMAGES ?>images/ajax-loader.gif',
		review	: '<i class="fa fa-chevron-right"></i>',
		video	: '<?php echo SITE_ROOT ?>jw_player/six/six.xml',
	}
});
});
function initialiceMasonry(){
	
 var container = document.querySelector('.photoSearch');
    var msnry = new Masonry( container, {
      // options
      itemSelector: '.item',
      gutter: 0
    });
}
</script>
<?php
	include_once(DIR_ROOT."js/include/fameuz_lightbox_library.php");
	include_once(DIR_ROOT."class/search_functions.php");
	$obj_search_functions		=	new search_functions();
	$perPage 				   	 = 	30;
	$page 					  	= 	1;
	$type			  			=	'all';
	$searchQuery				 =	'';
	if(isset($_GET['filter_keywords'])&&$_GET['filter_keywords']!=''){
		$keyWord			  	 =	$objCommon->esc($_GET['filter_keywords']);
		$searchQuery		  	 =	" AND (u.first_name LIKE '%$keyWord%' OR u.last_name LIKE '%$keyWord%' OR u.display_name LIKE '%$keyWord%' OR a.ai_caption LIKE '%$keyWord%')";
	}
	if(isset($_GET['filter_category'])&&$_GET['filter_category']!=''){
		$type			  =	$objCommon->esc($_GET['filter_category']);
	}
if($type=='new-photos'){
	$sqlImagesList	=	"SELECT * FROM(SELECT a.ai_id,a.ai_images,a.user_id,a.ai_like_count,a.ai_comment_count,a.ai_caption,u.display_name,u.first_name,u.email,a.ai_created FROM album_images AS a LEFT JOIN users AS u ON a.user_id=u.user_id".$searchQuery." WHERE u.email_validation=1 AND u.status=1 ORDER BY a.ai_created DESC) AS t GROUP BY user_id ORDER BY ai_created DESC";
}else if($type=='popular'){
	$sqlImagesList	=	"SELECT a.ai_id,a.ai_images,a.user_id,a.ai_like_count,a.ai_comment_count,a.ai_caption,u.display_name,u.first_name,u.email,a.ai_created FROM album_images AS a LEFT JOIN users AS u ON a.user_id=u.user_id".$searchQuery." WHERE u.email_validation=1 AND u.status=1 GROUP BY a.ai_id ORDER BY a.ai_like_count DESC";
}else if($type=='top'){
	$sqlImagesList	=	"SELECT a.ai_id,a.ai_images,a.user_id,a.ai_like_count,a.ai_comment_count,a.ai_caption,u.display_name,u.first_name,u.email,a.ai_created,AVG(vote.aiv_vote) AS voteRate FROM album_images_vote AS vote LEFT JOIN album_images AS a ON a.ai_id=vote.ai_id LEFT JOIN users AS u ON a.user_id=u.user_id".$searchQuery." WHERE u.email_validation=1 AND u.status=1 GROUP BY a.ai_id ORDER BY voteRate DESC";
}else if($type=='last-visited'&&isset($_SESSION['userId'])){
	$sqlImagesList	=	"SELECT a.ai_id,a.ai_images,a.user_id,a.ai_like_count,a.ai_comment_count,a.ai_caption,u.display_name,u.first_name,u.email,a.ai_created FROM images_view AS iv LEFT JOIN album_images AS a ON iv.iv_content=a.ai_id LEFT JOIN users AS u ON a.user_id=u.user_id".$searchQuery." WHERE u.email_validation=1 AND u.status=1 AND iv.iv_cat=2 AND iv.iv_user_id=".$_SESSION['userId']." ORDER BY iv.iv_created DESC";
}else if($type=='all'){
	$sqlImagesList	=	"SELECT * FROM(SELECT a.ai_id,a.ai_images,a.user_id,a.ai_like_count,a.ai_comment_count,a.ai_caption,u.display_name,u.first_name,u.email,a.ai_created FROM album_images AS a LEFT JOIN users AS u ON a.user_id=u.user_id".$searchQuery." WHERE u.email_validation=1 AND u.status=1 ORDER BY a.ai_created DESC) AS t ORDER BY ai_created DESC";
}
if(isset($_GET['searchKey'])&&$_GET['searchKey']!=""){
	$searchKey		=	$objCommon->esc($_GET['searchKey']);
	$sqlImagesList	=	"SELECT * FROM(SELECT a.ai_id,a.ai_images,a.user_id,a.ai_like_count,a.ai_comment_count,a.ai_caption,u.display_name,u.first_name,u.email,a.ai_created FROM album_images AS a LEFT JOIN users AS u ON a.user_id=u.user_id WHERE u.email_validation=1 AND u.status=1 AND (u.first_name LIKE '%$searchKey%' OR u.last_name LIKE '%$searchKey%' OR u.display_name LIKE '%$searchKey%' OR a.ai_caption LIKE '%$searchKey%') ORDER BY a.ai_created DESC) AS t ORDER BY ai_created DESC";
}
/*$imageCount	=	$objAlbumImages->countRows("SELECT ai_id FROM album_images WHERE 1");
$start 					 =	($page * $limit) - $limit;
if(($objAlbums->countRows($sqlImagesList)) > ($page * $limit) ){
	$next = ++$page;
}
$newImagesList	=	$objAlbums->listQuery($sqlImagesList. " LIMIT ".$start.", ".$limit);*/


	
$start 						   =	0;
	$queryMemberSearch 		   =	$sqlImagesList . " limit " . $start . "," . $perPage; 
if(empty($_GET["rowcount"])) {
	$_GET["rowcount"] 		    =	$countSearchMembers	=	$objUsers->countRows($sqlImagesList);
}
$pages  = ceil($_GET["rowcount"]/$perPage);
$getMemberSearch			  	 =	$objUsers->listQuery($queryMemberSearch);
if($start < $countSearchMembers){
	?>
	<div class="photoSearch row">
	<?php
	if(count($getMemberSearch)>0){
		echo '<input type="hidden" class="pagenum" value="' . $page . '" /><input type="hidden" class="total-page" value="' . $pages . '" /><input type="hidden" id="total-count" value="'.$countSearchMembers.'" />';
		foreach($getMemberSearch as $allPhotoDetails){
			
			$displayNameFriend			  =	$objCommon->displayName($allPhotoDetails);
			//$vidRateCound				   =	$objCommon->round_to_nearest_half($allVideoDetails['voteAvg']);
			//$vidRateCoundStyle	   		  =	'style="width:'.($vidRateCound*20).'%;"';
	?>
            <div class="item col-sm-3">
                    <div class="potoImageFrame"><img src="<?php echo SITE_ROOT_AWS_IMAGES."uploads/albums/".$objCommon->html2text($allPhotoDetails['ai_images']); ?>" class="lightBoxs" data-contentId="<?php echo $allPhotoDetails['ai_id']; ?>" data-contentType="<?php echo $allPhotoDetails['user_id']; ?>" data-typeOf="<?php echo $type; ?>"  alt=""></div>
            <div class="clearfix"></div>
            <div class="dessi_bottom">
                
                <div class="clearfix"></div>
                <div class="by_bottom">
                    <div class="by">by <?php echo $displayNameFriend; ?></div>
                </div>
            </div>
            </div>
<?php
		}
		echo '</div>';
	}
}else{
	echo '<div class="row"><div class="col-md-6"><p>No results found....</p></div></div>';
}
?>