<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/likes.php");
	$objCommon				   =	new common();
	$ObjLikes					=	new likes();
	$likeId					  =	$objCommon->esc($_POST['likeId']);
	$likecat					 =	$objCommon->esc($_POST['likecat']);
	$imgUserId				   =	$objCommon->esc($_POST['imgUserId']);
	$videoId					 =	$objCommon->esc($_POST['videoId']);
	$userId					  =	$_SESSION['userId'];
	if($likecat != '' && $likeId != '' && $userId != ''){
		$_POST['like_cat']	   =	$likecat;
		$_POST['like_content']   =	$likeId;
		$_POST['like_user_id']   =	$userId;
		$_POST['like_img_user_by']=   $imgUserId;
		$_POST['like_time']	  =	date("Y-m-d H:i:s");
		$getLikes				=	$ObjLikes->getRow("like_content=".$likeId." and like_user_id =".$userId." and like_cat=".$likecat);
		$getlikeCount			=	$ObjLikes->getRowSql("SELECT count(like_id) AS likeCount FROM likes WHERE like_content=".$likeId." and like_cat=".$likecat." and like_status = 1");
		$likeCount			   =	0;
		$likeCount			   =	$getlikeCount['likeCount'];
		if($getLikes['like_id']==''){
			$_POST['like_status']=	1;
			$ObjLikes->insert($_POST);
			echo '<a href="javascript:;" class="commentLikeBtn liked" data-like="'.$likeId.'" data-userto="'.$imgUserId.'"><i class="fa fa-heart"></i><span> Liked</span></a><span class="seperator">-</span><i class="fa fa-thumbs-up"></i> <span>'.($likeCount+1).'</span>';
			//----notification table------------------------------------
			$myDetails				   		   	   =	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$userId);
			$displayName						  	 =	$objCommon->displayName($myDetails);
			$friend_id							   =	$imgUserId;
			$notiType								=	'video_like';
			$notiImg								 =	'';
			$notiDescr  	 	 				   	   =	'<b>'.$displayName.'</b> liked your Comment.';
			$notiUrl  								 =	SITE_ROOT.'video/watch/'.$videoId;
			$objCommon->pushNotification($friend_id,$userId,$notiType,$notiImg,$notiDescr,$notiUrl);
			//----------------------------------------------------------
		}else{
			if($getLikes['like_status']==1){
				$upStatus		=	0;
				echo '<a href="javascript:;" class="commentLikeBtn like" data-like="'.$likeId.'" data-userto="'.$imgUserId.'"><i class="fa fa-heart"></i><span> Like</span></a><span class="seperator">-</span><i class="fa fa-thumbs-up"></i> <span>'.($likeCount-1).'</span>';
			}else{
				$upStatus		=	1;
				echo '<a href="javascript:;" class="commentLikeBtn liked" data-like="'.$likeId.'" data-userto="'.$imgUserId.'"><i class="fa fa-heart"></i><span> Liked</span></a><span class="seperator">-</span><i class="fa fa-thumbs-up"></i> <span>'.($likeCount+1).'</span>';
			}
			$ObjLikes->updateField(array("like_status"=>$upStatus),"like_id=".$getLikes['like_id']);
		}
	}
}
?>
