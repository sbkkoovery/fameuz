<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/music.php");
	$objCommon				   =	new common();
	$objMusic				 	=	new music();
	$music_id					=	$objCommon->esc($_POST['music_id']);
	$userId					  =	$_SESSION['userId'];
	$friendId					=	$objCommon->esc($_POST['friendId']);
	if($music_id !='' && $userId !='' && $friendId !=''){
		$getMusicDetails		 =	$objMusic->getRow("music_id=".$music_id." AND user_id =".$friendId);
	?>
	<div class="modal-body ">
            <div class="form-group">
            <label>Title</label>
                <p><?php echo ($getMusicDetails['music_title'])?$objCommon->html2text($getMusicDetails['music_title']):''?></p>
            </div>
			<div class="form-group">
            <label>Artist</label>
                <p><?php echo ($getMusicDetails['music_artist'])?$objCommon->html2text($getMusicDetails['music_artist']):''?></p>
            </div>
            <div class="form-group">
            <label>Details</label>
                <p><?php echo ($getMusicDetails['music_descr'])?$objCommon->html2text($getMusicDetails['music_descr']):''?></p>
            </div>
			<div class="row">
            	<div class="col-sm-3">
					<div class="form-group">
						<?php
						$musicImage			=	($getMusicDetails['music_thumb'])?$getMusicDetails['music_thumb']:'audio-thump.jpg';
						?>
						<p><img src="<?php echo SITE_ROOT_AWS_MUSIC.'uploads/music/'.$musicImage?>"  width="50" height="50" class="musicImg"/></p>
					</div>
				</div>
				<div class="col-sm-9">
					<div id="myElement"></div>
				</div>
			</div>
      </div>
      <div class="modal-footer">
       
      </div>
	   
	<?php
	}
}
?>
<script>
jwplayer("myElement").setup({
	file: '<?php echo SITE_ROOT_AWS_MUSIC.'uploads/music/'.$objCommon->html2text($getMusicDetails['music_url'])?>',
	  'playlist': [{
		'file': '<?php echo SITE_ROOT_AWS_MUSIC.'uploads/music/'.$objCommon->html2text($getMusicDetails['music_url'])?>',
		'title': '<?php echo $objCommon->html2text($getMusicDetails['music_title'])?>'
	}],
	repeat: 'list',
	width: 250,
	height: 30
});
$('#myModal').on('hidden.bs.modal', function () {
  jwplayer("myElement").stop();
});
</script>