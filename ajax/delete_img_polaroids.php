<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/polaroids.php");
	$objCommon				   =	new common();
	$objPolaroids			    =	new polaroids();
	$imgid				  	   =	$objCommon->esc($_GET['imgid']);
	$userId					  =	$_SESSION['userId'];
	if($imgid !='' && $userId!=''){
		$getImageName			=	$objPolaroids->getRow("polo_id=".$imgid);
		$explAiImages	   		=	explode("/",$getImageName['polo_url']);
		$getAiImag1		  	  =	$explAiImages[0].'/thumb/'.$explAiImages[1];
		$getAiImag2		  	  =	$explAiImages[0].'/original/'.$explAiImages[1];
		$imgPath				 =	DIR_ROOT.'uploads/albums/'.$getImageName['polo_url'];
		$imgPath1				=	DIR_ROOT.'uploads/albums/'.$getAiImag1;
		$imgPath2				=	DIR_ROOT.'uploads/albums/'.$getAiImag2;
		unlink($imgPath);
		unlink($imgPath1);
		unlink($imgPath2);
		$objPolaroids->delete("polo_id=".$imgid);
	}
}
?>