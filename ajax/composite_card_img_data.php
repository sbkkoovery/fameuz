<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/users.php");
$objCommon				 =	new common();
$objUsers				  =	new users();
$userId				    =	$_SESSION['userId'];
$getModelDetails		   =	$objUsers->getRowSql("SELECT personal.p_gender,dat.ccd_data_table FROM users AS user LEFT JOIN personal_details AS personal ON user.user_id = personal.user_id LEFT JOIN composite_card_data as dat ON user.user_id = dat.user_id WHERE user.user_id=".$userId);
$modelDataMale			 =	array('model_chest'=>"Chest",'model_collar'=>"Collar",'model_hair'=>"Hair",'model_eyes'=>"Eyes",'model_height'=>"Height",'model_hips'=>"Hip",'model_jacket'=>"jacket",'model_shoes'=>"Shoes",'model_trousers'=>"Trousers",'model_waist'=>"Waist");
$modelDataFemale		   =	array('model_chest'=>"Bust",'model_hair'=>"Hair",'model_eyes'=>"Eyes",'model_height'=>"Height",'model_hips'=>"Hip",'model_dress'=>"Dress",'model_shoes'=>"Shoes",'model_waist'=>"Waist");
if($getModelDetails['ccd_data_table']){
	$ccd_data_tableArr	 =	explode(",",$getModelDetails['ccd_data_table']);
	$ccd_data_tableArr	 =	array_filter($ccd_data_tableArr);	
}
?> 
<form action="<?php echo SITE_ROOT?>access/add_composite_data.php" method="post">
<div class="modal-body">
	<div class="row">
	<?php
	if($getModelDetails['p_gender']==2){
		foreach($modelDataFemale AS $keyModelDet=>$allModelDet){
			if(count($ccd_data_tableArr) >0){
				if(in_array($keyModelDet,$ccd_data_tableArr)){
					$checkDataFemale		=	'checked';
				}else{
					$checkDataFemale		=	'';
				}
			}
		?>
		<div class="col-md-6">
			<label class="modelDataLabel"><input type="checkbox" name="modelDataArr[]" value="<?php echo $keyModelDet?>" <?php echo $checkDataFemale?> /> <?php echo $allModelDet?></label>
		</div>
		<?php
		}		
	}else{
		foreach($modelDataMale AS $keyModelDet=>$allModelDet){
			if(count($ccd_data_tableArr) >0){
				if(in_array($keyModelDet,$ccd_data_tableArr)){
					$checkDataMale		=	'checked';
				}else{
					$checkDataMale		=	'';
				}
			}
			?>
		<div class="col-md-6">
			<label class="modelDataLabel"><input type="checkbox" name="modelDataArr[]" value="<?php echo $keyModelDet?>" <?php echo $checkDataMale?> /> <?php echo $allModelDet?></label>
		</div>
		<?php
		}
	}
	?>
	</div>
</div>
<div class="modal-footer">
	<button type="submit" class="btn btn-primary">Save</button>
</div>
</form>
