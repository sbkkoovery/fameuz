<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/message.php");
	$objCommon				   =	new common();
	$objMessage				  =	new message();
	$userfrom					=	$objCommon->esc($_POST['userfrom']);
	$userId					  =	$_SESSION['userId'];
	if($userfrom != '' && $userId != ''){
		$objMessage->updateField(array("msg_trash_from"=>0),"msg_from=".$userId." AND msg_to=".$userfrom);
		$objMessage->updateField(array("msg_trash_to"=>0),"msg_to=".$userId." AND msg_from=".$userfrom);
	}
}
?>