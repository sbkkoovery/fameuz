<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/videos.php");
$objCommon				   =	new common();
$objVideos			  	   =	new videos();
$userId					  =	$_SESSION['userId'];
if($userId !=''){
	$getVideos	   		   =	$objVideos->listQuery("select * from videos  where user_id=".$userId."  order by video_created desc");
	
?>
	<div class="row albListShow">
		<div class="col-md-12">
			<div class="add_photo_album text-center video_upload">
				<form action='<?php echo SITE_ROOT?>ajax/ajaxvideo_album.php?path=<?php echo DIR_ROOT?>uploads/videos/' method="post" enctype="multipart/form-data" id="video_form">
					<i class="fa fa-video-camera"></i> Add Videos From Device<input type="file" class="album_uploader"  name="file" id="file_browse" multiple="multiple">
					<p class="or">OR</p>
					<input type="text" name="youtube_link" id="youtube_link" class="youtube_link" placeholder="External url ( eg: https://www.youtube.com )"/><button type="button" class="btn btn-default btn-sm submitLink" onclick="submitlink();">submit</button>
				</form>
			</div>
		</div>
	</div>
	<?php
	if(count($getVideos)>0){
	?>
	<div class="row albListShow">
	<div class="col-md-12">
	<div class="add_photo_album_list">
		<ul class="album_list_ul">
			<?php
			foreach($getVideos as $allVideos){
				if($allVideos['video_type']==1){
					$getAiImages				=	SITE_ROOT.'uploads/videos/'.$allVideos['video_thumb'];
				}else if($allVideos['video_type']==2){
					$getAiImages				=	$allVideos['video_thumb'];
					if (preg_match('%^https?://[^\s]+$%', $getAiImages)) {
						$getAiImages			=	$allVideos['video_thumb'];
					} else {
						$getAiImages			=	SITE_ROOT.'uploads/videos/'.$allVideos['video_thumb'];
					}
				}
				$vidUrl					 	 =	$objCommon->html2text($allVideos['video_encr_id']);
				?>
			<li><div class="vid"><img src="<?php echo $getAiImages?>" class="img-responsive"  /><a href="javascript:;" data-videncrid="<?php echo $vidUrl?>" class="playbtn lightBoxs"></a></div></li>
			<?php
			}
			?>
		</ul>
	</div>
	</div>
	</div>
	<?php
	}else{
		echo '<div class="row albListShow"><div class="col-md-12"><p class="no_img_album text-center">There are no videos in your profile</p></div></div>';
	}
	?>
    
	<div class="row">
		<div class="col-md-12">
		<div id="preview"></div>
		</div>
	</div>
	<?php
}
?>
<script type="text/javascript">
$(document).ready(function(){
	//$(".lightBoxs").lightBox({
//		videoSkin: '<?php echo SITE_ROOT?>jw_player/six/six.xml',
//		photosObj: true,
//		videoObj : true,
//		album	 : true,
//		defaults : false,
//		deleteCommentUrl : '<?php echo SITE_ROOT ?>access/delete_comment.php',
//		skins	: {
//			loader			: '<?php echo SITE_ROOT?>images/ajax-loader.gif',
//			next		    : '<img src="<?php echo SITE_ROOT?>fameuz_lightbox/images/next.png">',
//			prev			: '<img src="<?php echo SITE_ROOT?>fameuz_lightbox/images/prev.png">',
//			dismiss		  	: '<img src="<?php echo SITE_ROOT?>fameuz_lightbox/images/close.png" width="15">',
//			reviewIcon		: '<i class="fa fa-chevron-right"></i>',
//		},
//		photos	: {
//			fetchUrl: '<?php echo SITE_ROOT.'ajax/popUpImageUser.php'?>',
//			ajaxUrl	: '<?php echo SITE_ROOT.'widget/pop_image_comment_box.php' ?>',
//			comment	: {
//				CommentFormAction : '<?php echo SITE_ROOT?>access/post_comment.php?action=add_comment',
//			  	commentLikeUrl	  : '<?php echo SITE_ROOT?>ajax/like_comment.php',
//			},
//			shareItems	: {
//				likeAjaxUrl  : '<?php echo SITE_ROOT?>ajax/like.php',
//				shareAjaxUrl : '<?php echo SITE_ROOT?>ajax/share.php',
//				workedAjaxUrl: '<?php echo SITE_ROOT?>ajax/worked_together.php',
//			},
//		},
//		video	: {
//			url		: '<?php echo SITE_ROOT?>uploads/testuploads/1.mp4',
//			fetchUrl: '<?php echo SITE_ROOT.'ajax/popUpVideoUser.php'?>',
//			ajaxUrl	: '<?php echo SITE_ROOT; ?>ajax/popUpVideoMe.php',
//			comment	: {
//				CommentFormAction : '<?php echo SITE_ROOT?>access/post_video_comment.php?action=add_comment',
//				commentLikeUrl	  : '<?php echo SITE_ROOT?>ajax/like_video_comment_pop.php',
//			},
//			shareItems	: {
//				likeAjaxUrl  : '<?php echo SITE_ROOT?>ajax/like_video.php',
//				shareAjaxUrl : '<?php echo SITE_ROOT?>ajax/share_video.php',
//			},
//		},
//		shareItems	: {
//			likeAjaxUrl : '<?php echo SITE_ROOT?>ajax/like.php',
//			shareAjaxUrl: '<?php echo SITE_ROOT?>ajax/share.php',
//		}
//	});
});
</script>
<script type="text/javascript">
$(document).ready(function(e) {
	var inst = $.remodal.lookup[$('[data-remodal-id=album_pre_loader]').data('remodal')];
	$('#file_browse').on('change', function(){ 
	$("#preview").html('<div class="load_album_preloader"></div>');
		$("#video_form").ajaxForm(
				{
					target: '#preview',
					success:successCall
				
				}).submit();
				$(".albListShow").hide();
				//setTimeout(function () { inst.close();window.location.hash ='list';}, 3000);
	});
});
function successCall(){
	$(".load_album_preloader").hide();
}
function submitlink(){
	$("#preview").html('<div class="load_album_preloader"></div>');
	$("#video_form").ajaxForm(
	{
		target: '#preview',
		success:successCall
	
	}).submit();
	$(".albListShow").hide();
}
</script>