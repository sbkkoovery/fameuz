<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/videos.php");
	$objCommon				  =	new common();
	$objVideos			  =	new videos();
	$userId					 =	$_SESSION['userId'];
	if($_GET['hidImgId'] !=''  && $userId !=''){
		$hid_img_id			 =	$objCommon->esc($_GET['hidImgId']);
		$img_title	 		  =	$objCommon->esc($_GET['img_title']);
		$img_descr	 		  =	$objCommon->esc($_GET['img_descr']);
		$video_tags	 		 =	$objCommon->esc($_GET['video_tags']);
		$privacy	 		  	=	$objCommon->esc($_GET['privacy']);
		if($privacy ==1){
			$video_privacy	   =	'1,0,0,0';
		}else if($privacy ==2){
			$video_privacy	   =	'0,1,0,0';
		}else if($privacy ==4){
			$video_privacy	   =	'0,0,0,1';
		}
		$editedTime	         =	date('Y-m-d H:i:s');
		$objVideos->updateField(array('video_title'=>$img_title,'video_descr'=>$img_descr,'video_tags'=>$video_tags,'video_privacy'=>$video_privacy,'video_updated'=>$editedTime),"video_id=".$hid_img_id);
	}
}
?>