<?php 
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/album_images.php");
include_once(DIR_ROOT."class/images_view.php");
$objCommon			   =	new common();
$objAlbums			   =	new album_images();
$objImageView	 		=	new images_view();
$getUserDetails	  	  =	$objAlbums->getRowSql("SELECT profileImg.upi_img_url,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 WHERE user.status=1 AND user.user_id=".$_SESSION['userId']);
	$imageId		 	 =	244;
	$im_type		 	 =	2;
	$albumId			 =	($_GET['albumId'])?$objCommon->esc($_GET['albumId']):0;
	if($im_type==1){
		$imageDetails    =	$objAlbums->getRowSql("SELECT photos.user_id,photos.photo_url AS img_url,photos.photo_id AS img_id,photos.photo_descr as img_descr,photos.photo_created AS img_created,IF(photos.photo_id != '', 1, '') 		AS img_type,photos.photo_like_count AS likeCount,photos.photo_comment_count AS commentCount,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,count(DISTINCT likes.like_id) AS likeCount,group_concat(DISTINCT likes.like_user_id) AS youLike,count(DISTINCT share.share_id) AS shareCount,group_concat(DISTINCT share.user_by) AS youShare
	FROM user_photos as photos
	LEFT  JOIN users AS user ON photos.user_id = user.user_id
	LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
	LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
	LEFT JOIN likes ON photos.photo_id = likes.like_content AND likes.like_cat =".$im_type." AND likes.like_status=1
	LEFT JOIN share  ON  photos.photo_id = share.share_content AND share.share_category = ".$im_type." AND share.share_status=1
	WHERE photos.photo_id=".$imageId);	
	}else if($im_type==2){
		$imageDetails    =	$objAlbums->getRowSql("SELECT photos.user_id,photos.ai_images AS img_url,ai_id AS img_id,photos.ai_caption AS img_descr,photos.ai_created AS img_created,IF(photos.ai_id != '', 2, '') AS img_type,photos.ai_like_count AS likeCount,photos.ai_comment_count AS commentCount,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,count(DISTINCT likes.like_id) AS likeCount,group_concat(DISTINCT likes.like_user_id) AS youLike,count(DISTINCT share.share_id) AS shareCount,group_concat(DISTINCT share.user_by) AS youShare
		FROM album_images  AS photos
		LEFT  JOIN users AS user ON photos.user_id = user.user_id
		LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
		LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
		LEFT JOIN likes ON photos.ai_id = likes.like_content AND likes.like_cat =".$im_type." AND likes.like_status=1
		LEFT JOIN share  ON photos.ai_id = share.share_content AND share.share_category = ".$im_type." AND share.share_status=1
		WHERE ai_id=".$imageId);
	}
$arr				=	array("first_name"=>$imageDetails['first_name']);
echo	$text	=	json_encode($arr);

?>