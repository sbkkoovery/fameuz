<?php
@session_start();
ob_start();
include_once("../includes/site_root.php");
if($_SESSION['userId']){
	include_once(DIR_ROOT."class/message.php");
	$objMessage	  		=	new message();
	$userId				=	$_SESSION['userId'];
	$countNotificationMsg  =	$objMessage->getRowSql("SELECT count(tab.msg_from) AS totalMsgCount FROM (SELECT msg_from FROM message WHERE msg_to=".$userId." AND msg_read_status=0 group by msg_from) AS tab");
	if($countNotificationMsg['totalMsgCount'] >0){
		echo '<sup>'.$countNotificationMsg['totalMsgCount'].'</sup>';
	}
}
?>