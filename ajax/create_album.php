<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/album.php");
	$objCommon				   =	new common();
	$objAlbum					=	new album();
	$_POST['a_name']			 =	$album_name				  =	$objCommon->esc($_GET['album_name']);
	$_POST['a_caption']		  =	$album_descr				 =	$objCommon->esc($_GET['album_descr']);
	$_POST['user_id']			=	$_SESSION['userId'];
	$_POST['a_created']		  =	date("Y-m-d H:i:s");
	$_POST['a_edited']		   =	date("Y-m-d H:i:s");
	if($album_name !='' && $_POST['user_id']!=''){
		$objAlbum->insert($_POST);
		echo $albumId				 =	$objAlbum->insertId();
	}
}
?>