<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/likes.php");
	$objCommon				   =	new common();
	$ObjLikes					=	new likes();
	$likeId					  =	$objCommon->esc($_POST['likeId']);
	$likeCat					 =	$objCommon->esc($_POST['likeCat']);
	$noti_whome				  =	$objCommon->esc($_POST['noti_whome']);
	$userId					  =	$_SESSION['userId'];
	if($likeCat != '' && $likeId != '' && $userId != ''){
		$_POST['like_cat']	   =	$likeCat;
		$_POST['like_content']   =	$likeId;
		$_POST['like_user_id']   =	$userId;
		$_POST['like_img_user_by']=   $noti_whome;
		$_POST['like_time']	  =	date("Y-m-d H:i:s");
		$getLikes				=	$ObjLikes->getRow("like_content=".$likeId." and like_user_id =".$userId." and like_cat=".$likeCat);
		if($getLikes['like_id']==''){
			$_POST['like_status']=	1;
			$ObjLikes->insert($_POST);
			//----notification table------------------------------------
			$myDetails				   		   	   =	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$userId);
			$displayName						  	 =	$objCommon->displayName($myDetails);
			if($likeCat==1){
				$ObjLikes->build_result_insert("update user_photos SET photo_like_count=photo_like_count+1 where photo_id=".$likeId);
				$notiType		  				=	'likes';
				$notiDescr   	 	 			   =	'<b>'.$displayName.'</b> liked your <b>image</b>.';
				$notiUrl  						 =	SITE_ROOT.'user/single-image?type='.$likeCat.'&id='.$likeId;
			}else if($likeCat==2){
				$ObjLikes->build_result_insert("update album_images SET ai_like_count=ai_like_count+1 where ai_id=".$likeId);
				$notiType		  				=	'likes';
				$notiDescr   	 	 			   =	'<b>'.$displayName.'</b> liked your <b>image</b>.';
				$notiUrl  						 =	SITE_ROOT.'user/single-image?type='.$likeCat.'&id='.$likeId;
			}else if($likeCat==4){
				$ObjLikes->build_result_insert("update videos SET video_likes=video_likes+1 where video_id=".$likeId);
				$getVideoId			  		  =	$ObjLikes->getRowSql("SELECT video_encr_id FROM videos WHERE video_id =".$likeId);
				$notiType		  				=	'video_like';
				$notiDescr   	 	 			   =	'<b>'.$displayName.'</b> liked your <b>video</b>.';
				$notiUrl  						 =	SITE_ROOT.'video/watch/'.$getVideoId['video_encr_id'];
			}else if($likeCat==6){
				$ObjLikes->build_result_insert("update user_profile_image SET upi_like_count=upi_like_count+1 where upi_id=".$likeId);
				$notiType		  				=	'likes';
				$notiDescr   	 	 			   =	'<b>'.$displayName.'</b> liked your <b>image</b>.';
				$notiUrl  						 =	SITE_ROOT.'user/single-image?type='.$likeCat.'&id='.$likeId;
			}else if($likeCat==9){
				$getAlbumName					=	$ObjLikes->getRowSql("SELECT a_name FROM album WHERE a_id =".$likeId);
				$notiType		 				=	'album_like';
				$notiDescr   	 	 			   =	'<b>'.$displayName."</b> liked your <b>album '".$objCommon->html2text($getAlbumName['a_name'])."'</b>.";
				$notiUrl  						 =	SITE_ROOT.'user/my-album';
			}
			echo '<a class="like_btn liked" data-like="'.$likeId.'" data-category="'.$likeCat.'" data-userby="'.$noti_whome.'" href="javascript:;"><i class="fa fa-heart"></i> <span class="text">Liked</span> </a>';
			$friend_id							   =	$noti_whome;
			$notiImg								 =	'';
			$objCommon->pushNotification($friend_id,$userId,$notiType,$notiImg,$notiDescr,$notiUrl);
			//----------------------------------------------------------
		}else{
			if($getLikes['like_status']==1){
				$upStatus		=	0;
				if($likeCat==1){
					$ObjLikes->build_result_insert("update user_photos SET photo_like_count=photo_like_count-1 where photo_id=".$likeId);
				}else if($likeCat==2){
					$ObjLikes->build_result_insert("update album_images SET ai_like_count=ai_like_count-1 where ai_id=".$likeId);
				}else if($likeCat==4){
				$ObjLikes->build_result_insert("update videos SET video_likes=video_likes-1 where video_id=".$likeId);
				}else if($likeCat==6){
					$ObjLikes->build_result_insert("update user_profile_image SET upi_like_count=upi_like_count-1 where upi_id=".$likeId);
				}
				echo '<a class="like_btn" data-like="'.$likeId.'" data-category="'.$likeCat.'" data-userby="'.$noti_whome.'" href="javascript:;"><i class="fa fa-heart"></i> <span class="text">Like</span> </a>';
			}else{
				$upStatus		=	1;
				if($likeCat==1){
					$ObjLikes->build_result_insert("update user_photos SET photo_like_count=photo_like_count+1 where photo_id=".$likeId);
				}else if($likeCat==2){
					$ObjLikes->build_result_insert("update album_images SET ai_like_count=ai_like_count+1 where ai_id=".$likeId);
				}else if($likeCat==4){
				$ObjLikes->build_result_insert("update videos SET video_likes=video_likes+1 where video_id=".$likeId);
				}else if($likeCat==6){
					$ObjLikes->build_result_insert("update user_profile_image SET upi_like_count=upi_like_count+1 where upi_id=".$likeId);
				}
				echo '<a class="like_btn liked" data-like="'.$likeId.'" data-category="'.$likeCat.'" data-userby="'.$noti_whome.'" href="javascript:;"><i class="fa fa-heart"></i> <span class="text">Liked</span> </a>';
			}
			$ObjLikes->updateField(array("like_status"=>$upStatus),"like_id=".$getLikes['like_id']);
		}
	}
}
exit;
?>