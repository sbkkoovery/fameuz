<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/polaroids.php");
	$objCommon				  =	new common();
	$objPolaroids			   =	new polaroids();
	$userId					 =	$_SESSION['userId'];
	if($_GET['hidImgId'] !=''  && $userId !=''){
		$hid_img_id			 =	$objCommon->esc($_GET['hidImgId']);
		$img_descr	 		  =	$objCommon->esc($_GET['img_descr']);
		$img_order	 		  =	$objCommon->esc($_GET['img_order']);
		$ai_caption			 =	$img_descr;
		$editedTime	         =	date('Y-m-d H:i:s');
		$objPolaroids->updateField(array('polo_descr'=>$ai_caption,'polo_edited'=>$editedTime,'polo_order'=>$img_order),"polo_id=".$hid_img_id);
		
	}
}
?>