<!-- 
http://10.0.0.8/flagday/api/index.php?method=mapimages&parse=[{%22language%22:%222%22}]
-->

<?php

define('ABS_PATH','../');
require_once ABS_PATH.'classes/rest_server.php';
require_once ABS_PATH.'classes/utils.php';
require_once ABS_PATH.'classes/validate.php';
require_once ABS_PATH.'classes/logger.php';
require_once ABS_PATH . 'classes/upload_hex_image.php';

	
	class mapimagesModel extends indexModel
		{
			private $state;
			
			public function __construct($state)
				{
					
					$this->state=$state;
					$this->mapimages_list();
				
				}
			
			private function mapimages_list()
				{	
					
					$parse			=	$this->state->request_vars['parse'];
					$result			=	end(json_decode($parse));
					
					if (trim($result->language)=="")
        			{
						RestServer::send_response(400, array("status"=>"0",'error' => 'Please Specify language mode'));
       	 			}
       	 			
					
					
					if(trim($result->language)==1)
					$location = 'e_name_eng as  locationname';
					else
					$location = 'e_name_ar as  locationname';
					$sql = "select e_id as locationid,$location,e_name_eng from emirates where 1";
					$emiratesary	=	$this->getdbcontents_sql($sql);
				    $emirates =array();
				    
				   foreach($emiratesary as $key=>$value)
				   {
					   if($value['e_name_eng'] =='Abu Dhabi')
					   $emirates[0] = $value;
					  else if($value['e_name_eng'] =='Dubai')
					   $emirates[1] = $value;
					  else if($value['e_name_eng'] =='Sharjah')
					   $emirates[2] = $value;
					  else if($value['e_name_eng'] =='Ajman')
					   $emirates[3] = $value;
					  else if(trim($value['e_name_eng']) =='Umm al-Quwain')
					     {
						   $value['e_name_eng'] = str_replace("-"," ",$value['e_name_eng']);
					       $emirates[4] = $value;
						 }
					  else if($value['e_name_eng'] =='Ras al-Khaimah')
					     $emirates[5] = $value;
					     
					  else if($value['e_name_eng'] =='Fujairah')
					   $emirates[6] = $value;
					   
				   }
				    ksort($emirates);	
					foreach($emirates as $key=>$value)
					{
						$files= array();
						  $sql	 =	"SELECT max(fl.f_id) as id
										FROM flags fl
										 JOIN  flag_more  flm 
										on fl.f_id=flm.f_id
										 JOIN  users  usr 
										on fl.user_id=usr.user_id
										WHERE    fl.f_status=1 and flm.fm_status=1 and flm.fm_type =1
										and fl.f_location like '%".$value['e_name_eng']."%' ";				
						$max	=	end($this->getdbcontents_sql($sql));
					
						 $sql	 =	"SELECT fl.f_id,flm.fm_type ,flm.fm_url,usr.name,usr.img_url as profile,fl.f_location as locationname
										FROM flags fl
										 JOIN  flag_more  flm 
										on fl.f_id=flm.f_id
										 JOIN  users  usr 
										on fl.user_id=usr.user_id
										WHERE   fl.f_id =".$max['id']." order by fl.f_id desc limit 0,1";					
						$files	=	end($this->getdbcontents_sql($sql));
						$emirates[$key]['filetype'] = $files['fm_type'];
						
						if($files['fm_url'] !="")
						$emirates[$key]['url'] = constant("CONST_HOST_ADDRESS")."uploads/flags_images/".$files['fm_url'];
						else
						$emirates[$key]['url'] = "";
						
						$emirates[$key]['name'] = $files['name'];
						if($files['profile']!="")
						$emirates[$key]['profile'] =constant("CONST_HOST_ADDRESS").'uploads/profile/'.$files['profile'];
						
					if(trim($result->language)==1)	
								{			
				    $sql	 =	"SELECT count(user_id) as count
										FROM users usr join emirates em on usr.emirates=em.e_id WHERE em.e_name_eng LIKE '%".$value['locationname']."%'  ";
									}
				  if(trim($result->language)==2)	
								{	
				$sql	 =	"SELECT count(user_id) as count
				  						FROM users usr join emirates em on usr.emirates=em.e_id WHERE em.e_name_ar LIKE '%".$value['locationname']."%'  ";
									}
						$count	=	end($this->getdbcontents_sql($sql));
						
						$emirates[$key]['count'] =(string)intval($count['count']);
						
								if(trim($result->language)==1)	
								{
								 $emirates[$key]['count'] = "Participants ".$emirates[$key]['count'];
								}
								if(trim($result->language)==2)	
								{
									$emirates[$key]['count'] = " مشارك ".$emirates[$key]['count'];
								}
					}
							
					$content=array();
					
				
					if(trim($result->language)==1)	
       	 			{				
					 $sql	 =	"SELECT page_id  as id ,page_title as title ,page_content as content FROM pages WHERE page_id=2 and status=1";
       	 			$content	=	end($this->getdbcontents_sql($sql));      	 			

       	 			}
       	 			else if(trim($result->language)==2)					
					
					{
					 	$sql	 =	"SELECT page_id  as id ,page_title as title ,page_content as content FROM ar_pages WHERE page_id=2 and status=1";
						$content	=	end($this->getdbcontents_sql($sql));  
   	 			
					}
	
					if(!empty($content))	
					{						
					  $content["content"]=   strip_tags($content['content']);
					  $content["content"]=    str_replace("&quot;", "'",$content['content']);
					  $content["content"]=    str_replace(array("\r","\n","\t"), "",$content['content']);

					}
					//	$emirates[1]['count'] = $emirates[1]['count']."a";print_r($emirates[1]['count']);exit;
				//print_r($emirates);exit;
				
					if(!empty($emirates))							
						RestServer::send_response(201,array("status"=>"1","images"=>$emirates,"content"=>$content));	
						
					else
						RestServer::send_response(400, array("status"=>"0",'error' => 'No records found'));

       	 		
       	 		}	
			}
	
			
		?>
