<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/polaroids.php");
	$objCommon				   =	new common();
	$objPolaroids			    =	new polaroids();
	$userId					  =	$_SESSION['userId'];
	if(count($_POST['hid_img_id'])>0 && $userId !=''){
		$hid_img_id			  =	$_POST['hid_img_id'];
		$img_descr	 		   =	$_POST['img_descr'];
		$img_order			   =	$_POST['img_order'];
		foreach($hid_img_id as $keyHid=>$allHidId){
			if($img_descr[$keyHid]){
				$ai_caption	  =	$objCommon->esc($img_descr[$keyHid]);
				$polo_order	  =	$objCommon->esc($img_order[$keyHid]);
				$objPolaroids->updateField(array('polo_descr'=>$ai_caption,'polo_order'=>$polo_order),"polo_id=".$allHidId);
			}
		}
	}
}
?>