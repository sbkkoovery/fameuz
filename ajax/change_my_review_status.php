<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/user_reviews.php");
	$objCommon				   =	new common();
	$objUserReviewStatus		 =	new user_reviews();
	$review_id				   =	$objCommon->esc($_GET['review_id']);
	$user_id					 =	$_SESSION['userId'];
	$changeStat				  =	$objCommon->esc($_GET['changeStat']);
	$changeStatme				=	$objCommon->esc($_GET['changeStatme']);
	if($review_id !='' && $user_id !='' && $changeStat != ''){
		$objUserReviewStatus->updateField(array('review_user_to_status'=>$changeStat),"review_id=".$review_id);
	}
	if($review_id !='' && $user_id !='' && $changeStatme != ''){
		$objUserReviewStatus->updateField(array('review_status'=>$changeStatme),"review_id=".$review_id);
	}
}
?>