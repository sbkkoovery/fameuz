<?php
	include_once(DIR_ROOT."class/search_functions.php");
	$obj_search_functions		=	new search_functions();
	$perPage 				   	 = 	20;
	$page 					  	= 	1;
	$search_keyword			  =	$objCommon->esc($_GET['filter_keywords']);
	$search_type			  	 =	$objCommon->esc($_GET['filter_category']);
	if($search_keyword){
		$sqlMemberSearch		 =	$obj_search_functions->search($search_keyword);
	}else if($search_type){
		if($search_type=='new_videos'){
			$sqlMemberSearch			   =	"SELECT vid.video_url,video_encr_id,vid.video_thumb,vid.video_duration,vid.video_type,vid.video_title,vid.video_visits,vid.video_created,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz,avg(vote.vv_vote) as voteAvg,case when (promo.promo_id !='') then 1 else 0 END AS promoted_video 
											FROM videos AS vid 
											LEFT JOIN users AS user ON  vid.user_id = user.user_id
											LEFT JOIN user_social_links AS social ON user.user_id=social.user_id  
											LEFT JOIN videos_vote AS vote ON vid.video_id	=	vote.video_id
											LEFT JOIN promotion AS promo ON vid.video_id = promo.promo_content_id AND promo.promo_type='video' AND  promo.promo_country LIKE '%,".$countryId.",%' AND promo.promo_category LIKE '%".$myCategoryId."%' AND DATE(NOW()) between promo.promo_start_date and promo.promo_end_date AND promo.promo_admin_status=1 AND promo.promo_status=1
											WHERE video_status=1 AND user.status=1 AND user.email_validation =1 ORDER BY promoted_video DESC,vid.video_created DESC";
		}else if($search_type=='top_videos'){
			$sqlMemberSearch			= 	"SELECT vid.video_id,vid.video_url,video_encr_id,vid.video_thumb,vid.video_duration,vid.video_type,vid.video_title,vid.video_visits,vid.video_created,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz,SUM(vote.vv_vote) as voteSum,avg(vote.vv_vote) as voteAvg
										FROM videos AS vid  
										LEFT JOIN users AS user ON vid.user_id = user.user_id 
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
										LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
										LEFT JOIN videos_vote AS vote ON vid.video_id	=	vote.video_id
										WHERE video_status=1 AND user.status=1 AND user.email_validation =1 
										GROUP BY vid.video_id 
										ORDER BY voteSum DESC,video_created DESC";
		}else if($search_type=='popular_videos'){
			$sqlMemberSearch			=	"SELECT vid.video_url,video_encr_id,vid.video_thumb,vid.video_duration,vid.video_type,vid.video_title,vid.video_visits,vid.video_created,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz,avg(vote.vv_vote) as voteAvg,case when (promo.promo_id !='') then 1 else 0 END AS promoted_video 
										FROM videos AS vid 
										LEFT JOIN users AS user ON  vid.user_id = user.user_id
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN videos_vote AS vote ON vid.video_id	=	vote.video_id
										LEFT JOIN promotion AS promo ON vid.video_id = promo.promo_content_id AND promo.promo_type='video' AND  promo.promo_country LIKE '%,".$countryId.",%' AND promo.promo_category LIKE '%".$myCategoryId."%' AND DATE(NOW()) between promo.promo_start_date and promo.promo_end_date AND promo.promo_admin_status=1 AND promo.promo_status=1  
										WHERE vid.video_status=1 AND user.status=1 AND user.email_validation =1 
										GROUP BY vid.video_id
										ORDER BY promoted_video DESC,vid.video_created DESC";
		}else if($search_type=='last_visited'){
		$sqlMemberSearch			   =	"SELECT vid.video_id,vid.video_url,video_encr_id,vid.video_thumb,vid.video_duration,vid.video_type,vid.video_title,vid.video_descr,vid.video_visits,vid.video_likes,vid.video_created,user.first_name,user.user_id,user.last_name,user.display_name,user.email,social.usl_fameuz,cat.c_name,profileImg.upi_img_url,avg(vote.vv_vote) as voteAvg
											FROM videos AS vid
											LEFT JOIN users AS user ON  vid.user_id = user.user_id
											LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
											LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
											LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
											LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id
											LEFT JOIN videos_vote AS vote ON vid.video_id	=	vote.video_id 
											LEFT JOIN videos_views AS view ON vid.video_id = view.video_id
											WHERE vid.video_status=1 AND user.status=1 AND user.email_validation =1 AND view.user_id =".$_SESSION['userId']." GROUP BY vid.video_id ORDER BY view.vvs_created DESC";
		}else if($search_type=='all_videos'){
		$sqlMemberSearch			   =	"SELECT vid.video_id,vid.video_url,video_encr_id,vid.video_thumb,vid.video_duration,vid.video_type,vid.video_title,vid.video_descr,vid.video_visits,vid.video_likes,vid.video_created,user.first_name,user.user_id,user.last_name,user.display_name,user.email,social.usl_fameuz,cat.c_name,profileImg.upi_img_url,avg(vote.vv_vote) as voteAvg,case when (promo.promo_id !='') then 1 else 0 END AS promoted_video
											FROM videos AS vid
											LEFT JOIN users AS user ON  vid.user_id = user.user_id
											LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
											LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
											LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
											LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id
											LEFT JOIN videos_vote AS vote ON vid.video_id	=	vote.video_id 
											LEFT JOIN promotion AS promo ON vid.video_id = promo.promo_content_id AND promo.promo_type='video' AND  promo.promo_country LIKE '%,".$countryId.",%' AND promo.promo_category LIKE '%".$myCategoryId."%' AND DATE(NOW()) between promo.promo_start_date and promo.promo_end_date AND promo.promo_admin_status=1 AND promo.promo_status=1
											WHERE vid.video_status=1 AND user.status=1 AND user.email_validation =1 GROUP BY vid.video_id ORDER BY promoted_video DESC,vid.video_created DESC";
		}
	}else{
		$sqlMemberSearch			   =	"SELECT vid.video_id,vid.video_url,video_encr_id,vid.video_thumb,vid.video_duration,vid.video_type,vid.video_title,vid.video_descr,vid.video_visits,vid.video_likes,vid.video_created,user.first_name,user.user_id,user.last_name,user.display_name,user.email,social.usl_fameuz,cat.c_name,profileImg.upi_img_url,avg(vote.vv_vote) as voteAvg,case when (promo.promo_id !='') then 1 else 0 END AS promoted_video
											FROM videos AS vid
											LEFT JOIN users AS user ON  vid.user_id = user.user_id
											LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
											LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
											LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
											LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id
											LEFT JOIN videos_vote AS vote ON vid.video_id	=	vote.video_id
											LEFT JOIN promotion AS promo ON vid.video_id = promo.promo_content_id AND promo.promo_type='video' AND  promo.promo_country LIKE '%,".$countryId.",%' AND promo.promo_category LIKE '%".$myCategoryId."%' AND DATE(NOW()) between promo.promo_start_date and promo.promo_end_date AND promo.promo_admin_status=1 AND promo.promo_status=1
											WHERE vid.video_status=1 AND user.status=1 AND user.email_validation =1 GROUP BY vid.video_id ORDER BY promoted_video DESC,vid.video_created DESC";
	}
$start 						   =	0;
$queryMemberSearch 		   	   =	$sqlMemberSearch . " limit " . $start . "," . $perPage; 
if(empty($_GET["rowcount"])) {
	$_GET["rowcount"] 		    =	$countSearchMembers	=	$objUsers->countRows($sqlMemberSearch);
}
$pages  = ceil($_GET["rowcount"]/$perPage);
$getMemberSearch			  	 =	$objUsers->listQuery($queryMemberSearch);
if($start < $countSearchMembers){
	?>
	<div class="row">
	<?php
	if(count($getMemberSearch)>0){
		echo '<input type="hidden" class="pagenum" value="' . $page . '" /><input type="hidden" class="total-page" value="' . $pages . '" /><input type="hidden" id="total-count" value="'.$countSearchMembers.'" />';
		foreach($getMemberSearch as $allVideoDetails){
			if($allVideoDetails['video_type']==1){
				$getAiImages				=	SITE_ROOT_AWS_VIDEOS.'uploads/videos/'.$allVideoDetails['video_thumb'];
				$vidUrl					 =	SITE_ROOT.'uploads/videos/'.$allVideoDetails['video_url'];
			}else if($allVideoDetails['video_type']==2){
				if (preg_match('%^https?://[^\s]+$%',$allVideoDetails['video_thumb'])) {
					$getAiImages			=	$allVideoDetails['video_thumb'];
				} else {
					$getAiImages			=	SITE_ROOT_AWS_VIDEOS.'uploads/videos/'.$allVideoDetails['video_thumb'];
				}
				$vidUrl					 =	$objCommon->html2text($allVideoDetails['video_url']);
			}
			$displayNameFriend			  =	$objCommon->displayName($allVideoDetails);
			$vidcreatedTime				 =	$objCommon->time_elapsed_string(strtotime($objCommon->local_time($allVideoDetails['video_created'])));
			$vidRateCound				   =	$objCommon->round_to_nearest_half($allVideoDetails['voteAvg']);
			$vidRateCoundStyle	   		  =	'style="width:'.($vidRateCound*20).'%;"';
	?>
		   <div class="col-sm-12 videoSearch">
				<div class="item">
				<div class="row">	<div class="col-xs-12 col-sm-5 col-md-3">
						<a href="<?php echo SITE_ROOT.'video/watch/'.$objCommon->html2text($allVideoDetails['video_encr_id'])?>" class="vidimg pull-left"><img src="<?php echo $getAiImages?>" alt="video" width="100%" class="img-responsive"/>
							<div class="video_duration"><?php echo $allVideoDetails['video_duration']?></div>
						</a>
					</div>
					<div class="col-xs-12 col-sm-7 col-md-9 vidDescr">
						<h2><a href="<?php echo SITE_ROOT.'video/watch/'.$objCommon->html2text($allVideoDetails['video_encr_id'])?>"><?php echo $objCommon->html2text($allVideoDetails['video_title'])?></a></h2>
						<div class="row">
						<div class="col-sm-5"><p class="publishedBy"><i class="fa_publish"></i> <span class="publishedBySpan">Published By:</span> <a href="<?php echo SITE_ROOT.$objCommon->html2text($allVideoDetails['usl_fameuz'])?>"><?php echo $displayNameFriend?></a></p></div><div class="col-sm-4"><p class="publishedBy"><i class="fa_clock"></i> <?php echo $vidcreatedTime?></p></div></div>
                        
						<p><?php echo $objCommon->limitWords($allVideoDetails['video_descr'],120)?></p>
						<div class="row"></div>
						<div class="likeWidget">
							<div class="row">
                            	<div class="col-sm-2 col-md-2 col-lg-2"><span><i class="fa_heart_gery"></i> <?php echo number_format($allVideoDetails['video_likes']);?>Like</span></div>
								<div class="col-sm-2 col-md-2 col-lg-2"><span><div class="rating_box"><div <?php echo $vidRateCoundStyle?> class="rating_yellow"></div></div></span></div>
								<div class="col-sm-5"><span><i class="fa_eye"></i> <span class="vidVisits"><?php echo number_format($allVideoDetails['video_visits']);?> Views </span></span></div>
								<?php
								if($allVideoDetails['promoted_video']==1){
									echo '<div class="col-sm-2"><div class="promoted_video"><i class="fa fa-external-link-square"></i> Promoted</div></div>';
								}
								?>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
<?php
		}
		echo '</div>';
	}
}else{
	echo '<div class="row"><div class="col-md-6"><p>No results found....</p></div></div>';
}
?>
	