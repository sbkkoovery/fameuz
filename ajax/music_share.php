<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/share.php");
	$objCommon				   =	new common();
	$objShare					=	new share();
	$musicId					 =	$objCommon->esc($_POST['musicId']);
	$userId					  =	$_SESSION['userId'];
	if($musicId != '' && $userId != ''){		
		$getMusicDetails		 =	$objShare->getRowSql("SELECT music.music_id,music.user_id,share.share_id FROM music LEFT JOIN share ON music.music_id = share.share_content AND share.share_category=14 WHERE music.music_id =".$musicId);
		$notiType				=	'music_share';
		$myDetails			   =	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$userId);
		$displayName			 =	$objCommon->displayName($myDetails);
		$notiUrl  				 =	SITE_ROOT.'music?musicid='.$musicId;
		if($getMusicDetails['share_id']!=''){
			$share_created  	   =	date("Y-m-d H:i:s");
			$objShare->updateField(array("share_created"=>$share_created),"share_id=".$getMusicDetails['share_id']);
		}else{
			$_POST['user_by']   		=	$notiFrom	  =	$userId;
			$_POST['user_whose']   	 =	$notiTo		=	$getMusicDetails['user_id'];
			$_POST['share_content']  =	$musicId;
			$_POST['share_category'] =	14;
			$_POST['share_status']   =	1;
			$_POST['share_created']  =	date("Y-m-d H:i:s");
			$objShare				=	$objShare->insert($_POST);
			$notiDescr  	 	 	   =	'<b>'.$displayName.'</b> shared your music.';
			$objCommon->pushNotification($notiTo,$notiFrom,$notiType,$notiImg,$notiDescr,$notiUrl);
		}
	}
}
?>