<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/share.php");
	$objShare	=	new share();
	include_once(DIR_ROOT."class/common_class.php");
	$objCommon				   =	new common();
	$userId					  =	$_SESSION['userId'];	
	if(isset($_GET['dataID'],$_GET['dataCat'])&&$_GET['dataID']&&$_GET['dataCat']){
		$dataID	=	$objCommon->esc($_GET['dataID']);
		$dataCat	=	$objCommon->esc($_GET['dataCat']);
		if($dataCat=='shared'){
			$shareData	=	$objShare->getRow("user_by=$userId AND share_id=$dataID");
			if(count($shareData)>1){
				$objShare->updateField(array("share_status"=>0),"user_by=$userId AND share_id=$dataID");
				echo "success";
			}
		}else if($dataCat=='create_posts'){
			include_once(DIR_ROOT."class/posts.php");
			$objPosts		 =	new posts();
			$postData		=	$objPosts->getRow("user_id=$userId AND post_id=$dataID");
			if(count($postData)>1){
				$objPosts->updateField(array("post_status"=>0),"user_id=$userId AND post_id=$dataID");
				$shareStatus	=	$objShare->getRow("share_category=10 AND share_content=$dataID");
				if(count($shareStatus)>1){
					$objShare->updateField(array("share_status"=>0),"share_category=10 AND share_content=$dataID");
				}
				echo "success";
			}
		}else if($dataCat=='create_wall'){
			include_once(DIR_ROOT."class/posts.php");
			include_once(DIR_ROOT."class/user_photos.php");
			$objPosts		 =	new posts();
			$objUserPhotos	=	new user_photos();
			$userPhotoData	=	$objUserPhotos->getRow("user_id=$userId AND photo_id=$dataID");
			if(count($userPhotoData)>1){
				if($userPhotoData['post_id']){
					$objPosts->updateField(array("post_status"=>0),"user_id=$userId AND post_id=".$userPhotoData['post_id']);
				}
				$shareStatus	=	$objShare->getRow("share_category=1 AND share_content=".$userPhotoData['post_id']);
				if(count($shareStatus)>1){
					$objShare->updateField(array("share_status"=>0),"share_category=1 AND share_content=".$userPhotoData['post_id']);
				}
				$objUserPhotos->delete("user_id=$userId AND photo_id=$dataID");
				echo "success";
			}
		}else if($dataCat=='create_album'){
			include_once(DIR_ROOT."class/album.php");
			include_once(DIR_ROOT."class/album_images.php");
			$objAlbums		 =	new album();
			$objAlbumImages	=	new album_images();
			$userAlbumDetails  =	$objAlbums->getRow("user_id=$userId AND a_id=$dataID");
			if(count($userAlbumDetails)>1){
				$imageDetails	=	$objAlbumImages->getFields("ai_images","user_id=$userId AND a_id=$dataID");
				if(count($imageDetails)>0){
					foreach($imageDetails as $aiImages){
						if(file_exists(SITE_ROOT."uploads/albums/".$aiImages['ai_images'])){
							unlink(SITE_ROOT."uploads/albums/".$aiImages['ai_images']);
						}
					}
					$objAlbumImages->delete("user_id=$userId AND a_id=$dataID");
				}
				$shareStatus	=	$objShare->getRow("share_category=2 AND share_content=".$userPhotoData['ai_id']);
				if(count($shareStatus)>1){
					$objShare->updateField(array("share_status"=>0),"share_category=2 AND share_content=".$userPhotoData['ai_id']);
				}
				$objAlbums->delete("user_id=$userId AND a_id=$dataID");
				echo "success";
			}
		}else if($dataCat=='create_video'){
			include_once(DIR_ROOT."class/videos.php");
			$objVideos   =	new videos();
			$videoData	=	$objVideos->getRow("user_id=$userId AND video_id=$dataID");
			if(count($videoData)>1){
				if($videoData['video_type']==1){
					if(file_exists(SITE_ROOT."uploads/videos/".$videoData['video_url'])){
						unlink(SITE_ROOT."uploads/videos/".$videoData['video_url']);
					}					
				}
				if(file_exists(SITE_ROOT."uploads/videos/".$videoData['video_thumb'])){
					unlink(SITE_ROOT."uploads/videos/".$videoData['video_thumb']);
				}
				$shareStatus	=	$objShare->getRow("share_category=3 AND share_content=$dataID");
				if(count($shareStatus)>1){
					$objShare->updateField(array("share_status"=>0),"share_category=3 AND share_content=$dataID");
				}
				$objVideos->delete("user_id=$userId AND video_id=$dataID");
				echo "success";
			}
		}else if($dataCat=='create_review'){
			include_once(DIR_ROOT."class/user_reviews.php");
			$objReviews   =	new user_reviews();
			$reviewData	=	$objReviews->getRow("user_id_by=$userId AND review_id=$dataID");
			if(count($reviewData)>1){
				//$shareStatus	=	$objShare->getRow("share_category=11 AND share_content=$dataID");
				//if(count($shareStatus)>1){
					//$objShare->updateField(array("share_status"=>0),"share_category=11 AND share_content=$dataID");
				//}
				$objReviews->delete("review_id=".$dataID);
				echo "success";
			}
		}
	}
}
?>