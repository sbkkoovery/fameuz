<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/music.php");
	$objCommon				   =	new common();
	$objMusic				 	=	new music();
	$music_id					=	$objCommon->esc($_POST['music_id']);
	$userId					  =	$_SESSION['userId'];
	if($music_id !='' && $userId !=''){
		$getMusicDetails		 =	$objMusic->getRow("music_id=".$music_id." AND user_id =".$userId);
	?>
	<div class="modal-body ">
        <form action="<?php echo SITE_ROOT?>access/edit_my_music.php" method="post" enctype="multipart/form-data">
            <div class="form-group">
            <label>Title</label>
                <input type="text" class="form-control" name="music_title" placeholder="Music Title" value="<?php echo ($getMusicDetails['music_title'])?$objCommon->html2text($getMusicDetails['music_title']):''?>">
            </div>
			<div class="form-group">
            <label>Artist</label>
                <input type="text" class="form-control" name="music_artist" placeholder="Music Artist" value="<?php echo ($getMusicDetails['music_artist'])?$objCommon->html2text($getMusicDetails['music_artist']):''?>">
            </div>
            <div class="form-group">
            <label>Details</label>
                <textarea class="form-control" placeholder="Add more info" rows="4" name="music_descr"><?php echo ($getMusicDetails['music_descr'])?$objCommon->html2text($getMusicDetails['music_descr']):''?></textarea>
            </div>
            <div class="form-group">
            <label>Tags</label>
                <input type="text" class="form-control" name="music_tags" placeholder="Music Tags" value="<?php echo ($getMusicDetails['music_tags'])?$objCommon->html2text($getMusicDetails['music_tags']):''?>">
            </div>
            <div class="row">
            	<div class="col-sm-6">
                    <div class="form-group">
                        <label>Privacy</label>
                         <select class="form-control" name="privacy">
                          <option value="1" <?php echo ($getMusicDetails['music_privacy'] == '1,0,0,0')?'selected':''?>>Public</option>
						  <option value="2" <?php echo ($getMusicDetails['music_privacy'] == '0,1,0,0')?'selected':''?>>Friends</option>
						  <option value="4" <?php echo ($getMusicDetails['music_privacy'] == '0,0,0,1')?'selected':''?>>Only for me</option>
                         </select>
                    </div>
                </div>
                <div class="col-sm-6">
                	<div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="music_status">
                          <option value="1"  <?php echo ($getMusicDetails['music_status'] == 1)?'selected':''?>>Enabled</option>
						  <option value="0" <?php echo ($getMusicDetails['music_status'] == 0)?'selected':''?>>Disabled</option>
                         </select>
                    </div>
                </div>
            </div>
			<div class="row">
            	<div class="col-sm-6">
					<div class="form-group">
						<label>Music File</label>
						<input type="file" class="form-control" name="file" />
					</div>
				</div>
				<div class="col-sm-6">
					<div id="myElement"></div>
				</div>
			</div>
			 <div class="row">
            	<div class="col-sm-6">
					<div class="form-group">
						<label>Image</label>
						<input type="file" class="form-control" name="music_thumb" />
					</div>
				</div>
				<div class="col-sm-6">
					<?php
					$musicImage			=	($getMusicDetails['music_thumb'])?$getMusicDetails['music_thumb']:'audio-thump.jpg';
					?>
					<img src="<?php echo SITE_ROOT_AWS_MUSIC.'uploads/music/'.$musicImage?>"  width="50" height="50" class="musicImg"/>
				</div>
			</div>
       <input type="hidden" name="editMusicId" value="<?php echo $getMusicDetails['music_id']?>" />
	   <div class="row">
			<div class="col-sm-12 text-right"> 
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="submit" class="btn btn-primary">Edit</button>
			</div>
		</div>
      </div>
	  </form>
      <div class="modal-footer">
       
      </div>
	   
	<?php
	}
}
?>
<script>
jwplayer("myElement").setup({
	file: '<?php echo SITE_ROOT_AWS_MUSIC.'uploads/music/'.$objCommon->html2text($getMusicDetails['music_url'])?>',
	  'playlist': [{
		'file': '<?php echo SITE_ROOT_AWS_MUSIC.'uploads/music/'.$objCommon->html2text($getMusicDetails['music_url'])?>',
		'title': '<?php echo $objCommon->html2text($getMusicDetails['music_title'])?>'
	}],
	repeat: 'list',
	width: 250,
	height: 30
});
$('#myModal').on('hidden.bs.modal', function () {
  jwplayer("myElement").stop();
});
</script>