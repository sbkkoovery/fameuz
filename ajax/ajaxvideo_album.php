<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/videos.php");
$objCommon				   =	new common();
$path 						= 	$_GET['path'];
$userId					  =	$_SESSION['userId'];
$objVideos			   	   =	new videos();
$youtube_link				=	$objCommon->esc($_POST['youtube_link']);
$_POST['user_id']			=	$userId;
$_POST['video_status']	   =	1;
$_POST['video_created']	  =	date("Y-m-d H:i:s");
$_POST['video_updated']	  =	date("Y-m-d H:i:s");
$_POST['video_visits']	   =	0;
$_POST['video_likes']		=	0;
$uploadStatus				=	0;
mysql_query("START TRANSACTION");
$todayDate			   =	date('Y-m-d');
if(!file_exists($path.$todayDate)){
	mkdir($path.$todayDate);
	mkdir($path.$todayDate.'/thumb');
}
$path			   	   =	$path.$todayDate.'/';
$pathThumb		  	  =	$path.'thumb/';
$time			  	   =	time();
if($youtube_link !=''){
	$_POST['video_type']	 =	2;
	$_POST['video_url']	  =	$youtube_link;
	$getyoutube			  =	$objCommon->getYoutubeDetails($youtube_link);
	$_POST['video_duration'] =	$getyoutube['duration'];
	$newYouTubeImgName	   =	$time."_".$userId.".jpg";
	file_put_contents ($pathThumb.$newYouTubeImgName,file_get_contents($getyoutube['thumbnail']));
	//----------------start upload to AWS----------------------------------------
	require(DIR_ROOT."amazone_s3/S3.php");
	$s3			=	new S3(AWS_ID,AWS_KEY);
	S3::putObjectFile($pathThumb.$newYouTubeImgName,AWS_VIDEO_BUCKET,'uploads/videos/'.$todayDate.'/thumb/'.$newYouTubeImgName,S3::ACL_PUBLIC_READ,array());
	//----------------end upload to AWS-------------------------------------------
	$_POST['video_thumb']	=	$todayDate.'/thumb/'.$newYouTubeImgName;
	$objVideos->insert($_POST);
	$uploadStatus			=	1;
	$lastAlbImgId			=	$objVideos->insertId();
	$video_encr_id		   =	$objCommon->randStrGenSpecial(10).'-'.$lastAlbImgId.$objCommon->randStrGenSpecial(4);
	$objVideos->updateField(array("video_encr_id"=>$video_encr_id),"video_id=".$lastAlbImgId);
}else if($_FILES['file']['tmp_name'] !=''){
	$_POST['video_type']	 =	1;
	$valid_formats 		   = 	array("mp4", "mov","mpeg4","avi","wmv","MPEGPS","flv","3gp","WebM","AVI","MP4","FLV");
	$ext 				 	= 	pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
	if(in_array($ext,$valid_formats))
	{
		
		$actual_vid_name 	= 	$time."_".$userId.".".$ext;
		$newVidName		 =	$time."_".$userId.".mp4";
		//shell_exec('/usr/local/bin/ffmpeg -i '.$_FILES['file']['tmp_name'].' -strict -2 '.$path.$newVidName);
		//shell_exec('/usr/local/bin/ffmpeg -itsoffset -4  -i '.$_FILES['file']['tmp_name'].' -vcodec mjpeg -vframes 1 -an -f rawvideo -s 320x240 '.$pathThumb.$time.'_'.$userId.'.jpg');
		shell_exec('/usr/local/bin/ffmpeg -i '.$_FILES['file']['tmp_name'].' -strict -2 '.$path.$newVidName);
		shell_exec('/usr/local/bin/ffmpeg -i '.$_FILES['file']['tmp_name'].' -ss 00:00:02.000 -vframes 1 '.$pathThumb.$time.'_'.$userId.'.jpg');
		$videofile		  =	$path.$newVidName;
		//----------------start upload to AWS----------------------------------------
		require(DIR_ROOT."amazone_s3/S3.php");
		$s3			=	new S3(AWS_ID,AWS_KEY);
		S3::putObjectFile($videofile,AWS_VIDEO_BUCKET,'uploads/videos/'.$todayDate.'/'.$newVidName,S3::ACL_PUBLIC_READ,array());
		S3::putObjectFile($pathThumb.$time.'_'.$userId.'.jpg',AWS_VIDEO_BUCKET,'uploads/videos/'.$todayDate.'/thumb/'.$time.'_'.$userId.'.jpg',S3::ACL_PUBLIC_READ,array());
		//----------------end upload to AWS-------------------------------------------
		$xyz 				= 	shell_exec("/usr/local/bin/ffmpeg -i \"{$videofile}\" 2>&1");
		$search			 =	'/Duration: (.*?),/';
		preg_match($search, $xyz, $matches);
		$explode			= 	explode(':', $matches[1]);
		$_POST['video_duration']		=	$explode[0].':'.$explode[1].':'.floor($explode[2]);
		$_POST['video_url']			 =	$todayDate.'/'.$newVidName;
		$_POST['video_thumb']		   =	$todayDate.'/thumb/'.$time.'_'.$userId.'.jpg';
		$objVideos->insert($_POST);
		$uploadStatus				   =	1;
		$lastAlbImgId				   =	$objVideos->insertId();
		$video_encr_id		   		  =	$objCommon->randStrGenSpecial(10).'-'.$lastAlbImgId.$objCommon->randStrGenSpecial(4);
		$objVideos->updateField(array("video_encr_id"=>$video_encr_id),"video_id=".$lastAlbImgId);	
	}
}
mysql_query("COMMIT");
if($uploadStatus==1){
	if (preg_match('%^https?://[^\s]+$%',$_POST['video_thumb'])) {
		$imgUrlshow			=	$_POST['video_thumb'];
	} else {
		$imgUrlshow			=	SITE_ROOT.'uploads/videos/'.$_POST['video_thumb'];
	}
	echo '<form id="publish_album" action="'.SITE_ROOT.'ajax/publish_album_videos.php" method="post">';
	echo '<table class="table">
		<tr>
			<td>Video</td>
			<td><div class="post_imag" style="height:38px;"><img src="'.$imgUrlshow.'" class="img-responsive" width="50"/></div></td>
		</tr>
		<tr>
			<td>Video Title</td>
			<td><textarea class="form-control" rows="1" name="img_title"></textarea><input type="hidden" name="hid_img_id" value="'.$lastAlbImgId.'"/></td>
		</tr>
		<tr>
			<td>Video Description</td>
			<td><textarea class="form-control" rows="1" name="img_descr"></textarea></td>
		</tr>
		<tr>
			<td>Video Tags</td>
			<td><input type="text" class="form-control"  name="video_tags" placeholder="Add related tags (separated by commas, please!)"></td>
		</tr>
		<tr>
			<td>Privacy Settings</td>
			<td>
				<select class="form-control" name="privacy">
					<option value="1">Public</option>
					<option value="2">Friends</option>
					<option value="4">Only for me</option>
				</select>
			</td>
		</tr>
		<tr><td colspan="2"><button type="button" class="btn btn-default" id="post_alb">Publish to my post</button></td></tr>
	</table>';
	echo '</form>';
}
?>
<div id="preview2"></div>
<script type="text/javascript">
$(document).ready(function(e) {

	$("#publish_album").submit(function()
{
	$(".load_album_content").html('<div class="load_album_preloader"></div>');
    var postData = $(this).serializeArray();
    var formURL = $(this).attr("action");
    $.ajax(
    {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR) 
        {
			$(".load_album_preloader").hide();
           $(".load_album_content").load('<?php echo SITE_ROOT?>ajax/album_video_list.php');
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            //if fails      
        }
    });
    e.preventDefault(); //STOP default action
    e.unbind(); //unbind. to stop multiple form submit.
});

$("#post_alb").click(function(){
	
		
		$("#publish_album").submit(); //Submit  the FORM
	});
});

</script>