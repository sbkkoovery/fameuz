<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/videos.php");
$objCommon				   =	new common();
$objVideos			  	   =	new videos();
$friendId					=	$objCommon->esc($_GET['friendId']);
if($friendId !=''){
	$getVideos	   		   =	$objVideos->listQuery("select * from videos  where user_id=".$friendId."  order by video_created desc");
	
?>
<div class="album-head inner"> <img src="<?php echo SITE_ROOT?>images/video.png"><span class="head-album">Videos</span> </div>
	<?php
	if(count($getVideos)>0){
	?>
	<div class="row albListShow">
	<div class="col-md-12">
	<div class="add_photo_album_list">
		<ul class="album_list_ul">
			<?php
			foreach($getVideos as $allVideos){
				if($allVideos['video_type']==1){
					$getAiImages				=	SITE_ROOT.'uploads/videos/'.$allVideos['video_thumb'];
				}else if($allVideos['video_type']==2){
					$getAiImages				=	$allVideos['video_thumb'];
					if (preg_match('%^https?://[^\s]+$%', $getAiImages)) {
						$getAiImages			=	$allVideos['video_thumb'];
					} else {
						$getAiImages			=	SITE_ROOT.'uploads/videos/'.$allVideos['video_thumb'];
					}
				}
				$vidUrl					 =	$objCommon->html2text($allVideos['video_encr_id']);
				?>
			<li><div class="vid"><img src="<?php echo $getAiImages?>" class="img-responsive"  /><a href="javascript:;" data-videncrid="<?php echo $vidUrl?>" class="playbtn lightBoxs"></a></div></li>
			<?php
			}
			?>
		</ul>
	</div>
	</div>
	</div>
	<?php
	}else{
		echo '<div class="row albListShow"><div class="col-md-12"><p class="no_img_album text-center">There are no videos in this album</p></div></div>';
	}
}
?>
<script language="javascript" type="text/javascript">
function photoPopUpVideo(ecrId){
	if(ecrId!=""){
		var sH	=	$(window).height();
		var sW	=	$(window).width();
		$("#pop_up").fadeIn(200);
		$.get("<?php echo SITE_ROOT; ?>ajax/popUpVideoUser.php",{ecrId:ecrId,screenHeight:sH,screenWidth:sW},
		function(data){
			$("#pop_up").html(data);
			$(".cd-popup-container").fadeIn(400,function(){
			});
			$(".close_popup").on("click",function(e) {
				$("#pop_up").fadeOut(300,function(){
					$("#pop_up").html("");
					$(".cd-popup-container").hide();
				});
			});
		});
	}
}
</script>
<script type="text/javascript">
$(document).ready(function(){
	 $(".lightBoxs").lightBox({
	  videoSkin: '<?php echo SITE_ROOT?>jw_player/six/six.xml',
	  photosObj: true,
	  videoObj :  true,
	  defaults : true,
	  album   : false,
	  deleteCommentUrl : '<?php echo SITE_ROOT ?>access/delete_comment.php',
	  skins : {
	   loader   : '<?php echo SITE_ROOT?>images/ajax-loader.gif',
	   next      : '<img src="<?php echo SITE_ROOT?>fameuz_lightbox/images/next.png">',
	   prev   : '<img src="<?php echo SITE_ROOT?>fameuz_lightbox/images/prev.png">',
	   dismiss     : '<img src="<?php echo SITE_ROOT?>fameuz_lightbox/images/close.png" width="15">',
	   reviewIcon  : '<i class="fa fa-chevron-right"></i>',
	  },
	  photos : {
	   fetchUrl: '<?php echo SITE_ROOT.'ajax/popUpImageUser.php'?>',
	   ajaxUrl : '<?php echo SITE_ROOT.'widget/pop_image_comment_box.php' ?>',
	   comment : {
		CommentFormAction : '<?php echo SITE_ROOT?>access/post_comment.php?action=add_comment',
		  commentLikeUrl   : '<?php echo SITE_ROOT?>ajax/like_comment.php',
	   },
	   shareItems : {
		likeAjaxUrl  : '<?php echo SITE_ROOT?>ajax/like.php',
		shareAjaxUrl : '<?php echo SITE_ROOT?>ajax/share.php',
		workedAjaxUrl: '<?php echo SITE_ROOT?>ajax/worked_together.php',
	   },
	  },
	  video : {
	   url  : '<?php echo SITE_ROOT?>uploads/testuploads/1.mp4',
	   fetchUrl: '<?php echo SITE_ROOT.'ajax/popUpVideoUser.php'?>',
	   ajaxUrl : '<?php echo SITE_ROOT.'widget/pop_video_comment_box.php' ?>',
	   comment : {
		CommentFormAction : '<?php echo SITE_ROOT?>access/post_video_comment.php?action=add_comment',
		commentLikeUrl   : '<?php echo SITE_ROOT?>ajax/like_video_comment_pop.php',
	   },
	   shareItems : {
		likeAjaxUrl  : '<?php echo SITE_ROOT?>ajax/like_video.php',
		shareAjaxUrl : '<?php echo SITE_ROOT?>ajax/share_video.php',
	   },
	  },
	  shareItems : {
	   likeAjaxUrl : '<?php echo SITE_ROOT?>ajax/like.php',
	   shareAjaxUrl: '<?php echo SITE_ROOT?>ajax/share.php',
	  }
	 });
});
</script>