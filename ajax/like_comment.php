<?php

@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/likes.php");
	$objCommon				   =	new common();
	$ObjLikes					=	new likes();
	$likeId					  =	$objCommon->esc($_POST['likeId']);
	$likecat					 =	$objCommon->esc($_POST['likecat']);
	$imageType				   =	$objCommon->esc($_POST['likecat']);
	$imgUserId				   =	$objCommon->esc($_POST['imgUserId']);
	$imageType				   =	$objCommon->esc($_POST['imageType']);
	$imageId					 =	$objCommon->esc($_POST['imageId']);
	$userId					  =	$_SESSION['userId'];
	if($likecat != '' && $likeId != '' && $userId != ''){
		$_POST['like_cat']	   =	$likecat;
		$_POST['like_content']   =	$likeId;
		$_POST['like_user_id']   =	$userId;
		$_POST['like_img_user_by']=   $imgUserId;
		$_POST['like_time']	  =	date("Y-m-d H:i:s");
		$getLikes				=	$ObjLikes->getRow("like_content=".$likeId." and like_user_id =".$userId ." and like_cat=3");
		$getLikeCount			=	$ObjLikes->getRowSql("SELECT count(like_id) AS likeCount FROM likes WHERE like_content =".$likeId." AND like_cat =3 AND like_status=1");
		$commentLike			 =	$getLikeCount['likeCount'];
		if($getLikes['like_id']==''){
			$_POST['like_status']=	1;
			$ObjLikes->insert($_POST);
			//echo '<a href="javascript:;" class=" like_button liked" data-like="'.$likeId.'" data-userto="'.$imgUserId.'">Liked <i class="fa fa-heart"></i> <span>'.($commentLike+1).'</span></a>';
			  echo 'cmdLiked';
			//----notification table------------------------------------
			$myDetails				   		   	   =	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$userId);
			$displayName						  	 =	$objCommon->displayName($myDetails);
			$friend_id							   =	$imgUserId;
			$notiType								=	'comments like';
			$notiImg								 =	'';
			$notiDescr  	 	 				   	   =	'<b>'.$displayName.'</b> liked  your <b>comment</b>.';
			$notiUrl  								 =	SITE_ROOT.'user/single-image?type='.$imageType.'&id='.$imageId;
			$objCommon->pushNotification($friend_id,$userId,$notiType,$notiImg,$notiDescr,$notiUrl);
			//----------------------------------------------------------
		}else{
			if($getLikes['like_status']==1){
				$upStatus		=	0;
				//echo '<a href="javascript:;" class="like_button" data-like="'.$likeId.'" data-userto="'.$imgUserId.'">Like <i class="fa fa-heart"></i> <span>'.($commentLike-1).'</span></a>';
				echo 'cmdLike';
				
			}else{
				$upStatus		=	1;
				//echo '<a href="javascript:;" class="like_button liked" data-like="'.$likeId.'" data-userto="'.$imgUserId.'">Liked <i class="fa fa-heart"></i> <span>'.($commentLike+1).'</span></a>';
				echo 'cmdLiked';
			}
			$ObjLikes->updateField(array("like_status"=>$upStatus),"like_id=".$getLikes['like_id']);
		}
	}
}
?>