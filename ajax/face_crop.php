<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
include_once("FaceDetector.php");
$face_detect = new Face_Detector('detection.dat'); 
function cropImg($file,$folder,$name,$width=NULL,$height=NULL){
	global $face_detect;
	$ret="";
	if(isset($file)){
		$ext=explode(".",$file);
		$ext=(count($ext)!=0)?strtolower($ext[count($ext)-1]):"";
		$path="$folder$name.$ext";
		$name="$name.$ext";
		if(copy($file,$path)){
			$proc=false;
			switch($ext){
				case "jpg": $proc=true; $im=imagecreatefromjpeg($path); break;
				case "jpeg": $proc=true; $im=imagecreatefromjpeg($path); break;
				case "gif": $proc=true; $im=imagecreatefromgif($path); break;
				case "png": $proc=true; $im=imagecreatefrompng($path); break;
			}
			if($proc){
				$ow=imagesx($im);
				$oh=imagesy($im);
				$bow=$ow;
				$boh=$oh;
				$posX=0;
				$posY=0;
				if($ow>$width||$oh>$height){
						$cmp=1; 
						if($ow>$oh){ $cmp = $ow/$width; }
						if($ow<$oh){ $cmp = $oh/$height; }
						if($ow==$oh){ $cmp = $oh/$height; }
						$ow=round($ow/$cmp); 
						$oh=round($oh/$cmp);
						if($ow < $width){
							$addWidth		=	$width-$ow;
							$ow			  =	$ow+$addWidth;
							$oh			  =	$oh+$addWidth;
							$prio			=	1;
						}else if($oh < $height){
							$addHeight	   =	$height-$oh;
							$ow			  =	$ow+$addHeight;
							$oh			  =	$oh+$addHeight;
							$prio			=	2;
						}
						
						//-----------first crop-------------------
						$newImg = imagecreatetruecolor($ow,$oh);
						if($ext=="png"){
							imagealphablending($newImg, false);
							imagesavealpha($newImg,true);
							$transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 0);
							imagefilledrectangle($newImg, 0, 0, $ow, $oh, $transparent);
						}
						imagecopyresampled($newImg, $im, 0, 0, 0, 0 ,$ow,$oh, $bow, $boh);
						if($ext=="png"){ 
							imagepng($newImg,$path,9); 
						}else if($ext=="jpg"){ 
							imagejpeg($newImg,$path,90); 
						}else if($ext=="gif"){ 
							imagegif($newImg,$path); 
						}
						$ret= $name;
						//echo $path;
						
						//----------------------------------------
						$face_detect->face_detect($path);
						$arrface	=	$face_detect->cropFace();
						//echo "<br>".$path;
						//print_r($arrface);
						//exit;
						if($prio==2){
							$left			 =	0;
							$top			  =	0;
							if($arrface['x'] !='' && $arrface['x'] !=0 && $arrface['w'] !=0){
								$w			=	$width-$arrface['w'];
								$wHalf		=	$w/2;
								$y			=	$ow-($arrface['x']+$arrface['w']);
								if($wHalf > $y){
									$left		  =	$ow-$width;
									$top 		   = 	0;
								}else{
									$left		  =	0;
									$top 		   = 	0;
								}
							}else{
								$noface1	   =	$ow/2;
								$noface2	   =	$width/2;
								$left		  =	$noface1-$noface2;
								$top 		   = 	0;
							}
						}else{
							//-------------------------------height---------------------------------
							$left			 =	0;
							$top			  =	0;
							if($arrface['y'] !='' && $arrface['y'] !=0 && $arrface['y'] !=0){
								$w			=	$height-$arrface['y'];
								$wHalf		=	$w/2;
								$y			=	$oh-($arrface['y']+$arrface['w']);
								if($wHalf > $y){
									$top		  =	$oh-$height;
									$left 		 = 	0;
								}else{
									$left		  =	0;
									$top 		   = 	0;
								}
							}else{
								$noface1	   =	$oh/2;
								$noface2	   =	$height/2;
								$top		   =	$noface1-$noface2;
								$left 		  = 	0;
							}
							//----------------------------------------------------------------------
						}
						$filename = $path;
						 $extension = strtolower(strrchr($filename, '.'));
						switch($extension)
						{
							case '.jpg':
							case '.jpeg':
								$img1 = @imagecreatefromjpeg($filename);
								break;
							case '.gif':
								$img1 = @imagecreatefromgif($filename);
								break;
							case '.png':
								$img1 = @imagecreatefrompng($filename);
								break;
							default:
								$img1 = false;
								break;
						}
						list($current_width, $current_height) = getimagesize($filename);
						$crop_width = $width;
						$crop_height = $height;
						$canvas = imagecreatetruecolor($crop_width, $crop_height);
						$current_image = $img1;
						imagecopy($canvas, $current_image, 0, 0, $left, $top, $current_width, $current_height);
						//imagejpeg($canvas, $filename, 100);
						switch($extension)
						{
							case '.jpg':
							case '.jpeg':
								@imagejpeg($canvas, $filename, 100);
								break;
							case '.gif':
								@imagegif($canvas, $filename, 100);
								break;
							case '.png':
								@imagepng($canvas, $filename, 9);
								break;
							default:
								false;
								break;
						}
				}			
			}
		}
	}
	//return $ret;
}

?>