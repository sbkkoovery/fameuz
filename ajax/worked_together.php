<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/worked_together.php");
	$objCommon				   =	new common();
	$objWorked				   =	new worked_together();
	$img_id					   =	$objCommon->esc($_POST['img_id']);
	$img_cat					  =	$objCommon->esc($_POST['img_cat']);
	$send_to				     =	$objCommon->esc($_POST['send_to']);
	$userId					  =	$_SESSION['userId'];
	if($img_id != '' && $img_cat != '' && $send_to != '' && $userId !=''){
		$_POST['img_cat']	   	 =	$img_cat;
		$_POST['img_id']   		  =	$img_id;
		$_POST['send_from']   	  =	$userId;
		$_POST['send_to']		=    $send_to;
		$_POST['worked_date']	=	date("Y-m-d H:i:s");
		$getWorked			   =	$objWorked->getRow("img_cat=".$img_cat." and img_id =".$img_id." and send_from=".$userId." and send_to=".$send_to);
		if($getWorked['worked_id']==''){
			$_POST['from_status']=	1;
			$_POST['to_status']  =	0;
			$objWorked->insert($_POST);
			//echo '<a href="javascript:;" class="worked_request_sent"><i class="fa fa-check"></i>Worked Together</a>';
			echo 'workedTogether';
		//----notification table------------------------------------
				$myDetails				   		   	   =	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$userId);
				$displayName						  	 =	$objCommon->displayName($myDetails);
				$friend_id							   =	$send_to;
				$notiType								=	'worked_together';
				$notiImg								 =	'';
				$notiDescr  	 	 				   	   =	'<b>'.$displayName.'</b> has sent you <b>worked together</b> request.';
				$notiUrl  								 =	SITE_ROOT.'user/single-image?type='.$img_cat.'&id='.$img_id;
				$objCommon->pushNotification($friend_id,$userId,$notiType,$notiImg,$notiDescr,$notiUrl);
			//----------------------------------------------------------
		}else{
			if($getWorked['from_status']==1){
				$upStatus		=	0;
				//echo '<a data-likecat="'.$img_cat.'" data-like="'.$img_id.'"  href="javascript:;" class="worked_request has-spinner"><span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span><i class="fa fa-users"></i>Worked Together</a>';
				echo 'worked';
			}else{
				$upStatus		=	1;
				//echo '<a href="javascript:;" class="worked_request_sent"><i class="fa fa-check"></i>Worked Togethert</a>';
				echo 'workedTogether';
			}
			$objWorked->updateField(array("from_status"=>$upStatus),"worked_id=".$getWorked['worked_id']);
		}
	}
}
?>