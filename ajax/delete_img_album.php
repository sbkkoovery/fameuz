<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	$objCommon				   =	new common();
	//----------------start upload to AWS----------------------------------------
	require(DIR_ROOT."amazone_s3/S3.php");
	$s3			=	new S3(AWS_ID,AWS_KEY);
	//----------------end upload to AWS-------------------------------------------
	$imgid				  	   =	$_GET['imgid'];
	$imgType				   =	$_GET['imgType'];
	$userId					   =	$_SESSION['userId'];
	if($imgid !='' && $userId!='' && $imgType !=''){
		switch($imgType){
			case 1:
				include_once(DIR_ROOT."class/user_photos.php");
				$objUserPhotos			   =	new user_photos();
				$getImageName			=	$objUserPhotos->getRow("photo_id=".$imgid);
				$explAiImages	   		=	explode("/",$getImageName['photo_url']);
				$getAiImag1		  	  =	$explAiImages[0].'/thumb/'.$explAiImages[1];
				$getAiImag2		  	  =	$explAiImages[0].'/original/'.$explAiImages[1];
				$imgPath				 =	DIR_ROOT.'uploads/albums/'.$getImageName['photo_url'];
				$imgPath1				=	DIR_ROOT.'uploads/albums/'.$getAiImag1;
				$imgPath2				=	DIR_ROOT.'uploads/albums/'.$getAiImag2;
				unlink($imgPath);
				unlink($imgPath1);
				unlink($imgPath2);
				S3::deleteObject(AWS_IMG_BUCKET,'uploads/albums/'.$getImageName['polo_url']);
				S3::deleteObject(AWS_IMG_BUCKET,'uploads/albums/'.$getAiImag1);
				$objUserPhotos->delete("photo_id=".$imgid);
			break;
			case 2:
				include_once(DIR_ROOT."class/album_images.php");
				$objAlbumImages			   =	new album_images();
				$getImageName			=	$objAlbumImages->getRow("ai_id=".$imgid);
				$explAiImages	   		=	explode("/",$getImageName['ai_images']);
				$getAiImag1		  	  =	$explAiImages[0].'/thumb/'.$explAiImages[1];
				$getAiImag2		  	  =	$explAiImages[0].'/original/'.$explAiImages[1];
				$imgPath				 =	DIR_ROOT.'uploads/albums/'.$getImageName['ai_images'];
				$imgPath1				=	DIR_ROOT.'uploads/albums/'.$getAiImag1;
				$imgPath2				=	DIR_ROOT.'uploads/albums/'.$getAiImag2;
				unlink($imgPath);
				unlink($imgPath1);
				unlink($imgPath2);
				S3::deleteObject(AWS_IMG_BUCKET,'uploads/albums/'.$getImageName['ai_images']);
				S3::deleteObject(AWS_IMG_BUCKET,'uploads/albums/'.$getAiImag1);
				$objAlbumImages->delete("ai_id=".$imgid);
			break;
			case 6:
			include_once(DIR_ROOT."class/user_profile_image.php");
				$objUserProfileImg		   =	new user_profile_image();
				$getImageName			=	$objUserProfileImg->getRow("upi_id=".$imgid);
				$explAiImages	   		=	explode("/",$getImageName['upi_img_url']);
				$getAiImag1		  	  =	$explAiImages[0].'/thumb/'.$explAiImages[1];
				$getAiImag2		  	  =	$explAiImages[0].'/original/'.$explAiImages[1];
				$imgPath				 =	DIR_ROOT.'uploads/profile_images/'.$getImageName['upi_img_url'];
				$imgPath1				=	DIR_ROOT.'uploads/profile_images/'.$getAiImag1;
				$imgPath2				=	DIR_ROOT.'uploads/profile_images/'.$getAiImag2;
				unlink($imgPath);
				unlink($imgPath1);
				unlink($imgPath2);
				S3::deleteObject(AWS_IMG_BUCKET,'uploads/albums/'.$getImageName['upi_img_url']);
				S3::deleteObject(AWS_IMG_BUCKET,'uploads/albums/'.$getAiImag1);
				$objUserProfileImg->delete("upi_id=".$imgid);
			break;
			case 8:
			include_once(DIR_ROOT."class/polaroids.php");
				$objPolaroids			    =	new polaroids();
				$getImageName			=	$objPolaroids->getRow("polo_id=".$imgid);
				$explAiImages	   		=	explode("/",$getImageName['polo_url']);
				$getAiImag1		  	  =	$explAiImages[0].'/thumb/'.$explAiImages[1];
				$getAiImag2		  	  =	$explAiImages[0].'/original/'.$explAiImages[1];
				$imgPath				 =	DIR_ROOT.'uploads/albums/'.$getImageName['polo_url'];
				$imgPath1				=	DIR_ROOT.'uploads/albums/'.$getAiImag1;
				$imgPath2				=	DIR_ROOT.'uploads/albums/'.$getAiImag2;
				unlink($imgPath);
				unlink($imgPath1);
				unlink($imgPath2);
				S3::deleteObject(AWS_IMG_BUCKET,'uploads/albums/'.$getImageName['polo_url']);
				S3::deleteObject(AWS_IMG_BUCKET,'uploads/albums/'.$getAiImag1);
				$objPolaroids->delete("polo_id=".$imgid);
			break;
		}
		
	} 
}
?>