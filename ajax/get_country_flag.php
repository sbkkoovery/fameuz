<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
$objCommon				=	new common();
$get_client_ip			=	$objCommon->get_client_ip();
function iptocountry($ip) {  
   $numbers = preg_split( "/\./", $ip); 
   $two_letter_country_code	=	'';
   if (file_exists(DIR_ROOT."ip_files/".$numbers[0].".php")){ 
	   include(DIR_ROOT."ip_files/".$numbers[0].".php");  
	   $code=($numbers[0] * 16777216) + ($numbers[1] * 65536) + ($numbers[2] * 256) + ($numbers[3]);  
	   foreach($ranges as $key => $value){  
		  if($key<=$code){  
			 if($ranges[$key][0]>=$code){$two_letter_country_code=$ranges[$key][1];break;}  
			}  
		} 
   	}
		if ($two_letter_country_code==""){ $two_letter_country_code="unknown"; }  
		return $two_letter_country_code; 
	 
	}
	$two_letter_country_code  =	iptocountry($get_client_ip);
	if($two_letter_country_code != ""  && $two_letter_country_code != "unknown"){  
		include(DIR_ROOT."ip_files/countries.php");   
		$country_name			 =	$countries[$two_letter_country_code][1];  
		$file_to_check			=	SITE_ROOT."ip_files/flags/".strtolower($two_letter_country_code).".png";  
		if (file_exists(DIR_ROOT."ip_files/flags/".strtolower($two_letter_country_code).".png")){  
			$flagImg	=	'<img src="'.$file_to_check.'" width="15" height="10">';  
		}else{  
			$flagImg	=	'<img src="'.SITE_ROOT.'"ip_files/flags/noflag.gif" width="20" height="15">';  
		}  
		echo ' <span>'.$flagImg.'</span><font class="myCountry">'.$country_name.'</font></div> ';
	}
$getCountryDetails			   =	$objUsers->getRowSql("SELECT country_id FROM country WHERE country_code='".$two_letter_country_code."'");
$getCountryId					=	$getCountryDetails['country_id'];
$_SESSION['country_user']		=	array("country_id"=>$getCountryId,"country_name"=>$country_name,"country_code"=>$two_letter_country_code,"country_flag"=>$flagImg);
$getPromotionDetails			 =	$objUsers->listQuery("SELECT promo.user_id,promo.promo_type,promo.promo_content_id,promo.promo_category FROM promotion AS promo  
										LEFT JOIN promotion_payment AS payment ON promo.promo_id = payment.promo_id
										WHERE promo.promo_country LIKE '%,".$getCountryId.",%' AND  DATE(NOW()) between promo.promo_start_date and 
promo.promo_end_date AND promo.promo_admin_status=1 AND promo.promo_status=1 AND payment.payment_staus='Completed'");
if(count($getPromotionDetails)>0){
	unset($_SESSION['promotion_profile']);
	unset($_SESSION['promotion_video']);
	unset($_SESSION['promotion_music']);
	foreach($getPromotionDetails as $allPromoDetails){
		if($allPromoDetails['promo_type']=='profile'){
			$_SESSION['promotion_profile'][]			=	array($allPromoDetails['promo_content_id'],$allPromoDetails['promo_category']);
		}else if($allPromoDetails['promo_type']=='video'){
			$_SESSION['promotion_video'][]			  =	array($allPromoDetails['promo_content_id'],$allPromoDetails['promo_category']);
		}else if($allPromoDetails['promo_type']=='music'){
			$_SESSION['promotion_music'][]			  =	array($allPromoDetails['promo_content_id'],$allPromoDetails['promo_category']);
		}
	}
}
?>