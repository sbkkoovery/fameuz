<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/share.php");
	$objCommon				   =	new common();
	$objShare					=	new share();
	$shareId					 =	$objCommon->esc($_POST['shareId']);
	$shareCat					=	$objCommon->esc($_POST['shareCat']);
	$noti_whome				  =	$objCommon->esc($_POST['noti_whome']);
	$userId					  =	$_SESSION['userId'];
	if($shareCat != '' && $shareId != '' && $userId != ''){
		$_POST['share_category'] =	$shareCat;
		$_POST['share_content']  =	$shareId;
		$_POST['user_by']   		=	$userId;
		$_POST['user_whose']	 =   $noti_whome;
		$_POST['share_created']  =	date("Y-m-d H:i:s");
		$getLikes				=	$objShare->getRow("share_content=".$shareId." and user_by =".$userId." and share_category=".$shareCat);
		if($getLikes['share_id']==''){
			$_POST['share_status']=	1;
			$objShare->insert($_POST);
			$myDetails				   		   	=	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$userId);
				$displayName						  	 =	$objCommon->displayName($myDetails);
			if($shareCat==1){
				$notiType		  				=	'image_share';
				$notiDescr   	 	 			   =	'<b>'.$displayName.'</b> shared your <b>image</b>.';
				$notiUrl  						 =	SITE_ROOT.'user/single-image?type='.$shareCat.'&id='.$shareId;
			}else if($shareCat==2){
				$notiType		  				=	'image_share';
				$notiDescr   	 	 			   =	'<b>'.$displayName.'</b> shared your <b>image</b>.';
				$notiUrl  						 =	SITE_ROOT.'user/single-image?type='.$shareCat.'&id='.$shareId;
			}else if($shareCat==4){
				$getVideoId			  		  =	$objShare->getRowSql("SELECT video_encr_id FROM videos WHERE video_id =".$shareId);
				$notiType		  				=	'video_share';
				$notiDescr   	 	 			   =	'<b>'.$displayName.'</b> shared your <b>video</b>.';
				$notiUrl  						 =	SITE_ROOT.'video/watch/'.$getVideoId['video_encr_id'];
			}else if($shareCat==6){
				$notiType		  				=	'image_share';
				$notiDescr   	 	 			   =	'<b>'.$displayName.'</b> shared your <b>image</b>.';
				$notiUrl  						 =	SITE_ROOT.'user/single-image?type='.$shareCat.'&id='.$shareId;
			}else if($shareCat==9){
				$getAlbumName					=	$objShare->getRowSql("SELECT a_name FROM album WHERE a_id =".$shareId);
				$notiType		  				=	'album_share';
				$notiDescr   	 	 			   =	'<b>'.$displayName."</b> shared your <b>album '".$objCommon->html2text($getAlbumName['a_name'])."'</b>.";
				$notiUrl  						 =	SITE_ROOT.'user/my-album';
			}else if($shareCat==10){
				$notiType		  				=	'post_share';
				$notiDescr   	 	 			   =	'<b>'.$displayName."</b> shared your <b>Post</b>";
				$notiUrl  						 =	SITE_ROOT.$getWhomeDetails['usl_fameuz'];
			}
			echo '<a class=" shared" data-share="'.$shareId.'" data-category="'.$shareCat.'" data-userby="'.$noti_whome.'" href="javascript:;"><i class="fa fa-share-alt"></i>Shared </a>';
		//----notification table------------------------------------
				
				$friend_id							   =	$noti_whome;
				$notiImg								 =	'';
				$objCommon->pushNotification($friend_id,$userId,$notiType,$notiImg,$notiDescr,$notiUrl);
			//----------------------------------------------------------
		}else{
			if($getLikes['share_status']==1){
				$upStatus		=	0;
				echo '<a class="" data-share="'.$shareId.'" data-category="'.$shareCat.'" data-userby="'.$noti_whome.'" href="javascript:;"><i class="fa fa-share-alt"></i>Share </a>';
			}else{
				$upStatus		=	1;
				echo '<a class=" shared" data-share="'.$shareId.'" data-category="'.$shareCat.'" data-userby="'.$noti_whome.'" href="javascript:;"><i class="fa fa-share-alt"></i>Shared </a>';
			}
			$objShare->updateField(array("share_status"=>$upStatus),"share_id=".$getLikes['share_id']);
		}
	}
}
exit;
?>