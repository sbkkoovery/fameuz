<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/users.php");
$objCommon				 =	new common();
$objUsers				  =	new users();
$perPage 				   = 	20;
$page 					  = 	1;
if(!empty($_GET["page"])) {
$page = $_GET["page"];
}
$search					=	$objCommon->esc($_GET['search']);
if($search){
	$searchWhere		   =	" and ( user.first_name LIKE '%".$search."%' or  user.display_name LIKE '%".$search."%' or  user.email LIKE '%".$search."%' or  social.usl_fameuz LIKE '%".$search."%') ";
}else{
	$searchWhere		   =	" ";
}
$sqlFollowing		    =	"select user.user_id,user.first_name,user.last_name,user.display_name,profileImg.upi_img_url,user.email,social.usl_fameuz,follow.follow_user1,follow.follow_user2,follow.follow_status,case when (TIMESTAMPDIFF(MINUTE,chat.ucs_last_seen,'".date("Y-m-d H:i:s")."') >15 or chat.ucs_status=0) then '0' else '1' END AS chatStatus,personal.p_country,personal.p_city,cat.c_name
from following as follow 
left join users as user on (follow.follow_user1 = user.user_id and follow.follow_user1 !='".$_SESSION['userId']."') or (follow.follow_user2 = user.user_id and follow.follow_user2 !='".$_SESSION['userId']."')  
left join user_chat_status as chat on user.user_id = chat.user_id 
LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN personal_details AS personal ON user.user_id=personal.user_id 
LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
where ((follow.follow_user1='".$_SESSION['userId']."') or (follow.follow_user2='".$_SESSION['userId']."' and follow.follow_status='2')) ".$searchWhere." order by user.first_name asc";
$start = ($page-1)*$perPage;
if($start < 0) { $start = 0; }
$queryMyFollowers =  $sqlFollowing . " limit " . $start . "," . $perPage; 
if(empty($_GET["rowcount"])) {
	$_GET["rowcount"] = $countFollowers	=	$objUsers->countRows($sqlFollowing);
}
$pages  = ceil($_GET["rowcount"]/$perPage);
$getMyFollowing		=	$objUsers->listQuery($queryMyFollowers);
if($start < $countFollowers){
?>
									<?php
									if(count($getMyFollowing)>0){
										echo '<input type="hidden" class="pagenum" value="' . $page . '" /><input type="hidden" class="total-page" value="' . $pages . '" /><input type="hidden" id="total-count" value="'.$countFollowers.'" />';
										foreach($getMyFollowing as $keyFollowing=>$allFollowing){
											$allFollowingImg	=	SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.(($allFollowing['upi_img_url'])?$allFollowing['upi_img_url']:"profile_pic.jpg");
											$allFollowingName		 =	$objCommon->displayName($allFollowing);

											if($allFollowing['follow_status']==2){ 
												$friendStatus =2; 
											}else if($allFollowing['follow_status']==1 && $allFollowing['follow_user1']==$_SESSION['userId']){ 
												$friendStatus =1; 
											}else { 
												$friendStatus=0; 
											}
											if($allFollowing['p_city'] != '' || $allFollowing['p_country'] != ''){
												$friendPcity	=	($allFollowing['p_city'])?$objCommon->html2text($allFollowing['p_city'])." ,":"";
											}
									?>
									<li class="col-md-6">
										<div class="visitor_inner_box">
											<div class="image_thumb pull-left">
												 <a href="<?php echo SITE_ROOT.$objCommon->html2text($allFollowing['usl_fameuz'])?>" title="<?php echo $allFollowingName?>"><img src="<?php echo $allFollowingImg?>" class="img-responsive" alt="<?php echo $allFollowingName?>" title="<?php echo $allFollowingName?>" /></a>
											</div>
											<div class="visitor_box">
												<p class="name_head"><a href="<?php echo SITE_ROOT.$objCommon->html2text($allFollowing['usl_fameuz'])?>" title="<?php echo $allFollowingName?>"><?php echo $allFollowingName?></a></p>
												<p class="proffesion"> <?php echo $objCommon->html2text($allFollowing['c_name'])?></p>
												<p><i class="fa fa-globe"></i> <?php echo $friendPcity.' '.$objCommon->limitWords($allFollowing['p_country'],15)?></p>
												<?php
												if($allFollowing['chatStatus']==1){
													echo '<p class="online_status">online</p>';
												}else{
													echo '<p class="offline_status">offline</p>';
												}
												?>
												<div class="follow_status_box">
													<div class="follow_status">
														<?php
														if($friendStatus==2){
															$unfriendStr	=	'Unfriend';
														?>
														<a href="javascript:;" class="friend popUnfriend"><i class="fa fa-star-o"></i> Friend <i class="fa fa-caret-down folow_option"></i></a>
														<?php
														
														}else if($friendStatus==1){
															$unfriendStr	=	'Unfollow';
														?>
														<a href="javascript:;" class="following popUnfriend"><i class="fa fa-check"></i> Following <i class="fa fa-caret-down folow_option"></i></a>
														<?php
														}
														?>
														<?php
														$unfriendId				=	$allFollowing['user_id'];	//friend id for unfriend widget
														include(DIR_ROOT."widget/un_friend_pop.php");
														?>
													</div>
												</div>
											</div>
                                             <div class="side_mark"></div>
                                             <div class="clearfix"></div>
										</div>
									</li>
									<?php
									/*if(($keyFollowing+1)%2==0){
											echo '<div class="clearfix "></div>';
									}*/
										}
									}
}
?>