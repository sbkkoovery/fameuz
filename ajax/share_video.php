<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/share.php");
	$objCommon				   =	new common();
	$objShare					=	new share();
	$shareId					 =	$objCommon->esc($_POST['shareId']);
	$shareCat					=	$objCommon->esc($_POST['shareCat']);
	$shareWhoseId				=	$objCommon->esc($_POST['shareWhoseId']);
	$userId					  =	$_SESSION['userId'];
	if($shareId != '' && $shareId != '' && $userId != ''){
		$_POST['share_category'] =	$shareCat;
		$_POST['share_content']  =	$shareId;
		$_POST['user_by']   		=	$userId;
		$_POST['user_whose']	 =    $shareWhoseId;
		$_POST['share_created']  =	date("Y-m-d H:i:s");
		$getShare				=	$objShare->getRow("share_content=".$shareId." and user_by =".$userId." and share_category=".$shareCat);
		$getVideoId			  =	$objShare->getRowSql("SELECT video_encr_id FROM videos WHERE video_id =".$shareId);
		if($getShare['share_id']==''){
			$_POST['share_status']=	1;
			$objShare->insert($_POST);
			//echo '<a href="javascript:;" class="share_heart2 shareVideo shared" data-share="'.$shareId.'"><i class="fa fa-share-alt"></i>Shared</a>';
			echo 'shared';
			//----notification table------------------------------------
			if($shareWhoseId){
				$myDetails				   		   	   =	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$userId);
				$displayName						  	 =	$objCommon->displayName($myDetails);
				$friend_id							   =	$shareWhoseId;
				$notiType								=	'video_share';
				$notiImg								 =	'';
				$notiDescr  	 	 				   	   =	'<b>'.$displayName.'</b> shared your <b>video</b>.';
				$notiUrl  								 =	SITE_ROOT.'video/watch/'.$getVideoId['video_encr_id'];
				$objCommon->pushNotification($friend_id,$userId,$notiType,$notiImg,$notiDescr,$notiUrl);
			}
			//----------------------------------------------------------
		}else{
			if($getShare['share_status']==1){
				$upStatus		=	0;
				//echo '<a href="javascript:;" class="share_heart2 shareVideo" data-share="'.$shareId.'"><i class="fa fa-share-alt"></i>Share</a>';
				echo 'share';
			}else{
				$upStatus		=	1;
				//echo '<a href="javascript:;" class="share_heart2 shareVideo shared" data-share="'.$shareId.'"><i class="fa fa-share-alt"></i>Shared</a>';
				echo 'shared';
			}
			$objShare->updateField(array("share_status"=>$upStatus),"share_id=".$getShare['share_id']);
		}
	}
}
?>