<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/following.php");
	$objCommon				   =	new common();
	$objFollowing			 	=	new following();
	$friendId			 		=	$objCommon->esc($_GET['friendId']);
	$myId					 	=	$_SESSION['userId'];
	$if						  =	-1;
	if($friendId !='' && $myId !=''){
		$getFriendStatus		 =	$objFollowing->getRow("(follow_user1=".$myId." OR follow_user2=".$myId.") AND (follow_user1=".$friendId." OR follow_user2=".$friendId.")");
		if($getFriendStatus['follow_id']){
			if($getFriendStatus['follow_user1']==$friendId && $getFriendStatus['follow_status'] !=2){
				$if				  =	1;
				$_POST['follow_status']  =	'2';
				$objFollowing->update($_POST,'follow_id='.$getFriendStatus['follow_id']);
			}
		}else{
			$if				  	  =	0;
			$_POST['follow_user1']   =	$myId;
			$_POST['follow_user2']   =	$friendId;
			$_POST['follow_status']  =	'1';
			$objFollowing->insert($_POST);
		}
			//----notification table------------------------------------
			if($if >=0){
				$myDetails				   		   	   =	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$myId);
				$displayName						  	 =	$objCommon->displayName($myDetails);
				$friend_id							   =	$friendId;
				$notiType								=	'following';
				$notiImg								 =	'';
				if($if==1){
					$notiDescr   	 					   =	'<b>You</b> and <b>'.$displayName.'</b> are friends now';
					echo '<a href="javascript:;" class="friend"><i class="fa fa-star-o"></i> Friend</a>';
				}else if($if==0){
					$notiDescr   						   =	'<b>'.$displayName.'</b> is <b>following</b> you';
					echo '<a href="javascript:;" class="following"><i class="fa fa-check"></i> Following</a>';
				}
				$notiUrl  								 =	SITE_ROOT.$objCommon->html2text($myDetails['usl_fameuz']);
				$objCommon->pushNotification($friend_id,$myId,$notiType,$notiImg,$notiDescr,$notiUrl);
			}
			//----------------------------------------------------------
	}
}
?>