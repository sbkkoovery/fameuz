<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
?>
<link href="<?php echo SITE_ROOT?>css/user-profile.css" rel="stylesheet" type="text/css">
<?php /*?><script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.masonry.js"></script><?php */?>
<script type="text/javascript">
//	$(function(){
//		//run masonry when page first loads
//		$(window).load(function() {$('.album_maso').masonry();});
//		//run masonry when window is resized
//			$(window).resize(function() {$('.album_maso').masonry();});
//	});
	</script>
<div class="inner_content_section">
  <div class="container">
    <div class="inner_top_border">
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
          <div class="user_profile"> <?php echo $objCommon->checkEmailverification();?>
            <div class="content">
              <div class="pagination_box"> <a href="#">Home <i class="fa fa-caret-right"></i></a> <a href="#" class="active"> My Photos </a>
                <div class="upload_photo_head">
                  <ul>
                    <li id="create_album_li"><a href="#create_album_pop" name="create"><i class="fa fa-plus"></i> Create Album</a></li>
                    <li id="edit_album_li"><a href="javascript:;" name="create" onclick="editAlbumOpen();"><i class="fa fa-pencil-square-o"></i> Edit Album</a></li>
                    <li id="add_photo_li"><a href="javascript:;" id="add_timeline_photos"><i class="fa fa-camera"></i> Add Photo</a></li>
                    <li id="add_videos_li"><a href="javascript:;" id="add_album_videos"><i class="fa fa-video-camera"></i> Add Video</a></li>
                    <li id="all_photos_li"><a href="javascript:;" class="all_photos">All Albums</a></li>
                  </ul>
                </div>
                <?php
				include(DIR_ROOT."widget/notification_head.php");
			  ?>
              </div>
              <div class="profile_content album-content">
                <div class="top-album-section">
                <div class="album">
                <a href="javascript:;" class="all_photos">Albums</a>
                <img class="arrow_down_tab" src="<?php echo SITE_ROOT?>images/status_option_arrow_down.png" />
                </div>
                <div class="album">
                 <a href="#" onclick="showFullPhotos()">Photos</a>
                </div>
                <div class="album">
                 <a href="#" onclick="showAlbumVideo()">Videos</a>
                </div>
                 </div>
                <div class="profile_border_box">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="load_album_content">
                        <?php //include_once(DIR_ROOT.'includes/album_list_home_page.php');?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="clr"></div>
            </div>
          </div>
        </div>
        <?php
		include_once(DIR_ROOT.'includes/my_page_right.php');
		?>
      </div>
    </div>
  </div>
</div>
<div class="remodal create_album_pop_box" data-remodal-id="create_album_pop">
  <div class="pop_new_album">
    <div class="pop_new_album_head"> New Album </div>
    <div class="pop_new_album_form">
      <div class="form-group">
        <label  class="fontGray">Album Name</label>
        <div class="input-group">
  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-tag"></i></span>
        <input type="text" class="form-control" id="album_name" placeholder="Enter album name here....">
        </div>
      </div>
      <div class="form-group">
        <label class="fontGray">Album Description</label>
        <textarea class="form-control" rows="3" id="album_descr"></textarea>
      </div>
      <div class="form-group">
        <button type="button" class="btn btn-default" id="create_alb">Create</button>
      </div>
    </div>
  </div>
</div>
<input type="hidden" id="edit_album_id" value="" />
<div class="remodal create_album_pop_box" data-remodal-id="edit_album_pop">
  <div class="pop_new_album">
    <div class="pop_new_album_head"> Edit Album </div>
    <div class="pop_new_album_form ret_edit_val"> </div>
  </div>
</div>
<div class="remodal album_pre_loader" data-remodal-id="album_pre_loader">
  <div class="album_pre_load"><img src="<?php echo SITE_ROOT?>images/1.gif" width="32" height="32"/></div>
</div>
<div class="remodal create_album_pop_box" data-remodal-id="upload_video">
  <div class="pop_new_album">
    <div class="pop_new_album_head"> New Album </div>
    <div class="pop_new_album_form">
      <div class="form-group">
        <label  class="fontGray">Album Name</label>
        <input type="text" class="form-control" id="album_name" placeholder="Enter album name here....">
      </div>
      <div class="form-group">
        <label class="fontGray">Album Description</label>
        <textarea class="form-control" rows="3" id="album_descr"></textarea>
      </div>
      <div class="form-group">
        <button type="button" class="btn btn-default" id="create_alb">Create</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
window.location.hash ='list';
$(document).ready(function(e) {
	$(".load_album_content").load('<?php echo SITE_ROOT?>includes/album_list_home_page.php');
	$(".all_photos").click(function(){
		$("#edit_album_id").val('');
		$("#edit_album_li").css("display", "none");$("#all_photos_li").css("display", "none");$("#add_videos_li").css("display", "inline-block");$("#add_photo_li").css("display", "inline-block");  $("#create_album_li").css("display", "inline-block");
		$(".load_album_content").load('<?php echo SITE_ROOT?>includes/album_list_home_page.php');
	});
	$("#add_timeline_photos").click(function(){
		$("#edit_album_id").val('');
		$("#edit_album_li").css("display", "none");$("#add_photo_li").css("display", "none");$("#create_album_li").css("display", "inline-block");;$("#add_videos_li").css("display", "inline-block");$("#all_photos_li").css("display", "inline-block");
		$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/timeline_photo_page.php');
	});
	$("#add_album_videos").click(function(){
		$("#edit_album_id").val('');
		$("#edit_album_li").css("display", "none");$("#create_album_li").css("display", "inline-block");$("#add_videos_li").css("display", "none");$("#add_photo_li").css("display", "inline-block"); $("#all_photos_li").css("display", "inline-block");
		$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/album_video_list.php');
	});
	$("#create_alb").click(function(e){
		var album_name	=	$("#album_name").val();
		var album_descr   =	$("#album_descr").val();
		if(album_name){
			window.location.hash ='album_pre_loader';
			var inst = $.remodal.lookup[$('[data-remodal-id=album_pre_loader]').data('remodal')];
			$.get('<?php echo SITE_ROOT?>ajax/create_album.php',{"album_name":album_name,"album_descr":album_descr},function(data){
				$("#album_name").val('');
				$("#album_descr").val('');
				setTimeout(function () { 
				$("#create_album_li").css("display", "none");
				$("#add_videos_li").css("display", "none");
				$("#edit_album_li").css("display", "inline-block");
				$("#all_photos_li").css("display", "inline-block"); 
				$("#edit_album_id").val(data);
				$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/album_photo_page.php?albumId='+data);inst.close();
				window.location.hash ='list';}, 3000);
			});
		}else{
			 $( "#album_name" ).focus();
			 return false;
		}
	});	
});
function showAlbum(aId){
	window.location.hash ='album_pre_loader';
	$("#create_album_li").css("display", "none");
	$("#add_videos_li").css("display", "none");
	$("#edit_album_li").css("display", "inline-block");
	$("#all_photos_li").css("display", "inline-block"); 
	$("#edit_album_id").val(aId);
	$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/album_photo_page.php?albumId='+aId);
	var inst = $.remodal.lookup[$('[data-remodal-id=album_pre_loader]').data('remodal')];
	setTimeout(function () {inst.close();window.location.hash ='create';}, 3000);
}
function showFullPhotos(){
	window.location.hash ='album_pre_loader';
	$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/fullphotos_load.php');
	var inst = $.remodal.lookup[$('[data-remodal-id=album_pre_loader]').data('remodal')];
	$("#edit_album_li").css("display", "none");$("#create_album_li").css("display", "inline-block");$("#add_videos_li").css("display", "none");$("#add_photo_li").css("display", "inline-block"); $("#all_photos_li").css("display", "inline-block");
	setTimeout(function () { inst.close();window.location.hash ='create';}, 3000);
}
function showWallImg(){
	window.location.hash ='album_pre_loader';
	$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/timeline_photo_page.php');
	var inst = $.remodal.lookup[$('[data-remodal-id=album_pre_loader]').data('remodal')];
	$("#edit_album_li").css("display", "none");$("#add_photo_li").css("display", "none");$("#create_album_li").css("display", "inline-block");;$("#add_videos_li").css("display", "inline-block");$("#all_photos_li").css("display", "inline-block");
	setTimeout(function () { inst.close();window.location.hash ='create';}, 3000);
}
function showAlbumVideo(){
	window.location.hash ='album_pre_loader';
	$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/album_video_list.php');
	var inst = $.remodal.lookup[$('[data-remodal-id=album_pre_loader]').data('remodal')];
	$("#edit_album_li").css("display", "none");$("#create_album_li").css("display", "inline-block");$("#add_videos_li").css("display", "none");$("#add_photo_li").css("display", "inline-block"); $("#all_photos_li").css("display", "inline-block");
	setTimeout(function () { inst.close();window.location.hash ='create';}, 3000);
}

function showProfileImg(){
	window.location.hash ='album_pre_loader';
	$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/profile_photo_page.php');
	var inst = $.remodal.lookup[$('[data-remodal-id=album_pre_loader]').data('remodal')];
	setTimeout(function () { inst.close();window.location.hash ='create';}, 3000);
}
function editAlbumOpen(){
	var inst_open 		= $.remodal.lookup[$('[data-remodal-id=edit_album_pop]').data('remodal')];
	var edit_album_id	= $("#edit_album_id").val();
	$.get('<?php echo SITE_ROOT?>ajax/album_edit_details.php',{'edit_album_id':edit_album_id},function(data){
		$(".ret_edit_val").html(data);
	});
	inst_open.open();
}
function update_form_album(){
		var album_name		=	$("#album_name_edit").val();
		var album_descr   	   =	$("#album_descr_edit").val();
		var update_album_id   =	$("#update_album_id").val();
		if(album_name){
			window.location.hash ='album_pre_loader';
			var inst = $.remodal.lookup[$('[data-remodal-id=album_pre_loader]').data('remodal')];
			$.get('<?php echo SITE_ROOT?>ajax/edit_album.php',{"album_name":album_name,"album_descr":album_descr,"update_album_id":update_album_id},function(data){
				$("#album_name_edit").val('');
				$("#album_descr_edit").val('');
				$("#update_album_id").val('');
				setTimeout(function () { $("#create_album_li").css("display", "none");$("#edit_album_li").css("display", "inline-block"); $("#edit_album_id").val(update_album_id);
				$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/album_photo_page.php?albumId='+update_album_id);inst.close();
				window.location.hash ='list';}, 3000);
			});
		}else{
			 $( "#album_name_edit" ).focus();
			 return false;
		}
}
function delAlbum(albumId){
	var inst1 = $.remodal.lookup[$('[data-remodal-id=album_pre_loader]').data('remodal')];
	var delConfirm		=	confirm('Are you sure you want to delete this item?');
	if(delConfirm){
		window.location.hash ='album_pre_loader';
		$.get('<?php echo SITE_ROOT?>ajax/delete_album.php',{"albumId":albumId},function(data){
			 setTimeout(function () { $("#create_album_li").css("display", "inline-block");$("#edit_album_li").css("display", "none"); $("#edit_album_id").val('');
			 $(".load_album_content").load('<?php echo SITE_ROOT?>includes/album_list_home_page.php');inst1.close();
				window.location.hash ='list';}, 3000);
		});
	}
}
$('.status_box').on("click",'#change_status',function(e) {
                $('#status_option').fadeToggle();
				return false;
});
$('#status_option').on("click",'.check_Status',function(e) {
		var dataVal	=	$(this).attr('data-value');
		if(dataVal){
			$.get('<?php echo SITE_ROOT?>ajax/change_online_chat_status.php',{"dataVal":dataVal},function(data1){
				$(".status_box").html(data1);
			});
		}
	});
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
