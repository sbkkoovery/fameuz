<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/user_photos.php");
	include_once(DIR_ROOT."class/album_images.php");
	$objCommon				  =	new common();
	$objUserPhotos			  =	new user_photos();
	$objAlbumImages			 =	new album_images();
	$userId					 =	$_SESSION['userId'];
	if($_GET['hidImgId'] !=''  && $userId !=''){
		$hid_img_id			 =	$objCommon->esc($_GET['hidImgId']);
		$img_descr	 		  =	$objCommon->esc($_GET['img_descr']);
		$set_cover			  =	$objCommon->esc($_GET['set_cover']);
		$ai_caption			 =	$img_descr;
		$editedTime	         =	date('Y-m-d H:i:s');
		if($set_cover=='1'){
			$objUserPhotos->updateField(array('photo_set_main'=>0),"user_id=".$userId);
			$objAlbumImages->updateField(array('ai_set_main'=>0),"user_id=".$userId);
		}
		$objUserPhotos->updateField(array('photo_descr'=>$ai_caption,'photo_edited'=>$editedTime,'photo_set_main'=>$set_cover),"photo_id=".$hid_img_id);
		
	}
}
?>