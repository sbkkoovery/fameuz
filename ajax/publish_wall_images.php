<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/user_photos.php");
	$objCommon				   =	new common();
	$objUserPhotos			   =	new user_photos();
	$userId					  =	$_SESSION['userId'];
	if(count($_POST['hid_img_id'])>0 && $userId !=''){
		$hid_img_id			  =	$_POST['hid_img_id'];
		$img_descr	 		   =	$_POST['img_descr'];
		foreach($hid_img_id as $keyHid=>$allHidId){
			if($img_descr[$keyHid]){
				$ai_caption	  =	$objCommon->esc($img_descr[$keyHid]);
				$objUserPhotos->updateField(array('photo_descr'=>$ai_caption),"photo_id=".$allHidId);
			}
		}
	}
}
?>