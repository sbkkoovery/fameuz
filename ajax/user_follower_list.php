<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/users.php");
$objCommon				 =	new common();
$objUsers				  =	new users();
$perPage 				   = 	10;
$page 					  = 	1;
$friendId				  =	$objCommon->esc($_GET['friendId']);
if(!empty($_GET["page"])) {
$page = $_GET["page"];
}
$search					=	$objCommon->esc($_GET['search']);
if($search){
	$searchWhere		   =	" and ( user.first_name LIKE '%".$search."%' or  user.display_name LIKE '%".$search."%' or  user.email LIKE '%".$search."%' or  social.usl_fameuz LIKE '%".$search."%') ";
}else{
	$searchWhere		   =	" ";
}
$sqlMyFollowers		    =	"select user.user_id,user.first_name,user.last_name,user.display_name,profileImg.upi_img_url,user.email,social.usl_fameuz,follow.follow_user1,follow.follow_user2,follow.follow_status,case when (TIMESTAMPDIFF(MINUTE,chat.ucs_last_seen,'".date("Y-m-d H:i:s")."') >15 or chat.ucs_status=0) then '0' else '1' END AS chatStatus,personal.p_country,personal.p_city 
from following as follow 
left join users as user on (follow.follow_user1 = user.user_id and follow.follow_user1 !='".$friendId."') or (follow.follow_user2 = user.user_id and follow.follow_user2 !='".$friendId."')  
left join user_chat_status as chat on user.user_id = chat.user_id 
LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN personal_details AS personal ON user.user_id=personal.user_id 
LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
where((follow.follow_user2='".$friendId."') or (follow.follow_user1='".$friendId."' and follow.follow_status='2')) ".$searchWhere." order by user.first_name asc";
$start = ($page-1)*$perPage;
if($start < 0) { $start = 0; }
$queryMyFollowers =  $sqlMyFollowers . " limit " . $start . "," . $perPage; 
if(empty($_GET["rowcount"])) {
	$_GET["rowcount"] = $countFollowers	=	$objUsers->countRows($sqlMyFollowers);
}
$pages  = ceil($_GET["rowcount"]/$perPage);
$getMyFollowers		=	$objUsers->listQuery($queryMyFollowers);
if($start < $countFollowers){
?>
									<?php
									if(count($getMyFollowers)>0){
										echo '<input type="hidden" class="pagenum" value="' . $page . '" /><input type="hidden" class="total-page" value="'.$pages.'" /><input type="hidden" id="total-count" value="'.$countFollowers.'" />';
										foreach($getMyFollowers as $keyFollowers=>$allFollowers){
											$allFollowersImg	=	SITE_ROOT.'uploads/profile_images/'.(($allFollowers['upi_img_url'])?$allFollowers['upi_img_url']:"profile_pic.jpg");
											$allFollowersName		  =	$objCommon->displayName($allFollowers);
											if($allFollowers['follow_status']==2){ 
												$friendStatus =2; 
											}else if($allFollowers['follow_status']==1 && $allFollowers['follow_user1']==$_SESSION['userId']){ 
												$friendStatus =1; 
											}else { 
												$friendStatus=0; 
											}
											if($allFollowers['p_city'] != '' || $allFollowers['p_country'] != ''){
												$friendPcity	=	($allFollowers['p_city'])?$objCommon->html2text($allFollowers['p_city'])." ,":"";
											}
									?>
									<li class="col-md-6">
										<div class="visitor_inner_box">
											<div class="image_thumb pull-left">
												 <a href="<?php echo SITE_ROOT.$objCommon->html2text($allFollowers['usl_fameuz'])?>" title="<?php echo $allFollowersName?>"><img src="<?php echo $allFollowersImg?>" class="img-responsive" alt="<?php echo $allFollowersName?>" title="<?php echo $allFollowersName?>" /></a>
											</div>
											<div class="visitor_box">
												<p class="name_head"><a href="<?php echo SITE_ROOT.$objCommon->html2text($allFollowers['usl_fameuz'])?>" title="<?php echo $allFollowersName?>"><?php echo $allFollowersName?></a></p>
												<p><i class="fa fa-globe"></i> <?php echo $friendPcity.' '.$objCommon->html2text($allFollowers['p_country'])?></p>
												<?php
												if($allFollowers['chatStatus']==1){
													echo '<p class="online_status">online</p>';
												}else{
													echo '<p class="offline_status">offline</p>';
												}
												?>
											</div>
										</div>
									</li>
									<?php
										}
									}
}
?>