<?php
@session_start();
ob_start();
include_once("../includes/site_root.php");
if($_SESSION['userId']){
	include_once(DIR_ROOT."class/notifications.php");
	$objNotifications	  =	new notifications();
	$userId				=	$_SESSION['userId'];
	$countNotification	 =	$objNotifications->count("notification_to=".$userId." and notification_read_status=0 AND notification_type != ''");
	if($countNotification >0){
		echo '<sup>'.$countNotification.'</sup>';
	}
}else{
	?>
	<script>
	window.location.href	=	'<?php echo SITE_ROOT?>';
	</script>
	<?php	
}
?>