<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/album.php");
	include_once(DIR_ROOT."class/album_images.php");
	$objCommon				   =	new common();
	$objAlbum					=	new album();	
	$objAlbumImages			  =	new album_images();
	$albumId				  	 =	$objCommon->esc($_GET['albumId']);
	$userId					  =	$_SESSION['userId'];
	if($albumId !='' && $userId!=''){
		$getImageName			=	$objAlbumImages->getAll("a_id=".$albumId);
		foreach($getImageName as $allImgs){
			$explAiImages	   		=	explode("/",$allImgs['ai_images']);
			$getAiImag1		  	  =	$explAiImages[0].'/thumb/'.$explAiImages[1];
			$getAiImag2		  	  =	$explAiImages[0].'/original/'.$explAiImages[1];
			$imgPath				 =	DIR_ROOT.'uploads/albums/'.$allImgs['ai_images'];
			$imgPath1				=	DIR_ROOT.'uploads/albums/'.$getAiImag1;
			$imgPath2				=	DIR_ROOT.'uploads/albums/'.$getAiImag2;
			unlink($imgPath);
			unlink($imgPath1);
			unlink($imgPath2);
		}
		$objAlbum->delete("a_id=".$albumId);
	}
}
?>