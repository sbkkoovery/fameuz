<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/my_playlist.php");
	$objCommon				   =	new common();
	$objPlayList				 =	new my_playlist();
	$deleteId				  	=	$objCommon->esc($_POST['deleteId']);
	$userId					  =	$_SESSION['userId'];
	if($deleteId !='' && $userId!=''){
		$objPlayList->delete("music_id=".$deleteId." AND user_id=".$userId);
	}
}
?>