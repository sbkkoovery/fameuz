<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/album.php");
include_once(DIR_ROOT."class/album_images.php");
include(DIR_ROOT."ajax/resize-class.php");
include(DIR_ROOT."ajax/face_crop.php");
$objCommon				   =	new common();
$objAlbum					=	new album();
$path 						= 	$_GET['path'];
$userId					  =	$_SESSION['userId'];
$getAlbumComposite		   =	$objAlbum->getRow("user_id=".$userId." AND a_name='Composite Card'");
if($getAlbumComposite['a_id']){
	$albumId				 =	$objCommon->esc($getAlbumComposite['a_id']);
}else{
	$_POST['a_name']		 =	'Composite Card';
	$_POST['user_id']		=	$userId;
	$_POST['a_created']	  =	date("Y-m-d H:i:s");
	$_POST['a_edited']	   =	date("Y-m-d H:i:s");
	$objAlbum->insert($_POST);
	$albumId				 =	$objAlbum->insertId();
}
$objAlbumImages			  =	new album_images();
$valid_formats 			   = array("jpg", "png", "gif", "bmp","jpeg","JPG", "PNG", "GIF", "BMP","JPEG");
$uploadStatus				=	0;
$imgStr			  		  =	'';
if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
{
	$todayDate			   =	date('Y-m-d');
	if(!file_exists($path.$todayDate)){
		mkdir($path.$todayDate);
	}
	$path	=	$path.$todayDate.'/';
$name = $_FILES['file']['name'];
$size = $_FILES['file']['size'];
if(strlen($name))
{
	$ext	=	strtolower(pathinfo($name, PATHINFO_EXTENSION));
	if(in_array($ext,$valid_formats))
	{
	if($size<(3072*10240))
	{
	$time						=	time();				
	$actual_image_name_ext_no	=	$time."_".$userId.'_0';
	$actual_image_name 		   = 	$time."_".$userId."_0.".$ext;
	
	if(!file_exists($path."original")){
		mkdir($path."original");
	}
	if(!file_exists($path."thumb")){
		mkdir($path."thumb");
	}
	$tmpName = $_FILES['file']['tmp_name'];    
list($widthu, $heighut, $typeu, $attru) = getimagesize($tmpName); 
	if(move_uploaded_file($_FILES['file']['tmp_name'], $path.'original/'.$actual_image_name)){
	
	$resizeObj = new resize($path.'original/'.$actual_image_name);
	//$resizeObj -> resizeImage(200, 200, 'exact');
	//$resizeObj -> saveImage($path.'thumb/'.$actual_image_name, 100);
	if($heighut >$widthu){
		if ($heighut > 700){
			$resizeObj -> resizeImage('', 700, 'auto');
			$resizeObj -> saveImage($path.$actual_image_name, 100);
		}else{
			$resizeObj -> resizeImage('', $heighut, 'auto');
			$resizeObj -> saveImage($path.$actual_image_name, 100);
		}
	}else{
		if ($widthu > 900){
			$resizeObj -> resizeImage(900, '', 'auto');
			$resizeObj -> saveImage($path.$actual_image_name, 100);
		}else{
			$resizeObj -> resizeImage($widthu,'', 'auto');
			$resizeObj -> saveImage($path.$actual_image_name, 100);
		}
	}
	//----------------face cropping----------------------------------------------
	if($heighut < 192 || $widthu <192){
		$resizeObj -> resizeImage($widthu, $heighut, 'exact');
		$resizeObj -> saveImage($path.'thumb/'.$actual_image_name, 100);
	}else{
		cropImg($path.'original/'.$actual_image_name,$path.'thumb/',$actual_image_name_ext_no,'192','192');
	}
	//----------------face cropping----------------------------------------------
	//----------------start upload to AWS----------------------------------------
	require(DIR_ROOT."amazone_s3/S3.php");
	$s3			=	new S3(AWS_ID,AWS_KEY);
	S3::putObjectFile($path.$actual_image_name,AWS_IMG_BUCKET,'uploads/albums/'.$todayDate.'/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
	S3::putObjectFile($path.'thumb/'.$actual_image_name,AWS_IMG_BUCKET,'uploads/albums/'.$todayDate.'/thumb/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
	//----------------end upload to AWS-------------------------------------------
	$uploadStatus				=	1;
	$_POST['a_id']			   =	$albumId;
	$_POST['user_id']			=	$userId;
	$_POST['ai_images']		  =	$todayDate.'/'.$actual_image_name;
	$_POST['ai_created']		 =	date("Y-m-d H:i:s");
	$_POST['ai_edited']		  =	date("Y-m-d H:i:s");
	$objAlbumImages->insert($_POST);
	echo '<input type="hidden" id="uploadAlbumImg" value="'.$_POST['ai_images'].'" />';	
	 }
}
}
//else
//echo '<font color="#CC0000">Invalid file format..</font>'; 
}
//else
//echo '<font color="#CC0000">Please select image..!</font>';
	//exit;

}
?>