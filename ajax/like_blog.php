<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/blogs_like.php");
	include_once(DIR_ROOT."class/notifications.php");
	$objCommon				   =	new common();
	$ObBlogLikes				 =	new blogs_like();
	$objNotifications			=	new notifications();
	$likeId					  =	$objCommon->esc($_POST['like']);
	$userId					  =	$_SESSION['userId'];
	if($likeId != '' && $userId != ''){
		$_POST['blog_id']   		=	$likeId;
		$_POST['user_id']   		=	$userId;
		$_POST['bl_date']	  	=	date("Y-m-d H:i:s");
		$getLikes				=	$ObBlogLikes->getRow("blog_id=".$likeId." and user_id =".$userId);
		if($getLikes['bl_id']==''){
			$_POST['bl_status']  =	1;
			$ObBlogLikes->insert($_POST);
			echo '<a href="javascript:;" data-like="'.$likeId.'" class="like liked-blog"><i class="fa fa-heart"></i><span>Liked</span></a>';
		}else{
			if($getLikes['bl_status']==1){
				$upStatus		=	0;
				echo '<a href="javascript:;" data-like="'.$likeId.'" class="like"><i class="fa fa-heart"></i><span>Like</span></a>';
			}else{
				$upStatus		=	1;
				echo '<a href="javascript:;" data-like="'.$likeId.'" class="like liked-blog"><i class="fa fa-heart"></i><span>Liked</span></a>';
			}
			$ObBlogLikes->updateField(array("bl_status"=>$upStatus),"bl_id=".$getLikes['bl_id']);
		}
	}
}
?>