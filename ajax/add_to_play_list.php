<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/my_playlist.php");
	include_once(DIR_ROOT."class/music_views.php");
	$objCommon				   =	new common();
	$objMyPlayList			   =	new my_playlist();
	$objMusicViews			   =	new music_views();
	$music_id					=	$objCommon->esc($_POST['music_id']);
	$userId					  =	$_SESSION['userId'];
	if($userId !='' && $music_id != ''){
		$getMusicViews		   =	$objMusicViews->getRow("user_id=".$userId." AND music_id=".$music_id);
		if($getMusicViews['mv_id'] == ''){
			$_POST['user_id']	=	$userId;
			$_POST['music_id']   =	$music_id;
			$_POST['mv_visit']   =	1;
			$_POST['mv_created'] =	date("Y-m-d H:i:s");
			$objMusicViews->insert($_POST);
		}else{
			$mv_created 		  =	date("Y-m-d H:i:s");
			$objMusicViews->updateField(array('mv_created'=>$mv_created),"user_id = ".$userId." AND music_id=".$music_id);
		}
		$getPlayExist			=	$objMyPlayList->getRow("user_id=".$userId." AND music_id=".$music_id);
		if($getPlayExist['myp_id'] ==''){
			$_POST['user_id']	=	$userId;
			$_POST['music_id']   =	$music_id;
			$_POST['myp_created']=	date("Y-m-d H:i:s");
			$objMyPlayList->insert($_POST);
			$getPlayList		 =	$objMyPlayList->getRowSql("SELECT music.music_id,music.music_title,music.music_descr,music.music_artist,music.music_thumb,music.music_url ,music.music_created,play_list.myp_id ,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name,review.reviewAvg,ml.ml_status
																				FROM music 
																				LEFT JOIN my_playlist AS play_list ON music.music_id = play_list.music_id
																				lEFT JOIN users AS user ON music.user_id = user.user_id
																				LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
																				LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
																				LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id 
																				LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id
																				LEFT JOIN (SELECT AVG(review_rate) AS reviewAvg,user_id_to FROM user_reviews GROUP BY user_id_to ) AS review ON user.user_id = review.user_id_to
																				LEFT JOIN music_like AS ml ON music.music_id = ml.music_id AND ml.user_id = ".$_SESSION['userId']."
																				WHERE play_list.user_id=".$userId." AND play_list.music_id=".$music_id);
		$musicThumb				   =	($getPlayList['music_thumb'])?$getPlayList['music_thumb']:'audio-thump.jpg';
		$musicUser					=	$objCommon->displayName($getPlayList);
		$musicUserThumb			   =	($getPlayList['upi_img_url'])?$getPlayList['upi_img_url']:'profile_pic.jpg';
		$musicCreated				 =	date(" F d, Y ",strtotime($getPlayList['music_created']));
		$musicuserRate				=	round($getPlayList['reviewAvg'])*20;
		$musicLikeStatus			  =	($getPlayList['ml_status']==1)?1:0;
			$json_arr = array("mp3"=>SITE_ROOT."uploads/music/".$getPlayList['music_url'],"title" => addslashes($objCommon->html2text($getPlayList['music_title'])),"artist"=> trim(addslashes($objCommon->html2text($getPlayList['music_artist']))),"dateCreated" => $musicCreated,"rating_yellow" =>$musicuserRate."%","likei" => $getPlayList['music_id'],"sharei" => $keyMyPlayList,"proffesion" => $objCommon->html2text($getPlayList['c_name']),"userInfo" =>$musicUser,"artist1" =>"faisal Darshan","poster" => SITE_ROOT."uploads/music/".$musicThumb,"userthumb" => SITE_ROOT."uploads/profile_images/".$musicUserThumb,"oga" => SITE_ROOT."uploads/music/".$getPlayList['music_url'],
"musicid" => $getPlayList['music_id'],"musicid" =>$getPlayList['music_id'],"musicdesc" => addslashes($objCommon->limitWords($getPlayList['music_descr'],150)),"music_user" => $getPlayList['user_id'],"like_status"=>$musicLikeStatus);
			echo json_encode( $json_arr );
		}
	}
}
?>