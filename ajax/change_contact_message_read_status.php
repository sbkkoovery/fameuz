<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/email_message.php");
	$objCommon				   =	new common();
	$objEmailMsg			     =	new email_message();
	$bookId				  	  =	$objCommon->esc($_GET['bookId']);
	$userId					  =	$_SESSION['userId'];
	if($bookId !='' && $userId!=''){
		$objEmailMsg->updateField(array("em_read_status"=>'1'),"em_id=".$bookId);
	}
}
?>