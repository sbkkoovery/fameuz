<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/album_images.php");
	include_once(DIR_ROOT."class/user_photos.php");
	$objCommon				   =	new common();
	$objAlbumImages			  =	new album_images();
	$objUserPhotos			   =	new user_photos();
	$userId					  =	$_SESSION['userId'];
	if($_POST['imgId'] !=''  && $userId !='' && $_POST['imgCat'] != ''){
		$imgId				  =	$objCommon->esc($_POST['imgId']);
		$imgCat				 =	$objCommon->esc($_POST['imgCat']);
		$objUserPhotos->updateField(array('photo_set_main'=>0),"user_id=".$userId);
		$objAlbumImages->updateField(array('ai_set_main'=>0),"user_id=".$userId);
		if($imgCat ==1){
			$objUserPhotos->updateField(array('photo_set_main'=>1),"photo_id=".$imgId);
		}else if($imgCat ==2){
			$objAlbumImages->updateField(array('ai_set_main'=>1),"ai_id=".$imgId);	
		}
	}
}
?>