<?php 
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/album_images.php");
$objCommon			   =	new common();
$objAlbums			   =	new album_images();
$getUserDetails	  	  =	$objAlbums->getRowSql("SELECT profileImg.upi_img_url,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 WHERE user.status=1 AND user.user_id=".$_SESSION['userId']);
if(isset($_GET['imageId'])){
	$screenHeight		=	$objCommon->esc($_GET['screenHeight']);
	$screenWidth	 	 =	$objCommon->esc($_GET['screenWidth']);
	$imageId		 	 =	$objCommon->esc($_GET['imageId']);
	$im_type		 	 =	$objCommon->esc($_GET['im_type']);
	$albumId			 =	($_GET['albumId'])?$objCommon->esc($_GET['albumId']):0;
	if($im_type==1){
		$imageDetails    =	$objAlbums->getRowSql("SELECT photos.user_id,photos.photo_url AS img_url,photos.photo_id AS img_id,photos.photo_descr as img_descr,photos.photo_created AS img_created,IF(photos.photo_id != '', 1, '') 		AS img_type,photos.photo_like_count AS likeCount,photos.photo_comment_count AS commentCount,photos.photo_set_main AS set_main,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz
	FROM user_photos as photos
	LEFT  JOIN users AS user ON photos.user_id = user.user_id
	LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
	LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
	WHERE photos.photo_id=".$imageId);	
	}else if($im_type==2){
		$imageDetails    =	$objAlbums->getRowSql("SELECT photos.user_id,photos.ai_images AS img_url,ai_id AS img_id,photos.ai_caption AS img_descr,photos.ai_created AS img_created,IF(photos.ai_id != '', 2, '') AS img_type,photos.ai_like_count AS likeCount,photos.ai_comment_count AS commentCount,photos.ai_set_main AS set_main,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz
		FROM album_images  AS photos
		LEFT  JOIN users AS user ON photos.user_id = user.user_id
		LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
		LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
		WHERE ai_id=".$imageId);
	}
//---------------------------------------get images for sliders--------------
if($albumId !='' && $albumId != 0 && $albumId != -1){
	$getOtherPhotosSql				 	=	"SELECT group_concat(tabPhotos.img_id ORDER BY tabPhotos.img_created DESC) AS Ids,group_concat(tabPhotos.img_type ORDER BY tabPhotos.img_created DESC) AS Typs FROM(SELECT ai_id AS img_id,IF(ai_id != '', 2, '') AS img_type,ai_created AS img_created
		FROM album_images 
		WHERE user_id=".$imageDetails['user_id']." AND a_id=".$albumId."
		ORDER BY ai_created desc) AS tabPhotos ORDER BY tabPhotos.img_created DESC";
}else if($albumId !='' && $albumId != 0 && $albumId == -1 && $im_type !=8){
	$getOtherPhotosSql				 	=	"SELECT group_concat(tabPhotos.img_id ORDER BY tabPhotos.img_created DESC) AS Ids,group_concat(tabPhotos.img_type ORDER BY tabPhotos.img_created DESC) AS Typs FROM(SELECT photo_id AS img_id,IF(photo_id != '', 1, '') AS img_type,photo_created AS img_created
		FROM user_photos 
		WHERE user_id=".$imageDetails['user_id']." 
		ORDER BY photo_created DESC) AS tabPhotos ORDER BY tabPhotos.img_created DESC";

}else{
	$getOtherPhotosSql				 	=	"SELECT group_concat(tabPhotos.img_id ORDER BY tabPhotos.img_created DESC) AS Ids,group_concat(tabPhotos.img_type ORDER BY tabPhotos.img_created DESC) AS Typs FROM((SELECT photo_id AS img_id,IF(photo_id != '', 1, '') AS img_type,photo_created AS img_created
		FROM user_photos 
		WHERE user_id=".$imageDetails['user_id']." 
		ORDER BY photo_created DESC)
		UNION ALL
		(SELECT ai_id AS img_id,IF(ai_id != '', 2, '') AS img_type,ai_created AS img_created
		FROM album_images 
		WHERE user_id=".$imageDetails['user_id']."
		ORDER BY ai_created desc)) AS tabPhotos ORDER BY tabPhotos.img_created DESC";
}
$getAllOtherPhotos					=	$objAlbums->getRowSql($getOtherPhotosSql);
$idsArr							   =	$getAllOtherPhotos['Ids'];
$typsArr							  =	$getAllOtherPhotos['Typs'];
$idsArrExpl						   =	explode(",",$idsArr);
$typsArrExpl						  =	explode(",",$typsArr);
if(count($idsArrExpl)>0){
	$ImagArr						  =	array();
	foreach($idsArrExpl as $keyExpl=>$allArrExpl){
		array_push($ImagArr,$allArrExpl.','.$typsArrExpl[$keyExpl]);
	}
	$keyCurrent					   =	array_search($imageId.','.$im_type,$ImagArr);
	$keyNext						  =	$keyCurrent+1;
	$keyPrev						  =	$keyCurrent-1;
	if(array_key_exists($keyNext,$ImagArr)){
		$nextValue					=	$ImagArr[$keyNext];
	}else{
		$nextValue					=	'';
	}
	if(array_key_exists($keyPrev,$ImagArr)){
		$prevValue					=	$ImagArr[$keyPrev];
	}else{
		$prevValue					=	'';
	}
}
//--------------------------------------------------------------------------------
$imageName			   =	SITE_ROOT."uploads/albums/".$imageDetails['img_url'];
$imageExt				=	strtolower(pathinfo($imageName, PATHINFO_EXTENSION));
$imageExt;
switch ($imageExt) { 
	case 'gif' : 
		$imageExpand   = imagecreatefromgif($imageName);
	break; 
	case 'jpeg' : 
		$imageExpand   = imagecreatefromjpeg($imageName);
	break; 
	case 'jpg' : 
		$imageExpand   = imagecreatefromjpeg($imageName);
	break; 
	case 'png' : 
		$imageExpand   = imagecreatefrompng($imageName);
	break; 
}    
$imageWidth		 	=	imagesx($imageExpand);
$imageHeight		   =	imagesy($imageExpand);
$boxWidth		   	  =	$screenWidth-100;
$boxHeight		  	 =	$screenHeight-50;
$rightSide		     =	290;
$maxWidth		   	  =	($boxWidth-$rightSide);
$imageHeight		   =	($imageHeight>$screenHeight)?($boxHeight):$imageHeight;
$dummyHeight		   =	($imageHeight>545)?$imageHeight:545;
$marginTop		  	 =	($screenHeight-$dummyHeight)/2;
$imageMarginTop	 	=	0;
if($imageHeight<545){
	$imageMarginTop	=	(545-$imageHeight)/2;
}
if($imageWidth<$maxWidth){
	$maxWidth		  =	$imageWidth	=	($imageWidth>530)?$imageWidth:530;
	$boxWidth		  =	$maxWidth+$rightSide;
}else{
	
}
$imageBoxWidth		 =	$boxWidth-$rightSide;
$sideHalf			  =	(($imageHeight-300)<150)?150:($imageHeight-300);
$getLikeCount		  =	$objAlbums->getRowSql("SELECT count(like_id) AS likeCount,group_concat(like_user_id) AS youLike FROM likes WHERE like_content =".$imageId." AND like_cat =".$im_type." AND like_status=1");
?>
<div class="image_comment_load">
<div class="alert alertClose alert-success alert-dismissible" role="alert" style="display:none; margin-bottom:0px;"><button type="button" class="close" data-dismiss="alertClose" aria-label="Close"><span aria-hidden="true">&times;</span></button> Updated</div>
    <div class="comments">
        <form class="img_pop_form" id="img_pop_form" action="<?php echo SITE_ROOT?>ajax/photo_settings.php" method="post">
                <div class="form-group">
                    <label for="img_descr">Image description</label>
                    <textarea class="form-control" rows="1" name="img_descr" id="img_descr" placeholder="Say something about...."><?php echo $objCommon->html2textarea($imageDetails['img_descr'])?></textarea>
                    <input type="hidden"  value="<?php echo $imageDetails['img_id']?>" name="hidImgId" class="hidImgId" />
                    <label><input type="checkbox" name="set_cover" id="set_cover" value="1" <?php echo ($imageDetails['set_main']==1)?'checked="checked"':''?> > Set as cover image</label>
                </div>
                <button type="button" class="btn btn-default btn-sm" id="post_img_descr" onclick="sendFrm(this);">save</button>

        </form>
    </div>
    <div class="remove_img pull-right"><a href="javascript:;" onclick="delImg('<?php echo $imageDetails['im_type']?>','<?php echo $imageDetails['img_id']?>');" class="delPopImg">Remove Image</a></div>
</div>
<script type="text/javascript">
function sendFrm(e){
	var hidImgId=$(e).parent().children().children(".hidImgId").val();
	var img_descr=$(e).parent().children().children("#img_descr").val();
	var set_cover;
	if($(e).parent().children().children().children("#set_cover").is(":checked")){
		set_cover	=1;
	}
	if(hidImgId){
		$.ajax({
				url:'<?php echo SITE_ROOT?>ajax/photo_settings.php',
				data:{img_descr:img_descr,hidImgId:hidImgId,set_cover:set_cover},
				type:"GET",
				success: function(){
					//photoPopUp('<?php echo $imageId?>','<?php echo $im_type?>',-1);
					$( ".alertClose" ).show().delay( 4000 ).slideUp( 400 );
				}
		});
	}
}
function delImg(imgid){	
	$.confirm({
		'title'		: 'Delete Confirmation',
		'message'	: 'You are about to delete this item ?',
		'buttons'	: {
		'Yes'	: {
		'class'	: 'blue',
		'action': function(){
			$("#pop_up").fadeOut(300,function(){
				$("#pop_up").html("");
				$(".cd-popup-container").hide();
			});
			$(".load_album_content").html('<div class="load_album_preloader"></div>');
			$.ajax({
				url:'<?php echo SITE_ROOT?>ajax/delete_img_wall.php',
				data:{imgid:imgid},
				type:"GET",
				success: function(){
					$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/timeline_photo_page.php');
					$(".load_album_preloader").hide();
				}
			});
		}
		},
		'No'	: {
		'class'	: 'gray',
		'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
		}
		}
	});
}
</script>
<?php }?>