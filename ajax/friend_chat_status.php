<?php
@session_start();
date_default_timezone_set('UTC');
ini_set('date.timezone', 'UTC');
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/user_chat_status.php");
$objUserChatStatus			=	new user_chat_status();
$objCommon				  	=	new common();
$timenow					  =	date("Y-m-d H:i:s");
$fid						  =	$objCommon->esc($_REQUEST['fid']);
$getChatStatus				=	$objUserChatStatus->getRowSql("select ucs_last_seen,ucs_status,ucs_online_status,case when (TIMESTAMPDIFF(MINUTE,ucs_last_seen,'".$timenow."') >15 or ucs_status=0) then '0' else  '1' END AS chatStatus from user_chat_status where user_id=".$fid);
if($getChatStatus['chatStatus']==1){
	if($getChatStatus['ucs_online_status']==1){ 
		$onlineStat	=	'online';
		$onlineStatStr =	'Available';
	} else if($getChatStatus['ucs_online_status']==2){
		$onlineStat	=	'busy';
		$onlineStatStr =	'Busy'; 
	}else if($getChatStatus['ucs_online_status']==0){
		$onlineStat	=	'offline';
		$onlineStatStr =	'offline'; 
	}
	echo '<div class="status '.$onlineStat.'"><i class="fa fa-circle"></i>'.$onlineStatStr.'</div>';
}else if($getChatStatus['chatStatus']==0){
	echo '<div class="status offline"><i class="fa fa-circle"></i>Offline</div>';
	$friendLastSeen		  =	date("Y-m-d",strtotime($getChatStatus['ucs_last_seen']));
	$timenow2				=	date("Y-m-d");
	$timeYester			  =	date('Y-m-d',strtotime('-1 days'));
	$newLastSeen			 =	$objCommon->local_time($getChatStatus['ucs_last_seen']);
	if($timenow2==$friendLastSeen){
		echo '<p class="time"><i class="fa fa-clock-o"></i>Last seen today at '.date("h:i a",strtotime($newLastSeen)).' </p>';
	}else if($timeYester==$friendLastSeen){
		echo '<p class="time"><i class="fa fa-clock-o"></i>Last seen yesterday at '.date("h:i a",strtotime($newLastSeen)).' </p>';
	}else{
		echo '<p class="time"><i class="fa fa-clock-o"></i>Last seen '.date("l F d, h:i a",strtotime($newLastSeen)).'</p>';
	}	
}
?>