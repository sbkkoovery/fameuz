<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/game_player.php");
	$objCommon				   =	new common();
	$objGamePlayer			   =	new game_player();
	$video_id					=	$objCommon->esc($_POST['video_id']);
	$userId					  =	$_SESSION['userId'];
	if($userId !='' && $video_id != ''){
		$getGamePlayer		   =	$objGamePlayer->getRow("user_id=".$userId." AND g_id=".$video_id);
		if($getGamePlayer['gp_id'] ==''){
			$_POST['user_id']	=	$userId;
			$_POST['g_id']   	   =	$video_id;
			$_POST['gp_created'] =	date("Y-m-d H:i:s");
			$objGamePlayer->insert($_POST);
		}else{
			$gp_created 		  =	date("Y-m-d H:i:s");
			$objGamePlayer->updateField(array("gp_created"=>$gp_created),"gp_id=".$getGamePlayer['gp_id']);	
		}
	}
}
?>