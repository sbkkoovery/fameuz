<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/comments.php");
$objCommon				   =	new common();
$objComments			     =	new comments();
$type						=	$objCommon->esc($_GET['type']);
$id						  =	$objCommon->esc($_GET['id']);
$userto					  =	$objCommon->esc($_GET['userto']);
$myimage					 =	$objCommon->esc($_GET['myimage']);
$page						=	($objCommon->esc($_GET['page']))?$objCommon->esc($_GET['page']):1;
$pageCount				   =	5;
$pageLimit				   =	$page*$pageCount;
?>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.form.js"></script>
<h5 class="heading with_border"><span>Comments</span></h5>
<?php
$sqlComments		    	=	"SELECT comments.comment_id,comments.comment_descr,comments.comment_time,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz FROM comments 
	LEFT JOIN users AS user ON comments.comment_user_by = user.user_id
	LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
	LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
	WHERE comments.comment_status=1 and comments.comment_cat=".$type." AND comments.comment_content =".$id." AND comments.comment_status=1 ORDER BY comments.comment_time desc ";
$countSql				  =	$objComments->countRows($sqlComments);
$queryComments			 =	$sqlComments." limit 0,".$pageLimit;	
$getMyComments			 =	$objComments->listQuery($queryComments);
if($pageLimit < $countSql){
?>
<div class="loadMoreTime"><a href="javascript:;" class="view_more_comments">View more comments</a></div>
<?php
}
if(count($getMyComments)>0){
		for($i=(count($getMyComments)-1);$i>=0;$i--){
		if($getMyComments[$i]['display_name']){
			$displayNameComment	 =	$objCommon->html2text($getMyComments[$i]['display_name']);
		}else if($getMyFriendDetails['first_name']){
			$displayNameComment	 =	$objCommon->html2text($getMyComments[$i]['first_name']);
		}else{
			$exploEmailComment	  =	explode("@",$objCommon->html2text($getMyComments[$i]['email']));
			$displayNameComment	 =	$exploEmailComment[0];
		}
		$getLikeCount			   =	$objComments->getRowSql("SELECT count(like_id) AS likeCount,group_concat(like_user_id) AS youLike FROM likes WHERE like_content =".$getMyComments[$i]['comment_id']." AND like_cat =3 AND like_status=1");
		$likeCount				  =	$getLikeCount['likeCount'];
		$youLikeArr	   			 =	explode(",",$getLikeCount['youLike']);
		if(count($youLikeArr)>0){
			if(in_array($_SESSION['userId'],$youLikeArr)){
				$youLike	  		=	1;
			}else{
				$youLike	  		=	0;
			}
		}
	?>
	<div class="commentsingle left100">
		<div class="thumbImg"><a href="<?php echo SITE_ROOT.$objCommon->html2text($getMyComments[$i]['usl_fameuz'])?>" title="<?php echo $displayNameComment?>"><img alt="thumb" src="<?php echo SITE_ROOT?>uploads/profile_images/<?php echo($getMyComments[$i]['upi_img_url'])?$objCommon->html2text($getMyComments[$i]['upi_img_url']):'profile_pic.jpg'?>"></a></div>
		<div class="commentsText">
			<h6><a href="<?php echo SITE_ROOT.$objCommon->html2text($getMyComments[$i]['usl_fameuz'])?>" title="<?php echo $displayNameComment?>"><?php echo $displayNameComment?></a></h6>
			<p><?php echo $objCommon->html2text($getMyComments[$i]['comment_descr']);?></p>
			<div class="clr"></div>
			<div class="likeBox left100"> 
				<div class="likecom">
					<a href="javascript:;" class="left10 like_button <?php echo ($youLike==1)?'liked':''?>" data-like="<?php echo $objCommon->html2text($getMyComments[$i]['comment_id'])?>" data-userto="<?php echo $objCommon->html2text($getMyComments[$i]['user_id'])?>">
						<?php echo ($youLike==1)?'Liked':'Like'?> <i class="fa fa-heart"></i> <span><?php echo ($likeCount >0)?$likeCount:''?></span>
					</a>
				</div>
				<div class="time pull-left"><i class="fa fa-clock-o"></i> <?php echo date("d M , Y h:i a",strtotime($objCommon->local_time($getMyComments[$i]['comment_time'])));?></div>
			</div>
		</div>
		<?php
		if($getMyComments[$i]['user_id']== $_SESSION['userId']){
		?>
		<div class="deletComment"><a href="javascript:;" title="Delete" data-delid="<?php echo $getMyComments[$i]['comment_id']?>"><i class="fa fa-times"></i></a></div>
		<?php
		}
		?>
	</div>
	<?php
	}
}
?>
<div class="clr"></div>
<div class="working_comments">
	<div class="thumb"><img alt="thumb" src="<?php echo SITE_ROOT?>uploads/profile_images/<?php echo($myimage)?$myimage:'profile_pic.jpg'?>"></div>
	<div class="comments">
		<div class="comments_arrow"><img alt="comments_arrow" src="<?php echo SITE_ROOT?>images/comments_arrow.png"></div>
		<form action="<?php echo SITE_ROOT?>access/post_comment.php?action=add_comment" method="post" id="comment_form">
			<textarea name="comment_descr" id="comment_descr" placeholder="Your comment...." ></textarea>
			<div class="upload_section">
				<div class="post_btns">
					<input type="submit" value="Post" id="post_submit">
				</div>
			</div>
			<input type="hidden" name="type" value="<?php echo $type?>"  />
			<input type="hidden" name="id" value="<?php echo $id?>"  />
			<input type="hidden" name="userto" value="<?php echo $userto?>"  />
		</form>
		<div class="clr"></div>
	</div>
</div>
<div id="preview"></div>
<script type="text/javascript">
$('.post_btns').on('click','#post_submit',function(){
	$("#comment_form").ajaxForm(
		{
			target: '#preview',
			success:successCall
		}).submit();
		return false;
});
$("#comment_form").on("click","#comment_descr",function(){
	$(".upload_section").slideDown();
});
$(".loadMoreTime").on("click",".view_more_comments",function(){
		$(".commentBox").load('<?php echo SITE_ROOT?>ajax/comments_list.php?type=<?php echo $type?>&id=<?php echo $id?>&userto=<?php echo $userto?>&myimage=<?php echo $myimage?>&page=<?php echo ($page+1)?>');
});
$(".likecom").on("click",".like_button",function(){
		var likeId		=	$(this).data('like');
		var likecat	   =	3;
		var imgUserId	 =	$(this).data('userto');
		var that		  =	this;
		var imageType	 =	'<?php echo $type?>';
		var imageId	   =	'<?php echo $id?>';
		if(likeId !='' && likecat != ''){
			$.ajax({
				url:"<?php echo SITE_ROOT?>ajax/like_comment.php",
				method: "POST",
				data:{likeId:likeId,likecat:likecat,imgUserId:imgUserId,imageType:imageType,imageId:imageId},
				success:function(result){
					if(result == 'cmdLiked'){
						$(that).addClass('liked');
					}else if(result == 'cmdLike'){
						$(that).removeClass('liked');
					}
					//$(that).parent().html(result);
			}});
		}
});
function successCall(){
	 $(".commentBox").load('<?php echo SITE_ROOT?>ajax/comments_list.php?type=<?php echo $type?>&id=<?php echo $id?>&userto=<?php echo $userto?>&myimage=<?php echo $myimage?>');
}
$(".commentsingle").on("mouseover",function(){
	$(".deletComment a").hide();
	$(this).find(".deletComment a").show();
});
$(".commentsingle").on("mouseout",function(){
	$(".deletComment a").hide();
});
$(".deletComment").on("click","a",function(){
	var elem = $(this).closest('.item');	
	var that		=	this;			
	$.confirm({
		'title'	  : 'Delete Confirmation',
		'message'	: 'You are about to delete this comment ?',
		'buttons'	: {
		'Yes'	: {
		'class'	: 'blue',
		'action': function(){
		elem.slideUp();
			var delid       =	$(that).data("delid");
			if(delid){
				$.ajax({
					url :'<?php echo SITE_ROOT?>access/delete_comment.php',
					method:"POST",
					data:{delid:delid},
					success:function(result){
						$(".commentBox").load('<?php echo SITE_ROOT?>ajax/comments_list.php?type=<?php echo $type?>&id=<?php echo $id?>&userto=<?php echo $userto?>&myimage=<?php echo $myimage?>');
					}});
			}
		}
		},
		'No'	: {
		'class'	: 'gray',
		'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
		}
		}
	});
});
</script>