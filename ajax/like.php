<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/likes.php");
	$objCommon				   =	new common();
	$ObjLikes					=	new likes();
	$likeId					  =	$objCommon->esc($_POST['likeId']);
	$likecat					 =	$objCommon->esc($_POST['likecat']);
	$imgUserId				   =	$objCommon->esc($_POST['imgUserId']);
	$userId					  =	$_SESSION['userId'];
	if($likecat != '' && $likeId != '' && $userId != ''){
		$_POST['like_cat']	   =	$likecat;
		$_POST['like_content']   =	$likeId;
		$_POST['like_user_id']   =	$userId;
		$_POST['like_img_user_by']=   $imgUserId;
		$_POST['like_time']	  =	date("Y-m-d H:i:s");
		$getLikes				=	$ObjLikes->getRow("like_content=".$likeId." and like_user_id =".$userId." and like_cat=".$likecat);
		if($getLikes['like_id']==''){
			$_POST['like_status']=	1;
			$ObjLikes->insert($_POST);
			if($likecat==1){
				$ObjLikes->build_result_insert("update user_photos SET photo_like_count=photo_like_count+1 where photo_id=".$likeId);
			}else if($likecat==2){
				$ObjLikes->build_result_insert("update album_images SET ai_like_count=ai_like_count+1 where ai_id=".$likeId);
			}else if($likecat==6){
				$ObjLikes->build_result_insert("update user_profile_image SET upi_like_count=upi_like_count+1 where upi_id=".$likeId);
			}
			
			//echo '<a data-likecat="'.$likecat.'" data-like="'.$likeId.'" id="like_btn" href="javascript:;" class="liked like_btn"><i class="fa fa-heart"></i> <span class="text">Liked</span> </a>';
			echo 'liked';
			//----notification table------------------------------------
			$myDetails				   		   	   =	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$userId);
			$displayName						  	 =	$objCommon->displayName($myDetails);
			$friend_id							   =	$imgUserId;
			$notiType								=	'likes';
			$notiImg								 =	'';
			$notiDescr  	 	 				   	   =	'<b>'.$displayName.'</b> liked your <b>image</b>.';
			$notiUrl  								 =	SITE_ROOT.'user/single-image?type='.$likecat.'&id='.$likeId;
			$objCommon->pushNotification($friend_id,$userId,$notiType,$notiImg,$notiDescr,$notiUrl);
			//----------------------------------------------------------
		}else{
			if($getLikes['like_status']==1){
				$upStatus		=	0;
				if($likecat==1){
					$ObjLikes->build_result_insert("update user_photos SET photo_like_count=photo_like_count-1 where photo_id=".$likeId);
				}else if($likecat==2){
					$ObjLikes->build_result_insert("update album_images SET ai_like_count=ai_like_count-1 where ai_id=".$likeId);
				}
				//echo '<a data-likecat="'.$likecat.'" data-like="'.$likeId.'" id="like_btn" href="javascript:;" class="like_btn"><i class="fa fa-heart"></i> <span class="text">Like</span> </a>';
				echo 'like';
			}else{
				$upStatus		=	1;
				if($likecat==1){
					$ObjLikes->build_result_insert("update user_photos SET photo_like_count=photo_like_count+1 where photo_id=".$likeId);
				}else if($likecat==2){
					$ObjLikes->build_result_insert("update album_images SET ai_like_count=ai_like_count+1 where ai_id=".$likeId);
				}
				//echo '<a data-likecat="'.$likecat.'" data-like="'.$likeId.'" id="like_btn" href="javascript:;" class="liked like_btn"><i class="fa fa-heart"></i> <span class="text">Liked</span> </a>';
				echo 'liked';
			}
			$ObjLikes->updateField(array("like_status"=>$upStatus),"like_id=".$getLikes['like_id']);
		}
	}
}
?>