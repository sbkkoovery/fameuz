<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	$objCommon				   =	new common();
	$perPage 				   	 = 	30;
	$page 					  	= 	1;
	if(!empty($_GET["page"])) {
		$page 					=	$_GET["page"];
	}
	$search_keyword			  =	$objCommon->esc($_GET['filter_keywords']);
	$search_type			  	 =	$objCommon->esc($_GET['filter_category']);
	$myCategoryId		   		=	$objCommon->esc($_GET['myCategoryId']);
	$countryId		   	  	   =	$objCommon->esc($_GET['countryId']);
	if($search_keyword){
		$sqlSearch		 	   =	" AND (music.music_title LIKE '%".$search_keyword."%' OR music.music_artist LIKE '%".$search_keyword."%') ";
	}else{
		$sqlSearch			   =	" ";
	}
	if($search_type){
		if($search_type=='new_music'){
			$sqlMemberSearch			 =	"SELECT music.*,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz
											FROM music  
											LEFT JOIN users AS user ON  music.user_id = user.user_id
											LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
											WHERE music.music_status=1 AND user.status=1 AND user.email_validation =1 ".$sqlSearch." ORDER BY music.music_created DESC";
		}else if($search_type=='top_music'){
			$sqlMemberSearch			= 	"SELECT music.music_id,music.music_title,music.music_descr,music.music_artist,music.music_thumb,music.music_url ,music.music_created,play_list.myp_id ,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name,review.reviewAvg,ml.ml_status,musicLike.likeCount,CASE WHEN play_list.myp_id > 0 THEN 1 ELSE 0 END as PlayList
										FROM music
										lEFT JOIN users AS user ON music.user_id = user.user_id
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id 
										LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
										LEFT JOIN (SELECT AVG(review_rate) AS reviewAvg,user_id_to FROM user_reviews GROUP BY user_id_to ) AS review ON user.user_id = review.user_id_to
										LEFT JOIN (SELECT SUM(ml_status) AS likeCount,music_id FROM music_like GROUP BY music_id) AS musicLike ON music.music_id = musicLike.music_id
										LEFT JOIN music_like AS ml ON music.music_id = ml.music_id AND ml.user_id = ".$_SESSION['userId']."
										LEFT JOIN my_playlist AS play_list ON music.music_id = play_list.music_id AND play_list.user_id = ".$_SESSION['userId']."
										WHERE music.music_status=1 ".$sqlSearch." ORDER BY musicLike.likeCount DESC";
		}else if($search_type=='popular_music'){
			$sqlMemberSearch			=	"SELECT music.music_id,music.music_title,music.music_descr,music.music_artist,music.music_thumb,music.music_url ,music.music_created,play_list.myp_id ,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name,review.reviewAvg,mv.mv_visit,CASE WHEN play_list.myp_id > 0 THEN 1 ELSE 0 END as PlayList
										FROM music
										lEFT JOIN users AS user ON music.user_id = user.user_id
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id 
										LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
										LEFT JOIN (SELECT AVG(review_rate) AS reviewAvg,user_id_to FROM user_reviews GROUP BY user_id_to ) AS review ON user.user_id = review.user_id_to
										LEFT JOIN music_views AS mv ON music.music_id = mv.music_id
										LEFT JOIN my_playlist AS play_list ON music.music_id = play_list.music_id AND play_list.user_id = ".$_SESSION['userId']."
										WHERE music.music_status=1 ".$sqlSearch." ORDER BY mv.mv_visit DESC";
		}else if($search_type=='last_visited'){
		$sqlMemberSearch			   =	"SELECT music.music_id,music.music_title,music.music_descr,music.music_artist,music.music_thumb,music.music_url ,music.music_created,play_list.myp_id ,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name,review.reviewAvg,CASE WHEN play_list.myp_id > 0 THEN 1 ELSE 0 END as PlayList
										FROM music_views AS mviews
										LEFT JOIN  music ON mviews.music_id = music.music_id
										lEFT JOIN users AS user ON music.user_id = user.user_id
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id 
										LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
										LEFT JOIN (SELECT AVG(review_rate) AS reviewAvg,user_id_to FROM user_reviews GROUP BY user_id_to ) AS review ON user.user_id = review.user_id_to
										LEFT JOIN my_playlist AS play_list ON music.music_id = play_list.music_id AND play_list.user_id = ".$_SESSION['userId']."
										WHERE  music.music_status=1 AND mviews.user_id= ".$_SESSION['userId'].$sqlSearch."  ORDER BY mviews.mv_created DESC";
		}else if($search_type=='all_music'){
		$sqlMemberSearch			   =	"SELECT music.music_id,music.music_title,music.music_descr,music.music_artist,music.music_thumb,music.music_url ,music.music_created,play_list.myp_id ,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name,review.reviewAvg,ml.ml_status,CASE WHEN play_list.myp_id > 0 THEN 1 ELSE 0 END as PlayList,case when (promo.promo_id !='') then 1 else 0 END AS promoted_music
										FROM music
										lEFT JOIN users AS user ON music.user_id = user.user_id
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id 
										LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
										LEFT JOIN (SELECT AVG(review_rate) AS reviewAvg,user_id_to FROM user_reviews GROUP BY user_id_to ) AS review ON user.user_id = review.user_id_to
										LEFT JOIN music_like AS ml ON music.music_id = ml.music_id AND ml.user_id = ".$_SESSION['userId']."
										LEFT JOIN my_playlist AS play_list ON music.music_id = play_list.music_id AND play_list.user_id = ".$_SESSION['userId']."
										LEFT JOIN promotion AS promo ON music.music_id = promo.promo_content_id AND promo.promo_type='music' AND  promo.promo_country LIKE '%,".$countryId.",%' AND promo.promo_category LIKE '%".$myCategoryId."%' AND DATE(NOW()) between promo.promo_start_date and promo.promo_end_date AND promo.promo_admin_status=1 AND promo.promo_status=1
										WHERE music.music_status=1 ".$sqlSearch." ORDER BY promoted_music DESC,music_created DESC";
		}
	}else{
		$sqlMemberSearch			   =	"SELECT music.*,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz,case when (promo.promo_id !='') then 1 else 0 END AS promoted_music
											FROM music  
											LEFT JOIN users AS user ON  music.user_id = user.user_id
											LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
											LEFT JOIN promotion AS promo ON music.music_id = promo.promo_content_id AND promo.promo_type='music' AND  promo.promo_country LIKE '%,".$countryId.",%' AND promo.promo_category LIKE '%".$myCategoryId."%' AND DATE(NOW()) between promo.promo_start_date and promo.promo_end_date AND promo.promo_admin_status=1 AND promo.promo_status=1 
											WHERE music.music_status=1 AND user.status=1 AND user.email_validation =1 ".$sqlSearch." ORDER BY promoted_music DESC,music.music_created DESC";
	}
$start 						   =	($page-1)*$perPage;
if($start < 0) { $start = 0; }
	$queryMemberSearch 		   =	$sqlMemberSearch . " limit " . $start . "," . $perPage; 
if(empty($_GET["rowcount"])) {
	$_GET["rowcount"] 		    =	$countSearchMembers	=	$objUsers->countRows($sqlMemberSearch);
}
$pages  = ceil($_GET["rowcount"]/$perPage);
$getMemberSearch			  	 =	$objUsers->listQuery($queryMemberSearch);
if($start < $countSearchMembers){
	?>
	<div class="row">
	<?php
	if(count($getMemberSearch)>0){
		echo '<input type="hidden" class="pagenum" value="' . $page . '" /><input type="hidden" class="total-page" value="' . $pages . '" /><input type="hidden" id="total-count" value="'.$countSearchMembers.'" />';
		foreach($getMemberSearch as $allVideoDetails){
			$musicThumb				     =	($allVideoDetails['music_thumb'])?$allVideoDetails['music_thumb']:'audio-thump.jpg';
			$displayNameFriend			  =	$objCommon->displayName($allVideoDetails);
			$vidcreatedTime				 =	$objCommon->time_elapsed_string(strtotime($objCommon->local_time($allVideoDetails['music_created'])));
			$vidRateCound				   =	$objCommon->round_to_nearest_half($allVideoDetails['voteAvg']);
			$vidRateCoundStyle	   		  =	'style="width:'.($vidRateCound*20).'%;"';
	?>
		   <div class="col-sm-6 videoSearch">
				<div class="item item_music">
					<div class="row">	
                		<div class="col-xs-12 col-sm-5 col-md-4">
						<a href="<?php echo SITE_ROOT.'music?musicid='.$objCommon->html2text($allVideoDetails['music_id'])?>" class="vidimg MusicImg ovr" style="background-image:url('<?php echo SITE_ROOT.'uploads/music/'.$musicThumb;?>');">
						</a>
					</div>
					<div class="col-xs-12 col-sm-7 col-md-8 vidDescr vidDescr_music">
						<h2><a href="<?php echo SITE_ROOT.'music?musicid='.$objCommon->html2text($allVideoDetails['music_id'])?>"><?php echo $objCommon->limitWords($allVideoDetails['music_title'], 55)?></a></h2>
						<div class="row">
						<div class="col-sm-12"><p class="publishedBy"><i class="fa_publish"></i> <span class="publishedBySpan">Published By:</span> <a href="<?php echo SITE_ROOT.$objCommon->html2text($allVideoDetails['usl_fameuz'])?>"><?php echo $displayNameFriend?></a></p></div><div class="col-sm-12"><p class="publishedBy"><i class="fa_clock"></i> <?php echo $vidcreatedTime?></p></div></div>
                        
						<p><?php echo $objCommon->limitWords($allVideoDetails['video_descr'],120)?></p>
						<div class="row">
							<?php
							if($allVideoDetails['promoted_music']==1){
								echo '<div class="col-sm-5"><div class="promoted_music"><i class="fa fa-external-link-square"></i> Promoted</div></div>';
							}
							?>
						</div>
						
					</div>
					</div>
				</div>
			</div>
<?php
		}
	}
	echo '</div>';
}

}
?>
