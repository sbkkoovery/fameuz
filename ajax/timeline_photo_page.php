<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/user_photos.php");
$objCommon				   =	new common();
$objUserPhotos			   =	new user_photos();
$userId					  =	$_SESSION['userId'];
if($userId !=''){
	$getTimeLinePhotos	   =	$objUserPhotos->listQuery("select photos.* from user_photos as photos  where photos.user_id=".$userId." order by photos.photo_created	desc");
?>
<!-------------------------------------------------------------------------------------------------------------->
<div class="dropZoneSection">
    <div class="my_prev">
            <form action="<?php echo SITE_ROOT?>ajax/ajaximage_wall.php?path=<?php echo DIR_ROOT?>uploads/albums/" class="dropzone" id="newUplaod" method="post"></form>
            <div id="drop_message">Drop Your File Here</div>
        </div>
    <div class="form_submit">
        	<button class="btn btn-default pull-right removeAttr media_form_submit" disabled="disabled">Submit</button>
        <div class="clearfix"></div>
    </div>
</div>
<script>
$("#newUplaod").dropzone({
	   paramName: "file[]",
	   addRemoveLinks: false,
       thumbnailWidth: 182,
       thumbnailHeight: 184,
	   success:function(file, response){
		   $('#drop_message').show();
		   $('.my_prev .title_upload_dz, .my_prev .desc_photo_dz').click(function(){
			   insertId($(this));
			});
			$('.dz-remove').click(function(){
				var deletId	=	$(this).parent('.dz-preview').attr('id');
				$(this).attr('data-deleteId',deletId );
				
			});
	  },
	  queuecomplete: function(){
			$('.my_prev .title_upload_dz, .my_prev .desc_photo_dz, .removeAttr').removeAttr('disabled');
	 }
});

function insertId(_this) {
	var primaryId	=	_this.parent('.form_section').parent('.dz-preview').attr('id');
	
		_this.parent('.form_section').children('.hiddenField').attr({'name': 'hiddenIdDz[]', 'value':primaryId});
		_this.parent('.form_section').children('.desc_photo_dz').attr('name', 'dZdesc_'+primaryId);
	
}
</script>
<!------------------------------------------------------------------------------------------------------------->
	<?php /*?><div class="row albListShow">
		<div class="col-md-12">
			<div class="add_photo_album text-center">
				<form action='<?php echo SITE_ROOT?>ajax/ajaximage_wall.php?path=<?php echo DIR_ROOT?>uploads/albums/' method="post" enctype="multipart/form-data" id="media_form">
					<i class="fa fa-camera"></i> Add Photos to wall<input type="file" class="album_uploader"  name="file[]" id="file_browse" multiple="multiple">
				</form>
			</div>
		</div>
	</div><?php */?>
	<?php
	if(count($getTimeLinePhotos)>0){
	?>
	<div class="row albListShow">
	<div class="col-md-12">
	<div class="add_photo_album_list">
		<ul class="album_list_ul">
			<?php
			foreach($getTimeLinePhotos as $allTimeLinePhotos){
				$getAiImages		=	$objCommon->getThumb($allTimeLinePhotos['photo_url']);
				?>
			<li><div class="pic"><a href="javascript:;" class="lightBoxs" data-contentId="<?php echo $allTimeLinePhotos['photo_id']?>" data-contentType="1" data-contentAlbum="-1"><img src="<?php echo SITE_ROOT?>uploads/albums/<?php echo $getAiImages?>" class="img-responsive"  /></a></div></li>
			<?php
			}
			?>
		</ul>
	</div>
	</div>
	</div>
	<?php
	}else{
		echo '<div class="row albListShow"><div class="col-md-12"><p class="no_img_album text-center">There are no photos in your wall</p></div></div>';
	}
	?>
	<div class="row">
		<div class="col-md-12">
		<div id="preview"></div>
		</div>
	</div>
	<?php
}
?>
<script type="text/javascript">
$(document).ready(function(e) {
	$('.media_form_submit').on('click', function(){ 
		$("#preview").html('<div class="load_album_preloader"></div>');
		$("#newUplaod").ajaxForm(
				{
					target: '#preview',
					success:successCall
				}).submit();
				$(".albListShow").hide();
	});
});
function successCall(){
	$(".load_album_preloader").hide();
	$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/timeline_photo_page.php');
}
</script>