<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/promotion_based_duration.php");

	$objPromoduration			=	new promotion_based_duration();
	$objCommon				   =	new common();
	
	$date1				       =	$objCommon->esc($_POST['dat']);
	$duration				    =	$objCommon->esc($_POST['days']); 
	$getDays				   =	$objPromoduration->getRow("pbd_id =".$duration);	
	$totDays                   =    $getDays['pbd_days'];
	$fromdate                  = strtotime($date1);
	$todate                    = strtotime("+".$totDays."day", $fromdate);	
	$todatePromo               = date('m/d/Y', $todate);
	
	$date11=date("Y-m-d",strtotime($date1));
	$date12=date("Y-m-d",strtotime($todatePromo));
	
	echo "<span>From: ".$date11." To: ".$date12."</span>"; 
}
?>