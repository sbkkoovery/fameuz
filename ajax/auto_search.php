<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/users.php");
$objCommon				     =	new common();
$objUser					   =	new users();
$userId						=	$_SESSION['userId'];
$searchVal					 =	$objCommon->esc($_GET['searchVal']);
if($userId !='' && $searchVal != ''){
	$sqlUserSearch		     =	"select user.user_id,user.first_name,user.last_name,user.display_name,profileImg.upi_img_url,user.email,social.usl_fameuz,follow.follow_user1,follow.follow_user2,follow.follow_status,case when (TIMESTAMPDIFF(MINUTE,chat.ucs_last_seen,'".date("Y-m-d H:i:s")."') >15 or chat.ucs_status=0) then '0' else '1' END AS chatStatus,cat.c_name
	from following as follow 
	left join users as user on (follow.follow_user1 = user.user_id and follow.follow_user1 !='".$userId."') or (follow.follow_user2 = user.user_id and follow.follow_user2 !='".$userId."')  
	left join user_chat_status as chat on user.user_id = chat.user_id
	LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
	LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
	LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
	LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
	where((follow.follow_user2='".$userId."') or (follow.follow_user1='".$userId."')) AND (user.first_name LIKE '%".$searchVal."%' OR user.display_name LIKE '%".$searchVal."%' OR user.email LIKE '%".$searchVal."%'  OR social.usl_fameuz LIKE '%".$searchVal."%') order by user.first_name asc";
	$getUser				   =	$objUser->listQuery($sqlUserSearch);
	?>
	<div class="auto_suggest">
	<ul style="display:block;">
		<?php
		if(count($getUser) >0){
			foreach($getUser as $allUser){
				$allUserImg		=	SITE_ROOT.'uploads/profile_images/'.(($allUser['upi_img_url'])?$allUser['upi_img_url']:"profile_pic.jpg");
				$allUserName	   =	$objCommon->displayName($allUser);
			?>
			<li>
				<div class="img-section">
					<a href="<?php echo SITE_ROOT.'user/'.$objCommon->html2text($allUser['usl_fameuz']).'/message'?>"><img class="img-responsive" src="<?php echo $allUserImg?>"></a>
				</div>
				<div class="user-info-sec">
					<h3><a href="<?php echo SITE_ROOT.'user/'.$objCommon->html2text($allUser['usl_fameuz']).'/message'?>"><?php echo $allUserName?></a></h3>
					<p><a href="<?php echo SITE_ROOT.'user/'.$objCommon->html2text($allUser['usl_fameuz']).'/message'?>"><?php echo $objCommon->html2text($allUser['c_name']);?></a></p>
				</div>
			</li>
			<?php 
			}
		}else{
			?>
			<li><p>No users found...</p></li>
			<?php
		}
		?>
		
	</ul>
	</div>
<?php
}
?>