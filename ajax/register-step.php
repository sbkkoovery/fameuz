<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/users.php");
include_once(DIR_ROOT."class/personal_details.php");
include_once(DIR_ROOT."class/user_privacy.php");
include_once(DIR_ROOT."class/user_categories.php");
include_once(DIR_ROOT."class/model_details.php");
include_once(DIR_ROOT."class/user_profile_complete.php");
$objProfComplete		  		=	new user_profile_complete();
$objCommon					  =	new common();
$objUsers					   =	new users();
$objPersonal					=	new personal_details();
$objUserPrivacy				 =	new user_privacy();
$objUserCategories			  =	new user_categories();
$objModelDetails				=	new model_details();
$userId						 =	$_SESSION['userId'];
if($userId){
	$getUserCat				 =	$objUserCategories->getRowSql("SELECT ucat.uc_m_id,cate.c_model_details_yes FROM user_categories AS ucat LEFT JOIN category AS cate ON ucat.uc_c_id = cate.c_id WHERE ucat.user_id=".$userId);
	$userMainCat				=	$getUserCat['uc_m_id'];
}else{
	header("location:".SITE_ROOT);
}
if(isset($_POST['primary_details'])){
	$_POST['p_dob']			 =	$objCommon->esc($_POST['dobY'])."-".$objCommon->esc($_POST['dobM'])."-".$objCommon->esc($_POST['dobD']);
	$per_phone_privacy		  =	array(($_POST['per_phone_privacy1']==1)?1:0,($_POST['per_phone_privacy2']==1)?1:0,($_POST['per_phone_privacy3']==1)?1:0,($_POST['per_phone_privacy4']==1)?1:0);
	$_POST['uc_p_phone']		=	implode(",",$per_phone_privacy);
	$per_dob_privacy		  	=	array(($_POST['per_dob_privacy1']==1)?1:0,($_POST['per_dob_privacy2']==1)?1:0,($_POST['per_dob_privacy3']==1)?1:0,($_POST['per_dob_privacy4']==1)?1:0);
	$_POST['uc_p_dob']		  =	implode(",",$per_dob_privacy);
	$_POST['n_id']			  =	$objCommon->esc($_POST['nationality']);
	$_POST['p_city']			=	$objCommon->esc($_POST['city']);
	$_POST['p_country']		 =	$objCommon->esc($_POST['country']);
	$objUsers->update($_POST,"user_id=$userId");
	$objPersonal->update($_POST,"user_id=$userId");
	$objUserPrivacy->update($_POST,"user_id=".$userId);
	if($userMainCat==2 && $getUserCat['c_model_details_yes']==1){
		$_SESSION['step']	  =	2;
	}else{
		$_SESSION['step']	  =	3;
	}
	//header("location:".SITE_ROOT.'user/register-page');
	//exit;
}
if(isset($_POST['personal_details'])){
	$_POST['me_id']			   =	$objCommon->esc($_POST['pub_model_eyes']);
	$_POST['mhair_id']	  		=	$objCommon->esc($_POST['pub_model_hair']);
  	$_POST['mh_id']			   =	$objCommon->esc($_POST['pub_model_height']);
	$_POST['mw_id']			   =	$objCommon->esc($_POST['pub_model_waist']);
	$_POST['mhp_id']			  =	$objCommon->esc($_POST['pub_model_hips']);
	$_POST['mc_id']			   =	$objCommon->esc($_POST['pub_model_chest']);
	$_POST['ms_id']			   =	$objCommon->esc($_POST['pub_model_shoe']);
	$_POST['mdress_id']		   =	$objCommon->esc($_POST['pub_model_dress']);
	$_POST['mj_id']			   =	$objCommon->esc($_POST['pub_model_jacket']);
	$_POST['mt_id']			   =	$objCommon->esc($_POST['pub_model_trousers']);
	$_POST['mcollar_id']		  =	$objCommon->esc($_POST['pub_model_collar']);
	$objModelDetails->update($_POST,"user_id=".$_SESSION['userId']);
	$_SESSION['step']	  =	3;
	//header("location:".SITE_ROOT.'user/register-page');
	//exit;
}
//---------------------------------profile complete status----------------------------------------------------------------------------------
$getUserComplete					=	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,personal.p_dob,personal.p_gender,personal.p_phone,personal.p_alt_email,personal.p_about, personal.p_ethnicity,personal.p_languages,personal.n_id,personal.p_country,personal.p_city,profileImg.upi_img_url,social.usl_fb,social.usl_twitter,social.usl_gplus,social.usl_instagram,social.usl_others,ucat.uc_m_id,det.mw_id,det.mt_id,det.ms_id,det.mj_id,det.model_dis_id,det.mhp_id,det.mh_id,det.me_id,det.mcollar_id,det.mc_id,det.mhair_id,album.ai_images,photos.photo_url
		FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
		LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id 
		LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id  
		LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
		LEFT JOIN personal_details as personal ON user.user_id = personal.user_id 
		LEFT JOIN model_details  AS det ON user.user_id = det.user_id
		LEFT JOIN album_images as album ON user.user_id = album.user_id AND album.ai_set_main =1
		LEFT JOIN  user_photos as photos ON user.user_id = photos.user_id AND photos.photo_set_main=1
		WHERE user.status=1 AND user.user_id=".$_SESSION['userId']);
$userMainCat						=	$getUserComplete['uc_m_id'];
$com_first_name					 =	($getUserComplete['first_name'] !='')?1:0;
$com_last_name					  =	($getUserComplete['last_name'] !='')?1:0;
$com_display_name				   =	($getUserComplete['display_name'] !='')?1:0;
if($userMainCat==2){
	$_POST['complete_primary']	  =	($com_first_name * 4)+($com_last_name * 4)+($com_display_name * 4);
}else if($userMainCat==1){
	$_POST['complete_primary']	  =	($com_first_name * 8)+($com_last_name * 8)+($com_display_name * 8);
}
$com_dob					 		=	($getUserComplete['p_dob'] !='0000-00-00' && $getUserComplete['p_dob'] !='' && $getUserComplete['p_dob'] !='NULL')?1:0;
$com_gender					 	 =	($getUserComplete['p_gender'] !='0' && $getUserComplete['p_gender'] !='' && $getUserComplete['p_gender'] !='NULL')?1:0;
$com_phone					 	  =	($getUserComplete['p_phone'] !='' && $getUserComplete['p_phone'] !='NULL')?1:0;
$com_alt_email					  =	($getUserComplete['p_alt_email'] !='' && $getUserComplete['p_alt_email'] !='NULL')?1:0;
$com_about					  	  =	($getUserComplete['p_about'] !='' && $getUserComplete['p_about'] !='NULL')?1:0;
$com_ethnicity					  =	($getUserComplete['p_ethnicity'] !='' && $getUserComplete['p_ethnicity'] !='NULL')?1:0;
$com_languages					  =	($getUserComplete['p_languages'] !='' && $getUserComplete['p_languages'] !='NULL')?1:0;
$com_nationality					=	($getUserComplete['n_id'] !='0' && $getUserComplete['n_id'] !='' && $getUserComplete['n_id'] !='NULL')?1:0;
$com_country					  	=	($getUserComplete['p_country'] !='' && $getUserComplete['p_country'] !='NULL')?1:0;
$com_city					  	   =	($getUserComplete['p_city'] !='' && $getUserComplete['p_city'] !='NULL')?1:0;
if($userMainCat==2){
	$_POST['complete_personal']	 =	($com_dob * 4)+($com_gender * 4)+($com_phone * 4)+($com_alt_email * 4)+($com_about * 4)+($com_ethnicity * 4)+($com_languages * 4)+($com_nationality * 4)+($com_country * 4)+($com_city * 4);
}else if($userMainCat==1){
	$_POST['complete_personal']	 =	($com_dob * 8)+($com_gender * 8)+($com_phone * 4)+($com_alt_email * 4)+($com_about * 4)+($com_ethnicity * 4)+($com_languages * 4)+($com_nationality * 8)+($com_country * 8)+($com_city * 8);
}
$com_profile_img					=	($getUserComplete['upi_img_url'] !='' && $getUserComplete['upi_img_url'] !='NULL')?1:0;
$com_cover_img1					 =	($getUserComplete['ai_images'] !='' && $getUserComplete['ai_images'] !='NULL')?1:0;
$com_cover_img2					 =	($getUserComplete['photo_url'] !='' && $getUserComplete['photo_url'] !='NULL')?1:0;
if($userMainCat==2){
	$_POST['complete_profile_img']  =	($com_profile_img * 4)+($com_cover_img1 * 4)+($com_cover_img2 * 4);
}else if($userMainCat==1){
	$_POST['complete_profile_img']  =	($com_profile_img * 4)+($com_cover_img1 * 4)+($com_cover_img2 * 4);
}
$com_social1					 	=	($getUserComplete['usl_fb'] !='' && $getUserComplete['usl_fb'] !='NULL')?1:0;
$com_social2					 	=	($getUserComplete['usl_twitter'] !='' && $getUserComplete['usl_twitter'] !='NULL')?1:0;
$com_social3					 	=	($getUserComplete['usl_gplus'] !='' && $getUserComplete['usl_gplus'] !='NULL')?1:0;
$com_social4					 	=	($getUserComplete['usl_instagram'] !='' && $getUserComplete['usl_instagram'] !='NULL')?1:0;
$com_social5					 	=	($getUserComplete['usl_others'] !='' && $getUserComplete['usl_others'] !='NULL')?1:0;
$com_social						 =	$com_social1+$com_social2+$com_social3+$com_social4+$com_social5;
if($com_social >=4){
	$_POST['complete_social']  	   =	8;
}else{
	$_POST['complete_social']  	   =	$com_social*2;
}
$com_det1						   =	($getUserComplete['mw_id'] !='0' && $getUserComplete['mw_id'] !='' && $getUserComplete['mw_id'] !='NULL')?1:0;
$com_det2						   =	($getUserComplete['mt_id'] !='0' && $getUserComplete['mt_id'] !='' && $getUserComplete['mt_id'] !='NULL')?1:0;
$com_det3						   =	($getUserComplete['ms_id'] !='0' && $getUserComplete['ms_id'] !='' && $getUserComplete['ms_id'] !='NULL')?1:0;
$com_det4						   =	($getUserComplete['mj_id'] !='0' && $getUserComplete['mj_id'] !='' && $getUserComplete['mj_id'] !='NULL')?1:0;
$com_det5						   =	($getUserComplete['model_dis_id'] !='0' && $getUserComplete['model_dis_id'] !='' && $getUserComplete['model_dis_id'] !='NULL')?1:0;
$com_det6						   =	($getUserComplete['mhp_id'] !='0' && $getUserComplete['mhp_id'] !='' && $getUserComplete['mhp_id'] !='NULL')?1:0;
$com_det7						   =	($getUserComplete['mh_id'] !='0' && $getUserComplete['mh_id'] !='' && $getUserComplete['mh_id'] !='NULL')?1:0;
$com_det8						   =	($getUserComplete['me_id'] !='0' && $getUserComplete['me_id'] !='' && $getUserComplete['me_id'] !='NULL')?1:0;
$com_det9						   =	($getUserComplete['mcollar_id'] !='0' && $getUserComplete['mcollar_id'] !='' && $getUserComplete['mcollar_id'] !='NULL')?1:0;
$com_det10						  =	($getUserComplete['mc_id'] !='0' && $getUserComplete['mc_id'] !='' && $getUserComplete['mc_id'] !='NULL')?1:0;
$com_det11						  =	($getUserComplete['mhair_id'] !='0' && $getUserComplete['mhair_id'] !='' && $getUserComplete['mhair_id'] !='NULL')?1:0;
if($userMainCat==2){
	$com_det						=	$com_det1+$com_det2+$com_det3+$com_det4+$com_det5+$com_det6+$com_det7+$com_det8+$com_det9+$com_det10+$com_det11;
	if($com_det >=8){
		$_POST['complete_public']   =	32;
	}else{
		$_POST['complete_public']   =	$com_det*2;
	}
}
$_POST['complete_created']		  =	date("Y-m-d H:i:s");
$countCompeteProf				   =	$objProfComplete->count("user_id=".$_SESSION['userId']);
if($countCompeteProf >0){
	$objProfComplete->update($_POST,"user_id=".$_SESSION['userId']);
}else{
	$_POST['user_id']			   =	$_SESSION['userId'];
	$objProfComplete->insert($_POST);
}
//-------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------profile complete status----------------------------------------------------------------------------------
$getUserComplete					=	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,personal.p_dob,personal.p_gender,personal.p_phone,personal.p_alt_email,personal.p_about, personal.p_ethnicity,personal.p_languages,personal.n_id,personal.p_country,personal.p_city,profileImg.upi_img_url,social.usl_fb,social.usl_twitter,social.usl_gplus,social.usl_instagram,social.usl_others,ucat.uc_m_id,det.mw_id,det.mt_id,det.ms_id,det.mj_id,det.model_dis_id,det.mhp_id,det.mh_id,det.me_id,det.mcollar_id,det.mc_id,det.mhair_id,album.ai_images,photos.photo_url
		FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
		LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id 
		LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id  
		LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
		LEFT JOIN personal_details as personal ON user.user_id = personal.user_id 
		LEFT JOIN model_details  AS det ON user.user_id = det.user_id
		LEFT JOIN album_images as album ON user.user_id = album.user_id AND album.ai_set_main =1
		LEFT JOIN  user_photos as photos ON user.user_id = photos.user_id AND photos.photo_set_main=1
		WHERE user.status=1 AND user.user_id=".$_SESSION['userId']);
$userMainCat						=	$getUserComplete['uc_m_id'];
$com_first_name					 =	($getUserComplete['first_name'] !='')?1:0;
$com_last_name					  =	($getUserComplete['last_name'] !='')?1:0;
$com_display_name				   =	($getUserComplete['display_name'] !='')?1:0;
if($userMainCat==2){
	$_POST['complete_primary']	  =	($com_first_name * 4)+($com_last_name * 4)+($com_display_name * 4);
}else if($userMainCat==1){
	$_POST['complete_primary']	  =	($com_first_name * 8)+($com_last_name * 8)+($com_display_name * 8);
}
$com_dob					 		=	($getUserComplete['p_dob'] !='0000-00-00' && $getUserComplete['p_dob'] !='' && $getUserComplete['p_dob'] !='NULL')?1:0;
$com_gender					 	 =	($getUserComplete['p_gender'] !='0' && $getUserComplete['p_gender'] !='' && $getUserComplete['p_gender'] !='NULL')?1:0;
$com_phone					 	  =	($getUserComplete['p_phone'] !='' && $getUserComplete['p_phone'] !='NULL')?1:0;
$com_alt_email					  =	($getUserComplete['p_alt_email'] !='' && $getUserComplete['p_alt_email'] !='NULL')?1:0;
$com_about					  	  =	($getUserComplete['p_about'] !='' && $getUserComplete['p_about'] !='NULL')?1:0;
$com_ethnicity					  =	($getUserComplete['p_ethnicity'] !='' && $getUserComplete['p_ethnicity'] !='NULL')?1:0;
$com_languages					  =	($getUserComplete['p_languages'] !='' && $getUserComplete['p_languages'] !='NULL')?1:0;
$com_nationality					=	($getUserComplete['n_id'] !='0' && $getUserComplete['n_id'] !='' && $getUserComplete['n_id'] !='NULL')?1:0;
$com_country					  	=	($getUserComplete['p_country'] !='' && $getUserComplete['p_country'] !='NULL')?1:0;
$com_city					  	   =	($getUserComplete['p_city'] !='' && $getUserComplete['p_city'] !='NULL')?1:0;
if($userMainCat==2){
	$_POST['complete_personal']	 =	($com_dob * 4)+($com_gender * 4)+($com_phone * 4)+($com_alt_email * 4)+($com_about * 4)+($com_ethnicity * 4)+($com_languages * 4)+($com_nationality * 4)+($com_country * 4)+($com_city * 4);
}else if($userMainCat==1){
	$_POST['complete_personal']	 =	($com_dob * 8)+($com_gender * 8)+($com_phone * 4)+($com_alt_email * 4)+($com_about * 4)+($com_ethnicity * 4)+($com_languages * 4)+($com_nationality * 8)+($com_country * 8)+($com_city * 8);
}
$com_profile_img					=	($getUserComplete['upi_img_url'] !='' && $getUserComplete['upi_img_url'] !='NULL')?1:0;
$com_cover_img1					 =	($getUserComplete['ai_images'] !='' && $getUserComplete['ai_images'] !='NULL')?1:0;
$com_cover_img2					 =	($getUserComplete['photo_url'] !='' && $getUserComplete['photo_url'] !='NULL')?1:0;
if($userMainCat==2){
	$_POST['complete_profile_img']  =	($com_profile_img * 4)+($com_cover_img1 * 4)+($com_cover_img2 * 4);
}else if($userMainCat==1){
	$_POST['complete_profile_img']  =	($com_profile_img * 4)+($com_cover_img1 * 4)+($com_cover_img2 * 4);
}
$com_social1					 	=	($getUserComplete['usl_fb'] !='' && $getUserComplete['usl_fb'] !='NULL')?1:0;
$com_social2					 	=	($getUserComplete['usl_twitter'] !='' && $getUserComplete['usl_twitter'] !='NULL')?1:0;
$com_social3					 	=	($getUserComplete['usl_gplus'] !='' && $getUserComplete['usl_gplus'] !='NULL')?1:0;
$com_social4					 	=	($getUserComplete['usl_instagram'] !='' && $getUserComplete['usl_instagram'] !='NULL')?1:0;
$com_social5					 	=	($getUserComplete['usl_others'] !='' && $getUserComplete['usl_others'] !='NULL')?1:0;
$com_social						 =	$com_social1+$com_social2+$com_social3+$com_social4+$com_social5;
if($com_social >=4){
	$_POST['complete_social']  	   =	8;
}else{
	$_POST['complete_social']  	   =	$com_social*2;
}
$com_det1						   =	($getUserComplete['mw_id'] !='0' && $getUserComplete['mw_id'] !='' && $getUserComplete['mw_id'] !='NULL')?1:0;
$com_det2						   =	($getUserComplete['mt_id'] !='0' && $getUserComplete['mt_id'] !='' && $getUserComplete['mt_id'] !='NULL')?1:0;
$com_det3						   =	($getUserComplete['ms_id'] !='0' && $getUserComplete['ms_id'] !='' && $getUserComplete['ms_id'] !='NULL')?1:0;
$com_det4						   =	($getUserComplete['mj_id'] !='0' && $getUserComplete['mj_id'] !='' && $getUserComplete['mj_id'] !='NULL')?1:0;
$com_det5						   =	($getUserComplete['model_dis_id'] !='0' && $getUserComplete['model_dis_id'] !='' && $getUserComplete['model_dis_id'] !='NULL')?1:0;
$com_det6						   =	($getUserComplete['mhp_id'] !='0' && $getUserComplete['mhp_id'] !='' && $getUserComplete['mhp_id'] !='NULL')?1:0;
$com_det7						   =	($getUserComplete['mh_id'] !='0' && $getUserComplete['mh_id'] !='' && $getUserComplete['mh_id'] !='NULL')?1:0;
$com_det8						   =	($getUserComplete['me_id'] !='0' && $getUserComplete['me_id'] !='' && $getUserComplete['me_id'] !='NULL')?1:0;
$com_det9						   =	($getUserComplete['mcollar_id'] !='0' && $getUserComplete['mcollar_id'] !='' && $getUserComplete['mcollar_id'] !='NULL')?1:0;
$com_det10						  =	($getUserComplete['mc_id'] !='0' && $getUserComplete['mc_id'] !='' && $getUserComplete['mc_id'] !='NULL')?1:0;
$com_det11						  =	($getUserComplete['mhair_id'] !='0' && $getUserComplete['mhair_id'] !='' && $getUserComplete['mhair_id'] !='NULL')?1:0;
if($userMainCat==2){
	$com_det						=	$com_det1+$com_det2+$com_det3+$com_det4+$com_det5+$com_det6+$com_det7+$com_det8+$com_det9+$com_det10+$com_det11;
	if($com_det >=8){
		$_POST['complete_public']   =	32;
	}else{
		$_POST['complete_public']   =	$com_det*2;
	}
}
$_POST['complete_created']		  =	date("Y-m-d H:i:s");
$countCompeteProf				   =	$objProfComplete->count("user_id=".$_SESSION['userId']);
if($countCompeteProf >0){
	$objProfComplete->update($_POST,"user_id=".$_SESSION['userId']);
}else{
	$_POST['user_id']			   =	$_SESSION['userId'];
	$objProfComplete->insert($_POST);
}
//-------------------------------------------------------------------------------------------------------------------------------------

header("location:".SITE_ROOT.'user/register-page');
exit;
?>