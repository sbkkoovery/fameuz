<?php
session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/user_profile_image.php");
include(DIR_ROOT."ajax/resize-class.php");
include_once(DIR_ROOT."class/user_profile_complete.php");
$objCommon				=	new common();
$objUserProfileImg		=	new user_profile_image();
$objProfComplete		  =	new user_profile_complete();
$userId				   =	$_SESSION['userId'];
if(isset($_GET['src'],$userId)&& $_GET['src']!="" && $userId != ""){
	$imageName		=	$_GET['src'];
	$x			    =	$objCommon->esc($_GET['x']);
	$y			    =	$objCommon->esc($_GET['y']);
	$w			    =	$objCommon->esc($_GET['w']);
	$h			    =	$objCommon->esc($_GET['h']);
	$resWidth		 =	$objCommon->esc($_GET['resWidth']);
	$resHeight	    =	$objCommon->esc($_GET['resHeight']);
	$targ_w 	   	   = 	230;
	$targ_h 	   	   = 	312;
	$jpeg_quality 	 = 	90;
	$imageNameExpl	=	explode('?',$imageName);
	$imageName		=	$imageNameExpl[0];
	$imgExp		   =	explode("/",$imageName);
	$lastArr		  =	array_slice($imgExp, -3, 3, true);
	$lastArr 		  = 	array_values($lastArr);
	$src			  =	DIR_ROOT."uploads/profile_images/".$lastArr[0]."/".$lastArr[1]."/".$lastArr[2];
	$newSrc		   =	DIR_ROOT."uploads/profile_images/".$lastArr[0]."/".$lastArr[2];
	$ext 			  = 	pathinfo($lastArr[2], PATHINFO_EXTENSION);
	$widRat		   =	1;
	$heiRat		   =	1;
	list($current_width, $current_height) = getimagesize($src);
	if($current_width >$resWidth){
		$widRat	   =	$current_width/$resWidth;
	}
	if($current_height >$resHeight){
		$heiRat	   =	$current_height/$resHeight;
	}
	switch($ext){
		case "jpg": $im=imagecreatefromjpeg($src);break;
		case "gif":$im=imagecreatefromgif($src); break;
		case "png": $im=imagecreatefrompng($src); break;
	}
	$crop_width = $w*$widRat;
	$crop_height = $h*$heiRat;
	$canvas = imagecreatetruecolor($crop_width, $crop_height);
	$current_image = $im;
	imagecopy($canvas, $current_image, 0, 0, $x, $y, $current_width, $current_height);
	switch($ext)
	{
		case 'jpg':
		case 'jpeg':
			@imagejpeg($canvas, $newSrc, 100);
			break;
		case 'gif':
			@imagegif($canvas, $newSrc, 100);
			break;
		case 'png':
			@imagepng($canvas, $newSrc, 9);
			break;
		default:
			false;
			break;
	}
	$resizeObj = new resize($newSrc);
	$resizeObj -> resizeImage($targ_w, $targ_h, 'exact');
	$resizeObj -> saveImage($newSrc, 100);
	//----------------start upload to AWS----------------------------------------
	require(DIR_ROOT."amazone_s3/S3.php");
	$s3			=	new S3(AWS_ID,AWS_KEY);
	S3::putObjectFile($newSrc,AWS_IMG_BUCKET,'uploads/profile_images/'.$lastArr[0]."/".$lastArr[2],S3::ACL_PUBLIC_READ,array());
	//----------------end upload to AWS-------------------------------------------
	if(file_exists($newSrc)){
		$objUserProfileImg->updateField(array("upi_status"=>0),"user_id=".$userId);
		$_POST['upi_created']				=	date("Y-m-d H:i:s");
		$_POST['upi_edited']				 =	date("Y-m-d H:i:s");
		$_POST['user_id']					=	$userId;
		$_POST['upi_img_url']				=	$lastArr[0]."/".$lastArr[2];
		$_POST['upi_status']				 =	1;
		$objUserProfileImg->insert($_POST);
		echo $lastArr[0]."/".$lastArr[2];
		//---------------------------------profile complete status----------------------------------------------------------------------------------
$getUserComplete					=	$objProfComplete->getRowSql("SELECT user.first_name,user.last_name,user.display_name,personal.p_dob,personal.p_gender,personal.p_phone,personal.p_alt_email,personal.p_about, personal.p_ethnicity,personal.p_languages,personal.n_id,personal.p_country,personal.p_city,profileImg.upi_img_url,social.usl_fb,social.usl_twitter,social.usl_gplus,social.usl_instagram,social.usl_others,ucat.uc_m_id,det.mw_id,det.mt_id,det.ms_id,det.mj_id,det.model_dis_id,det.mhp_id,det.mh_id,det.me_id,det.mcollar_id,det.mc_id,det.mhair_id,album.ai_images,photos.photo_url
		FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
		LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id 
		LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id  
		LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
		LEFT JOIN personal_details as personal ON user.user_id = personal.user_id 
		LEFT JOIN model_details  AS det ON user.user_id = det.user_id
		LEFT JOIN album_images as album ON user.user_id = album.user_id AND album.ai_set_main =1
		LEFT JOIN  user_photos as photos ON user.user_id = photos.user_id AND photos.photo_set_main=1
		WHERE user.status=1 AND user.user_id=".$_SESSION['userId']);
$userMainCat						=	$getUserComplete['uc_m_id'];
$com_first_name					 =	($getUserComplete['first_name'] !='')?1:0;
$com_last_name					  =	($getUserComplete['last_name'] !='')?1:0;
$com_display_name				   =	($getUserComplete['display_name'] !='')?1:0;
if($userMainCat==2){
	$_POST['complete_primary']	  =	($com_first_name * 4)+($com_last_name * 4)+($com_display_name * 4);
}else if($userMainCat==1){
	$_POST['complete_primary']	  =	($com_first_name * 8)+($com_last_name * 8)+($com_display_name * 8);
}
$com_dob					 		=	($getUserComplete['p_dob'] !='0000-00-00' && $getUserComplete['p_dob'] !='' && $getUserComplete['p_dob'] !='NULL')?1:0;
$com_gender					 	 =	($getUserComplete['p_gender'] !='0' && $getUserComplete['p_gender'] !='' && $getUserComplete['p_gender'] !='NULL')?1:0;
$com_phone					 	  =	($getUserComplete['p_phone'] !='' && $getUserComplete['p_phone'] !='NULL')?1:0;
$com_alt_email					  =	($getUserComplete['p_alt_email'] !='' && $getUserComplete['p_alt_email'] !='NULL')?1:0;
$com_about					  	  =	($getUserComplete['p_about'] !='' && $getUserComplete['p_about'] !='NULL')?1:0;
$com_ethnicity					  =	($getUserComplete['p_ethnicity'] !='' && $getUserComplete['p_ethnicity'] !='NULL')?1:0;
$com_languages					  =	($getUserComplete['p_languages'] !='' && $getUserComplete['p_languages'] !='NULL')?1:0;
$com_nationality					=	($getUserComplete['n_id'] !='0' && $getUserComplete['n_id'] !='' && $getUserComplete['n_id'] !='NULL')?1:0;
$com_country					  	=	($getUserComplete['p_country'] !='' && $getUserComplete['p_country'] !='NULL')?1:0;
$com_city					  	   =	($getUserComplete['p_city'] !='' && $getUserComplete['p_city'] !='NULL')?1:0;
if($userMainCat==2){
	$_POST['complete_personal']	 =	($com_dob * 4)+($com_gender * 4)+($com_phone * 4)+($com_alt_email * 4)+($com_about * 4)+($com_ethnicity * 4)+($com_languages * 4)+($com_nationality * 4)+($com_country * 4)+($com_city * 4);
}else if($userMainCat==1){
	$_POST['complete_personal']	 =	($com_dob * 8)+($com_gender * 8)+($com_phone * 4)+($com_alt_email * 4)+($com_about * 4)+($com_ethnicity * 4)+($com_languages * 4)+($com_nationality * 8)+($com_country * 8)+($com_city * 8);
}
$com_profile_img					=	($getUserComplete['upi_img_url'] !='' && $getUserComplete['upi_img_url'] !='NULL')?1:0;
$com_cover_img1					 =	($getUserComplete['ai_images'] !='' && $getUserComplete['ai_images'] !='NULL')?1:0;
$com_cover_img2					 =	($getUserComplete['photo_url'] !='' && $getUserComplete['photo_url'] !='NULL')?1:0;
if($userMainCat==2){
	$_POST['complete_profile_img']  =	($com_profile_img * 4)+($com_cover_img1 * 4)+($com_cover_img2 * 4);
}else if($userMainCat==1){
	$_POST['complete_profile_img']  =	($com_profile_img * 4)+($com_cover_img1 * 4)+($com_cover_img2 * 4);
}
$com_social1					 	=	($getUserComplete['usl_fb'] !='' && $getUserComplete['usl_fb'] !='NULL')?1:0;
$com_social2					 	=	($getUserComplete['usl_twitter'] !='' && $getUserComplete['usl_twitter'] !='NULL')?1:0;
$com_social3					 	=	($getUserComplete['usl_gplus'] !='' && $getUserComplete['usl_gplus'] !='NULL')?1:0;
$com_social4					 	=	($getUserComplete['usl_instagram'] !='' && $getUserComplete['usl_instagram'] !='NULL')?1:0;
$com_social5					 	=	($getUserComplete['usl_others'] !='' && $getUserComplete['usl_others'] !='NULL')?1:0;
$com_social						 =	$com_social1+$com_social2+$com_social3+$com_social4+$com_social5;
if($com_social >=4){
	$_POST['complete_social']  	   =	8;
}else{
	$_POST['complete_social']  	   =	$com_social*2;
}
$com_det1						   =	($getUserComplete['mw_id'] !='0' && $getUserComplete['mw_id'] !='' && $getUserComplete['mw_id'] !='NULL')?1:0;
$com_det2						   =	($getUserComplete['mt_id'] !='0' && $getUserComplete['mt_id'] !='' && $getUserComplete['mt_id'] !='NULL')?1:0;
$com_det3						   =	($getUserComplete['ms_id'] !='0' && $getUserComplete['ms_id'] !='' && $getUserComplete['ms_id'] !='NULL')?1:0;
$com_det4						   =	($getUserComplete['mj_id'] !='0' && $getUserComplete['mj_id'] !='' && $getUserComplete['mj_id'] !='NULL')?1:0;
$com_det5						   =	($getUserComplete['model_dis_id'] !='0' && $getUserComplete['model_dis_id'] !='' && $getUserComplete['model_dis_id'] !='NULL')?1:0;
$com_det6						   =	($getUserComplete['mhp_id'] !='0' && $getUserComplete['mhp_id'] !='' && $getUserComplete['mhp_id'] !='NULL')?1:0;
$com_det7						   =	($getUserComplete['mh_id'] !='0' && $getUserComplete['mh_id'] !='' && $getUserComplete['mh_id'] !='NULL')?1:0;
$com_det8						   =	($getUserComplete['me_id'] !='0' && $getUserComplete['me_id'] !='' && $getUserComplete['me_id'] !='NULL')?1:0;
$com_det9						   =	($getUserComplete['mcollar_id'] !='0' && $getUserComplete['mcollar_id'] !='' && $getUserComplete['mcollar_id'] !='NULL')?1:0;
$com_det10						  =	($getUserComplete['mc_id'] !='0' && $getUserComplete['mc_id'] !='' && $getUserComplete['mc_id'] !='NULL')?1:0;
$com_det11						  =	($getUserComplete['mhair_id'] !='0' && $getUserComplete['mhair_id'] !='' && $getUserComplete['mhair_id'] !='NULL')?1:0;
if($userMainCat==2){
	$com_det						=	$com_det1+$com_det2+$com_det3+$com_det4+$com_det5+$com_det6+$com_det7+$com_det8+$com_det9+$com_det10+$com_det11;
	if($com_det >=8){
		$_POST['complete_public']   =	32;
	}else{
		$_POST['complete_public']   =	$com_det*2;
	}
}
$_POST['complete_created']		  =	date("Y-m-d H:i:s");
$countCompeteProf				   =	$objProfComplete->count("user_id=".$_SESSION['userId']);
if($countCompeteProf >0){
	$objProfComplete->update($_POST,"user_id=".$_SESSION['userId']);
}else{
	$_POST['user_id']			   =	$_SESSION['userId'];
	$objProfComplete->insert($_POST);
}
//-------------------------------------------------------------------------------------------------------------------------------------

	}
	exit;
}
?>