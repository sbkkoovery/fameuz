<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/users.php");
	include_once(DIR_ROOT."class/search_functions.php");
	$objUsers					=	new users();
	$objCommon				   =	new common();
	$obj_search_functions		=	new search_functions();
	$perPage 				   	 = 	20;
	$page 					  	= 	1;
	if(!empty($_GET["page"])) {
		$page 					=	$_GET["page"];
	}
	$type			  =	'all';
	$searchQuery		=	'';
	if(isset($_GET['filter_keywords'])&&$_GET['filter_keywords']!=''){
		$keyWord			  =	$objCommon->esc($_GET['filter_keywords']);
		$searchQuery		  =	" AND (u.first_name LIKE '%$keyWord%' OR u.last_name LIKE '%$keyWord%' OR u.display_name LIKE '%$keyWord%' OR a.ai_caption LIKE '%$keyWord%')";
	}
	if(isset($_GET['filter_category'])&&$_GET['filter_category']!=''){
		$type			  =	$objCommon->esc($_GET['filter_category']);
	}
if($type=='new-photos'){
	$sqlImagesList	=	"SELECT * FROM(SELECT a.ai_id,a.ai_images,a.user_id,a.ai_like_count,a.ai_comment_count,a.ai_caption,u.display_name,u.first_name,u.email,a.ai_created FROM album_images AS a LEFT JOIN users AS u ON a.user_id=u.user_id".$searchQuery." WHERE u.email_validation=1 AND u.status=1 ORDER BY a.ai_created DESC) AS t GROUP BY user_id ORDER BY ai_created DESC";
}else if($type=='popular'){
	$sqlImagesList	=	"SELECT a.ai_id,a.ai_images,a.user_id,a.ai_like_count,a.ai_comment_count,a.ai_caption,u.display_name,u.first_name,u.email,a.ai_created FROM album_images AS a LEFT JOIN users AS u ON a.user_id=u.user_id".$searchQuery." WHERE u.email_validation=1 AND u.status=1 GROUP BY a.ai_id ORDER BY a.ai_like_count DESC";
}else if($type=='top'){
	$sqlImagesList	=	"SELECT a.ai_id,a.ai_images,a.user_id,a.ai_like_count,a.ai_comment_count,a.ai_caption,u.display_name,u.first_name,u.email,a.ai_created,AVG(vote.aiv_vote) AS voteRate FROM album_images_vote AS vote LEFT JOIN album_images AS a ON a.ai_id=vote.ai_id LEFT JOIN users AS u ON a.user_id=u.user_id".$searchQuery." WHERE u.email_validation=1 AND u.status=1 GROUP BY a.ai_id ORDER BY voteRate DESC";
}else if($type=='last-visited'&&isset($_SESSION['userId'])){
	$sqlImagesList	=	"SELECT a.ai_id,a.ai_images,a.user_id,a.ai_like_count,a.ai_comment_count,a.ai_caption,u.display_name,u.first_name,u.email,a.ai_created FROM images_view AS iv LEFT JOIN album_images AS a ON iv.iv_content=a.ai_id LEFT JOIN users AS u ON a.user_id=u.user_id".$searchQuery." WHERE u.email_validation=1 AND u.status=1 AND iv.iv_cat=2 AND iv.iv_user_id=".$_SESSION['userId']." ORDER BY iv.iv_created DESC";
}else if($type=='all'){
	$sqlImagesList	=	"SELECT * FROM(SELECT a.ai_id,a.ai_images,a.user_id,a.ai_like_count,a.ai_comment_count,a.ai_caption,u.display_name,u.first_name,u.email,a.ai_created FROM album_images AS a LEFT JOIN users AS u ON a.user_id=u.user_id".$searchQuery." WHERE u.email_validation=1 AND u.status=1 ORDER BY a.ai_created DESC) AS t ORDER BY ai_created DESC";
}
if(isset($_GET['searchKey'])&&$_GET['searchKey']!=""){
	$searchKey		=	$objCommon->esc($_GET['searchKey']);
	$sqlImagesList	=	"SELECT * FROM(SELECT a.ai_id,a.ai_images,a.user_id,a.ai_like_count,a.ai_comment_count,a.ai_caption,u.display_name,u.first_name,u.email,a.ai_created FROM album_images AS a LEFT JOIN users AS u ON a.user_id=u.user_id WHERE u.email_validation=1 AND u.status=1 AND (u.first_name LIKE '%$searchKey%' OR u.last_name LIKE '%$searchKey%' OR u.display_name LIKE '%$searchKey%' OR a.ai_caption LIKE '%$searchKey%') ORDER BY a.ai_created DESC) AS t ORDER BY ai_created DESC";
}
/*$imageCount	=	$objAlbumImages->countRows("SELECT ai_id FROM album_images WHERE 1");
$start 					 =	($page * $limit) - $limit;
if(($objAlbums->countRows($sqlImagesList)) > ($page * $limit) ){
	$next = ++$page;
}
$newImagesList	=	$objAlbums->listQuery($sqlImagesList. " LIMIT ".$start.", ".$limit);*/


	
$start 						   =	($page-1)*$perPage;
if($start < 0) { $start = 0; }
	$queryMemberSearch 		   =	$sqlImagesList . " limit " . $start . "," . $perPage; 
if(empty($_GET["rowcount"])) {
	$_GET["rowcount"] 		    =	$countSearchMembers	=	$objUsers->countRows($sqlImagesList);
}
$pages  = ceil($_GET["rowcount"]/$perPage);
$getMemberSearch			  	 =	$objUsers->listQuery($queryMemberSearch);
if($start < $countSearchMembers){
	?>
	<div class="photoSearch">
	<?php
	if(count($getMemberSearch)>0){
		echo '<input type="hidden" class="pagenum" value="' . $page . '" /><input type="hidden" class="total-page" value="' . $pages . '" /><input type="hidden" id="total-count" value="'.$countSearchMembers.'" />';
		foreach($getMemberSearch as $allPhotoDetails){
			
			$displayNameFriend			  =	$objCommon->displayName($allPhotoDetails);
			//$vidRateCound				   =	$objCommon->round_to_nearest_half($allVideoDetails['voteAvg']);
			//$vidRateCoundStyle	   		  =	'style="width:'.($vidRateCound*20).'%;"';
	?>
            <div class="item">
                    <div class="potoImageFrame"><img src="<?php echo SITE_ROOT."uploads/albums/".$objCommon->html2text($allPhotoDetails['ai_images']); ?>" class="lightBoxs" data-contentId="<?php echo $allPhotoDetails['ai_id']; ?>" data-contentType="<?php echo $allPhotoDetails['user_id']; ?>" data-typeOf="<?php echo $type; ?>"  alt=""></div>
            <div class="clearfix"></div>
            <div class="dessi_bottom">
                
                <div class="clearfix"></div>
                <div class="by_bottom">
                    <div class="by">by <?php echo $displayNameFriend; ?></div>
                </div>
            </div>
            </div>
<?php
		}
	}
	echo '</div>';
}
}
?>
