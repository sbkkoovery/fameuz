<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/jobs.php");
	$objCommon				   =	new common();
	$objJobs				 	 =	new jobs();
	$jobid					   =	$objCommon->esc($_POST['jobid']);
	$userId					  =	$_SESSION['userId'];
	if($jobid !='' && $userId !=''){
		$getJobDetails		   =	$objJobs->getRowSql("SELECT job.*,applied.ja_id FROM jobs AS job LEFT JOIN job_applied AS applied ON (job.job_id = applied.job_id AND applied.ja_applied_by =".$userId.") WHERE job.job_id=".$jobid);
	?>
	<div class="modal-body ">
		<div class="row">
			<div class="col-md-3">
            	<div class="job_desc_head">
                   <h3> Job Title</h3>
                </div>
            </div>
			<div class="col-md-1">:</div>
			<div class="col-md-8"><?php echo $objCommon->html2text($getJobDetails['job_title'])?></div>
		</div>
		<div class="row">
			<div class="col-md-3">
                <div class="job_desc_head">
                    <h3>Job Location</h3>
                </div>
            </div>
			<div class="col-md-1">:</div>
			<div class="col-md-8"><?php echo ($getJobDetails['job_city'])?$objCommon->html2text($getJobDetails['job_city']).',':''?>
			<?php echo ($getJobDetails['job_state'])?$objCommon->html2text($getJobDetails['job_state']).',':''?><?php echo ($getJobDetails['job_country'])?$objCommon->html2text($getJobDetails['job_country']):''?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
                <div class="job_desc_head">
                    <h3>Job Date</h3>
                </div>
             </div>
			<div class="col-md-1">:</div>
			<div class="col-md-8"><?php echo ($getJobDetails['job_start'] !='' || $getJobDetails['job_start'] !=0000-00-00)?'Start Date : '.date("d M Y",strtotime($getJobDetails['job_start'])).' ':''?> <?php echo ($getJobDetails['job_start'] !='' || $getJobDetails['job_end'] !=0000-00-00)?'End Date : '.date("d M Y",strtotime($getJobDetails['job_end'])).' ':''?></div>
		</div> 
	<div class="row">
		<div class="col-md-3">
            <div class="job_desc_head">
                <h3>Job Company</h3>
            </div>
        </div>
		<div class="col-md-1">:</div>
		<div class="col-md-8"><?php echo $objCommon->html2text($getJobDetails['job_company'])?></div>
	</div>
	<div class="row">
		<div class="col-md-3">
            <div class="job_desc_head">
                <h3>Job Skill</h3>
            </div>
        </div>
		<div class="col-md-1">:</div>
		<div class="col-md-8"><?php echo $objCommon->html2text($getJobDetails['job_skill'])?></div>
	</div>
	<div class="row">
		<div class="col-md-12">
        <div class="job_desc_head">
                <h3>Job Description</h3>
        </div>
		<?php echo $objCommon->html2textarea($getJobDetails['job_descr'])?>
        </div>
	</div>
	<div class="row">
		<div class="col-sm-12 text-right">
			<?php
			if($getJobDetails['ja_id'] < 1){
			?>
			<button type="submit" class="btn btn-primary apply_for_job">Apply</button>
			<?php
			}else{
				echo '<span class="applied">You have already applied for this job !</span>';
			}
			?>
		</div>
	</div>
	<?php
	}
}
?>
<script>
$(".apply_for_job").on("click",function(){
	var jobid				=	'<?php echo $jobid?>';
	var jobBy				=	'<?php echo $getJobDetails['user_id']?>';
	var that				 =	this;
	if(jobid){
		$.ajax({
				url:'<?php echo SITE_ROOT.'ajax/apply_for_job.php'?>',
				data:{jobid:jobid,jobBy:jobBy},
				type:"POST",
				success: function(result){
					$(that).parent().html('<span style="color:#afb420;">Applied</span>');
				}
			});
	}
});
</script>