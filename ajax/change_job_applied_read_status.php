<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/job_applied.php");
	$objCommon				   =	new common();
	$objJobApplied			   =	new job_applied();
	$jobid				  	   =	$objCommon->esc($_GET['jobid']);
	$userId					  =	$_SESSION['userId'];
	if($jobid !='' && $userId!=''){
		$objJobApplied->updateField(array("ja_view_status"=>'1'),"job_id=".$jobid);
	}
}
?>