<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/videos.php");
	$objCommon				   =	new common();
	$objVideos			   	   =	new videos();
	$userId					  =	$_SESSION['userId'];
	if($_POST['hid_img_id'] !='' && $userId !=''){
		$hid_img_id			  =	$_POST['hid_img_id'];
		$img_title	 		   =	$_POST['img_title'];
		$video_tags			  =	$objCommon->esc($_POST['video_tags']);
		$ai_caption	  		  =	$objCommon->esc($img_title);
		$img_descr			   =	$objCommon->esc($_POST['img_descr']);
		$privacy				 =	$objCommon->esc($_POST['privacy']);
		if($privacy ==1){
			$video_privacy	   =	'1,0,0,0';
		}else if($privacy ==2){
			$video_privacy	   =	'0,1,0,0';
		}else if($privacy ==4){
			$video_privacy	   =	'0,0,0,1';
		}
		$objVideos->updateField(array('video_title'=>$ai_caption,'video_descr'=>$img_descr,'video_privacy'=>$video_privacy,"video_tags"=>$video_tags),"video_id=".$hid_img_id);
	}
}
?>