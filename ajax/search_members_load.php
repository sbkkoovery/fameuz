<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/users.php");
	$objUsers					=	new users();
	$objCommon				   =	new common();
	$perPage 				   	 = 	20;
	$page 					  	= 	1;
	if(!empty($_GET["page"])) {
		$page 					=	$_GET["page"];
	}
	$filter_online		  =	$objCommon->esc($_GET['filter_online']);
	$filter_category		=	$objCommon->esc($_GET['filter_category']);
	$filter_dicipline	   =	$objCommon->esc($_GET['filter_dicipline']);
	$filter_gender		  =	$objCommon->esc($_GET['filter_gender']);
	$filter_location		=	$objCommon->esc($_GET['filter_location']);
	$filter_keywords		=	$objCommon->esc($_GET['filter_keywords']);
	$myCategoryId		   =	$objCommon->esc($_GET['myCategoryId']);
	$countryId		   	  =	$objCommon->esc($_GET['countryId']);
	//-------------filter query-------------------------------------------------
	$sqlFilterOut			=	" ";
	if($filter_online==1){
		$sqlFilterOut	   .=	" AND tab.chatStatus = 1 ";
	}
	if($filter_category){
		$sqlFilterOut	   .=	" AND tab.uc_c_id =".$filter_category;
	}
	if($filter_dicipline){
		$sqlFilterOut	    .=	" AND tab.model_dis_id LIKE '%,".$filter_dicipline.",%'";
	}
	if($filter_gender){
		$sqlFilterOut	    .=	" AND tab.p_gender =".$filter_gender;
	}
	if($filter_location){
		$filter_locationArr  =	explode(",",$filter_location);
		$filter_locationArr  =	array_filter($filter_locationArr);
		if(count($filter_locationArr) >0){
			$sqlFilterOut	.=	" AND ( ";
			foreach($filter_locationArr as $keyFilterLocation=>$allFilterLocation){
				if($keyFilterLocation ==0){
					$sqlFilterOut.=	" tab.p_country LIKE '%".$allFilterLocation."%'  OR tab.p_city LIKE '%".$allFilterLocation."%' ";
				}else{
					$sqlFilterOut.=	" OR tab.p_country LIKE '%".$allFilterLocation."%'  OR tab.p_city LIKE '%".$allFilterLocation."%' ";
				}
			}
			$sqlFilterOut.=	" ) ";
		}
	}
	if($filter_keywords){
		$filter_keywordsArr  =	explode(",",$filter_keywords);
		$filter_keywordsArr  =	array_filter($filter_keywordsArr);
		if(count($filter_keywordsArr) >0){
			$sqlFilterOut	.=	" AND ( ";
			foreach($filter_keywordsArr as $keyFilterKeywords=>$allFilterKeywords){
				if($keyFilterKeywords ==0){
					$sqlFilterOut.=	" tab.p_country LIKE '%".$allFilterKeywords."%'  OR tab.p_city LIKE '%".$allFilterKeywords."%'  OR tab.first_name LIKE '%".$allFilterKeywords."%' OR tab.last_name LIKE '%".$allFilterKeywords."%' OR tab.display_name LIKE '%".$allFilterKeywords."%' OR tab.email LIKE '%".$allFilterKeywords."%' OR tab.usl_fameuz LIKE '%".$allFilterKeywords."%'";
				}else{
					$sqlFilterOut.=	" OR tab.p_country LIKE '%".$allFilterKeywords."%'  OR tab.p_city LIKE '%".$allFilterKeywords."%' ";
				}
			}
			$sqlFilterOut.=	" ) ";
		}
	}
//--------------------------------------------------------------------------
$sqlMemberSearch				 =	"SELECT tab.* FROM (SELECT user.user_id,user.first_name,user.last_name,user.display_name,profileImg.upi_img_url,user.email,social.usl_fameuz,follow.follow_user1,follow.follow_user2,follow.follow_status,case when (TIMESTAMPDIFF(MINUTE,chat.ucs_last_seen,'".date("Y-m-d H:i:s")."') >15 or chat.ucs_status=0) then '0' else '1' END AS chatStatus,personal.p_country,personal.p_city,personal.p_gender,
case when ((follow.follow_user1=".$_SESSION['userId']." or follow.follow_user2 =".$_SESSION['userId'].") and follow.follow_status=2) then  'friends' when (follow.follow_user1=".$_SESSION['userId']." and follow.follow_status=1) then 'following' when (follow.follow_user2=".$_SESSION['userId']." and follow.follow_status=1) then 'follower' else 'none' end as friendStatus,case when ((follow.follow_user1=".$_SESSION['userId']." or follow.follow_user2 =".$_SESSION['userId'].") and follow.follow_status=2) then  '3' when (follow.follow_user1=".$_SESSION['userId']." and follow.follow_status=1) then '2' when (follow.follow_user2=".$_SESSION['userId']." and follow.follow_status=1) then '1' else '0' end as friendStatusOrder,ucat.uc_c_id,mdetails.model_dis_id,cat.c_name,case when (promo.promo_id !='') then 1 else 0 END AS promoted_profile
FROM users AS user
LEFT JOIN user_chat_status AS chat ON user.user_id = chat.user_id 
LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
LEFT JOIN personal_details AS personal ON user.user_id=personal.user_id 
LEFT JOIN user_categories AS ucat ON user.user_id = ucat.user_id
LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
LEFT JOIN model_details AS mdetails ON user.user_id = mdetails.user_id
LEFT JOIN user_profile_image AS profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
LEFT JOIN promotion AS promo ON user.user_id = promo.promo_content_id AND promo.promo_type='profile' AND  promo.promo_country LIKE '%,".$countryId.",%' AND promo.promo_category LIKE '%".$myCategoryId."%' AND DATE(NOW()) between promo.promo_start_date and promo.promo_end_date AND promo.promo_admin_status=1 AND promo.promo_status=1
LEFT JOIN following as follow  ON (user.user_id = follow.follow_user1 and follow.follow_user1 !=".$_SESSION['userId']." ) or (user.user_id = follow.follow_user2 and follow.follow_user2 !=".$_SESSION['userId']." ) 
WHERE user.status= 1 AND user.email_validation=1 AND user.user_id != ".$_SESSION['userId']." 
) AS tab  WHERE 1 ".$sqlFilterOut."   GROUP BY tab.user_id ORDER BY   tab.promoted_profile DESC,tab.friendStatusOrder DESC,tab.first_name ASC";
$start 						   =	($page-1)*$perPage;
if($start < 0) { $start = 0; }
	$queryMemberSearch 		   =	$sqlMemberSearch . " limit " . $start . "," . $perPage; 
if(empty($_GET["rowcount"])) {
	$_GET["rowcount"] 		    =	$countSearchMembers	=	$objUsers->countRows($sqlMemberSearch);
}
$pages  = ceil($_GET["rowcount"]/$perPage);
$getMemberSearch			  	 =	$objUsers->listQuery($queryMemberSearch);
if($start < $countSearchMembers){
	?>
        <div class="row">
        <?php
        if(count($getMemberSearch)>0){
            echo '<input type="hidden" class="pagenum" value="' . $page . '" /><input type="hidden" class="total-page" value="' . $pages . '" /><input type="hidden" id="total-count" value="'.$countSearchMembers.'" />';
            foreach($getMemberSearch as $keyMemberSearch=>$allMemberSearch){
                $allMemberImg	   =	SITE_ROOT.'uploads/profile_images/'.(($allMemberSearch['upi_img_url'])?$allMemberSearch['upi_img_url']:"profile_pic.jpg");
                $allMemberName	  =	$objCommon->displayName($allMemberSearch);
                if($allMemberSearch['p_city'] != '' || $allMemberSearch['p_country'] != ''){
                    $friendPcity	=	($allMemberSearch['p_city'])?$objCommon->html2text($allMemberSearch['p_city'])." ,":"";
                }
        ?>
               <div class="col-sm-12 col-md-12 col-lg-6 border-support">
               <div class="row">
               <div class="col-sm-4 new-width-search">
               <div class="search-img">
                 <a href="<?php echo SITE_ROOT.$objCommon->html2text($allMemberSearch['usl_fameuz'])?>" title="<?php echo $allMemberName?>"><img src="<?php echo $allMemberImg?>" class="img-responsive" alt="<?php echo $allMemberName?>" title="<?php echo $allMemberName?>" /></a>
               </div>
               </div>
               <div class="col-sm-8">
               <div class="info-job search-main"><div class="job-head">
                <p><a href="<?php echo SITE_ROOT.$objCommon->html2text($allMemberSearch['usl_fameuz'])?>" title="<?php echo $allMemberName?>"><?php echo $allMemberName?></a></p><!--<img class="verify" src="images/verify.png" />-->
                </div>
                <div class="post-info">
				  <p class="proffesion"> <?php echo $objCommon->html2text($allMemberSearch['c_name'])?></p>
                  <p class="dim-me"><i class="fa_globe"></i>&nbsp;<?php echo $friendPcity.' '.$objCommon->html2text($allMemberSearch['p_country'])?></p>
                  </div>
                 <div class="status-user">
                <?php
                if($allMemberSearch['chatStatus']==1){
                    echo '<p class="online_status">online</p>';
                }else{
                    echo '<p class="offline_status">offline</p>';
                }
                ?>
                 </div>
                 <div class="row">
                 <div class="col-sm-7">
                 <!--<div class="rating_box">
                 <div class="rating_yellow" style="width:30%"></div>
                 </div>-->
					<?php
					if($allMemberSearch['promoted_profile']==1){
						echo '<div class="promoted_profile"><i class="fa fa-external-link-square"></i> Promoted</div>';
					}
					?>
                 </div>
                 <div class="col-sm-5 less-padding">
                 <div class="follow-btn pull-right">
                 <?php
                    if($allMemberSearch['friendStatus']=='friends'){
                    ?>
                    <a href="javascript:;" class="friend"><i class="fa fa-star-o"></i>Friend</a>
                    <?php
                    }else if($allMemberSearch['friendStatus']=='following'){
                    ?>
                    <a href="javascript:;" class="following"><i class="fa fa-check"></i> Following</a>
                    <?php
                    }else if($allMemberSearch['friendStatus']=='follower' || $allMemberSearch['friendStatus']=='none'){
                    ?>
                    <a href="javascript:;" class="follow has-spinner" data-friendid="<?php echo $objCommon->html2text($allMemberSearch['user_id'])?>">
                        <span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span>
                        <i class="fa fa-plus"></i>Follow
                    </a>
                    <?php
                    }
                    ?>
                 </div>
                 </div>
                 </div>
               </div>
               </div>
                </div>
               <div class="border-me-under"> </div>
               </div>
    <?php
            }
        }
    }
    ?>
    </div>
	<?php
}
?>