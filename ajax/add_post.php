<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/posts.php");
include_once(DIR_ROOT."class/user_photos.php");
include(DIR_ROOT."ajax/face_crop.php");
$objCommon				     =	new common();
$objPosts					  =	new posts();
$objUserPhotos				 =	new user_photos();
$userId					  	=	$_SESSION['userId'];
if($_POST['add_post']=='Post' && $userId != '' && ($_POST['post_descr'] !='' || count($_FILES['post_img']['tmp_name']) >0)){
	mysql_query("START TRANSACTION");
	$_POST['post_descr']	   =	$post_descr		=	$objCommon->esc($_POST['post_descr']);
	$_POST['user_id']		  =	$userId;
	$privacy1				  =	($objCommon->esc($_POST['post_privacy1'])==1)?1:0;
	$privacy2				  =	($objCommon->esc($_POST['post_privacy2'])==1)?1:0;
	$privacy4				  =	($objCommon->esc($_POST['post_privacy4'])==1)?1:0;
	if($privacy1==0 && $privacy2 ==0 && $privacy3 ==0){
		$_POST['post_privacy'] =	'1,0,0,0';
	}else{
		$_POST['post_privacy'] =	$privacy1.",".$privacy2.",0,".$privacy4;
	}
	$_POST['post_created']	 =	date("Y-m-d H:i:s");
	$_POST['post_edited']	  =	date("Y-m-d H:i:s");
	$_POST['edited_status']	=	0;
	$_POST['post_status']	  =	1;
	$objPosts->insert($_POST);
	$lastPostId				=	$objPosts->insertId();
	//----------------------------------image uploading-----------------------------
	include(DIR_ROOT."ajax/resize-class.php");
	$path 						= 	DIR_ROOT.'uploads/albums/';
	$valid_formats = array("jpg", "png", "gif", "bmp","jpeg","JPG", "PNG", "GIF", "BMP","JPEG");
	$uploadStatus		=	0;
	$imgStr			  =	'';
	if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
	{
		$todayDate		=	date('Y-m-d');
		if(!file_exists($path.$todayDate)){
			mkdir($path.$todayDate);
		}
		$path	=	$path.$todayDate.'/';
	foreach($_FILES['post_img']['tmp_name'] as $i => $tmp_name ){ 
	$name = $_FILES['post_img']['name'][$i];
	$size = $_FILES['post_img']['size'][$i];
	if(strlen($name))
	{
		$ext	=	strtolower(pathinfo($name, PATHINFO_EXTENSION));
		if(in_array($ext,$valid_formats))
		{
		if($size<(3072*10240))
		{
		$time						=	time();				
		$actual_image_name_ext_no	=	$time."_".$userId.'_'.$i;
		$actual_image_name 		   = 	$time."_".$userId.'_'.$i.".".$ext;
		
		if(!file_exists($path."original")){
			mkdir($path."original");
		}
		if(!file_exists($path."thumb")){
			mkdir($path."thumb");
		}
		$tmpName = $_FILES['post_img']['tmp_name'][$i];    
	list($widthu, $heighut, $typeu, $attru) = getimagesize($tmpName); 
		if(move_uploaded_file($_FILES['post_img']['tmp_name'][$i], $path.'original/'.$actual_image_name)){
		
		$resizeObj = new resize($path.'original/'.$actual_image_name);
		//$resizeObj -> resizeImage(200, 200, 'exact');
		//$resizeObj -> saveImage($path.'thumb/'.$actual_image_name, 100);
		if($heighut >$widthu){
			if ($heighut > 700){
				$resizeObj -> resizeImage('', 700, 'auto');
				$resizeObj -> saveImage($path.$actual_image_name, 100);
			}else{
				$resizeObj -> resizeImage('', $heighut, 'auto');
				$resizeObj -> saveImage($path.$actual_image_name, 100);
			}
		}else{
			if ($widthu > 900){
				$resizeObj -> resizeImage(900, '', 'auto');
				$resizeObj -> saveImage($path.$actual_image_name, 100);
			}else{
				$resizeObj -> resizeImage($widthu,'', 'auto');
				$resizeObj -> saveImage($path.$actual_image_name, 100);
			}
		}
		//----------------face cropping----------------------------------------------
		if($heighut < 192 || $widthu <192){
			$resizeObj -> resizeImage($widthu, $heighut, 'exact');
			$resizeObj -> saveImage($path.'thumb/'.$actual_image_name, 100);
		}else{
			cropImg($path.'original/'.$actual_image_name,$path.'thumb/',$actual_image_name_ext_no,'192','192');
		}
		//----------------face cropping----------------------------------------------
		//----------------start upload to AWS----------------------------------------
		require(DIR_ROOT."amazone_s3/S3.php");
		$s3			=	new S3(AWS_ID,AWS_KEY);
		S3::putObjectFile($path.$actual_image_name,AWS_IMG_BUCKET,'uploads/albums/'.$todayDate.'/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
		S3::putObjectFile($path.'thumb/'.$actual_image_name,AWS_IMG_BUCKET,'uploads/albums/'.$todayDate.'/thumb/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
		//----------------end upload to AWS-------------------------------------------
		$_POST['photo_url']   		  =	$todayDate."/".$actual_image_name;
		$_POST['post_id']	 		=	$lastPostId;
		$_POST['photo_created']	  =	date("Y-m-d H:i:s");
		$_POST['photo_edited']	   =	date("Y-m-d H:i:s");
		$objUserPhotos->insert($_POST);
		 }
	}
	
	}
	
	}
	
		}
	
	}
//------------------------------------------------------------
	mysql_query("COMMIT");
}
header("location:".SITE_ROOT."user/home");
?>