<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/user_chat_status.php");
	$objCommon				   =	new common();
	$objUserChatStatus		   =	new user_chat_status();
	$dataVal					 =	$objCommon->esc($_GET['dataVal']);
	$user_id					 =	$_SESSION['userId'];
	if($dataVal !='' && $user_id !=''){
		$objUserChatStatus->updateField(array('ucs_online_status'=>$dataVal),"user_id=".$user_id);
		$getUserChatStatus	   =	$objUserChatStatus->getRow("user_id=".$user_id);
		$statusOnlineDis		 =	'';
		$statusBsyDis			=	'';
		$statusInvDis			=	'';
		if($getUserChatStatus['ucs_online_status']==1){
			$statusClass		 =	'status_online';
			$statusText		  =	'Online';
			$statusOnlineDis	 =	'status_hide';
		}else if($getUserChatStatus['ucs_online_status']==2){
			$statusClass		 =	'status_busy';
			$statusText		  =	'Busy';
			$statusBsyDis	 	=	'status_hide';
		}else if($getUserChatStatus['ucs_online_status']==0){
			$statusClass		 =	'status_invisible';
			$statusText		  =	'Invisible';
			$statusInvDis	 	=	'status_hide';
		}
		?>
		<a href="#" class="<?php echo $statusClass?> current_status" id="change_status"><?php echo $statusText?><i class="fa fa-sort-desc"></i></a>
		<div class="status_option" id="status_option">
			<div class="status_option_arrow"><img src="<?php echo SITE_ROOT?>images/status_option_arrow.png" alt="status_option_arrow" /></div>
			<a href="#"  class="status_online check_Status <?php echo $statusOnlineDis?>" data-value="1">Online</a> 
			<a href="#"  class="status_busy check_Status <?php echo $statusBsyDis?>" data-value="2">Busy</a>     
			<a href="#" class="status_invisible check_Status <?php echo $statusInvDis?>" data-value="0">Invisible</a>
			<a href="<?php echo SITE_ROOT?>user/logout"  class="status_invisible">Sign out </a>
		</div>
		<script>
		$('#status_option').on("click",'.check_Status',function(e) {
                var dataVal	=	$(this).attr('data-value');
				if(dataVal){
					$.get('<?php echo SITE_ROOT?>ajax/change_online_chat_status.php',{"dataVal":dataVal},function(data1){
						$(".status_box").html(data1);
					});
				}
            });
			</script>
	 <?php
	}
}
?>