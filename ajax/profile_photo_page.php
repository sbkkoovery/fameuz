<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/user_profile_image.php");
$objCommon				   =	new common();
$objUserProfileImg		   =	new user_profile_image();
$userId					  =	$_SESSION['userId'];
if($userId !=''){
	$getProfileImages	   =	$objUserProfileImg->getAll("user_id=".$userId,"upi_created desc");
?>
	<?php
	if(count($getProfileImages)>0){
	?>
	<div class="row albListShow">
	<div class="col-md-12">
	<div class="add_photo_album_list">
		<ul class="album_list_ul">
			<?php
			foreach($getProfileImages as $allProfileImages){
				$getAiImages		=	$allProfileImages['upi_img_url'];
				?>
			<li>
				<div class="pic">
					<?php if($allProfileImages['upi_status']==1){ echo '<div class="cur_profile_pic"><i class="fa fa-check"></i></div>'; } ?>
					<a href="javascript:;" data-contentId="<?php echo $allProfileImages['upi_id']?>" data-contentType="6" data-contentAlbum="0" class="lightBoxs"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo $allProfileImages['upi_img_url']?>" class="img-responsive"  /></a>
				</div>
			</li>
			<?php
			}
			?>
		</ul>
	</div>
	</div>
	</div>
	<?php
	}else{
		echo '<div class="row albListShow"><div class="col-md-12"><p class="no_img_album text-center">There are no photos in your profile</p></div></div>';
	}
	?>
	<div class="row">
		<div class="col-md-12">
		<div id="preview"></div>
		</div>
	</div>
	<?php
}
?>