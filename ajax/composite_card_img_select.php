<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/album_images.php");
$objCommon				 =	new common();
$objAlbumImages			=	new album_images();
$ImgPos					=	$objCommon->esc($_GET['ImgPos']);
$getAllAlbumImages		 =	$objAlbumImages->listQuery("SELECT ai_id,ai_images FROM album_images WHERE user_id =".$_SESSION['userId']." ORDER BY ai_created DESC");
?> 
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.form.js"></script>
<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select Photo</h4>
      </div>
      <div class="modal-body pic-body">
       		<div class="gallery-pic">
            	<div class="row">
                <?php 
				if(count($getAllAlbumImages)>0){
				foreach($getAllAlbumImages as $allAlbumImages){?>
                	<div class="col-sm-2">
                    	<div class="thumb-galery" data-imageid="<?php echo $allAlbumImages['ai_images']?>">
                            <img class="img-responsive" src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/albums/'.$objCommon->getThumb($objCommon->html2text($allAlbumImages['ai_images']))?>" />
                        </div>
                    </div>
                <?php 
				}
				}else{
					echo '<p>No photos found in your album...</p>';
				}
				?>
                </div>
            </div> 
			
      </div>
	  <form action='<?php echo SITE_ROOT?>ajax/ajaximage_composite_card.php?path=<?php echo DIR_ROOT?>uploads/albums/' method="post" enctype="multipart/form-data" id="media_form">
	  <div class="selectFromDevice text-center">
	  	<div id="preview"></div>
		<i class="fa fa-video-camera"></i>
		Add photos From Device
		<input id="file_browse" class="album_uploader" type="file"  name="file">
	</div>
	</form>
      <div class="modal-footer">
       <button type="button" disabled="disabled" class="btn btn-primary btn-change selectPhoto">Select Photo</button>
      </div>
    </div>
<script>
$(".thumb-galery").click(function () {
    $(".thumb-galery").removeClass("border-selected");
    $(this).addClass("border-selected");
    $(".btn-change").prop("disabled", false);
});
$(".close").click(function(){
	$(".btn-change").prop("disabled", true);
});
$(".selectPhoto").on("click",function(){
	$("#slectPhoto").modal('hide');
	/*$(".modalUploadPosition").html('<div class="modal-body"><div class="load_album_preloader"></div></div>');*/
	var imgPos			=	'<?php echo $ImgPos?>';
	var imgId			 =	$(".border-selected").data("imageid");
	$.ajax({
		url:'<?php echo SITE_ROOT?>ajax/composite_card_img_position.php?imgPos='+imgPos+'&imgId='+imgId,
		success:function(data){
			$(".modalUploadPosition").html(data);
		}
	});
});
$(function() {
		$('.pic-body').perfectScrollbar();
});
$(document).ready(function(e) {
	$('#media_form').on('change', function(){ 
	$("#preview").html('<div class="load_album_preloader"></div>');
		$("#media_form").ajaxForm(
				{
					target: '#preview',
					success:successCall
				
				}).submit();
				$(".albListShow").hide();
				//setTimeout(function () { inst.close();window.location.hash ='list';}, 3000);
	});
});
function successCall(){
	 $(".load_album_preloader").hide();
	 $("#slectPhoto").modal('hide');
	var imgPos			=	'<?php echo $ImgPos?>';
	var imgId			 =	$("#uploadAlbumImg").val();
		$.ajax({
			url:'<?php echo SITE_ROOT?>ajax/composite_card_img_position.php?imgPos='+imgPos+'&imgId='+imgId,
			success:function(data){
				$(".modalUploadPosition").html(data);
			}
		});
}
</script>