<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/game_rating.php");
	$objCommon				   =	new common();
	$objGameRating			   =	new game_rating();
	$game_id					 =	$objCommon->esc($_POST['game_id']);
	$rating					  =	$objCommon->esc($_POST['rating']);
	$userId					  =	$_SESSION['userId'];
	if($userId !='' && $game_id != '' && $rating != ''){
		$getGameRate		   	 =	$objGameRating->getRow("user_id=".$userId." AND g_id=".$game_id);
		if($getGameRate['gr_id'] ==''){
			$_POST['user_id']	=	$userId;
			$_POST['g_id']   	   =	$game_id;
			$_POST['gr_created'] =	date("Y-m-d H:i:s");
			$_POST['gr_rating']  =	$rating;
			$objGameRating->insert($_POST);
		}else{
			$gr_created 		  =	date("Y-m-d H:i:s");
			$objGameRating->updateField(array("gr_created"=>$gr_created,"gr_rating"=>$rating),"gr_id=".$getGameRate['gr_id']);	
		}
		$getAvg				  =	$objGameRating->getRowSql("SELECT avg(gr_rating) AS rateAvg FROM game_rating WHERE  g_id=".$game_id);
		$avgRate				 =	$getAvg['rateAvg']*20;
		echo '<div class="rating_box"><div class="rating_yellow" style="width:'.round($avgRate).'%;"></div></div>';
	}
}
?>