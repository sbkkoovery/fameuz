<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/book_model.php");
	$objCommon				   =	new common();
	$objBookModel			    =	new book_model();
	$bookId				  	  =	$objCommon->esc($_GET['bookId']);
	$read				  	  	=	$objCommon->esc($_GET['read']);
	$userId					  =	$_SESSION['userId'];
	if($bookId !='' && $userId!='' && $read !='' && $read==0){
		$objBookModel->updateField(array("bm_read_status"=>'1'),"bm_id=".$bookId);
	}
	$acceptid					=	$objCommon->esc($_GET['acceptid']);
	if($acceptid !='' && $userId!=''){
		$objBookModel->updateField(array("bm_model_status"=>'1'),"bm_id=".$acceptid);
		$getAgent				=	$objBookModel->getRowSql("select agent_id from book_model where bm_id=".$acceptid);
		$agentId				 =	$getAgent['agent_id'];
	}
	$declineid					=	$objCommon->esc($_GET['declineid']);
	if($declineid !='' && $userId!=''){
		$objBookModel->updateField(array("bm_model_status"=>'2'),"bm_id=".$declineid);
		$getAgent				=	$objBookModel->getRowSql("select agent_id from book_model where bm_id=".$declineid);
		$agentId				 =	$getAgent['agent_id'];
	}
	//----notification table------------------------------------
			$myDetails				   		   	   =	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$userId);
			$displayName						  	 =	$objCommon->displayName($myDetails);
			$friend_id							   =	$agentId;
			$notiType								=	'booking';
			$notiImg								 =	'';
			if($acceptid){
				$notiDescr  	 	 				   =	'<b><a href="'.SITE_ROOT.$objCommon->html2text($myDetails['usl_fameuz']).'">'.$displayName.'</a></b> has accepeted your <b>booking request</b>.';
			}else{
				$notiDescr   	 	 				   =	'<b><a href="'.SITE_ROOT.$objCommon->html2text($myDetails['usl_fameuz']).'">'.$displayName.'</a></b> has declined your <b>booking request</b>.';
			}
			$notiUrl  								 =	SITE_ROOT.'user/booking-requests';
			$objCommon->pushNotification($friend_id,$userId,$notiType,$notiImg,$notiDescr,$notiUrl);
			//----------------------------------------------------------
}
?>