<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/album_images.php");
$objCommon				   =	new common();
$objAlbumImages			  =	new album_images();
$albumId					 =	$objCommon->esc($_GET['albumId']);
$friendId					=	$objCommon->esc($_GET['friendId']);
if($albumId !='' && $friendId !=''){
	$getAlbumPhotos		  =	$objAlbumImages->listQuery("select img.ai_images,img.ai_caption,album.a_name,img.ai_id,img.ai_created,img.ai_set_main from album_images as img left join album on img.a_id=album.a_id where   img.a_id=".$albumId." and img.user_id=".$friendId." order by img.ai_created desc");
?>
<div class="album-head inner"> <img src="<?php echo SITE_ROOT?>images/gallery.png"><span class="head-album"><?php echo $objCommon->html2text($getAlbumPhotos[0]['a_name'])?></span> </div>
	<?php
	if(count($getAlbumPhotos)>0){
	?>
	<div class="row albListShow">
	<div class="col-md-12">
	<div class="add_photo_album_list">
		<ul class="album_list_ul">
			<?php
			foreach($getAlbumPhotos as $allAlbumPhotos){
				$getAiImages		=	$allAlbumPhotos['ai_images'];
				list($widthImg,$heightImg,$typeImg,$attrImg) = getimagesize(DIR_ROOT."uploads/albums/".$allAlbumPhotos['ai_images']);
				?>
			<li><div class="pic"><a href="javascript:;" class="lightBoxs" data-contentId="<?php echo $allAlbumPhotos['ai_id']?>" data-contentType="2" data-contentAlbum="<?php echo $albumId?>"><img src="<?php echo SITE_ROOT?>uploads/albums/<?php echo $objCommon->getThumb($allAlbumPhotos['ai_images'])?>" class="img-responsive"  /></a></div></li>
			<?php
			}
			?>
		</ul>
	</div>
	</div>
	</div>
	<?php
	}else{
		echo '<div class="row albListShow"><div class="col-md-12"><p class="no_img_album text-center">There are no photos in this album</p></div></div>';
	}
}
?>
<script type="text/javascript">
$(document).ready(function(){
	 $(".lightBoxs").lightBox({
	  videoSkin: '<?php echo SITE_ROOT?>jw_player/six/six.xml',
	  photosObj: true,
	  videoObj :  true,
	  defaults : true,
	  album   : false,
	  deleteCommentUrl : '<?php echo SITE_ROOT ?>access/delete_comment.php',
	  skins : {
	   loader   : '<?php echo SITE_ROOT?>images/ajax-loader.gif',
	   next      : '<img src="<?php echo SITE_ROOT?>fameuz_lightbox/images/next.png">',
	   prev   : '<img src="<?php echo SITE_ROOT?>fameuz_lightbox/images/prev.png">',
	   dismiss     : '<img src="<?php echo SITE_ROOT?>fameuz_lightbox/images/close.png" width="15">',
	   reviewIcon  : '<i class="fa fa-chevron-right"></i>',
	  },
	  photos : {
	   fetchUrl: '<?php echo SITE_ROOT.'ajax/popUpImageUser.php'?>',
	   ajaxUrl : '<?php echo SITE_ROOT.'widget/pop_image_comment_box.php' ?>',
	   comment : {
		CommentFormAction : '<?php echo SITE_ROOT?>access/post_comment.php?action=add_comment',
		  commentLikeUrl   : '<?php echo SITE_ROOT?>ajax/like_comment.php',
	   },
	   shareItems : {
		likeAjaxUrl  : '<?php echo SITE_ROOT?>ajax/like.php',
		shareAjaxUrl : '<?php echo SITE_ROOT?>ajax/share.php',
		workedAjaxUrl: '<?php echo SITE_ROOT?>ajax/worked_together.php',
	   },
	  },
	  video : {
	   url  : '<?php echo SITE_ROOT?>uploads/testuploads/1.mp4',
	   fetchUrl: '<?php echo SITE_ROOT.'ajax/popUpVideoUser.php'?>',
	   ajaxUrl : '<?php echo SITE_ROOT.'widget/pop_video_comment_box.php' ?>',
	   comment : {
		CommentFormAction : '<?php echo SITE_ROOT?>access/post_video_comment.php?action=add_comment',
		commentLikeUrl   : '<?php echo SITE_ROOT?>ajax/like_video_comment_pop.php',
	   },
	   shareItems : {
		likeAjaxUrl  : '<?php echo SITE_ROOT?>ajax/like_video.php',
		shareAjaxUrl : '<?php echo SITE_ROOT?>ajax/share_video.php',
	   },
	  },
	  shareItems : {
	   likeAjaxUrl : '<?php echo SITE_ROOT?>ajax/like.php',
	   shareAjaxUrl: '<?php echo SITE_ROOT?>ajax/share.php',
	  }
	 });
});
</script>