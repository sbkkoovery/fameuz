<?php 
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/users.php");
$objCommon			   =	new common();
$objUser			  	 =	new users();
$getUserDetails	  	  =	$objUser->getRowSql("SELECT profileImg.upi_img_url,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 WHERE user.status=1 AND user.user_id=".$_SESSION['userId']);
if(isset($_GET['ecrId'])){
	$ecrId		 	   =	$objCommon->esc($_GET['ecrId']);
	$videoDetails    	=	$objUser->getRowSql("SELECT vid.video_id,vid.video_url,video_encr_id,vid.video_thumb,vid.video_duration,vid.video_type,vid.video_title,vid.video_descr,vid.video_visits,vid.video_created,vid.video_privacy,user.first_name,user.user_id,user.last_name,user.display_name,user.email,social.usl_fameuz,profileImg.upi_img_url,count(DISTINCT likes.like_id) AS likeCount,group_concat(DISTINCT likes.like_user_id) AS youLike,count(DISTINCT share.share_id) AS shareCount,group_concat(DISTINCT share.user_by) AS youShare,comments.commentCount
										FROM videos AS vid
										LEFT JOIN users AS user ON  vid.user_id = user.user_id
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN likes  ON  vid.video_id = likes.like_content AND likes.like_cat = 4 AND likes.like_status=1
										LEFT JOIN share  ON  vid.video_id = share.share_content AND share.share_category = 3 AND share.share_status=1
										LEFT JOIN (SELECT COUNT(comment_id) AS commentCount,comment_content  FROM comments WHERE comment_cat=4 GROUP BY comment_content) AS comments ON vid.video_id  = comments.comment_content 
										WHERE vid.video_status=1 AND user.status=1 AND user.email_validation =1 AND vid.video_encr_id='".$ecrId."'");							
if($videoDetails['video_url'] != ''){
//---------------------------------------get videos for sliders--------------
$getOtherVideosSql					=	"select group_concat(vid.video_encr_id order by video_created desc) AS vidUrls from videos as vid  where user_id=".$videoDetails['user_id']." and video_status = 1  order by video_created desc";
$getOtherVideo						=	$objUser->getRowSql($getOtherVideosSql);
$videosArr							=	$getOtherVideo['vidUrls'];
$videoArrExpl						 =	explode(",",$videosArr);
$videoArrExpl						 =	array_filter($videoArrExpl);
if(count($videoArrExpl)>0){
	$keyCurrent					   =	array_search($ecrId,$videoArrExpl);
	$keyNext						  =	$keyCurrent+1;
	$keyPrev						  =	$keyCurrent-1;
	if(array_key_exists($keyNext,$videoArrExpl)){
		$nextValue					=	$videoArrExpl[$keyNext];
	}else{
		$nextValue					=	'';
	}
	if(array_key_exists($keyPrev,$videoArrExpl)){
		$prevValue					=	$videoArrExpl[$keyPrev];
	}else{
		$prevValue					=	'';
	}
}
//-------------------------------------------------------------------------------- 
$userImageCon			=	($videoDetails['upi_img_url'])?$videoDetails['upi_img_url']:'profile_pic.jpg';
$userImage			   =	SITE_ROOT.'uploads/profile_images/'.$userImageCon;
$userName				=	$objCommon->displayName($videoDetails);
$myImageCon			  =	($getUserDetails['upi_img_url'])?$getUserDetails['upi_img_url']:'profile_pic.jpg';
$myImage				 =	SITE_ROOT.'uploads/profile_images/'.$myImageCon;
$videoTitle			  =	$objCommon->html2text($videoDetails['video_title']);
$img_descr			   =	$objCommon->html2text($videoDetails['video_descr']);
$limit				   =	300;
if(strlen($img_descr) > $limit){
	$img_descrSmall	  =	mb_substr($img_descr,0,$limit,'UTF-8')."...";
	$img_descrSmall	 .=	$img_descrSmall.'<p class="readP text-right"><a href="javascript:;" class="readMore">Read more</a></p>';
	$img_descrFull	   =	$img_descr;
}else{
	$img_descrSmall	  =	$img_descr;
	$img_descrFull	   =	$img_descrFull;
}
$commentCount			=	number_format($videoDetails['commentCount']);
$imageCreatedOn		  =	date("d M , Y h:i a",strtotime($objCommon->local_time($videoDetails['video_created'])));
if($videoDetails['video_type']==1){
	$getAiImages				=	SITE_ROOT.'uploads/videos/'.$videoDetails['video_thumb'];
	$vidUrl					 =	SITE_ROOT.'uploads/videos/'.$videoDetails['video_url'];
}else if($videoDetails['video_type']==2){
	if (preg_match('%^https?://[^\s]+$%',$getVideoDetails['video_thumb'])) {
		$getAiImages			=	$getVideoDetails['video_thumb'];
	} else {
		$getAiImages			=	SITE_ROOT.'uploads/videos/'.$videoDetails['video_thumb'];
	}
	$vidUrl					 =	$objCommon->html2text($videoDetails['video_url']);
}
$imagePrevId					=	$prevValue;
$imageNextId					=	$nextValue;
$likeCount					  =	$videoDetails['likeCount'];
$youLikeArr	   				 =	explode(",",$videoDetails['youLike']);
if(count($youLikeArr)>0){
	if(in_array($_SESSION['userId'],$youLikeArr)){
		$youLike	  			=	1;
	}else{
		$youLike	  			=	0;
	}
}
$shareCount	   				 =	$videoDetails['shareCount'];
$youShareArr	  				=	explode(",",$videoDetails['youShare']);
if(count($youShareArr)>0){
	if(in_array($_SESSION['userId'],$youShareArr)){
		$youShare	  		   =	1;
	}else{
		$youShare   	  		   =	0;
	}
}
$jsonArr				 =	array('userMe'=>$_SESSION['userId'],'videoId'=>$videoDetails['video_id'],'videourl'=>$vidUrl,'imageUrl'=>$getAiImages,'userImage'=>$userImage,'userName'=>$userName,'videoTitle'=>$videoTitle,'imgDescrSmall'=>$img_descrSmall,'imgDescrFull'=>$img_descrFull,'imageCreatedOn'=>$imageCreatedOn,'imagePrevId'=>$imagePrevId,'imagePrevType'=>4,'imageNextId'=>$imageNextId,'imageNextType'=>4,'userto'=>$videoDetails['user_id'],'myimage'=>$myImage,'youLike'=>$youLike,'likeCount'=>$likeCount,'youShare'=>$youShare,'shareCount'=>$shareCount,'commentCount'=>$commentCount);
echo json_encode($jsonArr);
exit;
} 
}
?>