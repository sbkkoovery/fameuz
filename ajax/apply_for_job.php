<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/job_applied.php");
	$objCommon				   =	new common();
	$objJobApplied			   =	new job_applied();
	$jobid					   =	$objCommon->esc($_POST['jobid']);
	$jobBy					   =	$objCommon->esc($_POST['jobBy']);
	$userId					  =	$_SESSION['userId'];
	if($jobid !='' && $userId !=''){
		$_POST['job_id']		 =	$jobid;
		$_POST['ja_applied_by']  =	$userId;
		$_POST['ja_view_status'] =	0;
		$_POST['ja_created']	 =	date("Y-m-d H:i:s");
		$getAppliedFor		   =	$objJobApplied->getRow("job_id=".$jobid." AND ja_applied_by=".$userId);
		if($getAppliedFor['ja_id'] ==''){
			$objJobApplied->insert($_POST);
			//----notification table------------------------------------
			$myDetails				   		   	   =	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$userId);
			$displayName						  	 =	$objCommon->displayName($myDetails);
			$friend_id							   =	$jobBy;
			$notiType								=	'applied_job';
			$notiImg								 =	'';
			$notiDescr   	 	 					   =	'<b>'.$displayName.'</b> has sent you <b>job apply request</b>';
			$notiUrl  								 =	SITE_ROOT.'user/my-jobs?active='.$jobid;
			$objCommon->pushNotification($friend_id,$userId,$notiType,$notiImg,$notiDescr,$notiUrl);
			//----------------------------------------------------------
		}
	}
}
?>