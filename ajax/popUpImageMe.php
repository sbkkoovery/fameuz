<?php 
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/album_images.php");
$objCommon			   =	new common();
$objAlbums			   =	new album_images();
if(isset($_GET['imageId'])){
	$imageId		 	 =	$objCommon->esc($_GET['imageId']);
	$im_type		 	 =	$objCommon->esc($_GET['im_type']);
	$albumId			 =	($_GET['albumId'])?$objCommon->esc($_GET['albumId']):0;
	if($im_type==1){
		$imageDetails    =	$objAlbums->getRowSql("SELECT photos.user_id,photos.photo_url AS img_url,photos.photo_id AS img_id,photos.photo_descr as img_descr,photos.photo_created AS img_created,IF(photos.photo_id != '', 1, '') 		AS img_type,photos.photo_like_count AS likeCount,photos.photo_comment_count AS commentCount,photos.photo_set_main AS set_main,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz
	FROM user_photos as photos
	LEFT  JOIN users AS user ON photos.user_id = user.user_id
	LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
	LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
	WHERE photos.photo_id=".$imageId);	
	}else if($im_type==2){
		$imageDetails    =	$objAlbums->getRowSql("SELECT photos.user_id,photos.ai_images AS img_url,ai_id AS img_id,photos.ai_caption AS img_descr,photos.ai_created AS img_created,IF(photos.ai_id != '', 2, '') AS img_type,photos.ai_like_count AS likeCount,photos.ai_comment_count AS commentCount,photos.ai_set_main AS set_main,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz
		FROM album_images  AS photos
		LEFT  JOIN users AS user ON photos.user_id = user.user_id
		LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
		LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
		WHERE ai_id=".$imageId);
	}else if($im_type==8){
		$imageDetails    =	$objAlbums->getRowSql("SELECT photos.user_id,photos.polo_url AS img_url,photos.polo_id AS img_id,photos.polo_descr as img_descr,photos.polo_created AS img_created,IF(photos.polo_id != '', 1, '') 		AS img_type,photos.polo_like_count AS likeCount,photos.polo_comment_count AS commentCount,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,photos.polo_order AS img_order
	FROM polaroids as photos
	LEFT  JOIN users AS user ON photos.user_id = user.user_id
	LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
	LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
	WHERE photos.polo_id=".$imageId);	
	}else if($im_type==6){
		include_once(DIR_ROOT."class/user_profile_image.php");
		$objUserProfileImg	   =	new user_profile_image();
		$imageDetails    =	$objUserProfileImg->getRow("upi_id=".$imageId);	
	}
?>
<div class="image_comment_load">
<div class="
 alertClose alert-success alert-dismissible" role="alert" style="display:none; margin-bottom:0px;"><button type="button" class="close" data-dismiss="alertClose" aria-label="Close"><span aria-hidden="true">&times;</span></button> Updated</div>
    <div class="comments">
        <form class="img_pop_form" id="img_pop_form" action="<?php echo SITE_ROOT?>ajax/album_settings.php" method="post">
			<?php
            if($im_type==8){
            ?>
            <div class="form-group">
					<label>Polaroids Order</label>
					<input type="text" name="img_order" id="img_order" value="<?php echo $objCommon->html2textarea($imageDetails['img_order'])?>" />
				</div>
                <?php }?>
                <div class="form-group">
                <?php
				if($im_type != 6 ){
				?>
                <label for="img_descr">Image description</label>
                <textarea class="form-control" rows="1" name="img_descr" id="img_descr" placeholder="Say something about...."><?php echo $objCommon->html2text($imageDetails['img_descr'])?></textarea>
                <?php }?>
                    <input type="hidden"  value="<?php echo $imageId?>" name="hidImgId"  class="hidImgId"/>
				<?php
					if($im_type==1 && $im_type==2){
				?>
                    <label><input type="checkbox" name="set_cover" id="set_cover" value="1" <?php echo ($imageDetails['set_main']==1)?'checked="checked"':''?> > Set as cover image</label>
				<?php 
					 }else if($im_type == 6){
				 ?>
                         <label><input type="checkbox" name="set_cover" id="set_cover" value="1" <?php echo ($imageDetails['upi_status']==1)?'checked="checked"':''?> > Set as profile image</label>
                 <?php
					 }
				?>
            </div>
            <button type="button" class="btn btn-default btn-sm" id="post_img_descr" onclick="sendFrm(this);">save</button>
        </form>
    </div>
    <div class="remove_img pull-right"><a href="javascript:;" onclick="delImg('<?php echo $im_type?>','<?php echo $imageId?>');" class="delPopImg">Remove Image</a></div>
</div>
<script type="text/javascript">
function sendFrm(e){
	var hidImgId=$(e).parent().children().children(".hidImgId").val();
	var img_descr=$(e).parent().children().children("#img_descr").val();
	var img_order=$(e).parent().children().children("#img_order").val();
	var set_cover;
	if($(e).parent().children().children().children("#set_cover").is(":checked")){
		set_cover	=1;
	}
	if(hidImgId){
		$.ajax({
				url:'<?php echo SITE_ROOT?>ajax/album_settings.php',
				data:{img_descr:img_descr,hidImgId:hidImgId,set_cover:set_cover,imgType:'<?php echo $im_type ?>',img_order:img_order},
				type:"GET",
				success: function(data){
					$( ".alertClose" ).show().delay( 4000 ).slideUp( 400 );
				}
		});
	}
	
}
function delImg(imgType,imgid){
	$.confirm({
		'title'	  : 'Delete Confirmation',
		'message'	: 'You are about to delete this item ?',
		'buttons'	: {
		'Yes'		: {
		'class'	  : 'blue',
		'action': function(){
			$('.overlay_bg').hide();
			$('.user_data h3').text('');
			$('.user_data p').text('');
			jwplayer( 'vidSection' ).stop();
			$(".load_album_content").html('<div class="load_album_preloader"></div>');
			$.ajax({
				url:'<?php echo SITE_ROOT?>ajax/delete_img_album.php',
				data:{imgType:imgType,imgid:imgid},
				type:"GET",
				success: function(){
					if(imgType == 1){
						$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/timeline_photo_page.php');
					}else if(imgType == 2){
						$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/album_photo_page.php?albumId=<?php echo $albumId?>');
					}else if(imgType == 8){
						$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/polaroids_photo_page.php');
					}else if(imgType == 6){
						$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/profile_photo_page.php');
					}
					$(".load_album_preloader").hide();
					
				}
			});
		}
		},
		'No'	: {
		'class'	: 'gray',
		'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
		}
		}
	});
}
</script>
<?php }?>