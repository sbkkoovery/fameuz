<?php
	@session_start();
	date_default_timezone_set('UTC');
	ini_set('date.timezone', 'UTC');
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	$objCommon				   =	new common();
	$timenow	=	$objCommon->local_time(date("Y-m-d H:i:s"));
	echo date("l. F d, Y h:i:s A",strtotime($timenow));
?>