<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/album_images.php");
	include_once(DIR_ROOT."class/user_photos.php");
	$objCommon				   =	new common();
	$objAlbumImages			  =	new album_images();
	$objUserPhotos			   =	new user_photos();
	$userId					  =	$_SESSION['userId'];
	$imgType				= $objCommon->esc($_GET['imgType']);
	if($_GET['hidImgId'] !=''  && $userId !='' && $imgType != ''){
		$hid_img_id			 =	$_GET['hidImgId'];
		$img_descr	 		  =	$_GET['img_descr'];
		$set_cover			  =	$objCommon->esc($_GET['set_cover']);		
		$ai_caption			 =	$objCommon->esc($img_descr);
		$editedTime			 =	date('Y-m-d H:i:s');
		if($set_cover=='1' && $imgType ==1 && $imgType ==2){
			$objUserPhotos->updateField(array('photo_set_main'=>0),"user_id=".$userId);
			$objAlbumImages->updateField(array('ai_set_main'=>0),"user_id=".$userId);
		}
		if($imgType == 1){
			$objUserPhotos->updateField(array('photo_descr'=>$ai_caption,'photo_edited'=>$editedTime,'photo_set_main'=>$set_cover),"photo_id=".$hid_img_id);
		}else if($imgType ==2){
			$objAlbumImages->updateField(array('ai_caption'=>$ai_caption,'ai_edited'=>$editedTime,'ai_set_main'=>$set_cover),"ai_id=".$hid_img_id);
		}else if($imgType == 8){
			include_once(DIR_ROOT."class/polaroids.php");
			$objPolaroids			   =	new polaroids();
			$img_order	 		  =	$objCommon->esc($_GET['img_order']);
			$objPolaroids->updateField(array('polo_descr'=>$ai_caption,'polo_edited'=>$editedTime,'polo_order'=>$img_order),"polo_id=".$hid_img_id);
		}else if($imgType == 6){
			echo 'upi_edited'.$editedTime.'----upi_status'.$set_cover.'---upi_id'.$hid_img_id;
			include_once(DIR_ROOT."class/user_profile_image.php");
			$objUserProfileImg		  =	new user_profile_image();
			if($set_cover=='1'){
				$objUserProfileImg->updateField(array('upi_status'=>0),"user_id=".$userId);
				$upi_status	=	1;
			}else{
				$upi_status	=	0;
			}
			$objUserProfileImg->updateField(array('upi_edited'=>$editedTime,'upi_status'=>$upi_status),"upi_id=".$hid_img_id);
		}
	}
}
echo 'end';
?>