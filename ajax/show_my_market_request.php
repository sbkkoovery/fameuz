<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/market_place_booked.php");
	$objCommon				   =	new common();
	$ObjMarBooked				=	new market_place_booked();
	$marketplace				 =	$objCommon->esc($_GET['marketplace']);
	$userId					  =	$_SESSION['userId'];
	if($marketplace != ''  && $userId != ''){
		$getBookStatus		   =	$ObjMarBooked->getRowSql("select mpb.mpb_id,mpb.mpb_date,mpb.mpb_msg,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name
																FROM market_place_booked AS mpb
																LEFT JOIN users AS user ON mpb.user_id = user.user_id 
																LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
																LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
																LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
																LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
																WHERE mpb.mpb_id = ".$marketplace." AND  user.status=1 AND user.email_validation=1");
	  $userMarketImage			=($getBookStatus['upi_img_url'])?$getBookStatus['upi_img_url']:'profile_pic.jpg';
?>
	<div class="user-post-info-ad">
		<div class="img-section">
			<img class="img-responsive" src="<?php echo SITE_ROOT?>uploads/profile_images/<?php echo $userMarketImage?>" />
		</div>
		<div class="pot-user-info">
			<p><?php echo $objCommon->displayName($getBookStatus);?></p>
			<p class="profesn"><span class="proffesion"></span><?php echo $getBookStatus['c_name']?></p>
		</div>
		<div class="desc">
			<?php echo $objCommon->html2text($getBookStatus['mpb_msg']);?>
		</div>
	</div>
	<?php
	}
}
?>