<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/game_like.php");
	$objCommon				   =	new common();
	$ObjGameLike				 =	new game_like();
	$game_id					 =	$objCommon->esc($_POST['game_id']);
	$userId					  =	$_SESSION['userId'];
	if($game_id != '' && $userId != ''){
		$_POST['g_id']   		   =	$game_id;
		$_POST['user_id']   		=	$userId;
		$_POST['gl_created']	 =	date("Y-m-d H:i:s");
		$getLikes				=	$ObjGameLike->getRow("g_id=".$game_id." and user_id =".$userId);
		if($getLikes['gl_id']==''){
			$_POST['gl_status']  =	1;
			$ObjGameLike->insert($_POST);
			echo '<a href="javascript:;" data-like="'.$game_id.'" class="like_btn liked-game"><i class="fa fa-heart"></i><span> Liked</span></a>';
		}else{
			if($getLikes['gl_status']==1){
				$upStatus		=	0;
				echo '<a href="javascript:;" data-like="'.$game_id.'" class="like_btn"><i class="fa fa-heart"></i><span> Like</span></a>';
			}else{
				$upStatus		=	1;
				echo '<a href="javascript:;" data-like="'.$game_id.'" class="like_btn liked-game"><i class="fa fa-heart"></i><span> Liked</span></a>';
			}
			$ObjGameLike->updateField(array("gl_status"=>$upStatus),"gl_id=".$getLikes['gl_id']);
		}
	}
}
?>