<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/likes.php");
$objCommon				   =	new common();
$ObjLikes					=	new likes();
$id					  	  =	$objCommon->esc($_GET['id']);
$type					    =	$objCommon->esc($_GET['type']);
$userId					  =	$_SESSION['userId'];
$getLikeCount			    =	$ObjLikes->getRowSql("SELECT count(like_id) AS likeCount,group_concat(like_user_id) AS youLike FROM likes WHERE like_content =".$id." AND like_cat =".$type." AND like_status=1");
$likeCount				   =	$getLikeCount['likeCount'];
$youLikeArr	   			  =	explode(",",$getLikeCount['youLike']);
if(count($youLikeArr)>0){
	if(in_array($_SESSION['userId'],$youLikeArr)){
		$youLike	  		 =	1;
	}else{
		$youLike	  		 =	0;
	}
}
if($youLike ==1 && $likeCount	==1){
	$likeStr	  			 =	'You like this.';
}else if($youLike ==1 && $likeCount>1){
	$likeStr	  			 =	'You and other '.($likeCount-1).' people like this.';
}else if($youLike ==0 && $likeCount>0){
	$likeStr	  			=	$likeCount.' people like this.';
}else{
	$likeStr	  =	'';
}
echo $likeStr;
?>