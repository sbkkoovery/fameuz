<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/music_like.php");
	$objCommon				   =	new common();
	$objMusicLike				=	new music_like();
	$musicId					 =	$objCommon->esc($_POST['musicId']);
	$userId					  =	$_SESSION['userId'];
	if($musicId != '' && $userId != ''){
		$_POST['music_id']   	   =	$musicId;
		$_POST['user_id']   		=	$userId;
		$_POST['ml_created']	 =	date("Y-m-d H:i:s");
		$getLikes				=	$objMusicLike->getRow("music_id=".$musicId." and user_id =".$userId);
		if($getLikes['ml_id']==''){
			$_POST['ml_status']  =	1;
			$objMusicLike->insert($_POST);
			echo '<a href="javascript:;" data-like="'.$musicId.'" class="share_heart1 likeVideo liked"><i class="fa fa-heart"></i><span> Liked</span></a>';
		}else{
			if($getLikes['ml_status']==1){
				$upStatus		=	0;
				echo '<a href="javascript:;" data-like="'.$musicId.'" class="share_heart1 likeVideo"><i class="fa fa-heart"></i><span> Like</span></a>';
			}else{
				$upStatus		=	1;
				echo '<a href="javascript:;" data-like="'.$musicId.'" class="share_heart1 likeVideo liked"><i class="fa fa-heart"></i><span> Liked</span></a>';
			}
			$objMusicLike->updateField(array("ml_status"=>$upStatus),"ml_id=".$getLikes['ml_id']);
		}
	}
}
?>