<?php 
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/users.php");
$objCommon			   =	new common();
$objUser			  	 =	new users();
$getUserDetails	  	  =	$objUser->getRowSql("SELECT profileImg.upi_img_url,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 WHERE user.status=1 AND user.user_id=".$_SESSION['userId']);
if(isset($_GET['ecrId'])){
	$screenHeight		=	$objCommon->esc($_GET['screenHeight']);
	$screenWidth	 	 =	$objCommon->esc($_GET['screenWidth']);
	$ecrId		 	   =	$objCommon->esc($_GET['ecrId']);
	$videoDetails    	=	$objUser->getRowSql("SELECT vid.video_id,vid.video_url,video_encr_id,vid.video_thumb,vid.video_duration,vid.video_type,vid.video_title,vid.video_visits,vid.video_created,vid.video_privacy,vid.video_descr,vid.video_tags,user.first_name,user.user_id,user.last_name,user.display_name,user.email,social.usl_fameuz,profileImg.upi_img_url
										FROM videos AS vid
										LEFT JOIN users AS user ON  vid.user_id = user.user_id
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										WHERE vid.video_status=1 AND user.status=1 AND user.email_validation =1 AND vid.video_encr_id='".$ecrId."'");
if($videoDetails['video_url'] != ''){
//---------------------------------------get videos for sliders--------------
$getOtherVideosSql					=	"select group_concat(vid.video_encr_id order by video_created desc) AS vidUrls from videos as vid  where user_id=".$videoDetails['user_id']." and video_status = 1  order by video_created desc";
$getOtherVideo						=	$objUser->getRowSql($getOtherVideosSql);
$videosArr							=	$getOtherVideo['vidUrls'];
$videoArrExpl						 =	explode(",",$videosArr);
$videoArrExpl						 =	array_filter($videoArrExpl);
if(count($videoArrExpl)>0){
	$keyCurrent					   =	array_search($ecrId,$videoArrExpl);
	$keyNext						  =	$keyCurrent+1;
	$keyPrev						  =	$keyCurrent-1;
	if(array_key_exists($keyNext,$videoArrExpl)){
		$nextValue					=	$videoArrExpl[$keyNext];
	}else{
		$nextValue					=	'';
	}
	if(array_key_exists($keyPrev,$videoArrExpl)){
		$prevValue					=	$videoArrExpl[$keyPrev];
	}else{
		$prevValue					=	'';
	}
}
//--------------------------------------------------------------------------------  
$imageWidth		 	=	500;
$imageHeight		   =	300;
$boxWidth		   	  =	$screenWidth-100;
$boxHeight		  	 =	$screenHeight-50;
$rightSide		     =	290;
$maxWidth		   	  =	($boxWidth-$rightSide);
$imageHeight		   =	($imageHeight>$screenHeight)?($boxHeight):$imageHeight;
$dummyHeight		   =	($imageHeight>545)?$imageHeight:545;
$marginTop		  	 =	($screenHeight-$dummyHeight)/2;
$imageMarginTop	 	=	0;
if($imageHeight<545){
	$imageMarginTop	=	(545-$imageHeight)/2;
}
if($imageWidth<$maxWidth){
	$maxWidth		  =	$imageWidth	=	($imageWidth>530)?$imageWidth:530;
	$boxWidth		  =	$maxWidth+$rightSide;
}else{
	
}
$imageBoxWidth		 =	$boxWidth-$rightSide;
$sideHalf			  =	300;
$getLikeCount		  =	$objUser->getRowSql("SELECT count(like_id) AS likeCount,group_concat(like_user_id) AS youLike FROM likes WHERE like_content =".$videoDetails['video_id']." AND like_cat = 4 AND like_status=1");
?>
<div class="image_comment_load">
<div class="alert alertClose alert-success alert-dismissible" role="alert" style="display:none; margin-bottom:0px;"><button type="button" class="close" data-dismiss="alertClose" aria-label="Close"><span aria-hidden="true">&times;</span></button> Updated</div>
					<div class="comments">
						<form class="img_pop_form" id="img_pop_form" action="<?php echo SITE_ROOT?>ajax/video_settings.php" method="post">
							<div class="form-group">
								<label for="img_descr">Video Title</label>
								<textarea class="form-control" rows="1" name="img_title" id="img_title" placeholder="Say something about...."><?php echo $objCommon->html2textarea($videoDetails['video_title'])?></textarea>
								<input type="hidden"  value="<?php echo $videoDetails['video_id']?>" name="hidImgId" class="hidImgId" />
							</div>
							<div class="form-group">
								<label for="img_descr">Description</label>
								<textarea class="form-control" rows="1" name="img_descr" id="img_descr" placeholder="video description here...."><?php echo $objCommon->html2textarea($videoDetails['video_descr'])?></textarea>
							</div>
							<div class="form-group">
								<label for="img_descr">Video Tags</label>
								<input type="text" class="form-control"  name="video_tags" id="video_tags" placeholder="Add related tags (separated by commas, please!)" value="<?php echo $objCommon->html2text($videoDetails['video_tags'])?>">
							</div>
							<div class="form-group">
								<label for="img_descr">Privacy Settings</label>
								<select class="form-control" name="privacy" id="privacy">
									<option value="1" <?php echo ($videoDetails['video_privacy']=='1,0,0,0')?'selected="selected"':''?> >Public</option>
									<option value="2" <?php echo ($videoDetails['video_privacy']=='0,1,0,0')?'selected="selected"':''?> >Friends</option>
									<option value="4" <?php echo ($videoDetails['video_privacy']=='0,0,0,1')?'selected="selected"':''?> >Only for me</option>
								</select>
							</div>
							<button type="button" class="btn btn-default btn-sm" id="post_img_descr" onclick="sendFrm(this);">save</button>
						</form>
					</div>
					<div class="remove_img pull-right"><a href="javascript:;" onclick="delImg('<?php echo $videoDetails['video_id']?>');" class="delPopImg">Remove video</a></div>
				</div>

<!--<script src="<?php echo SITE_ROOT?>jw_player/jwplayer.js"></script>
//<link href="<?php echo SITE_ROOT?>jw_player/jw_player.css" rel="stylesheet" type="text/css">
//        <div class="cd-popup-container" style="width:<?php echo $boxWidth; ?>px; margin-top:<?php echo $marginTop.'px'; ?>">
//            <div class="right_side">
//                <?php /*?><div class="logo"><img src="<?php echo SITE_ROOT?>images/logo.png" alt="logo"/></div><?php */?>
//				<div class="imgUserBox">
//					<span class="prof_pic"><img alt="pro-thumb" src="<?php echo SITE_ROOT?>uploads/profile_images/<?php echo($videoDetails['upi_img_url'])?$videoDetails['upi_img_url']:'profile_pic.jpg'?>"></span>
//					<span class="commnt_content">
//						<span class="name"><?php echo $objCommon->displayName($videoDetails);?></span>
//						<span class="text">Added: <?php echo date("d M , Y h:i a",strtotime($objCommon->local_time($videoDetails['video_created'])));?></span>-->
						<?php
//							$likeCount		=	$getLikeCount['likeCount'];
//							$youLikeArr	   =	explode(",",$getLikeCount['youLike']);
//							if(count($youLikeArr)>0){
//								if(in_array($_SESSION['userId'],$youLikeArr)){
//									$youLike	  =	1;
//								}else{
//									$youLike	  =	0;
//								}
//							}
//							if($youLike ==1 && $likeCount	==1){
//								$likeStr	  =	'You like this.';
//							}else if($youLike ==1 && $likeCount>1){
//								$likeStr	  =	'You and other '.($likeCount-1).' people like this.';
//							}else if($youLike ==0 && $likeCount>0){
//								$likeStr	  =	$likeCount.' people like this.';
//							}else{
//								$likeStr	  =	'';
//							}
//						?>
						<?php /*?><span class="text likestr"><?php echo $likeStr;?></span><?php */?>
				<?php /*?>	</span>
				</div><?php */?>
				
          <?php /*?>  </div><?php */?>
            
            <?php /*?><div class="left_side" style="width:<?php echo $maxWidth; ?>px">
                <div class="slider_big_box">
                    <div class="big_image"  style="padding-top:<?php echo $imageMarginTop."px"; ?>">
						<?php
						if($videoDetails['video_type']==1){
							$getAiImages				=	SITE_ROOT.'uploads/videos/'.$videoDetails['video_thumb'];
							$vidUrl					 =	SITE_ROOT.'uploads/videos/'.$videoDetails['video_url'];
						}else if($videoDetails['video_type']==2){
							if (preg_match('%^https?://[^\s]+$%',$getVideoDetails['video_thumb'])) {
								$getAiImages			=	$getVideoDetails['video_thumb'];
							} else {
								$getAiImages			=	SITE_ROOT.'uploads/videos/'.$videoDetails['video_thumb'];
							}
							$vidUrl					 =	$objCommon->html2text($videoDetails['video_url']);
						}
						?>
						<div id="thePlayer"></div>
						<script type="text/javascript">
							jwplayer("thePlayer").setup({
								flashplayer: "player.swf",
								 image: "<?php echo $getAiImages?>",
								file: "<?php echo $vidUrl?>",
								skin: "<?php echo SITE_ROOT?>jw_player/six/six.xml",
								width: "100%",
								height: "<?php echo $imageHeight?>",
								stretching:"exactfit"
							});
						</script>
                        <div class="arrow_box">
							<?php
							if($prevValue !=''){
							?>
                            <a href="javascript:void(0);" class="left_arrow" onclick="photoPopUpVideo('<?php echo $prevValue?>');"><img src="<?php echo SITE_ROOT?>images/slider_arrow_left.png" /></a>
							<?php
							}
							if($nextValue !=''){
							?>
                            <a href="javascript:void(0);" class="right_arrow" onclick="photoPopUpVideo('<?php echo $nextValue?>');"><img src="<?php echo SITE_ROOT?>images/slider_arrow_right.png" /></a>
							<?php
							}
							?>
                        </div>
                    </div>
                </div>
                <div class="share_like_box">
                    <div class="like">
						<a class="<?php echo ($youLike==1)?'liked':''?> like_btn" data-like="<?php echo $videoDetails['video_id']?>" href="javascript:;"><i class="fa fa-heart"></i> <?php echo ($youLike==1)?'Liked':'Like'?></a>
                    </div>
                    <div class="share">
                        <a href="#"><i class="fa fa-share-alt"></i>Share </a>
                    </div>
                </div>
            </div><?php */?>
           <?php /*?> <div class="clr"></div>
            <div class="close_popup"><img src="<?php echo SITE_ROOT?>images/close_new01.png" alt="close"></div>
        </div><?php */?>
<?php } }?>
<script type="text/javascript">
//$('.like').on('click','.like_btn',function(){
//	var likeId		=	$(this).data('like');
//	var likecat	   =	4;
//	var imgUserId	 =	'<?php echo $videoDetails['user_id']?>';
//	var that		  =	this;
//	if(likeId !='' && likecat != ''){
//		$.ajax({
//			url:"<?php echo SITE_ROOT?>ajax/like_video.php",
//			method: "POST",
//			data:{likeId:likeId,likecat:likecat,imgUserId:imgUserId},
//			success:function(result){
//				$(that).parent().html(result);
//				$(".likestr").load('<?php echo SITE_ROOT?>ajax/like_count.php?type='+likecat+'&id='+likeId);
//		}});
//	}
//});
function sendFrm(e){
	var hidImgId=$(e).parent().children().children(".hidImgId").val();
	var img_title=$(e).parent().children().children("#img_title").val();
	var img_descr=$(e).parent().children().children("#img_descr").val();
	var video_tags=$(e).parent().children().children("#video_tags").val();
	var privacy=$(e).parent().children().children("#privacy").val();
	if(hidImgId){
		$.ajax(
    	{
			url : '<?php echo SITE_ROOT?>ajax/video_settings.php',
			type: "GET",
			data : {img_title:img_title,img_descr:img_descr,video_tags:video_tags,privacy:privacy,hidImgId:hidImgId},
			success:function(data, textStatus, jqXHR) 
			{
				$( ".alertClose" ).show().delay( 4000 ).slideUp( 400 );
			}
    	});
	}	
}
function delImg(imgid){
	$.confirm({
		'title'	  : 'Delete Confirmation',
		'message'	: 'You are about to delete this item ?',
		'buttons'	: {
		'Yes'		: {
		'class'	  : 'blue',
		'action': function(){
			$("#pop_up").fadeOut(300,function(){
				$("#pop_up").html("");
				$(".cd-popup-container").hide();
			});
			$(".load_album_content").html('<div class="load_album_preloader"></div>');
			$.ajax({
				url:'<?php echo SITE_ROOT?>ajax/delete_video_album.php',
				data:{imgid:imgid},
				type:"GET",
				success: function(){
					$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/album_video_list.php');
					$(".load_album_preloader").hide();
					
				}
			});
		}
		},
		'No'	: {
		'class'	: 'gray',
		'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
		}
		}
	});
}
</script>