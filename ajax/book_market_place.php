<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/market_place_booked.php");
	$objCommon				   =	new common();
	$ObjMarBooked				=	new market_place_booked();
	$adId					  	=	$objCommon->esc($_POST['adId']);
	$userId					  =	$_SESSION['userId'];
	if($adId != ''  && $userId != ''){
		$_POST['mp_id']   		  =	$adId;
		$_POST['user_id']   		=	$userId;
		$_POST['mpb_date']	   =	date("Y-m-d H:i:s");
		$_POST['mpb_msg']		=	$objCommon->esc($_POST['request_msg']);
		$getContent			  =	$ObjMarBooked->getRow("mp_id=".$adId." and user_id =".$userId ." ");
		$getMarket			   =	$ObjMarBooked->getRowSql("SELECT user_id FROM market_place WHERE mp_id = ".$adId);
		if($getContent['mpb_id']==''){
			$ObjMarBooked->insert($_POST);
			//----notification table------------------------------------
			$myDetails				   		   	   =	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$userId);
			$displayName						  	 =	$objCommon->displayName($myDetails);
			$friend_id							   =	$getMarket['user_id'];
			$notiType								=	'market place book request';
			$notiImg								 =	'';
			$notiDescr   	 	 					   =	'<b>'.$displayName.'</b> sent a book request for your <b>marketplace</b>.';
			$notiUrl  								 =	SITE_ROOT.'user/my-market-place?show='.$adId;
			$objCommon->pushNotification($friend_id,$userId,$notiType,$notiImg,$notiDescr,$notiUrl);
			//----------------------------------------------------------
		}
	}
}
?>