<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/user_profile_image.php");
	$objCommon				   =	new common();
	$objUserProfileImg		   =	new user_profile_image();
	$imgid				  	   =	$objCommon->esc($_GET['imgid']);
	$userId					  =	$_SESSION['userId'];
	if($imgid !='' && $userId!=''){
		$getImageName			=	$objUserProfileImg->getRow("upi_id=".$imgid);
		$explAiImages	   		=	explode("/",$getImageName['upi_img_url']);
		$getAiImag1		  	  =	$explAiImages[0].'/thumb/'.$explAiImages[1];
		$getAiImag2		  	  =	$explAiImages[0].'/original/'.$explAiImages[1];
		$imgPath				 =	DIR_ROOT.'uploads/profile_images/'.$getImageName['photo_url'];
		$imgPath1				=	DIR_ROOT.'uploads/profile_images/'.$getAiImag1;
		$imgPath2				=	DIR_ROOT.'uploads/profile_images/'.$getAiImag2;
		unlink($imgPath);
		unlink($imgPath1);
		unlink($imgPath2);
		$objUserProfileImg->delete("upi_id=".$imgid);
	}
}
?>