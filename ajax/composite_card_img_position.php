<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/composite_card_img.php");
$objCommon				 =	new common();
$objCompoCardImg		   =	new composite_card_img();
$imgPos					=	$objCommon->esc($_GET['imgPos']);
$imgId					 =	$objCommon->esc($_GET['imgId']);
$userId				    =	$_SESSION['userId'];
if($userId != ''){
	if($imgPos != '' && $imgId != ''){
		$_POST['cci_pos'.$imgPos]	=	$imgId;
		$_POST['cci_created']		=	date("Y-m-d H:i:s");
		$getComp					 =	$objCompoCardImg->count("user_id=".$userId);
		if($getComp >0){
			$objCompoCardImg->update($_POST,"user_id=".$userId);
		}else{
			$_POST['user_id']		=	$userId;
			$objCompoCardImg->insert($_POST);
		}
	}
}else{
	exit;
}
$getCompoImgDetails			  =	$objCompoCardImg->getRow("user_id=".$userId);
$posImg1						 =	($getCompoImgDetails['cci_pos1'])?'uploads/albums/'.$objCommon->html2text($getCompoImgDetails['cci_pos1']):'images/composite-img.jpg';
$posImg2						 =	($getCompoImgDetails['cci_pos2'])?'uploads/albums/'.$objCommon->html2text($getCompoImgDetails['cci_pos2']):'images/compo-img1.jpg';
$posImg3						 =	($getCompoImgDetails['cci_pos3'])?'uploads/albums/'.$objCommon->html2text($getCompoImgDetails['cci_pos3']):'images/compo-img-6.jpg';
$posImg4						 =	($getCompoImgDetails['cci_pos4'])?'uploads/albums/'.$objCommon->html2text($getCompoImgDetails['cci_pos4']):'images/compo-img-3.jpg';
?>

      <div class="modal-body">
        <div class="img-sec">
        	<div class="row">
            	<div class="col-sm-6">
                	<a href="javascript:;" data-toggle="modal" data-target=".slectPhoto" data-position="1" class="selCompositePhoto" style="height:384px; width:269px; display:block; overflow:hidden;">
                        <div class="ovelay_image_indicator"><p><i class="fa fa-upload"></i>Click here to upload</p></div>
                        <img class="img-responsive" src="<?php echo SITE_ROOT_AWS_IMAGES.$posImg1?>" />
					</a>
                </div>
            	<div class="col-sm-6">
                	<div class="row">
                    	<div class="col-sm-6">
                        	<a href="javascript:;" data-toggle="modal" data-target=".slectPhoto" data-position="2" style="height:190px; width:100%" class="selCompositePhoto">
                                <div class="ovelay_image_indicator"><p><i class="fa fa-upload"></i>Click here to upload</p></div>
                                <img class="img-responsive" src="<?php echo SITE_ROOT_AWS_IMAGES.$posImg2?>" />
                            </a>
                        </div>
                    	<div class="col-sm-6">
                        	<a href="javascript:;" data-toggle="modal" data-target=".slectPhoto" data-position="3" style="height:190px; width:100%" class="selCompositePhoto">
                                <div class="ovelay_image_indicator"><p><i class="fa fa-upload"></i>Click here to upload</p></div>
                                <img class="img-responsive" src="<?php echo SITE_ROOT_AWS_IMAGES.$posImg3?>" />
                            </a>
                        </div>
                    </div>
                    <div class="row">
                    	<div class="col-sm-6">
                            <div class="right-btm-img">
                                <img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/dummy-info.jpg" />
                            </div>
                        </div>
                    	<div class="col-sm-6">
                        	<div class="right-btm-img">
                                <a href="javascript:;" data-toggle="modal" data-target=".slectPhoto" data-position="4" style="height:190px; width:100%" class="selCompositePhoto">
                                     <div class="ovelay_image_indicator"><p><i class="fa fa-upload"></i>Click here to upload</p></div>
                                    <img class="img-responsive" src="<?php echo SITE_ROOT_AWS_IMAGES.$posImg4?>" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary saveChange">Save Changes</button>
      </div>
<script>
$(".selCompositePhoto").on("click",function(){
	var ImgPos	=	$(this).data("position");
	$.ajax({
		url:'<?php echo SITE_ROOT?>ajax/composite_card_img_select.php',
		type:"GET",
		data:{ImgPos:ImgPos},
		success:function(data){
			$(".modalSelImg").html(data);
		}
	});
});
$(".saveChange").on("click",function(){
	window.location.href	=	'<?php echo SITE_ROOT?>user/composite-card';
});
</script>