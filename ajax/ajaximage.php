<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/album_images.php");
include(DIR_ROOT."ajax/resize-class.php");
include(DIR_ROOT."ajax/face_crop.php");
$objCommon				   =	new common();
$path 						= 	$_GET['path'];
$albumId					 =	$objCommon->esc($_GET['albumId']);
$userId					  =	$_SESSION['userId'];
$objAlbumImages			  =	new album_images();
$valid_formats = array("jpg", "png", "gif", "bmp","jpeg","JPG", "PNG", "GIF", "BMP","JPEG");
$uploadStatus		=	0;
$imgStr			  =	'';
if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST" && $_FILES['file']['tmp_name'] !='')
{
	$todayDate		=	date('Y-m-d');
	if(!file_exists($path.$todayDate)){
		mkdir($path.$todayDate);
	}
	$path	=	$path.$todayDate.'/';
foreach($_FILES['file']['tmp_name'] as $i => $tmp_name ){ 
$name = $_FILES['file']['name'][$i];
$size = $_FILES['file']['size'][$i];
if(strlen($name))
{
	$ext	=	strtolower(pathinfo($name, PATHINFO_EXTENSION));
	if(in_array($ext,$valid_formats))
	{
	if($size<(3072*10240))
	{
	$randomDigit				 =	$objCommon->randStrGenDigit(4);
	$time						=	time();				
	$actual_image_name_ext_no	=	$time."_".$userId.'_'.$randomDigit;
	$actual_image_name 		   = 	$time."_".$userId.'_'.$randomDigit.".".$ext;
	
	if(!file_exists($path."original")){
		mkdir($path."original");
	}
	if(!file_exists($path."thumb")){
		mkdir($path."thumb");
	}
	$tmpName = $_FILES['file']['tmp_name'][$i];    
list($widthu, $heighut, $typeu, $attru) = getimagesize($tmpName); 
	if(move_uploaded_file($_FILES['file']['tmp_name'][$i], $path.'original/'.$actual_image_name)){
	
	$resizeObj = new resize($path.'original/'.$actual_image_name);
	//$resizeObj -> resizeImage(200, 200, 'exact');
	//$resizeObj -> saveImage($path.'thumb/'.$actual_image_name, 100);
	if($heighut >$widthu){
		if ($heighut > 700){
			$resizeObj -> resizeImage('', 700, 'auto');
			$resizeObj -> saveImage($path.$actual_image_name, 100);
		}else{
			$resizeObj -> resizeImage('', $heighut, 'auto');
			$resizeObj -> saveImage($path.$actual_image_name, 100);
		}
	}else{
		if ($widthu > 900){
			$resizeObj -> resizeImage(900, '', 'auto');
			$resizeObj -> saveImage($path.$actual_image_name, 100);
		}else{
			$resizeObj -> resizeImage($widthu,'', 'auto');
			$resizeObj -> saveImage($path.$actual_image_name, 100);
		}
	}
	//----------------face cropping----------------------------------------------
	if($heighut < 192 || $widthu <192){
		$resizeObj -> resizeImage($widthu, $heighut, 'exact');
		$resizeObj -> saveImage($path.'thumb/'.$actual_image_name, 100);
	}else{
		cropImg($path.'original/'.$actual_image_name,$path.'thumb/',$actual_image_name_ext_no,'192','192');
	}
	//----------------face cropping----------------------------------------------
	//----------------start upload to AWS----------------------------------------
	require(DIR_ROOT."amazone_s3/S3.php");
	$s3			=	new S3(AWS_ID,AWS_KEY);
	S3::putObjectFile($path.$actual_image_name,AWS_IMG_BUCKET,'uploads/albums/'.$todayDate.'/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
	S3::putObjectFile($path.'thumb/'.$actual_image_name,AWS_IMG_BUCKET,'uploads/albums/'.$todayDate.'/thumb/'.$actual_image_name,S3::ACL_PUBLIC_READ,array());
	//----------------end upload to AWS-------------------------------------------
	$uploadStatus				=	1;
	$_POST['a_id']			   =	$albumId;
	$_POST['user_id']			=	$userId;
	$_POST['ai_images']		  =	$todayDate.'/'.$actual_image_name;
	$_POST['ai_created']		 =	date("Y-m-d H:i:s");
	$_POST['ai_edited']		  =	date("Y-m-d H:i:s");
	$objAlbumImages->insert($_POST);
	$lastAlbImgId				=	$objAlbumImages->insertId();
	$arrayResponse			   =	array("imgId"=>$lastAlbImgId);	
	//$imgStr					 .=	'<tr><td><img src="'.SITE_ROOT.'uploads/albums/'.$todayDate.'/thumb/'.$actual_image_name.'" class="img-responsive" width="50" height="60"/></td><td><textarea class="form-control" rows="1" name="img_descr[]"></textarea><input type="hidden" name="hid_img_id[]" value="'.$lastAlbImgId.'"/></td></tr>'; 
	 }
}
}
//else
//echo '<font color="#CC0000">Invalid file format..</font>'; 
}
//else
//echo '<font color="#CC0000">Please select image..!</font>';
	}
if($uploadStatus==1){
	echo $jsonResponse		=	json_encode($arrayResponse);
}
	//exit;

}
$hiddenIdDz				=	$_POST['hiddenIdDz'];
if(count($hiddenIdDz)>0){
	foreach($hiddenIdDz as $allhid){
		$ai_caption	=	$objCommon->esc($_POST['dZdesc_'.$allhid]);
		$objAlbumImages->updateField(array('ai_caption'=>$ai_caption),"ai_id=".$allhid);
	}
}
?>