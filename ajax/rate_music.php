<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/music_vote.php");
	include_once(DIR_ROOT."class/notifications.php");
	$objCommon				   =	new common();
	$ObjMusicVote			    =	new music_vote();
	$objNotifications			=	new notifications();
	$rate_val					=	$objCommon->esc($_POST['rate_val']);
	$vidId					   =	$objCommon->esc($_POST['vidId']);
	$userId					  =	$_SESSION['userId'];
	if($rate_val != '' && $vidId != '' && $userId != ''){
		$_POST['music_id']   	   =	$vidId;
		$_POST['user_id']   		=	$userId;
		$_POST['mv_vote']   		=	$rate_val;
		$_POST['mv_created']	 =	date("Y-m-d H:i:s");
		$getVotes				=	$ObjMusicVote->getRow("music_id=".$vidId." and user_id =".$userId);
		if($getVotes['mv_id'] == ''){
			$ObjMusicVote->insert($_POST);
		}
	}
	
}
?>