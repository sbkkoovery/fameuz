<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/reminders.php");
$objCommon	  =	new common();
$objReminder	=	new reminders();
$userId				   =	$_SESSION['userId'];
if(isset($_GET['remindId'])){
$remindId	=	$objCommon->esc($_GET['remindId']);
$remindDetails	=	$objReminder->getRow("remind_id=$remindId AND user_id=$userId");
if(count($remindDetails)>1){
?>
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Create Private Event</h4>
  </div>
  <form action="<?php echo SITE_ROOT.'access/create_reminder.php' ?>" method="post">
  <input type="hidden" name="remindId" value="<?php echo $remindId; ?>" />
  <div class="modal-body">
        <div class="form-group">
        <label>Name</label>
            <input type="text" name="remind_title" value="<?php echo $objCommon->html2text($remindDetails['remind_title']); ?>" class="form-control" placeholder="Reminder Title">
        </div>
        <div class="form-group">
        <label>Details</label>
            <textarea class="form-control" name="remind_description" placeholder="Add more info" rows="4"><?php echo $objCommon->html2textarea($remindDetails['remind_description']); ?></textarea>
        </div>
        <div class="form-group">
        <label>Where</label>
            <input type="text" class="form-control background-img addPlace" value="<?php echo $objCommon->html2text($remindDetails['remind_place']); ?>" name="remind_place" placeholder="Add a place" name="remind_date" onFocus="geolocate()" autocomplete="off"  id="locality"  value="" />
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>When</label>
                    <input type="text" name="remind_date" value="<?php echo date("m/d/Y",strtotime($remindDetails['remind_date'])); ?>" class="form-control background-img when" id="remindDate" placeholder="Add Date">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Time</label>
                    <input type="text" name="remind_time" value="<?php echo date("H:i",strtotime($remindDetails['remind_time'])); ?>" class="form-control background-img time" placeholder="Add Time">
                </div>
            </div>
        </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    <button type="submit" class="btn btn-primary">Update</button>
  </div>
    </form>
<?php }}?>