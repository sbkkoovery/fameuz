<?php
	include_once(DIR_ROOT."class/search_functions.php");
	$obj_search_functions		=	new search_functions();
	$perPage 				   	 = 	20;
	$page 					  	= 	1;
	$search_keyword			  =	$objCommon->esc($_GET['filter_keywords']);
	$search_type			  	 =	$objCommon->esc($_GET['filter_category']);
	if($search_type){
		$sqlFilterType		   =	" AND b.bc_id=".$search_type." ";
	}else{
		$sqlFilterType		   =	"";
	}
	if($search_keyword){	
	$filterSearchArr			=	$obj_search_functions->filterSearchKeys($search_keyword);
	$scoreFullTitle 			 = 	6;
	$scoreTitleKeyword 		  = 	5;
	$titleSQL				   =	array();
	$aliasSQL				   =	array();
	foreach($filterSearchArr as $searchArr){
			$titleSQL[] 		 = 	"if (blog_title LIKE '%".$searchArr."%',{$scoreFullTitle},0)";
			$aliasSQL[] 		 = 	"if (blog_alias LIKE '%".$searchArr."%',{$scoreTitleKeyword},0)";
			$sqlMemberSearch	=	"SELECT b.*,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,group_concat(l.user_id) AS likeUser,count(comment_id) AS commentCount,bc.bc_title,
				(
					(
					".implode(" + ", $titleSQL)."
					)+
					(
					".implode(" + ", $aliasSQL)."
					)
				) as relevance
										FROM blogs AS b
										LEFT JOIN blog_category AS bc ON b.bc_id = bc.bc_id   
										LEFT JOIN users AS user ON b.user_id = user.user_id
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN blogs_like AS l ON b.blog_id = l.blog_id AND l.bl_status = 1
										LEFT JOIN  comments ON b.blog_id=comments.comment_content AND comments.comment_cat=7
										WHERE user.status=1 AND user.email_validation=1  AND b.blog_status=1 ".$sqlFilterType." GROUP BY b.blog_id   HAVING relevance > 0 ORDER BY relevance DESC, b.created_time desc";
	}

	}else{
		$sqlMemberSearch		  =	"SELECT b.*,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,group_concat(l.user_id) AS likeUser,count(comment_id) AS commentCount,bc.bc_title
										FROM blogs AS b 
										LEFT JOIN blog_category AS bc ON b.bc_id = bc.bc_id 
										LEFT JOIN users AS user ON b.user_id = user.user_id
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN blogs_like AS l ON b.blog_id = l.blog_id AND l.bl_status = 1
										LEFT JOIN  comments ON b.blog_id=comments.comment_content AND comments.comment_cat=7
										WHERE user.status=1 AND user.email_validation=1  AND b.blog_status=1 ".$sqlFilterType." GROUP BY b.blog_id  ORDER BY b.created_time desc";
	}
$start 						   =	0;
$queryMemberSearch 		   	   =	$sqlMemberSearch . " limit " . $start . "," . $perPage; 
if(empty($_GET["rowcount"])) {
	$_GET["rowcount"] 		    =	$countSearchMembers	=	$objUsers->countRows($sqlMemberSearch);
}
$pages  = ceil($_GET["rowcount"]/$perPage);
$getMemberSearch			  	 =	$objUsers->listQuery($queryMemberSearch);
if($start < $countSearchMembers){
	?>
	<div class="row">
	<?php
	if(count($getMemberSearch)>0){
		echo '<input type="hidden" class="pagenum" value="' . $page . '" /><input type="hidden" class="total-page" value="' . $pages . '" /><input type="hidden" id="total-count" value="'.$countSearchMembers.'" />';
		foreach($getMemberSearch as $allBlogDetails){
			$likeUser					=	$allBlogDetails['likeUser'];
			$likeUserArr				 =	'';
			if($likeUser){
				$likeUserArr			 =	explode(",",$likeUser);
				$likeUserArr			 =	array_filter($likeUserArr);
				$likeCounts			  =	count($likeUserArr).' Likes';
			}else{
				$likeCounts			  =	 '0 Likes';
			}
	?>
		   <div class="col-sm-12 blogSearch">
				<div class="item">
                <div class="likeWidget">
							<div class="row">
								<div class="col-md-2"><span><i class="fa_heart_gery"></i> <?php echo $likeCounts?> </span></div>
								<div class="col-md-9"><span><i class="fa_comment_alt"></i> <?php echo $objCommon->html2text($allBlogDetails['commentCount'])?> Comments</span></div>
							</div>
						</div>
				<div class="row">
					<?php
					if($allBlogDetails['blog_img']){
					?>
						<div class="col-xs-12 col-sm-5 col-md-3">
						<a href="<?php echo SITE_ROOT.'blog/show/'.$objCommon->html2text($allBlogDetails['blog_alias']).'-'.$allBlogDetails['blog_id']?>" class="blogImg" style="background-image:url('<?php echo SITE_ROOT_AWS_IMAGES?>uploads/blogs/<?php echo $objCommon->html2text($allBlogDetails['blog_img'])?>')">
						</a>
						</div>
						<div class="col-xs-12 col-sm-7 col-md-9 BlogDescr">
					<?php
					}else{
						?>
						<div class="col-md-12 BlogDescr">
						<?php
					}
					?>
					<h2><a href="<?php echo SITE_ROOT.'blog/show/'.$objCommon->html2text($allBlogDetails['blog_alias']).'-'.$allBlogDetails['blog_id']?>"><?php echo $objCommon->html2text($allBlogDetails['blog_title'])?></a></h2>
                    <div class="row">
							<div class="col-sm-12 col-md-5">
								<p class="publishedBy"><i class="fa_publish"></i> <span class="publishedBySpan">Published By:</span> <a href="<?php echo SITE_ROOT.$objCommon->html2text($allBlogDetails['usl_fameuz'])?>"><?php echo $objCommon->displayName($allBlogDetails)?></a></p>
							</div>
							<div class="col-sm-12 col-md-4">
								<p class="publishedBy"><i class="fa fa-tag"></i> <span class="publishedBySpan">Category : </span> <?php echo $objCommon->html2text($allBlogDetails['bc_title'])?></p>
							</div>
                            <div class="col-sm-3">
								<p class="publishedBy"><i class="fa_clock"></i> <?php echo date("M, d, Y",strtotime($allBlogDetails['created_time']))?></p>
							</div>
						</div>
						<p class="desc-blog-s"><?php echo strip_tags($objCommon->limitWords($allBlogDetails['blog_descr'],150))?></p>
						
						<div class="row">
							
						</div>
						
					</div>
					</div>
				</div>
			</div>
<?php
		}
		echo '</div>';
	}
}else{
	echo '<div class="row"><div class="col-md-6"><p>No results found....</p></div></div>';
}
?>