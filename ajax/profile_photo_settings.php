<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/user_profile_image.php");
	include_once(DIR_ROOT."class/album_images.php");
	$objCommon				  =	new common();
	$objUserProfileImg		  =	new user_profile_image();
	$userId					 =	$_SESSION['userId'];
	if($_GET['hidImgId'] !=''  && $userId !=''){
		$hid_img_id			 =	$objCommon->esc($_GET['hidImgId']);
		$set_cover			  =	$objCommon->esc($_GET['set_cover']);
		$editedTime	         =	date('Y-m-d H:i:s');
		if($set_cover=='1'){
			$objUserProfileImg->updateField(array('upi_status'=>0),"user_id=".$userId);
		}
		$objUserProfileImg->updateField(array('upi_edited'=>$editedTime,'upi_status'=>$set_cover),"upi_id=".$hid_img_id);
		
	}
}
?>