<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
$objCommon				 =	new common();
$perPage 				   = 	14;
$page 					  = 	1;
$getUserDetails	  		=	$objUsers->getRowSql("SELECT ucat.uc_c_id,personal.p_city,personal.p_country FROM users AS user LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id LEFT JOIN personal_details as personal on user.user_id=personal.user_id WHERE user.status=1 AND user.user_id=".$_SESSION['userId']);
$getFollowers			  =	$objUsers->getRowSql("SELECT group_concat(user.user_id) AS users FROM users AS user LEFT JOIN following AS follow ON (user.user_id	=	follow.follow_user1 and follow.follow_user1 !='".$_SESSION['userId']."') or (user.user_id	=	follow.follow_user2 and follow.follow_user2 !='".$_SESSION['userId']."') WHERE (follow.follow_user2='".$_SESSION['userId']."' and follow.follow_status ='2') or follow.follow_user1='".$_SESSION['userId']."'");
$search					=	$objCommon->esc($_GET['search']);
if($search){
	$searchWhere		   =	" and ( user.first_name LIKE '%".$search."%' or  user.display_name LIKE '%".$search."%' or  user.email LIKE '%".$search."%' or  social.usl_fameuz LIKE '%".$search."%') ";
}else{
	$searchWhere		   =	" ";
}
$deafltSql				 =	$objUsers->getRowSql("SELECT group_concat(tab.users) as users FROM(
SELECT group_concat(user.user_id) AS users FROM users AS user LEFT JOIN following AS follow ON (user.user_id =	follow.follow_user1 and follow.follow_user1 !='".$_SESSION['userId']."') or (user.user_id	=	follow.follow_user2 and follow.follow_user2 !='".$_SESSION['userId']."') LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE (follow.follow_user2='".$_SESSION['userId']."' and follow.follow_status !='2') ".$searchWhere."
UNION ALL
SELECT group_concat(user.user_id) AS users FROM users AS user LEFT JOIN personal_details AS personal ON user.user_id=personal.user_id LEFT JOIN user_social_links AS social ON user.user_id=social.user_id  WHERE (personal.p_city LIKE '%".$getUserDetails['p_city']."%' OR personal.p_country LIKE '%".$getUserDetails['p_country']."%') AND personal.user_id !='".$_SESSION['userId']."' ".$searchWhere."
UNION ALL
SELECT group_concat(user.user_id) AS users FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN user_categories as ucat ON user.user_id	=	ucat.user_id WHERE ucat.uc_c_id='".$getUserDetails['uc_c_id']."' AND ucat.user_id !='".$_SESSION['userId']."' ".$searchWhere."
UNION ALL
SELECT group_concat(user.user_id) AS users FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE  user.user_id !='".$_SESSION['userId']."' ".$searchWhere."
) as tab");
if($deafltSql['users']){
	$getFollowerArr			=	array_unique(explode(',',$getFollowers['users']));
	$getDefaltArr			  =	array_unique(explode(',',$deafltSql['users']));
	$resulArr				  =	array_diff($getDefaltArr,$getFollowerArr);
	$strUser 				   =    implode(',',array_unique($resulArr));
	if($strUser){
	$sqlMyInvitors		=	"select user.user_id,user.first_name,user.last_name,user.display_name,profileImg.upi_img_url,user.email,social.usl_fameuz,case when (TIMESTAMPDIFF(MINUTE,chat.ucs_last_seen,'".date("Y-m-d H:i:s")."') >15 or chat.ucs_status=0) then '0' else '1' END AS chatStatus,personal.p_country,personal.p_city,cat.c_name
	from users as user 
	left join user_chat_status as chat on user.user_id = chat.user_id 
	LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
	LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
	LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN personal_details AS personal ON user.user_id=personal.user_id 
	LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
	where user.user_id IN (".$strUser.") AND user.email_validation=1 AND user.status=1 ORDER BY FIELD(user.user_id,".$strUser.") limit 30";
	$getMyFollowers		=	$objUsers->listQuery($sqlMyInvitors);
?>
<ul class="row">
									<?php
									if(count($getMyFollowers)>0){
										foreach($getMyFollowers as $keyFollowers=>$allFollowers){
											$allFollowersImg	=	SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.(($allFollowers['upi_img_url'])?$allFollowers['upi_img_url']:"profile_pic.jpg");
											$allFollowersName		 =	$objCommon->displayName($allFollowers);
											if($allFollowers['follow_status']==2){ 
												$friendStatus =2;
											}else if($allFollowers['follow_status']==1 && $allFollowers['follow_user1']==$_SESSION['userId']){ 
												$friendStatus =1; 
											}else { 
												$friendStatus=0; 
											}
											if($allFollowers['p_city'] != '' || $allFollowers['p_country'] != ''){
												$friendPcity	=	($allFollowers['p_city'])?$objCommon->html2text($allFollowers['p_city'])." ,":"";
											}
									?>
									<li class="col-md-6">
										<div class="visitor_inner_box">
											<div class="image_thumb pull-left">
												 <a href="<?php echo SITE_ROOT.$objCommon->html2text($allFollowers['usl_fameuz'])?>" title="<?php echo $allFollowersName?>"><img src="<?php echo $allFollowersImg?>" class="img-responsive" alt="<?php echo $allFollowersName?>" title="<?php echo $allFollowersName?>" /></a>
											</div>
											<div class="visitor_box">
												<p class="name_head"><a href="<?php echo SITE_ROOT.$objCommon->html2text($allFollowers['usl_fameuz'])?>" title="<?php echo $allFollowersName?>"><?php echo $allFollowersName?></a></p>
												<p class="proffesion"> <?php echo $objCommon->html2text($allFollowers['c_name'])?></p>
												<p><i class="fa fa-globe"></i> <?php echo $friendPcity.' '.$objCommon->limitWords($allFollowers['p_country'],15)?></p>
												<?php
												if($allFollowers['chatStatus']==1){
													echo '<p class="online_status">online</p>';
												}else{
													echo '<p class="offline_status">offline</p>';
												}
												?>
												<div class="follow_status_box">
													<div class="follow_status">
														<?php
														if($friendStatus==2){
														?>
														<a href="javascript:;" class="friend"><i class="fa fa-star-o"></i> Friend</a>
														<?php
														}else if($friendStatus==1){
														?>
														<a href="javascript:;" class="following"><i class="fa fa-check"></i> Following</a>
														<?php
														}else if($friendStatus==0){
														?>
														<a href="javascript:;" class="follow has-spinner" data-friendid="<?php echo $objCommon->html2text($allFollowers['user_id'])?>">
															<span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span>
															<i class="fa fa-plus"></i> Follow
														</a>
														<?php
														}
														?>
													</div>
												</div>
											</div>
                                             <div class="side_mark"></div>
                                             <div class="clearfix"></div>
										</div>
									</li>
									<?php
									if(($keyFollowers+1)%2==0){
											echo '<div class="clearfix "></div>';
									}
										}
									}
									
									?>
								</ul>
								<?php
									}else{
										echo '<p>No results found....</p>';
									}
								}else{
									echo '<p>No results found....</p>';
								}
								?>
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});
$('.follow').click(function(e) {
	var friendid		=	$(this).data('friendid');
	$(this).addClass('active');
	$(this).find('.fa-plus').hide();
	that	=	this;
	$.get('<?php echo SITE_ROOT?>ajax/sent_friend_request.php',{"friendId":friendid},function(data){
	  setTimeout(function () {
			$(that).removeClass('active');
			$(that).parent().html(data);
			
		},3000);
	});
});
</script>