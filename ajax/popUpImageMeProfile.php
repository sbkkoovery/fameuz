<?php 
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/user_profile_image.php");
$objCommon			   =	new common();
$objUserProfileImg	   =	new user_profile_image();
if(isset($_GET['imageId'])){
	$imageId		 	 =	$objCommon->esc($_GET['imageId']);
	if($imageId){
		$imageDetails    =	$objUserProfileImg->getRow("upi_id=".$imageId);
	}
$imageName			   =	SITE_ROOT."uploads/profile_images/".$imageDetails['upi_img_url'];
?>
<div class="image_comment_load">
<div class="alert alertClose alert-success alert-dismissible" role="alert" style="display:none; margin-bottom:0px;"><button type="button" class="close" data-dismiss="alertClose" aria-label="Close"><span aria-hidden="true">&times;</span></button> Updated</div>
		<div class="comments">
			<form class="img_pop_form" id="img_pop_form" action="<?php echo SITE_ROOT?>ajax/profile_photo_settings.php" method="post">
				<div class="form-group">
					<input type="hidden"  value="<?php echo $imageDetails['upi_id']?>" name="hidImgId" class="hidImgId" />
					<label><input type="checkbox" name="set_cover" id="set_cover" value="1" <?php echo ($imageDetails['upi_status']==1)?'checked="checked"':''?> > Set as profile image</label>
				</div>
				<button type="button" class="btn btn-default btn-sm" id="post_img_descr" onclick="sendFrm(this);">save</button>
			</form>
		</div>
		<div class="remove_img pull-right"><a href="javascript:;" onclick="delImg('<?php echo $imageDetails['upi_id']?>');" class="delPopImg">Remove Image</a></div>
</div>
<script type="text/javascript">
function sendFrm(e){
	var hidImgId=$(e).parent().children().children(".hidImgId").val();
	var set_cover;
	if($(e).parent().children().children().children("#set_cover").is(":checked")){
		set_cover	=1;
	}
	if(hidImgId){
		$.ajax({
				url:'<?php echo SITE_ROOT?>ajax/profile_photo_settings.php',
				data:{hidImgId:hidImgId,set_cover:set_cover},
				type:"GET",
				success: function(){
					$( ".alertClose" ).show().delay( 4000 ).slideUp( 400 );
				}
		});
	}
	
}
function delImg(imgid){
	$.confirm({
		'title'		: 'Delete Confirmation',
		'message'	: 'You are about to delete this item ?',
		'buttons'	: {
		'Yes'	: {
		'class'	: 'blue',
		'action': function(){
			$('.overlay_bg').hide();
			$('.user_data h3').text('');
			$('.user_data p').text('');
			jwplayer( 'vidSection' ).stop();
			$(".load_album_content").html('<div class="load_album_preloader"></div>');
			$.ajax({
				url:'<?php echo SITE_ROOT?>ajax/delete_img_profile.php',
				data:{imgid:imgid},
				type:"GET",
				success: function(){
					$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/profile_photo_page.php');
					$(".load_album_preloader").hide();
				}
			});
		}
		},
		'No'	: {
		'class'	: 'gray',
		'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
		}
		}
	});
}
</script>
<?php }?>