<?php
@session_start();
date_default_timezone_set('UTC');
ini_set('date.timezone', 'UTC');
//if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
//{
	function getIP() {
	  foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
		 if (array_key_exists($key, $_SERVER) === true) {
			foreach (explode(',', $_SERVER[$key]) as $ip) {
			   if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
				  return $ip;
			   }
			}
		 }
	  }
	}
	if(strlen(getIP() >6)){
		$ipUser  =	getIP();
	}else{
		$ipUser  =	'83.110.193.65';
	}
	$json		=	file_get_contents('http://getcitydetails.geobytes.com/GetCityDetails?fqcn='.$ipUser); 
	$data		=	json_decode($json);
	$t 		   =	explode(":",$data->geobytestimezone); 
	$h 		   =	$t[0]; 
	if(isset($t[1])){ 
		$m 	   =	$t[1]; 
	} else { 
		$m 	   =	"00"; 
	} 
	$_SESSION['localtime'] 	 =	($h * 60)+$m;
	
function local_time_str($dateStr){
	$minutes_to_add	=	0;
	$minutes_to_add	=	$_SESSION['localtime'];//+240 if mysql time zone is Arabian Standard Time
	$time = new DateTime(date($dateStr));
	if($minutes_to_add >0){
		$minutes_to_add	=	abs($minutes_to_add);
		$time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
	}else{
		$minutes_to_add	=	abs($minutes_to_add);
		$time->sub(new DateInterval('PT' . $minutes_to_add . 'M'));
	}
	
	return $time->format('F d, Y H:i:s');
}
$date	=	date('Y-m-d H:i:s');
echo local_time_str($date);
//}
?>