<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/music.php");
$objMusic							=	new music();
include_once(DIR_ROOT."js/include/myplaylist.php");
if(!empty($_GET["musicid"])) {
$mid 					=	$_GET["musicid"];
$getMyPlayList				=	$objMusic->listQuery("SELECT music.music_id,music.music_title,music.music_descr,music.music_artist,music.music_thumb,music.music_url ,music.music_created,play_list.myp_id ,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name,review.reviewAvg,ml.ml_status,mv.mv_id
																				FROM music 
																				LEFT JOIN my_playlist AS play_list ON music.music_id = play_list.music_id
																				lEFT JOIN users AS user ON music.user_id = user.user_id
																				LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
																				LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
																				LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id 
																				LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id  
																				LEFT JOIN (SELECT AVG(review_rate) AS reviewAvg,user_id_to FROM user_reviews GROUP BY user_id_to ) AS review ON user.user_id = review.user_id_to
																				LEFT JOIN music_like AS ml ON music.music_id = ml.music_id AND ml.user_id = ".$_SESSION['userId']."
																				LEFT JOIN music_vote AS mv ON music.music_id = mv.music_id AND mv.user_id = ".$_SESSION['userId']."
																				WHERE music.music_status=1 AND music.music_id =".$mid." ORDER BY music_created DESC");												
$playListString			   =	"";
if(count($getMyPlayList) >0){
foreach($getMyPlayList as $keyMyPlayList=>$allMyPlayList){
		$musicThumb				   =	($allMyPlayList['music_thumb'])?$allMyPlayList['music_thumb']:'audio-thump.jpg';
		$musicUser					=	$objCommon->displayName($allMyPlayList);
		$musicUserThumb			   =	($allMyPlayList['upi_img_url'])?$allMyPlayList['upi_img_url']:'profile_pic.jpg';
		$musicCreated				 =	date(" F d, Y ",strtotime($allMyPlayList['music_created']));
		$musicuserRate				=	round($allMyPlayList['reviewAvg'])*20;
		$musicLikeStatus			  =	($allMyPlayList['ml_status']==1)?1:0;
		$playListString	  .=	"{ ";
		$playListString	  .=	"mp3:'".SITE_ROOT_AWS_MUSIC."uploads/music/".$allMyPlayList['music_url']."',title:'".addslashes($objCommon->limitWords($allMyPlayList['music_title'],20))."',artist:'".trim(addslashes($objCommon->html2text($allMyPlayList['music_artist'])))."',dateCreated:'".$musicCreated."',rating_yellow:'".$musicuserRate."%',likei:'".$allMyPlayList['music_id']."',sharei:'".$allMyPlayList['music_id']."',proffesion:'".$objCommon->html2text($allMyPlayList['c_name'])."',userInfo:'".$musicUser."',artist1:'".$objCommon->html2text($allMyPlayList['music_title'])."',poster:'".SITE_ROOT_AWS_MUSIC."uploads/music/".$musicThumb."',userthumb:'".SITE_ROOT_AWS_IMAGES."uploads/profile_images/".$musicUserThumb."',oga:'".SITE_ROOT_AWS_MUSIC."uploads/music/".$allMyPlayList['music_url']."',musicid:'".$allMyPlayList['music_id']."',musicid:'".$allMyPlayList['music_id']."',musicdesc:'".addslashes($objCommon->limitWords($allMyPlayList['music_descr'],150))."',music_user:'".$allMyPlayList['user_id']."',like_status:'".$musicLikeStatus."',votei:'".$allMyPlayList['music_id']."',vote_status:'".$allMyPlayList['mv_id']."'";
		if((count($getMyPlayList)-1) == $keyMyPlayList){
			$playListString  .=	" } ";
		}else{
			$playListString  .=	" }, ";
		}
		if($keyMyPlayList == 0){
			$firstPlayListItem=	$playListString;
		}
	}
	//echo $playListString;
}
} 
?>
<link href="<?php echo SITE_ROOT?>css/rating.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>css/mp_style.css">
<link href="<?php echo SITE_ROOT?>js/dist/skin/pink.flag/css/jplayer.pink.flag.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/dist/jplayer/jquery.jplayer.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/dist/add-on/jplayer.playlist.js"></script>
<script type="text/javascript">
//<![CDATA[
var myPlaylist;
var _playlistarray = [];
var SITE_ROOT = '<?php echo SITE_ROOT ?>';
var session_id	=	'<?php echo $_SESSION['userId'] ?>';
	
$(document).ready(function(){
		myPlaylist = new jPlayerPlaylist({
		jPlayer: "#jquery_jplayer_N",
		cssSelectorAncestor: "#jp_container_N",
	}, [<?php echo $firstPlayListItem?>], {
		playlistOptions: {
			enableRemoveControls: true
		},
		swfPath: "../js/dist/jplayer",
		supplied: "oga, mp3",
		useStateClassSkin: true,
		autoBlur: false,
		smoothPlayBar: true,
		keyEnabled: true,
		audioFullScreen: true,
		verticalVolume: true,
		loop:true,
	});
	myPlaylist.setPlaylist([<?php echo $playListString?>]);
	 $('.rate-btn').hover(function(){
		$('.rate-btn').removeClass('rate-btn-hover');
		var therate = $(this).attr('id');
		for (var i = therate; i >= 0; i--) {
			$('.rate-btn-'+i).addClass('rate-btn-hover');
		};
	});
	 $('.rate-btn').click(function(){
			var therate = $(this).attr('id');
			$("#rate_val").val(therate);
	 });
	  $('.rate-btn').mouseout(function(){
			var therate = $("#rate_val").val();
			therate++;
			for (var i = therate; i <= 5; i++) {
				$('.rate-btn-'+i).removeClass('rate-btn-hover');
			};
	 });
});

function loadMusicScript(){
	
	var	music_user_id	=	$('.musicId').attr('data-musicuser');
	if(session_id == music_user_id){
		$('.favorite').show();
	}else{
		$('.favorite').hide();
	}
	
	$(".add_to_play_list").click(function(){
		var musicIdinsert	=	$(this).parent().children('a.p_button').attr('data-musicid');
		_playlistarray.push(musicIdinsert);
		var music_id			=	$(this).data("musicid");
		$(this).addClass("active");
		var that				=	$(this);
		if(music_id){
			$.ajax({
				url:'<?php echo SITE_ROOT?>ajax/add_to_play_list.php',
				data:{music_id:music_id},
				type:"POST",
				dataType:"json",
				success: function(data){
					$(that).removeClass("active");
					$(that).html('<span class="hidden-sm hidden-md">Added</span><i class="fa fa-bars hidden-lg"></i>');
					myPlaylist.add({
						title:data['title'],
						artist:data['artist'],
						mp3:data['mp3'],
						oga:data['oga'],
						poster: data['poster'],
						musicid:data['musicid'],
						musicdesc:data['musicdesc'],
						userInfo:data['userInfo'],
						userthumb:data['userthumb'],
						proffesion:data['proffesion'],
						dateCreated:data['dateCreated'],
						rating_yellow:data['rating_yellow'],
						like_status:data['like_status'],
						music_user:data['music_user']
					});
				}
			});
		}
		var	music_user_id	=	$('.musicId').attr('data-musicuser');
		if(session_id == music_user_id){
			$('.favorite').show();
		}else{
			$('.favorite').hide();
		}
		
	});
	
	
// modified parts
var _playerInstantValue;
var currentElement;
var _unlistedarray = [];

var playerContainer = $("#jp_container_N");
	_parentAnchor = $(".player");
	globalanchortag = $("a.p_button");
	_jplayerGlobal = $("#jquery_jplayer_N");
	_play = "playingnow";
	_pause = "pauseit";
	_initializePlay = "notplay";
	_playIcon = "playingOn";
	_pauseIcon = "pauseIt";
	_playlistContainer = $(".jp-playlist");
	_playerNext = $(".jp-next");
	_playerPrev = $(".jp-previous");
	_disabled = "disabled";
	_play_inti = $(".jp-play");
	_prev_init = $(".jp-previous");
	_next_init = $(".jp-next");
	_listed_init = $("ul li a.jp-playlist-item");

var _playlistLiNumb = _playlistContainer.children('ul').find('li');
	_unliseddivNum = $(".top_videos").children('div.loadMusic').children('div.albums').find('div.album');

for (i = 0; i < _playlistLiNumb.length; i++) {
    var idValude = $(_playlistLiNumb[i]).children('div').children('a.jp-playlist-item').children('span.jp-artist').children('span.musicId').html();
    _playlistarray.push(idValude);
}
for (i = 0; i < _unliseddivNum.length; i++) {
    var anchorvalue = $(_unliseddivNum[i]).children('div.row').children('div.col-sm-7').children('div.desc').children('div.player').children('a:nth-of-type(1)').attr("data-musicid");
    _unlistedarray.push(anchorvalue);
}
if($(".share_heart1").attr("data-liked") == "1"){
		$(".share_heart1 span").html("Liked").parent('a').addClass("liked");
	}else{
		$(".share_heart1 span").html("Like").removeClass("liked");
	}
	
	var anchorLink	=	SITE_ROOT+'user/promote/music/';
		modifiedAnchor	=	anchorLink+_playlistarray[0];
		$('.favorite').attr('href', modifiedAnchor);

_parentAnchor.on("click", ".notplay", function(e) {
    var _this = $(this);
	var datMusicId		=	_this.attr("data-musicid");
	if(datMusicId){
		$.ajax({
				url:"<?php echo SITE_ROOT?>ajax/music_views.php",
				type:"POST",
				data:{musicId:datMusicId},
				success: function(data){
				}
			});
	}
    $(this).removeClass(_playIcon);
    $(this).addClass(_pauseIcon);
    $(_parentAnchor).children('a:nth-of-type(1)').not($(this)).addClass(_playIcon);
    $(_parentAnchor).children('a:nth-of-type(1)').not($(this)).removeClass(_pauseIcon);

    globalanchortag.removeClass(_play);
    globalanchortag.not($(this)).addClass(_initializePlay);

    

    $(".jp-playlist ul li").removeClass(playing);

    _this.removeClass(_initializePlay);
    _this.addClass(_play);
    e.preventDefault();

    _jplayerGlobal.jPlayer("setMedia",
			{
                mp3: _this.attr("data-path"),
                title: _this.parent().parent().children('h6').html(),
				like_status:_this.attr("data-like_status"),
                poster: _this.attr("data-image"),
                artist: _this.attr("data-artist"),
                dateCreated: _this.attr("data-dateCreated"),
                userthumb: _this.attr("data-userthumb"),
                proffesion: _this.attr("data-proffesion"),
                musicid: _this.attr("data-musicid"),
				musicdesc:_this.attr("data-musicdesc"),
				userInfo:_this.attr("data-userInfo"),
				rating_yellow:_this.attr("data-rating_yellow"),
				likei:_this.attr("data-likei"),
				votie:_this.attr("votei"),
				voteStatus: _this.attr("vote_status"),
				
				
            })
        .jPlayer("play");
		
    _playerInstantValue = $(".updside .jp-details #artist span.musicId").attr('data-verify');
	
    var get_musicid = $(this).attr("data-musicid");
    if (_playerInstantValue === get_musicid && $.inArray(get_musicid, _playlistarray) > -1) {
		//alert("t");
        currentElement = $('#music_' + get_musicid).parent('.jp-artist').parent('.jp-playlist-item').parent('div').parent('li');
		currentElement.addClass("playing");
		
		_playerNext.removeAttr(_disabled, _disabled).removeClass('no-point');
		_playerPrev.removeAttr(_disabled, _disabled).removeClass('no-point');
		
    }else{
		_playerNext.attr(_disabled, _disabled).addClass('no-point');
		_playerPrev.attr(_disabled, _disabled).addClass('no-point');
	}
	
	var	music_user_id	=	$(this).attr('data-musicuser');
	if(session_id == music_user_id){
		$('.favorite').show();
	}else{
		$('.favorite').hide();
	}
	
	if($(this).attr("data-like_status") == "1"){
		$(".share_heart1 span").html("Liked").parent('a').addClass("liked");
	}else{
		$(".share_heart1 span").html("Like").removeClass("liked");
	}
	var downloadlink 	=	$(this).attr("data-path");
	$(".musicLink").html(downloadlink);
	var downloadmelink	=	$(".musicLink").html();
	$(".download").attr("href", downloadmelink);
	var shareId	=	$(this).attr('data-sharei');
	$('.share_heart2').attr('data-share', shareId).html('<i class="fa fa-share-alt"></i>Share');
	var voteid	=	$(this).attr('data-votei');
	$('.share_heart3').attr('data-vote', voteid);
	if(_this.attr('data-vote_status') != ''){
		$('.share_heart3').attr('href','javascript:;').html('<i class="fa fa-thumbs-up"></i>Voted').addClass('voted');
	}else{
		$('.share_heart3').attr('href','#rate-video').html('<i class="fa fa-thumbs-up"></i>Vote').removeClass('voted');
	}
	var anchorLink	=	SITE_ROOT+'user/promote/music/';
		modifiedAnchor	=	anchorLink+get_musicid;
		$('.favorite').attr('href', modifiedAnchor);
	

});

_parentAnchor.on("click", ".playingnow", function(e) {
    var _this = $(this);

    _this.addClass(_pause);
    _this.removeClass(_pauseIcon);
    _this.addClass(_playIcon);


    globalanchortag.removeClass(_play);
    e.preventDefault();
    _jplayerGlobal.jPlayer().jPlayer("pause");

    _playerInstantValue = $(".updside .jp-details #artist span.musicId").attr('data-verify');;
    var get_musicid = $(this).attr("data-musicid");
    get_playlistid = _listed_init.children().children('span').html();

    if (_playerInstantValue === get_musicid && $.inArray(get_musicid, _playlistarray) > -1) {
		currentElement = $('#music_' + get_musicid).parent('.jp-artist').parent('.jp-playlist-item').parent('div').parent('li');
		currentElement.removeClass("playing");
    
		_playerNext.removeAttr(_disabled, _disabled).removeClass('no-point');
		_playerPrev.removeAttr(_disabled, _disabled).removeClass('no-point');
		
    }else{
		_playerNext.attr(_disabled, _disabled).addClass('no-point');
		_playerPrev.attr(_disabled, _disabled).addClass('no-point');
	}
	
	
});
_parentAnchor.on("click", ".pauseit", function(e) {
    var _this = $(this);

    _this.addClass(_play);
    _this.removeClass(_playIcon);
    _this.addClass(_pauseIcon);



    globalanchortag.removeClass(_pause);
    e.preventDefault();
    _jplayerGlobal.jPlayer().jPlayer("play");


    _playerInstantValue = $(".updside .jp-details #artist span.musicId").attr('data-verify');
    var get_musicid = $(this).attr("data-musicid");
    get_playlistid = _listed_init.children().children('span').html();

    if (_playerInstantValue === get_musicid && $.inArray(get_musicid, _playlistarray) > -1) {
		currentElement = $('#music_' + get_musicid).parent('.jp-artist').parent('.jp-playlist-item').parent('div').parent('li');
        currentElement.addClass("playing");
    
		_playerNext.removeAttr(_disabled, _disabled).removeClass('no-point');
		_playerPrev.removeAttr(_disabled, _disabled).removeClass('no-point');
		
    }else{
		_playerNext.attr(_disabled, _disabled).addClass('no-point');
		_playerPrev.attr(_disabled, _disabled).addClass('no-point');
	}
});

$(".jp-playlist").on("click", "ul li .jp-playlist-item", function() {
	
		var	music_user_id	=	$('.musicId').attr('data-musicuser');
		if(session_id == music_user_id){
			$('.favorite').show();
		}else{
			$('.favorite').hide();
		}

    globalanchortag.removeClass(_play);
    globalanchortag.addClass(_playIcon);
    globalanchortag.removeClass(_pauseIcon);

    globalanchortag.removeClass(_pause);
    globalanchortag.addClass(_initializePlay);

    _playerNext.removeAttr(_disabled, _disabled);
    _playerPrev.removeAttr(_disabled, _disabled);

    _playerNext.css('cursor', 'pointer');
    _playerPrev.css('cursor', 'pointer');
	
	if($(this).attr("data-like_status") == "1"){
		$(".share_heart1 span").html("Liked").parent('a').addClass("liked");
	}else{
		$(".share_heart1 span").html("Like").removeClass("liked");
	}
	
	var downloadmelink	=	$(".musicLink").html();

	$(".download").attr("href", downloadmelink);
});

$(".jp-repeat, .jp-shuffle").click(function() {
    $(this).toggleClass("toggledmP");
    //alert(_new_valuse);
});
var _playlist_Musicid = $(".playlistJp").children(".musicId").html();
_jp_player_musicid = $(".jp-details").children().children(".musicId").html();
_jp_songs_Listed = $("a.p_button ").attr("data-musicid");
playing = "playing";
itemsPlayers = $(".jp-play, .jp-next, .jp-previous, .jp-playlist-item");
_next_prev	=	 $(".jp-next, .jp-previous");

$(".jp-playlist").on("click", "ul li .jp-playlist-item", function() {
	$('.share_heart2').html('<i class="fa fa-share-alt"></i>Share');
    var currenClicked = $(this).children('span.jp-artist').children('span').html();
    extreamcurrent = $('.unlisted' + currenClicked);

    if ($.inArray(currenClicked, _unlistedarray) > -1) {
        if (extreamcurrent.hasClass("playingOn")) {
            extreamcurrent.addClass("pauseIt playingnow");
        }
    }
    if (playerContainer.hasClass('jp-state-playing')) {
        $("li.jp-playlist-current").removeClass(playing);
    } else {

        $(".jp-playlist ul li").removeClass(playing);
        $("li.jp-playlist-current").addClass(playing);
    }
	
	var anchorLink	=	SITE_ROOT+'user/promote/music/';
		modifiedAnchor	=	anchorLink+currenClicked;
		$('.favorite').attr('href', modifiedAnchor);
});

itemsPlayers.click(function() {
var RecentPlayerMusicid = $(".updside .jp-details #artist span.musicId").attr('data-verify');
currentElement = $('#music_' + RecentPlayerMusicid).parent('.jp-artist').parent('.jp-playlist-item').parent('div').parent('li');
currentUnlistedEle = $('.unlisted' + RecentPlayerMusicid);

if ($.inArray(RecentPlayerMusicid, _playlistarray) > -1) {
    $(".jp-playlist ul li").removeClass(playing);
    currentElement.addClass(playing);

    if (playerContainer.hasClass('jp-state-playing') && $.inArray(RecentPlayerMusicid, _playlistarray) > -1) {
        currentElement.removeClass(playing);
    }
}
globalanchortag.addClass("notplay playingOn");
currentUnlistedEle.removeClass("notplay");
globalanchortag.removeClass("pauseIt playingnow");
currentUnlistedEle.addClass("pauseIt playingnow");

if (playerContainer.hasClass('jp-state-playing')) {
	currentUnlistedEle.removeClass("pauseIt playingnow");
    currentUnlistedEle.addClass("pauseit playingOn");

}
var anchorLink	=	SITE_ROOT+'user/promote/music/';
		modifiedAnchor	=	anchorLink+RecentPlayerMusicid;
		$('.favorite').attr('href', modifiedAnchor);
});
var	CurrentListSelected	=	$("#music_"+_playlistarray[0]).parent('.jp-artist').parent('.jp-playlist-item').parent('div').parent('li');
//alert(CurrentListSelected.html())
	if(CurrentListSelected.hasClass("jp-playlist-current")){
		$(".jp-previous").attr(_disabled, _disabled);
		//alert('true');
	}else{
		$(".jp-previous").removeAttr(_disabled, _disabled);
	}
	
_next_prev.click(function(){
	var	CurrentListSelected	=	$("#music_"+_playlistarray[0]).parent('.jp-artist').parent('.jp-playlist-item').parent('div').parent('li');
	if(CurrentListSelected.hasClass("jp-playlist-current")){
		$(".jp-previous").attr(_disabled, _disabled);
		
	}else{
		$(".jp-previous").removeAttr(_disabled, _disabled);
	}
	//scrollAuto();
});
var downloadmelink	=	$(".musicLink").html();
$(".download").attr("href", downloadmelink);
}


		
		

function scrollAuto(){
	var Numeric	=	_playlistarray[7];
	var checkItem	=	$("#music_"+Numeric);
		parentEle	=	checkItem.parent('span').parent('a').parent('div').parent('li');
		remaing		=	parentEle.nextAll('li');
		baseHeight	=	$(".jp-playlist ul li").height();
		margin_up	=	(baseHeight*remaing.length)-baseHeight;
		scrolEle	=	$(".jp-playlist ul");
		liElement	=	scrolEle.find('li');
		indexofs	=	$(".jp-playlist ul li").index($(".jp-playlist-current"));
		
	if(indexofs >= 5){
		scrolEle.css({"margin-top" : -margin_up});
		
	}else{
		scrolEle.css({"margin-top" : 0});
	}
}
</script>
    <div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="user_profile">
                        <div class="row">
                         <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                             <div class="pagination_box">
                                    <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
                                    <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                    <a href="#" class="active"> Music </a>
                                  <?php
                                    include_once(DIR_ROOT."widget/notification_head.php");
                                    ?>	
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3 music-page">
                                     <?php
									  include_once(DIR_ROOT.'widget/right_search_main.php');
									  ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                                    <!---no-->
                                </div> 
                           </div>
                            <div class="clr"></div>
                            <div class="videos">
                                <div class="row">
                                    <div class="col-sm-12 col-sm-12 col-md-12 col-lg-sp-9">
                                    <div class="content music-pagein">
                                        <div class="pagination_box">
                                            <div class="row">
                                                <div class="col-sm-8 newWidthMusic"><div class="video_search">
                                                	<div class="row">
                                                    	<div class="col-sm-9 no-padding-r">
                                                    <input type="text" placeholder="Search Music" name="keyword" id="keyword" />
                                                    	</div>
                                                    	<div class="col-sm-3 no-padding-l">
                                                    <button class="btn btn-group bt-submit" id="search_music"><i class="fa fa-search hidden-lg"></i><span class="hidden-sm hidden-md hidden-xs">Search Music</span></button>
                                                    	</div>
                                                    </div>
												</form>
                                                </div>
                                                </div>
                                                <div class="col-sm-1 col-md-1 col-lg-2 alt-padding-r alt-padding-l">
                                                    <div class="upload"><a href="<?php echo SITE_ROOT.'music/upload'?>"  data-toggle="tooltip" data-placement="bottom" title="Upload Music"><i class="fa fa-volume-up"></i><span class="hidden-sm hidden-md">Upload Music</span></a></div>
                                                    </div>
                                                    <div class="col-sm-3 col-md-3 col-lg-2 alt-padding-l">
                                                    <div class="option">
                                                        <p><span>Select Category</span><i class="fa fa-chevron-down hidden-sm hidden-md "></i><i class="fa fa-bars hidden-lg"></i></p>
                                                        <ul>
                                                            <li><a href="javascript:;" class="search_category" data-category="new">New Music</a></li>
                                                            <li><a href="javascript:;" class="search_category" data-category="popular">Popular Music</a></li>
															<li><a href="javascript:;" class="search_category" data-category="top">Top Music</a></li>
															<li><a href="javascript:;" class="search_category" data-category="last">Last Visited</a></li>
															<li><a href="javascript:;" class="search_category" data-category="all">All Music</a></li>
                                                        </ul>
                                                  </div>
                                              </div>
                                                <div class="col-sm-1 not">
                                              <?php
												//include_once(DIR_ROOT."widget/notification_head.php");
												?>	
                                                </div>
                                                </div>
                                      </div>
                                        <div class="clr"></div>            
                                    </div>
                                        <div class="video_left c ">
											<div id="jp_container_N" class="jp-video jp-video-270p" role="application" aria-label="media player">
												<div class="jp-type-playlist">
													<div id="jquery_jplayer_N" class="jp-jplayer"></div>
                                                    <div class="updside">
                                                        <div class="jp-details ">
                                                            <div class="jp-title" aria-label="title">&nbsp;</div>
                                                            <div id="artist" class="jp-artist" aria-label="artist">Albumby:</div>
                                                            <!--<div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>-->
                                                            <div class="jp-desc"></div>
                                                            <a href="javascript:;" class="jp-play" role="button" tabindex="0">Play</a>
                                                            <a href="<?php echo SITE_ROOT?>access/download_music.php?file=2015-08-24/1440420779_2.mp3" class="download"  role="button" tabindex="0" download>Download</a>
                                                            <a href="<?php echo SITE_ROOT ?>user/promote/music/" class="favorite" role="button" tabindex="0" style="display:none;">Promote</a>
                                                           <!-- <a href="javascript:;" class="more" role="button" tabindex="0"></a>-->
                                                        </div>
                                                    </div>
													<div class="jp-gui">
														<div class="jp-video-play">
															<button class="jp-video-play-icon" role="button" tabindex="0">play</button>
														</div>
														<div class="jp-interface">
															<div class="jp-controls-holder">
																<div class="jp-controls">
																	<button class="jp-previous" role="button" tabindex="0">previous</button>
																	<button class="jp-play" role="button" tabindex="0"><div id="bars">
                                                                        <div class="bar"></div>
                                                                        <div class="bar"></div>
                                                                        <div class="bar"></div>
                                                                        <div class="bar"></div>
                                                                      
                                                                    </div></button>
																	<button class="jp-next" role="button" tabindex="0">next</button>
																</div>
																<div class="jp-volume-controls">
																	<button class="jp-mute" role="button" tabindex="0">mute</button>
																	<div class="jp-volume-bar">
																		<div class="jp-volume-bar-value"></div>
																	</div>
																</div>
																
																<div class="jp-toggles">
																	<button class="jp-repeat toggledmP" role="button" tabindex="0">Repeat</button>
																	<button class="jp-shuffle" role="button" tabindex="0">Shuffle</button>
																</div>
															</div>
                                                            <div class="right-side">
                                                                <div class="jp-details">
                                                                    <div class="jp-title" aria-label="title">&nbsp;</div>
                                                                    <div id="artist" class="jp-artist" aria-label="artist"></div>
                                                                    
                                                                </div>
                                                                <div class="jp_progress_time">
                                                                    <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                                                                        <div class="jp-progress">
                                                                            <div class="jp-seek-bar">
                                                                                <div class="jp-play-bar"></div>
                                                                            </div>
                                                                        </div>
                                                                    <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                                                                </div>
                                                            </div>
														</div>
													</div>
													<div class="jp-no-solution">
														<span>Update Required</span>
														To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
													</div>
												</div>
                                                <div class="user-info-mp">
                                                 <div class="top-right-sec"> 
                                                	<div class="userthumb"><div class="img-thumbs"><img /></div></div>
                                                	<div class="right-info"> 
                                                          <p class="user-name"></p>
                                                            <p class="dateCreated dropdown_option"><i class="fa fa-globe"></i><span class="insertDate"></span><span class="professions"><span class="proffesion"></span></span></p>
                                                            <div class="rating_box">
                                                                <div class="rating_yellow" style="width:60%"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="bottom-right-sec">
                                                    	<div class="share_coment">
													<div class="likeVideoBtn">
                                                    	<a class="share_heart1 likeVideo" href="javascript:;"><i class="fa fa-heart"></i><span>Like</span></a>
                                                    </div>
													<div class="shareVideoBtn">
                                                    	<a class="share_heart2 shareVideo" href="javascript:;"><i class="fa fa-share-alt"></i>Share</a>
                                                    </div>
													 <a href="#rate-video" id="rateVideo" class="share_heart3"><i class="fa fa-thumbs-up"></i>Vote</a>
                                                    </div>
                                                    </div>
                                                </div>
												<div class="tracklist">
                                                	<div class="track-head">
                                                    	<img src="<?php echo SITE_ROOT?>images/menuplayer.png" /><span>My Playlist</span>
                                                        <?php /*?><img src="<?php echo SITE_ROOT?>images/TRASH.png" class="pull-right trashed"><?php */?>
                                                    </div>
                                                    <div class="jp-playlist">
                                                        <ul>The method Playlist.displayPlaylist() uses this unordered list <li></li></ul>
                                                    </div>
                                                    <!--<div class="track-footer">
                                                    	<div class="share_social">
															<a href="#"><i class="fa fa-facebook"></i></a>
															<a href="#"><i class="fa fa-twitter"></i></a>
															<a href="#"><i class="fa fa-google-plus"></i></a>
															<a href="#"><i class="fa fa-pinterest-p"></i></a>
														</div>
                                                    </div>-->
                                                </div>
											</div>
										</div>
                                    </div>
                                    <div class="hidden-sm hidden-md col-lg-sp-3">
                                         <?php include_once(DIR_ROOT.'widget/ad/right_ad_two_block.php');?>
                                    </div>
                                    <div class="clr"></div> 
                                    <div class="top_videos">
										<h1>All Music</h1>
										<div class="loadMusic"></div>
                            </div>
                            <div class="clr"></div>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
   <!---------------------------------------->
    <div class="remodal popup_box" data-remodal-id="rate-video">
       <div class="video_rate_table login_table">
		<div>
		Rate this Music
		</div>
       	<div class="tr">
			<div class="rate-ex2-cnt">
				<div id="1" class="rate-btn-1 rate-btn"></div>
				<div id="2" class="rate-btn-2 rate-btn"></div>
				<div id="3" class="rate-btn-3 rate-btn"></div>
				<div id="4" class="rate-btn-4 rate-btn"></div>
				<div id="5" class="rate-btn-5 rate-btn"></div>
				<input type="text" name="rate_val" id="rate_val" style="display:none;" />
			</div><input type="button" value="Rate" class="rate_submit">
		</div>
		<div class="tr submit">
			
		</div> 
       </div>
    </div>
    <!---------------------------------------->
<script type="text/javascript">
$(document).ready(function(e) {
	getresult('<?php echo SITE_ROOT?>widget/load_music.php');
	
	$(function () {
		$('[data-toggle="tooltip"]').tooltip();
	});
	$('#playlist').perfectScrollbar();
	
	$(".option").click(function(){
		$(this).find("ul").slideToggle();
	});
	$(".item").click(function(){
	//$(".item").removeClass("activemp3");
		$(this).toggleClass("activemp3");
	});
	$(".delete").click(function(){
		if($(".item").hasClass('activemp3')){
			if (confirm("Do you want to delete")){
				$(".activemp3").remove();
			}
		}
	});
	$(".close_btn").click(function(){
		if (confirm("Do you want to delete")){
			$(this).parent(".item").remove();
		}
	});
	$(".jp-playlist").perfectScrollbar();
});
$(".jp-playlist").perfectScrollbar();
$(".likeVideoBtn").on("click",".likeVideo",function(){
	var that			   =	this;
		musicId			=	$(that).data("like");
	if(musicId){
		$.ajax({
			url:"<?php echo SITE_ROOT?>ajax/music_like.php",
			type:"POST",
			data:{musicId:musicId},
			success: function(data){
				$(that).parent().html(data);
			}
		});
	}
});
$(".shareVideoBtn").on("click",".shareVideo",function(){
	var that			   =	this;
		musicId			=	$(that).data("share");
	if(musicId){
		$.ajax({
			url:"<?php echo SITE_ROOT?>ajax/music_share.php",
			type:"POST",
			data:{musicId:musicId},
			success: function(data){
				$(that).html('<i class="fa fa-share-alt"></i>Shared');
			}
		});
	}
});
$("#search_music").on("click",function(){
	var keyword		=	$("#keyword").val();
	getresultSearch('<?php echo SITE_ROOT?>widget/load_music.php?keyword='+keyword+'&page=1');
});
$('#keyword').keydown(function(event){			
	if (event.keyCode == 13 && !event.shiftKey) {
		var keyword		=	$("#keyword").val();
		getresultSearch('<?php echo SITE_ROOT?>widget/load_music.php?keyword='+keyword+'&page=1');
  }
});
$(".search_category").on("click",function(){
	var that			=	this;
		searchCat	   =	$(that).data("category");
	if(searchCat){
		getresultSearch('<?php echo SITE_ROOT?>widget/load_music.php?keywordCat='+searchCat+'&page=1');
		$("#keyword").val('');
		if(searchCat=='new'){
			$(".option p span").html('New Music');
			$(".top_videos h1").html('New Music');
		}else if(searchCat=='popular'){
			$(".option p span").html('Popular Music');
			$(".top_videos h1").html('Popular Music');
		}else if(searchCat=='top'){
			$(".option p span").html('Top Music');
			$(".top_videos h1").html('Top Music');
		}else if(searchCat=='last'){
			$(".option p span").html('Last Visited');
			$(".top_videos h1").html('Last Visited');
		}else if(searchCat=='all'){
			$(".option p span").html('All Music');
			$(".top_videos h1").html('All Music');
		}
	}
});
function getresult(getUrl) {
	$.ajax({
		url: getUrl,
		type: "GET",
		data:  {rowcount:$("#rowcount").val(),myCategoryId:<?php echo $getUserDetails['uc_c_id']?>},
		beforeSend: function(){
		//$('#loader-icon').show();
		},
		complete: function(){
		//$('#loader-icon').hide();
		},
		success: function(data){
			$(".loadMusic").append(data);
			if($("#total-count").val()>0){
			}else{
				$(".loadMusic").html('<p>No music found....</p>');
			}
		},
		error: function(){} 	        
   });
}
function getresultSearch(getUrl) {
		$.ajax({
		url: getUrl,
		type: "GET",
		data:  {rowcount:$("#rowcount").val(),myCategoryId:<?php echo $getUserDetails['uc_c_id']?>},
		beforeSend: function(){
		//$('#loader-icon').show();
		},
		complete: function(){
		//$('#loader-icon').hide();
		},
		success: function(data){
			$(".loadMusic").html(data);
			if($("#total-count").val()>0){
			}else{
				$(".loadMusic").html('<p>No music found....</p>');
			}
		},
		error: function(){} 	        
   });
}
$(window).scroll(function(){
		if ($(window).scrollTop() == $(document).height() - $(window).height()){
			if($(".pagenum:last").val() <= $(".total-page").val()) {
				var pagenum = parseInt($(".pagenum:last").val()) + 1;
				var keyword		=	$("#keyword").val();
				getresult('<?php echo SITE_ROOT?>widget/load_music.php?page='+pagenum+'&keyword='+keyword);
			}
		}
	});
$(".video_rate_table").on("click",".rate_submit",function(){
	var inst = $.remodal.lookup[$('[data-remodal-id=rate-video]').data('remodal')];
	var rate_val				=	$("#rate_val").val();
	var vidId				   =	$(".share_heart3").data('vote');
	var voted				   =	$(".share_heart3").data('voted');
	var that		  			=	this;
	if(rate_val != '' && vidId != '' && voted ==''){
		$.ajax({
			url:"<?php echo SITE_ROOT?>ajax/rate_music.php",
			method:"POST",
			data:{rate_val:rate_val,vidId:vidId},
			success:function(result){
				$("#rateVideo").addClass("voted");
				$("#rateVideo").html('<i class="fa fa-thumbs-up"></i>Voted');
				$("#rateVideo").attr("href",'javascript:;');
				inst.close();
		}});
	}else{
		inst.close();
	}
});
</script>
    <?php
//include_once(DIR_ROOT."includes/login_box.php");
include_once(DIR_ROOT."includes/footer.php");
?>
    