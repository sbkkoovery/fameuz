<?php
include_once("includes/site_root.php");
$ogTitle				  =	'Fameuz.com - Composite Card';
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/composite_card_img.php");
$objCompoCardImg				 =	new composite_card_img();
$user_url						=	$objCommon->esc($_GET['user_url']);
if($user_url==$getUserDetails['usl_fameuz'] || $user_url == '')
{
	include_once(DIR_ROOT.'widget/my_composite_card.php');
}else{
	include_once(DIR_ROOT.'widget/user_composite_card.php');
}
include_once(DIR_ROOT."includes/footer.php");
?>
  