<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
$user_url		=	$objCommon->esc($_GET['user_url']);
if($user_url==$getUserDetails['usl_fameuz'])
{
	header("location:".SITE_ROOT);
	exit;
}else{
$getMyFriendDetails			=	$objUsers->getRowSql("SELECT user.user_id,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz
															FROM users AS user 
															LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
															WHERE user.status=1 AND user.email_validation=1  AND social.usl_fameuz='".$user_url."'");
$getPolaroids				  =	$objUsers->listQuery("SELECT polo_id,polo_url FROM polaroids WHERE user_id=".$getMyFriendDetails['user_id']." ORDER BY polo_order");			
}
include_once(DIR_ROOT."js/include/fameuz_lightbox_library.php");
?>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.masonry.js"></script>
<script type="text/javascript">
	$(function(){
		//run masonry when page first loads
		$(window).load(function() {$('.album_maso').masonry();});
		//run masonry when window is resized
			$(window).resize(function() {$('.album_maso').masonry();});
	});
	</script>


<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
  <div class="inner_top_border">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
	  <?php echo $objCommon->checkEmailverification();?>
      <div class="jobs-head">
        <div class="pagination_box_jobs pagination_box">
        <a href="<?php echo SITE_ROOT.'user/home'?>">Home <i class="fa fa-caret-right"></i></a>
      	<a href="<?php echo SITE_ROOT.$getMyFriendDetails['usl_fameuz']?>"> <?php echo $objCommon->displayNameSmall($getMyFriendDetails,12);?>'s <i class="fa fa-caret-right"></i></a> 
        <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
        <a href="javascript:;" class="active">Polaroids</a>
       <?php
        include_once(DIR_ROOT."widget/notification_head.php");
        ?>
        </div>
       </div>
       <div class="polarids">
       		<div class="ploarids-sec">
            	<div class="row">
				<?php
				if(count($getPolaroids) >0){
				?>
				<div id="masonry" class="album_maso masonry" >
                <?php 
				
				foreach($getPolaroids as $allPolaroids){
				?>
                	<div class="album_maso_album_maso_box masonry-brick" >
                    	<div class="polarid-items">
                        	<div class="polarid-img">
                            	<a href="javascript:;" class="lightBoxs" data-contentId="<?php echo $allPolaroids['polo_id']?>" data-contentType="8"><img src="<?php echo SITE_ROOT?>uploads/albums/<?php echo $objCommon->html2text($allPolaroids['polo_url'])?>" /></a>
                            </div>
                        </div>
                    </div>
                    <?php 
					}
				?>
				</div>
				<?php
				}else{
					echo '<p>'.$objCommon->displayNameSmall($getMyFriendDetails,12).' has no Polaroids yet</p>';
				}
				?>
                </div>
            </div>
       </div>
        </div>
        <div class="col-sm-3 col-lg-sp-3">
			<?php
				include_once(DIR_ROOT."widget/right_static_ad_bar.php");
            ?>
        </div>
    </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$('.album_maso').masonry();
$(document).ready(function(){
	$('body').fameuzLightbox({
				class : 'lightBoxs',
				sidebar: 'default',
				photos:{
					imageLoadUrl: '<?php echo SITE_ROOT.'ajax/popUpImageUser.php'?>',
					data_attr	: ['data-contentId','data-contentType','data-contentAlbum'],
					likeUrl	: '<?php echo SITE_ROOT?>ajax/like.php',
					shareUrl: '<?php echo SITE_ROOT?>ajax/share.php',
					workedUrl: '<?php echo SITE_ROOT?>ajax/worked_together.php',
					commentBoxUrl: '<?php echo SITE_ROOT.'widget/pop_image_comment_box.php' ?>',
					comments :{
						commentActionUrl:'<?php echo SITE_ROOT?>access/post_comment.php?action=add_comment',
						commentDelete: '<?php echo SITE_ROOT?>access/delete_comment.php',
					}
				},
				videos : {
					videoLoadUrl: '<?php echo SITE_ROOT.'ajax/popUpVideoUser.php'?>',
					data_attr	: ['data-vidEncrId'],
					likeUrl		: '<?php echo SITE_ROOT?>ajax/like_video.php',
					shareUrl	: '<?php echo SITE_ROOT?>ajax/share_video.php',
					commentBoxUrl:'<?php echo SITE_ROOT.'widget/pop_video_comment_box.php' ?>',
					comments :{
						commentActionUrl:'<?php echo SITE_ROOT?>access/post_video_comment.php?action=add_comment',
					}
				},
				skin: {
					next	: '<img src="<?php echo SITE_ROOT ?>fameuz_lightbox/images/next.png">',
					prev	: '<img src="<?php echo SITE_ROOT ?>fameuz_lightbox/images/prev.png">',
					reset	: '<i class="fa fa-refresh"></i>',
					close	: '<img src="<?php echo SITE_ROOT ?>fameuz_lightbox/images/close.png" width="15">',
					loader	: '<?php echo SITE_ROOT ?>images/ajax-loader.gif',
					review	: '<i class="fa fa-chevron-right"></i>',
					video	: '<?php echo SITE_ROOT ?>jw_player/six/six.xml',
				}
			});
});
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  