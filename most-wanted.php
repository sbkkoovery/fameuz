<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
?>
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
  <div class="inner_top_border">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-sp-9">
      <div class="jobs-head">
        <div class="pagination_box_jobs pagination_box">
        <a href="<?php echo SITE_ROOT.'user/home'?>">Home <i class="fa fa-caret-right"></i></a>
        <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
        <a href="javascript:;" class="active"> Most Wanted</a>
       <?php
        include_once(DIR_ROOT."widget/notification_head.php");
        ?>
        </div>
       </div>
	   <?php
	   include_once(DIR_ROOT."widget/face_month.php");
	   ?>
        </div>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-sp-3">
        <div class="fixer">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <?php include_once(DIR_ROOT.'widget/right_search_main.php');?>
            </div>
          </div>
         <?php include_once(DIR_ROOT.'widget/ad/right_ad_single_block.php');?>
        </div>
      </div>
    </div>
    <div class="most-wanted-grey">
    <ul>
   		<li data-gender="2"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/female.png" class="femaleIco displyIco" />Female<span class="point-it-1"></span></li>
    	<li data-gender="1"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/male.png" height="20" width="23" class="maleIco displyIco"/>Male<span></span></li>
    </ul>
    </div>
    <div class="tab_white_most_wanted_float">
    <div class="row loadMostwanted">
		<div class="load_album_preloader"></div>
    </div>
    </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$(".load_album_preloader").show();
	$(".loadMostwanted").load('<?php echo SITE_ROOT.'widget/most_wanted_list.php?gender=2'?>',function(){
		$(".load_album_preloader").hide();
	});
	$(".most-wanted-grey ul li").click(function(){
		var gender	=	$(this).data("gender");
		$(".most-wanted-grey ul li span").removeClass("point-it-1");
		$(".displyIco").hide();
		$(this).children("span").addClass("point-it-1");
		$(this).find(".displyIco").show();
		$(".load_album_preloader").show();
		$(".loadMostwanted").load('<?php echo SITE_ROOT.'widget/most_wanted_list.php?gender='?>'+gender,function(){
			$(".load_album_preloader").hide();
		});
	});
 });
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  