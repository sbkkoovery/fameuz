<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."class/videos.php");
include_once(DIR_ROOT."class/pagination-class.php");
include_once(DIR_ROOT."class/search_functions.php");
$objVideos				   =	new videos();
$obj_search_functions		=	new search_functions();
$search_keyword			  =	$objCommon->esc($_GET['search-keyword']);
$search_type			  	 =	$objCommon->esc($_GET['search-type']);
$myCategoryId		   		=	$getUserDetails['uc_c_id'];
$countryId			  	   =	$_SESSION['country_user']['country_id'];
if($search_keyword){
	$sqlVideos		  	   =	$obj_search_functions->search($search_keyword);
	$searchHeadStr		   =	'Search results for "'.$search_keyword.'"';
}else if($search_type){
	if($search_type=='new-videos'){
		$sqlVideos			   =	"SELECT vid.video_url,video_encr_id,vid.video_thumb,vid.video_duration,vid.video_type,vid.video_title,vid.video_visits,vid.video_created,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz,avg(vote.vv_vote) as voteAvg,case when (promo.promo_id !='') then 1 else 0 END AS promoted_video
										FROM videos AS vid 
										LEFT JOIN users AS user ON  vid.user_id = user.user_id
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id  
										LEFT JOIN videos_vote AS vote ON vid.video_id	=	vote.video_id
										LEFT JOIN promotion AS promo ON vid.video_id = promo.promo_content_id AND promo.promo_type='video' AND  promo.promo_country LIKE '%,".$countryId.",%' AND promo.promo_category LIKE '%".$myCategoryId."%' AND DATE(NOW()) between promo.promo_start_date and promo.promo_end_date AND promo.promo_admin_status=1 AND promo.promo_status=1
										WHERE video_status=1 AND user.status=1 AND user.email_validation =1 ORDER BY promoted_video DESC,vid.video_created DESC";
	$searchHeadStr		   =	"New Videos";
	}else if($search_type=='top-videos'){
		$sqlVideos			= 	"SELECT vid.video_id,vid.video_url,video_encr_id,vid.video_thumb,vid.video_duration,vid.video_type,vid.video_title,vid.video_visits,vid.video_created,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz,SUM(vote.vv_vote) as voteSum,avg(vote.vv_vote) as voteAvg 
									FROM videos AS vid  
									LEFT JOIN users AS user ON vid.user_id = user.user_id 
									LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
									LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
									LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
									LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
									LEFT JOIN videos_vote AS vote ON vid.video_id	=	vote.video_id 
									WHERE video_status=1 AND user.status=1 AND user.email_validation =1 
									GROUP BY vid.video_id 
									ORDER BY voteSum DESC,video_created DESC";
	$searchHeadStr		   =	"Top Videos";
	}else if($search_type=='popular-videos'){
		$sqlVideos			=	"SELECT vid.video_url,video_encr_id,vid.video_thumb,vid.video_duration,vid.video_type,vid.video_title,vid.video_visits,vid.video_created,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz,avg(vote.vv_vote) as voteAvg 
									FROM videos AS vid 
									LEFT JOIN users AS user ON  vid.user_id = user.user_id
									LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
									LEFT JOIN videos_vote AS vote ON vid.video_id	=	vote.video_id 
									WHERE vid.video_status=1 AND user.status=1 AND user.email_validation =1 GROUP BY vid.video_id  ORDER BY vid.video_visits DESC";
	$searchHeadStr		   =	"Popular Videos";
	}else if($search_type=='last-visited'){
	$sqlVideos			   =	"SELECT vid.video_id,vid.video_url,video_encr_id,vid.video_thumb,vid.video_duration,vid.video_type,vid.video_title,vid.video_descr,vid.video_visits,vid.video_likes,vid.video_created,user.first_name,user.user_id,user.last_name,user.display_name,user.email,social.usl_fameuz,cat.c_name,profileImg.upi_img_url,avg(vote.vv_vote) as voteAvg
										FROM videos AS vid
										LEFT JOIN users AS user ON  vid.user_id = user.user_id
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
										LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id
										LEFT JOIN videos_vote AS vote ON vid.video_id	=	vote.video_id 
										LEFT JOIN videos_views AS view ON vid.video_id = view.video_id
										WHERE vid.video_status=1 AND user.status=1 AND user.email_validation =1 AND view.user_id =".$_SESSION['userId']." GROUP BY vid.video_id ORDER BY view.vvs_created DESC";
	$searchHeadStr		   =	"Last Visited";
	}else if($search_type=='all-videos'){
	$sqlVideos			   =	"SELECT vid.video_id,vid.video_url,video_encr_id,vid.video_thumb,vid.video_duration,vid.video_type,vid.video_title,vid.video_descr,vid.video_visits,vid.video_likes,vid.video_created,user.first_name,user.user_id,user.last_name,user.display_name,user.email,social.usl_fameuz,cat.c_name,profileImg.upi_img_url,avg(vote.vv_vote) as voteAvg,case when (promo.promo_id !='') then 1 else 0 END AS promoted_video
										FROM videos AS vid
										LEFT JOIN users AS user ON  vid.user_id = user.user_id
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
										LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id
										LEFT JOIN videos_vote AS vote ON vid.video_id	=	vote.video_id
										LEFT JOIN promotion AS promo ON vid.video_id = promo.promo_content_id AND promo.promo_type='video' AND  promo.promo_country LIKE '%,".$countryId.",%' AND promo.promo_category LIKE '%".$myCategoryId."%' AND DATE(NOW()) between promo.promo_start_date and promo.promo_end_date AND promo.promo_admin_status=1 AND promo.promo_status=1 
										WHERE vid.video_status=1 AND user.status=1 AND user.email_validation =1 GROUP BY vid.video_id ORDER BY promoted_video DESC,vid.video_created DESC";
	$searchHeadStr		   =	"All Videos";
	}
}else{
	$sqlVideos			   =	"SELECT vid.video_id,vid.video_url,video_encr_id,vid.video_thumb,vid.video_duration,vid.video_type,vid.video_title,vid.video_descr,vid.video_visits,vid.video_likes,vid.video_created,user.first_name,user.user_id,user.last_name,user.display_name,user.email,social.usl_fameuz,cat.c_name,profileImg.upi_img_url,avg(vote.vv_vote) as voteAvg,case when (promo.promo_id !='') then 1 else 0 END AS promoted_video
										FROM videos AS vid
										LEFT JOIN users AS user ON  vid.user_id = user.user_id
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
										LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id
										LEFT JOIN videos_vote AS vote ON vid.video_id	=	vote.video_id
										LEFT JOIN promotion AS promo ON vid.video_id = promo.promo_content_id AND promo.promo_type='video' AND  promo.promo_country LIKE '%,".$countryId.",%' AND promo.promo_category LIKE '%".$myCategoryId."%' AND DATE(NOW()) between promo.promo_start_date and promo.promo_end_date AND promo.promo_admin_status=1 AND promo.promo_status=1
										WHERE vid.video_status=1 AND user.status=1 AND user.email_validation =1 GROUP BY vid.video_id ORDER BY promoted_video DESC,vid.video_created DESC";
	$searchHeadStr		   =	"All Videos";
}
$num_results_per_page		=	10;
$num_page_links_per_page 	 =	5;
$pg_param 					=	"";
$pagesection				 =	'';
$sqlVideos;
pagination($sqlVideos,$num_results_per_page,$num_page_links_per_page,$pg_param,$pagesection);
//$getAllVideoDetails		  =	$objVideos->listQuery($pg_result);
$getAllVideoDetails		  =	$objVideos->listQuery($paginationQuery);
?>
<link href="<?php echo SITE_ROOT?>css/pagination-class.css" rel="stylesheet" type="text/css" />
<div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="user_profile">
                            <div class="row">
                            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                            	<div class="upper-video-sec">
                                	<?php
									include_once(DIR_ROOT."widget/video_serach_widget.php");
									?>
                                    <div class="col-sm-1">
                                     <?php
									 if(isset($_SESSION['userId'])){
										include_once(DIR_ROOT."widget/notification_head.php");
									 }
								?>
                                </div>
                                </div>
                                    </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3">
                            	     <div class="sidebar no-border">
                            	<div class="search_box"><form action="<?php echo SITE_ROOT.'user/search-result'?>" method="get"><input type="text" placeholder="Search"  name="keywordSearch" /></form></div>
                            </div>
                            </div> 
                            </div> 
                            <div class="row">
                               <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                               <div class="video_left">
                                	<div class="search_videos custom-container">
                                		<div class="videos_slider_section">
                                        <div class="item">
                                        <div class="row">
                                        <div class="col-sm-6">
                                        <h1><?php echo $searchHeadStr;?></h1>
                                        </div>
                                        <div class="col-sm-6">
                                        <p class="pull-right serch-count"> About <?php echo $objVideos->countRows($sqlVideos) ?> Results</p>
                                        </div>
                                        </div>
                                         </div>
                                       <?php
										if(count($getAllVideoDetails) >0){
										?>
											<div class="row">
												<?php
												foreach($getAllVideoDetails as $allVideoDetails){
													if($allVideoDetails['video_type']==1){
														$getAiImages				=	SITE_ROOT_AWS_VIDEOS.'uploads/videos/'.$allVideoDetails['video_thumb'];
														$vidUrl					 =	SITE_ROOT.'uploads/videos/'.$allVideoDetails['video_url'];
													}else if($allVideoDetails['video_type']==2){
														if (preg_match('%^https?://[^\s]+$%',$allVideoDetails['video_thumb'])) {
															$getAiImages			=	$allVideoDetails['video_thumb'];
														} else {
															$getAiImages			=	SITE_ROOT_AWS_VIDEOS.'uploads/videos/'.$allVideoDetails['video_thumb'];
														}
														$vidUrl					 =	$objCommon->html2text($allVideoDetails['video_url']);
													}
													$displayNameFriend			  =	$objCommon->displayName($allVideoDetails);
													$vidcreatedTime				 =	$objCommon->time_elapsed_string(strtotime($objCommon->local_time($allVideoDetails['video_created'])));
													$vidRateCound				   =	$objCommon->round_to_nearest_half($allVideoDetails['voteAvg']);
													$vidRateCoundStyle	   		  =	'style="width:'.($vidRateCound*20).'%;"';
												?>
												<div class="col-sm-12">
													<div class="item">
													<div class="row">	<div class="col-xs-12 col-sm-5 col-md-3">
															<a href="<?php echo SITE_ROOT.'video/watch/'.$objCommon->html2text($allVideoDetails['video_encr_id'])?>" class="vidimg pull-left"><img src="<?php echo $getAiImages?>" alt="video" width="100%" class="img-responsive"/>
																<div class="video_duration"><?php echo $allVideoDetails['video_duration']?></div>
															</a>
														</div>
														<div class="col-xs-12 col-sm-7 col-md-9 vidDescr">
															<h2><a href="<?php echo SITE_ROOT.'video/watch/'.$objCommon->html2text($allVideoDetails['video_encr_id'])?>"><?php echo $objCommon->html2text($allVideoDetails['video_title'])?></a></h2>	
															<div class="row"><div class="col-sm-5"><p class="publishedBy"><i class="fa fa-globe"></i> <span class="publishedBySpan">Published By:</span> <a href="<?php echo SITE_ROOT.$objCommon->html2text($allVideoDetails['usl_fameuz'])?>"><?php echo $displayNameFriend?></a></p></div><div class="col-sm-4"><p class="publishedBy"><i class="fa fa-clock-o"></i> <?php echo $vidcreatedTime?></p></div></div>
                                                            <p><?php echo $objCommon->limitWords($allVideoDetails['video_descr'],120)?></p>
                                                            <?php /*?><div class="row">
															<div class="col-sm-12"><p class="publishedBy"><i class="fa fa-globe"></i> <span class="publishedBySpan">Published By:</span> <a href="<?php echo SITE_ROOT.$objCommon->html2text($allVideoDetails['usl_fameuz'])?>"><?php echo $displayNameFriend?></a></p></div></div><?php
															<div class="row"><div class="col-sm-12"><p class="publishedBy"><i class="fa fa-clock-o"></i> <?php echo $vidcreatedTime?></p></div></div> */?>
															<div class="likeWidget">
																<div class="row">
																	<div class="col-sm-2 col-md-2 col-lg-2"><span><i class="fa fa-heart"></i> <?php echo number_format($allVideoDetails['video_likes']);?> Likes</span></div>
																	<div class="col-sm-5 col-md-5 col-lg-2"><span><div class="rating_box"><div <?php echo $vidRateCoundStyle?> class="rating_yellow"></div></div></span></div>
																	<div class="col-sm-5"><span><i class="fa fa-eye"></i> <span class="vidVisits"><?php echo number_format($allVideoDetails['video_visits']);?> </span></span></div>
																	<?php
																	if($allVideoDetails['promoted_video']==1){
																		echo '<div class="col-sm-2"><div class="promoted_video"><i class="fa fa-external-link-square"></i> Promoted</div></div>';
																	}
																	?>
																</div>
															</div>
														</div>
                                                        </div>
													</div>
												</div>
												<?php
												}
												?>
											</div>
											<div class="paginationDiv"><?php echo $pagination_output;?></div>
											<?php
										}else{
											echo "No results found for your query. check your spelling or try another term";
										}
										?>
										</div>
                               		 </div>
                                </div></div>
                                  <div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3">
                                  <?php include_once(DIR_ROOT.'widget/ad/right_ad_two_block.php');?>
								  </div>
                                <div class="clr"></div>
                            	</div>
                            <div class="clr"></div>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
