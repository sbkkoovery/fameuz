<?php
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/email_message_attachments.php");
$objEmailMsgAttach		   =	new email_message_attachments();
$objCommon		 		   =	new common();
if(isset($_GET['attach_id'])&&$_GET['attach_id']!=""){
	$attach_id			   =	$objCommon->esc($_GET['attach_id']);
	$allAttachments		  =	$objEmailMsgAttach->getAll("em_id=".$attach_id);
	if(count($allAttachments)>0){
		error_reporting(E_ALL);
		if (!extension_loaded('zip')) {
			dl('zip.so');
		}
		$thisdir = dirname(__FILE__);
		$zip = new ZipArchive();
		$filename = time()."_".$attach_id.".zip";
		
		if (!$zip->open($filename, ZIPARCHIVE::CREATE)) {
			exit("cannot open <$filename>\n");
		} else {
			echo "file <$filename> OK\n";
		}
		foreach($allAttachments as $attach){
			$singleFileNameArr		=	explode("/",$attach['ema_file']);
			$singleFileNameArr		=	array_filter($singleFileNameArr);
			$zip->addFile(DIR_ROOT.'uploads/email_attachment/'.$attach['ema_file'],$singleFileNameArr[1]);
		}
		echo "numfiles: " . $zip->numFiles . "\n";
		echo "status:" . $zip->status . "\n";
		$zip->close();
		unset($zip);
		header("Content-type: application/zip"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Pragma: no-cache"); 
		header("Expires: 0"); 
		readfile("$filename");
		unlink($filename);
		exit;
	}
}
?>