<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/do_not_show.php");
$obj_do_not_show		  =	new do_not_show();
$checckDoNotShow		  =	$obj_do_not_show->getRow("dns_type = 1 AND user_id=".$_SESSION['userId']);
?>
<div class="inner_content_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="inner_top_border">
					<div class="user_profile">
					<?php echo $objCommon->checkEmailverification();?>
						<div class="content">
							
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                                    <div class="pagination_box">
                                        <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
                                        <a href="<?php echo SITE_ROOT.$getUserDetails['usl_fameuz']?>"> My profile </a><i class="fa fa-caret-right"></i></a>
                                        <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                        <a href="javascript:;" class="active"> Invite Friends  </a>
                                        <?php
                                            include_once(DIR_ROOT."widget/notification_head.php");
                                        ?>
                                    </div>
                                    <div class="tab_white_search">
                                        <div class="section_search">
                                            <ul>
                                                <li class="visitor_head_box"><h5>Invite Friends</h5><span class="point-it"></span>
													<?php
													if($checckDoNotShow['dns_id']==''){
													?>
                                                    <div class="noticess">
                                                        <i class="fa fa-close pull-right"></i>
                                                        <p>You are about to follow these users. If he/she follows you back you will become friends, otherwise, stay followers</p>
                                                        <a href="javascript:;" class="do_not_show">Dont show this message again</a>
                                                    </div>
													<?php
													}
													?>
                                                </li>
                                            </ul>
                                            <span>
                                                <input type="text" placeholder="Start typing friend's name" id="invite_search" name="invite_search">
                                            </span>
                                        </div>
                                    </div>
                                <?php /*?><div class="whiteBg left100">
                                            <div class="row"><div class="col-sm-4 col-md-4 col-lg-3"><div class="visitor_head_box"><h5>Invite Friends</h5></div></div>
                                            <div class="col-sm-6"><div class="search"><div class="search_box"><input type="text" placeholder="Start typing friend's name" id="invite_search" name="invite_search"></div></div></div></div>
                                       </div><?php */?>
                                    <div class="clearfix"></div>
                                    <div class="visitor_list_box">
                                    </div>
                                </div>
                                <div class="col-sm-3 col-lg-sp-3">
									<?php
										include_once(DIR_ROOT."widget/right_static_ad_bar.php");
                                    ?>
                            </div>
                        </div>
					</div>
				</div>
                <div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	getresult('<?php echo SITE_ROOT?>ajax/invite_friends_list.php');
	 function getresult(url) {
		$.ajax({
			url: url,
			type: "GET",
			data:  {rowcount:$("#rowcount").val()},
			beforeSend: function(){
			$('#loader-icon').show();
			},
			complete: function(){
			$('#loader-icon').hide();
			},
			success: function(data){
				$(".visitor_list_box").html(data);
			},
			error: function(){} 	        
	   });
	}
	$('#invite_search').keyup(function(){
		var keyVal		=	$(this).val();
			$(".visitor_list_box").html('');
			getresult('<?php echo SITE_ROOT?>ajax/invite_friends_list.php?search='+keyVal+'&page=1');
	});

	$('.noticess i').click(function(){
		$('.noticess').hide();
	});
});
$(".do_not_show").on("click",function(){
	$.ajax({
		url:'<?php echo SITE_ROOT?>access/add_do_not_show.php',
		type:"POST",
		data:{dns_type:1},
		success: function(){
			$('.noticess').hide();
		}
	})
});
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>