<?php
include_once("includes/site_root.php");
include_once("includes/header.php");
include_once("includes/is_session.php");
include_once("class/testimonial.php");
include_once("class/common_class.php");

$objTestonial			   	=	new testimonial();
$objCommon		 		   =	new common();

$sql						 = "SELECT testimonial.* FROM testimonial ORDER by testi_id DESC ";
$contentList				 = $objTestonial->listQuery($sql);
?>
    <div class="community_section">
    	<div class="container relative">
        	<div class="community_img_bg"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/community_bg.png" alt="community_bg" /></div>
        	 <?php  if(count($contentList)>0){ ?>
            <div class="row padding_left">
            	<div class="col-sm-6">
                	<div class="head"><h4>A happy community</h4></div>
                </div>
            	<div class="col-sm-6 a_right"><!--<a href="#" class="view_all">View all  <i class="fa fa-play "></i></a>--></div>
            </div>
            <div class="row padding_left">
            <?php      foreach($contentList as $list){
					   $explAiImages	   		=	explode("/",$objCommon->html2text($list['t_image']));
					   $getAiImag1		  	  =	$explAiImages[0].'/thumb/'.$explAiImages[1];
				       $imgss = 'uploads/testimonial/imgs/'.$getAiImag1;  ?>
            	<div class="col-lg-4">
                	<div class="community_box">
                    	<p class="discription"><?php echo $objCommon->html2text($list['text']); ?><!--Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took--></p>
                        <p class="name"><?php echo $objCommon->html2text($list['fullname']); ?><!--Victor Moscardó--></p>
                        <p class="position"><span class="proffesion"></span><?php echo $objCommon->html2text($list['profession']); ?><!--Victor Moscardó--></p>
                        <p class="position"><i class="fa fa-map-marker"></i><?php echo $objCommon->html2text($list['place']); ?><!--Victor Moscardó--></p>
                        <div class="image "><img src="<?php echo $imgss; ?>" alt="community_img" /></div>
                        <div class="arrow_community"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/arrow_community.png" alt="arrow_community"></div>
                    </div>
                </div>
			<?php }}?>
         	</div>
        </div>
    </div>
     <div class="clearfix" style="margin-top:1%;"></div>
<?php
include_once(DIR_ROOT."includes/login_box.php");
include_once(DIR_ROOT."includes/footer.php");
?>