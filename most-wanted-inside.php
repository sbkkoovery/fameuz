<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
$memberCategory					=	$objCommon->esc($_GET['member_cat']);
$gender							=	$objCommon->esc($_GET['gender']);
if($gender=='male'){
	$genderStr					 =	" AND personal.p_gender =1 ";
	$genderHead					=	" Male > ";
}else if($gender =='female'){
	$genderStr					 =	" AND personal.p_gender =2 ";
	$genderHead					=	" Female > ";
}else{
	$genderStr					 =	" AND (personal.p_gender =1 OR personal.p_gender =2) ";
	$genderHead					=	"";
}
if($memberCategory){
	$getMostWantedCat			  =	$objUsers->getRowSql("SELECT c_id,c_name,c_alias FROM category WHERE c_alias LIKE '".$memberCategory."'");
	if($getMostWantedCat['c_id']==''){
		header("location:".SITE_ROOT.'most-wanted');
		exit;
	}
}else{
	header("location:".SITE_ROOT.'most-wanted');
	exit;
}
$totalLikeCountSQl			   =	$objUsers->getRowSql("SELECT COUNT(like_id) AS likeCount FROM likes WHERE like_status=1");
$totalLikeCount				  =	$totalLikeCountSQl['likeCount'];

?>
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
    <div class="inner_top_border">
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
          <div class="jobs-head">
          <div class="pagination_box_jobs pagination_box"> 
            <a href="<?php echo SITE_ROOT.'user/home'?>">Home <i class="fa fa-caret-right"></i></a>
            <a href="<?php echo SITE_ROOT?>most-wanted">Most Wanted <i class="fa fa-caret-right"></i></a>
            <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
            <a href="javascript:;" class="active">  <?php echo $genderHead.$objCommon->html2text($getMostWantedCat['c_name'])?> </a>   
			<?php
				include_once(DIR_ROOT."widget/notification_head.php");
			?>
           </div>
            
          </div>
<?php
$getFaceMonth					=	$objUsers->getRowSql("SELECT fmm_members FROM face_month_members WHERE fmm_cat=".$getMostWantedCat['c_id']);
if($getFaceMonth['fmm_members'] !=''){
	$trimFaceMember			  =	trim($getFaceMonth['fmm_members'],",");
	$getAllFaceMember			=	$objUsers->listQuery("SELECT user.user_id,user.first_name,user.last_name,user.email,profileImg.upi_img_url,social.usl_fameuz,personal.p_country,personal.p_city,nationality.n_name FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id  LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 LEFT JOIN personal_details AS personal ON user.user_id=personal.user_id LEFT JOIN nationality ON personal.n_id=nationality.n_id WHERE user.status=1 AND user.user_id IN (".$trimFaceMember.") ORDER BY FIELD(user.user_id,".$trimFaceMember.")");
}
?>
          <div class="tab_white_most_wanted">
            <div class="info-job search-main">
              <div class="job-head">
                <p>Faces of the Month</p>
              </div>
            </div>
            <div class="row">
			  <?php
			  foreach($getAllFaceMember as $allFaceMember){
				  if($allFaceMember['p_city'] != '' || $allFaceMember['p_country'] != ''){
						$friendPcity			 =	($allFaceMember['p_city'])?$objCommon->html2text($allFaceMember['p_city'])." ,":"";
				  }
			  ?>
              <div class="col-sm-4 col-md-4 col-lg-4">
                <div class="main-face-sec">
                  <div class="faceof-month-img"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($allFaceMember['upi_img_url'])?$allFaceMember['upi_img_url']:'profile_pic.jpg'?>" /> </div>
                  <div class="like_com">
                    <div class="name-sec">
                      <p><?php echo $objCommon->displayName($allFaceMember);?></p>
                      <p><?php echo $friendPcity.' '.$objCommon->html2text($allFaceMember['p_country'])?></p>
                    </div>
                  </div>
                </div>
              </div>
			  <?php
			  }
			  ?>
            </div>
          </div>
          <div class="most-wanted-detailed">
            <div class="row">
			  <?php
	$getMostProfileSql		=	"SELECT tab1.* FROM (
SELECT (((tab.countLike*100)/".$totalLikeCount.")+tab.reviewAvg) AS finalCount,tab.*,case when ((follow.follow_user1=".$_SESSION['userId']." or follow.follow_user2 =".$_SESSION['userId'].") and follow.follow_status=2) then 'friends' when (follow.follow_user1=".$_SESSION['userId']." and follow.follow_status=1) then 'following' when (follow.follow_user2=".$_SESSION['userId']." and follow.follow_status=1) then 'follower' else 'none' end as friendStatus,case when ((follow.follow_user1=".$_SESSION['userId']." or follow.follow_user2 =".$_SESSION['userId'].") and follow.follow_status=2) then '3' when (follow.follow_user1=".$_SESSION['userId']." and follow.follow_status=1) then '2' when (follow.follow_user2=".$_SESSION['userId']." and follow.follow_status=1) then '1' else '0' end as friendStatusOrder 
FROM( 
SELECT ucat.user_id,(AVG(reviews.review_rate)*20) AS reviewAvg,lk.likeCount AS countLike,profileImg.upi_img_url,user.first_name,user.last_name,user.display_name,user.email,personal.p_country,personal.p_city,social.usl_fameuz,ranking.main_cat_rank 
FROM user_categories AS ucat 
LEFT JOIN user_reviews AS reviews ON ucat.user_id = reviews.user_id_to 
LEFT JOIN users AS user ON ucat.user_id = user.user_id 
LEFT JOIN user_profile_image as profileImg ON ucat.user_id = profileImg.user_id AND profileImg.upi_status=1 
LEFT JOIN personal_details AS personal ON user.user_id = personal.user_id 
LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
LEFT JOIN ranking ON user.user_id = ranking.user_id
LEFT JOIN (SELECT likes.like_img_user_by,count(likes.like_id) AS likeCount FROM user_categories AS ucat LEFT JOIN likes ON ucat.user_id = likes.like_img_user_by WHERE likes.like_status=1 GROUP BY ucat.user_id) AS lk ON ucat.user_id = lk.like_img_user_by 
WHERE ucat.uc_c_id =".$getMostWantedCat['c_id']." ".$genderStr." GROUP BY ucat.user_id ) AS tab 
LEFT JOIN following as follow ON (tab.user_id = follow.follow_user1 and follow.follow_user1 !=".$_SESSION['userId']." ) or (tab.user_id = follow.follow_user2 and follow.follow_user2 !=".$_SESSION['userId']." ) 
ORDER BY friendStatusOrder DESC 
) AS tab1 GROUP BY user_id  ORDER BY finalCount DESC LIMIT 10";
	$getMostProfile				  		=	$objUsers->listQuery($getMostProfileSql);
	if(count($getMostProfile) >0){
	foreach($getMostProfile as $allMostProfile){
		if($allMostProfile['p_city'] != '' || $allMostProfile['p_country'] != ''){
			$friendPcity	=	($allMostProfile['p_city'])?$objCommon->html2text($allMostProfile['p_city'])." ,":"";
		}
	?>
	  <div class="col-sm-12 col-md-12 col-lg-6">
		<div class="seperate"><div class="row">
		  <div class="col-sm-4">
			<div class="most-img"><a href="<?php echo SITE_ROOT.$objCommon->html2text($allMostProfile['usl_fameuz'])?>"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($allMostProfile['upi_img_url'])?$allMostProfile['upi_img_url']:'profile_pic.jpg'?>" /></a></div>
		  </div>
		  <div class="col-sm-7">
			<div class="head-most-img">
			  <p><a href="<?php echo SITE_ROOT.$objCommon->html2text($allMostProfile['usl_fameuz'])?>"><?php echo $objCommon->displayName($allMostProfile)?> <!--<img src="<?php echo SITE_ROOT?>images/verify.png" />--></a></p>
			</div>
			<div class="post-info">
			  <p class="dim-me"><i class="fa fa-globe"></i><?php echo $friendPcity.' '.$objCommon->html2text($allMostProfile['p_country'])?></p>
			</div>
			<div class="rating_box most-wanted-rating">
			  <div class="rating_yellow" style="width:<?php echo round($allMostProfile['reviewAvg'])?>%"></div>
			</div>
			<div class="ranking-number">
			  <p>Ranking : <?php echo $allMostProfile['main_cat_rank']?></p>
			</div>
			<?php
			if($allMostProfile['user_id'] != $_SESSION['userId']){
			?>
			<div class="follow-wanted follow-btn">
			 <?php
				if($allMostProfile['friendStatus']=='friends'){
				?>
				<a href="javascript:;" class="friend"><i class="fa fa-star-o"></i> Friend</a>
				<?php
				}else if($allMostProfile['friendStatus']=='following'){
				?>
				<a href="javascript:;" class="following"><i class="fa fa-check"></i> Following</a>
				<?php
				}else if($allMostProfile['friendStatus']=='follower' || $allMostProfile['friendStatus']=='none'){
				?>
				<a href="javascript:;" class="follow has-spinner" data-friendid="<?php echo $objCommon->html2text($allMostProfile['user_id'])?>">
					<span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span>&nbsp;&nbsp;
					<i class="fa fa-plus"></i> Follow
				</a>
				<?php
				}
				?>
			</div>
			<?php
			}
			?>
		  </div>
		</div></div>
	  </div>
	<?php
	}
	}else{
		echo '<p>No models found...</p>';
	}
	?>
              <div class="col-sm-12 col-md-12 col-lg-6"> </div>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3">
          <div class="fixer">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                 <?php
				include_once(DIR_ROOT.'widget/right_search_main.php');
				?>
              </div>
            </div>
            <?php
  			include_once(DIR_ROOT.'widget/ad/right_ad_two_block.php');
            include_once(DIR_ROOT."widget/most_wanted_inside_filter.php");?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$(".head-filter-pre ul li").click(function(){
		$(".head-filter-pre ul li a").removeClass("active-filter");
		$(this).children("a").addClass("active-filter");
	});
});
$('.follow').on("click",function(e) {
	var friendid		=	$(this).data('friendid');
	$(this).addClass('active');
	$(this).find('.fa-plus').hide();
	that	=	this;
	$.get('<?php echo SITE_ROOT?>ajax/sent_friend_request.php',{"friendId":friendid},function(data){
	  setTimeout(function () {
			$(that).removeClass('active');
			$(that).parent().html('<a href="javascript:;" class="following"><i class="fa fa-check"></i> Following</a>');
			
		},3000);
	});
});
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
