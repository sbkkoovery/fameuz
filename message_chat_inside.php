<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/message.php");
$objMsg							=	new message();
$user_url					   	  =	$objCommon->esc($_GET['user_url']);
$sqlGetFriendDetails			   =	"SELECT user.user_id,user.first_name,user.last_name,user.display_name,profileImg.upi_img_url,user.email,social.usl_fameuz,case when (TIMESTAMPDIFF(MINUTE,chat.ucs_last_seen,'".date("Y-m-d H:i:s")."') >15 or chat.ucs_status=0) then '0' else '1' END AS chatStatus
FROM users AS user
LEFT JOIN user_chat_status AS chat ON user.user_id = chat.user_id 
LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
LEFT JOIN user_profile_image AS profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
WHERE user.status= 1 AND user.email_validation=1 AND social.usl_fameuz = '".$user_url."'";
$getFriendDetails			   	  =	$objUsers->getRowSql($sqlGetFriendDetails);
if($user_url ='' || $getFriendDetails['user_id'] =='' || $getFriendDetails['user_id']==$_SESSION['userId']){
	header("location:".SITE_ROOT."user/my-chats");
	exit;
}
?>
<link href="<?php echo SITE_ROOT?>css/auto_search.css" rel="stylesheet" type="text/css" />
<link href="<?php echo SITE_ROOT?>css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>css/lobibox.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>css/jquery.tokenize.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>css/message.css" />
<script src="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.js"></script>
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
  <div class="inner_top_border">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-sp-9">
      <div class="jobs-head">
        <div class="pagination_box_jobs pagination_box">
        <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
        <a href="<?php echo SITE_ROOT.$getUserDetails['usl_fameuz']?>"> My profile </a><i class="fa fa-caret-right"></i></a>
         <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
        <a href="javascript:;" class="active">Message</a>
        <?php
		if(isset($_SESSION['userId'])){
        	include_once(DIR_ROOT."widget/notification_head.php");
		}
        ?>
        </div>
       </div>
       <div class="white-compo">
       		<div class="message-container">
                <?php include_once(DIR_ROOT."widget/email_message_header.php");
				$count_per_page						 =	50;
				$currentPage							=	($_GET['page'])?$objCommon->esc($_GET['page']):1;
				$sql_all_messages					   =	"SELECT msg.*,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,attach.ma_attachement,attach.ma_attach_type
													FROM  message AS msg
													LEFT JOIN message_attachement AS attach ON msg.msg_id = attach.msg_id
													LEFT JOIN users AS user ON msg.msg_from = user.user_id
													LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
													LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
													WHERE ((msg.msg_from=".$_SESSION['userId']." AND msg.msg_to=".$getFriendDetails['user_id'].") OR (msg.msg_to=".$_SESSION['userId']." AND msg.msg_from=".$getFriendDetails['user_id'].")) AND msg.msg_status=1 ORDER BY msg.msg_created_date desc";
				$totalCount							 =	$objMsg->countRows($sql_all_messages);
				$totalPages							 =	ceil(($totalCount/$count_per_page));
				$orderFrom							  =	$count_per_page*($currentPage-1);
				$orderTo								=	$count_per_page*$currentPage;
				$sql_all_messages				   	  .=   " LIMIT ".$orderFrom.",".$count_per_page;
				$myAllMsg							   =	$objMsg->listQuery($sql_all_messages);			
				?>
				<div class="inbox_msges">
					<div class="chat_history">
						<div class="reportStartz">
							<h4>Chat With <?php echo $objCommon->displayName($getFriendDetails);?></h4>
							<div class="options_onCheck chat_inside_main">
								<span class="more_optn chat_inside">
									<a href="javascript:;" id="cancelThis">Cancel</a>
									<a href="javascript:;" id="deleteThis">Delete</a>
								</span>
							</div>
						</div>
						<?php
					$count_per_page; 
				if($totalCount>0){
					foreach($myAllMsg as $keyAllMsg=>$allMsg){ 
						if($chatDate != date("d-m-Y",strtotime($allMsg['msg_created_date']))){
							$chatDate			=	date("d-m-Y",strtotime($allMsg['msg_created_date']));
							if($keyAllMsg!=0){
								echo '</div>';//close div of class 'chat_show'
							}
							echo '<div class="date_chat"><span>'.$chatDate.'</span></div><div class="chats_show">';
						}
						$chatUserImg			 =	($allMsg['upi_img_url'])?$allMsg['upi_img_url']:'profile_pic.jpg';
						?>
						<div class="sender_page delMe">
								<div class="chk_delt">
									<input type="checkbox" class="checkEd" />
								</div>
								<div class="img_user">
									<img src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.$chatUserImg?>" />
								</div>
								<div class="userInfo_online_chat">
									<h4><?php echo $objCommon->displayName($allMsg);?><span class="time_chat"><?php echo date("h:i a",strtotime($objCommon->local_time($allMsg['msg_created_date'])))?></span></h4>
									<p><?php echo $objCommon->html2text($allMsg['msg_descr']);?></p>
								</div>
							</div>
				<?php 
					}
					echo '</div>';//close div of class 'chat_show'
					if($count_per_page < $totalCount){
						echo '<div class="loadMoreList"></div><a href="javascript:;" onclick="doShowChatList('.$getFriendDetails['user_id'].','.($currentPage+1).','.$count_per_page.','.$totalCount.');" class="loadClick">Load more...</a>';
					}
				}else{
					echo '<p style="padding: 20px;">No messages found...</p>';
				}
				?>
					</div>
				</div>
            </div>
       </div>
   </div>
      <?php
	include_once(DIR_ROOT."widget/right_online_memebers.php");
	?>
    </div>
    </div>
  </div>
</div>
<script src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery-ui.js" type="text/javascript"></script>
<script src="<?php echo SITE_ROOT_AWS_CDN?>js/chat_inside.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery.tokenize.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/fixed_sidebar.js"></script>
<?php include_once(DIR_ROOT."js/include/chat_inside.php");?>
<script type="text/javascript">
	$(window).load(function(e) {
		$('.fixer').fixedSidebar();
	});
	$(document).ready(function(){
		$('#tokenize').tokenize();
	});
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  