<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
$proCat								=	$objCommon->esc($_GET['proCat']);
$getCountryList						=	$objUsers->listQuery("SELECT country_id,country_name FROM country ORDER BY country_name");
$getCategoryList					   =	$objUsers->listQuery("SELECT c_id,c_name FROM category ORDER BY c_name");
$getDurationList					   =	$objUsers->listQuery("SELECT * FROM promotion_based_duration ORDER BY pbd_id LIMIT 3");
$userId								=	$_SESSION['userId'];
if($proCat=='profile'){
	$contentid						 =	$userId;
	$promobanerStr					 =	'<h3>Promote Profile</h3><p>Connect More With Your Profile</p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s</p>';
}else if($proCat=='video'){
	$vidId							 =	$objCommon->esc($_GET['vidId']);
	$getVideoDetails				   =	$objUsers->getRowSql("SELECT video_id,user_id,video_thumb,video_title FROM videos WHERE video_encr_id='".$vidId."'");
	if($getVideoDetails['user_id']!=$userId || $getVideoDetails['video_id'] ==''){
		header("location:".SITE_ROOT.'videos');
		exit;
	}
	$contentid						 =	$getVideoDetails['video_id'];
	$promobanerStr					 =	'<h3>Promote Video</h3><p>Connect More With Your Profile</p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s</p>';
}else if($proCat=='music'){
	$vidId							 =	$objCommon->esc($_GET['vidId']);
	$getMusicDetails				   =	$objUsers->getRowSql("SELECT music_id,user_id,music_thumb,music_title FROM music WHERE music_id='".$vidId."'");
	if($getMusicDetails['user_id']!=$userId || $getMusicDetails['music_id'] ==''){
		header("location:".SITE_ROOT.'music');
		exit;
	}
	$contentid						 =	$getMusicDetails['music_id'];
	$promobanerStr					 =	'<h3>Promote Music</h3><p>Connect More With Your Profile</p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s</p>';
}
echo $objCommon->displayMsg2();
?>
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>css/jquery.tokenize.css" />
<script src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery-ui.js"></script>
<link href="<?php echo SITE_ROOT?>css/jquery-ui.css" rel="stylesheet" type="text/css" />
<script>
 $(function() {
	$( "#datefrom" ).datepicker().datepicker("setDate", new Date());;
   
 });
</script>
<div class="inner_content_section">
<div class="top_status_bar">
	<div class="container">
		<h1 class="top_bar_head"><img src="<?php echo SITE_ROOT_AWS_IMAGES ?>images/loud_new.png" /> Promote Your <?php echo ucfirst($proCat)?></h1>
	</div>
</div>
	<div class="container">
		<div class="row">
		<form action="<?php echo SITE_ROOT?>access/promote.php" method="post">
				<div class="inner_top_border">
					<div class="user_profile">
					<?php echo $objCommon->checkEmailverification();?>
                        <div class="col-xs-12 col-sm-12">
							<div class="full_white_container">
                            	<div class="left_container">
                                	<div class="block_container">
                                    	<div class="user_head">
                                        	<div class="img_user_defined">
                                            	<img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($getUserDetails['upi_img_url'])?$getUserDetails['upi_img_url']:'profile_pic.jpg'?>" />
                                            </div>
                                            <div class="user_info_defined">
                                            	<h2 class="name_user">
                                                	<?php echo $displayName?>
                                                </h2>
                                                <span class="proffesion"><?php echo $objCommon->html2text($getUserDetails['c_name']);?></span>
                                            </div>
                                        <div class="clearfix"></div>
                                        </div>
										<?php
										if($proCat=='profile'){
											$getCoverImg1					  =	$objUsers->getRowSql("select photo_url,photo_id from user_photos where user_id=".$userId." and photo_set_main=1");
											$getCoverimage					 =	$getCoverImg1['photo_url'];
											$getCoverCat					   =	1;
											if($getCoverimage ==''){
												$getCoverImg1				  =	$objUsers->getRowSql("select ai_images,ai_id from album_images where user_id=".$userId." and ai_set_main=1");
												$getCoverimage				 =	$getCoverImg1['ai_images'];
												$getCoverCat				   =	2;
											}
										?>
                                        <div class="cover_user_photo" style="background-image:url('<?php echo SITE_ROOT_AWS_IMAGES.'uploads/albums/'.$getCoverimage?>')"></div>
                                        <div class="menus_under_photo">
                                        	<ul class="list-unstyled">
                                        		<li><a href="#">Updates</a></li>
                                        		<li><a href="#">Photos</a></li>
                                        		<li><a href="#">Videos</a></li>
                                        		<li><a href="#">Friends</a></li>
                                        	</ul>
                                        </div>
										<?php
										}else if($proCat=='video'){
											$thumbPathVideo			=	SITE_ROOT_AWS_VIDEOS.'uploads/videos/'.$objCommon->html2text($getVideoDetails['video_thumb']);
										?>
										 <div class="cover_user_photo videoCover">
										 	<a class="videoThumb" href="javascript:;"></a>
										 	<img src="<?php echo $thumbPathVideo ?>" />
										 </div>
										 <div class="videoPromoTitle"><?php echo $objCommon->html2text($getVideoDetails['video_title'])?></div>
										<?php
										}else if($proCat=='music'){
											$thumbPathMusic			=	($allTopMusic['music_thumb'])?$allTopMusic['music_thumb']:'audio-thump.jpg';
										?>
										 <div class="cover_user_photo videoCover">
										 	<a class="videoThumb" href="javascript:;"></a>
										 	<img src="<?php echo SITE_ROOT_AWS_MUSIC.'uploads/music/'.$thumbPathMusic;?>" />
										 </div>
										 <div class="videoPromoTitle"><?php echo $objCommon->html2text($getMusicDetails['music_title'])?></div>
										<?php
										}
										?>
                                    </div>
                                    <div class="block_container promote_info">
                                    	<ul class="list-unstyled">
                                    		<li><a href="#">Promote and growth your profile</a></li>
                                    		<li><a href="#">Maximize the power of your network</a></li>
                                    		<li><a href="#">Enhance your professional brands</a></li>
                                    		<li><a href="#">Find and get job, faster</a></li>
                                    		<li><a href="#">Content top talent directly</a></li>
                                    		<li><a href="#">Build relationships with prospective hirea</a></li>
                                    	</ul>
                                    </div>
                                </div>
								<input type="hidden" value="<?php echo $proCat?>" name="proType" />
								<input type="hidden" value="<?php echo $contentid?>" name="proContent" />
                                <div class="right_container">	
                                	<div class="promote_profile_Banner">
                                    	<?php echo $promobanerStr?>
                                    </div>
                                    <div class="options_contianer">
                                        <div class="optn_head">
                                            <h4>Audience</h4>
                                        </div>
                                    	<div class="row">
                                        	<div class="col-sm-6">
                                            	<div class="optn_sectn">
                                                	<h4 class="sub_head">Location</h4>
                                                    <div class="select_optns">
                                                            <select id="locationSel" multiple="multiple" class="tokenize-sample tokenize1" name="promoteloc[]">
																<?php
																foreach($getCountryList as $allCountries){
																?>
                                                                <option value="<?php echo $objCommon->html2text($allCountries['country_id'])?>"><?php echo $objCommon->html2text($allCountries['country_name'])?></option>
																<?php
																}
																?>
                                                            </select> 
                                                    </div>
                                                </div>
                                            </div>
                                        	<div class="col-sm-6">
                                            	<div class="optn_sectn">
                                                	<h4 class="sub_head">Category</h4>
                                                    <div class="select_optns">
                                                            <select id="categorySel" name="promotecat[]" multiple="multiple" class="tokenize-sample tokenize2">
                                                                <?php
																foreach($getCategoryList as $allCategory){
																?>
                                                                <option value="<?php echo $objCommon->html2text($allCategory['c_id'])?>"><?php echo $objCommon->html2text($allCategory['c_name'])?></option>
																<?php
																}
																?>
                                                            </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="optn_head">
                                            <h4>Budget</h4>
                                        </div>
                                        <div class="row">
                                        	<div class="col-sm-6">
                                            	<div class="optn_sectn">
                                                	<h4 class="sub_head">Duration</h4>
                                                    <div class="select_optns asTab">
                                                    	<ul>
															<?php
															foreach($getDurationList as $keyDuration=>$allDurationList){
															?>
                                                    		<li><a  class="durDef <?php echo ($keyDuration==0)?'durSelect':''?>" href="javascript:;" data-durid="<?php echo $objCommon->html2text($allDurationList['pbd_id'])?>" data-durdays="<?php echo $objCommon->html2text($allDurationList['pbd_days'])?>"><?php echo $objCommon->html2text($allDurationList['pbd_name'])?></a></li>
															<?php
															}
															?>
                                                    	</ul>
                                                    </div>
                                                    <div class="date_picker_untill">
                                                    	<span class="until_date">Run this ad from</span><input type="text" name="datefrom" id="datefrom" ><input type="hidden" name="datefto" id="datefto" /></div>
                                                    </div>
                                                </div>
											<div class="col-sm-6">
                                            	<div class="optn_sectn">
                                                	<h4 class="sub_head">Daily Budget</h4>
                                                    <div class="select_optns">
                                                           <span class="totalCost"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/album_preloader.GIF" /></span>
                                                        <span class="estimate"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                               		<div class="clearfix"></div>
                                </div>
                                <div class="footer_sec_promote">
                            	<div class="row">
                                	<div class="col-sm-6">
                                    	<div class="terms_conditions_promote">
                                        	<a href="#">Terms & conditions</a>
                                            <a href="#">Help Center</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                    	<div class="save_cancel pull-right">
                                        	<button type="submit" class="btn btn-default grn_btn">Make A Payment</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                         </div>
					</div>
		</form>
				</div>
			</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery.tokenize.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	updatePrice();
	changeDateDuration();
})
$('.tokenize1').tokenize({
 onAddToken:function(value, text, e){
 	 updatePrice();
 },
 onRemoveToken:function(value,e){
  updatePrice();
 }
});
$('.tokenize2').tokenize({
 onAddToken:function(value, text, e){
  updatePrice();
 },
 onRemoveToken:function(value,e){
  updatePrice();
 }
});
function updatePrice(){
		$(".totalCost").html('<img src="<?php echo SITE_ROOT?>images/album_preloader.GIF" />');
		var locationSel	=	$('#locationSel option:selected').size();
			categorySel	=	$('#categorySel option:selected').size();
			durationSel	=	$(".durSelect").data('durid');
			promoCat	   =	'<?php echo $proCat?>';
			if(locationSel >0 && categorySel >0){
				$(".grn_btn").show();
			}
		$.ajax({
				url:'<?php echo SITE_ROOT?>access/promote_cost_calulate.php',
				data:{locationSel:locationSel,categorySel:categorySel,durationSel:durationSel,promoCat:promoCat},
				type:"POST",
				success: function(data){
					$(".totalCost").html("USD "+data);
				}
			});
}
function changeDateDuration(){
	var days	=	$(".durSelect").data('durdays');
	var datefrom	=	$("#datefrom").val();
	 var date = new Date(datefrom),
    days = parseInt(days,10)+1;

   if(!isNaN(date.getTime())){
    date.setDate(date.getDate() + days);
	
	var fetchedDate	= date.toInputFormat();
	$("#datefto").val(fetchedDate);
    $(".estimate").html('Run this ad from : '+datefrom+' To : '+fetchedDate);
   }
 
}
   Date.prototype.toInputFormat = function() {
   var yyyy = this.getFullYear().toString();
   var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
   var dd  = this.getDate().toString();
   //var actualDate	= dd+'-'+mm+'-'+yyyy;
   var MyDateString =  ('0' + (mm)).slice(-2) + '/'+('0' + dd).slice(-2) + '/'+yyyy;
   return MyDateString;
   //return ((mm[1]?mm:"0"+mm[0]) +"-"+dd[1]?dd:"0"+dd[0]+ "-"+yyyy); // padding
}
$(".durDef").on("click",function(){
	$(".durDef").removeClass("durSelect");
	$(this).addClass("durSelect");
	updatePrice();
	changeDateDuration();
});
 $("#datefrom").on("change", function(){
   changeDateDuration();
  });
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>