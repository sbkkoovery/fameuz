<?php
@session_start();
include_once("includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/polaroids.php");
$objCommon				 	   =	new common();
$objPolaroids				 	=	new polaroids();

if($_SESSION['userId']==''){
	header("location:".SITE_ROOT.'my-polaroids');
	exit;
}
$userid = $_SESSION['userId'];
$poloid					    =	$objCommon->esc($_GET['poloid']);
$polaChunks = explode(",", $poloid);
$getpol1			             =	$objPolaroids->getRow("polo_id=".$polaChunks[0]);
$posImg1						 =	($getpol1['polo_url'])?SITE_ROOT.'uploads/albums/'.$objCommon->html2text($getpol1['polo_url']):SITE_ROOT.'images/composite-img.jpg';
$getpol2			             =	$objPolaroids->getRow("polo_id=".$polaChunks[1]);
$posImg2						 =	($getpol2['polo_url'])?SITE_ROOT.'uploads/albums/'.$objCommon->html2text($getpol2['polo_url']):SITE_ROOT.'images/composite-img.jpg';
$getpol3			             =	$objPolaroids->getRow("polo_id=".$polaChunks[2]);
$posImg3						 =	($getpol3['polo_url'])?SITE_ROOT.'uploads/albums/'.$objCommon->html2text($getpol3['polo_url']):SITE_ROOT.'images/composite-img.jpg';
$getpol4			             =	$objPolaroids->getRow("polo_id=".$polaChunks[3]);
$posImg4						 =	($getpol4['polo_url'])?SITE_ROOT.'uploads/albums/'.$objCommon->html2text($getpol4['polo_url']):SITE_ROOT.'images/composite-img.jpg';
$getpol5			             =	$objPolaroids->getRow("polo_id=".$polaChunks[4]);
$posImg5						 =	($getpol5['polo_url'])?SITE_ROOT.'uploads/albums/'.$objCommon->html2text($getpol5['polo_url']):SITE_ROOT.'images/composite-img.jpg';
$getpol6			             =	$objPolaroids->getRow("polo_id=".$polaChunks[5]);
$posImg6						 =	($getpol6['polo_url'])?SITE_ROOT.'uploads/albums/'.$objCommon->html2text($getpol6['polo_url']):SITE_ROOT.'images/composite-img.jpg';
$getpol7			             =	$objPolaroids->getRow("polo_id=".$polaChunks[6]);
$posImg7						 =	($getpol7['polo_url'])?SITE_ROOT.'uploads/albums/'.$objCommon->html2text($getpol7['polo_url']):SITE_ROOT.'images/composite-img.jpg';
$getpol8			             =	$objPolaroids->getRow("polo_id=".$polaChunks[7]);
$posImg8						 =	($getpol8['polo_url'])?SITE_ROOT.'uploads/albums/'.$objCommon->html2text($getpol8['polo_url']):SITE_ROOT.'images/composite-img.jpg';

$html='

<link href="http://fonts.googleapis.com/css?family=Roboto:500,900,100,300,700,400" rel="stylesheet" type="text/css">
<style>
	body{
		font-family: "Roboto", sans-serif;
	}
	.main-container{
		width:976px;
		height:690px;
		overflow:hidden;
		margin:0px auto;
		background:#f4f4f4;
		font-family: "Roboto", sans-serif;
	}
	.left-section, .right-section{
		width:450.5px;
		height:650px;
		overflow:hidden;
		float:left;
		padding:15px;
		border-top:5px solid;
		border-bottom:5px solid;
	}
	.left-section{
		background:#fff;
		border-left:5px solid;
		border-right:2.5px solid;
		margin-left:-0.5px;
	}
	.right-section{
		background:#fff;
		border-right:5px solid;
		border-left:2.5px solid;
		margin-left:-0.5px;
	}
	.image-sections{
		width:450.5px;
		height:650px;
		overflow:hidden;
		position:relative;
	}
	.image-sections img{
		width:450.5px;
		height:650px;
	}
	
	
	.top-right-section{
		padding-top:20px;
		padding-bottom:8px;
	}
	.right-bottom-section{
		padding-top:20px;
	}
	.bottom-left-section{
		padding-top:20px;
	}
	.top-left-section{
		padding-top:20px;
		padding-bottom:8px;
	}
	
	.top-left, .top-right{
		width:215px;
		height:317.5px;
		float:left;
		overflow:hidden;
	}
	.top-left{
		padding-right:7.5px;
	}
	.top-right{
		padding-left:7.5px;
	}
	.top-left img, .top-right img{
		width:215px;
		height:317.5px;
	}
	.log-section{
		width:102px;
		margin:0 auto;
		height:45px;
	}
	
	.info-user-section ul li{
		list-style:none;
		font-size:12px;
		margin-bottom:5px;
	}
	.info-user-section ul li span{
		font-weight:bold;
	}
	.polarid-styl {
    background: #ececec none repeat scroll 0 0;
    border-bottom: 1px solid #bebebe;
	height:200px;
    margin: 2px 3px;
    padding: 10px 10px 40px;
    
}
	
	
	
	.clearfix{
	  float:none;
	  clear:both;
	 }
</style>
<body>
	<div class="main-container">
    	<div class="left-section" style="width:450.5px; height:650px;">
        	<div class="top-left-section">
			   <div class="top-left" style="width:217.5px; height:280px; border-bottom: .5px solid #bebebe; background: #ececec none repeat scroll 0 0; ">
			   	          	<img src="'.$posImg1.'" style="width:200px; height:245px;" />
               </div>
                <div class="top-right" style="width:217.5px; height:280px; border-bottom: 1px solid #bebebe; background: #ececec none repeat scroll 0 0; ">
                	<img src="'.$posImg2.'" style="width:200px; height:245px;" />
                </div>
				<div class="clearfix"></div>
			 </div>
			 <div class="bottom-left-section">
			   <div class="top-left" style="width:217.5px; height:280px; border-bottom: .5px solid #bebebe; background: #ececec none repeat scroll 0 0; ">
                	<img src="'.$posImg3.'" style="width:210px; height:245px;" />
                </div>
                <div class="top-right" style="width:217.5px; height:280px; border-bottom: .5px solid #bebebe; background: #ececec none repeat scroll 0 0; ">
                	<img src="'.$posImg4.'" style="width:210px; height:245px;" />
                </div>
				<div class="clearfix"></div>
			 </div>
			
        </div>
        <div class="right-section" style="width:450.5px; height:650px; ">
        	<div class="top-right-section">
            	<div class="top-left" style="width:217.5px; height:280px; border-bottom: .5px solid #bebebe; background: #ececec none repeat scroll 0 0; ">
                	<img src="'.$posImg5.'" style="width:200px; height:245px;" />
                </div>
                <div class="top-right" style="width:217.5px; height:280px; border-bottom: .5px solid #bebebe; background: #ececec none repeat scroll 0 0;">
                	<img src="'.$posImg6.'" style="width:210px; height:245px;" />
                </div>
				<div class="clearfix"></div>
            </div>
            <div class="right-bottom-section">
			    <div class="top-left" style="width:217.5px; height:280px; border-bottom: .5px solid #bebebe; background: #ececec none repeat scroll 0 0; ">
                	<img src="'.$posImg7.'" style="width:210px; height:245px;" />
                </div>
            	
                <div class="top-right" style="width:217.5px; height:280px; border-bottom: .5px solid #bebebe; background: #ececec none repeat scroll 0 0;">
                	<img src="'.$posImg8.'" style="width:210px; height:245px;" />
                </div>
				<div class="clearfix"></div>
            </div>
        </div>
		<div class="clearfix"></div>
    </div></body>';
//echo $html;

$polaroidcard	=	DIR_ROOT."uploads/polaroids/".$userid.'.pdf';
	//$compocard  =	$objCommon->displayName($getUserDetails).'_'.$compUser.'.pdf';
	include(DIR_ROOT."phptoPDF/mpdf.php");
	$mpdf=new mPDF('utf-8', 'A4-L',0,0,0,0,11);
	$mpdf->WriteHTML($html);
	$mpdf->Output($polaroidcard,'D');
	exit;
?>