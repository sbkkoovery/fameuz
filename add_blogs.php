<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/blogs.php");
$objBlogs					  =	new blogs();
$editId						=	$objCommon->esc($_GET['edit']);
$getBlogCategories			 =	$objBlogs->listQuery("select * from blog_category order by bc_order");
if($editId){
	$getContent			 	=	$objBlogs->getRowSql("select * from blogs where blog_id=".$editId." and user_id=".$_SESSION['userId']);
}
?>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery.validate.js"></script>
<script src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery-ui.js"></script>
<link href="<?php echo SITE_ROOT?>css/cmxform.css" rel="stylesheet" type="text/css" />
<link href="<?php echo SITE_ROOT?>css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT_AWS_CDN?>js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <div class="inner_content_section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="inner_top_border">
                        <div class="user_profile">
						<?php echo $objCommon->checkEmailverification();?>
                            <div class="content">
                                <div class="pagination_box">
                                	<a href="<?php echo SITE_ROOT?>user/home">Home <i class="fa fa-caret-right"></i></a>
                                    <a href="<?php echo SITE_ROOT?>blog"> Blog <i class="fa fa-caret-right"></i></a>
        							 <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                    <a href="javascript:;" class="active"> Create Blogs</a>
                                    <?php
								include_once(DIR_ROOT."widget/notification_head.php");
								?>
                                </div>
                                <div class="row">
                                <div class="col-lg-sp-2">
                                <?php
								include_once(DIR_ROOT."includes/profile_left.php");
								?>
                                </div>
                                <div class="col-lg-sp-10">
                                <div class="create-blog-info">
                                	<div class="arw-down"></div>
                                    <div class="blog-info-head">
                                    	<p>Your Blog Matters</p>
                                    </div>
                                    <div class="blog-ifo-desc">
                                    	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic</p>
                                        <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic 
                                        <a href="#">Visit our Privacy Center</a></p>
                                    </div>
                                </div>
                                <div class="post_job_main"><div class="main-cont">
                                	<?php
									echo $objCommon->displayMsg();
									?>
									<h5>Create Blogs</h5>
									<?php
									if($getUserDetails['email_validation']==1){
									?>
                                    <form class="post_job_form" id="post-job-form" method="post" action="<?php echo SITE_ROOT.'access/create_blogs.php?action=create_blogs'?>" enctype="multipart/form-data">
                                      <div class="row">
                                          <div class="col-sm-6">
                                              <div class="form-group">
                                              	 <div class="input-group">
                                                      <span class="input-group-addon" id="basic-addon1"><i class="fa fa-tag"></i></span>
                                                      <input type="text" class="form-control" id="blog_title" placeholder="Title" name="blog_title" value="<?php echo $objCommon->html2text($getContent['blog_title'])?>" />
                                                  </div>
                                              </div>
                                      </div>
									   <div class="col-sm-6">
                                           <div class="form-group">
                                              	 <div class="input-group">
                                                      <span class="input-group-addon" id="basic-addon1"><i class="fa fa-tag"></i></span>
                                                        <select name="bc_id" id="bc_id"  class="form-control">
                                                            <option value="">Select Category</option>
                                                            <?php
                                                            foreach($getBlogCategories as $allBlogCat){
                                                            ?>
                                                            <option value="<?php echo $allBlogCat['bc_id']?>" <?php echo ($allBlogCat['bc_id']==$getContent['bc_id'])?'selected':''?>><?php echo $objCommon->html2text($allBlogCat['bc_title'])?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                </div>
                                          </div>
                                      </div>
									  <div class="col-sm-12">
                                          <div class="form-group">
                                             <textarea class="wysihtml5 form-control" rows="9" name="blog_descr" placeholder="Description" id="blog_descr"><?php echo $objCommon->html2text($getContent['blog_descr'])?></textarea>
                                         </div>
                                      </div>
									  <div class="col-sm-6">
                                          <div class="form-group">
                                              <input  type="file" id="blog_img" class="filestyle" name="blog_img" data-icon="false" />
                                          </div>
                                      </div>
									  <div class="col-sm-6">
                                          <div class="form-group">
                                              	 <div class="input-group">
                                                      <span class="input-group-addon" id="basic-addon1"><i class="fa fa-tag"></i></span>
                                                      <select  class="form-control"  name="blog_status" id="blog_status">
                                                        <option value="1" <?php echo ($getContent['blog_status']==1 || $editId =='')?'selected="selected"':''?>>Enable</option>
                                                        <option value="0" <?php echo ($getContent['blog_status']===0)?'selected="selected"':''?>>Disable</option>
                                                      </select>
                                              </div>
                                          </div>
                                      </div>
									  <div class="col-sm-12">
                                          <div class="form-group">
                                            <input type="hidden" name="editId" value="<?php echo $getContent['blog_id']?>" />
                                              <button type="submit" class="btn btn-default btn-log">Post Blog</button>
                                          </div>
                                      </div>
                                  </div>
								</form>
								<?php
									}else{
										echo '<div role="alert" class="alert alert-warning"><strong>Warning ! </strong> You do not have sufficient permissions to access this page.</div>';
									}
								?>
                                <div class="clr"></div>            
                            </div></div>
                            </div>
                            </div> 
                             
                            <div class="clr"></div>                        
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/bootstrap-filestyle.min.js"></script>
<script>
$(document).ready(function(e) {
	$("#blog_img").filestyle();
	$(".bootstrap-filestyle input").attr("placeholder", "Attach Your File");
	$('.wysihtml5').wysihtml5();
});
 	$("#post-job-form").validate({
		rules: {
			blog_title: "required",
			bc_id: "required",
			blog_descr: "required"
		},
		messages: {
			blog_title: 'Can\'t be empty',
			bc_id: 'Can\'t be empty',
			blog_descr: 'Can\'t be empty'
		}
});
</script>
 
<?php
include_once(DIR_ROOT."includes/footer.php");
?>