<?php
#====================================================================================================
# FUNCTION: Generiert einen Zufallsstring
#====================================================================================================
function getRandomValue($length = 40) {
	return strtoupper(substr(hash('sha512', uniqid(rand(0, 9999).sha1(time()))), 0, $length));
}

#====================================================================================================
# FUNCTION: Entfernt alle Zeilenumbrüche und Tabulatoren aus einem String
#====================================================================================================
function removeLineBreaksAndTabs($mixed, $replace = NULL) {
	if(is_array($mixed)) {
		return array_map(__FUNCTION__, $mixed);
	}

	return is_string($mixed) ? str_replace(["\r\n", "\r", "\n", "\t"], $replace, $mixed) : $mixed;
}
?>