<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
$user_url		=	$objCommon->esc($_GET['user_url']);
if($user_url ==''){ header("location:".SITE_ROOT); exit; }
if($user_url==$getUserDetails['usl_fameuz'])
{
	include_once(DIR_ROOT."includes/worked_with_me.php");
}else{
	$getMyFriendDetails			=	$objUsers->getRowSql("SELECT user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz
	FROM users AS user 
	LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
	LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
	WHERE user.status=1 AND user.email_validation=1  AND social.usl_fameuz='".$user_url."'");
	if($getMyFriendDetails['user_id']){
		if($getMyFriendDetails['display_name']){
			$displayNameprofile	 =	$objCommon->html2text($getMyFriendDetails['display_name']);
		}else if($getMyFriendDetails['first_name']){
			$displayNameprofile	 =	$objCommon->html2text($getMyFriendDetails['first_name']);
		}else{
			$exploEmailFriend	 	=	explode("@",$objCommon->html2text($getMyFriendDetails['email']));
			$displayNameprofile	  =	$exploEmailFriend[0];
		}
		include_once(DIR_ROOT."includes/worked_with.php");
	}
}
include_once(DIR_ROOT."includes/footer.php");
?>
  