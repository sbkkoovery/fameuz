<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/games.php");
$objGames								=	new games();
$game_search_cat						 =	$objCommon->esc($_GET['category']);
$game_search_game_cat					=	$objCommon->esc($_GET['game_category']);
if($game_search_cat =='' && $game_search_game_cat ==''){
	header("location:".SITE_ROOT."games");
	exit;
}
$gameSearchSql					   	   =	"";
if($game_search_cat =='my-games'){
	$game_search_head					=	'My Games';
	$gameSearchSql					   =	"SELECT games.*,cat.gc_name 
												FROM games 
												LEFT JOIN game_category AS cat ON games.gc_id = cat.gc_id 
												LEFT JOIN game_player AS player ON games.g_id = player.g_id
												WHERE  games.g_status=1 AND player.user_id =".$_SESSION['userId']."
												ORDER BY player.gp_created desc LIMIT 32";
}else if($game_search_cat =='new-games'){
	$game_search_head					=	'New Games';
	$gameSearchSql					   =	"SELECT games.*,cat.gc_name 
												FROM games 
												LEFT JOIN game_category AS cat ON games.gc_id = cat.gc_id 
												WHERE  games.g_status=1
												ORDER BY games.g_created desc LIMIT 32";
}else if($game_search_cat =='popular-games'){
	$game_search_head					=	'Popular Games';
	$gameSearchSql					   =	"SELECT games.*,cat.gc_name,count(player.gp_id) AS playerCount 
												FROM games 
												LEFT JOIN game_category AS cat ON games.gc_id = cat.gc_id 
												LEFT JOIN game_player AS player ON games.g_id = player.g_id
												WHERE  games.g_status=1 
											    GROUP BY games.g_id ORDER BY playerCount desc,games.g_created desc LIMIT 32";
}else if($game_search_cat !=''){
	$game_search_head					=	'Refine results for "'.$game_search_cat.'"';
	$gameSearchSql					   =	"SELECT games.*,cat.gc_name 
												FROM games 
												LEFT JOIN game_category AS cat ON games.gc_id = cat.gc_id 
												WHERE  games.g_status=1 AND (games.g_name LIKE '%".$game_search_cat."%'  OR games.g_alias LIKE '%".$game_search_cat."%' OR cat.gc_name LIKE '%".$game_search_cat."%' OR cat.gc_alias LIKE '%".$game_search_cat."%')
												ORDER BY games.g_created desc LIMIT 32";
}
if($game_search_game_cat){
	$getGameCat						  =	$objGames->getRowSql("SELECT gc_name FROM game_category WHERE gc_id=".$game_search_game_cat);
	$game_search_head					=	$getGameCat['gc_name'];
	$gameSearchSql					   =	"SELECT games.*,cat.gc_name 
												FROM games 
												LEFT JOIN game_category AS cat ON games.gc_id = cat.gc_id 
												WHERE  games.g_status=1 AND cat.gc_id=".$game_search_game_cat."
												ORDER BY games.g_created desc LIMIT 32";
}
$getGamesList							=	$objGames->listQuery($gameSearchSql);
?>
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
  <div class="inner_top_border">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-sp-9">
      <div class="jobs-head">
        <div class="pagination_box_jobs pagination_box">
        <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
        <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
 	     <a href="javascript:;" class="active"> Games</a>
       <?php
        include_once(DIR_ROOT."widget/notification_head.php");
        ?>
        </div>
       </div>
        <div class="tab_white_most_wanted">
			<?php
			include_once(DIR_ROOT."widget/games_head_widget.php");
			?>      
		</div>
        <div class="games_categories">
            <div class="top-category">
				<p><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/game-info.png" /><?php echo $game_search_head?></p>
			</div>
            <div class="row">
            	<div class="col-sm-12">
                	<div class="owl-carousel_2 ovr">
                    <?php
					if(count($getGamesList) >0){
						foreach($getGamesList as $allGetGames){
						?>
							<div class="col-sm-3">
							<div class="item_1">
								 <a href="<?php echo SITE_ROOT.'games/show/'.$objCommon->html2text($allGetGames['g_alias']).'-'.$objCommon->html2text($allGetGames['g_id'])?>">
									<img src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/games/img/'.$objCommon->getThumb($allGetGames['g_img'])?>" />
								 </a>
									 <span class="game-info">
										<p><a href="<?php echo SITE_ROOT.'games/show/'.$objCommon->html2text($allGetGames['g_alias']).'-'.$objCommon->html2text($allGetGames['g_id'])?>"><?php echo $objCommon->html2text($allGetGames['g_name'])?></a></p>
										<span class="rating_box">
											<span class="rating_yellow" style="width:40%"></span>
										</span>
										<span class="game-cat"><p><?php echo $objCommon->html2text($allGetGames['gc_name'])?></p></span>
									</span>
								</div>
								</div>
							   <?php 
								}
						 }else{
							 echo '<p>No games found....</p>';
						 }
						?>
						   </div>
                   
                </div>
            </div>
        </div>
      </div>
	  <?php
	  include_once(DIR_ROOT."widget/games_right_widget.php");
	  ?>
    </div>
    </div>
  </div>
</div>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  