<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
$user_url		=	$objCommon->esc($_GET['user_url']);
if($user_url==$getUserDetails['usl_fameuz'])
{
	header("location:".SITE_ROOT."user/my-album");
	exit;
}else{
$getMyFriendDetails			=	$objUsers->getRowSql("SELECT user.user_id,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz
															FROM users AS user 
															LEFT JOIN user_social_links AS social ON user.user_id=social.user_id															
															WHERE user.status=1 AND user.email_validation=1  AND social.usl_fameuz='".$user_url."'");					
}
?>
<link href="<?php echo SITE_ROOT?>css/my-album.css" rel="stylesheet" type="text/css">
<link href="<?php echo SITE_ROOT?>fameuz_lightbox/fameuz_lightbox.css" rel="stylesheet">
<link href="<?php echo SITE_ROOT?>css/image_popup.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.css" />
<script src="<?php echo SITE_ROOT?>jw_player/jwplayer.js"></script>
<link href="<?php echo SITE_ROOT?>jw_player/jw_player.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery-ui.js"></script>
<script src="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>fameuz_lightbox/famuez_lightbox.js"></script>
<div class="inner_content_section">
  <div class="container">
    <div class="inner_top_border">
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
          <div class="user_profile"> <?php echo $objCommon->checkEmailverification();?>
            <div class="content">
              <div class="pagination_box"> <a href="<?php echo SITE_ROOT.'user/home'?>">Home <i class="fa fa-caret-right"></i></a>
              <a href="<?php echo SITE_ROOT.$getMyFriendDetails['usl_fameuz']?>"> <?php echo $objCommon->displayName($getMyFriendDetails);?> <i class="fa fa-caret-right"></i></a> 
        	<a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
              <a href="#" class="active">Photos</a>
                <?php
				include(DIR_ROOT."widget/notification_head.php");
			  ?>
              </div>
              <div class="profile_content album-content">
                <div class="top-album-section">
                <div class="album">
                <a href="javascript:;" class="all_photos">Albums</a>
                <img class="arrow_down_tab arrow1" src="<?php echo SITE_ROOT?>images/arrow-down-grey.png" />
                </div>
                <div class="album">
                 <a href="javascript:;" onclick="showAllImages()">Photos</a>
                  <img class="arrow_down_tab arrow2" src="<?php echo SITE_ROOT?>images/arrow-down-grey.png" />
                </div>
                <div class="album">
                 <a href="javascript:;" onclick="showAlbumVideo()">Videos</a>
                  <img class="arrow_down_tab arrow3" src="<?php echo SITE_ROOT?>images/arrow-down-grey.png" />
                </div>
                 </div>
                <div class="profile_border_box">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="load_album_content">
                        <?php //include_once(DIR_ROOT.'includes/album_list_home_page_friends.php');?>
                      </div>
					  <!-- POPUP STARTS -->
						<link href="<?php echo SITE_ROOT?>perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet">
						<link href="<?php echo SITE_ROOT?>css/image_popup.css" rel="stylesheet">
						<script type="text/javascript" src="<?php echo SITE_ROOT?>perfect-scrollbar/perfect-scrollbar.js"></script>
						<div id="pop_up" class="cd-popup">
							
						</div>
						<!-- POPUP ENDS -->
                    </div>
                  </div>
                </div>
              </div>
              <div class="clr"></div>
              </div>
          </div>
        </div>
        <?php
		include_once(DIR_ROOT."widget/right_static_ad_bar.php");
		?>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(e) {
	var hash		=	document.location.hash.substr(1);
	if(hash =='videos'){
		$(".arrow_down_tab").hide();$(".arrow3").show();
		$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/album_video_list_friends.php?friendId=<?php echo $getMyFriendDetails['user_id']?>');
	}else{
		$(".arrow_down_tab").hide();$(".arrow1").show();
		$(".load_album_content").load('<?php echo SITE_ROOT?>includes/album_list_home_page_friends.php?friendId=<?php echo $getMyFriendDetails['user_id']?>');
	}
	$(".all_photos").click(function(){
		$(".arrow_down_tab").hide();$(".arrow1").show();
		$(".load_album_content").load('<?php echo SITE_ROOT?>includes/album_list_home_page_friends.php?friendId=<?php echo $getMyFriendDetails['user_id']?>');
	});
	$("#add_timeline_photos").click(function(){
		$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/timeline_photo_page_friends.php?friendId=<?php echo $getMyFriendDetails['user_id']?>');
	});
});		
function showAlbum(aId){
	$(".load_album_content").html('<div class="load_album_preloader"></div>');
	$(".arrow_down_tab").hide();$(".arrow1").show();
	$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/album_photo_page_friends.php?friendId=<?php echo $getMyFriendDetails['user_id']?>&albumId='+aId,function(){
		$(".load_album_preloader").hide();
	});
}
function showWallImg(){
	$(".load_album_content").html('<div class="load_album_preloader"></div>');
	$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/timeline_photo_page_friends.php?friendId=<?php echo $getMyFriendDetails['user_id']?>',function(){
		$(".load_album_preloader").hide();
	});
}
function showAlbumVideo(){
	$(".load_album_content").html('<div class="load_album_preloader"></div>');
	$(".arrow_down_tab").hide();$(".arrow3").show();
	$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/album_video_list_friends.php?friendId=<?php echo $getMyFriendDetails['user_id']?>',function(){
		$(".load_album_preloader").hide();
	});
}
function showAllImages(){
	$(".load_album_content").html('<div class="load_album_preloader"></div>');
	$(".arrow_down_tab").hide();$(".arrow2").show()
	$(".load_album_content").load('<?php echo SITE_ROOT?>includes/all_photos_list_home_page_friends.php?friendId=<?php echo $getMyFriendDetails['user_id']?>',function(){
		$(".load_album_preloader").hide();
	});
}
function showProfileImg(){
	$(".load_album_content").html('<div class="load_album_preloader"></div>');
	$(".load_album_content").load('<?php echo SITE_ROOT?>ajax/profile_photo_page_friends.php?friendId=<?php echo $getMyFriendDetails['user_id']?>',function(){
		$(".load_album_preloader").hide();
	});
}
$('.status_box').on("click",'#change_status',function(e) {
                $('#status_option').fadeToggle();
				return false;
});
$('#status_option').on("click",'.check_Status',function(e) {
		var dataVal	=	$(this).attr('data-value');
		if(dataVal){
			$.get('<?php echo SITE_ROOT?>ajax/change_online_chat_status.php',{"dataVal":dataVal},function(data1){
				$(".status_box").html(data1);
			});
		}
	});
	 $(function () {
 	 $('[data-toggle="tooltip"]').tooltip();
	});
				
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>