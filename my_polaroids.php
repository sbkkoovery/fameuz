<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
$getPolaroids				  =	$objUsers->listQuery("SELECT polo_id,polo_url FROM polaroids WHERE user_id=".$_SESSION['userId']." ORDER BY polo_order ASC");
include_once(DIR_ROOT."js/include/fameuz_lightbox_library.php");			
?>
<link href="<?php echo SITE_ROOT?>fameuz_lightbox/fameuz_lightbox.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.masonry.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery-migrate-1.2.0.min.js"></script>
<script type="text/javascript">
	$(function(){
		$(window).load(function() {$('.album_maso').masonry();});
			$(window).resize(function() {$('.album_maso').masonry();});
	});
	</script>

<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
  <div class="inner_top_border">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
	  <?php echo $objCommon->checkEmailverification();?>
      <div class="jobs-head">
        <div class="pagination_box_jobs pagination_box">
        <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
        <a href="<?php echo SITE_ROOT.$getUserDetails['usl_fameuz']?>"> My profile </a><i class="fa fa-caret-right"></i></a>
        <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
      <a href="javascript:;" class="active"> My Polaroids </a>
      
       <?php
        include_once(DIR_ROOT."widget/notification_head.php");
        ?>
        </div>
       </div>
       <div class="polarids">
	   <div class="custom-upload text-right">
        <a href="#create_pdf_pop" onclick="makePdf();"><span class="upload-btn pull-right"><img src="<?php echo SITE_ROOT?>images/pdf.png"><span class="">Make PDF</span></span></a>       
       <a class="callModalUploadPos" href="<?php echo SITE_ROOT?>user/my-album?show=polaroids"><span class="upload-btn"><i class="fa fa-camera"> </i><span class="">Upload Polaroids</span></span></a></div>
       		<div class="ploarids-sec">
            	<div class="row">
				<div id="masonry" class="album_maso masonry" >
                <?php 
				if(count($getPolaroids) >0){
				foreach($getPolaroids as $allPolaroids){
				?>
                	<div class="album_maso_album_maso_box masonry-brick" >
                    	<div class="polarid-items">
                        	<div class="polarid-img">
                            	<a href="javascript:;" class="lightBoxs" data-contentId="<?php echo $allPolaroids['polo_id']?>" data-contentType="8"><img src="<?php echo SITE_ROOT?>uploads/albums/<?php echo $objCommon->html2text($allPolaroids['polo_url'])?>" /></a>
                            </div>
                            <!-- <div class="col-md-6">
                             <input type="checkbox" name="poloroidselect[]" value="<?php echo $allPolaroids['polo_id']?>"/>
   			                </div>-->
                        </div>
                    </div>
                    <?php 
					}
				}else{
					echo $objCommon->displayNameSmall($getMyFriendDetails,12).' has no Polaroids yet';
				}
				?>
				</div>
                </div>
            </div>
       </div>
        </div>
        <div class="col-sm-3 col-lg-sp-3">
			<?php
				include_once(DIR_ROOT."widget/right_static_ad_bar.php");
            ?>
        </div>
    </div>
    </div>
  </div>
</div>
<div class="remodal create_pdf_pop_box" style="max-width:980px;" data-remodal-id="create_pdf_pop">
  <div class="pop_new_album">
    <div class="pop_new_album_head"> Select 8 Polaroids and click button "Make PDF"</div>
           <div class="pop_make_pdf ret_pdf_val"> </div>     
           
    </div>
  </div>
</div>
<script type="text/javascript">
$('.album_maso').masonry();
$(document).ready(function(){
	$('body').fameuzLightbox({
				class : 'lightBoxs',
				sidebar: 'default',
				photos:{
					imageLoadUrl: '<?php echo SITE_ROOT.'ajax/popUpImageUser.php'?>',
					data_attr	: ['data-contentId','data-contentType','data-contentAlbum'],
					likeUrl	: '<?php echo SITE_ROOT?>ajax/like.php',
					shareUrl: '<?php echo SITE_ROOT?>ajax/share.php',
					workedUrl: '<?php echo SITE_ROOT?>ajax/worked_together.php',
					commentBoxUrl: '<?php echo SITE_ROOT.'widget/pop_image_comment_box.php' ?>',
					comments :{
						commentActionUrl:'<?php echo SITE_ROOT?>access/post_comment.php?action=add_comment',
						commentDelete: '<?php echo SITE_ROOT?>access/delete_comment.php',
					}
				},
				videos : {
					videoLoadUrl: '<?php echo SITE_ROOT.'ajax/popUpVideoUser.php'?>',
					data_attr	: ['data-vidEncrId'],
					likeUrl		: '<?php echo SITE_ROOT?>ajax/like_video.php',
					shareUrl	: '<?php echo SITE_ROOT?>ajax/share_video.php',
					commentBoxUrl:'<?php echo SITE_ROOT.'widget/pop_video_comment_box.php' ?>',
					comments :{
						commentActionUrl:'<?php echo SITE_ROOT?>access/post_video_comment.php?action=add_comment',
					}
				},
				skin: {
					next	: '<img src="<?php echo SITE_ROOT ?>fameuz_lightbox/images/next.png">',
					prev	: '<img src="<?php echo SITE_ROOT ?>fameuz_lightbox/images/prev.png">',
					reset	: '<i class="fa fa-refresh"></i>',
					close	: '<img src="<?php echo SITE_ROOT ?>fameuz_lightbox/images/close.png" width="15">',
					loader	: '<?php echo SITE_ROOT ?>images/ajax-loader.gif',
					review	: '<i class="fa fa-chevron-right"></i>',
					video	: '<?php echo SITE_ROOT ?>jw_player/six/six.xml',
				}
			});
});
function makePdf(){
	var inst = $.remodal.lookup[$('[data-remodal-id=create_pdf_pop]').data('remodal')];
	$.get('<?php echo SITE_ROOT?>ajax/make_pdf_pop.php',{},function(data){
		$(".ret_pdf_val").html(data);
		
	});
	inst.open(); 
	/*var check = document.getElementsByName('poloroidselect[]');
    var selectedRows = [];
    for (var i = 0, l = check.length; i < l; i++) {
    if (check[i].checked) {
        selectedRows.push(check[i].value);
          
	}
    }
	if(selectedRows.length==8){
window.location.href	=	'<?php echo SITE_ROOT?>poloroid-print.php?poloid='+selectedRows;

	}
	else
	{
	alert("Please select 8 polaroids");
	}*/
}
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  