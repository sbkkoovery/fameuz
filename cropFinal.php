<!DOCTYPE html>
<html lang="en">
<head>
  <title>Aspect Ratio with Preview Pane | Jcrop Demo</title>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />

<script src="js/jquery.min.js"></script>
<script src="js/jquery.Jcrop.js"></script>
<script type="text/javascript">

function cropImage(){
	if (parseInt($('#w').val())){
		$(".preview-container").html('<div class="text-center"><img src="images/loading.gif"/></div>');
		var targetFile	=	$("#target").attr("src");
		var xvalue		=	$('#x').val();
		var yvalue		=	$('#y').val();
		var wvalue		=	$('#w').val();
		var hvalue		=	$('#h').val();
		$.get("ajax/imageCrope.php",{x:xvalue,y:yvalue,w:wvalue,h:hvalue,src:targetFile},
		function(data){
			if(data!=""){
				d = new Date();
				var imageFile	=	'<img src="uploads/profile_images/'+data+'?id='+d.getTime()+'" class="jcrop-preview" alt="Preview" />';
				$(".preview-container").html(imageFile);
			}
		});
	}else{
		alert('Please select a crop region then press submit.');
	}
}
</script>
<link rel="stylesheet" href="css/jquery.Jcrop.css" type="text/css" />
<style type="text/css">

/* Apply these styles only when #preview-pane has
   been placed within the Jcrop widget */
.jcrop-holder #preview-pane {
  display: block;
  position: absolute;
  z-index: 2000;
  top: 10px;
  right: -280px;
  padding: 6px;
  border: 1px rgba(0,0,0,.4) solid;
  background-color: white;

  -webkit-border-radius: 6px;
  -moz-border-radius: 6px;
  border-radius: 6px;

  -webkit-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
  display:none;
}

/* The Javascript code will set the aspect ratio of the crop
   area based on the size of the thumbnail preview,
   specified here */
#preview-pane .preview-container {
  width: 230px;
  height: 312px;
  overflow: hidden;
}

</style>
<script src="js/jquery.upload-1.0.2.js"></script>
<script>
$(document).ready(function(){
$('input[type=file]').change(function() {
    $(this).upload('access/upload-profile-image.php?ID=1', function(res) {
        //alert('File uploaded');
		//alert(res);
		if(res!=""){
			d = new Date();
			var uploadedContent	=	'<img src="uploads/profile_images/'+res+'?'+d.getTime()+'" id="target" />';
			var uploadedPreview	=	'<img src="uploads/profile_images/'+res+'?'+d.getTime()+'" class="jcrop-preview" alt="Preview" />';
			$(".uploadedImage").html(uploadedContent);
			$(".preview-container").html(uploadedPreview);
			jQuery(function($){

    // Create variables (in this scope) to hold the API and image size
    var jcrop_api,
        boundx,
        boundy,

        // Grab some information about the preview pane
        $preview = $('#preview-pane'),
        $pcnt = $('#preview-pane .preview-container'),
        $pimg = $('#preview-pane .preview-container img'),

        xsize = $pcnt.width(),
        ysize = $pcnt.height();
    
    console.log('init',[xsize,ysize]);
    $('#target').Jcrop({
      onChange: updatePreview,
      onSelect: updateCoords,
      aspectRatio: xsize / ysize
    },function(){
      // Use the API to get the real image size
      var bounds = this.getBounds();
      boundx = bounds[0];
      boundy = bounds[1];
      // Store the API in the jcrop_api variable
      jcrop_api = this;
      jcrop_api.setSelect([130,20,130+350,65+385]);
      jcrop_api.setOptions({ bgFade: true });
      jcrop_api.ui.selection.addClass('jcrop-selection');

      // Move the preview into the jcrop container for css positioning
      //$preview.appendTo(jcrop_api.ui.holder);
    });

    function updatePreview(c){
      if (parseInt(c.w) > 0)
      {
		$("#preview-pane").show();
        var rx = xsize / c.w;
        var ry = ysize / c.h;

        $pimg.css({
          width: Math.round(rx * boundx) + 'px',
          height: Math.round(ry * boundy) + 'px',
          marginLeft: '-' + Math.round(rx * c.x) + 'px',
          marginTop: '-' + Math.round(ry * c.y) + 'px'
        });
      }
    };
	function updateCoords(c){
		$('#x').val(c.x);
		$('#y').val(c.y);
		$('#w').val(c.w);
		$('#h').val(c.h);
	 };
	function checkCoords(){
		if (parseInt($('#w').val())) return true;
		alert('Please select a crop region then press submit.');
		return false;
	  };

  });
		}
    });
});
});
</script>
</head>
<body>

<div class="container">
<div class="row">
<div class="span12">
<div class="jc-demo-box">
	<div class="uploadedImage"></div>
  
<div class="hiddenFields">
	<input type="hidden" id="x" name="x" />
    <input type="hidden" id="y" name="y" />
    <input type="hidden" id="w" name="w" />
    <input type="hidden" id="h" name="h" />
</div>
  <div id="preview-pane">
    <div class="preview-container"></div>
  </div>    
    <input type="button" onClick="cropImage();" value="Crop Image" class="btn btn-large btn-inverse" />
<div class="clearfix"></div>
<input type="file" name="photoimg"/>
</div>
</div>
</div>
</div>

    
 
</body>
</html>