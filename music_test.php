<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
?>
  <link href="<?php echo SITE_ROOT?>css/music.css" rel="stylesheet" type="text/css">
    <div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="user_profile">
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                                    <div class="content music-pagein">
                                        <div class="pagination_box">
                                            <div class="row">
                                                <div class="col-sm-7"><div class="video_search">
                                                	<div class="row">
                                                    	<div class="col-sm-9 no-padding-r">
                                                    <input type="text" placeholder="Search Music" />
                                                    	</div>
                                                    	<div class="col-sm-3 no-padding-l">
                                                    <button class="btn btn-group bt-submit"><i class="fa fa-search hidden-lg"></i><span class="hidden-sm hidden-md hidden-xs">Search Music</span></button>
                                                    	</div>
                                                    </div>
                                                </div>
                                                </div>
                                                <div class="col-sm-1 col-md-1 col-lg-2 alt-padding-r alt-padding-l">
                                                    <div class="upload"><a href="<?php echo SITE_ROOT.'music/upload'?>"  data-toggle="tooltip" data-placement="bottom" title="Upload Music"><i class="fa fa-volume-up"></i><span class="hidden-sm hidden-md">Upload Music</span></a></div>
                                                    </div>
                                                    <div class="col-sm-3 col-md-3 col-lg-2 alt-padding-l">
                                                    <div class="option">
                                                        <p><span>Select Category</span><i class="fa fa-chevron-down hidden-sm hidden-md "></i><i class="fa fa-bars hidden-lg"></i></p>
                                                        <ul>
                                                            <li><a href="#">Popular This Week</a></li>
                                                            <li><a href="#">Popular This Month</a></li>
                                                        </ul>
                                                  </div>
                                              </div>
                                                <div class="col-sm-1 not">
                                              <?php
												include_once(DIR_ROOT."widget/notification_head.php");
												?>	
                                                </div>
                                                </div>
                                      </div>
                                        <div class="clr"></div>            
                                    </div>
                                </div> 
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3 music-page">
                                    <div class="sidebar">
                                        <div class="search_box"><form action="<?php echo SITE_ROOT.'user/search-result'?>" method="get"><input type="text" placeholder="Search "  name="keywordSearch" /></form></div>
                                    </div>
                                </div> 
                            </div>
                            <div class="clr"></div>
                            <div class="videos">
                                <div class="row">
                                    <div class="col-sm-12 col-sm-12 col-md-12 col-lg-sp-9">
                                        <div class="video_left c">
                                            <div class="audio">
                                                <div class="row">
                                                    <div class="col-sm-12 no-padding-r">
                                                         <script src="<?php echo SITE_ROOT?>jw_player/jwplayer.js"></script>
																<link href="<?php echo SITE_ROOT?>jw_player/jw_player_music.css" rel="stylesheet" type="text/css">
																<div class="player"><div id="myElement"></div></div>
																<script>
																jwplayer("myElement").setup({
																file: "http://example.com/uploads/myAudio.m4a",
																'playlist': [{
																'file': '<?php echo SITE_ROOT?>uploads/music/2015-08-02/1438514655_2.mp3',
																'image':'<?php echo SITE_ROOT?>uploads/music/2015-08-02/thumb/1438514757_2.jpg',
																'title': 'The first video',
																'created':'sandeep'
																},{
																'file': '<?php echo SITE_ROOT?>uploads/music/2015-08-02/1438515728_2.mp3',
																'image':'<?php echo SITE_ROOT?>uploads/music/2015-08-02/thumb/1438515769_2.jpg',
																'title': 'The second video',
																'created':'sandeep nambiar'
																},{
																'file': '<?php echo SITE_ROOT?>uploads/music/2015-08-02/1438516046_2.mp3',
																'image':'<?php echo SITE_ROOT?>uploads/music/2015-08-02/thumb/1438516462_8.jpg',
																'title': 'The third video',
																'created':'sandeep nambiar'
																},{
																'file': '<?php echo SITE_ROOT?>uploads/music/2015-08-02/1438516271_2.mp3',
																'image':'<?php echo SITE_ROOT?>uploads/music/2015-08-02/thumb/1438517570_8.jpg',
																'title': 'The fourth video',
																'created':'sandeep nambiar'
																}],
																repeat: 'list',
																skin: "<?php echo SITE_ROOT?>jw_player/six/six_music.xml",
																width: 830,
																height: 450,
																listbar: {
																  position: 'right',
																  width: 270,
																	height: 450,

																},
																});
																</script>
                                                    </div>
                                                    
                                            </div>
                                        </div>
                                       </div>
                                    </div>
                                    <div class="hidden-sm hidden-md col-lg-sp-3">
                                        <div class="video_right">
                                            <div class="left50"><h6>Sponsored <i class="fa fa-bullhorn"></i></h6></div>
                                            <div class="left50 a_right"><a href="#" class="create">Create Ad</a></div>
                                            <div class="clr"></div>
                                            <div class="ads">
                                                <div class="ad">
                                                    <a href="#"><img src="images/ad1.jpg" alt="ad" /></a>
                                                    <h4>Men's NIKE Shoes -80%</h4>
                                                    <h6>xxxxxxxxx.com</h6>
                                                    <p>See how this unique UAE website can get you 
                                                    Top Brand Shoes &amp; Clothing at up to 80% off</p>
                                                </div>
                                                <div class="ad">
                                                    <a href="#"><img src="images/ad1.jpg" alt="ad" /></a>
                                                    <h4>Men's NIKE Shoes -80%</h4>
                                                    <h6>xxxxxxxxx.com</h6>
                                                    <p>See how this unique UAE website can get you 
                                                    Top Brand Shoes &amp; Clothing at up to 80% off</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clr"></div>  
                                    
                                </div>
                            </div>
                            <div class="clr"></div>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
$(document).ready(function(e) {
	$(function () {
		$('[data-toggle="tooltip"]').tooltip();
	});
	$('#playlist').perfectScrollbar();
	
	$(".option").click(function(){
		$(this).find("ul").slideToggle();
	});
	$(".item").click(function(){
	//$(".item").removeClass("activemp3");
		$(this).toggleClass("activemp3");
	});
	$(".delete").click(function(){
		if($(".item").hasClass('activemp3')){
			if (confirm("Do you want to delete")){
				$(".activemp3").remove();
			}
		}
	});
	$(".close_btn").click(function(){
		if (confirm("Do you want to delete")){
			$(this).parent(".item").remove();
		}
	});
});
	</script>
<?php
include_once(DIR_ROOT."includes/login_box.php");
include_once(DIR_ROOT."includes/footer.php");
?>
    