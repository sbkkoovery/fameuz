<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/model_details.php");
include_once(DIR_ROOT."class/following.php");
include_once(DIR_ROOT."class/user_reviews.php");
$objUserReviews  =	new user_reviews();
$objFollowing	=	new following();
$objModelDetails =	new model_details();
$user_url		=	$objCommon->esc($_GET['user_url']);
$editId		  =	$objCommon->esc($_GET['edit']);
if($user_url!=$getUserDetails['usl_fameuz'])
{
	$getMyFriendDetails			=	$objUsers->getRowSql("SELECT user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,privacy.uc_p_phone,privacy.uc_p_alt_phone,privacy.uc_p_dob,personal.p_dob,personal.p_gender,personal.p_phone,personal.p_alt_phone,personal.p_alt_email,personal.p_about,personal.p_ethnicity,personal.p_languages,personal.p_country,personal.p_city,nationality.n_name,ucat.uc_s_id,ucat.uc_m_id,ucat.uc_c_id,ethnicity.ethnicity_name
															FROM users AS user 
															LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
															LEFT JOIN personal_details AS personal ON user.user_id=personal.user_id
															LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
															LEFT JOIN user_privacy AS privacy ON user.user_id=privacy.user_id
															LEFT JOIN nationality ON personal.n_id=nationality.n_id
															LEFT JOIN ethnicity ON personal.p_ethnicity=ethnicity.ethnicity_id
															LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
															WHERE user.status=1 AND user.email_validation=1  AND social.usl_fameuz='".$user_url."'");
	if($getMyFriendDetails['user_id']){
	$getFriendStatus		   =	$objFollowing->getRow("(follow_user1=".$getUserDetails['user_id']." OR follow_user2=".$getUserDetails['user_id'].") AND (follow_user1=".$getMyFriendDetails['user_id']." OR follow_user2=".$getMyFriendDetails['user_id'].")");
	if(($getFriendStatus['follow_status']==1 && $getFriendStatus['follow_user1']==$_SESSION['userId']) || $getFriendStatus['follow_status']==2){ 
		$friendStatus =1; 
	}else { 
		$friendStatus=0; 
	}
	if($getMyFriendDetails['display_name']){
		$displayNameFriend	 =	$objCommon->html2text($getMyFriendDetails['display_name']);
	}else if($getMyFriendDetails['first_name']){
		$displayNameFriend	 =	$objCommon->html2text($getMyFriendDetails['first_name']);
	}else{
		$exploEmailFriend	  =	explode("@",$objCommon->html2text($getMyFriendDetails['email']));
		$displayNameFriend	 =	$exploEmailFriend[0];
	}
	$getModelDetails		   =	$objModelDetails->getRowSql("SELECT 														det.model_dis_id,chest.mc_name,collar.mcollar_name,disc.model_dis_name,dress.mdress_name,eyes.me_name,hair.mhair_name,height.mh_name,hips.mhp_name,jacket.mj_name,shoes.ms_name,trousers.mt_name,waist.mw_name
																FROM model_details AS det
																LEFT JOIN model_chest AS chest ON det.mc_id=chest.mc_id
																LEFT JOIN model_collar AS collar ON det.mcollar_id=collar.mcollar_id
																LEFT JOIN model_dress AS dress ON det.mdress_id=dress.mdress_id
																LEFT JOIN model_disciplines AS disc ON det.model_dis_id=disc.model_dis_id
																LEFT JOIN model_eyes AS eyes ON det.me_id=eyes.me_id
																LEFT JOIN model_hair AS hair ON det.mhair_id=hair.mhair_id
																LEFT JOIN model_height AS height ON det.mh_id=height.mh_id
																LEFT JOIN model_hips AS hips ON det.mhp_id=hips.mhp_id
																LEFT JOIN model_jacket AS jacket ON det.mj_id=jacket.mj_id
																LEFT JOIN model_shoes AS shoes ON det.ms_id=shoes.ms_id
																LEFT JOIN model_trousers AS trousers ON det.mt_id=trousers.mt_id
																LEFT JOIN model_waist AS waist ON det.mw_id=waist.mw_id
																WHERE det.user_id=".$getMyFriendDetails['user_id']);
																
?>
<link href="<?php echo SITE_ROOT?>css/user-view.css" rel="stylesheet" type="text/css">
<div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="user_profile">
						<?php echo $objCommon->checkEmailverification();?>
                            <div class="row"><div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9"><div class="content">
                            	<div class="pagination_box">
                                	<a href="<?php echo SITE_ROOT?>user/home">Home <i class="fa fa-caret-right"></i></a>
									<a href="#"><?php echo $displayNameFriend?> <i class="fa fa-caret-right"></i></a>
                                	<a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
									<a href="javascript:;" class="active">About Me</a>
                                    <?php
								include_once(DIR_ROOT."widget/notification_head.php");
								?>
                                </div>
                            	<?php
								include_once(DIR_ROOT."includes/my_friend_page_left.php");
								?>
                            	<div class="profile_content">
									<div class="row">
										<div class="col-md-12">
											<div class="book_form_outer">
												<div class="book_form_inner">
													<h2>About Me</h2>
													<div class="row">
														<div class="col-md-12">
															<p><?php echo ($getMyFriendDetails['p_about'])?$objCommon->html2text($getMyFriendDetails['p_about']):'No contents found';?></p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
                                    
                                </div>
                            	<div class="clr"></div>            
                            </div></div> 
                               <?php include_once(DIR_ROOT."includes/my_friend_page_right.php");?>    </div>                
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
	}else{
		header("location:".SITE_ROOT.'user/home');
	}
}
include_once(DIR_ROOT."includes/footer.php");
?>
  