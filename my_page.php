<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
require_once DIR_ROOT.'bigpipe/include/classes/class.bigpipe.php';
require_once DIR_ROOT.'bigpipe/include/classes/class.pagelet.php';
require_once DIR_ROOT.'bigpipe/include/functions.php';
$PageletRed = new Pagelet();
$preloaderframe = '';
for($i=0; $i<3; $i++){
	$preloaderframe .= '
	<section id="newsfeed">
		<div class="feedLoader">
			<div class="feedContainer">
				<div class="row">
					<div class="col-sm-1">
						<div class="img-s"></div>
					</div>
					<div class="col-sm-11">
						<div class="rectangle-md">
							<div class="rectangle"></div>
							<div class="rectangle-sm pull-right"></div>
						</div>
						<div class="rectangle-lg">
							<div class="rectangle-sm"></div>
							<div class="rectangle-sm"></div>
							<div class="rectangle-sm"></div>
						</div>
						<div class="rectangle-lg">
							<div class="rectangle-full"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="line"></div>
			<div class="rectangle-wd">
				<div class="rectangle-sm-down"></div>
				<div class="rectangle-sm-down"></div>
				<div class="rectangle-sm-down"></div>
			</div>
		</div>
	</section>';
}
$PageletRed->addHTML($preloaderframe);
$PageletRed->addCSS(SITE_ROOT.'bigpipe/static/red.php?cachebuster='.getRandomValue());
$PageletRed->addJS(SITE_ROOT.'bigpipe/static/delayJS.php?cachebuster='.getRandomValue());
$PageletRed->addJSCode("$('#newsfeed').load('".SITE_ROOT."widget/my_time_line.php',function(){ $('.feedLoader').hide(); }); ");
include_once(DIR_ROOT."class/model_details.php");
include_once(DIR_ROOT."class/following.php");
include_once(DIR_ROOT."class/user_reviews.php");
$objUserReviews  =	new user_reviews();
$objFollowing	=	new following();
$objModelDetails =	new model_details();
$user_url		=	$objCommon->esc($_GET['user_url']);
if($user_url==$getUserDetails['usl_fameuz'])
{
	
	$getMyReviews						  =	$objUserReviews->listQuery("SELECT review.review_msg,review.review_rate,review.review_date,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url from user_reviews AS review LEFT JOIN users AS user ON review.user_id_by=user.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 WHERE review.review_status=1 AND review.review_user_to_status=1 AND review.user_id_to='".$_SESSION['userId']."'  ORDER BY review.review_date desc LIMIT 3");
	include_once(DIR_ROOT."includes/my_page.php");
}else{
	$getMyFriendDetails			=	$objUsers->getRowSql("SELECT social.usl_fb,social.usl_twitter,social.usl_gplus,social.usl_instagram,social.usl_others,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,profileImg.upi_id,privacy.uc_p_phone,privacy.uc_p_alt_phone,privacy.uc_p_dob,personal.p_dob,personal.p_gender,personal.p_phone,personal.p_alt_phone,personal.p_alt_email,personal.p_about,personal.p_ethnicity,personal.p_languages,personal.p_country,personal.p_city,nationality.n_name,ucat.uc_s_id,ucat.uc_m_id,ethnicity.ethnicity_name,cat.c_name,cat.c_book_by_me,cat.c_book_me,ranking.main_cat_rank,privacy_set.ps_value,vidInto.vi_url,vidInto.vi_thumb,vidInto.vi_type
															FROM users AS user 
															LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
															LEFT JOIN personal_details AS personal ON user.user_id=personal.user_id
															LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
															LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
															LEFT JOIN user_privacy AS privacy ON user.user_id=privacy.user_id
															LEFT JOIN nationality ON personal.n_id=nationality.n_id
															LEFT JOIN ethnicity ON personal.p_ethnicity=ethnicity.ethnicity_id
															LEFT JOIN ranking ON user.user_id = ranking.user_id
															LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
															LEFT JOIN privacy_settings AS privacy_set ON user.user_id = privacy_set.user_id
															LEFT JOIN video_introduction AS vidInto ON user.user_id = vidInto.user_id
															WHERE user.status=1 AND user.email_validation=1  AND social.usl_fameuz='".$user_url."'");
//echo "<pre>";print_r($getMyFriendDetails);echo "</pre>";
	if($getMyFriendDetails['user_id']){
	$getFriendStatus		   =	$objFollowing->getRow("(follow_user1=".$getUserDetails['user_id']." OR follow_user2=".$getUserDetails['user_id'].") AND (follow_user1=".$getMyFriendDetails['user_id']." OR follow_user2=".$getMyFriendDetails['user_id'].")");
	if(($getFriendStatus['follow_status']==1 && $getFriendStatus['follow_user1']==$_SESSION['userId']) || $getFriendStatus['follow_status']==2){ 
		$friendStatus =1; 
	}else { 
		$friendStatus=0; 
	}
//----------------------------------Privacy Settings---------------------------------------------------------------------------------------------------------------------
if($getMyFriendDetails['ps_value']){
	$showPage				  =	0;
	$privacySettingArr		 =	unserialize($getMyFriendDetails['ps_value']);
	$profileDisplayArr		 =	$privacySettingArr['profile_see'];
	$disply_see_arr			=	$privacySettingArr['disply_see'];
	if((is_array($profileDisplayArr) && in_array("4",$profileDisplayArr))){
		$showPage			  =	0;
	}else {
		if((is_array($profileDisplayArr) && in_array("1",$profileDisplayArr)) || count($profileDisplayArr)>0){
		$showPage			  =	1;
		}
		if(is_array($profileDisplayArr) && in_array("2",$profileDisplayArr) && $friendStatus ==1){
			$showPage		  =	1;
		}
		if(is_array($profileDisplayArr) && in_array("3",$profileDisplayArr) && $getFriendStatus['uc_m_id'] ==1){
			$showPage		  =	1;
		}
	}
}else{
	$showPage				  =	1;
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
	$displayNameFriend		 =	$objCommon->displayNameSmall($getMyFriendDetails,12);
	$getModelDetails		   =	$objModelDetails->getRowSql("SELECT 														det.model_dis_id,chest.mc_name,collar.mcollar_name,disc.model_dis_name,dress.mdress_name,eyes.me_name,hair.mhair_name,height.mh_name,hips.mhp_name,jacket.mj_name,shoes.ms_name,trousers.mt_name,waist.mw_name
																FROM model_details AS det
																LEFT JOIN model_chest AS chest ON det.mc_id=chest.mc_id
																LEFT JOIN model_collar AS collar ON det.mcollar_id=collar.mcollar_id
																LEFT JOIN model_dress AS dress ON det.mdress_id=dress.mdress_id
																LEFT JOIN model_disciplines AS disc ON det.model_dis_id=disc.model_dis_id
																LEFT JOIN model_eyes AS eyes ON det.me_id=eyes.me_id
																LEFT JOIN model_hair AS hair ON det.mhair_id=hair.mhair_id
																LEFT JOIN model_height AS height ON det.mh_id=height.mh_id
																LEFT JOIN model_hips AS hips ON det.mhp_id=hips.mhp_id
																LEFT JOIN model_jacket AS jacket ON det.mj_id=jacket.mj_id
																LEFT JOIN model_shoes AS shoes ON det.ms_id=shoes.ms_id
																LEFT JOIN model_trousers AS trousers ON det.mt_id=trousers.mt_id
																LEFT JOIN model_waist AS waist ON det.mw_id=waist.mw_id
																WHERE det.user_id=".$getMyFriendDetails['user_id']);
$getMyReviews				=	$objUserReviews->listQuery("SELECT review.review_msg,review.review_rate,review.review_date,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url from user_reviews AS review LEFT JOIN users AS user ON review.user_id_by=user.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 WHERE review.review_status=1 AND review.review_user_to_status=1  AND review.user_id_to='".$getMyFriendDetails['user_id']."'  ORDER BY review.review_date desc LIMIT 3");
																
	include_once(DIR_ROOT."includes/my_friend_page.php");
	include_once(DIR_ROOT."includes/profile_visitors_add.php");
	}else{
		header("location:".SITE_ROOT.'user/home');
	}
}
include_once(DIR_ROOT."includes/footer.php");
?>
  