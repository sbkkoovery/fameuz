<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/jobs.php");
include_once(DIR_ROOT."class/pagination-class.php");
$objJobs					 =	new jobs();
if($_GET['job_title'] !='' || $_GET['job_skills'] !='' || $_GET['job_place'] !='' || $_GET['job_companies'] !='' || $_GET['job_order'] !='' || $_GET['job_category'] !=''){
	$job_title			   =	$objCommon->esc($_GET['job_title']);
	$job_skills			  =	$objCommon->esc($_GET['job_skills']);
	$job_place			   =	$objCommon->esc($_GET['job_place']);
	$job_companies		   =	$objCommon->esc($_GET['job_companies']);
	$job_order			   =	$objCommon->esc($_GET['job_order']);
	$job_category			=	$objCommon->esc($_GET['job_category']);
	$sql_job_cond			=	'';
	if($job_title){
		$job_titleArr		=	explode(",",$job_title);
		if(count($job_titleArr)>0){
			$sql_job_cond			.=	" and ( ";
			foreach($job_titleArr as $keyAlljob=>$allJobTitle){
				if($keyAlljob ==0){
					$sql_job_cond	.=	" job_title  LIKE '%".$allJobTitle."%'";
				}else{
					$sql_job_cond	.=	" or job_title  LIKE '%".$allJobTitle."%'";
				}	
			}
			$sql_job_cond	.=	" ) ";
		}
	}
	if($job_skills){
		$job_skillsArr		=	explode(",",$job_skills);
		if(count($job_skillsArr)>0){
			$sql_job_cond			.=	" and ( ";
			foreach($job_skillsArr as $keyAllSkill=>$allSkillTitle){
				if($keyAllSkill ==0){
					$sql_job_cond	.=	" job_skill  LIKE '%".$allSkillTitle."%'";
				}else{
					$sql_job_cond	.=	" or job_skill  LIKE '%".$allSkillTitle."%'";
				}	
			}
			$sql_job_cond	.=	" ) ";
		}
	}
	if($job_place){
		$job_placeArr		=	explode(",",$job_place);
		if(count($job_placeArr)>0){
			$sql_job_cond			.=	" and ( ";
			foreach($job_placeArr as $keyAllPlace=>$allPlaceTitle){
				if($keyAllPlace ==0){
					$sql_job_cond	.=	" job_country  LIKE '%".$allPlaceTitle."%' or job_state  LIKE '%".$allPlaceTitle."%' or job_city  LIKE '%".$allPlaceTitle."%'";
				}else{
					$sql_job_cond	.=	" or job_country  LIKE '%".$allPlaceTitle."%' or job_state  LIKE '%".$allPlaceTitle."%' or job_city  LIKE '%".$allPlaceTitle."%'";
				}	
			}
			$sql_job_cond	.=	" ) ";
		}
	}
}
	if($job_companies){
		$job_companiesArr		=	explode(",",$job_companies);
		if(count($job_companiesArr)>0){
			$sql_job_cond			.=	" and ( ";
			foreach($job_companiesArr as $keyAllCompany=>$allCompanyTitle){
				if($keyAllCompany ==0){
					$sql_job_cond	.=	" job_company  LIKE '%".$job_company."%'";
				}else{
					$sql_job_cond	.=	" or job_company  LIKE '%".$job_company."%'";
				}	
			}
			$sql_job_cond	.=	" ) ";
		}
	}
	if($job_category){
		$sql_job_cond		.=	" and job_category LIKE '%,".$job_category.",%'";
	}
$num_results_per_page		= 	10;
$num_page_links_per_page 	 = 	5;
$pg_param 					= 	"";
$pagesection				 = 	'';
$sql_list_job				=	"SELECT j.*,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz,profileImg.upi_img_url
										FROM jobs AS j 
										LEFT JOIN users AS user  ON j.user_id	=	user.user_id
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										WHERE ((j.job_start <= CURDATE() AND j.job_end >= CURDATE()) OR j.job_start > CURDATE())  AND j.job_status=1 ".$sql_job_cond;
if($job_order){
	if($job_order == 1){
		$sql_list_job		.=	" ORDER BY job_created asc";
	}else if($job_order == 2){
		$sql_list_job		.=	" ORDER BY job_created desc";
	}
}
pagination($sql_list_job, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$listJobs				   =	$objJobs->listQuery($paginationQuery);
$getJobCat				  =	$objJobs->listQuery("SELECT jc_name,jc_id FROM job_category ORDER BY jc_order");
?>
<link href="<?php echo SITE_ROOT?>css/pagination.css" rel="stylesheet" type="text/css">
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
  <div class="inner_top_border">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
      <div class="jobs-head">
        <div class="pagination_box_jobs pagination_box">
            <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
            <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
             <a href="#" class="active"> Jobs </a><span class="result_job">&nbsp;( <?php echo number_format($objJobs->countRows($sql_list_job))?> Result )</span>
       <div class="post_jobs">
       <a href="<?php echo SITE_ROOT?>user/post-jobs"><i class="fa fa-briefcase"></i>&nbsp;Post Jobs</a>
       </div>
        <?php
        include_once(DIR_ROOT."widget/notification_head.php");
        ?>
        </div>
       </div>
       <div class="job_results_display">
       <div class="grey_sec">
       <img class="fix-down" src="<?php echo SITE_ROOT_AWS_IMAGES?>images/arrow-white.png" />
       <div class="content-jobs">
		<form method="get" action="">
			<div class="row">
				<div class="col-sm-6">
					<label for="job_title">I'm looking for...</label>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<input type="text" class="form-control checkme" name="job_title" id="job_title"  placeholder="job title - e.g., models,photographers" value="<?php echo $objCommon->html2text($job_title);?>" />
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<input type="text" class="form-control checkme" name="job_skills"  placeholder="Any skills or Keywords" value="<?php echo $objCommon->html2text($job_skills);?>" />
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<input type="text" class="form-control" name="job_place"  placeholder="City, State, or Country" value="<?php echo $objCommon->html2text($job_place);?>" />
					</div>
				</div>
				<div class="col-sm-2">
					<button type="submit" id="hide-advance" class="btn btn-default"  <?php echo ($job_companies != '' || $job_order != '' || $job_category != '')?'style="display: none;"':'';?>>Submit</button>
					<p id="advance"  <?php echo ($job_companies != '' || $job_order != '' || $job_category != '')?'style="display: none;"':'';?>>Advanced Search &nbsp;<i class="fa fa-chevron-down"></i></p>
				</div>
			</div>
			<div class="collapse-advance" <?php echo ($job_companies != '' || $job_order != '' || $job_category != '')?'style="display: block;"':'';?>>
				<div class="row">
					<div class="col-sm-4">
						<label for="exampleInputEmail1">Companies </label>
						<div class="form-group">
							<input type="text" class="form-control checkme"   name="job_companies" placeholder="job companies - e.g., models,photographers" value="<?php echo $objCommon->html2text($job_companies);?>" />
						</div>
					</div>
					<div class="col-sm-3">
						<label for="exampleInputEmail1">Order by Date</label>
						<div class="form-group upload-help">
							<select class="form-control" name="job_order">
								<option value="">Select</option>
								<option value="1" <?php echo ($job_order==1)?'selected="selected"':''?>>Ascending</option>
								<option value="2" <?php echo ($job_order==2)?'selected="selected"':''?>>Descending</option>
							</select>
						</div>
					</div>
					<div class="col-sm-3">
						<label for="exampleInputEmail1">Job Type</label>
						<div class="form-group upload-help">
							<select class="form-control" name="job_category">
								<option value="">Select type</option>
								<?php
								foreach($getJobCat as $allJobCat){
								?>
								<option value="<?php echo $allJobCat['jc_id']?>" <?php echo ($allJobCat['jc_id']==$job_category)?'selected="selected"':''?>><?php echo $objCommon->html2text($allJobCat['jc_name'])?></option>
								<?php
								}
								?>
							</select>
						</div>
					</div>
					<div class="col-sm-2">
						<label for="exampleInputEmail1"> </label>
						<button type="submit" class="btn btn-default">Submit</button>
						<p id="advance1">Normal Search &nbsp;<i class="fa fa-chevron-down"></i></p>
					</div>
				</div>
			</div>
		</form>
       </div>
       </div>
       <div class="result-display ovr">
	   <?php
	   if(count($listJobs)>0){
		   	foreach($listJobs as $allJobs){
		?>
       <div class="display-content-sec">
		   <div class="row">
				<div class="col-sm-2 newwidth">
					<div class="img-sec-job">
						<a href="<?php echo SITE_ROOT?>jobs/show/job-<?php echo $objCommon->html2text($allJobs['job_id'])?>"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/jobs/<?php echo($allJobs['job_image'])?$allJobs['job_image']:'job-architecture-designer.jpg'?>" /></a>
					</div>
				</div>
				<div class="col-sm-9">
					<div class="info-job">
						<div class="job-head">
						<p><a href="<?php echo SITE_ROOT?>jobs/show/job-<?php echo $objCommon->html2text($allJobs['job_id'])?>"><?php echo $objCommon->html2text($allJobs['job_title']);?></a></p>
					</div>
					<div class="place-job">
						<p><i class="fa fa-briefcase"></i> &nbsp;Posted by <?php echo (($allJobs['job_company'])?$objCommon->html2text($allJobs['job_company']):$objCommon->displayName($allJobs)).' , on '.date("l, d F Y",strtotime($allJobs['job_created']));?></p>
					</div>
						<div class="post-info">
							<p class="dim-me"><i class="fa fa-globe"></i>&nbsp;<?php echo $objCommon->html2text($allJobs['job_city']).' - '.$objCommon->html2text(ucfirst($allJobs['job_state'])).' - '.$objCommon->html2text(ucfirst($allJobs['job_country']))?></p>
							<p><?php  echo $objCommon->limitWords($allJobs['job_descr'],200);?></p>
							<div class="row">
								<div class="col-sm-6 col-md-5">
									<p><span class="dim-me">Expires: </span><?php echo date("l, d F Y",strtotime($allJobs['job_end']));?> </p>
								</div>
								<div class="col-sm-6">
									<p><span class="dim-me">Job date:</span><?php echo date("l, d F Y",strtotime($allJobs['job_start']));?> </p>
								</div>
								<div class="col-sm-1">
									<?php
									if($allJobs['user_id'] != $_SESSION['userId']){
									?>
									<a href="<?php echo SITE_ROOT?>jobs/show/job-<?php echo $objCommon->html2text($allJobs['job_id'])?>"><input type="button" class="btn btn-default" value="View Job" /></a>
									<?php
									}
									?>
								</div>
							</div>
						</div>
				</div>
				
			   </div>
		   </div>
		   <div class="border-make"></div>
       </div>
	   <?php
	   	}
	   	echo '<div class="clr"></div><div class="paginationDiv">'.$pagination_output.'</div>';
	   }else{
		   echo '<p>No jobs found...</p>';
	   }
	   ?>
       </div> 
      </div>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3">
      <?php
			include_once(DIR_ROOT."widget/right_ads_sidebar.php");
		?>
    </div>
    </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/fixed_sidebar.js"></script>
<script type="text/javascript">
$(window).load(function(e) {
	$('.fixer').fixedSidebar();
});
$(document).ready(function() {
	$("#advance").click(function(){
		$(".collapse-advance").slideDown();
		$("#hide-advance").hide();
		$(this).hide();
	});
	$("#advance1").click(function(){
		$(".collapse-advance").slideUp();
		$("#hide-advance").delay(150).fadeIn();
		$("#advance").delay(150).fadeIn();
	});	
});
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>