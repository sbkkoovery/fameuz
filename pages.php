<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
$page_alias							=	$objCommon->esc($_GET['page_alias']);
if($page_alias){
	$getPageContent					=	$objUsers->getRowSql("SELECT page_id,page_title,page_alias,page_descr FROM pages WHERE page_alias='".$page_alias."' AND page_status=1");
}
if($getPageContent['page_id']==''){
	header("location:".SITE_ROOT);
	exit;
}
?>
    <div class="inner_content_section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="inner_top_border">
                        <div class="user_profile">
                            <div class="content">
                                <div class="pagination_box">
                                
                                	<a href="<?php echo SITE_ROOT?>user/home">Home <i class="fa fa-caret-right"></i></a>
        							 <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                    <a href="javascript:;" class="active"><?php echo $objCommon->html2text($getPageContent['page_title'])?></a>
                                    <?php
								//if(isset($_SESSION['userId'])){ include_once(DIR_ROOT."widget/notification_head.php"); }
								?>
                                </div>
                                <div class="row">
                                <div class="col-lg-sp-12">
									<div class="post_job_main">
										<div class="container-compress white_bg">
											<h5 class="pagehead"><?php echo $objCommon->html2text($getPageContent['page_title'])?></h5>
											<p class="pagedesc"><?php echo $objCommon->html2text($getPageContent['page_descr'])?></p>
										</div>
									<div class="clr"></div>            
								</div>
                            </div>
                            </div> 
                             
                            <div class="clr"></div>                        
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
 
<?php
include_once(DIR_ROOT."includes/login_box.php");
include_once(DIR_ROOT."includes/footer.php");
?>
