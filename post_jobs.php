<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/job_category.php");
$objJobCategory				=	new job_category();
$getJobCategory				=	$objJobCategory->getAll("","jc_order");
$editId						=	$objCommon->esc($_GET['edit']);
if($editId){
	$getJobContent			 =	$objJobCategory->getRowSql("select * from jobs where job_id=".$editId." and user_id=".$_SESSION['userId']);
	$getpaymentJob			 =	$objCommon->html2text($getJobContent['job_payment']);
	$explGetpaymentJob		 =	explode("###",$getpaymentJob);
}
?>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery.validate.js"></script>
<script src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery-ui.js"></script>
<link href="<?php echo SITE_ROOT?>css/cmxform.css" rel="stylesheet" type="text/css" />
<link href="<?php echo SITE_ROOT?>css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <div class="inner_content_section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="inner_top_border">
                        <div class="user_profile">
						<?php echo $objCommon->checkEmailverification();?>
                            <div class="content">
                                <div class="pagination_box">
                                
                                	<a href="<?php echo SITE_ROOT?>user/home">Home <i class="fa fa-caret-right"></i></a>
                                    <a href="<?php echo SITE_ROOT?>jobs"> Jobs <i class="fa fa-caret-right"></i></a>
        							 <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                    <a href="javascript:;" class="active"> Post Job</a>
                                    <?php
								include_once(DIR_ROOT."widget/notification_head.php");
								?>
                                </div>
                                <div class="row">
                                <div class="col-lg-sp-2">
                                <?php
								include_once(DIR_ROOT."includes/profile_left.php");
								?>
                                </div>
                                <div class="col-lg-sp-10">
                                <div class="head-sec">
                                	<span class="arw-point"></span>
									<h5>Post Jobs</h5>
                                </div>
                                <div class="post_job_main">
                                	<?php
									echo $objCommon->displayMsg();
									?>
                                    <div class="container-compress">
									<?php
									if($getUserDetails['email_validation']==1){
									?>
                                    <form class="post_job_form" id="post-job-form" method="post" action="<?php echo SITE_ROOT.'access/post_job.php?action=post_job'?>" enctype="multipart/form-data">
                                    	<div class="row">
                                            <div class="col-sm-6">
                                              <div class="form-group">
                                              	<div class="input-group">
                                                      <span class="input-group-addon" id="basic-addon1"><i class="fa fa-tag"></i></span>
                                                  <input type="text" class="form-control" id="job_title" name="job_title" value="<?php echo $objCommon->html2text($getJobContent['job_title'])?>" placeholder="Job Title" />
                                                </div>
                                              </div>
                                          </div>
                                            <div class="col-sm-6">
                                          <div class="form-group">
                                            <div class="input-group">
                                                  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-globe"></i></span>
                                                  <input type="text" class="form-control" id="job_country" name="job_country" value="<?php echo $objCommon->html2text($getJobContent['job_country'])?>" placeholder="Job Country" />
                                              </div>
                                            </div>
                                          </div>
                                            <div class="col-sm-6">
                                          <div class="form-group">
                                            <div class="input-group">
                                                  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-globe"></i></span>
                                                  <input type="text" class="form-control" id="job_state" name="job_state" value="<?php echo $objCommon->html2text($getJobContent['job_state'])?>" placeholder="Job State" />
                                              </div>
                                            </div>
                                          </div>
                                            <div class="col-sm-6">
                                          <div class="form-group">
                                            <div class="input-group">
                                                  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-globe"></i></span>
                                              <input type="text" class="form-control" id="job_city" name="job_city" value="<?php echo $objCommon->html2text($getJobContent['job_city'])?>" placeholder="Job City" />
                                              </div>
                                            </div>
                                          </div>
                                            <div class="col-sm-12">
                                           <div class="form-group">
                                            <div class="input-group">
                                                  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-building"></i></span>
                                              <input type="text" class="form-control" id="job_company" name="job_company" value="<?php echo $objCommon->html2text($getJobContent['job_company'])?>" placeholder="Company" />
                                              </div>
                                            </div>
                                          </div>
                                            <div class="col-sm-6">
                                          <div class="form-group">
                                            <div class="input-group">
                                                  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                              <input type="text" class="form-control start_date" id="job_start" name="job_start" value="<?php echo ($getJobContent['job_start'] !='0000-00-00' && $editId != '' && $getJobContent['job_start'] !='')?date("m/d/Y",strtotime($getJobContent['job_start'])):''?>" placeholder="Start Date" />
                                              </div>
                                            </div>
                                          </div>
                                            <div class="col-sm-6">
                                          <div class="form-group">
                                            <div class="input-group">
                                                  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                              <input type="text" class="form-control end_date" id="job_end" name="job_end" value="<?php echo ($getJobContent['job_end'] !='0000-00-00' && $editId != '' && $getJobContent['job_end'] !='')?date("m/d/Y",strtotime($getJobContent['job_end'])):''?>" placeholder="End Date" />
                                              </div>
                                            </div>
                                          </div>
										  <div class="col-sm-4">
                                          <div class="form-group">
                                            <div class="input-group">
                                                  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-usd"></i></span>
                                              	  <select class="form-control" name="job_currency">
													<?php
													$getCurrency		=	$objCommon->currencies();
													foreach($getCurrency as $keyCurrency=>$allCurrency){
														$selCurr		=	($explGetpaymentJob[0]==$keyCurrency)?'selected="selected"':'';
														echo '<option value="'.$keyCurrency.'" '.$selCurr.' >'.$allCurrency.'</option>';
													}
													?>
												  </select>
                                              </div>
                                            </div>
                                          </div>
										  <div class="col-sm-8">
                                          <div class="form-group">
                                            <div class="input-group">
                                                  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-usd"></i></span>
                                              <input type="text" class="form-control" id="job_payment" name="job_payment" placeholder="Payment  (eg :1,000)" value="<?php echo $explGetpaymentJob[1];?>" />
                                              </div>
                                            </div>
                                          </div>
										  	<div class="col-sm-12">
											  <div class="form-group">
												<div class="input-group">
													  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-briefcase"></i></span>
												  	  <input type="text" class="form-control" id="job_skill" name="job_skill" placeholder="Skills eg: modeling,acting..." value="<?php echo $objCommon->html2text($getJobContent['job_skill'])?>" />
												  </div>
												</div>
                                          </div>
                                            <div class="col-sm-6">
                                          <div class="form-group">
                                              <select class="form-control" id="job_category" multiple="multiple">
                                                <?php
                                                if($getJobContent['job_category']){
                                                        $exploJobCat		=	explode(",",$getJobContent['job_category']);
                                                        $exploJobCat		=	array_filter($exploJobCat);
                                                }
                                                foreach($getJobCategory as $allJobCat){
                                                ?>
                                                <option value="<?php echo $objCommon->html2text($allJobCat['jc_id'])?>"><?php echo $objCommon->html2text($allJobCat['jc_name'])?></option>
                                                <?php
                                                }
                                                ?>
                                              </select>
                                            </div>
                                          </div>
                                          <div class="col-sm-6">
                                          	<div class="selected_items">
                                            <?php
                                                if($getJobContent['job_category']){
                                                        $exploJobCat		=	explode(",",$getJobContent['job_category']);
                                                        $exploJobCat		=	array_filter($exploJobCat);
                                                }
                                                foreach($getJobCategory as $allJobCat){
                                                    if(count($exploJobCat)>0){
                                                        if(in_array($allJobCat['jc_id'],$exploJobCat)){
                                                            echo '<span class="selected_items_input"><input type="hidden" value="'.$objCommon->html2text($allJobCat['jc_id']).'" name="job_category[]"/><p>'.$objCommon->html2text($allJobCat['jc_name']).'</p><span class="dis_miss">X</span></span>';
                                                        }
                                                    }
                                                }
                                                ?>
                                            </div>
                                          </div>
                                            <div class="col-sm-12">
                                          <div class="form-group">
                                              <textarea class="form-control" id="job_descr" rows="6" name="job_descr" placeholder="Job Description"><?php echo $objCommon->html2text($getJobContent['job_descr'])?></textarea>
                                            </div>
                                          </div>
                                            <div class="col-sm-12">
                                          <div class="form-group">
                                              <input  type="file" id="job_image" name="job_image"  data-icon="false"  />
                                            </div>
                                          </div>
                                          <div class="col-sm-12">
                                              <div class="form-group">
                                                <div>
                                                <input type="hidden" name="editId" value="<?php echo $getJobContent['job_id']?>" />
                                                  <button type="submit" class="btn btn-default btn-log">Post Job</button>
                                                </div>
                                              </div>
                                          </div>
                                      </div>
									</form>
									<?php
									}else{
										echo '<div role="alert" class="alert alert-warning"><strong>Warning ! </strong> You do not have sufficient permissions to access this page.</div>';
									}
									?>
								</div>
                                <div class="clr"></div>            
                            </div>
                            </div>
                            </div> 
                             
                            <div class="clr"></div>                        
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/bootstrap-filestyle.min.js"></script>
<script>
$(document).ready(function(e) {
	$("#job_image").filestyle();
	$(".bootstrap-filestyle input").attr("placeholder", "Job Image Here");
});
$(function() {
    $( ".start_date" ).datepicker({
      defaultDate: "+1w",
      onClose: function( selectedDate ) {
        $( ".end_date" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( ".end_date" ).datepicker({
      defaultDate: "+1w",
      onClose: function( selectedDate ) {
        $( ".start_date" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
  });

 	$("#post-job-form").validate({
		rules: {
			job_title: "required",
			job_country: "required",
			job_state:"required",
			job_state:"required",
			job_city:"required",
			job_start:"required",
			job_end:"required",
			job_category:"required",
			job_descr:"required",
		},
		messages: {
			job_title: 'Can\'t be empty',
			job_country: 'Can\'t be empty',
			job_state: 'Can\'t be empty',
			job_state: 'Can\'t be empty',
			job_city: 'Can\'t be empty',
			job_start: 'Can\'t be empty',
			job_end: 'Can\'t be empty',
			job_category: 'Can\'t be empty',
			job_descr: 'Can\'t be empty'
		}

});

$('#job_category option').click(function(){
	var inputText	=	$(this).text();
	var	inputValue	=	$(this).val();
	var	selectedItem=	$(".selected_items");
	var	appendItem	=	'<span class="selected_items_input">'+
		  				'<input type="hidden" value="'+inputValue+'" name="job_category[]"/><p>'+inputText+'</p><span class="dis_miss">X</span></span>';
	var arrayOfInput	= selectedItem.find('input');
	var	swift			= 1;
		for(var i=0; i<arrayOfInput.length; i++){
			if($(arrayOfInput[i]).val() == inputValue){
				swift	=	0;
			}
		}
		if(swift){			
			selectedItem.append(appendItem);
		}
});
$(".selected_items").on("click",".dis_miss", function(){
	var x  = $(this).parent('.selected_items_input');
	x.remove();
})
</script>
 
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
