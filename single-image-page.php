<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
$type					=	$objCommon->esc($_GET['type']);
$id					  =	$objCommon->esc($_GET['id']);
if($type==1){
	$sqlContent		  =	"SELECT photo.user_id,photo.photo_url,photo.photo_descr,photo.photo_created,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz,profileImg.upi_img_url FROM user_photos AS photo LEFT JOIN users AS user ON photo.user_id = user.user_id LEFT JOIN user_social_links AS social ON user.user_id=social.user_id  LEFT JOIN user_profile_image AS profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 WHERE photo.photo_id ='".$id."'";
}else if($type ==2){
	$sqlContent		  =	"SELECT photo.user_id,photo.ai_images AS photo_url,photo.ai_caption AS photo_descr,photo.ai_created AS photo_created,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz,profileImg.upi_img_url,album.a_name AS albumName FROM album_images AS photo LEFT JOIN album ON photo.a_id	=	album.a_id LEFT JOIN users AS user ON photo.user_id = user.user_id LEFT JOIN user_social_links AS social ON user.user_id=social.user_id  LEFT JOIN user_profile_image AS profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 WHERE photo.ai_id ='".$id."'";
}else if($type ==6){
	$sqlContent		  =	"SELECT photo.user_id,photo.upi_img_url AS photo_url,photo.upi_created AS photo_created,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz,profileImg.upi_img_url FROM user_profile_image AS photo  LEFT JOIN users AS user ON photo.user_id = user.user_id LEFT JOIN user_social_links AS social ON user.user_id=social.user_id  LEFT JOIN user_profile_image AS profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 WHERE photo.upi_id ='".$id."'";
}
$getContent			  =	$objUsers->getRowSql($sqlContent);
if($getContent['display_name']){
	$displayNameSingle   =	$objCommon->html2text($getContent['display_name']);
}else if($getContent['first_name']){
	$displayNameSingle   =	$objCommon->html2text($getContent['first_name']);
}else{
	$exploEmailSingle	=	explode("@",$objCommon->html2text($getContent['email']));
	$displayNameSingle   =	$exploEmailSingle[0];
}
$getLikeCount			=	$objUsers->getRowSql("SELECT count(like_id) AS likeCount,group_concat(like_user_id) AS youLike FROM likes WHERE like_content =".$id." AND like_cat =".$type." AND like_status=1");
?>
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.css" />
<script src="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.js"></script>
<div class="inner_content_section">
	<div class="container">
		<div class="inner_top_border">
					<div class="user_profile">
					<?php echo $objCommon->checkEmailverification();?>
						<div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                        <div class="content">
							<div class="pagination_box">
                                <a href="<?php echo SITE_ROOT?>user/home">Home <i class="fa fa-caret-right"></i></a>
                                <a href="#"><?php echo $displayNameFriend?><i class="fa fa-caret-right"></i></a>
                                <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
								<a href="javascript:;" class="active"> Images  </a>
                                    <?php
								include_once(DIR_ROOT."widget/notification_head.php");
								?>
							</div>
                            <div class="tab_white_search">
                                        <div class="section_search">
                                            <ul>
                                                <li class="visitor_head_box"><h5><?php echo $displayNameSingle?></h5><span class="point-it"></span></li>
                                            </ul>
                                         </div>
                                    </div>
							<?php /*?><div class="row">
								<div class="col-md-12">
									<div class="whiteBg left100">
										<div class="visitor_head_box"><h5><?php echo $displayNameSingle?></h5></div>
									</div>
								</div>
							</div><?php */?>
							<div class="visitor_list_box single_image">
							<?php
							if($getContent['photo_url']){
							?>
								<div class="row">
									<div class="col-md-12">
										<div class="singleImgBg">
											<div class="full_image center-block">
											<?php
											if($type==6){
											?>
											<img src="<?php echo SITE_ROOT.'uploads/profile_images/'.$getContent['photo_url']?>" class="img-responsive" />
											<?php
											}else{
											?>
											<img src="<?php echo SITE_ROOT.'uploads/albums/'.$getContent['photo_url']?>" class="img-responsive" />
											<?php
											}
											?>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="single_profile_image pull-left">
											<a href="<?php echo SITE_ROOT.$getContent['usl_fameuz']?>"><img img src="<?php echo SITE_ROOT?>uploads/profile_images/<?php echo($getContent['upi_img_url'])?$getContent['upi_img_url']:'profile_pic.jpg'?>" alt="user_thumb" class="img-responsive"/></a>
										</div>
										<div class="descrBox">
											<h3><a href="<?php echo SITE_ROOT.$getContent['usl_fameuz']?>"><?php echo $displayNameSingle?></a></h3>
											<p><?php echo $objCommon->html2text($getContent['photo_descr']);?></p>
											<p><?php echo ($getContent['albumName'])?'<font class="albumFont">Album:</font>'.$objCommon->html2text($getContent['albumName']).'</font>':''?>
												<font class="albumFont">Added:</font> <?php echo date("d M , Y h:i a",strtotime($objCommon->local_time($getContent['photo_created'])));?></p>
											<div class="singleImgLike">
												<?php
												$likeCount		=	$getLikeCount['likeCount'];
												$youLikeArr	   =	explode(",",$getLikeCount['youLike']);
												if(count($youLikeArr)>0){
													if(in_array($_SESSION['userId'],$youLikeArr)){
														$youLike	  =	1;
													}else{
														$youLike	  =	0;
													}
												}
												if($youLike ==1 && $likeCount	==1){
													$likeStr	  =	'You like this.';
												}else if($youLike ==1 && $likeCount>1){
													$likeStr	  =	'You and other '.($likeCount-1).' people like this.';
												}else if($youLike ==0 && $likeCount>0){
													$likeStr	  =	$likeCount.' people like this.';
												}else{
													$likeStr	  =	'';
												}
												?>
												<p class="left like"><a class="<?php echo ($youLike==1)?'liked':''?> like_btn" data-like="<?php echo $id?>" data-likecat="<?php echo $type?>" href="javascript:;"><i class="fa fa-heart"></i> <?php echo ($youLike==1)?'Liked':'Like'?></a></p>
												<p class="left">
												<?php
												if($getContent['user_id'] !=$_SESSION['userId']){
													$getWorkedDetails			 =	$objUsers->getRowSql("SELECT worked_id,from_status,to_status FROM worked_together WHERE img_cat=".$type." and img_id =".$id." and send_from=".$_SESSION['userId']." and send_to=".$getContent['user_id']);
													if($getWorkedDetails['from_status']==1 && $getWorkedDetails['to_status']==1){
													?>
													<a href="javascript:;" class="worked_btn worked_request_sent"><i class="fa fa-star-o"></i>Worked Together</a> 
													<?php
													}else if($getWorkedDetails['from_status']==1){
														?>
														<a href="javascript:;" class="worked_btn worked_request_sent"><i class="fa fa-check"></i>Worked Together</a> 
													<?php
													}else{
													?>
														<a href="javascript:;" class="worked_btn worked_request has-spinner" data-like="<?php echo $id?>" data-likecat="<?php echo $type?>">
															<span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span>
															<i class="fa fa-users"></i>Worked Together 
														</a>
													<?php
													}
												}
												?>
												</p>
												<p class="right"><span class="countStr"><?php echo $likeStr;?></span></p>
											</div>
											<div class="commentBox left100">
											</div>
										</div>
									</div>
								</div>
							<?php
							}else{
							?>
							<div class="dummy_box default_no_image">
							</div>
							<?php /*?><div class="row">
								<div class="col-md-12">
									<img src="<?php echo SITE_ROOT?>images/no_photos_default.png" />
								</div>
							</div><?php */?>
							<?php
							}
							?>
							</div>
							
						</div></div>
						<?php
						include_once(DIR_ROOT."widget/right_static_ad_bar.php");
						?></div>
					</div>
				</div>
			</div>
</div>
<script language="javascript" type="text/javascript">
$(document).ready(function(){
	$(".commentBox").load('<?php echo SITE_ROOT?>ajax/comments_list.php?type=<?php echo $type?>&id=<?php echo $id?>&userto=<?php echo $getContent['user_id']?>&myimage=<?php echo $getUserDetails['upi_img_url']?>');
});
$('.like').on('click','.like_btn',function(){
	var likeId		=	$(this).data('like');
	var likecat	   =	$(this).data('likecat');
	var imgUserId	 =	'<?php echo $getContent['user_id']?>';
	var that		  =	this;
	if(likeId !='' && likecat != ''){
		$.ajax({
			url:"<?php echo SITE_ROOT?>ajax/like.php",
			method: "POST",
			data:{likeId:likeId,likecat:likecat,imgUserId:imgUserId},
			success:function(result){
				if(result == 'liked'){
					$(that).addClass('liked');
				}else if(result == 'like'){
					$(that).removeClass('liked');
				}
				//$(that).parent().html(result);
				$(".countStr").load('<?php echo SITE_ROOT?>ajax/like_count.php?type='+likecat+'&id='+likeId);
		}});
	}
});
$(".worked_request").on("click",function(){
	$(this).addClass('active');
	var likeId		=	$(this).data('like');
	var likecat	   =	$(this).data('likecat');
	var imgUserId	 =	'<?php echo $getContent['user_id']?>';
	var that		  =	this;
	if(likeId !='' && likecat != ''){
		$.ajax({
			url:"<?php echo SITE_ROOT?>ajax/worked_together.php",
			method: "POST",
			data:{img_id:likeId,img_cat:likecat,send_to:imgUserId},
			success:function(result){
				$(that).removeClass('active');
				$(that).parent().html(result);
		}});
	}
});
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  