<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/market_place_category.php");
$objMarCategory				=	new market_place_category();
$getMarCategory				=	$objMarCategory->getAll("","mpc_alias");
$editId						=	$objCommon->esc($_GET['edit']);
if($editId){
	$getJobContent			 =	$objMarCategory->getRowSql("select * from market_place where mp_id=".$editId." and user_id=".$_SESSION['userId']);
}
?>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery.validate.js"></script>
<script src="<?php echo SITE_ROOT?>js/jquery-ui.js"></script>
<link href="<?php echo SITE_ROOT?>css/cmxform.css" rel="stylesheet" type="text/css" />
<link href="<?php echo SITE_ROOT?>css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT_AWS_CDN?>js/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <div class="inner_content_section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="inner_top_border">
                        <div class="user_profile">
						<?php echo $objCommon->checkEmailverification();?>
                            <div class="content">
                                <div class="pagination_box">
                                	<a href="<?php echo SITE_ROOT?>user/home">Home <i class="fa fa-caret-right"></i></a>
                                    <a href="<?php echo SITE_ROOT?>market-place"> Market Place <i class="fa fa-caret-right"></i></a>
        							 <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                    <a href="javascript:;" class="active"> Post ads</a>
                                    <?php
								include_once(DIR_ROOT."widget/notification_head.php");
								?>
                                </div>
                                <div class="row">
                                <div class="col-lg-sp-2">
                                <?php
								include_once(DIR_ROOT."includes/profile_left.php");
								?>
                                </div>
                                <div class="col-lg-sp-10">
                                <div class="head-sec">
                                	<span class="arw-point"></span>
									<h5>Post ads</h5>
                                </div>
                                <div class="post_job_main">
                                	<?php
									echo $objCommon->displayMsg();
									?>
                                    <div class="container-compress">
									<?php
									if($getUserDetails['email_validation']==1){
									?>
									<form class="post_job_form" id="post-job-form" method="post" action="<?php echo SITE_ROOT.'access/post_market_place.php?action=post_market_place'?>" enctype="multipart/form-data">
                                    <div class="row">
										<div class="col-sm-6">
                                          <div class="form-group">
                                              	<div class="input-group">
                                                      <span class="input-group-addon" id="basic-addon1"><i class="fa fa-tag"></i></span>
                                              <input type="text" class="form-control" id="mp_title" name="mp_title" value="<?php echo $objCommon->html2text($getJobContent['mp_title'])?>" placeholder="Ad Title" />
                                            </div>
                                            </div>
									  </div>
										<div class="col-sm-6">
									  <div class="form-group">
                                              	<div class="input-group">
                                                      <span class="input-group-addon" id="basic-addon1"><i class="fa fa-bars"></i></span>
										  <select  class="form-control"  name="mp_type" id="mp_type">
										  	<option value="">Ad Type | Select Ad Category</option>
											<?php
											foreach($getMarCategory as $allMarCategory){
											?>
											<option value="<?php echo $allMarCategory['mpc_id']?>" <?php echo ($allMarCategory['mpc_id']==$getJobContent['mp_type'])?'selected="selected"':''?>><?php echo $objCommon->html2text($allMarCategory['mpc_name'])?></option>
											<?php
											}
											?>
										  </select>
                                          </div>
										</div>
									  </div>
										<div class="col-sm-6">
									  <div class="form-group">
                                              	<div class="input-group">
                                                      <span class="input-group-addon" id="basic-addon1"><i class="fa fa-globe"></i></span>
										  <input type="text" class="form-control" id="mp_locations" name="mp_locations" value="<?php echo $objCommon->html2text($getJobContent['mp_locations'])?>" placeholder="Locations" />
										</div>
                                        </div>
									  </div>
										<div class="col-sm-6">
									  <div class="form-group">
                                              	<div class="input-group">
                                                      <span class="input-group-addon" id="basic-addon1"><i class="fa fa-clock-o"></i></span>
										  <input type="text" class="form-control" id="mp_duration" name="mp_duration" value="<?php echo $objCommon->html2text($getJobContent['mp_duration'])?>" placeholder="Duration" />
										</div>
                                        </div>
									  </div>
									  <?php
									  $editAcutalPrice				=	$objCommon->html2text($getJobContent['mp_actual_price']);
									  $editActu1					  =	'';
									  $editActu2					  =	'';
									  if($editAcutalPrice){
										  $editAcutalPriceExpl		=	explode('###',$editAcutalPrice);
										  $editActu1				  =	$editAcutalPriceExpl[0];
										  $editActu2				  =	$editAcutalPriceExpl[1];
									  }
									  ?>
									  <div class="form-group">
										<div class="col-sm-3">
                                              	<div class="input-group">
                                                      <span class="input-group-addon" id="basic-addon1"><i class="fa fa-cc-visa"></i></span>
										  <input type="text" class="form-control" id="mp_actual_price" name="mp_actual_price" value="<?php echo $editActu2?>" placeholder="Actual Price" />
                                          </div>
										  </div>
										  <div class="col-sm-3">
										  <select  class="form-control"  name="currency_actual" id="currency_actual">
										  	<?php
											$arrCurrency		=	$objCommon->currencies();
											foreach($arrCurrency as $keyCurr=>$allCurrency){
												?>
												<option value="<?php echo $keyCurr;?>" <?php echo ($keyCurr==$editActu1)?'selected="selected"':''?>><?php echo $allCurrency;?></option>
												<?php
											}
											?>
										  </select>
										</div>
										</div>
									 <?php
									  $editDisPrice				   =	$objCommon->html2text($getJobContent['mp_dis_price']);
									  $editDis1					   =	'';
									  $editDis2					   =	'';
									  if($editDisPrice){
										  $editDisPriceExpl		   =	explode('###',$editDisPrice);
										  $editDis1				   =	$editDisPriceExpl[0];
										  $editDis2				   =	$editDisPriceExpl[1];
									  }
									  ?>
									   <div class="form-group">
										<div class="col-sm-3">
                                              	<div class="input-group">
                                                      <span class="input-group-addon" id="basic-addon1"><i class="fa fa-cc-visa"></i></span>
										  <input type="text" class="form-control" id="mp_dis_price" name="mp_dis_price" value="<?php echo $editDis2?>" placeholder="Discount Price" />
                                          </div>
										  </div>
										  <div class="col-sm-3">
										  <select  class="form-control"  name="currency_discount" id="currency_discount">
										  	<?php
											$arrCurrency		=	$objCommon->currencies();
											foreach($arrCurrency as $keyCurr=>$allCurrency){
												?>
												<option value="<?php echo $keyCurr;?>" <?php echo ($keyCurr==$editDis1)?'selected="selected"':''?>><?php echo $allCurrency;?></option>
												<?php
											}
											?>
										  </select>
										</div>
									  </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <textarea class="wysihtml5 form-control" rows="9" name="mp_description" placeholder="Description"><?php echo $objCommon->html2text($getJobContent['mp_description'])?></textarea>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
									  <div class="form-group">
                                              	<div class="input-group">
                                                      <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
										  <input type="text" class="form-control start_date" id="mp_show_from" name="mp_show_from" value="<?php echo ($getJobContent['mp_show_from'] !='0000-00-00' && $editId != '' && $getJobContent['mp_show_from'] !='')?date("m/d/Y",strtotime($getJobContent['mp_show_from'])):date("m/d/Y")?>" placeholder="Ad show from Date" />
                                          </div>
										</div>
									  </div>
										<div class="col-sm-6">
									  <div class="form-group">
                                              	<div class="input-group">
                                                      <span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>
										  <input type="text" class="form-control end_date" id="mp_show_to" name="mp_show_to" value="<?php echo ($getJobContent['mp_show_from'] !='0000-00-00' && $editId != '' && $getJobContent['mp_show_from'] !='')?date("m/d/Y",strtotime($getJobContent['mp_show_to'])):date("m/d/Y", strtotime("+1 month"));?>" placeholder="Ad show to Date" />
										</div>
                                        </div>
									  </div>
										<div class="col-sm-12">
									  <div class="form-group">
										  <input  type="file" id="mp_image" name="mp_image" data-icon="false" />
										</div>
									  </div>
										<div class="col-sm-12">
									  <div class="form-group">
										<input type="hidden" name="editId" value="<?php echo $getJobContent['mp_id']?>" />
										  <button type="submit" class="btn btn-default btn-log">Post Ads</button>
										</div>
									  </div>
                                      </div>
									</form>
									<?php
									}else{
										echo '<div role="alert" class="alert alert-warning"><strong>Warning ! </strong> You do not have sufficient permissions to access this page.</div>';
									}
									?>
									</div>
                                <div class="clr"></div>            
                            </div>
                            </div>
                            </div> 
                             
                            <div class="clr"></div>                        
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/bootstrap-filestyle.min.js"></script>
<script>
$(document).ready(function(e) {
	$("#mp_image").filestyle({
		iconName:'fa-file'
		});
	$(".bootstrap-filestyle input").attr("placeholder", "Attach Your File");
});
$(function() {
	$('.wysihtml5').wysihtml5({
		 stylesheets: "<?php echo SITE_ROOT_AWS_CDN?>js/bootstrap-wysihtml5/wysiwyg-color.css"
	});
    $( ".start_date" ).datepicker({
      defaultDate: "+1w",
      onClose: function( selectedDate ) {
        $( ".end_date" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( ".end_date" ).datepicker({
      defaultDate: "+1w",
      onClose: function( selectedDate ) {
        $( ".start_date" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
  });

 	$("#post-job-form").validate({
		rules: {
			mp_title: "required",
			mp_locations: "required",
			mp_actual_price:{number: true},
			mp_dis_price:{number: true}
		},
		messages: {
			mp_title: 'Can\'t be empty',
			mp_locations: 'Can\'t be empty',
			mp_actual_price:'Please enter a valid number.',
			mp_dis_price:'Please enter a valid number.'
		}

});
</script>
 
<?php
include_once(DIR_ROOT."includes/footer.php");
?>