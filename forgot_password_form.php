<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/is_session.php");
include_once(DIR_ROOT."class/forgot_password.php");
$objForgot						  =	new forgot_password();
$confirm							=	$objCommon->esc($_GET['confirm']);
if($confirm){
	$getForDetails				  =	$objForgot->getRowSql("SELECT user.user_id,user.first_name,user.last_name,user.display_name,user.email FROM forgot_password AS forgot LEFT JOIN users AS user ON 
	forgot.user_id = user.user_id WHERE forgot.fp_string='".$confirm."' AND user.email_validation=1 AND user.status=1");
	if($getForDetails['user_id']==''){
	}
		
}else{
	header("location:".SITE_ROOT);
	exit;
}
?>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.validate.js"></script>
<link href="<?php echo SITE_ROOT?>css/cmxform.css" rel="stylesheet" type="text/css" />
    <div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="register_heading">
                        	<h1>Join the world's largest Fashion models network.</h1>
                            <p>Professional Model, New face, Photographers, Agency, Hair and makeup, Artist, Fashion stylist, Spectator, Industry Professional</p>
                        </div>
                        <div class="register_content">
                        	<?php 
							include_once(DIR_ROOT."widget/register_user_images.php");
							?>
                        	<div class="register_box">
                            	<div class="register_form_box" id="register_form_box">
                                    <h1>Chooose a new password</h1>
                                    <?php
									echo $objCommon->displayMsg();
									?>
                                    <form action="<?php echo SITE_ROOT?>access/forgot_password_form.php?requestStr=<?php echo $confirm?>" method="post" id="register_home_frm" role="form">
                                    <div class="register_form">
									 <div class="tr form-group">
                                            <div class="icon"><img src="<?php echo SITE_ROOT?>images/who.png" alt="register_icon1" /></div>
                                            <div class="field"><input type="text"  value="<?php echo $objCommon->displayName($getForDetails)?>" name="reg_text" id="reg_text" disabled="disabled"  /></div>
                                            <div class="clr"></div>
                                        </div>
                                        <div class="tr form-group">
                                            <div class="icon"><img src="<?php echo SITE_ROOT?>images/register_icon3.png" alt="register_icon3" /></div>
                                            <div class="field"><input type="password" placeholder="New Password ( 6 or more characters)" name="reg_pwd" id="reg_pwd"  /></div>
                                            <div class="clr"></div>
                                        </div>
										 <div class="tr form-group">
                                            <div class="icon"><img src="<?php echo SITE_ROOT?>images/register_icon3.png" alt="register_icon3" /></div>
                                            <div class="field"><input type="password" placeholder="Confirm  new password" name="reg_conf_pwd" id="reg_conf_pwd"  /></div>
                                            <div class="clr"></div>
                                        </div>
                                        <div class="tr submit">
                                            <input type="submit" value="Update" name="register_home" />
                                        </div>
                                    </div>
                                    </form>
                                </div>
                                
                            </div>
                            <div class="clr"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script language="javascript" type="text/javascript">
$(document).ready(function(e){
/*	var mainCat			=	'<?php echo $mainCat?>';
	if(mainCat){
		get_category_fun(mainCat);
	}*/
	$("#register_home_frm").validate({
			rules: {
				reg_pwd: {required:true,minlength: 6}, 
				reg_conf_pwd: { equalTo: "#reg_pwd" }
			},
			messages: {
				reg_pwd: {required:'Can\'t be empty',minlength:'Password must be at least 6 characters long'},
				reg_conf_pwd:'Please enter the same password as above'
			}
    });
	var reg_main_cat			=	$("#reg_main_cat").val();
	if(reg_main_cat){
		get_category_fun(reg_main_cat);
	}

});
</script>
<?php

include_once(DIR_ROOT."includes/login_box.php");
include_once(DIR_ROOT."includes/footer.php");
?>