<?php
$getAllPhotosSql				 =	"SELECT tabPhotos.* FROM((SELECT photo_url AS img_url,photo_id AS img_id,photo_descr as img_descr,'-1' AS albumId,photo_created AS img_created,IF(photo_id != '', 1, '') AS img_type,photo_like_count AS likeCount,photo_comment_count AS commentCount
	FROM user_photos 
	WHERE user_id=".$getMyFriendDetails['user_id']." 
	ORDER BY photo_created DESC,photo_id DESC LIMIT 0,6)
	UNION ALL
	(SELECT ai_images AS img_url,ai_id AS img_id,ai_caption AS img_descr,a_id AS albumId,ai_created AS img_created,IF(ai_id != '', 2, '') AS img_type,ai_like_count AS likeCount,ai_comment_count AS commentCount
	FROM album_images 
	WHERE user_id=".$getMyFriendDetails['user_id']."
	ORDER BY ai_created desc ,ai_id desc LIMIT 0,6)) AS tabPhotos ORDER BY tabPhotos.img_created DESC LIMIT 6";
$getAllPhotos					=	$objUsers->listQuery($getAllPhotosSql);
if(count($getAllPhotos)>0){
?>
<div class="photos_section">
	<div class="photos_box">
	<?php
		foreach($getAllPhotos as $allPhotos){
	?>
	<div class="photos">
		<div class="pic"><a href="javascript:;" data-contentId="<?php echo $allPhotos['img_id']?>" data-contentType="<?php echo $allPhotos['img_type']?>" data-contentAlbum="0" class="lightBoxs"><img src="<?php echo SITE_ROOT?>uploads/albums/<?php echo $objCommon->getThumb($objCommon->html2text($allPhotos['img_url']))?>" alt="photos" /></a></div>
		<div class="like_cmt_share">
			<a href="#" class="like like_btn_photo" ><i class="fa fa-heart"></i><?php echo $allPhotos['likeCount'];?></a>
			<a href="#" class="cmt"><i class="fa fa-comments"></i><?php echo $allPhotos['commentCount'];?></a>
			<a href="#" class="share"><i class="fa fa-share-alt"></i></a>
		</div>
	</div>
	<?php
		}
		?>
		<div class="clr"></div>
		<div class="row">
			<div class="col-sm-12 text-right">
				<span class="seemore "><a href="<?php echo SITE_ROOT?>user/<?php echo $user_url?>/albums">See More <i class="fa fa-play "></i></a></span>
			</div>
		</div>
	</div>
</div>
<?php
}else{
?>
<p>No photos found</p>
<?php
}
?>

