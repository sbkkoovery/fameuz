<?php
$getCompoImgDetails			  =	$objCompoCardImg->getRow("user_id=".$_SESSION['userId']);
$posImg1						 =	($getCompoImgDetails['cci_pos1'])?'uploads/albums/'.$objCommon->html2text($getCompoImgDetails['cci_pos1']):'images/composite-img.jpg';
$posImg2						 =	($getCompoImgDetails['cci_pos2'])?'uploads/albums/'.$objCommon->html2text($getCompoImgDetails['cci_pos2']):'images/compo-img1.jpg';
$posImg3						 =	($getCompoImgDetails['cci_pos3'])?'uploads/albums/'.$objCommon->html2text($getCompoImgDetails['cci_pos3']):'images/compo-img-6.jpg';
$posImg4						 =	($getCompoImgDetails['cci_pos4'])?'uploads/albums/'.$objCommon->html2text($getCompoImgDetails['cci_pos4']):'images/compo-img-3.jpg';
?>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/html5lightbox.js"></script>
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
  <div class="inner_top_border">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-sp-9">
      <div class="jobs-head">
        <div class="pagination_box_jobs pagination_box">
        <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
        <a href="<?php echo SITE_ROOT.$getUserDetails['usl_fameuz']?>"> My profile </a><i class="fa fa-caret-right"></i></a>
        <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
 	     <a href="javascript:;" class="active"> Composite Card</a>
      <?php
		if(isset($_SESSION['userId'])){
        	include_once(DIR_ROOT."widget/notification_head.php");
		}
        ?>
        </div>
       </div>
       <div class="white-compo">
       		<div class="compo-head">
            	<div class="row">
                	<div class="col-sm-10 col-md-10 col-lg-11">
                        <div class="composite-head">
                            <p>Get FREE Comp Card</p>
                        </div>
                        <div class="desc-compo">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when .</p>
                        </div>
                    </div>
                    <div class="col-sm-2 col-md-2 col-lg-1">
                    	<img class="img-responsive" src="<?php echo SITE_ROOT_AWS_IMAGES?>images/compo.png" />
                    </div>
                </div>
                <div class="row">
                	<div class="col-sm-4">
                    	<div class="uploa-change-sec">
                        	<div class="row">
                            	<div class="col-sm-6 lesspadding-r">
                                    	<form>
                                	<div class="custom-upload text-center">
                                        	<a href="javascript:;" data-toggle="modal" data-target="#uploadPhoto" class="callModalUploadPos"><span class="upload-btn"><i class="fa fa-camera"> </i><span class="">Upload Photos</span></span></a>
                                    </div>
                                        </form>
                                </div>
                            	<div class="col-sm-6 lesspadding-l">
                                	<div class="custom-upload text-center">
                                        <a href="javascript:;" data-toggle="modal" data-target="#datachange" class="changeData"><span class="upload-btn"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/data.png"> </i><span class="">Settings</span></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                	<div class="col-sm-4">
                    	<div class="uploa-change-sec">
                        	<div class="row">
                            	<div class="col-sm-6 lesspadding-r ">
                                	<div class="custom-upload text-center">
                                       <a href="javascript:;" onclick="makePdf();"><span class="upload-btn pull-right"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/pdf.png"><span class="">Make PDF</span></span></a>
                                    </div>
                                </div>
                            	<div class="col-sm-6 lesspadding-l">
                                	<div class="custom-upload text-center">
                                        <?php /*?><a href="javascript:;"><span class="upload-btn pull-left" onclick="makeJpg();"><img src="<?php echo SITE_ROOT?>images/jpg.png"><span class="">Make JPG</span></span></a><?php */?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                	<div class="col-sm-4">
                    	<div class="box-color-sec pull-right">
                        	<div class="color-box  zero" data-color="color1">
                            	<span class="selected"></span>
                            </div>
                            <div class="color-box ten" data-color="color2">
                            	<span class="selected"></span>
                            </div>
                            <div class="color-box thirty" data-color="color3">
                            	<span class="selected"></span>
                            </div>
                            <div class="color-box fifty" data-color="color4">
                            	<span class="selected"></span>
                            </div>
                            <div class="color-box seventy" data-color="color5">
                            	<span class="selected"></span>
                            </div>
                            <div class="color-box ninety" data-color="color6">
                            	<span class="selected"></span>
                            </div>
                            <div class="color-box dark-grey" data-color="color7">
                            	<span class="selected on"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="compo-container">
            	<div class="row">
                	<div class="col-sm-12 col-lg-sp-11">
                    	<div class="compo-designs">
                        	<div class="row">
                            	<div class="col-sm-6 col-lg-sp-6s border-compo no-padding-r">
                                	<div class="img-left">
                                       <div class="position-adjust"> 
                                           <a href="<?php echo SITE_ROOT.$posImg1?>" class="mylightbox"><img class="center-block img-responsive" src="<?php echo SITE_ROOT_AWS_IMAGES.$posImg1?>" /></a>
                                            <div class="shadow-compo text-center">
                                                <img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/shadow-compo.png" />
                                             </div>
                                         </div>
                                       <div class="name-info text-center">
                                            <p><?php echo $objCommon->displayName($getUserDetails);?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-sp-6r no-padding-l">
                                        <div class="img-sec-right">
                                        <div class="row">
                                            <div class="col-sm-6 alt-padding-half-r">
                                            	<div class="img-r-compo">
                                                    <a href="<?php echo SITE_ROOT.$posImg2?>" class="mylightbox"><img src="<?php echo SITE_ROOT_AWS_IMAGES.$posImg2?>" /></a>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 alt-padding-half-l">
                                            	<div class="img-r-compo">
                                                  <a href="<?php echo SITE_ROOT.$posImg3?>" class="mylightbox"><img src="<?php echo SITE_ROOT_AWS_IMAGES.$posImg3?>" /></a>
                                              </div>
                                            </div>
                                        </div>
<?php
$getDataTable								=	$objUsers->getRowSql("SELECT ccd_data_table FROM composite_card_data WHERE user_id=".$_SESSION['userId']);
$getDataTableArr						 	 =	explode(",",$getDataTable['ccd_data_table']);
$getDataTableArr						 	 =	array_filter($getDataTableArr);
if(count($getDataTableArr)>0){
	$sqlCompModelDetails					 =	"";
	$selectSql							   =	"";
	$leftSql								 =	"";
	if(in_array('model_chest',$getDataTableArr)){
		$selectSql						  .=	",chest.mc_name";
		$leftSql							.=	" LEFT JOIN model_chest AS chest ON det.mc_id = chest.mc_id ";
	}
	if(in_array('model_collar',$getDataTableArr)){
		$selectSql						  .=	",collar.mcollar_name";
		$leftSql							.=	" LEFT JOIN model_collar AS collar ON det.mcollar_id = collar.mcollar_id ";
	}
	if(in_array('model_dress',$getDataTableArr)){
		$selectSql						  .=	",dress.mdress_name";
		$leftSql							.=	" LEFT JOIN model_dress AS dress ON det.mdress_id = dress.mdress_id ";
	}
	if(in_array('model_eyes',$getDataTableArr)){
		$selectSql						  .=	",eyes.me_name";
		$leftSql							.=	" LEFT JOIN model_eyes AS eyes ON det.me_id = eyes.me_id ";
	}
	if(in_array('model_hair',$getDataTableArr)){
		$selectSql						  .=	",hair.mhair_name";
		$leftSql							.=	" LEFT JOIN model_hair AS hair ON det.mhair_id = hair.mhair_id ";
	}
	if(in_array('model_height',$getDataTableArr)){
		$selectSql						  .=	",height.mh_name";
		$leftSql							.=	" LEFT JOIN model_height AS height ON det.mh_id = height.mh_id ";
	}
	if(in_array('model_hips',$getDataTableArr)){
		$selectSql						  .=	",hips.mhp_name";
		$leftSql							.=	" LEFT JOIN model_hips AS hips ON det.mhp_id = hips.mhp_id ";
	}
	if(in_array('model_jacket',$getDataTableArr)){
		$selectSql						  .=	",jacket.mj_name";
		$leftSql							.=	" LEFT JOIN model_jacket AS jacket ON det.mj_id = jacket.mj_id ";
	}
	if(in_array('model_shoes',$getDataTableArr)){
		$selectSql						  .=	",shoes.ms_name";
		$leftSql							.=	" LEFT JOIN model_shoes AS shoes ON det.ms_id = shoes.ms_id ";
	}
	if(in_array('model_trousers',$getDataTableArr)){
		$selectSql						  .=	",trousers.mt_name";
		$leftSql							.=	" LEFT JOIN model_trousers AS trousers ON det.mt_id = trousers.mt_id ";
	}
	if(in_array('model_waist',$getDataTableArr)){
		$selectSql						  .=	",waist.mw_name";
		$leftSql							.=	" LEFT JOIN model_waist AS waist ON det.mw_id = waist.mw_id ";
	}
	$datSql							  	 .=	"SELECT det.model_id ".$selectSql." FROM model_details AS det ".$leftSql." WHERE det.user_id=".$_SESSION['userId'];
}else{
	$datSql								  =	"SELECT 														det.model_dis_id,chest.mc_name,collar.mcollar_name,disc.model_dis_name,dress.mdress_name,eyes.me_name,hair.mhair_name,height.mh_name,hips.mhp_name,jacket.mj_name,shoes.ms_name,trousers.mt_name,waist.mw_name
																FROM model_details AS det
																LEFT JOIN model_chest AS chest ON det.mc_id=chest.mc_id
																LEFT JOIN model_collar AS collar ON det.mcollar_id=collar.mcollar_id
																LEFT JOIN model_dress AS dress ON det.mdress_id=dress.mdress_id
																LEFT JOIN model_disciplines AS disc ON det.model_dis_id=disc.model_dis_id
																LEFT JOIN model_eyes AS eyes ON det.me_id=eyes.me_id
																LEFT JOIN model_hair AS hair ON det.mhair_id=hair.mhair_id
																LEFT JOIN model_height AS height ON det.mh_id=height.mh_id
																LEFT JOIN model_hips AS hips ON det.mhp_id=hips.mhp_id
																LEFT JOIN model_jacket AS jacket ON det.mj_id=jacket.mj_id
																LEFT JOIN model_shoes AS shoes ON det.ms_id=shoes.ms_id
																LEFT JOIN model_trousers AS trousers ON det.mt_id=trousers.mt_id
																LEFT JOIN model_waist AS waist ON det.mw_id=waist.mw_id
																WHERE det.user_id=".$_SESSION['userId'];
}
$getDataSql							  	  =	$objCompoCardImg->getRowSql($datSql);
?>
                                        <div class="row margin-top-up">
                                            <div class="col-sm-6 alt-padding-half-r">
                                            	<div class="log-cenetr-fam">
                                                	<img class="center-block" src="<?php echo SITE_ROOT_AWS_IMAGES?>images/log-compo.png" />
                                                </div>
                                                <div class="info-model">
                                                	<ul>
														<?php
														if($getDataSql['mh_name']){
															echo '<li>Height : <b>'.$objCommon->html2text($getDataSql['mh_name']).'</b></li>';
														}
														if($getDataSql['mc_name']){
															$chestCheck	=	($getUserDetails['p_gender']==2)?'Bust':'Chest';
															echo '<li>'.$chestCheck.' : <b>'.$objCommon->html2text($getDataSql['mc_name']).'</b></li>';
														}
														if($getDataSql['me_name']){
															echo '<li>Eyes : <b>'.$objCommon->html2text($getDataSql['me_name']).'</b></li>';
														}
														if($getDataSql['mhair_name']){
															echo '<li>Hair : <b>'.$objCommon->html2text($getDataSql['mhair_name']).'</b></li>';
														}
														if($getDataSql['mw_name']){
															echo '<li>Waist : <b>'.$objCommon->html2text($getDataSql['mw_name']).'</b></li>';
														}
														if($getDataSql['mhp_name']){
															echo '<li>Hips : <b>'.$objCommon->html2text($getDataSql['mhp_name']).'</b></li>';
														}
														if($getDataSql['mcollar_name']){
															echo '<li>Collar : <b>'.$objCommon->html2text($getDataSql['mcollar_name']).'</b></li>';
														}
														if($getDataSql['mdress_name']){
															echo '<li>Dress : <b>'.$objCommon->html2text($getDataSql['mdress_name']).'</b></li>';
														}
														if($getDataSql['ms_name']){
															echo '<li>Shoes : <b>'.$objCommon->html2text($getDataSql['ms_name']).'</b></li>';
														}
														if($getDataSql['mj_name']){
															echo '<li>Jacket : <b>'.$objCommon->html2text($getDataSql['mj_name']).'</b></li>';
														}
														if($getDataSql['mt_name']){
															echo '<li>Trousers : <b>'.$objCommon->html2text($getDataSql['mt_name']).'</b></li>';
														}
														?>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 alt-padding-half-l">
                                            	<div class="img-como-d">
                                                      <a href="<?php echo SITE_ROOT.$posImg4?>" class="mylightbox"><img src="<?php echo SITE_ROOT_AWS_IMAGES.$posImg4?>" /></a>
                                                  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="row shareComposite">
				<div class="col-md-12 text-right">
					<!-- Load Facebook SDK for JavaScript -->  
					<span class='st_facebook' displayText='Facebook'></span>
					<span class='st_googleplus' displayText='Google +'></span>
					<span class='st_twitter' displayText='Tweet'></span>
					<span class='st_linkedin' displayText='LinkedIn'></span>
					<span class='st_pinterest' displayText='Pinterest'></span>
					<span class='st_email' displayText='Email'></span>
					<script type="text/javascript">var switchTo5x=true;</script>
					<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
					<script type="text/javascript">stLight.options({publisher: "318b9a8f-b81c-4b92-89da-cb0ad4ccf6d6", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
					<!---------------------------------->
				</div>
			</div>
       </div>
   </div>
   <div class="col-sm-3 col-lg-sp-3">
		<?php
			include_once(DIR_ROOT."widget/right_static_ad_bar.php");
        ?>
    </div>
    </div>
    </div>
  </div>
</div>
<div class="modal fade" id="uploadPhoto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
  <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select Photos</h4>
      </div>
	  <div class="modalUploadPosition"></div>
	</div>

  </div>
</div>
<div class="modal fade" id="datachange" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Display My</h4>
      </div>
	  <div class="modalChangeData"></div>
    </div>
  </div>
</div>
<div class="modal fade slectPhoto" id="slectPhoto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg modalSelImg" role="document">
    
  </div>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$(".mylightbox").html5lightbox();
	$(".color-box").click(function () {
		$(".color-box .selected").removeClass("on");
		$(this).children(".selected").addClass("on");
	});
	$(".color-box").click(function () {
		var bg = $(this).css('background-color');
		$(".compo-designs, .border-compo").css({
			"border-color": bg
		});
	});
	/*var compColor	=	$(".selected.on").parent().data("color");
	$.ajax({
		url:'<?php echo SITE_ROOT?>make_composite_image.php',
		data:{compColor:compColor},
		success:function(data){
		}
	});*/
});
	
$(".callModalUploadPos").on("click",function(){
	$.ajax({
		url:'<?php echo SITE_ROOT?>ajax/composite_card_img_position.php',
		success:function(data){
			$(".modalUploadPosition").html(data);
		}
	});
});
$(".changeData").on("click",function(){
	$.ajax({
		url:'<?php echo SITE_ROOT?>ajax/composite_card_img_data.php',
		success:function(data){
			$(".modalChangeData").html(data);
		}
	});
});
function makePdf(){
	var compColor	=	$(".selected.on").parent().data("color");
	window.location.href	=	'<?php echo SITE_ROOT?>composite-print.php?compColor='+compColor+'&compUser=<?php echo $_SESSION['userId']?>';
}
function makeJpg(){
	var compColor	=	$(".selected.on").parent().data("color");
	window.location.href	=	'<?php echo SITE_ROOT?>composite-jpg.php?compColor='+compColor+'&compUser=<?php echo $_SESSION['userId']?>';
}
</script>