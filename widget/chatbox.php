<link href="<?php echo SITE_ROOT ?>css/chatbox.css" rel="stylesheet" type="text/css">
<div class="chat_body">
    <div class="header_chat">
    	<p>Online Friends<span class="count_online">72</span></p>
        <div class="chat_controls pull-right">
            <i class="fa fa-minus"></i>
        </div>
    </div>
    <div class="self_details">
    	<div class="user_image">
        	<img src="<?php echo SITE_ROOT ?>uploads/profile_images/2015-03-21/1426937720_7.jpg">
        </div>
        <div class="user_details">
        	<h4>Olesy Luenburger</h4>
            <a  href="javascript:;"><span>Online</span><i class="fa fa-caret-down"></i></a>
        </div>
        <div class="settings_chat">
        	<a href="#"><i class="fa fa-gear"></i></a>
        </div>
    </div>
    <div class="chat_bodys">
    <ul>
    <?php for($i=0; $i<5; $i++){?>
    	<li class="online_members_list">
        	<div class="img_online_members">
            	<img src="<?php echo SITE_ROOT ?>uploads/albums/2015-07-25/1437812637_8_0.jpg">
            </div>
            <div class="name_online-members">
            	<h4>Annena Padikar</h4>
            </div>
            <div class="staus pull-right">
            	<i class="fa fa-desktop"></i>
                <i class="fa fa-circle green_online"></i>
            </div>
        </li>
        <li class="online_members_list">
        	<div class="img_online_members">
            	<img src="<?php echo SITE_ROOT ?>uploads/albums/2015-12-29/1451369028_2_2270.jpg">
            </div>
            <div class="name_online-members">
            	<h4>Ammy Jackson</h4>
            </div>
            <div class="staus pull-right">
            	<i class="fa fa-mobile"></i>
                <i class="fa fa-circle green_online"></i>
            </div>
        </li>
        <li class="online_members_list">
        	<div class="img_online_members">
            	<img src="<?php echo SITE_ROOT ?>uploads/profile_images/2015-03-16/1426512866_2.jpg">
            </div>
            <div class="name_online-members">
            	<h4>Stella Mery Ann</h4>
            </div>
            <div class="staus pull-right">
            	<i class="fa fa-desktop"></i>
                <i class="fa fa-circle green_online"></i>
            </div>
        </li>
        <?php }?>
       </ul>
       <div class="contacts">
       		More Friends
       </div>
       <ul>
    <?php for($i=0; $i<5; $i++){?>
    	<li class="online_members_list">
        	<div class="img_online_members">
            	<img src="<?php echo SITE_ROOT ?>uploads/albums/2015-07-25/1437812637_8_0.jpg">
            </div>
            <div class="name_online-members">
            	<h4>Annena Padikar</h4>
            </div>
            <div class="staus pull-right">
            	<i class="fa fa-desktop"></i>
                <i class="fa fa-circle offline"></i>
            </div>
        </li>
        <li class="online_members_list">
        	<div class="img_online_members">
            	<img src="<?php echo SITE_ROOT ?>uploads/albums/2015-12-29/1451369028_2_2270.jpg">
            </div>
            <div class="name_online-members">
            	<h4>Ammy Jackson</h4>
            </div>
            <div class="staus pull-right">
            	<i class="fa fa-mobile"></i>
                <i class="fa fa-circle offline"></i>
            </div>
        </li>
        <li class="online_members_list">
        	<div class="img_online_members">
            	<img src="<?php echo SITE_ROOT ?>uploads/profile_images/2015-03-16/1426512866_2.jpg">
            </div>
            <div class="name_online-members">
            	<h4>Stella Mery Ann</h4>
            </div>
            <div class="staus pull-right">
            	<i class="fa fa-desktop"></i>
                <i class="fa fa-circle offline"></i>
            </div>
        </li>
        <?php }?>
       </ul>
    </div>	
</div>
<script type="text/javascript" src="<?php echo SITE_ROOT ?>js/chatbox.js"></script>