<?php
$sqlThisLocationModel				   =	"SELECT model_location.* FROM (SELECT user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,cat.c_name,personal.p_city,personal.p_country,social.usl_fameuz FROM users AS user LEFT JOIN personal_details AS personal ON user.user_id = personal.user_id  LEFT JOIN user_categories AS ucat ON user.user_id = ucat.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id LEFT JOIN user_social_links AS social ON user.user_id=social.user_id  WHERE user.status=1 AND user.email_validation=1 AND user.user_id !='".$getMyFriendDetails['user_id']."'  AND personal.p_city LIKE '".$getMyFriendDetails['p_city']."' AND personal.p_country LIKE '".$getMyFriendDetails['p_country']."' AND ucat.uc_c_id='".$getMyFriendDetails['uc_c_id']."' AND ucat.uc_m_id ='".$getMyFriendDetails['uc_m_id']."'
UNION 
SELECT user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,cat.c_name,personal.p_city,personal.p_country,social.usl_fameuz FROM users AS user LEFT JOIN personal_details AS personal ON user.user_id = personal.user_id  LEFT JOIN user_categories AS ucat ON user.user_id = ucat.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id  LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.email_validation=1 AND user.user_id !='".$getMyFriendDetails['user_id']."'  AND personal.p_city LIKE '".$getMyFriendDetails['p_city']."' AND personal.p_country LIKE '".$getMyFriendDetails['p_country']."' AND  ucat.uc_m_id ='".$getMyFriendDetails['uc_m_id']."'
UNION 
SELECT user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,cat.c_name,personal.p_city,personal.p_country,social.usl_fameuz FROM users AS user LEFT JOIN personal_details AS personal ON user.user_id = personal.user_id  LEFT JOIN user_categories AS ucat ON user.user_id = ucat.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.email_validation=1 AND user.user_id !='".$getMyFriendDetails['user_id']."'  AND personal.p_country LIKE '".$getMyFriendDetails['p_country']."' AND  ucat.uc_m_id ='".$getMyFriendDetails['uc_m_id']."'
) AS model_location LIMIT 4";
$getThisLocationModels				=	$objUsers->listQuery($sqlThisLocationModel);
if(count($getThisLocationModels)>0){
?>
<div class="model_in_location_box">
	<h2 class="heading">More Models in this location</h2>
	<?php
	foreach($getThisLocationModels as $allThisLocationModels){
	if($allThisLocationModels['display_name']){
		$displayNameOther	  =	$objCommon->html2text($allThisLocationModels['display_name']);
	}else if($getMyFriendDetails['first_name']){
		$displayNameOther	  =	$objCommon->html2text($allThisLocationModels['first_name']);
	}else{
		$exploEmailOther	   =	explode("@",$objCommon->html2text($allThisLocationModels['email']));
		$displayNameOther	  =	$exploEmailOther[0];
	}
	if($allThisLocationModels['p_city'] != '' || $allThisLocationModels['p_country'] != ''){
		$otherPcity			=	($allThisLocationModels['p_city'])?$objCommon->html2text($allThisLocationModels['p_city'])." ,":"";
	}
	?>
	<div class="model_box">
		<div class="thumb"><a href="<?php echo SITE_ROOT.$objCommon->html2text($allThisLocationModels['usl_fameuz'])?>"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($allThisLocationModels['upi_img_url'])?$allThisLocationModels['upi_img_url']:'profile_pic.jpg'?>" alt="model_thumb" /></a></div>
		<div class="text">
			<h3><a href="<?php echo SITE_ROOT.$objCommon->html2text($allThisLocationModels['usl_fameuz'])?>"><?php echo $displayNameOther?></a></h3>
			<p class="font12"><?php echo $otherPcity.' '.$objCommon->html2text($allThisLocationModels['p_country']);?></p>
			<p><?php echo $objCommon->html2text($allThisLocationModels['c_name'])?></p>
		</div>
	</div>
	<?php
	}
	?>
</div>
<?php
}
?>