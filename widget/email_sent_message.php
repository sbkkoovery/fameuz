<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/email_message.php");
$objCommon				   			  =	new common();
$objEmailMsg							=	new email_message();
$search								 =	$objCommon->esc($_GET['search']);
$count_per_page						 =	20;
$currentPage							=	($_GET['page'])?$objCommon->esc($_GET['page']):1;
if($search){
	$getEmailMessagesSql				=	"SELECT msg.*,msg_user.emu_from,msg_user.emu_to,msg_user.emu_read_status,msg_user.emu_delete_from,group_concat(user_to.user_id) AS userId,group_concat(user_to.first_name) AS userFirstName,group_concat(user_to.last_name) AS userLastName,group_concat(user_to.display_name) AS userDisplayName,group_concat(user_to.email) AS userEmail,group_concat(profileImg.upi_img_url) AS userImgUrl,group_concat(social.usl_fameuz) AS userSocialUrl
	FROM email_message_users AS msg_user 
	LEFT JOIN email_message AS msg ON msg_user.em_id =msg.em_id 
	LEFT JOIN users AS user_to ON msg_user.emu_to = user_to.user_id 
	LEFT JOIN user_social_links AS social ON user_to.user_id=social.user_id 
	LEFT JOIN user_profile_image as profileImg ON user_to.user_id=profileImg.user_id AND profileImg.upi_status=1 
	WHERE msg_user.emu_from=".$_SESSION['userId']." AND msg_user.emu_delete_from=1 AND user_to.email_validation=1 AND user_to.status=1  AND msg.em_replyof=0 AND  msg.em_subject LIKE '%".$search."%'
	GROUP BY msg.em_id
ORDER BY msg.em_createdon DESC";
}else{
	$getEmailMessagesSql				=	"SELECT msg.*,msg_user.emu_from,msg_user.emu_to,msg_user.emu_read_status,msg_user.emu_delete_from,group_concat(user_to.user_id) AS userId,group_concat(user_to.first_name) AS userFirstName,group_concat(user_to.last_name) AS userLastName,group_concat(user_to.display_name) AS userDisplayName,group_concat(user_to.email) AS userEmail,group_concat(profileImg.upi_img_url) AS userImgUrl,group_concat(social.usl_fameuz) AS userSocialUrl
	FROM email_message_users AS msg_user 
	LEFT JOIN email_message AS msg ON msg_user.em_id =msg.em_id 
	LEFT JOIN users AS user_to ON msg_user.emu_to = user_to.user_id 
	LEFT JOIN user_social_links AS social ON user_to.user_id=social.user_id 
	LEFT JOIN user_profile_image as profileImg ON user_to.user_id=profileImg.user_id AND profileImg.upi_status=1 
	WHERE msg_user.emu_from=".$_SESSION['userId']." AND msg_user.emu_delete_from=1 AND user_to.email_validation=1 AND user_to.status=1  AND msg.em_replyof=0
	GROUP BY msg.em_id
ORDER BY msg.em_createdon DESC";
}
$totalCount							 =	$objEmailMsg->countRows($getEmailMessagesSql);
$totalPages							 =	ceil(($totalCount/$count_per_page));
$orderFrom							  =	$count_per_page*($currentPage-1);
$orderTo								=	$count_per_page*$currentPage;
$getEmailMessagesSql					.=   " LIMIT ".$orderFrom.",".$count_per_page;
$getEmailMessages					   =	$objEmailMsg->listQuery($getEmailMessagesSql);	
?>
<div class="inbox_msges">
<?php
if(count($getEmailMessages)>0){
	foreach($getEmailMessages as $allEmailMessages) {
		$userIds						=	$allEmailMessages['userId'];
		$userIdsExpl					=	explode(",",$userIds);
		$userIdsExpl					=	array_filter($userIdsExpl);
		$userFname					  =	$allEmailMessages['userFirstName'];
		$userFnameExpl				  =	explode(",",$userFname);
		$userFnameExpl				  =	array_filter($userFnameExpl);
		$userDisname					=	$allEmailMessages['userDisplayName'];
		$userDisnameExpl				=	explode(",",$userDisname);
		$userDisnameExpl				=	array_filter($userDisnameExpl);
		$userEmail					  =	$allEmailMessages['userEmail'];
		$userEmailExpl				  =	explode(",",$userEmail);
		$userEmailExpl				  =	array_filter($userEmailExpl);
		$i							  =	0;
		$userNameString				 =	'';
		foreach($userIdsExpl as $userIdAll){
			$firstName				  =	$userFnameExpl[$i];
			$displayName				=	$userDisnameExpl[$i];
			$userEmail				  =	$userEmailExpl[$i];
			$userDetailsArray		   =	array('display_name'=>$displayName,'first_name'=>$firstName,'email'=>$userEmail);
			$userNameString			 .=	$objCommon->displayNameSmall($userDetailsArray,10).",";
			if($i==1){ break; }
			$i++;
		}
		$emailMsgId					 =	$allEmailMessages['em_id'];
	?>
		<div class="new_msg_unread read_messages">
			<div class="checkBox_delete">
				<input class="check"   type="checkbox" name="msgId" value="<?php echo $emailMsgId?>" />
			</div>
			<div class="user_info_name_msg">
				<h4><a href="<?php echo SITE_ROOT.'user/my-sent-messages/'.$emailMsgId?>" class="colorFameuz">To: <?php echo rtrim($userNameString,',');?></a></h4>
			</div>
			<div class="message_stratz">
				<p><a href="<?php echo SITE_ROOT.'user/my-sent-messages/'.$emailMsgId?>" class="colorFameuz"><?php echo $objCommon->limitWords($allEmailMessages['em_subject'],100)?></a></p>
			</div>
			<div class="date_msg">
				<p><a href="<?php echo SITE_ROOT.'user/my-sent-messages/'.$emailMsgId?>" class="colorFameuz"><?php echo date("d/m/Y",strtotime($allEmailMessages['em_createdon']))?></a></p>
			</div>
		</div>
<?php 
	}
	?>
	<div class="new_msg_unread">
		<?php 
		$orderToStr	   =	($orderTo >$totalCount)?$totalCount:$orderTo;
		echo ($orderFrom+1)."-".$orderToStr." of ".$totalCount;
		$prev			=	($currentPage>1)?($currentPage-1):1;
		$next			=	($totalPages > $currentPage)?($currentPage+1):$totalPages;
		if($totalPages >1){
		?>
		<span class="more_optn">
			<a href="javascript:;" onclick="doShowMsgList('<?php echo $prev?>')" <?php echo ($currentPage==1)?'class="colorWhite"':''?>><i class="fa fa-chevron-left"></i></a>
			<a href="javascript:;" onclick="doShowMsgList('<?php echo $next?>')" <?php echo ($currentPage==$totalPages)?'class="colorWhite"':''?>><i class="fa fa-chevron-right"></i></a>
		</span>
	</div>
	<?php
		}
}else{
	echo '<div class="new_msg_unread"><p>No Messages found</p></div>';
}
?>
</div>