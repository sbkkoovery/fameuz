<div class="dropdown_option_unfriend">      
	<div class="header-popup">
    	<p><i class="fa fa-user-times"> </i><?php echo $unfriendStr ?></p>
    </div>                   	
	<div class="menu-drop">
    <p>Do You want to <?php echo $unfriendStr ?> <span>" <?php echo $objCommon->displayNameSmall($allFollowing,10);?> "</span>?</p>
		<a href="javascript:void(0);" class="unfriend has-spinner" data-friend="<?php echo $unfriendId?>">Yes<span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span></a>
        <a href="#" class="cancel">Cancel</a>
	</div>
	<div class="arrow"><img alt="status_option_arrow" src="<?php echo SITE_ROOT?>images/popover.png"></div>                              
</div>