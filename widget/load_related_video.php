<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/videos.php");
include_once(DIR_ROOT."class/search_functions.php");
$objCommon				   =	new common();
$objVideos				   =	new videos();
$obj_search_functions		=	new search_functions();
$videoEncrId				 =	$objCommon->esc($_GET['videoEncrId']);
$page						=	($objCommon->esc($_GET['page']))?$objCommon->esc($_GET['page']):1;
$pageCount				   =	5;
$pageLimit				   =	$page*$pageCount;
$getVideoDetails		  	 =	$objVideos->getRowSql("SELECT vid.video_title,vid.video_id,user.first_name,user.user_id,user.last_name,user.display_name,user.email,social.usl_fameuz FROM videos AS vid 
										LEFT JOIN users AS user ON  vid.user_id = user.user_id
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id  
										WHERE vid.video_status=1  AND vid.video_encr_id='".$videoEncrId."'");
$searchStr				   =	$getVideoDetails['video_title']." + ".$objCommon->displayName($getVideoDetails);;
$sqlRelatedVideos		  	=	$obj_search_functions->search($searchStr," AND vid.video_id !=".$getVideoDetails['video_id']);
$queryComments			   =	$sqlRelatedVideos." limit 0,".$pageLimit;	
$getRelatedVideos			=	$objVideos->listQuery($queryComments);
$countSqlRel				 =	$objVideos->countRows($sqlRelatedVideos);
?>
<div class="related_box_outer">
	<div class="related_head"><h1>Related Videos</h1></div>
	<?php
	if(count($getRelatedVideos)>0){
		foreach($getRelatedVideos as $allRelatedVideos){
			if($allRelatedVideos['video_type']==1){
				$getAiImages				=	SITE_ROOT_AWS_VIDEOS.'uploads/videos/'.$allRelatedVideos['video_thumb'];
				$vidUrl					 =	SITE_ROOT.'uploads/videos/'.$allRelatedVideos['video_url'];
			}else if($allRelatedVideos['video_type']==2){
				if(preg_match('%^https?://[^\s]+$%',$allRelatedVideos['video_thumb'])) {
					$getAiImages			=	$allRelatedVideos['video_thumb'];
				} else {
					$getAiImages			=	SITE_ROOT_AWS_VIDEOS.'uploads/videos/'.$allRelatedVideos['video_thumb'];
				}
				$vidUrl					 =	$objCommon->html2text($allRelatedVideos['video_url']);
			}
			$displayNameFriend			  =	$objCommon->displayName($allRelatedVideos);
	?>
	<div class="related_video_inner">
		<div class="row">
			<div class="col-md-4">
				<div class="thumb">
					<a href="<?php echo SITE_ROOT.'video/watch/'.$objCommon->html2text($allRelatedVideos['video_encr_id'])?>"><img src="<?php echo $getAiImages?>" class="img-responsive" /><div class="time_duration"><?php echo $allRelatedVideos['video_duration']?></div></a>
					
				</div>
			</div>
			<div class="col-md-8">
				<h3><a href="<?php echo SITE_ROOT.'video/watch/'.$objCommon->html2text($allRelatedVideos['video_encr_id'])?>"><?php echo $objCommon->limitWords($allRelatedVideos['video_title'],50)?></a></h3>
				<p><span>by</span> <?php echo $displayNameFriend?></p>
				<p><span><?php echo number_format($allRelatedVideos['video_visits']);?> views</span></p>
			</div>
		</div>
	</div>
	<?php
		}
		if($pageLimit < $countSqlRel){
			?>
			<div class="row"><div class="col-md-12 text-center"><a class="see_more_videos seeRelatedVid" href="javascript:;" >See more videos</a></div></div>
			<?php
		}
		
	}
	?>
	
</div>
<script language="javascript" type="text/javascript">
$(".seeRelatedVid").on("click",function(){
	$(".loadRelatedVideos").load('<?php echo SITE_ROOT?>widget/load_related_video.php?videoEncrId=<?php echo $videoEncrId ?>&page=<?php echo ($page+1)?>');
});
</script>