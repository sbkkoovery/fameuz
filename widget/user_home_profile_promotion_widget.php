<?php
$myCategoryId							 =	$getUserDetails['uc_c_id'];
$premiumModelsList						=	$_SESSION['promotion_profile'];
if(isset($premiumModelsList)){
	shuffle($premiumModelsList);
	$premiumModelsListRandom 			  =	array_slice($premiumModelsList, 0, 4);
	foreach($premiumModelsListRandom as $allModelsListRandom){
		if (strpos($allModelsListRandom[1], ','.$myCategoryId.',') !== false) {
			$premiumModelString			.=	",".$allModelsListRandom[0];
		}	
	}
	$premiumModelString				   =	trim($premiumModelString,',');
	if($premiumModelString !=''){
		$getPremiumModels				 =	$objUsers->listQuery("SELECT user.*,profileImg.upi_img_url,profileImg.upi_id,social.usl_fameuz,cat.c_name,review.reviewAvg 
																FROM users AS user 
																LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
																LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id 
																LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id  
																LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
																LEFT JOIN (SELECT AVG(review_rate) AS reviewAvg,user_id_to FROM user_reviews GROUP BY user_id_to ) AS review ON user.user_id = review.user_id_to
																WHERE (user.status=1 OR user.status=4) AND user.email_validation=1 AND  user.user_id IN (".$premiumModelString.") ORDER BY FIELD(user.user_id,".$premiumModelString.")");
		if(count($getPremiumModels)>0){
		?>
		<div class="bg_new_white">
			<div class="head_promoted">
				<h4>Premium</h4>
			</div>
			<ul class="promoted_profile">
				<div class="owl-carousel_1">
						<?php
						foreach($getPremiumModels as $allPremiumModels){
								$premiumModelName			=	$objCommon->displayNameSmall($allPremiumModels,10);
								$rating_style_premium		= 	'style="width:'.($allPremiumModels['reviewAvg']*20).'%;"';
								$premiumModelImg			 =	($allPremiumModels['upi_img_url'])?$allPremiumModels['upi_img_url']:'profile_pic.jpg';
						?>
						<li>
							<div class="item_1">
								<div class="img_sec_promo">
									<a href="<?php echo SITE_ROOT.$allPremiumModels['usl_fameuz']?>"><img src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.$premiumModelImg?>"  /></a>
								</div>
								<div class="info_promoted">
									<div class="info_brief">
										<a href="<?php echo SITE_ROOT.$allPremiumModels['usl_fameuz']?>"><?php echo $premiumModelName?></a>
										<div class="rating_box"><div class="rating_yellow" <?php echo $rating_style_premium?>></div></div>
										<span class="proffesion"><?php echo $objCommon->html2text($allPremiumModels['c_name'])?></span>
										<div class="clearfix"></div>
									</div>
									<div class="follow_btn_promoted">
										<div class="follow-btn pull-right">
											<a href="<?php echo SITE_ROOT.$allPremiumModels['usl_fameuz']?>" class="follow">View</a>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</li>
						<?php
						}
						?>
				</div>
			</ul>
		</div>
		<?php
		}
	}
}
?>
<div class="bg_new_white">
	<div class="apps">
		<p class="getapps">Get Fameuz apps</p>
			<span class="desc_app">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
			<a href="#"><img src="<?php echo SITE_ROOT_AWS_IMAGES ?>images/appStore.jpg" /></a>
			<a href="#"><img src="<?php echo SITE_ROOT_AWS_IMAGES ?>images/playStore.jpg" /></a>
	</div>
</div>
<?php
$premiumVideoList						=	$_SESSION['promotion_video'];
if(isset($premiumVideoList)){
	shuffle($premiumVideoList);
	$premiumVideoListRandom 			  =	array_slice($premiumVideoList, 0, 4);
	foreach($premiumVideoListRandom as $allVideoListRandom){
		if (strpos($allVideoListRandom[1], ','.$myCategoryId.',') !== false) {
			$premiumVideoString			.=	",".$allVideoListRandom[0];
		}	
	}
	$premiumVideoString				   =	trim($premiumVideoString,',');
	if($premiumVideoString !=''){
		$getPremiumVideos				 =	$objUsers->listQuery("SELECT video_id,video_encr_id,user_id,video_thumb,video_title FROM videos WHERE  video_status=1 AND video_id IN (".$premiumVideoString.") ORDER BY FIELD(video_id,".$premiumVideoString.")");
		if(count($getPremiumVideos)>0){
?>
	<div class="bg_new_white">
		<div class="head_promoted">
			<h4>Promoted</h4>
		</div>
		<ul class="promoted_profile">
			<div class="owl-carousel_2">
				<?php
				foreach($getPremiumVideos as $allPremiumVideos){
						$thumbPathPremiumModels			=	SITE_ROOT_AWS_VIDEOS.'uploads/videos/'.$objCommon->html2text($allPremiumVideos['video_thumb']);
				?>
				<li>
					<div class="item_2">
						<div class="img_sec_promo" style="position:relative;">
							<img src="<?php echo $thumbPathPremiumModels?>" />
							<a href="<?php echo SITE_ROOT.'video/watch/'.$objCommon->html2text($allPremiumVideos['video_encr_id'])?>" class="playVideoPremium">
							<!--<img src="<?php echo SITE_ROOT_AWS_VIDEOS.'images/play_btn_vid.png'?>" />-->
							</a>
						</div>
						<div class="info_promoted">
						<div class="info_brief Video_promo">
							<a href="<?php echo SITE_ROOT.'video/watch/'.$objCommon->html2text($allPremiumVideos['video_encr_id'])?>"><?php echo $objCommon->limitWords(ucfirst(strtolower($allPremiumVideos['video_title'])),50);?></a>
							<div class="clearfix"></div>
						</div>
					   <div class="clearfix"></div>
					</div>
					</div>
				</li>
				<?php
				}
				?>
			</div>
		</ul>
	</div>
<?php
		}
	}
}
?>
<script language="javascript" type="application/javascript">
$(document).ready(function(e) {
	var owl = $('.owl-carousel_1');
	var owl2 = $('.owl-carousel_2');
	owl.owlCarousel({
		loop:1,
		items : 1,
		autoplay:true
	});
	owl2.owlCarousel({
		loop:1,
		items : 1,
		autoplay:false
	});
});
</script>