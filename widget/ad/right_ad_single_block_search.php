<script type='text/javascript'>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
  (function() {
    var gads = document.createElement('script');
    gads.async = true;
    gads.type = 'text/javascript';
    var useSSL = 'https:' == document.location.protocol;
    gads.src = (useSSL ? 'https:' : 'http:') +
      '//www.googletagservices.com/tag/js/gpt.js';
    var node = document.getElementsByTagName('script')[0];
    node.parentNode.insertBefore(gads, node);
  })();
</script>
<script type='text/javascript'>
  googletag.cmd.push(function() {
    googletag.defineSlot('/194259912/fameuz_burjalsafa_300x233', [300, 233], 'div-gpt-ad-1454585006799-0').addService(googletag.pubads());
  <!--  googletag.pubads().enableSingleRequest();-->
    googletag.enableServices();
  });
</script>

<div class="video_right">
	<div class="left90">
	  <h6>Sponsored <i class="fa fa-bullhorn"></i></h6>
	</div>
	<div class="clr"></div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-12">
	  		<div class="ad">
				<div id='div-gpt-ad-1454585006799-0' style='height:233px; width:300px;'>
					<script type='text/javascript'>
					googletag.cmd.push(function() { googletag.display('div-gpt-ad-1454585006799-0'); });
					</script>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="job-head  filter_search">
						<p><i class="fa fa-filter"></i>&nbsp;Filter by</p>
					</div>
					<?php
					if($cat=='members'){
						include_once(DIR_ROOT."widget/filter_search_members.php");
					}else if($cat=='video'){
						include_once(DIR_ROOT."widget/filter_search_videos.php");
					}else if($cat=='blogs'){
						include_once(DIR_ROOT."widget/filter_search_blogs.php");
					}else if($cat=='companies'){
						include_once(DIR_ROOT."widget/filter_search_companies.php");
					}else if($cat=='music'){
						include_once(DIR_ROOT."widget/filter_search_music.php");
					}else if($cat=='photos'){
						include_once(DIR_ROOT."widget/filter_search_photos.php");
					}
					?>
                </div>
	</div>
</div>