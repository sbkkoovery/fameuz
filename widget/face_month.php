<?php
$getfaceMonthCat					   =	$objUsers->getRowSql("SELECT fmc_category FROM face_month_category WHERE fmc_id =1");
$sqlFaceMonthAll					   =	$objUsers->listQuery("SELECT face.fmm_members,cat.c_name,cat.c_alias FROM face_month_members AS face LEFT JOIN category AS cat  ON face.fmm_cat = cat.c_id  WHERE face.fmm_cat IN(".$getfaceMonthCat['fmc_category'].") ");
?>
<div class="tab_white_most_wanted">
   <div class="info-job search-main"><div class="job-head">
	<p>Faces of the Month</p>
	</div>
	</div>
   <div class="row">
   <?php
   if(count($sqlFaceMonthAll)>0){
  	 foreach($sqlFaceMonthAll as $keyFaceMonth=>$allFaceMonth){
		 if($allFaceMonth['fmm_members']){
			  $faceMembers			=	explode(",",$allFaceMonth['fmm_members']);
			  $faceMembers			=	array_filter($faceMembers);
			  $topfaceMember		  =	$faceMembers[1];
			  $getTopFaceMemDetails   =	$objUsers->getRowSql("SELECT user.user_id,profileImg.upi_img_url FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 WHERE user.status=1 AND user.user_id=".$topfaceMember);
		 }
		 if($getTopFaceMemDetails['user_id'] ==''){
			 continue;
		 }
		
   ?>
	<div class="col-sm-4 col-md-4 col-lg-4">
		<div class="main-face-sec">
			<div class="faceof-month-img">
				<img src="<?php echo SITE_ROOT?>uploads/profile_images/<?php echo($getTopFaceMemDetails['upi_img_url'])?$getTopFaceMemDetails['upi_img_url']:'profile_pic.jpg'?>" />
			</div>
			<div class="model-head">
				<p><?php echo $objCommon->html2text($allFaceMonth['c_name']);?></p>
				<a href="<?php echo SITE_ROOT.'most-wanted/show/'.$objCommon->html2text($allFaceMonth['c_alias'])?>" class="arrow-side">
					<i class="fa fa-chevron-right"></i>
				</a>
			</div>
		</div>
	</div>
   <?php
   			if($keyFaceMonth==2){
				break;
			}
   		}
	}
  ?>
   </div>
</div>