<?php
include_once(DIR_ROOT."class/jobs.php");
$objJobs					 =	new jobs();
$latestJobsSql			   =	"SELECT j.*,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz,profileImg.upi_img_url
										FROM jobs AS j 
										LEFT JOIN users AS user  ON j.user_id	=	user.user_id
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										WHERE ((j.job_start <= CURDATE() AND j.job_end >= CURDATE()) OR j.job_start > CURDATE())  AND j.job_status=1 ORDER BY j.job_created desc LIMIT 4";
$latestJobs				=	$objJobs->listQuery($latestJobsSql);
if(count($latestJobs) >0){
?>
<div class="profile_border_box">
	<h2 class="heading with_border no-color">
		<span>Latest Jobs</span>
		<span class="seemore"><a href="<?php echo SITE_ROOT?>jobs">See More <i class="fa fa-play "></i></a></span>
	</h2>
	<?php
	foreach($latestJobs as $allLatestJobs){
		$job_category				  =	trim($allLatestJobs['job_category'],",");
		$jobCat						=	$objJobs->getRowSql("SELECT group_concat(jc_name) AS jobCats FROM  job_category WHERE jc_id IN(".$job_category.")");
	?>
	<div class="casting_box">
		<div class="image"><a href="<?php echo SITE_ROOT?>jobs/show/job-<?php echo $objCommon->html2text($allLatestJobs['job_id']);?>"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/jobs/<?php echo($allLatestJobs['job_image'])?$allLatestJobs['job_image']:'job-architecture-designer.jpg'?>" alt="casting" /></a></div>
		<div class="text">
			<h5><?php echo $objCommon->html2text($jobCat['jobCats']);?></h5>
			<p class="name"><?php echo $objCommon->html2text($allLatestJobs['job_title']);?> wanted by <span><?php echo $objCommon->limitWords($allLatestJobs['job_company'],50);?></span></p>
			<p class="dates">
				<span class="date">
					<i class="fa fa-calendar"></i>Ends on <?php echo date("d F Y",strtotime($allLatestJobs['job_end']));?>
				</span>  
				<span class="loc">
					<i class="fa fa-map-marker"></i>
				<?php echo $objCommon->html2text($allLatestJobs['job_city']);?>
				</span>
			</p>
		</div>
		<div class="link"><a href="<?php echo SITE_ROOT?>jobs/show/job-<?php echo $objCommon->html2text($allLatestJobs['job_id']);?>"><i class="fa fa-play "></i></a></div>
		<div class="clr"></div>
	</div>
	<?php
	}
	?>
</div>
<?php
}
?>