<div class="tab_white_most_wanted">
   <div class="games-top">
		<div class="row">
			<div class="col-sm-5">
				<div class="nav-games">
					<ul>
						<li <?php echo (@$game_search_cat =='new-games')?'class="active"':''?>><a href="<?php echo SITE_ROOT?>games/search?category=new-games">New Games</a></li>
						<li <?php echo (@$game_search_cat =='my-games')?'class="active"':''?>><a href="<?php echo SITE_ROOT?>games/search?category=my-games">My Games</a></li><?php /*?>
						<li><a href="#">Activitiy</a></li><?php */?>
					</ul>
				</div>
			</div>
			<div class="col-sm-7">
				<div class="search_games">
					<form method="get" action="<?php echo SITE_ROOT?>games/search">
						<div class="row">
							<div class="col-sm-9 less-padding-r">
								<div class="form-group">
										<input type="text" class="form-control"  name="category" placeholder="Search Games.." value="<?php echo ($game_search_cat !='' && $game_search_cat !='new-games' && $game_search_cat !='popular-games' && $game_search_cat !='my-games')?$game_search_cat:''?>">
									</div>
								</div>
							<div class="col-sm-3 less-padding-l">
								<div class="search_game_btn">
									<button type="submit" class="btn btn-default">Submit</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
   </div>
</div>