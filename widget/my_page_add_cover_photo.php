<div class="add_cover_photo">
	<form action='<?php echo SITE_ROOT?>ajax/ajaximage_cover.php?path=<?php echo DIR_ROOT?>uploads/albums/' method="post" enctype="multipart/form-data" id="add_cover_photo">
	<div class="add_img has-spinner">
		<span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span>
		<i class="fa fa-camera"></i> Add a cover image
		<input type="file" class="file_img" name="file" id="file_browse" />
	</div>
	</form>
</div>
<div id="preview"></div>
<script type="text/javascript">
$(document).ready(function(e) {
	$('#add_cover_photo').on('change', function(){ 
		$(".add_img").addClass('active');
		$(".fa-camera").hide();
		$("#add_cover_photo").ajaxForm(
				{
					target: '#preview',
					success:successCall
				
				}).submit();
	});
});
function successCall(){
	location.reload();
}
</script>