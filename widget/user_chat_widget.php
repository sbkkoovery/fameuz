<?php
include_once(DIR_ROOT."class/message.php");
$objMessage						=	new message();
$msgLimit						  =	10;
$sqlMsg							=	"SELECT msg.*,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,attach.ma_attachement,attach.ma_attach_type
									FROM  message AS msg
									LEFT JOIN message_attachement AS attach ON msg.msg_id = attach.msg_id
									LEFT JOIN users AS user ON msg.msg_from = user.user_id
									LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
									LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
									WHERE ((msg.msg_from=".$_SESSION['userId']." AND msg.msg_to=".$getFriendDetails['user_id'].") OR (msg.msg_to=".$_SESSION['userId']." AND msg.msg_from=".$getFriendDetails['user_id'].")) AND msg.msg_status=1 ORDER BY msg.msg_created_date desc ";
$sqlMsgLimit					   =	" limit ".$msgLimit;
$getmsgCount					   =	$objMessage->countRows($sqlMsg);
$getMymessges					  =	$objMessage->listQuery($sqlMsg.$sqlMsgLimit);
?>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.elastic.source.js"></script>

<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
$('.message-container-list').scrollTop($('.message-container-list')[0].scrollHeight);
$('.comment_descr_input2').elastic();

 $('.comment_descr_input2').keydown(function(event){			
  if (event.keyCode == 13 && !event.shiftKey) {
	  $(this).closest("form").find('.ajaxloader').show();
	$(this).closest("form").ajaxForm(
		{
			target: '#preview2',
			success:successCommentCall
		
		}).submit();
	return false;
  }
});
$("#sender-button").click(function() {
	$(this).closest("form").ajaxForm(
		{
			target: '#preview2',
			success:successCommentCall
		
		}).submit();
	return false;
});


/*
$("#sender-button").click(function() {
	var content = $("#comment_descr_input2").val();
	$("#comment_descr_input2").val('');
	
	Comet.send(content);
})
$("#comment_descr_input2").keyup(function(evt) {
	if (evt.keyCode == 13) {
		var content = $("#comment_descr_input2").val();
		$("#comment_descr_input2").val('');
		
		Comet.send(content);
	}
});*/
				
	Comet.connect();
			});
			Comet = {
				connect: function() {
					return $.ajax({
						url: '<?php echo SITE_ROOT?>ajax/bind.php?msgFrom=<?php echo $_SESSION['userId']?>&msgTo=<?php echo $getFriendDetails['user_id']?>',
						type: 'POST',
						success: function(evt, request) {
							$("#content").append(evt);
							$('.message-container-list').scrollTop($('.message-container-list')[0].scrollHeight);
						},
						complete: function() {
							Comet.connect();
						}
					});
				},
				
				/*send: function(text) {
					$.post("<?php echo SITE_ROOT?>access/post_message.php", {msg_descr: text,msg_to:'<?php echo $getFriendDetails['user_id']?>'});
				}*/
			}
function successCommentCall(){
	$("#comment_descr_input2").val('');
	$("#add_photos").val('');
	$('.ajaxloader').hide();
	$("#uploadPreview").hide();
}
			
		</script>
<div class="name-user-inside">
                    <span class="arw-dwn"><img src="<?php echo SITE_ROOT?>images/arw-up-msg.png" /></span>
                	<p><?php echo $objCommon->displayName($getFriendDetails)?>&nbsp;<span class="<?php echo ($getFriendDetails['chatStatus']==1)?'online-e':'offline-e'?>"></span></p>
                </div>
<div class="message-container-list">
                <?php
				$getMymessges 		=	array_reverse($getMymessges);
				if(count($getMymessges)>0){
				$chatdate				=	'';
				$checkdate			   =	'';
				if($getmsgCount> $msgLimit){
					?>
					<div class="loadMoreTime text-center"><a href="javascript:;" class="view_more_comments">View Previous Messages</a></div>
					<div class="more_msg_load"></div>
					<?php
				}
				foreach($getMymessges as $allMyMsg){
					$objMessage->updateField(array("msg_read_status"=>1),"msg_id=".$allMyMsg['msg_id']." AND msg_from=".$getFriendDetails['user_id']);
					?>
                    <div class="message-items">
					<?php
					$chatdate			=	date("d-m-Y",strtotime($objCommon->local_time($allMyMsg['msg_created_date'])));
					if($chatdate !=$checkdate){
						$displayChatDate =	($chatdate==date("d-m-Y"))?'Today':$chatdate;
						echo '<div class="date-line"></div><div class="sapan-date center-block">'.$displayChatDate.'</div>';
					}
					$checkdate		   =	$chatdate;
					?>
                    	<div class="row">
                        	<div class="col-sm-10">
                                <div class="row">
                                    <div class="col-sm-2 alt-col-lg-1 alt-padding-right">
                                        <div class="user-img">
                                            <a href="<?php echo SITE_ROOT.$objCommon->html2text($allMyMsg['usl_fameuz'])?>"><img class="img-responsive" src="<?php echo SITE_ROOT?>uploads/profile_images/<?php echo($allMyMsg['upi_img_url'])?$allMyMsg['upi_img_url']:'profile_pic.jpg'?>" /></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 alt-padding-left">
                                        <div class="msg-user-info">
                                            <div class="user-name-msg">
                                                <p><a href="<?php echo SITE_ROOT.$objCommon->html2text($allMyMsg['usl_fameuz'])?>"><?php echo $objCommon->displayName($allMyMsg)?></a></p>
                                            </div>
                                            <div class="user-msg-brief">
                                                <p><?php echo $objCommon->html2text($allMyMsg['msg_descr'])?>
													<?php
													if($allMyMsg['ma_attachement'] !='' && $allMyMsg['ma_attach_type'] == 1){
													?>
													<br />
													<img src="<?php echo SITE_ROOT.'uploads/message/'.$allMyMsg['ma_attachement']?>" class="img-responsive" style="max-height:200px;" />
													<?php
													}
													?>
												</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                            	<div class="right-options pull-right">
                                	<div class="date-msg pull-left">
                                        <p class="date-time" title="<?php echo date("d M Y , h : i A",strtotime($objCommon->local_time($allMyMsg['msg_created_date'])))?>"><?php echo date("d M , h : i A",strtotime($objCommon->local_time($allMyMsg['msg_created_date'])))?></p>
                                    </div>
                                    <!--<div class="check-one pull-left">
                                    	<div class="checkbox">
                                        <label>
                                            <input class="mark" type="checkbox">
                                        </label>
                                    </div>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php 
					}
				}
				?>
				<div id="content"></div>
                </div>
<div id="preview2"></div>

<form action="<?php echo SITE_ROOT?>access/post_message.php" method="post" enctype="multipart/form-data">
<div class="ajaxloader text-center"><img src="<?php echo SITE_ROOT?>images/ajax-loader.gif" alt="comments_arrow"></div>
	<div class="write-msg">
		<div class="form-group">
			<textarea class="form-control text-area comment_descr_input2" id="comment_descr_input2" name="msg_descr"></textarea>
			<input type="hidden" name="msg_to" value="<?php echo $getFriendDetails['user_id']?>" />
		</div>
		<img id="uploadPreview" src="" width="100" class="img-responsive chatImg" style="display:none;"/>
		<div class="send-items-msg">
			<div class="row">
				<div class="col-sm-6">
					<div class="left-options-msg">
						<ul>
							<li class="smile"><img src="<?php echo SITE_ROOT?>images/smiles.png" /></li>
							<?php /*?><li><img src="<?php echo SITE_ROOT?>images/addfile.png" /><span><input type="file" name="add_file" id="add_file" />Add Files</span></li><?php */?>
							<li><img src="<?php echo SITE_ROOT?>images/photo.png" /><span><input type="file" name="add_photos" id="add_photos"/>Add photos</span></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="left-options-msg right-li pull-right">
						<ul>
							<li class="enter pull-left">Press Enter to send 
								 <label>
										<input type="checkbox">
									</label>
							</li>
							<li><button type="submit" class="btn btn-default" id="sender-button">Send</button></li>
						 </ul>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
</form>
<script>
$("#add_photos").on("change",function(data){
	 var oFReader = new FileReader();
      oFReader.readAsDataURL(document.getElementById("add_photos").files[0]);
	  oFReader.onload = function (oFREvent) {
		  $(".chatImg").show();
            document.getElementById("uploadPreview").src = oFREvent.target.result;
        };
});
$(".view_more_comments").on("click",function(){
	$(this).parent().hide();
	$(".more_msg_load").html('<div class="text-center"><img src="<?php echo SITE_ROOT?>images/ajax-loader.gif" alt="comments_arrow"></div>');
	$.ajax({
		url:'<?php echo SITE_ROOT?>widget/load_more_chat.php?page=1&limit=<?php echo $msgLimit?>&totalCount=<?php echo $getmsgCount?>&chatTo=<?php echo $getFriendDetails['user_id']?>',
		type:"GET",
		success: function(data){
			$(".more_msg_load").html(data);
		}
		});
});
</script>