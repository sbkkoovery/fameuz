<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/email_message.php");
$objCommon				   			  =	new common();
$objEmailMsg							=	new email_message();
$search								 =	$objCommon->esc($_GET['search']);
$count_per_page						 =	20;
$currentPage							=	($_GET['page'])?$objCommon->esc($_GET['page']):1;
if($search){
	$getEmailMessagesSql					=	"SELECT tab1.* FROM (
		SELECT msg.*,msg_user.emu_from,msg_user.emu_to,msg_user.emu_read_status,msg_user.emu_delete_to,user_from.user_id,user_from.first_name,user_from.last_name,user_from.display_name,user_from.email,profileImg.upi_img_url,profileImg.upi_id,social.usl_fameuz,CASE WHEN msg.em_replyof =0 THEN msg.em_id ELSE msg.em_replyof END AS groupId
		FROM email_message_users AS msg_user 
		LEFT JOIN email_message AS msg ON msg_user.em_id =msg.em_id 
		LEFT JOIN users AS user_from ON msg_user.emu_from = user_from.user_id 
		LEFT JOIN user_social_links AS social ON user_from.user_id=social.user_id 
		LEFT JOIN user_profile_image as profileImg ON user_from.user_id=profileImg.user_id AND profileImg.upi_status=1
		WHERE msg_user.emu_to=".$_SESSION['userId']." AND msg_user.emu_delete_to=1 AND user_from.email_validation=1 AND user_from.status=1 AND  (user_from.first_name LIKE '%".$search."%' OR user_from.last_name LIKE '%".$search."%' OR user_from.display_name LIKE '%".$search."%' OR social.usl_fameuz LIKE '%".$search."%'  OR social.usl_fameuz LIKE '%".$search."% 'OR msg.em_subject LIKE '%".$search."%')
		) AS tab1 GROUP BY tab1.groupId ORDER BY tab1.em_createdon DESC";
}else{
	$getEmailMessagesSql					=	"SELECT tab1.* FROM (
		SELECT msg.*,msg_user.emu_from,msg_user.emu_to,msg_user.emu_read_status,msg_user.emu_delete_to,user_from.user_id,user_from.first_name,user_from.last_name,user_from.display_name,user_from.email,profileImg.upi_img_url,profileImg.upi_id,social.usl_fameuz,CASE WHEN msg.em_replyof =0 THEN msg.em_id ELSE msg.em_replyof END AS groupId
		FROM email_message_users AS msg_user 
		LEFT JOIN email_message AS msg ON msg_user.em_id =msg.em_id 
		LEFT JOIN users AS user_from ON msg_user.emu_from = user_from.user_id 
		LEFT JOIN user_social_links AS social ON user_from.user_id=social.user_id 
		LEFT JOIN user_profile_image as profileImg ON user_from.user_id=profileImg.user_id AND profileImg.upi_status=1
		WHERE msg_user.emu_to=".$_SESSION['userId']." AND msg_user.emu_delete_to=1 AND user_from.email_validation=1 AND user_from.status=1
		) AS tab1 GROUP BY tab1.groupId ORDER BY tab1.em_createdon DESC";
}
$totalCount							 =	$objEmailMsg->countRows($getEmailMessagesSql);
$totalPages							 =	ceil(($totalCount/$count_per_page));
$orderFrom							  =	$count_per_page*($currentPage-1);
$orderTo								=	$count_per_page*$currentPage;
$getEmailMessagesSql					.=   " LIMIT ".$orderFrom.",".$count_per_page;
$getEmailMessages					   =	$objEmailMsg->listQuery($getEmailMessagesSql);			
?>
<div class="inbox_msges">
<?php
if(count($getEmailMessages)>0){
	foreach($getEmailMessages as $allEmailMessages) {
		$emailUserImg			=	($allEmailMessages['upi_img_url'])?$allEmailMessages['upi_img_url']:'profile_pic.jpg';
		$emailMsgId			  =	($allEmailMessages['em_replyof']==0)?$allEmailMessages['em_id']:$allEmailMessages['em_replyof'];
	?>
		<div class="new_msg_unread <?php echo ($allEmailMessages['emu_read_status']==1)?'read_messages':''?>">
			<div class="checkBox_delete">
				<input class="check"   type="checkbox" name="msgId" value="<?php echo $allEmailMessages['em_id']?>" />
			</div>
			<div class="sender_img">
				<a href="<?php echo SITE_ROOT.'user/'.$objCommon->html2text($allEmailMessages['usl_fameuz']).'/message/'.$emailMsgId?>" class="colorFameuz"><img src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.$emailUserImg?>" /></a>
			</div>
			<div class="user_info_name_msg">
				<h4><a href="<?php echo SITE_ROOT.'user/'.$objCommon->html2text($allEmailMessages['usl_fameuz']).'/message/'.$emailMsgId?>" class="colorFameuz"><?php echo $objCommon->displayNameSmall($allEmailMessages,18);?></a></h4>
			</div>
			<div class="message_stratz">
				<p><a href="<?php echo SITE_ROOT.'user/'.$objCommon->html2text($allEmailMessages['usl_fameuz']).'/message/'.$emailMsgId?>" class="colorFameuz"><?php echo $objCommon->limitWords($allEmailMessages['em_subject'],100)?></a></p>
			</div>
			<div class="date_msg">
				<p><a href="<?php echo SITE_ROOT.'user/'.$objCommon->html2text($allEmailMessages['usl_fameuz']).'/message/'.$emailMsgId?>" class="colorFameuz"><?php echo date("d/m/Y",strtotime($allEmailMessages['em_createdon']))?></a></p>
			</div>
		</div>
<?php 
	}
	?>
	<div class="new_msg_unread">
		<?php 
		$orderToStr	   =	($orderTo >$totalCount)?$totalCount:$orderTo;
		echo ($orderFrom+1)."-".$orderToStr." of ".$totalCount;
		$prev			=	($currentPage>1)?($currentPage-1):1;
		$next			=	($totalPages > $currentPage)?($currentPage+1):$totalPages;
		if($totalPages >1){
		?>
		<span class="more_optn">
			<a href="javascript:;" onclick="doShowMsgList('<?php echo $prev?>')" <?php echo ($currentPage==1)?'class="colorWhite"':''?>><i class="fa fa-chevron-left"></i></a>
			<a href="javascript:;" onclick="doShowMsgList('<?php echo $next?>')" <?php echo ($currentPage==$totalPages)?'class="colorWhite"':''?>><i class="fa fa-chevron-right"></i></a>
		</span>
	</div>
	<?php
		}
}else{
	echo '<div class="new_msg_unread"><p>No Messages found</p></div>';
}
?>
</div>