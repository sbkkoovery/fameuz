<?php
$profileComplPrimary				=	(int) $getUserDetails['complete_primary'];
$profileComplPersonal		   	   =	(int) $getUserDetails['complete_personal'];
$profileComplPublic			 	 =	(int) $getUserDetails['complete_public'];
$profileComplProfile				=	(int) $getUserDetails['complete_profile_img'];
$profileComplSocial			 	 =	(int) $getUserDetails['complete_social'];
if($getUserDetails['uc_m_id']==1){
	$primaryCompletedPer			=	24-$profileComplPrimary;
	$personalCompletedPer		   =	60-$profileComplPersonal;
	$profileCompletedPer			=	8-$profileComplProfile;
	$socialCompletedPer			 =	8-$profileComplSocial;
}else if($getUserDetails['uc_m_id']==2){
	$primaryCompletedPer			=	12-$profileComplPrimary;
	$personalCompletedPer		   =	40-$profileComplPersonal;
	$publicCompletedPer			 =	32-$profileComplPublic;
	$profileCompletedPer			=	8-$profileComplProfile;
	$socialCompletedPer			 =	8-$profileComplSocial;
}

?>

<div class="profile_completion">
	<div class="profile_graph">
		<p class="">My Profile Completion</p>
		<div class="graph">
		<canvas id="canvas" width="115" height="97"></canvas>
		<?php /*?><img src="<?php echo SITE_ROOT?>images/profile_graph.png" alt="profile_graph" /><?php */?>
		</div>
		<div class="percentage"><?php echo $profileComplStatus?>%</div>
		<div class="clr"></div>
	</div>
	<div class="profile_tips">
		<p class="tip_heading">Profile Completion Tips:</p>
		<p><a href="<?php echo SITE_ROOT.'user/edit-profile?active=public'?>">Public profile details (+<?php echo $personalCompletedPer?>%)</a></p>
		<p><a href="<?php echo SITE_ROOT.'user/edit-profile?active=personal'?>">Personal profile details (+<?php echo $primaryCompletedPer?>%)</a></p>
		<?php
		if($getUserDetails['uc_m_id']==2){
		?>
		<p><a href="<?php echo SITE_ROOT.'user/edit-profile?active=public'?>">Public profile details (+<?php echo $publicCompletedPer?>%)</a></p>
		<?php
		}
		?>
		<p><a href="<?php echo SITE_ROOT.'user/edit-profile?active=profile_photo'?>">Profile image details (+<?php echo $profileCompletedPer?>%)</a></p>
		<p><a href="<?php echo SITE_ROOT.'user/edit-profile?active=social_media'?>">Social details (+<?php echo $socialCompletedPer?>%)</a></p>
	</div>
	<div class="clr"></div>
</div>
<script type="text/javascript">
var complPer =	'<?php echo $profileComplStatus?>';
complPer	 =	parseInt(complPer);
var remainPer=	100 - parseInt(complPer);
var myColor = ["#ffffff","#E4E4E4"];
var myData = [complPer,remainPer];

function getTotal(){
var myTotal = 0;
for (var j = 0; j < myData.length; j++) {
myTotal += (typeof myData[j] == 'number') ? myData[j] : 0;
}
return myTotal;
}

function plotData() {
var canvas;
var ctx;
var lastend = 0;
var myTotal = getTotal();

canvas = document.getElementById("canvas");
ctx = canvas.getContext("2d");
ctx.clearRect(0, 0, canvas.width, canvas.height);



for (var i = 0; i < myData.length; i++) {
ctx.fillStyle = myColor[i];
ctx.beginPath();

ctx.moveTo(50,40);


ctx.arc(50,40,35,lastend,lastend+(Math.PI*2*(myData[i]/myTotal)),false);
ctx.lineTo(50,40);
ctx.fill();
ctx.strokeStyle = "#E4E4E4";
ctx.stroke();
lastend += Math.PI*2*(myData[i]/myTotal);
}
}
plotData();
</script>
