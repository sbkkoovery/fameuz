<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/message.php");
	$objMsg					  =	new message();
	$objCommon				   =	new common();
	$userId					  =	$_SESSION['userId'];
	if($userId){
		$sql_all_messages_notification		=	"SELECT tab.* FROM (SELECT msg.*,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,case when (TIMESTAMPDIFF(MINUTE,chat.ucs_last_seen,'".date("Y-m-d H:i:s")."') >15 or chat.ucs_status=0) then '0' else '1' END AS chatStatus,cat.c_name FROM message AS msg 
LEFT JOIN users AS user ON (msg.msg_from = user.user_id AND msg.msg_to=".$userId." AND msg.msg_trash_to = 1 ) OR (msg.msg_to= user.user_id AND msg.msg_from=".$_SESSION['userId']." AND msg.msg_trash_from = 1 )
LEFT JOIN user_chat_status AS chat ON user.user_id = chat.user_id 
LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
LEFT JOIN user_profile_image AS profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
LEFT JOIN user_categories AS ucat ON user.user_id = ucat.user_id
LEFT JOIN category AS cat ON ucat.uc_c_id = cat.c_id
WHERE (msg.msg_from =".$userId." OR msg.msg_to = ".$userId." )   ORDER BY msg.msg_created_date DESC ) AS tab WHERE tab.user_id !='' GROUP BY tab.user_id ORDER BY msg_created_date desc LIMIT 4";
	$myAllMsgNoti				=	$objMsg->listQuery($sql_all_messages_notification);
	if(count($myAllMsgNoti)>0){
				foreach($myAllMsgNoti as $allNotificationMsg){
					$notification_redirect_url			=	SITE_ROOT.'user/'.$objCommon->html2text($allNotificationMsg['usl_fameuz']).'/chat-messages';
?>
                	<div class="message-items trashClass<?php echo $objCommon->html2text($allNotificationMsg['user_id'])?>">
                                        <div class="user-img">
                                            <a href="javascript:;" onclick="clickme('<?php echo $notification_redirect_url?>');"><img class="img-responsive" src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($allNotificationMsg['upi_img_url'])?$allNotificationMsg['upi_img_url']:'profile_pic.jpg'?>" /></a>
                                    </div>
                                        <div class="msg-user-info">
                                            <div class="user-name-msg">
                                                <p>
												<a href="javascript:;" onclick="clickme('<?php echo $notification_redirect_url?>');"><?php echo $objCommon->displayName($allNotificationMsg)?></a>
												</p>
                                            </div>
                                            <?php /*?><div class="profession-msg">
                                                <p> 
													<a href="javascript:;" onclick="clickme('<?php echo $notification_redirect_url?>');"><?php echo $objCommon->html2text($allNotificationMsg['c_name'])?></a>
													<span class="pull-right date-msg"><?php echo date("M d, Y",strtotime($objCommon->local_time($allNotificationMsg['msg_created_date'])))?></span>
													</p>
                                            </div><?php */?>
                                            <div class="user-msg-brief">
											<?php
											if($allNotificationMsg['msg_from'] !=	$_SESSION['userId']){
												$msgFromOther	   =	'<i class="fa fa-share"></i>';
												$readStatus		 =	($allNotificationMsg['msg_read_status']==0)?'unreadMsg':'';
											}else{
												$msgFromOther	   =	'';
												$readStatus		 =	'';
											}
											?>
                                            <span class="pull-right date-msg"><?php echo date("M d, Y",strtotime($objCommon->local_time($allNotificationMsg['msg_created_date'])))?></span>
                                               <p class="<?php echo $readStatus?>"> <a href="javascript:;" onclick="clickme('<?php echo $notification_redirect_url?>');"><?php echo $msgFromOther?><?php echo $objCommon->limitWords($allNotificationMsg['msg_descr'],150)?></a></p>
                                            </div>
                                        </div>
                                        </div>
                    <?php 
					}
				$objMsg->updateField(array("msg_read_status"=>1),"msg_to='".$userId."'");
			}else{
				echo '<div class="no_messagez"><img class="center-block" src="'.SITE_ROOT_AWS_IMAGES.'images/no_message.png" /><p>No Conversations</p><span>Lorem Ipsum is simply dummy text of the printing 0s,<br/>it to make a type specimen book.</span></div>';
			}
	}
}
?>