<?php
@session_start();
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
	include_once("../includes/site_root.php");
	include_once(DIR_ROOT."class/common_class.php");
	include_once(DIR_ROOT."class/notifications.php");
	$objCommon				   =	new common();
	$objNotifications			=	new notifications();
	$userId					  =	$_SESSION['userId'];
	if($userId){
		$getAllNotification	  =	$objNotifications->listQuery("SELECT noti.notification_type,noti.notification_descr,noti.notification_time,noti.notification_redirect_url,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz,profileImg.upi_img_url FROM notifications AS noti LEFT JOIN users AS user ON noti.notification_from = user.user_id LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 WHERE noti.notification_to='".$userId."' AND notification_type !='' ORDER BY noti.notification_time desc limit 0,6");
		if(count($getAllNotification)>0){
			foreach($getAllNotification as $allNotification){
				if($allNotification['notification_type']=='worked_together'){
					$notification_redirect_url			=	SITE_ROOT.'user/notifications-worked-together';
				}else{
					$notification_redirect_url			=	$objCommon->html2text($allNotification['notification_redirect_url']);
				}
				?>
				<div class="notificationBox">
					<div class="noti_imag">
						<a href="javascript:;" onclick="clickme('<?php echo SITE_ROOT.$objCommon->html2text($allNotification['usl_fameuz'])?>');"><img src="<?php echo SITE_ROOT?>uploads/profile_images/<?php echo($allNotification['upi_img_url'])?$allNotification['upi_img_url']:'profile_pic.jpg'?>" alt="profile_pic" class="img-responsive" /></a>
					</div>
					<div class="noti_text">
				<p><a href="javascript:;" onclick="clickme('<?php echo $notification_redirect_url?>');"><?php echo $objCommon->html2text($allNotification['notification_descr']);?></a></p>
                        <p class="time" style="font-size:75%;"><span><i class="fa fa-clock-o"></i><?php echo date("d M , Y h:i a",strtotime($objCommon->local_time($allNotification['notification_time'])));?></span></p>
					</div>
				</div>
				<?php
			}
			$objNotifications->updateField(array("notification_read_status"=>1),"notification_to='".$userId."'");
		}else{
			echo '<p>No notification found...</p>';
		}
	}
}
?>