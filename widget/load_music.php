<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/music.php");
$objCommon				   =	new common();
$objMusic					=	new music();
$keyword					 =	$objCommon->esc($_GET['keyword']);
$keywordCat				  =	$objCommon->esc($_GET['keywordCat']);
$myCategoryId		   		=	$objCommon->esc($_GET['myCategoryId']);
$countryId			  	   =	$_SESSION['country_user']['country_id'];
$perPage 				   	 = 	20;
$page 					    = 	1;
if(!empty($_GET["page"])) {
	$page 					=	$_GET["page"];
}
if($keyword){
	$sqlMusicTop			 =	"SELECT music.music_id,music.music_title,music.music_descr,music.music_artist,music.music_thumb,music.music_url ,music.music_created,play_list.myp_id ,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name,review.reviewAvg,ml.ml_status,CASE WHEN play_list.myp_id > 0 THEN 1 ELSE 0 END as PlayList,mv.mv_id
										FROM music
										lEFT JOIN users AS user ON music.user_id = user.user_id
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id 
										LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
										LEFT JOIN (SELECT AVG(review_rate) AS reviewAvg,user_id_to FROM user_reviews GROUP BY user_id_to ) AS review ON user.user_id = review.user_id_to
										LEFT JOIN music_like AS ml ON music.music_id = ml.music_id AND ml.user_id = ".$_SESSION['userId']."
										LEFT JOIN my_playlist AS play_list ON music.music_id = play_list.music_id AND play_list.user_id = ".$_SESSION['userId']."
										LEFT JOIN music_vote AS mv ON music.music_id = mv.music_id AND mv.user_id = ".$_SESSION['userId']."
										WHERE music.music_status=1 AND (music.music_title LIKE '%".$keyword."%' OR music.music_artist LIKE '%".$keyword."%' OR music.music_tags LIKE '%".$keyword."%')
										ORDER BY music_created DESC";
										
}else if($keywordCat=='top'){
	$sqlMusicTop			 =	"SELECT music.music_id,music.music_title,music.music_descr,music.music_artist,music.music_thumb,music.music_url ,music.music_created,play_list.myp_id ,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name,review.reviewAvg,ml.ml_status,musicLike.likeCount,CASE WHEN play_list.myp_id > 0 THEN 1 ELSE 0 END as PlayList,mv.mv_id
										FROM music
										lEFT JOIN users AS user ON music.user_id = user.user_id
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id 
										LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
										LEFT JOIN (SELECT AVG(review_rate) AS reviewAvg,user_id_to FROM user_reviews GROUP BY user_id_to ) AS review ON user.user_id = review.user_id_to
										LEFT JOIN (SELECT SUM(ml_status) AS likeCount,music_id FROM music_like GROUP BY music_id) AS musicLike ON music.music_id = musicLike.music_id
										LEFT JOIN music_like AS ml ON music.music_id = ml.music_id AND ml.user_id = ".$_SESSION['userId']."
										LEFT JOIN my_playlist AS play_list ON music.music_id = play_list.music_id AND play_list.user_id = ".$_SESSION['userId']."
										LEFT JOIN music_vote AS mv ON music.music_id = mv.music_id AND mv.user_id = ".$_SESSION['userId']."
										WHERE music.music_status=1 ORDER BY musicLike.likeCount DESC";
}else if($keywordCat=='popular'){
	$sqlMusicTop			 =	"SELECT music.music_id,music.music_title,music.music_descr,music.music_artist,music.music_thumb,music.music_url ,music.music_created,play_list.myp_id ,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name,review.reviewAvg,mv.mv_visit,CASE WHEN play_list.myp_id > 0 THEN 1 ELSE 0 END as PlayList,mv.mv_id
										FROM music
										lEFT JOIN users AS user ON music.user_id = user.user_id
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id 
										LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
										LEFT JOIN (SELECT AVG(review_rate) AS reviewAvg,user_id_to FROM user_reviews GROUP BY user_id_to ) AS review ON user.user_id = review.user_id_to
										LEFT JOIN music_views AS mv ON music.music_id = mv.music_id
										LEFT JOIN my_playlist AS play_list ON music.music_id = play_list.music_id AND play_list.user_id = ".$_SESSION['userId']."
										LEFT JOIN music_vote AS mv ON music.music_id = mv.music_id AND mv.user_id = ".$_SESSION['userId']."
										WHERE music.music_status=1 ORDER BY mv.mv_visit DESC";
}else if($keywordCat=='last'){
	$sqlMusicTop			 =	"SELECT music.music_id,music.music_title,music.music_descr,music.music_artist,music.music_thumb,music.music_url ,music.music_created,play_list.myp_id ,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name,review.reviewAvg,CASE WHEN play_list.myp_id > 0 THEN 1 ELSE 0 END as PlayList,mv.mv_id
										FROM music_views AS mviews
										LEFT JOIN  music ON mviews.music_id = music.music_id
										lEFT JOIN users AS user ON music.user_id = user.user_id
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id 
										LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
										LEFT JOIN (SELECT AVG(review_rate) AS reviewAvg,user_id_to FROM user_reviews GROUP BY user_id_to ) AS review ON user.user_id = review.user_id_to
										LEFT JOIN my_playlist AS play_list ON music.music_id = play_list.music_id AND play_list.user_id = ".$_SESSION['userId']."
										LEFT JOIN music_vote AS mv ON music.music_id = mv.music_id AND mv.user_id = ".$_SESSION['userId']."
										WHERE music.music_status=1 AND mviews.user_id= ".$_SESSION['userId']." ORDER BY mviews.mv_created DESC";
}else if($keywordCat=='new'){
	$sqlMusicTop			 =	"SELECT music.music_id,music.music_title,music.music_descr,music.music_artist,music.music_thumb,music.music_url ,music.music_created,play_list.myp_id ,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name,review.reviewAvg,ml.ml_status,CASE WHEN play_list.myp_id > 0 THEN 1 ELSE 0 END as PlayList,mv.mv_id
										FROM music
										lEFT JOIN users AS user ON music.user_id = user.user_id
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id 
										LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
										LEFT JOIN (SELECT AVG(review_rate) AS reviewAvg,user_id_to FROM user_reviews GROUP BY user_id_to ) AS review ON user.user_id = review.user_id_to
										LEFT JOIN music_like AS ml ON music.music_id = ml.music_id AND ml.user_id = ".$_SESSION['userId']."
										LEFT JOIN my_playlist AS play_list ON music.music_id = play_list.music_id AND play_list.user_id = ".$_SESSION['userId']."
										WHERE music.music_status=1 ORDER BY music_created DESC";
}else{
	$sqlMusicTop			 =	"SELECT music.music_id,music.music_title,music.music_descr,music.music_artist,music.music_thumb,music.music_url ,music.music_created,play_list.myp_id ,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name,review.reviewAvg,ml.ml_status,CASE WHEN play_list.myp_id > 0 THEN 1 ELSE 0 END as PlayList,case when (promo.promo_id !='') then 1 else 0 END AS promoted_music,mv.mv_id
										FROM music
										lEFT JOIN users AS user ON music.user_id = user.user_id
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id 
										LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
										LEFT JOIN (SELECT AVG(review_rate) AS reviewAvg,user_id_to FROM user_reviews GROUP BY user_id_to ) AS review ON user.user_id = review.user_id_to
										LEFT JOIN music_like AS ml ON music.music_id = ml.music_id AND ml.user_id = ".$_SESSION['userId']."
										LEFT JOIN music_vote AS mv ON music.music_id = mv.music_id AND mv.user_id = ".$_SESSION['userId']."
										LEFT JOIN my_playlist AS play_list ON music.music_id = play_list.music_id AND play_list.user_id = ".$_SESSION['userId']."
										LEFT JOIN promotion AS promo ON music.music_id = promo.promo_content_id AND promo.promo_type='music' AND  promo.promo_country LIKE '%,".$countryId.",%' AND promo.promo_category LIKE '%".$myCategoryId."%' AND DATE(NOW()) between promo.promo_start_date and promo.promo_end_date AND promo.promo_admin_status=1 AND promo.promo_status=1
										WHERE music.music_status=1 ORDER BY promoted_music DESC,music_created DESC";
}
$start 							=	($page-1)*$perPage;
if($start < 0) { $start = 0; }
$queryMusicTop 				 	= 	$sqlMusicTop . " limit " . $start . "," . $perPage; 
if(empty($_GET["rowcount"])) {
	$_GET["rowcount"] 			 =	$countLoadMusic	=	$objMusic->countRows($sqlMusicTop);
}
$pages  							=	ceil($_GET["rowcount"]/$perPage);
$getTopMusic				   	  =	$objMusic->listQuery($queryMusicTop);
$countTopMusic			   		=	count($getTopMusic);
if($start < $countLoadMusic){
if($countTopMusic >0){
	echo '<input type="hidden" class="pagenum" value="' . $page . '" /><input type="hidden" class="total-page" value="'.$pages.'" /><input type="hidden" id="total-count" value="'.$countLoadMusic.'" />';
foreach($getTopMusic as $keyTopMusic=>$allTopMusic){
	if(($keyTopMusic+1)%2==1){
		echo '<div class="albums c"><div class="row">';
	}
	$musicThumb				   =	($allTopMusic['music_thumb'])?$allTopMusic['music_thumb']:'audio-thump.jpg';
	$musicUser					=	$objCommon->displayName($allTopMusic);
	$musicUserThumb			   =	($allTopMusic['upi_img_url'])?$allTopMusic['upi_img_url']:'profile_pic.jpg';
	$musicCreated				 =	date(" F d, Y ",strtotime($allTopMusic['music_created']));
	$musicuserRate				=	round($allTopMusic['reviewAvg'])*20;
	$musicLikeStatus			  =	($allTopMusic['ml_status']==1)?1:0;
?>
	<div class="col-sm-6 col-lg-sp-5-alt <?php echo (($keyTopMusic+1)%2==0)?'pull-right':''?>">
	<div class="album c ">
		<div class="row">
			<div class="col-sm-7 alt-padding-r-alt">
				<div class="desc ">
					<h6><?php echo $objCommon->html2text($allTopMusic['music_title']);?></h6>
					<p><?php echo $objCommon->limitWords($allTopMusic['music_descr'],150);?></p>
					<?php
					if($allTopMusic['promoted_music']==1){
						echo '<div class="promoted_music"><i class="fa fa-external-link-square"></i> Promoted</div>';
					}
					?>
						<div class="player"><a href="javascript:;" data-musicid="<?php echo $allTopMusic['music_id']?>" data-artist="<?php echo $objCommon->html2text($allTopMusic['music_artist']);?>" 
						data-musicdesc="<?php echo addslashes($objCommon->limitWords($allTopMusic['music_descr'],150));?>" data-image="<?php echo SITE_ROOT_AWS_MUSIC.'uploads/music/'.$musicThumb;?>"  data-path="<?php echo SITE_ROOT_AWS_MUSIC.'uploads/music/'.$objCommon->html2text($allTopMusic['music_url']);?>" data-userInfo="<?php echo $musicUser?>" data-userthumb="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.$musicUserThumb;?>" data-dateCreated="<?php echo $musicCreated?>" data-proffesion="<?php echo $objCommon->html2text($allTopMusic['c_name']);?>" data-rating_yellow="<?php echo $musicuserRate?>%" data-likei="<?php echo $allTopMusic['music_id']?>" data-sharei="<?php echo $allTopMusic['music_id']?>" data-like_status="<?php echo $musicLikeStatus?>" data-musicUser="<?php echo $allTopMusic['user_id'] ?>" data-votei="<?php echo $allTopMusic['music_id'] ?>" data-vote_status="<?php echo $allTopMusic['mv_id'] ?>"  class="p_button  play_on_click notplay playingOn unlisted<?php  echo $allTopMusic['music_id'] ?>"></a>                                                                

						<span class="dur"> <!--Listen 3:44-->&nbsp;</span>
						<?php
						if($allTopMusic['PlayList']==0){
						?>
						<a href="javascript:;" class="list right has-spinner add_to_play_list" data-musicid="<?php echo $allTopMusic['music_id']?>"><span class="hidden-sm hidden-md">
						<span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span>
						Playlist</span><i class="fa fa-bars hidden-lg"></i>
						</a>
						<?php
						}
						?></div>
					</div>
				</div>
		<div class="col-sm-5 alt-padding-l-alt"><div class="thumb_audio"><img src="<?php echo SITE_ROOT_AWS_MUSIC.'uploads/music/'.$musicThumb?>" alt="thump" /></div></div>
		</div>
	</div>
	</div>
<?php
 if(($keyTopMusic+1)%2==0 ){
	echo '</div></div>';
}else if(($keyTopMusic ==($countTopMusic-1))){
	echo '</div></div>';
}
}
}else{
	echo '<p>No music found...</p>';
}
}
?>
<script>
loadMusicScript();
</script>