<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/comments.php");
$objCommon					=	new common();
$objComments			      =	new comments();
$sideHalf					 =	$objCommon->esc($_GET['sideHalf']);
$imageId					  =	$objCommon->esc($_GET['imageId']);
$im_type					  =	$objCommon->esc($_GET['im_type']);
$userto					   =	$objCommon->esc($_GET['userto']);
$myimage					  =	$objCommon->esc($_GET['myimage']);
$getVidId					 =	$objComments->getRowSql("select video_id from videos where video_encr_id='".$imageId."'");
$vidEnc					   =	$getVidId['video_id'];
$page						 =	($objCommon->esc($_GET['page']))?$objCommon->esc($_GET['page']):1;
$pageCount				    =	5;
$pageLimit				    =	$page*$pageCount;
$sqlComments		    	  =	"SELECT comments.comment_id,comments.comment_descr,comments.comment_time,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz FROM comments 
	LEFT JOIN users AS user ON comments.comment_user_by = user.user_id
	LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
	LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
	WHERE comments.comment_status=1 and comments.comment_cat=".$im_type." AND comments.comment_content =".$vidEnc." AND comments.comment_status=1 ORDER BY comments.comment_time desc ";
$countSql				  	 =	$objComments->countRows($sqlComments);
$queryComments			 	=	$sqlComments." limit 0,".$pageLimit;
$getMyComments			 	=	$objComments->listQuery($queryComments);
?>
<link href="<?php echo SITE_ROOT?>perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo SITE_ROOT?>perfect-scrollbar/perfect-scrollbar.js"></script>	
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.form.js"></script>
<script src="<?php echo SITE_ROOT?>js/jquery.elastic.source.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.css" />
<script src="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.js"></script>
<div class="comments">
<?php if($pageLimit < $countSql){ ?>
<div class="loadMoreTime" data-pageNo="<?php echo $page?>"><a href="javascript:;" class="view_more_comments">View Previous Comments</a></div>
<?php } else { echo '<div class="loadMoreTime"><a href="javascript:;" class="">Comments</a></div>'; } ?>
	<div class="comment_shadow">
		<div class="cmnt_cnt" id="comments" style="max-height:<?php echo $sideHalf."px;" ?>">
		<?php
		if(count($getMyComments)>0){
		for($i=(count($getMyComments)-1);$i>=0;$i--){
		$displayNameComment	 =	$objCommon->displayName($getMyComments[$i]);
		$getLikeCount			   =	$objComments->getRowSql("SELECT count(like_id) AS likeCount,group_concat(like_user_id) AS youLike FROM likes WHERE like_content =".$getMyComments[$i]['comment_id']." AND like_cat =5 AND like_status=1");
		$likeCount				  =	$getLikeCount['likeCount'];
		$youLikeArr	   			 =	explode(",",$getLikeCount['youLike']);
		if(count($youLikeArr)>0){
			if(in_array($_SESSION['userId'],$youLikeArr)){
				$youLike	  		=	1;
			}else{
				$youLike	  		=	0;
			}
		}
		$commentTimerply			=	$objCommon->time_elapsed_string(strtotime($objCommon->local_time($getMyComments[$i]['comment_time'])));
		?>
			<div class="comment c">
				<span class="prof_pic"><img src="<?php echo SITE_ROOT?>uploads/profile_images/<?php echo($getMyComments[$i]['upi_img_url'])?$objCommon->html2text($getMyComments[$i]['upi_img_url']):'profile_pic.jpg'?>" alt="pro-thumb" /></span>
				<span class="commnt_content">
					<span class="name"><?php echo $displayNameComment?></span>
					<span class="text"><?php echo $objCommon->html2text($getMyComments[$i]['comment_descr']);?></span>
					<span class="likeBox"> 
						<div class="likecom pull-left"><a data-userto="<?php echo $objCommon->html2text($getMyComments[$i]['user_id'])?>" data-like="<?php echo $objCommon->html2text($getMyComments[$i]['comment_id'])?>" class="like_button <?php echo ($youLike==1)?'liked':''?>" href="javascript:;"><?php echo ($youLike==1)?'Like':'Like'?> <i class="fa fa-heart"></i> <span> <?php echo $likeCount?></a></span> </div>
						<span class="time"><?php echo $commentTimerply?></span>
					</span>
				</span>
				<?php
					if($getMyComments[$i]['user_id']== $_SESSION['userId']){
					?>
					<div class="deletComment"><a href="javascript:;" title="Delete" data-delid="<?php echo $getMyComments[$i]['comment_id']?>"><i class="fa fa-times"></i></a></div>
					<?php
					}
				?>
			</div>
		<?php
			}
		}
		?>
		</div>
	</div>
</div>
<div id="preview2"></div>
<?php /*?><div class="comment_box">
	<div class="prof_thump"><img src="<?php echo SITE_ROOT?>uploads/profile_images/<?php echo($myimage)?$myimage:'profile_pic.jpg'?>" alt="prof_thump"/></div>
	<div class="post_commnt">
		<form action="<?php echo SITE_ROOT?>access/post_video_comment.php?action=add_comment" method="post" enctype="multipart/form-data">
			<div class="arrow"><img alt="arrow" src="<?php echo SITE_ROOT?>images/arrow_left.png"></div>
			<div class="textarea">
				<span class="smile_icon"><i class="fa fa-smile-o"></i></span>
				<textarea name="comment_descr_input" rows="1" class="comment_descr_input2" ></textarea>
				<input type="hidden" name="video_id" value="<?php echo $imageId?>"  />
				<input type="hidden" name="userto" value="<?php echo $userto?>"  />
			<br/><small>Press Enter to post.</small></div>
			<div class="ajaxloader text-center"><img src="<?php echo SITE_ROOT?>images/ajax-loader.gif" alt="comments_arrow"></div>
		</form>
	</div>
	<div class="clr"></div>
</div><?php */?>
<script type="text/javascript">
//$('#comments').perfectScrollbar();
//$('.comment_descr_input2').elastic();
// $('.comment_descr_input2').keydown(function(event){			
//		  if (event.keyCode == 13 && !event.shiftKey) {
//			$(this).closest("form").find('.ajaxloader').show();
//			$(this).closest("form").ajaxForm(
//				{
//					target: '#preview2',
//					success:successCommentCall
//				
//				}).submit();
//			return false;
//		  }
//	});
//function successCommentCall(){
//	$('.ajaxloader').hide();
//	$(".comment_descr_input2").val('');
//	$(".image_comment_load").load('<?php echo SITE_ROOT?>widget/pop_video_comment_box.php?sideHalf=<?php echo $sideHalf?>&imageId=<?php echo $imageId?>&im_type=<?php echo $im_type?>&userto=<?php echo $userto?>&myimage=<?php echo $myimage?>');
//}
//$(".loadMoreTime").on("click",".view_more_comments",function(){
//		$(".image_comment_load").load('<?php echo SITE_ROOT?>widget/pop_video_comment_box.php?sideHalf=<?php echo $sideHalf?>&imageId=<?php echo $imageId?>&im_type=<?php echo $im_type?>&userto=<?php echo $userto?>&page=<?php echo ($page+1)?>&myimage=<?php echo $myimage?>');
//});
$(".likecom").on("click",".like_button",function(){
		var likeId		=	$(this).data('like');
		var likecat	   =	5;
		var imgUserId	 =	$(this).data('userto');
		var that		  =	this;
		var imageType	 =	'<?php echo $im_type?>';
		var imageId	   =	'<?php echo $imageId?>';
		if(likeId !='' && likecat != ''){
			$.ajax({
				url:"<?php echo SITE_ROOT?>ajax/like_video_comment_pop.php",
				method: "POST",
				data:{likeId:likeId,likecat:likecat,imgUserId:imgUserId,videoId:imageId},
				success:function(result){
					var count	=	$(that).children('span').html();
					if(result === 'cmdLiked'){
						$(that).addClass('liked');
						$(that).children('span').html(parseInt(count)+1);
					}else if(result === 'cmdLike'){
						$(that).removeClass('liked');
						$(that).children('span').html(parseInt(count)-1);
					}
			}});
		}
});
//$(".comment").on("mouseover",function(){
//	$(".deletComment a").hide();
//	$(this).find(".deletComment a").show();
//});
//$(".comment").on("mouseout",function(){
//	$(".deletComment a").hide();
//});
//$(".deletComment").on("click","a",function(){
//	var elem = $(this).closest('.item');	
//	var that		=	this;			
//	$.confirm({
//		'title'	  : 'Delete Confirmation',
//		'message'	: 'You are about to delete this comment ?',
//		'buttons'	: {
//		'Yes'	: {
//		'class'	: 'blue',
//		'action': function(){
//		elem.slideUp();
//			$('.ajaxloader').show();
//			var delid       =	$(that).data("delid");
//			if(delid){
//				$.ajax({
//					url :'<?php echo SITE_ROOT?>access/delete_comment.php',
//					method:"POST",
//					data:{delid:delid},
//					success:function(result){
//						$(".image_comment_load").load('<?php echo SITE_ROOT?>widget/pop_video_comment_box.php?sideHalf=<?php echo $sideHalf?>&imageId=<?php echo $imageId?>&im_type=<?php echo $im_type?>&userto=<?php echo $userto?>&myimage=<?php echo $myimage?>');
//						$('.ajaxloader').hide();
//					}});
//			}
//		}
//		},
//		'No'	: {
//		'class'	: 'gray',
//		'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
//		}
//		}
//	});
//});
</script>