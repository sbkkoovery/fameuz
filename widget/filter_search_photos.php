<form method="get" action="<?php echo SITE_ROOT?>search">
	<div class="row serach_filter">
		<div class="col-sm-12">
				<div class="form-group upload-help">
				<select class="form-control" name="filter_category" id="filter_category">
					<option value="">Select Category</option>
					<option value="new-photos" <?php echo ($_GET['filter_category']=='new-photos')?'selected':''?>>New Photos</option>
					<option value="popular" <?php echo ($_GET['filter_category']=='popular')?'selected':''?>>Popular Photos</option>
					<option value="top" <?php echo ($_GET['filter_category']=='top')?'selected':''?>>Top Photos</option>
					<option value="last-visited" <?php echo ($_GET['filter_category']=='last-visited')?'selected':''?>>Last Visited Photos</option>
					<option value="all" <?php echo ($_GET['filter_category']=='all')?'selected':''?>>All Photos</option>
				</select>
				</div>
			</div>
		<div class="col-sm-12">
				<div class="form-group">
                <?php 
				$filterKeyWords	=	$objCommon->esc($_GET['filter_keywords']);
				if(isset($_GET['searchKey'])&&$_GET['searchKey']!=""){
					$filterKeyWords	=	$objCommon->esc($_GET['searchKey']);
				}
				?>
				<input type="text" class="form-control checkme"  name="filter_keywords" id="filter_keywords" placeholder="Type Keywords" value="<?php echo $filterKeyWords; ?>">
				</div>
			</div>
		<div class="col-sm-12">
			  <input type="hidden" name="cat" value="photos" />
			  <button type="submit" class="btn btn-default">Submit</button>
			  <a href="javascript:;" class="clear_all pull-right">Clear All</a>
			</div>
	</div>
</form>
<script type="application/javascript">
$(".clear_all").on("click",function(){
	$("#filter_category").val('');
	$("#filter_keywords").val('');
});
</script>