<div class="col-xs-12 col-sm-12 col-md-12 col-lg-sp-3">
<?php include_once(DIR_ROOT."widget/right_ads_sidebar.php");?>
</div>
<style>
.more_games{
	margin-top:10px;
	border-bottom:1px solid #ccc;
	background:#fff;
	padding:10px 30px;
/*	position:relative;*/
}
.more_games ul li{
	width:60px;
	height:60px;
	overflow:hidden;
	display:inline-block;
}
.more_games ul li img{
	width:100%;
	min-height:100%;
}
.more_games ul:nth-of-type(1){
	margin-bottom:10px;
}
.more_games .owl-controls{
	position:absolute;
	z-index:999999;
	width:100%;
}
.more_games .owl-prev{
	left:-20px;
	position:absolute;
	bottom:20px;
	color:#cccccc;
}
.more_games .owl-next{
	right:-20px;
	position:absolute;
	bottom:20px;
	color:#cccccc;
}
.more_games h3{
	font-size:14px;
	font-weight:500;
	color:#4e4f53;
	margin-bottom:10px;
}
.more_games h3 a{
	text-decoration:none;
	color:#afb420;
	font-size:12px;
}
</style>
<script>
	$(document).ready(function(e) {
        
		var owl = $('.more_games ul');
		owl.owlCarousel({
			autoplay:true,
			autoplayTimeout:3000,
			loop:true,
			nav:true,
			navText: [ '<i class="fa fa-chevron-left arr"></i>', '<i class="fa fa-chevron-right arr1"></i>' ],
			margin: 5,
			responsive:{
				0:{
					items:1
				},
				600:{
					items:3
				},            
				960:{
					items:3
				},
				1200:{
					items:4
				}
			}
		});	
	});
</script>