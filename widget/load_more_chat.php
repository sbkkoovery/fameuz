<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/message.php");
$objCommon				     	 =	new common();
$objMessage						=	new message();
$msgLimit						  =	$objCommon->esc($_GET['limit']);
$page							  =	$objCommon->esc($_GET['page']);
$pageLimit				    	 =	$page*$msgLimit;
$totalCount						=	$objCommon->esc($_GET['totalCount']);
$chatTo							=	$objCommon->esc($_GET['chatTo']);
$sqlMsg							=	"SELECT msg.*,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,attach.ma_attachement,attach.ma_attach_type
									FROM  message AS msg
									LEFT JOIN message_attachement AS attach ON msg.msg_id = attach.msg_id
									LEFT JOIN users AS user ON msg.msg_from = user.user_id
									LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
									LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
									WHERE ((msg.msg_from=".$_SESSION['userId']." AND msg.msg_to=".$chatTo.") OR (msg.msg_to=".$_SESSION['userId']." AND msg.msg_from=".$chatTo.")) AND msg.msg_status=1 ORDER BY msg.msg_created_date desc ";
$sqlMsgLimit					   =	" limit ".$pageLimit.",".$msgLimit;
$getMymessges					  =	$objMessage->listQuery($sqlMsg.$sqlMsgLimit);
?>
                <?php
				$getMymessges 		=	array_reverse($getMymessges);
				if(count($getMymessges)>0){
				$chatdate				=	'';
				$checkdate			   =	'';
				if($totalCount> (($page+1)*$msgLimit)){
					?>
					<div class="loadMoreTime text-center"><a href="javascript:;" class="view_more_comments">View Previous Messages</a></div>
					<div class="more_msg_load"></div>
					<?php
				}
				foreach($getMymessges as $allMyMsg){
					?>
                    <div class="message-items">
					<?php
					$chatdate			=	date("d-m-Y",strtotime($objCommon->local_time($allMyMsg['msg_created_date'])));
					if($chatdate !=$checkdate){
						$displayChatDate =	($chatdate==date("d-m-Y"))?'Today':$chatdate;
						echo '<div class="date-line"></div><div class="sapan-date center-block">'.$displayChatDate.'</div>';
					}
					$checkdate		   =	$chatdate;
					?>
                    	<div class="row">
                        	<div class="col-sm-10">
                                <div class="row">
                                    <div class="col-sm-2 alt-col-lg-1 alt-padding-right">
                                        <div class="user-img">
                                            <a href="<?php echo SITE_ROOT.$objCommon->html2text($allMyMsg['usl_fameuz'])?>"><img class="img-responsive" src="<?php echo SITE_ROOT?>uploads/profile_images/<?php echo($allMyMsg['upi_img_url'])?$allMyMsg['upi_img_url']:'profile_pic.jpg'?>" /></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 alt-padding-left">
                                        <div class="msg-user-info">
                                            <div class="user-name-msg">
                                                <p><a href="<?php echo SITE_ROOT.$objCommon->html2text($allMyMsg['usl_fameuz'])?>"><?php echo $objCommon->displayName($allMyMsg)?></a></p>
                                            </div>
                                            <div class="user-msg-brief">
                                                <p><?php echo $objCommon->html2text($allMyMsg['msg_descr'])?>
													<?php
													if($allMyMsg['ma_attachement'] !='' && $allMyMsg['ma_attach_type'] == 1){
													?>
													<br />
													<img src="<?php echo SITE_ROOT.'uploads/message/'.$allMyMsg['ma_attachement']?>" class="img-responsive" style="max-height:200px;" />
													<?php
													}
													?>
												</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                            	<div class="right-options pull-right">
                                	<div class="date-msg pull-left">
                                        <p class="date-time" title="<?php echo date("d M Y , h : i A",strtotime($objCommon->local_time($allMyMsg['msg_created_date'])))?>"><?php echo date("d M , h : i A",strtotime($objCommon->local_time($allMyMsg['msg_created_date'])))?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php 
					}
				}
				?>
              

<script>
$(".view_more_comments").on("click",function(){
	$(this).parent().hide();
	$(".more_msg_load").html('<div class="text-center loaderChat"><img src="<?php echo SITE_ROOT?>images/ajax-loader.gif" alt="comments_arrow"></div>');
	$.ajax({
		url:'<?php echo SITE_ROOT?>widget/load_more_chat.php?page=<?php echo ($page+1)?>&limit=<?php echo $msgLimit?>&totalCount=<?php echo $totalCount?>&chatTo=<?php echo $chatTo?>',
		type:"GET",
		success: function(data){
			$(".loaderChat").hide();
			$(".more_msg_load").prepend(data);
		}
		});
});
</script>