<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/likes.php");
$objCommon				     =	new common();
$objLikes					  =	new likes();
$postid						=	$objCommon->esc($_GET['postid']);
$posttype					  =	$objCommon->esc($_GET['posttype']);
if($postid != '' && $posttype != ''){
	$sqlUserLiked			  =	"SELECT user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name,following.follow_user1,following.follow_user2,following.follow_status
									FROM likes
									LEFT JOIN users AS user ON  likes.like_user_id = user.user_id
									LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
									LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
									LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id 
									LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id
									LEFT JOIN following ON (user.user_id = following.follow_user1 OR user.user_id = following.follow_user2) AND (following.follow_user1='".$_SESSION['userId']."' OR following.follow_user2='".$_SESSION['userId']."')
									WHERE likes.like_cat = ".$posttype." AND likes.like_content = ".$postid." AND likes.like_status=1 GROUP BY user.user_id  ORDER BY likes.like_time DESC ";
	$getLikesPost			  =	$objLikes->listQuery($sqlUserLiked);
	if(count($getLikesPost) >0){
		?>
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel"><?php echo number_format(count($getLikesPost))?> People Liked</h4>
		</div>
		<div class="modal-body">
			<ul>
			<?php
			foreach($getLikesPost as $allLikePost){
				$userImg	=	($allLikePost['upi_img_url'])?$allLikePost['upi_img_url']:'profile_pic.jpg';
				if($allLikePost['follow_status']==2){ 
					$friendStatus =2; 
				}else if($allLikePost['follow_status']==1 && $allLikePost['follow_user1']==$_SESSION['userId']){ 
					$friendStatus =1; 
				}else { 
					$friendStatus=0; 
				}
			?>
				<li>
					<div class="liked_users">
						<div class="img_users_liked">
							<a href="<?php echo SITE_ROOT.$objCommon->html2text($allLikePost['usl_fameuz'])?>"><img src="<?php echo SITE_ROOT?>uploads/profile_images/<?php echo $userImg?>" /></a>
						</div>
						<div class="name_users_liked">
							<p class="postmsgs likedPeoples">
								<a href="<?php echo SITE_ROOT.$objCommon->html2text($allLikePost['usl_fameuz'])?>"><?php echo $objCommon->displayName($allLikePost)?></a>
								<span class="proffesion"><?php echo $objCommon->html2text($allLikePost['c_name'])?></span>
							</p>
						</div>
						<div class="follow-btn pull-right">
							<?php
							if($allLikePost['user_id'] != $_SESSION['userId']){
								if($friendStatus==2){
								?>
								<a href="javascript:;" class="friend"><i class="fa fa-users"></i> Friend</a>
								<?php
								}else if($friendStatus==1){
								?>
								<a href="javascript:;" class="following"><i class="fa fa-check"></i> Following</a>
								<?php
								}else if($friendStatus==0){
								?>
								<a href="javascript:;" class="follow has-spinner" data-friendid="<?php echo $objCommon->html2text($allLikePost['user_id'])?>">
									<span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span>
									<i class="fa fa-plus"></i> Follow
								</a>
								<?php
								}
							}
							?>
						</div>
						<div class="clearfix"></div>
					</div>
				</li>
				<?php
			}
			?>
			</ul>
		</div>
			<?php
	}
	
}
?>
<script>
$('.follow').on("click",function(e) {
	var friendid		=	$(this).data('friendid');
	$(this).addClass('active');
	$(this).find('.fa-plus').hide();
	that	=	this;
	$.get('<?php echo SITE_ROOT?>ajax/sent_friend_request.php',{"friendId":friendid},function(data){
	  setTimeout(function () {
			$(that).removeClass('active');
			$(that).parent().html('<a href="javascript:;" class="following"><i class="fa fa-check"></i> Following</a>');
			
		},3000);
	});
});
</script>