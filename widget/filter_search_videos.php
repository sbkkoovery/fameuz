<form method="get" action="<?php echo SITE_ROOT?>search">
	<div class="row serach_filter">
		<div class="col-sm-12">
				<div class="form-group upload-help">
				<select class="form-control" name="filter_category" id="filter_category">
					<option value="">Select Category</option>
					<option value="new_videos" <?php echo ($_GET['filter_category']=='new_videos')?'selected':''?>>New videos</option>
					<option value="popular_videos" <?php echo ($_GET['filter_category']=='popular_videos')?'selected':''?>>Popular videos</option>
					<option value="top_videos" <?php echo ($_GET['filter_category']=='top_videos')?'selected':''?>>Top videos</option>
					<option value="last_visited" <?php echo ($_GET['filter_category']=='last_visited')?'selected':''?>>Last visited</option>
					<option value="all_videos" <?php echo ($_GET['filter_category']=='all_videos')?'selected':''?>>All videos</option>
				</select>
				</div>
			</div>
		<div class="col-sm-12">
				<div class="form-group">
				<input type="text" class="form-control checkme"  name="filter_keywords" id="filter_keywords" placeholder="Type Keywords" value="<?php echo $_GET['filter_keywords']?>">
				</div>
			</div>
		<div class="col-sm-12">
			  <input type="hidden" name="cat" value="video" />
			  <button type="submit" class="btn btn-default">Submit</button>
			  <a href="javascript:;" class="clear_all pull-right">Clear All</a>
			</div>
	</div>
</form>
<script type="application/javascript">
$(".clear_all").on("click",function(){
	$("#filter_category").val('');
	$("#filter_keywords").val('');
});
</script>