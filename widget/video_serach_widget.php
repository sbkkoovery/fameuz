<div class="row reduce-margin">
<div class="col-sm-6 col-md-6 col-lg-6">
<form action="<?php echo SITE_ROOT?>video/search" method="get">
<div class="video_search">
<div class="row">
	<div class="col-sm-7 col-md-7 col-lg-9 reduce-padding">
	<input type="text" placeholder="Search videos" name="search-keyword" value="<?php echo $search_keyword?>" autocomplete="off" />
    </div>
    <div class="col-sm-5 col-md-5 col-lg-3 reduce-padding">
	<input type="submit" value="Search videos" />
    </div>
    </div>
</div>
</form>
</div>
<div class="col-sm-2 col-md-2 col-lg-2 reduce-padding">
<div class="upload"><a href="<?php echo SITE_ROOT.'video/upload'?>">Upload Video</a></div>
</div>
<div class="col-sm-3 col-md-3 col-lg-3 alt-padding-l-s">
	<div class="option">
		<p><span>Select Category</span><i class="fa fa-bars hidden-lg"></i></p>
		<ul>
			<li><a href="<?php echo SITE_ROOT?>video/search?search-type=new-videos">New videos</a></li>
			<li><a href="<?php echo SITE_ROOT?>video/search?search-type=popular-videos">Popular videos</a></li>
			<li><a href="<?php echo SITE_ROOT?>video/search?search-type=top-videos">Top videos</a></li>
			<li><a href="<?php echo SITE_ROOT?>video/search?search-type=last-visited">Last Visited</a></li>
			<li><a href="<?php echo SITE_ROOT?>video/search?search-type=all-videos">All videos</a></li>
		</ul>
  </div>
</div>
<script type="text/javascript">
	$(".option").click(function(){
		$(this).find("ul").slideToggle();
	});
</script>