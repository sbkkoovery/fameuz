<?php
$getFriendVideos	   =	$objUsers->listQuery("select vid.video_id,vid.video_url,video_encr_id,vid.video_thumb,vid.video_type,vid.video_title,vid.video_privacy from videos as vid  where user_id=".$getMyFriendDetails['user_id']." and video_status = 1  order by video_created desc");
if(count($getFriendVideos)>0){
?>
<div class="photos_section myVidLoad">
	<div class="photos_box">
	<?php
		$keyVideos	=	0;
		foreach($getFriendVideos as $allVideos){
			if(($allVideos['video_privacy']=='0,0,1,0' || $allVideos['video_privacy']=='0,0,0,1') || ($allVideos['video_privacy']=='0,1,0,0' && $friendStatus ==0 && $getMyFriendDetails['user_id'] != $_SESSION['userId']) ){
				continue;
			}
			$keyVideos++;
			if($allVideos['video_type']==1){
				$getAiImages				=	SITE_ROOT.'uploads/videos/'.$allVideos['video_thumb'];
				$vidEncrId				  =	$allVideos['video_encr_id'];
			}else if($allVideos['video_type']==2){
				$getAiImages				=	$allVideos['video_thumb'];
				if (preg_match('%^https?://[^\s]+$%', $getAiImages)) {
					$getAiImages			=	$allVideos['video_thumb'];
				} else {
					$getAiImages			=	SITE_ROOT.'uploads/videos/'.$allVideos['video_thumb'];
				}
				$vidEncrId				  =	$allVideos['video_encr_id'];
			}
	?>
	<div class="photos">
		<div class="pic"><img src="<?php echo $getAiImages?>" alt="videos" />
        	<?php /*?><a href="javascript:;" onclick="photoPopUpVideo('<?php echo $vidEncrId?>');" class="playbtn"></a><?php */?>
            <a href="javascript:;"  data-vidEncrId="<?php echo $vidEncrId?>" class="playbtn lightBoxs"></a>
        </div>
	</div>
	<?php
			if($keyVideos==6){
				break;
			}
		}
		?>
		<div class="clr"></div>
		<div class="row">
			<div class="col-sm-12 text-right">
				<span class="seemore "><a href="<?php echo SITE_ROOT?>user/<?php echo $user_url?>/albums#videos">See More <i class="fa fa-play "></i></a></span>
			</div>
		</div>
	</div>
</div>	
<?php
}else{
?>
<p>No videos found...</p>
<?php
}
?>
