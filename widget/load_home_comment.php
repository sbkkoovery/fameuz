<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/videos.php");
$objCommon				   =	new common();
$objVideos				   =	new videos();
$commentContent			  =	$objCommon->esc($_GET['commentContent']);
$commentCat				  =	$objCommon->esc($_GET['commentCat']);
$noti_whome				  =	$objCommon->esc($_GET['noti_whome']);
$likeCatId				   =	'';
if($commentCat==1 || $commentCat == 2){
	$likeCatId			   =	3;
}else if($commentCat==4){
	$likeCatId			   =	5;
}else if($commentCat==9){
	$likeCatId			   =	12;
}else if($commentCat==10){
	$likeCatId			   =	13;
}
$getUserDetails	  		  =	$objVideos->getRowSql("SELECT profileImg.upi_img_url 
										FROM users AS user 
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
										WHERE user.status=1 AND user.user_id=".$_SESSION['userId']);
$page						=	($objCommon->esc($_GET['page']))?$objCommon->esc($_GET['page']):1;
$pageCount				   =	3;
$pageLimit				   =	$page*$pageCount;
$sqlComments				 =	"SELECT comments.*,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz ,group_concat(likes.like_user_id) AS likeCount
									FROM comments
									LEFT JOIN users AS user ON comments.comment_user_by =	user.user_id
									LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
									LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
									LEFT JOIN likes ON comments.comment_id =likes.like_content and likes.like_status =1  and likes.like_cat =".$likeCatId."
									WHERE comments.comment_content = '".$commentContent."' AND comments.comment_cat = ".$commentCat." AND comments.comment_status = 1 AND  user.status=1 AND user.email_validation=1 AND  					comments.comment_reply_id = 0 GROUP BY comments.comment_id  ORDER BY comments.comment_time DESC";
$queryComments			   =	$sqlComments." limit 0,".$pageLimit;	
$getComments				 =	$objVideos->listQuery($queryComments);
$countSql				    =	$objVideos->countRows($sqlComments);
?>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.form.js"></script>
<script src="<?php echo SITE_ROOT?>js/jquery.elastic.source.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.css" />
<script src="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.js"></script>
<?php
if(count($getComments) >0){
	foreach($getComments as $allComments){
		$displayName			=	$objCommon->displayName($allComments);
		$commentTime			=	$objCommon->time_elapsed_string(strtotime($objCommon->local_time($allComments['comment_time'])));
		if($allComments['likeCount']){
			$likeCountArr	   =	explode(",",$allComments['likeCount']);
			if(in_array($_SESSION['userId'],$likeCountArr)){
				$youLikeCommentMain	  =	1;
			}else{
				$youLikeCommentMain	  =	0;
			}
		}else{
			unset($likeCountArr);
			$likeCountArr = array();
			$youLikeCommentMain	 	  =	0;	
		}
				
?>
<div id="preview2"></div>
<div class="comment_box">
	<div class="comment_box_inner">
		<div class="thumb"><a href="<?php echo SITE_ROOT.$objCommon->html2text($allComments['usl_fameuz'])?>"><img src="<?php echo SITE_ROOT?>uploads/profile_images/<?php echo($allComments['upi_img_url'])?$allComments['upi_img_url']:'profile_pic.jpg'?>" alt="thumb"></a></div>
		<div class="comment_descr">
			<h3><a href="<?php echo SITE_ROOT.$objCommon->html2text($allComments['usl_fameuz'])?>"><?php echo $displayName?></a></h3>
			<p><?php echo $objCommon->html2text($allComments['comment_descr'])?></p>
			<p class="bottomTime">
				<i class="fa fa-clock-o"></i> <span><?php echo $commentTime;?></span>
				<span class="seperator">-</span>
				<font class="commentLikeBoxMain">
					<a href="javascript:;" class="commentLikeBtn <?php echo ($youLikeCommentMain==1)?'liked':'like'?>" data-like="<?php echo $allComments['comment_id']?>" data-userto="<?php echo $allComments['user_id']?>"><i class="fa fa-heart"></i> <span><?php echo ($youLikeCommentMain==1)?'Liked':'Like'?></span></a>
					<span class="seperator">-</span>
					<i class="fa fa-thumbs-up"></i> <span><?php echo count($likeCountArr);?></span>
				</font>
				<span class="seperator">-</span>
				<span><a href="javascript:;" class="openReply">Reply</a></span>
			</p>
			<div class="clr"></div>
		</div>
		<div class="reply_comment">
			<div class="working_comments showReplyComments">
	<div class="thumb"><img src="<?php echo SITE_ROOT?>uploads/profile_images/<?php echo($getUserDetails['upi_img_url'])?$getUserDetails['upi_img_url']:'profile_pic.jpg'?>" alt="thumb"></div>
	<div class="comments">
		<div class="comments_arrow"><img src="<?php echo SITE_ROOT?>images/comments_arrow1.png" alt="comments_arrow"></div>
		<form id="comment_form" method="post" action="<?php echo SITE_ROOT?>access/post_home_comment.php?action=add_comment&commentContent=<?php echo $commentContent?>&commentCat=<?php echo $commentCat?>&noti_whome=<?php echo $noti_whome?>&replyId=<?php echo $allComments['comment_id']?>">
			<textarea placeholder="Your comment...." name="comment_descr_input" class="comment_descr_input2" rows="1"></textarea>
			<div class="ajaxloader text-center"><img src="<?php echo SITE_ROOT?>images/ajax-loader.gif" alt="comments_arrow"></div> 
		</form>
		<div class="clr"></div>
	</div>
</div>
			<?php
			$getReplyComments		=	$objVideos->listQuery("SELECT comments.*,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,group_concat(likes.like_user_id) AS likeCountReply 
									FROM comments
									LEFT JOIN users AS user ON comments.comment_user_by =	user.user_id
									LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
									LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
									LEFT JOIN likes ON comments.comment_id =likes.like_content and likes.like_status =1  and likes.like_cat =5
									WHERE comments.comment_content = '".$commentContent."' AND comments.comment_cat = ".$commentCat." AND comments.comment_status = 1 AND  user.status=1 AND user.email_validation=1 AND  					comments.comment_reply_id = ".$allComments['comment_id'] ." GROUP BY comments.comment_id ORDER BY comments.comment_time ASC");
			if(count($getReplyComments) >0){
				foreach($getReplyComments as $allReplyComments){
					$displayNameRply			=	$objCommon->displayName($allReplyComments);
					$commentTimerply			=	$objCommon->time_elapsed_string(strtotime($objCommon->local_time($allReplyComments['comment_time'])));
					if($allReplyComments['likeCountReply']){
						$likeCounReplytArr	  =	explode(",",$allReplyComments['likeCountReply']);
						if(in_array($_SESSION['userId'],$likeCounReplytArr)){
							$youLikeCommentReply	  =	1;
						}else{
							$youLikeCommentReply	  =	0;
						}
					}else{
						unset($likeCounReplytArr);
						$likeCounReplytArr 			= 	array();
						$youLikeCommentReply	 	  =	0;	
					}
			?>
			<div class="comment_box_inner">
				<div class="thumb"><a href="<?php echo SITE_ROOT.$objCommon->html2text($allReplyComments['usl_fameuz'])?>"><img src="<?php echo SITE_ROOT?>uploads/profile_images/<?php echo($allReplyComments['upi_img_url'])?$allReplyComments['upi_img_url']:'profile_pic.jpg'?>" alt="thumb"></a></div>
				<div class="comment_descr">
					<h3><a href="<?php echo SITE_ROOT.$objCommon->html2text($allReplyComments['usl_fameuz'])?>"><?php echo $displayNameRply?></a></h3>
					<p><?php echo $objCommon->html2text($allReplyComments['comment_descr'])?></p>
					<p class="bottomTime">
						<i class="fa fa-clock-o"></i> <span><?php echo $commentTimerply?></span>
						<span class="seperator">-</span>
						<font class="commentLikeBoxReply">
							<a href="javascript:;" class="commentLikeBtn <?php echo ($youLikeCommentReply==1)?'liked':'like'?>" data-like="<?php echo $allReplyComments['comment_id']?>" data-userto="<?php echo $allReplyComments['user_id']?>"><i class="fa fa-heart"></i> <span><?php echo ($youLikeCommentReply==1)?'Liked':'Like'?></span></a>
							<span class="seperator">-</span>
							<i class="fa fa-thumbs-up"></i> <span><?php echo count($likeCounReplytArr);?></span>
						</font>
					</p>
					<div class="clr"></div>
				</div>
				<?php
				if($allReplyComments['comment_user_by'] == $_SESSION['userId']){
				?>
				<div class="comment_action">
					<img src="<?php echo SITE_ROOT?>images/review_top_arrow.png">
					<div class="comment_action_drop">
					<div class="arrow"><img alt="status_option_arrow" src="<?php echo SITE_ROOT?>images/status_option_arrow.png"></div>
						<a class="delete_comment" data-comment-id="<?php echo $allReplyComments['comment_id']?>" href="javascript:;"><i class="fa fa-trash-o"></i> Delete</a>
					</div>
				</div>
				<?php
				}
				?>
			</div>
			<?php
				}
			}
			?>
		</div>
		<?php
		if($allComments['comment_user_by'] == $_SESSION['userId']){
		?>
		<div class="comment_action">
			<img src="<?php echo SITE_ROOT?>images/review_top_arrow.png">
			<div class="comment_action_drop">
			<div class="arrow"><img alt="status_option_arrow" src="<?php echo SITE_ROOT?>images/status_option_arrow.png"></div>
				<a class="delete_comment" data-comment-id="<?php echo $allComments['comment_id']?>" href="javascript:;"><i class="fa fa-trash-o"></i> Delete</a>
			</div>
		</div>
		<?php
		}
		?>
     <div class="clear-fix"></div>
	</div>
  
</div>
<?php
	}
}
if($pageLimit < $countSql){
?>
<div class="row"><div class="col-md-12 text-center"><a href="javascript:;" class="see_more_videos seeCommentsVid">See more comments</a></div></div><div class="clr"></div>
<?php
}
?>
<script language="javascript" type="text/javascript">
	 $('.comment_descr_input2').elastic();
	 $('.comment_descr_input2').keydown(function(event){			
		  if (event.keyCode == 13 && !event.shiftKey) {
			$(this).closest("form").find('.ajaxloader').show();
			$(this).closest("form").ajaxForm(
				{
					target: '#preview2',
					success:successCommentCall
				}).submit();
			return false;
		  }
	});
function successCommentCall(){
	$('.ajaxloader').hide();
	$(".comment_descr_input1").val('');
	$(".comment_descr_input2").val('');
	$(".loadComment_<?php echo $commentContent?>_<?php echo $commentCat?>").load('<?php echo SITE_ROOT?>widget/load_home_comment.php?commentContent=<?php echo $commentContent?>&commentCat=<?php echo $commentCat?>&noti_whome=<?php echo $noti_whome ?>');
	$(".commentCountDisplay_<?php echo $commentContent?>_<?php echo $commentCat?>").load('<?php echo SITE_ROOT?>ajax/comment_count_home.php?commentContent=<?php echo $commentContent?>&commentCat=<?php echo $commentCat?>');
}
$(".commentLikeBoxMain").on("click",".commentLikeBtn",function(){
	var likeId		=	$(this).data('like');
	var likecat	   =	'<?php echo $likeCatId?>';
	var commentCat	=	'<?php echo $commentCat?>';
	var commentContent=	'<?php echo $commentContent?>';
	var imgUserId	 =	$(this).data('userto');
	var that		  =	this;
	if(likeId !='' && likecat != ''){
		$.ajax({
			url:"<?php echo SITE_ROOT?>ajax/like_home_comments.php",
			method: "POST",
			data:{likeId:likeId,likeCat:likecat,noti_whome:imgUserId,commentCat:commentCat,commentContent:commentContent},
			success:function(result){
				$(that).parent().html(result);
		}});
	}
});
$(".commentLikeBoxReply").on("click",".commentLikeBtn",function(){
	var likeId		=	$(this).data('like');
	var likecat	   =	'<?php echo $likeCatId?>';
	var commentCat	=	'<?php echo $commentCat?>';
	var commentContent=	'<?php echo $commentContent?>';
	var imgUserId	 =	$(this).data('userto');
	var that		  =	this;
	if(likeId !='' && likecat != ''){
		$.ajax({
			url:"<?php echo SITE_ROOT?>ajax/like_home_comments.php",
			method: "POST",
			data:{likeId:likeId,likeCat:likecat,noti_whome:imgUserId,commentCat:commentCat,commentContent:commentContent},
			success:function(result){
				$(that).parent().html(result);
		}});
	}
});
$(".openReply").on("click",function(){
	var _this	=	$(this);
	$(".showReplyComments").not(_this).hide();
	$(this).parent().parent().parent().parent().children().children('.showReplyComments').toggle();
});
$(".seeCommentsVid").on("click",function(){
	$(".loadComment_<?php echo $commentContent?>_<?php echo $commentCat?>").load('<?php echo SITE_ROOT?>widget/load_home_comment.php?commentContent=<?php echo $commentContent?>&commentCat=<?php echo $commentCat?>&noti_whome=<?php echo $noti_whome ?>&page=<?php echo ($page+1)?>');
});
$(".comment_action").on("click","img",function(){
	 	var $el = $(this).next('.comment_action_drop');
    	$('.comment_action_drop').not($el).fadeOut();
		$el.fadeToggle(300);
});
$(".comment_action_drop").on("click",".delete_comment",function(){
	var delid			=	$(this).data("comment-id");
	var that			 =	this;
	var elem = $(this).closest('.item');
	$.confirm({
		'title'	  : 'Delete Confirmation',
		'message'	: 'You are about to delete this item ?',
		'buttons'	: {
		'Yes'	: {
		'class'	: 'blue',
		'action': function(){
			elem.slideUp();
			if(delid){
				$.ajax({
					url:"<?php echo SITE_ROOT?>access/delete_comment.php",
					method: "POST",
					data:{delid:delid},
					success:function(result){
						$(".loadComment_<?php echo $commentContent?>_<?php echo $commentCat?>").load('<?php echo SITE_ROOT?>widget/load_home_comment.php?commentContent=<?php echo $commentContent?>&commentCat=<?php echo $commentCat?>&noti_whome=<?php echo $noti_whome ?>');
						$(".commentCountDisplay_<?php echo $commentContent?>_<?php echo $commentCat?>").load('<?php echo SITE_ROOT?>ajax/comment_count_home.php?commentContent=<?php echo $commentContent?>&commentCat=<?php echo $commentCat?>');
				}});
			}
		}
		},
		'No'	: {
		'class'	: 'gray',
		'action': function(){
			$(".comment_action_drop").slideUp();
		}	// Nothing to do in this case. You can as well omit the action property.
		}
		}
	});
});
</script>