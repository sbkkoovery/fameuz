<?php
@session_start();
if(!empty($_GET["page"])) {
	include_once("../includes/site_root.php");
}
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/users.php");
$objCommon				     =	new common();
$objUsers					  =	new users();
$userId						=	$_SESSION['userId'];						  
//------------------------------------------------------------------------------------------------
$getMyFollowing		    =	$objUsers->getRowSql("select group_concat(user.user_id) as user_ids,profileImg.upi_img_url  from following as follow 
left join users as user on (follow.follow_user1 = user.user_id and follow.follow_user1 !='".$userId."') or (follow.follow_user2 = user.user_id and follow.follow_user2 !='".$userId."')
LEFT JOIN user_profile_image as profileImg ON profileImg.user_id =".$userId."  AND profileImg.upi_status=1
where ((follow.follow_user1='".$userId."') or (follow.follow_user2='".$userId."' and follow.follow_status='2'))");
$user_ids				  =	$userId;
$getPostsSql1			  =	"SELECT 'none' AS share_images,'none' AS share_images_id,album.a_name AS post_name,'none' AS share_video,'none' AS share_video_alias, album.a_id AS id,'create_album' AS post_category,album.user_id AS post_by,'none' AS post_to,'none' AS post_type,album.a_created AS post_created,group_concat(ai.ai_images ORDER BY ai.ai_created DESC) AS post_images,group_concat(ai.ai_id ORDER BY ai.ai_created DESC) AS post_images_id,'none' AS post_alias,user.user_id AS user_id_by,user.first_name AS first_name_by,user.last_name AS last_name_by,user.display_name AS display_name_by,user.email AS email_by,profileImg.upi_img_url AS upi_img_url_by,social.usl_fameuz AS usl_fameuz_by,'none' AS user_id_to,
'none' AS first_name_to,'none' AS last_name_to,'none' AS display_name_to,'none' AS email_to,'none' AS upi_img_url_to,'none' AS usl_fameuz_to,cat.c_name AS model_category,likes.likedUser,comments.commentCount,review.reviewAvg,share.sharedUser,album.a_id AS contentId
	FROM album 
	LEFT JOIN album_images AS ai ON album.a_id = ai.a_id AND ai.ai_id != ''
	LEFT JOIN users AS user ON  album.user_id = user.user_id
	LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
	LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
	LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
	LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
	LEFT JOIN (SELECT group_concat(DISTINCT like_user_id) AS likedUser,like_content FROM likes WHERE like_cat = 9 AND like_status = 1 GROUP BY  like_content) AS likes ON album.a_id = likes.like_content
	LEFT JOIN (SELECT group_concat(DISTINCT user_by) AS sharedUser,share_content FROM share WHERE share_category = 9 AND user_by=".$user_ids." GROUP BY  share_content) AS share ON album.a_id = share.share_content
	LEFT JOIN (SELECT COUNT(comment_id) AS commentCount,comment_content  FROM comments WHERE comment_cat=9 GROUP BY comment_content) AS comments ON album.a_id = comments.comment_content
	LEFT JOIN (SELECT AVG(review_rate) AS reviewAvg,user_id_to FROM user_reviews GROUP BY user_id_to ) AS review ON user.user_id = review.user_id_to
	WHERE album.user_id IN (".$user_ids.")  AND user.status=1 AND user.email_validation=1 AND ai.ai_images != ''
	GROUP BY ai.a_id,likes.like_content";
	
$getPostsSql2			  =	"SELECT 'none' AS share_images,'none' AS share_images_id,p.post_descr AS post_name,'none' AS share_video,'none' AS share_video_alias, p.post_id AS id,'create_posts' AS post_category,'none' AS post_type,'none' AS post_to,p.user_id AS post_by,p.post_created AS post_created,group_concat(photos.photo_url) AS post_images,'none' AS post_images_id,'none' AS post_alias,user.user_id AS user_id_by,user.first_name AS first_name_by,user.last_name AS last_name_by,user.display_name AS display_name_by,user.email AS email_by,profileImg.upi_img_url AS upi_img_url_by,social.usl_fameuz AS usl_fameuz_by,'none' AS user_id_to,
'none' AS first_name_to,'none' AS last_name_to,'none' AS display_name_to,'none' AS email_to,'none' AS upi_img_url_to,'none' AS usl_fameuz_to,cat.c_name AS model_category,likes.likedUser,comments.commentCount,review.reviewAvg,share.sharedUser,p.post_id AS contentId
	FROM posts AS p 
	LEFT JOIN user_photos AS photos ON p.post_id = photos.post_id 
	LEFT JOIN users AS user ON  p.user_id = user.user_id
	LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
	LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id
	LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
	LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
	LEFT JOIN (SELECT group_concat(DISTINCT like_user_id) AS likedUser,like_content FROM likes WHERE like_cat = 10 AND like_status = 1 GROUP BY  like_content) AS likes ON p.post_id = likes.like_content
	LEFT JOIN (SELECT group_concat(DISTINCT user_by) AS sharedUser,share_content FROM share WHERE share_category = 9 GROUP BY  share_content) AS share ON p.post_id = share.share_content
	LEFT JOIN (SELECT COUNT(comment_id) AS commentCount,comment_content  FROM comments WHERE comment_cat=10 GROUP BY comment_content) AS comments ON p.post_id = comments.comment_content
	LEFT JOIN (SELECT AVG(review_rate) AS reviewAvg,user_id_to FROM user_reviews GROUP BY user_id_to ) AS review ON user.user_id = review.user_id_to
	WHERE p.user_id IN (".$user_ids.") AND p.post_status=1  AND user.status=1 AND user.email_validation=1
	GROUP BY photos.post_id";
	
$getPostsSql3			  =	"SELECT 'none' AS share_images,'none' AS share_images_id,ur.review_msg AS post_name,'none' AS share_video,'none' AS share_video_alias, ur.review_id AS id,'create_review' AS post_category,'none' AS post_type,ur.user_id_to AS post_to,ur.user_id_by AS post_by,ur.review_date AS post_created,'none' AS post_images,'none' AS post_images_id,'none' AS post_alias,user_to.user_id AS user_id_to,user_to.first_name AS first_name_by,user_to.last_name AS last_name_by,user_to.display_name AS display_name_by,user_to.email AS email_by,profileImg.upi_img_url AS upi_img_url_by,social.usl_fameuz AS usl_fameuz_by,user_by.user_id AS user_id_by,
user_by.first_name AS first_name_to,user_by.last_name AS last_name_to,user_by.display_name AS display_name_to,user_by.email AS email_to,profileImg_by.upi_img_url AS upi_img_url_to,social_by.usl_fameuz AS usl_fameuz_to,cat.c_name AS model_category,likes.likedUser,'none' AS commentCount,review.reviewAvg,share.sharedUser,ur.review_id AS contentId
	FROM user_reviews AS ur 
	LEFT JOIN users AS user_to ON  ur.user_id_to = user_to.user_id
	LEFT JOIN user_social_links AS social ON user_to.user_id=social.user_id
	LEFT JOIN user_profile_image as profileImg ON user_to.user_id=profileImg.user_id AND profileImg.upi_status=1
	LEFT JOIN users AS user_by ON  ur.user_id_by = user_by.user_id
	LEFT JOIN user_categories AS ucat ON user_by.user_id=ucat.user_id
	LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id
	LEFT JOIN user_social_links AS social_by ON user_by.user_id=social_by.user_id
	LEFT JOIN user_profile_image as profileImg_by ON user_by.user_id=profileImg_by.user_id AND profileImg_by.upi_status=1
	LEFT JOIN (SELECT group_concat(DISTINCT like_user_id) AS likedUser,like_content FROM likes WHERE like_cat = 11 AND like_status = 1 GROUP BY  like_content) AS likes ON ur.review_id = likes.like_content
	LEFT JOIN (SELECT group_concat(DISTINCT user_by) AS sharedUser,share_content FROM share WHERE share_category = 9 GROUP BY  share_content) AS share ON ur.review_id = share.share_content
	LEFT JOIN (SELECT AVG(review_rate) AS reviewAvg,user_id_to FROM user_reviews GROUP BY user_id_to ) AS review ON user_by.user_id = review.user_id_to
	WHERE ur.review_user_to_status =1 AND  user_by.user_id IN (".$user_ids.")  AND user_to.status=1 AND user_to.email_validation=1 AND user_by.status=1 AND user_by.email_validation=1 ";	
	
$getPostsSql4			  =	"SELECT 'none' AS share_images,'none' AS share_images_id,photos.photo_descr AS post_name,'none' AS share_video,'none' AS share_video_alias,photos.photo_id AS id,'create_wall' AS post_category,'none' AS post_type,'none' AS post_to,photos.user_id AS post_by,photos.photo_created AS post_created,photos.photo_url AS post_images,'none' AS post_images_id,'none' AS post_alias,user.user_id AS user_id_by,user.first_name AS first_name_by,user.last_name AS last_name_by,user.display_name AS display_name_by,user.email AS email_by,profileImg.upi_img_url AS upi_img_url_by,social.usl_fameuz AS usl_fameuz_by,'none' AS user_id_to,
'none' AS first_name_to,'none' AS last_name_to,'none' AS display_name_to,'none' AS email_to,'none' AS upi_img_url_to,'none' AS usl_fameuz_to,cat.c_name AS model_category,likes.likedUser,comments.commentCount,review.reviewAvg,share.sharedUser,photos.photo_id AS contentId
	FROM user_photos AS photos 
	LEFT JOIN users AS user ON  photos.user_id = user.user_id
	LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
	LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id
	LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
	LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
	LEFT JOIN (SELECT group_concat(DISTINCT like_user_id) AS likedUser,like_content FROM likes WHERE like_cat = 1 AND like_status = 1 GROUP BY  like_content) AS likes ON photos.photo_id = likes.like_content
	LEFT JOIN (SELECT group_concat(DISTINCT user_by) AS sharedUser,share_content FROM share WHERE share_category = 9 GROUP BY  share_content) AS share ON photos.photo_id = share.share_content
	LEFT JOIN (SELECT COUNT(comment_id) AS commentCount,comment_content  FROM comments WHERE comment_cat=1 GROUP BY comment_content) AS comments ON photos.photo_id  = comments.comment_content
	LEFT JOIN (SELECT AVG(review_rate) AS reviewAvg,user_id_to FROM user_reviews GROUP BY user_id_to ) AS review ON user.user_id = review.user_id_to
	WHERE photos.post_id =0 AND  photos.user_id IN (".$user_ids.") AND user.status=1 AND user.email_validation=1";
	
$getPostsSql5			  =	"SELECT 'none' AS share_images,'none' AS share_images_id,video.video_title AS post_name,'none' AS share_video,'none' AS share_video_alias,video.video_id AS id,'create_video' AS post_category,video.video_type AS post_type,'none' AS post_to,video.user_id AS post_by,video.video_created AS post_created,video.video_thumb AS post_images,'none' AS post_images_id,video.video_encr_id AS post_alias,user.user_id AS user_id_by,user.first_name AS first_name_by,user.last_name AS last_name_by,user.display_name AS display_name_by,user.email AS email_by,profileImg.upi_img_url AS upi_img_url_by,social.usl_fameuz AS usl_fameuz_by,'none' AS user_id_to,'none' AS first_name_to,'none' AS last_name_to,'none' AS display_name_to,'none' AS email_to,'none' AS upi_img_url_to,'none' AS usl_fameuz_to,cat.c_name AS model_category,likes.likedUser,comments.commentCount,review.reviewAvg,share.sharedUser,video.video_id AS contentId
	FROM videos AS video 
	LEFT JOIN users AS user ON  video.user_id = user.user_id
	LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
	LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id
	LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
	LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
	LEFT JOIN (SELECT group_concat(DISTINCT like_user_id) AS likedUser,like_content FROM likes WHERE like_cat = 4 AND like_status = 1 GROUP BY  like_content) AS likes ON video.video_id = likes.like_content
	LEFT JOIN (SELECT group_concat(DISTINCT user_by) AS sharedUser,share_content FROM share WHERE share_category = 3 GROUP BY  share_content) AS share ON video.video_id = share.share_content
	LEFT JOIN (SELECT COUNT(comment_id) AS commentCount,comment_content  FROM comments WHERE comment_cat=4 GROUP BY comment_content) AS comments ON video.video_id  = comments.comment_content
	LEFT JOIN (SELECT AVG(review_rate) AS reviewAvg,user_id_to FROM user_reviews GROUP BY user_id_to ) AS review ON user.user_id = review.user_id_to
	WHERE video.video_status = 1 AND  video.user_id IN (".$user_ids.") AND user.status=1 AND user.email_validation=1";
	
$getPostsSql6			 = "SELECT 
CASE
WHEN tab.post_type IN(1)
	THEN
		(SELECT photo_url FROM user_photos WHERE photo_id=tab.contentId )
WHEN tab.post_type IN(2)
	THEN
		(SELECT group_concat(ai_images ORDER BY ai_created DESC) FROM album_images WHERE ai_id=tab.contentId )
WHEN tab.post_type IN(3)
	THEN
		(SELECT video_thumb FROM videos WHERE video_id=tab.contentId )
WHEN tab.post_type IN(6)
	THEN
		(SELECT upi_img_url FROM user_profile_image WHERE upi_id=tab.contentId )
WHEN tab.post_type IN(7)
	THEN
		(SELECT blog_img FROM blogs WHERE blog_id=tab.contentId )
WHEN tab.post_type IN(9)
	THEN
		(SELECT group_concat(ai_images ORDER BY ai_created DESC) FROM album_images WHERE a_id=tab.contentId )
WHEN tab.post_type IN(10)
	THEN
		(SELECT photos.photo_url  FROM posts LEFT JOIN user_photos AS photos ON posts.post_id = photos.post_id WHERE posts.post_id=tab.contentId limit 1 )
WHEN tab.post_type IN(11)
	THEN
		(SELECT '' AS reviewImg  FROM user_reviews  WHERE review_id = tab.contentId )
WHEN tab.post_type IN(13)
	THEN
		(SELECT mp_image FROM market_place WHERE mp_id=tab.contentId )
WHEN tab.post_type IN(14)
	THEN
		(SELECT music_thumb FROM music WHERE music_id=tab.contentId )
ElSE 'none'
END
AS share_images,
CASE
WHEN tab.post_type IN(2)
	THEN
		(SELECT group_concat(ai_id ORDER BY ai_created DESC) FROM album_images WHERE ai_id=tab.contentId )
WHEN tab.post_type IN(9)
	THEN
		(SELECT group_concat(ai_id ORDER BY ai_created DESC) FROM album_images WHERE a_id=tab.contentId )
ElSE 'none'
END
AS share_images_id,
CASE
WHEN tab.post_type IN(1)
	THEN
		(SELECT photo_descr FROM user_photos WHERE photo_id=tab.contentId )
WHEN tab.post_type IN(2)
	THEN
		(SELECT ai_caption FROM album_images WHERE a_id=tab.contentId )
WHEN tab.post_type IN(3)
	THEN
		(SELECT video_title FROM videos WHERE video_id=tab.contentId )
WHEN tab.post_type IN(6)
	THEN
		(SELECT '' AS profile_title FROM user_profile_image WHERE upi_id=tab.contentId )
WHEN tab.post_type IN(7)
	THEN
		(SELECT blog_title AS profile_title FROM blogs WHERE blog_id=tab.contentId )
WHEN tab.post_type IN(9)
	THEN
	(SELECT a_name FROM  album WHERE a_id=tab.contentId )
WHEN tab.post_type IN(10)
	THEN
		(SELECT post_descr  FROM posts WHERE posts.post_id=tab.contentId )
WHEN tab.post_type IN(11)
	THEN
		(SELECT review_msg AS reviewMsg  FROM user_reviews  WHERE review_id = tab.contentId)
WHEN tab.post_type IN(12)
	THEN
	(SELECT g_name  FROM games  WHERE g_id = tab.contentId)
WHEN tab.post_type IN(13)
	THEN
	(SELECT mp_title  FROM market_place  WHERE mp_id = tab.contentId)
WHEN tab.post_type IN(14)
	THEN
	(SELECT music_title  FROM music  WHERE music_id = tab.contentId)
ElSE 'none'
END
AS post_name,
CASE
WHEN tab.post_type IN(3)
	THEN
		(SELECT video_url FROM videos WHERE video_id=tab.contentId )
ElSE 'none'
END
AS share_video,
CASE
WHEN tab.post_type IN(3)
	THEN
		(SELECT video_encr_id FROM videos WHERE video_id=tab.contentId )
WHEN tab.post_type IN(12)
	THEN
		(SELECT g_alias FROM games WHERE g_id=tab.contentId )
WHEN tab.post_type IN(7)
	THEN
		(SELECT blog_alias FROM blogs WHERE blog_id=tab.contentId )
WHEN tab.post_type IN(13)
	THEN
		(SELECT mp_alias FROM market_place WHERE mp_id=tab.contentId )
ElSE 'none'
END
AS share_video_alias,tab.*

FROM (
	SELECT share.share_id AS id,
	'shared' AS post_category,
	share.user_whose AS post_to,
	share.user_by AS post_by,
	share.share_category AS post_type,
	share.share_created AS post_created,
	'none' AS post_images,
	'none' AS post_images_id,
	'none' AS post_alias,
	user_to.user_id AS user_id_to,
	user_to.first_name AS first_name_by,
	user_to.last_name AS last_name_by,
	user_to.display_name AS display_name_by,
	user_to.email AS email_by,
	profileImg.upi_img_url AS upi_img_url_by,
	social.usl_fameuz AS usl_fameuz_by,
	user_by.user_id AS user_id_by,
	user_by.first_name AS first_name_to,
	user_by.last_name AS last_name_to,
	user_by.display_name AS display_name_to,
	user_by.email AS email_to,
	profileImg_by.upi_img_url AS upi_img_url_to,
	social_by.usl_fameuz AS usl_fameuz_to,
	cat.c_name AS model_category,
	'none' AS likedUser,
	'none' AS commentCount,
	review.reviewAvg,
	'none' AS sharedUser,
	share_content  AS contentId
	FROM share 
	LEFT JOIN users AS user_to ON share.user_by = user_to.user_id 
	LEFT JOIN user_social_links AS social ON user_to.user_id=social.user_id 
	LEFT JOIN user_profile_image as profileImg ON user_to.user_id=profileImg.user_id AND profileImg.upi_status=1 
	LEFT JOIN users AS user_by ON share.user_whose = user_by.user_id 
	LEFT JOIN user_categories AS ucat ON user_by.user_id=ucat.user_id 
	LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
	LEFT JOIN user_social_links AS social_by ON user_by.user_id=social_by.user_id 
	LEFT JOIN user_profile_image as profileImg_by ON user_by.user_id=profileImg_by.user_id AND profileImg_by.upi_status=1 
	LEFT JOIN (SELECT AVG(review_rate) AS reviewAvg,user_id_to 
	FROM user_reviews GROUP BY user_id_to ) AS review ON user_to.user_id = review.user_id_to 
	WHERE share.share_status=1 AND user_by.user_id IN (".$user_ids.") AND user_to.status=1 AND user_to.email_validation=1 AND user_by.status=1 AND user_by.email_validation=1
) AS tab";
$sqlNewsFeed		=	"SELECT tab1.* FROM ((".$getPostsSql1.") union all (".$getPostsSql2.") union all(".$getPostsSql3.") union all (".$getPostsSql4.") union all (".$getPostsSql5.") union all (".$getPostsSql6.")) AS tab1 ORDER BY tab1.post_created DESC ";
$perPage 				   = 	5;
$page 					  = 	1;
if(!empty($_GET["page"])) {
	$page 				  =	$_GET["page"];
}
$start 					 =	($page-1)*$perPage;
if($start < 0) { $start = 0; }
$getPostsSql 			   = 	$sqlNewsFeed . " limit " . $start . "," . $perPage; 
if(empty($_GET["rowcount"])) {
	$_GET["rowcount"] 	  =	$countnewsFeeds	=	$objUsers->countRows($sqlNewsFeed);
}
$pages  					 =	ceil($_GET["rowcount"]/$perPage);

$getPosts				  =	$objUsers->listQuery($getPostsSql);
		
//-------------------------------------------------------------------------------------------------
?>
        <div class="row">
			<?php
			if(count($getPosts) >0){
				echo '<input type="hidden" class="pagenum" value="' . $page . '" /><input type="hidden" class="total-page" value="'.$pages.'" /><input type="hidden" id="total-count" value="'.$countnewsFeeds.'" />';
				foreach($getPosts as $allPosts){
						$post_category				=	$allPosts['post_category'];
						$postImageStr				 =	($allPosts['post_images'])?$allPosts['post_images']:'';
						$postImageIdStr			   =	($allPosts['post_images_id'])?$allPosts['post_images_id']:'';
						$postImage					=	'';
						$postMsg					  =	'';
						$postUserBy				   =	'';
						$postUserTo				   =	'';
						$postMedia					=	'';
						$countLike					=	0;
						$youLike					  =	'';
						$youShare					 =	'';
						if($postImageStr != 'none' && $postImageStr !='' && $postImageStr !=NULL){
							$postImageArr			 =	explode(",",$postImageStr);					
						}
						if($postImageIdStr != 'none' && $postImageIdStr !='' && $postImageIdStr !=NULL){
							$postImageIdArr		   =	explode(",",$postImageIdStr);
						}
						$postDescr					=	$objCommon->limitWords($allPosts['post_name'],300);
						if($allPosts['email_by'] != 'none' && $allPosts['email_by'] != '' ){
							$postUserBy			   =	$objCommon->displayNameFields($allPosts,array("display_name"=>'display_name_by',"first_name"=>'first_name_by',"email"=>'email_by'));
						}
						if($allPosts['email_to'] != 'none' && $allPosts['email_to'] != '' ){
							$postUserTo			   =	$objCommon->displayNameFields($allPosts,array("display_name"=>'display_name_to',"first_name"=>'first_name_to',"email"=>'email_to'));
						}
						if($allPosts['likedUser'] != 'none' && $allPosts['likedUser'] !='' && $allPosts['likedUser'] !=NULL){
							$explLikedUser			=	explode(",",$allPosts['likedUser']);
							$explLikedUser			=	array_filter($explLikedUser);
							$countLike				=	count($explLikedUser);
							if($countLike > 0){
								if(in_array($userId,$explLikedUser)){
									$youLike		  =	1;
								}
							}
						}
						if($allPosts['sharedUser'] != 'none' && $allPosts['sharedUser'] !='' && $allPosts['sharedUser'] !=NULL){
							$explSharedUser			=	explode(",",$allPosts['sharedUser']);
							$explSharedUser			=	array_filter($explSharedUser);
							$countShare				=	count($explSharedUser);
							if($countShare > 0){
								if(in_array($userId,$explSharedUser)){
									$youShare		  =	1;
								}
							}
						}
						if($post_category == 'create_posts'){
							$postMsg				  =	'<a href="'.SITE_ROOT.$allPosts['usl_fameuz_by'].'">'.$postUserBy.' </a><span class="postmsg"> posted on his timeline</span>';
							$likeCatId			    =	10;
							$shareCatId			   =	10;
							if(count($postImageArr)>0){
								$postImage			=	SITE_ROOT.'uploads/albums/'.$postImageArr[0];
								$postMedia			=	'<div class="media-sec fix-height" style=""> <img class="img-responsive" src="'.$postImage.'"> </div>';
							}
						}elseif($post_category == 'create_wall'){
							$postMsg				  =	'<a href="'.SITE_ROOT.$allPosts['usl_fameuz_by'].'">'.$postUserBy.' </a><span class="postmsg"> posted photos on his wall</span>';
							$likeCatId			    =	1;
							$shareCatId			   =	1;
							if(count($postImageArr)>0){
								$postImage			=	SITE_ROOT.'uploads/albums/'.$postImageArr[0];
								$postMedia			=	'<div class="media-sec fix-height"><a href="javascript:;" class="lightBoxs" data-contentId="'.$allPosts['id'].'" data-contentType="'.$likeCatId.'"><img class="img-responsive" src="'.$postImage.'"></a> </div>';	
							}
						}elseif($post_category == 'create_review'){
							$postMsg				  =	'<a href="'.SITE_ROOT.$allPosts['usl_fameuz_to'].'">'.$postUserTo.' </a> <span class="postmsg">has added a review to </span><a href="'.SITE_ROOT.$allPosts['usl_fameuz_by'].'">'.$postUserBy.'\'s </a>  <span class="postmsg">profile</span>';
							$likeCatId			    =	11;
							$shareCatId			   =	11;
						}elseif($post_category == 'create_album'){
							$postMsg				  =	'<a href="'.SITE_ROOT.$allPosts['usl_fameuz_by'].'">'.$postUserBy.' </a> <span class="postmsg">created a album</span> - \''.$postDescr.'\' <span class="postmsg">on his timeline</span>';
							$likeCatId			    =	9;
							$shareCatId			   =	9;
							$albumImgCount			=	count(array_filter($postImageArr));
							if($albumImgCount==1){
								$postMedia			=	'<div class="media-sec fix-height"><a href="javascript:;" class="lightBoxs" data-contentId="'.$postImageIdArr[0].'" data-contentType="2" data-popcat="'.$allPosts['id'].'"><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$postImageArr[0].'"></a></div><div class="clearfix"></div>';
							}else if($albumImgCount==2){
								$postMedia			=	'<div class="media-sec"><div class="left-img count-2-plus"><a href="javascript:;" class="photoPopUp" data-contentId="'.$postImageIdArr[0].'" data-contentType="2" data-popcat="'.$allPosts['id'].'"><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$postImageArr[0].'"></a></div><div class="left-img count-2-plus"><a href="javascript:;" class="lightBoxs" data-contentId="'.$postImageIdArr[0].'" data-contentType="2" data-popcat="'.$allPosts['id'].'" ><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$postImageArr[1].'"> </a></div></div><div class="clearfix"></div>';
							}else if($albumImgCount==3){
								$postMedia			=	'<div class="media-sec"><div class="left-img count-2-plus"><a href="javascript:;" class="lightBoxs" data-contentId="'.$postImageIdArr[0].'" data-contentType="2" data-popcat="'.$allPosts['id'].'" ><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$postImageArr[0].'"></a></div><div class="left-img count-2-plus"><a href="javascript:;" class="lightBoxs" data-contentId="'.$postImageIdArr[0].'" data-contentType="2" data-popcat="'.$allPosts['id'].'"><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$postImageArr[1].'"> </a><a href="#" class="more-items two_more"><p> <span>+</span> More Items</p></a></div><div class="clearfix"></div></div><div class="clearfix"></div>';
							}
							else if($albumImgCount==4){
								$postMedia			=	'<div class="media-sec"><div class="left-img count-4"><a href="javascript:;" class="lightBoxs" data-contentId="'.$postImageIdArr[0].'" data-contentType="2" data-popcat="'.$allPosts['id'].'"><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$objCommon->getThumb($postImageArr[0]).'"></a></div><div class="left-img count-4"><a href="javascript:;" class="lightBoxs" data-contentId="'.$postImageIdArr[1].'" data-contentType="2" data-popcat="'.$allPosts['id'].'"><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$objCommon->getThumb($postImageArr[1]).'"></a></div><div class="left-img count-4"><a href="javascript:;" class="lightBoxs" data-contentId="'.$postImageIdArr[2].'" data-contentType="2" data-popcat="'.$allPosts['id'].'"><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$objCommon->getThumb($postImageArr[2]).'"></a> </div><div class="left-img count-4"><a href="javascript:;" class="lightBoxs" data-contentId="'.$postImageIdArr[3].'" data-contentType="2" data-popcat="'.$allPosts['id'].'"><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$objCommon->getThumb($postImageArr[3]).'"></a></div></div><div class="clearfix"></div>';

							}
							else if($albumImgCount > 4){
								$postMediaStr	    =	'';
								$postMediaStr	   .=	'<div class="media-sec"><div class="left-img count-5-plus"><a href="javascript:;" class="lightBoxs" data-contentId="'.$postImageIdArr[0].'" data-contentType="2" data-popcat="'.$allPosts['id'].'" ><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$postImageArr[0].'" /></a></div>';
								for($i=1; $i<5; $i++){
									$postMediaStr   .=	'<div class="right-thumbs pull-left"><a href="javascript:;" class="lightBoxs" data-contentId="'.$postImageIdArr[$i].'" data-contentType="2" data-popcat="'.$allPosts['id'].'"><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$objCommon->getThumb($postImageArr[$i]).'" /></a>';
									if($i==4){
										$postMediaStr .=	'<a href="#" class="more-items">
																<p> <span>+</span> More Items</p>	
															 </a>'; //add more button here
									}
									$postMediaStr	.=	'</div>';
								}
								$postMediaStr	   .=	'<div class="clearfix"></div></div>';
								$postMedia		   =	$postMediaStr;
							}
						}else if($post_category == 'create_video'){
							$postMsg				  =	'<a href="'.SITE_ROOT.$allPosts['usl_fameuz_by'].'">'.$postUserBy.' </a><span class="postmsg"> posted a video</span>';
							$likeCatId			    =	4;
							$shareCatId			   =	3;
							$postMedia				=	'<div class="media-sec fix-height home_video"> <img class="img-responsive" src="'.SITE_ROOT.'uploads/videos/'.$postImageArr[0].'"><a class="playbtn lightBoxsVideo lightBoxs" href="javascript:;"  data-videncrid="'.$allPosts['post_alias'].'"></a> </div>';
						}else if($post_category == 'shared'){
//----------------------------------------share start-------------------------------------
							$arrShareCat			  =	array('1'=>"Wall Images",'2'=>"Album images",'3'=>"Videos",'6'=>"Profile Images",'7'=>"Blogs",'9'=>"Albums",'10'=>"posts",'11'=>"Reviews",'12'=>"Games");
							if($allPosts['post_type']== 12){
								$postMsg				  =	'<a href="'.SITE_ROOT.$allPosts['usl_fameuz_by'].'">'.$postUserBy.' </a><span class="postmsg"> Shared </span>
								<a href="'.SITE_ROOT.'games/show/'.$allPosts['share_video_alias'].'-'.$allPosts['contentId'].'">'.$objCommon->html2text($allPosts['post_name']).'</a> Games';
							}else{
								$postMsg				  =	'<a href="'.SITE_ROOT.$allPosts['usl_fameuz_by'].'">'.$postUserBy.' </a><span class="postmsg"> Shared </span><a href="'.SITE_ROOT.$allPosts['usl_fameuz_by'].'">'.$postUserTo.'\'s</a><span class="postmsg"> '.$arrShareCat[$allPosts['post_type']].'</span>';
							}
							if($allPosts['post_type']==1){ //share wall images
								$postImage			=		SITE_ROOT.'uploads/albums/'.$allPosts['share_images'];
								$postMedia			=	'<div class="media-sec fix-height"><a href="javascript:;" class="lightBoxs" data-contentId="'.$allPosts['contentId'].'" data-contentType="'.$allPosts['post_type'].'"><img class="img-responsive" src="'.$postImage.'"></a> </div>';
							}else if($allPosts['post_type']==2){ //share album images
								$postImage			=		SITE_ROOT.'uploads/albums/'.$allPosts['share_images'];
								$postMedia			=	'<div class="media-sec fix-height"><a href="javascript:;" class="lightBoxs" data-contentId="'.$allPosts['contentId'].'" data-contentType="'.$allPosts['post_type'].'"><img class="img-responsive" src="'.$postImage.'"></a> </div>';
							}else if($allPosts['post_type']==9){ //share albums
								$postImageArr			 =	explode(",",$allPosts['share_images']);
								$postImageIdArr		   =	explode(",",$allPosts['share_images_id']);
								$albumImgCount			=	count(array_filter($postImageArr));
								if($albumImgCount==1){
									$postMedia			=	'<div class="media-sec fix-height"><a href="javascript:;" class="lightBoxs" data-contentId="'.$postImageIdArr[0].'" data-contentType="2" data-popcat="'.$allPosts['id'].'"><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$postImageArr[0].'"></a></div><div class="clearfix"></div>';
								}else if($albumImgCount==2){
									$postMedia			=	'<div class="media-sec"><div class="left-img count-2-plus"><a href="javascript:;" class="photoPopUp" data-contentId="'.$postImageIdArr[0].'" data-contentType="2" data-popcat="'.$allPosts['id'].'"><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$postImageArr[0].'"></a></div><div class="left-img count-2-plus"><a href="javascript:;" class="lightBoxs" data-contentId="'.$postImageIdArr[0].'" data-contentType="2" data-popcat="'.$allPosts['id'].'" ><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$postImageArr[1].'"> </a></div></div><div class="clearfix"></div>';
								}else if($albumImgCount==3){
									$postMedia			=	'<div class="media-sec"><div class="left-img count-2-plus"><a href="javascript:;" class="lightBoxs" data-contentId="'.$postImageIdArr[0].'" data-contentType="2" data-popcat="'.$allPosts['id'].'" ><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$postImageArr[0].'"></a></div><div class="left-img count-2-plus"><a href="javascript:;" class="lightBoxs" data-contentId="'.$postImageIdArr[0].'" data-contentType="2" data-popcat="'.$allPosts['id'].'"><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$postImageArr[1].'"> </a><a href="#" class="more-items two_more"><p> <span>+</span> More Items</p></a></div><div class="clearfix"></div></div><div class="clearfix"></div>';
								}
								else if($albumImgCount==4){
									$postMedia			=	'<div class="media-sec"><div class="left-img count-4"><a href="javascript:;" class="lightBoxs" data-contentId="'.$postImageIdArr[0].'" data-contentType="2" data-popcat="'.$allPosts['id'].'"><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$objCommon->getThumb($postImageArr[0]).'"></a></div><div class="left-img count-4"><a href="javascript:;" class="lightBoxs" data-contentId="'.$postImageIdArr[1].'" data-contentType="2" data-popcat="'.$allPosts['id'].'"><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$objCommon->getThumb($postImageArr[1]).'"></a></div><div class="left-img count-4"><a href="javascript:;" class="lightBoxs" data-contentId="'.$postImageIdArr[2].'" data-contentType="2" data-popcat="'.$allPosts['id'].'"><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$objCommon->getThumb($postImageArr[2]).'"></a> </div><div class="left-img count-4"><a href="javascript:;" class="lightBoxs" data-contentId="'.$postImageIdArr[3].'" data-contentType="2" data-popcat="'.$allPosts['id'].'"><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$objCommon->getThumb($postImageArr[3]).'"></a></div></div><div class="clearfix"></div>';	
								}
								else if($albumImgCount > 4){
									$postMediaStr	    =	'';
									$postMediaStr	   .=	'<div class="media-sec"><div class="left-img count-5-plus"><a href="javascript:;" class="lightBoxs" data-contentId="'.$postImageIdArr[0].'" data-contentType="2" data-popcat="'.$allPosts['id'].'" ><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$postImageArr[0].'" /></a></div>';
									for($i=1; $i<5; $i++){
										$postMediaStr   .=	'<div class="right-thumbs pull-left"><a href="javascript:;" class="lightBoxs" data-contentId="'.$postImageIdArr[$i].'" data-contentType="2" data-popcat="'.$allPosts['id'].'"><img class="img-responsive" src="'.SITE_ROOT.'uploads/albums/'.$objCommon->getThumb($postImageArr[$i]).'" /></a>';
										if($i==4){
											$postMediaStr .=	'<a href="#" class="more-items">
																	<p> <span>+</span> More Items</p>	
																 </a>'; //add more button here
										}
										$postMediaStr	.=	'</div>';
									}
									$postMediaStr	   .=	'<div class="clearfix"></div></div>';
									$postMedia		   =	$postMediaStr;
								}
							}else if($allPosts['post_type']==3){ //share videos
							$postImage				=		SITE_ROOT.'uploads/videos/'.$allPosts['share_images'];
							$postMedia				=	'<div class="media-sec fix-height home_video"> <img class="img-responsive" src="'.$postImage.'"><a class="playbtn lightBoxsVideo lightBoxs" href="javascript:;"  data-videncrid="'.$allPosts['share_video_alias'].'"></a> </div>';
							}else if($allPosts['post_type']==6){ //share profile image
							$postImage				=		SITE_ROOT.'uploads/profile_images/'.$allPosts['share_images'];
							$postMedia			=	'<div class="media-sec fix-height"><a href="javascript:;" class="lightBoxs" data-contentId="'.$allPosts['contentId'].'" data-contentType="6"><img class="img-responsive" src="'.$postImage.'"></a> </div>';
							}else if($allPosts['post_type']==10){ //share posts
								if($allPosts['share_images']){
									$postImage			=	SITE_ROOT_AWS_IMAGES.'uploads/albums/'.$allPosts['share_images'];
									$postMedia			=	'<div class="media-sec fix-height" style=""> <img class="img-responsive" src="'.$postImage.'"> </div>';
								}else{
									$postImage	=	$postMedia	=	'';
								}
							}else if($allPosts['post_type']==7){ //share blogs
							$postImage				=		SITE_ROOT_AWS_IMAGES.'uploads/blogs/'.$allPosts['share_images'];
							$postMedia			=	'<div class="media-sec fix-height"><a href="'.SITE_ROOT.'blog/show/'.$allPosts['share_video_alias'].'-'.$allPosts['contentId'].'"><img class="img-responsive" src="'.$postImage.'"></a> </div>';
							}else if($allPosts['post_type']==13){ //share market place
							$postImage				=		SITE_ROOT_AWS_IMAGES.'uploads/market_place/'.$allPosts['share_images'];
							$postMedia			=	'<div class="media-sec fix-height"><a href="'.SITE_ROOT.'market-place/show/'.$allPosts['share_video_alias'].'-'.$allPosts['contentId'].'"><img class="img-responsive" src="'.$postImage.'"></a> </div>';
							}else if($allPosts['post_type']==14){ //share videos
							$musicThumb			   =	($allPosts['share_images'])?$allPosts['share_images']:'audio-thump.jpg';
							$postImage				=	SITE_ROOT_AWS_MUSIC.'uploads/music/'.$musicThumb;
							$postMedia				=	'<a class=" lightBoxsVideo" href="'.SITE_ROOT.'music?musicid='.$allPosts['contentId'].'" ><div class="media-sec fix-height home_video"> <img class="img-responsive" src="'.$postImage.'"></div></a>';
							}
//------------------------------share end-----------------------------------------
						}
					?>
					<div class="col-sm-12 home_update_margin">
						<div class="home_update">
                        <a href="javascript:;" class="dropMe_down pull-right" data-category="<?php echo $allPosts['post_category'];?>" data-id="<?php echo $allPosts['id'];?>">
                            <i class="fa fa-chevron-down"></i>
                                <ul class="drop_down_delete">
                                    <li onclick="deletePost(this);">
                                        <span class="mainText"><i class="fa fa-trash"></i>Delete Post </span>
                                        <span class="helpText">Stop seeing this post</span>
                                       
                                    </li>
                                    <?php /*?><li>
                                        <span class="mainText"><i class="fa fa-close"></i>Hide Post </span>
                                        <span class="helpText">Hide seeing this post</span>
                                       
                                    </li><?php */?>
                                </ul>
                        </a>
                        <div class="user-info">
							<?php
							$userImgCond	=	($allPosts['upi_img_url_by'])?$allPosts['upi_img_url_by']:'profile_pic.jpg';
							$userImg		=	'<a href="'.SITE_ROOT.$allPosts['usl_fameuz_by'].'"><img class="img-responsive" src="'.SITE_ROOT.'uploads/profile_images/'.$userImgCond.'" alt="thump"></a>';
							$objCommon->user_info_home_widget($postMsg,$userImg,date(" F d, Y ",strtotime($allPosts['post_created'])),$objCommon->html2text($allPosts['model_category']),$allPosts['reviewAvg'],$objCommon->time_elapsed_string(strtotime($objCommon->local_time($allPosts['post_created']))));
							?>
                            <div class="clearfix"></div>
							</div>
						<?php
							echo $postMedia;
						?>
                        <div class="desck fix-height">
							<?php
							if($allPosts['post_type'] != 12){
							?>
							<p class="desc-update"><?php echo $postDescr?></p>
							<?php
							}
							if($post_category != 'shared'){
							?>
                            <div class="botom-menus" style="border-top:1px solid #f5f5f5;"><div class="row" >
                            	<div class="col-sm-6">
                                    <div class="count-share-i">
									<?php
									if($likeCatId != 11){
									?>
                                        <div class="review_message" data-comment="<?php echo $allPosts['id']?>" data-category="<?php echo $likeCatId?>" data-for="<?php echo $allPosts['user_id_by']?>"><a  href="javascript:;"><i class="fa_comment"></i> <span class="text">Comment</span> </a> </div>
									<?php
									}
									?>
                                          <div class="like"> <a class="like_btn <?php echo ($youLike==1)?'liked':''?>"  data-like="<?php echo $allPosts['id']?>" data-category="<?php echo $likeCatId?>" data-userby="<?php echo $allPosts['user_id_by']?>" href="javascript:;"><i class="fa_heart"></i> <span class="text"><?php echo ($youLike==1)?'Liked':'Like'?></span> </a> </div>
                                          <div class="share"> <a class="share_btn <?php echo ($youShare==1)?'liked':''?>" data-share="<?php echo $allPosts['id']?>" data-category="<?php echo $shareCatId?>" data-userby="<?php echo $allPosts['user_id_by']?>" href="javascript:;" data-toggle="modal" data-target=".shared"><i class="fa_share"></i><?php echo ($youShare==1)?'Shared':'Share'?> </a> </div>
                                      </div>
                                  </div>
                                  <div class="col-sm-3">
                                  	<div class="comment-count">
							  <div>
							  	<?php
								if($likeCatId != 11){
									if($allPosts['commentCount'] != 'none' && $allPosts['commentCount'] !='' && $allPosts['commentCount'] !=NULL){
										$commentCount			 =	$allPosts['commentCount'];
									}else{
										$commentCount			 =	0;
									}
								?>
								<div class="comment"> <span><i class="fa_comment_alt"></i></span><span class="colo commentCountDisplay_<?php echo $allPosts['id']?>_<?php echo $likeCatId?>"><?php echo $commentCount?></span> </div>
								<?php
								}
								?>
								<a  <?php echo ($countLike >0)?' data-toggle="modal" data-target=".likes"  class="like likedPopUp"':'class="like"'?>  data-postid="<?php echo $allPosts['id']?>" data-posttype="<?php echo $likeCatId?>"> <span><i class="fa_heart_gery"></i></span><span class="colo likeCountDisplay_<?php echo $allPosts['id']?>_<?php echo $likeCatId?>"><?php echo $countLike;?></span> </a>
							  </div>
							</div>
                             </div>
                             <div class="col-sm-3">
                             	<div class="share_social"><!--<a href="javascript:;"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> <a href="#"><i class="fa fa-pinterest-p"></i></a><a href="#"><i class="fa fa-instagram"></i></a>--> </div>
                             </div>
                             <?php /*?><div class="col-sm-2">
                             	<!--<div class="mail-sec pull-right"> <a href="#" data-toggle="modal" data-target=".mail"><span class="fa_mail"></span>&nbsp;<span>Email</span></a></div>-->
                             </div><?php */?>
                              </div></div>
							<?php
							}
							?>
						  </div>
						  <div class="clr"></div>
						</div>
                        <div class="comment-sec" style="width:100%">
						<form id="comment_form_home" method="post" action="<?php echo SITE_ROOT?>access/post_home_comment.php?action=add_comment&commentContent=<?php echo $allPosts['id']?>&commentCat=<?php echo $likeCatId?>&noti_whome=<?php echo $allPosts['user_id_by']?>" data-comment="<?php echo $allPosts['id']?>" data-category="<?php echo $likeCatId?>" data-for="<?php echo $allPosts['user_id_by']?>">
                        	<div class="comment-secs-n">
                            	<div class="row">
                                	<div class="col-sm-1 col-alt-lg-1">
                                    	<div class="user-img">
                                        	<img src="<?php echo SITE_ROOT?>uploads/profile_images/<?php echo($getMyFollowing['upi_img_url'])?$getMyFollowing['upi_img_url']:'profile_pic.jpg'?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-11">
                                    	<div class="comments">
                                        	<div class="comments_arrow">
                                            	<img src="<?php echo SITE_ROOT?>images/comments_arrow1.png" />
                                            </div>
                                            <textarea placeholder="Comment" class="comment_descr_input1" name="comment_descr_input"></textarea>
											<div class="ajaxloader text-center"><img src="<?php echo SITE_ROOT?>images/ajax-loader.gif" alt="comments_arrow"></div> 
                                        </div>
                                    </div>
                                    <?php /*?><div class="col-sm-1 col-lg-sp-2">
                                    	<div class="smileys">
                                        	<img class="center-block" src="<?php echo SITE_ROOT?>images/cam.png" />
                                        </div>
                                    	<div class="smileys">
                                        	<img class="center-block" src="<?php echo SITE_ROOT?>images/smily.png" />
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
						</form>
<!--------------------------------------------------------------------->
						<div class="comment_box_outer loadComment_<?php echo $allPosts['id']?>_<?php echo $likeCatId?>"></div>
<!--------------------------------------------------------------------->
                        </div>
					 </div>
		  			<?php
				}
			}else{
				?>
				<!--<div class="dummy_box default_no_image"></div>-->
				<?php
			}
			?>
        </div>
        <!------------share post modal---------->
        	<div class="modal fade shared" id="shared" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                    <p>Your share posted to your news feed</p>
                  </div>
                </div>
              </div>
            </div>
        <!-----------end share modal----------->
<?php
include_once(DIR_ROOT."widget/like_view_popup.php");
?>
