<div class="row">
	<div class="col-sm-9">
	<div class="grey-sec-blog">
		<div class="row">
		<div class="col-sm-2 alt-width-blog">
			<div class="img-sec-blog">
				<img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($getUserDetails['upi_img_url'])?$getUserDetails['upi_img_url']:'profile_pic.jpg'?>" />
			</div>
		</div>
		<div class="col-sm-10 alt-width-blog-search">
	  <div class="form-group">
		  <form action="<?php echo SITE_ROOT?>blog" method="get">
		  	<div class="col-sm-10">
			<input type="text" class="form-control checkme col-sm-8"  placeholder="Search Blog" name="search" value="<?php echo ($searchBlog)?$searchBlog:''?>">
			</div>
			<div class="col-sm-2 alt-padding-lg-left">
			<input type="submit" value="Search" class="searchBlog" />
			</div>
		  </form>
	  </div>
	  </div>
	  </div>
	 </div>
	</div>
	<div class="col-sm-3">
	  <div class="grey-sec-blog-create">
		<div class="row">
			<div class="col-sm-9">
				<a href="<?php echo SITE_ROOT.'user/add-blogs'?>" class="create-blog" data-toggle="tooltip" data-placement="bottom" title="Create Blog"><img src="<?php echo SITE_ROOT?>images/blog.png" /><span class="hidden-sm hidden-md" >Create Blog</span></a>
			  </div>
			 <div class="col-sm-3">
				<div class="notification-blog">
			   <?php
				include_once(DIR_ROOT."widget/notification_head.php");
				?>
				</div>
			</div>
		</div>
	  </div>
	</div>
</div>