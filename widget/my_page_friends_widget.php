<?php
$getAllFriends	   =	$objUsers->listQuery("select user.user_id,user.first_name,user.last_name,user.display_name,profileImg.upi_img_url,user.email,social.usl_fameuz
from following as follow 
left join users as user on (follow.follow_user1 = user.user_id and follow.follow_user1 !='".$getMyFriendDetails['user_id']."') or (follow.follow_user2 = user.user_id and follow.follow_user2 !='".$getMyFriendDetails['user_id']."')  
LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN personal_details AS personal ON user.user_id=personal.user_id 
LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
where((follow.follow_user2='".$getMyFriendDetails['user_id']."' or follow.follow_user1='".$getMyFriendDetails['user_id']."') and follow.follow_status='2')  order by user.first_name asc limit 0,8");
if(count($getAllFriends)>0){
?>
<div class="photos_section myLoadFriends">
	<div class="photos_box">
	<?php
		foreach($getAllFriends as $allFriends){
			$allFrendImg		=	SITE_ROOT.'uploads/profile_images/'.(($allFriends['upi_img_url'])?$allFriends['upi_img_url']:"profile_pic.jpg");
			$allFrendImgName	=	$objCommon->displayName($allFriends);
	?>
	<div class="photos friends">
		<div class="pic">
			<a href="<?php echo SITE_ROOT.$objCommon->html2text($allFriends['usl_fameuz'])?>">
				<img src="<?php echo $allFrendImg?>" alt="<?php echo $allFrendImgName?>" title="<?php echo $allFrendImgName?>" />
                <div class="shadow_overlay"></div>
				<span><?php echo $objCommon->displayNameSmall($allFriends,12);?></span>
			</a>
		</div>
	</div>
	<?php
		}
		?>
		<div class="clr"></div>
		<div class="row">
			<div class="col-sm-12 text-right">
			<?php
			if($getMyFriendDetails['user_id']==$_SESSION['userId']){
				?>
				<span class="seemore "><a href="<?php echo SITE_ROOT?>user/following">See More <i class="fa fa-play "></i></a></span>
				<?php
			}else{
				?>
				<span class="seemore "><a href="<?php echo SITE_ROOT?>user/<?php echo $user_url?>/friends">See More <i class="fa fa-play "></i></a></span>
				<?php
			}
			?>
			</div>
		</div>
	</div>
</div>	
<?php
}else{
?>
<p>No friends found</p>
<?php
}
?>