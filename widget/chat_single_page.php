<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/message.php");
$objCommon				   			  =	new common();
$objMsg								 =	new message();
$friendId							   =	$objCommon->esc($_GET['friendId']);
$page							 	   =	$objCommon->esc($_GET['page']);
$count_per_page						 =	$objCommon->esc($_GET['count_per_page']);
$totalCount							 =	$objCommon->esc($_GET['totalCount']);
$sql_all_messages					   =	"SELECT msg.*,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,attach.ma_attachement,attach.ma_attach_type
									FROM  message AS msg
									LEFT JOIN message_attachement AS attach ON msg.msg_id = attach.msg_id
									LEFT JOIN users AS user ON msg.msg_from = user.user_id
									LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
									LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
									WHERE ((msg.msg_from=".$_SESSION['userId']." AND msg.msg_to=".$friendId.") OR (msg.msg_to=".$_SESSION['userId']." AND msg.msg_from=".$friendId.")) AND msg.msg_status=1 ORDER BY msg.msg_created_date desc";
$totalPages							 =	ceil(($totalCount/$count_per_page));
$orderFrom							  =	$count_per_page*($page-1);
$orderTo								=	$count_per_page*$page;
$sql_all_messages				   	  .=   " LIMIT ".$orderFrom.",".$count_per_page;
$myAllMsg							   =	$objMsg->listQuery($sql_all_messages);			
foreach($myAllMsg as $keyAllMsg=>$allMsg){ 
	if($chatDate != date("d-m-Y",strtotime($allMsg['msg_created_date']))){
		$chatDate			=	date("d-m-Y",strtotime($allMsg['msg_created_date']));
		echo '</div><div class="date_chat"><span>'.$chatDate.'</span></div><div class="chats_show">';
	}
	$chatUserImg			 =	($allMsg['upi_img_url'])?$allMsg['upi_img_url']:'profile_pic.jpg';
	?>
	<div class="sender delMe">
			<div class="chk_delt">
				<input type="checkbox" class="checkEd" />
			</div>
			<div class="img_user">
				<img src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.$chatUserImg?>" />
			</div>
			<div class="userInfo_online_chat">
				<h4><?php echo $objCommon->displayName($allMsg);?><span class="time_chat"><?php echo date("h:i a",strtotime($objCommon->local_time($allMsg['msg_created_date'])))?></span></h4>
				<p><?php echo $objCommon->html2text($allMsg['msg_descr']);?></p>
			</div>
		</div>
<?php 
}
echo '</div>';//close div of class 'chat_show'
if($orderTo < $totalCount){
	echo '<a href="javascript:;" onclick="doShowChatList('.$friendId.','.($page+1).','.$count_per_page.','.$totalCount.');" class="loadClick">Load more...</a>';
}
?>