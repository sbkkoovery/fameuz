<?php
$get_client_ip			=	$objCommon->get_client_ip();
function iptocountry($ip) {  
   $numbers = preg_split( "/\./", $ip); 
   $two_letter_country_code	=	'';
   if (file_exists(DIR_ROOT."ip_files/".$numbers[0].".php")){ 
	   include(DIR_ROOT."ip_files/".$numbers[0].".php");  
	   $code=($numbers[0] * 16777216) + ($numbers[1] * 65536) + ($numbers[2] * 256) + ($numbers[3]);  
	   foreach($ranges as $key => $value){  
		  if($key<=$code){  
			 if($ranges[$key][0]>=$code){$two_letter_country_code=$ranges[$key][1];break;}  
			}  
		} 
   	}
		if ($two_letter_country_code==""){ $two_letter_country_code="unknown"; }  
		return $two_letter_country_code; 
	 
	}
	$two_letter_country_code  =	iptocountry($get_client_ip);
	if($two_letter_country_code != ""  && $two_letter_country_code != "unknown"){  
		include(DIR_ROOT."ip_files/countries.php");   
		$country_name			 =	$countries[$two_letter_country_code][1];  
		$file_to_check			=	SITE_ROOT."ip_files/flags/".strtolower($two_letter_country_code).".png";  
		if (file_exists(DIR_ROOT."ip_files/flags/".strtolower($two_letter_country_code).".png")){  
			$flagImg	=	'<img src="'.$file_to_check.'" width="15" height="10">';  
		}else{  
			$flagImg	=	'<img src="'.SITE_ROOT.'"ip_files/flags/noflag.gif" width="20" height="15">';  
		}  
	}
$getCountryDetails			   =	$objUsers->getRowSql("SELECT country_id FROM country WHERE country_code='".$two_letter_country_code."'");
$getCountryId					=	$getCountryDetails['country_id'];
$_SESSION['country_user']		=	array("country_id"=>$getCountryId,"country_name"=>$country_name,"country_code"=>$two_letter_country_code,"country_flag"=>$flagImg);
$getPromotionDetails			 =	$objUsers->listQuery("SELECT user_id,promo_type,promo_content_id,promo_category FROM promotion  WHERE promo_country LIKE '%,".$getCountryId.",%' AND  DATE(NOW()) between promo_start_date and 
promo_end_date AND promo_admin_status=1 AND promo_status=1");
if(count($getPromotionDetails)>0){
	unset($_SESSION['promotion_profile']);
	unset($_SESSION['promotion_video']);
	unset($_SESSION['promotion_music']);
	foreach($getPromotionDetails as $allPromoDetails){
		if($allPromoDetails['promo_type']=='profile'){
			$premiumProfile[]						   =	$allPromoDetails['promo_content_id'];
			$_SESSION['promotion_profile'][]			=	array($allPromoDetails['promo_content_id'],$allPromoDetails['promo_category']);
		}else if($allPromoDetails['promo_type']=='video'){
			$_SESSION['promotion_video'][]			  =	array($allPromoDetails['promo_content_id'],$allPromoDetails['promo_category']);
		}else if($allPromoDetails['promo_type']=='music'){
			$_SESSION['promotion_music'][]			  =	array($allPromoDetails['promo_content_id'],$allPromoDetails['promo_category']);
		}
	}
}
if(count($premiumProfile)>0){
	shuffle($premiumProfile);
	$premiumProfileRandom 			  					  =	array_slice($premiumProfile, 0, 15);
	$premiumProfileRandomString				   			=	implode(",",$premiumProfileRandom);
}
	if(count($premiumProfileRandom)<15){
		$limitOthers									   =	15-count($premiumProfileRandom);
		$getPromotionDetailsAll			 				=	$objUsers->getRowSql("SELECT group_concat(promo_content_id) AS otherPremProf FROM promotion  WHERE  DATE(NOW()) between promo_start_date and 
	promo_end_date AND promo_admin_status=1 AND promo_status=1 AND promo_type='profile' AND user_id NOT IN (".$premiumProfileRandomString.")");
		$getPromotionDetailsAllOther					   =	$getPromotionDetailsAll['otherPremProf'];
	}
	$allPromotionProfiles								  =	$premiumProfileRandomString.','.$getPromotionDetailsAllOther;
	$allPromotionProfiles								  =	trim($allPromotionProfiles,",");
	$countPromotionFiles								   =	count(array_filter(explode(",",$allPromotionProfiles)));
	if($countPromotionFiles<15){
		$getOtherUser									  =	$objUsers->getRowSql("SELECT group_concat(user_id) AS otheruser FROM users WHERE status=1 AND email_validation=1 LIMIT ".(15-$countPromotionFiles));
		$allPromotionProfiles							  .=	','.$getOtherUser['otheruser'];
		$allPromotionProfiles							  =	trim($allPromotionProfiles,",");
	}
	if($allPromotionProfiles !=''){
		$getPremiumModels				 				  =	$objUsers->listQuery("SELECT user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz
																FROM users AS user 
																LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
																LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id 
																LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
																WHERE (user.status=1 OR user.status=4) AND user.email_validation=1 AND  user.user_id IN (".$allPromotionProfiles.")  ORDER BY FIELD(user.user_id,".$allPromotionProfiles.") LIMIT ".$limitOthers);
	?>
	<div class="models">
		<h2 class="home-head">Premium models</h2>
		<div class="model_thumbs">
			<?php
			foreach($getPremiumModels as $allPremProf){
				$premiumModelName						 =	$objCommon->displayNameSmall($allPremProf,10);
				$premiumModelImg			 			  =	($allPremProf['upi_img_url'])?$allPremProf['upi_img_url']:'profile_pic.jpg';
			?>
			<a href="<?php echo SITE_ROOT.$allPremProf['usl_fameuz']?>" title="<?php echo $premiumModelName?>"><img src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.$premiumModelImg?>" alt="<?php echo $premiumModelName?>" /></a>
			<?php
			}
			?>
			<div class="clr"></div>
			<div class="border"><span><?php /*?><a href="#">View all models  <i class="fa fa-play "></i></a><?php */?></span></div>
		</div>
	</div>
	<?php
	}
?>