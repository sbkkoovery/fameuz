<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/users.php");
$objUsers						=	new users();
$objCommon				   	   =	new common();
$gender						  =	($_GET['gender'])?$objCommon->esc($_GET['gender']):2;
$genderStr					   =	($_GET['gender']==1)?'male':'female';
$mostWantedCat				   =	$objUsers->getRowSql("SELECT mwc_category FROM most_wated_category WHERE mwc_id = 1");
$getMostWantedCat				=	$objUsers->listQuery("SELECT c_id,c_name,c_alias FROM category WHERE c_id IN (".$mostWantedCat['mwc_category'].")");
$totalLikeCountSQl			   =	$objUsers->getRowSql("SELECT COUNT(like_id) AS likeCount FROM likes WHERE like_status=1");
$totalLikeCount				  =	$totalLikeCountSQl['likeCount'];
$keyAllMost				= 0;
foreach($getMostWantedCat as $allMostWantedCat){
	$getMostProfile		=	$objUsers->getRowSql("SELECT tab.user_id,(((tab.countLike*100)/".$totalLikeCount.")+tab.reviewAvg) AS finalCount,tab.upi_img_url FROM(
SELECT ucat.user_id,(AVG(reviews.review_rate)*20) AS reviewAvg,lk.likeCount AS countLike,profileImg.upi_img_url
FROM user_categories AS ucat
LEFT JOIN user_reviews AS reviews ON ucat.user_id = reviews.user_id_to
LEFT JOIN user_profile_image as profileImg ON ucat.user_id = profileImg.user_id AND profileImg.upi_status=1
LEFT JOIN personal_details AS personal ON ucat.user_id = personal.user_id
LEFT JOIN (SELECT likes.like_img_user_by,count(likes.like_id) AS likeCount FROM user_categories AS ucat  LEFT JOIN likes  ON ucat.user_id = likes.like_img_user_by WHERE likes.like_status=1 GROUP BY ucat.user_id) AS lk
ON ucat.user_id = lk.like_img_user_by 
WHERE ucat.uc_c_id =".$allMostWantedCat['c_id']."  AND ucat.uc_m_id = 2 AND personal.p_gender = ".$gender."
GROUP BY ucat.user_id
) AS tab ORDER BY finalCount DESC LIMIT 1");

if($getMostProfile['user_id'] ==''){
	continue;
}
$keyAllMost++;
?>
	<div class="col-sm-4 col-lg-sp-5">
		<div class="face-img-category">
			<img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($getMostProfile['upi_img_url'])?$getMostProfile['upi_img_url']:'profile_pic.jpg'?>" />
		</div>
		<div class="model-head">
			<p><?php echo $objCommon->html2text($allMostWantedCat['c_name']);?></p>
			<a href="<?php echo SITE_ROOT.'most-wanted/show/'.$objCommon->html2text($allMostWantedCat['c_alias'])?>/<?php echo $genderStr?>" class="arrow-side">
				<i class="fa fa-chevron-right"></i>
			</a>
		</div>
	</div>
	<?php
	if(($keyAllMost)%5==0){
		echo '<div class="col-sm-12 hidden-sm"><div class="margin_arrange"></div></div>';
	}
	?>
<?php
}
?>