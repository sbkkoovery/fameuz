<?php
$mostWantedCat				   =	$objUsers->getRowSql("SELECT mwc_category FROM most_wated_category WHERE mwc_id = 1");
$getMostWantedCat				=	$objUsers->listQuery("SELECT c_id,c_name,c_alias FROM category WHERE c_id IN (".$mostWantedCat['mwc_category'].")");
?>
<div class="filter-most">
  <div class="most-head-sec">
  <p>Top Ranked Professionals </p>
  <span class="point-it-1"></span>
  </div>
  <div class="filter-pre">
	  <div class="row">
		<div class="col-sm-6">
			<div class="female_sec border-ed">
				<div class="head-filter-pre">
					<p>Female<i class="fa fa-chevron-down pull-right"></i></p>
					<ul>
						<?php
						foreach($getMostWantedCat as $allMostWantedCat){
						?>
						<li><a href="<?php echo SITE_ROOT.'most-wanted/show/'.$allMostWantedCat['c_alias'].'/female'?>" <?php echo ($allMostWantedCat['c_alias']==$memberCategory && $gender=='female')?'class="active-filter"':''?>><?php echo $objCommon->html2text($allMostWantedCat['c_name']);?></a></li>
						<?php
						}
						?>            
					 </ul>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="female_sec">
				<div class="head-filter-pre">
					<p>Male<i class="fa fa-chevron-down pull-right"></i></p>
					<ul>
						<?php
						foreach($getMostWantedCat as $allMostWantedCat){
						?>
						<li><a href="<?php echo SITE_ROOT.'most-wanted/show/'.$allMostWantedCat['c_alias'].'/male'?>" <?php echo ($allMostWantedCat['c_alias']==$memberCategory && $gender=='male')?'class="active-filter"':''?>><?php echo $objCommon->html2text($allMostWantedCat['c_name']);?></a></li>
						<?php
						}
						?>              
					 </ul>
				</div>
			</div>
		</div>
	  </div>
  </div>
 </div>