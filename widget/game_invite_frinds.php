<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/following.php");
$objCommon				     =	new common();
$objFollow					 =	new following();
if($_SESSION['userId'] != ''){
	$g_id					  =	$objCommon->esc($_GET['g_id']);
	$sqlInviteFriends		  =	"select user.user_id,user.first_name,user.last_name,user.display_name,profileImg.upi_img_url,user.email,social.usl_fameuz,follow.follow_user1,follow.follow_user2,follow.follow_status,case when (TIMESTAMPDIFF(MINUTE,chat.ucs_last_seen,'".date("Y-m-d H:i:s")."') >15 or chat.ucs_status=0) then '0' else '1' END AS chatStatus,personal.p_country,personal.p_city,cat.c_name
from following as follow 
left join users as user on (follow.follow_user1 = user.user_id and follow.follow_user1 !='".$_SESSION['userId']."') or (follow.follow_user2 = user.user_id and follow.follow_user2 !='".$_SESSION['userId']."')  
left join user_chat_status as chat on user.user_id = chat.user_id 
LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
LEFT JOIN user_social_links AS social ON user.user_id=social.user_id LEFT JOIN personal_details AS personal ON user.user_id=personal.user_id 
LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
where ((follow.follow_user1='".$_SESSION['userId']."') or (follow.follow_user2='".$_SESSION['userId']."' and follow.follow_status='2'))  order by user.first_name asc";
	$getInviteFriends		 =	$objFollow->listQuery($sqlInviteFriends);
	if(count($getInviteFriends) >0){
		?>
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel"><input type="checkbox" id="ckbCheckAll" /> Invite Friends</h4>
		</div>
		<div class="modal-body">
			<form action="<?php echo SITE_ROOT.'access/game_invite_friends.php'?>" method="post" id="gameInvite">
			<ul>
			<?php
			foreach($getInviteFriends as $allInviteFriends){
				$userImg	=	($allInviteFriends['upi_img_url'])?$allInviteFriends['upi_img_url']:'profile_pic.jpg';
			?>
				<li>
					<div class="row">
						<div class="col-md-1">
							<input type="checkbox" name="inviteFrd[]" value="<?php echo $allInviteFriends['user_id']?>" class="checkBoxClass" />
						</div>
						<div class="col-md-11">
							<div class="liked_users">
								<div class="img_users_liked">
									<a href="<?php echo SITE_ROOT.$objCommon->html2text($allInviteFriends['usl_fameuz'])?>"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo $userImg?>" /></a>
								</div>
								<div class="name_users_liked">
									<p class="postmsgs likedPeoples">
										<a href="<?php echo SITE_ROOT.$objCommon->html2text($allInviteFriends['usl_fameuz'])?>"><?php echo $objCommon->displayName($allInviteFriends)?></a>
										<span class="proffesion"><?php echo $objCommon->html2text($allInviteFriends['c_name'])?></span>
									</p>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</li>
			<?php
			}
			?>
			</ul>
			<div class="row">
				<div class="col-md-12 text-right has-spinner">
					<span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span>&nbsp;&nbsp;
					<input type="hidden" name="g_id" value="<?php echo $g_id?>" />
					<button class="btn btn-primary" type="button" id="inviteButton">Invite</button>
				</div>
			</div>
			</form>
		</div>
<?php
	}
	else{
		echo '<p>No friends Found</p>';
	}
}
?>
<script type="application/javascript" language="javascript">
$("#ckbCheckAll").click(function () {
    $(".checkBoxClass").prop('checked', $(this).prop('checked'));
});
$("#inviteButton").on("click",function(){
	$(this).addClass('active');
	$(this).attr('disabled','disabled');
	$(this).closest("form").ajaxForm(
	{
		success:successInvite	
	}).submit();
});
function successInvite(){
	$('#myModal').modal('hide');
}
</script>