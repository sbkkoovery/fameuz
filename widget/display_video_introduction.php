<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/video_introduction.php");
$objVidIntro					 =	new video_introduction();
$objCommon				   	   =	new common();
$user_id						 =	$objCommon->esc($_GET['user_id']);
if($user_id){
	$userId					  =	$user_id;
	$displayForm				 =	0;
}else{
	$userId					  =	$_SESSION['userId'];
	$displayForm				 =	1;
}

if($userId){
	?>
	<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.form.js"></script>
	<script src="<?php echo SITE_ROOT?>jw_player/jwplayer.js"></script>
	<link href="<?php echo SITE_ROOT?>jw_player/jw_player.css" rel="stylesheet" type="text/css">
	<?php
	$getVidIntro				 =	$objVidIntro->getRow("user_id=".$userId);
	if($getVidIntro['vi_id']){
		if($getVidIntro['vi_type']==1){
			$getAiImages				=	SITE_ROOT.'uploads/video_introduction/'.$getVidIntro['vi_thumb'];
			$vidUrl					 =	SITE_ROOT.'uploads/video_introduction/'.$getVidIntro['vi_url'];
		}else if($getVidIntro['vi_type']==2){
			if (preg_match('%^https?://[^\s]+$%',$getVidIntro['vi_thumb'])) {
				$getAiImages			=	$getVidIntro['vi_thumb'];
			} else {
				$getAiImages			=	SITE_ROOT.'uploads/videos/'.$getVidIntro['vi_thumb'];
			}
			$vidUrl					 =	$objCommon->html2text($getVidIntro['vi_url']);
		}
		?>
		<div class="row">
			<div class="col-sm-12">
				<div class="video" id="thePlayer"></div>
			</div>
		</div>
		<script type="text/javascript">
			jwplayer("thePlayer").setup({
				flashplayer: "player.swf",
				image: "<?php echo $getAiImages?>",
				file: "<?php echo $vidUrl?>",
				skin: "<?php echo SITE_ROOT?>jw_player/six/six.xml",
				height: "430",
				width: "100%",
				autostart: true,
				stretching:"exactfit"
			});
		</script>
		<script>
	
	$('#videoIntroduction').on('hide.bs.modal', function () {
	jwplayer().stop();
	});
	</script>
	<?php
	}
	?>
<div id="previewVideoIntro"></div>
<?php
if($displayForm==1){
	?>
<form action="<?php echo SITE_ROOT.'access/add_new_video_introduction.php'?>" id="addnewVideoIntro" enctype="multipart/form-data" method="post">
<?php /*?><div class="row videoIntroAdd">
	<div class="col-sm-12 text-center">
	Upload New Video
	</div>
</div><?php */?>
<div class="video_intro_update">
    <div class="row videoIntroAdd">
        <div class="col-sm-4">
            <div class="form-group">
                <input type="file" name="video_introduction" id="video_introduction" class="form-control"  />
                <small>Max video size 100 mb</small>
            </div>
        </div>
        <div class="col-sm-1">
        	<p class="or">OR</p>
        </div>
        <div class="col-sm-5">
            <div class="form-group text-center">
                <input type="text" name="video_intro_youtube" id="video_intro_youtube" class="form-control" placeholder="Youtube Link Here" />
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group text-center has-spinner" >
                <input type="button" name="upload_video_intro" id="upload_video_intro" value="Upload" > &nbsp;&nbsp;<span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span>
            </div>
        </div>
    </div>
</div>
</form> 
<?php
	}
}
?>
<script>
$('#upload_video_intro').on('click', function(e){
	if($("#video_intro_youtube").val() != '' || $("#video_introduction").val() != ''){
		$(this).parent().addClass('active');
		var that	=	this;
		$('#upload_video_intro').attr('disabled', 'disabled');
		$("#addnewVideoIntro").ajaxForm(
				{
					target: '#previewVideoIntro',
					success:successCall
				}).submit();
		
		}
	e.preventDefault();
});
function successCall(){
	$(".has-spinner").removeClass('active');
	$("#video_intro_youtube").val('');
	$("#video_introduction").val('');
	$(".modalDisplayIntoduction").load('<?php echo SITE_ROOT?>widget/display_video_introduction.php');
}
</script>