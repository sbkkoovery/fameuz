<script type="text/javascript" src="<?php echo SITE_ROOT?>js/ajax_nav.js"></script>
<script src="<?php echo SITE_ROOT?>jw_player/jwplayer.js"></script>
<div id="myElement"></div>
<script>
    jwplayer("myElement").setup({
        file: "http://example.com/uploads/myAudio.m4a",
		  'playlist': [{
            'file': '<?php echo SITE_ROOT?>list/1.mp3',
            'title': 'The first video'
        },{
            'file': '<?php echo SITE_ROOT?>list/2.mp3',
            'title': 'The second video'
        }],
        repeat: 'list',
        width: 1000,
        height: 30
    });
</script>