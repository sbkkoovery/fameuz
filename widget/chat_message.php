<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/message.php");
$objCommon				   			  =	new common();
$objMsg								 =	new message();
$search								 =	$objCommon->esc($_GET['search']);
$count_per_page						 =	20;
$currentPage							=	($_GET['page'])?$objCommon->esc($_GET['page']):1;
if($search){
	$searchCat		   				  =	" AND (user.first_name LIKE '%".$search."%' OR user.last_name LIKE '%".$search."%' OR user.display_name LIKE '%".$search."%' OR user.email LIKE '%".$search."%')";
}else{
	$searchCat		   				  =	" ";
}
$sql_all_messages					   =	"SELECT tab.* FROM (SELECT msg.*,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,case when (TIMESTAMPDIFF(MINUTE,chat.ucs_last_seen,'".date("Y-m-d H:i:s")."') >15 or chat.ucs_status=0) then '0' else '1' END AS chatStatus
FROM message AS msg 
LEFT JOIN users AS user ON (msg.msg_from = user.user_id AND msg.msg_to=".$_SESSION['userId']." AND msg.msg_trash_to = 1 ".$searchCat.") OR (msg.msg_to= user.user_id AND msg.msg_from=".$_SESSION['userId']." AND msg.msg_trash_from = 1 ".$searchCat.")
LEFT JOIN user_chat_status AS chat ON user.user_id = chat.user_id 
LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
LEFT JOIN user_profile_image AS profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
WHERE (msg.msg_from =".$_SESSION['userId']." OR msg.msg_to = ".$_SESSION['userId']." )   ORDER BY msg.msg_created_date DESC ) AS tab WHERE tab.user_id !='' GROUP BY tab.user_id ORDER BY msg_created_date desc";
$totalCount							 =	$objMsg->countRows($sql_all_messages);
$totalPages							 =	ceil(($totalCount/$count_per_page));
$orderFrom							  =	$count_per_page*($currentPage-1);
$orderTo								=	$count_per_page*$currentPage;
$sql_all_messages				   	  .=   " LIMIT ".$orderFrom.",".$count_per_page;
$myAllMsg							   =	$objMsg->listQuery($sql_all_messages);			
?>
<div class="inbox_msges">
<?php
if(count($myAllMsg) >0){
	foreach($myAllMsg as $allMsg){
		if($allMsg['msg_from'] !=	$_SESSION['userId']){
			$msgFromOther	   =	'<i class="fa fa-reply"></i>';
			$readStatus		 =	($allMsg['msg_read_status']==0)?'':'read_messages';
		}else{
			$msgFromOther	   =	'';
			$readStatus		 =	'read_messages';
		}
?>
	<div class="new_msg_unread <?php echo $readStatus?>">
		<div class="checkBox_delete">
			<input class="check"  type="checkbox" name="msgId" value="<?php echo $allMsg['user_id']?>" />
		</div>
		<div class="sender_img">
			<a href="<?php echo SITE_ROOT.'user/'.$objCommon->html2text($allMsg['usl_fameuz']).'/chat-messages'?>"><img class="img-responsive" src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($allMsg['upi_img_url'])?$allMsg['upi_img_url']:'profile_pic.jpg'?>" /></a>
		</div>
		<div class="user_info_name_msg">
			<h4><a href="<?php echo SITE_ROOT.'user/'.$objCommon->html2text($allMsg['usl_fameuz']).'/chat-messages'?>" class="colorFameuz"><?php echo $objCommon->displayName($allMsg)?></a></h4>
		</div>
		<div class="message_stratz sentItemz">
			<p><a href="<?php echo SITE_ROOT.'user/'.$objCommon->html2text($allMsg['usl_fameuz']).'/chat-messages'?>" class="colorFameuz"><?php echo $msgFromOther?><?php echo $objCommon->limitWords($allMsg['msg_descr'],100)?></a></p>
		</div>
		<div class="date_msg">
			<p><?php echo date("d/m/Y",strtotime($allMsg['msg_created_date']))?></p>
		</div>
	</div>
	<?php /*?><div class="new_msg_unread">
		<div class="checkBox_delete">
			<input class="check" type="checkbox" />
		</div>
		<div class="sender_img">
			<img src="<?php echo SITE_ROOT?>uploads/albums/2015-12-28/thumb/1451298692_2_4951.jpg" />
		</div>
		<div class="user_info_name_msg">
			<h4>Olesya Leuenberger </h4>
		</div>
		<div class="message_stratz recivedItemz">
			<p>Wow</p>
		</div>
		<div class="date_msg">
			<p>04/12/2016</p>
		</div>
	</div>
	<div class="new_msg_unread read_messages">
		<div class="checkBox_delete">
			<input  class="check" type="checkbox" />
		</div>
		<div class="sender_img">
			<img src="<?php echo SITE_ROOT?>uploads/profile_images/2015-03-16/1426512866_2.jpg" />
		</div>
		<div class="user_info_name_msg">
			<h4> Sandeep Nambiar</h4>
		</div>
		<div class="message_stratz">
			<p>I feel beeter..em ohky</p>
		</div>
		<div class="date_msg">
			<p>01/12/2016</p>
		</div>
	</div>
	<div class="new_msg_unread read_messages">
		<div class="checkBox_delete">
			<input class="check"  type="checkbox" />
		</div>
		<div class="sender_img">
			<img src="<?php echo SITE_ROOT?>uploads/albums/2015-07-25/1437812637_8_0.jpg" />
		</div>
		<div class="user_info_name_msg">
			<h4> Aneena Padiken</h4>
		</div>
		<div class="message_stratz">
			<p>Are you there? gonr?? :(</p>
		</div>
		<div class="date_msg">
			<p>31/12/2015</p>
		</div>
	</div><?php */?>
<?php 
	}
	?>
	<div class="new_msg_unread">
		<?php 
		$orderToStr	   =	($orderTo >$totalCount)?$totalCount:$orderTo;
		echo ($orderFrom+1)."-".$orderToStr." of ".$totalCount;
		$prev			=	($currentPage>1)?($currentPage-1):1;
		$next			=	($totalPages > $currentPage)?($currentPage+1):$totalPages;
		if($totalPages >1){
		?>
		<span class="more_optn">
			<a href="javascript:;" onclick="doShowMsgList('<?php echo $prev?>')" <?php echo ($currentPage==1)?'class="colorWhite"':''?>><i class="fa fa-chevron-left"></i></a>
			<a href="javascript:;" onclick="doShowMsgList('<?php echo $next?>')" <?php echo ($currentPage==$totalPages)?'class="colorWhite"':''?>><i class="fa fa-chevron-right"></i></a>
		</span>
	</div>
	<?php
		}
}else{
	echo '<p style="padding: 20px;">No messages found...</p>';
}
?>
</div>