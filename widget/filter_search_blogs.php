<?php
$getFilterCategory			=	$objUsers->listQuery("SELECT bc_id,bc_title FROM blog_category ORDER BY bc_order");
?>
<form method="get" action="<?php echo SITE_ROOT?>search">
	<div class="row serach_filter">
		<div class="col-sm-12">
				<div class="form-group upload-help">
				<select class="form-control" name="filter_category" id="filter_category">
					<option value="">Select Category</option>
					<?php
					foreach($getFilterCategory as $allFilterCategory){
					?>
					<option value="<?php echo $allFilterCategory['bc_id']?>" <?php echo ($allFilterCategory['bc_id'] == $_GET['filter_category'])?'selected':''?>><?php echo $objCommon->html2text($allFilterCategory['bc_title'])?></option>
					<?php
					}
					?>
				</select>
				</div>
			</div>
		<div class="col-sm-12">
				<div class="form-group">
				<input type="text" class="form-control checkme"  name="filter_keywords" id="filter_keywords" placeholder="Type Keywords" value="<?php echo $_GET['filter_keywords']?>">
				</div>
			</div>
		<div class="col-sm-12">
			  <input type="hidden" name="cat" value="blogs" />
			  <button type="submit" class="btn btn-default">Submit</button>
			   <a href="javascript:;" class="clear_all pull-right">Clear All</a>
			</div>
	</div>
</form>
<script type="application/javascript">
$(".clear_all").on("click",function(){
	$("#filter_category").val('');
	$("#filter_keywords").val('');	
});
</script>