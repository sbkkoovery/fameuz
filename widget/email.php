<link href="<?php echo SITE_ROOT?>css/email.css" rel="stylesheet" type="text/css" />
<div class="modal fade mail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Send a Message</h4>
      </div>
      <div class="modal-body">
        <form>
            <div class="form-group">
                <label>To (Enter recipient emails, seperated by commas)</label>
                <input type="email" class="form-control" placeholder="Enter email address">
                <label class="help">
                	Help?
                    <div class="pop_over_help">
                    	<p>Email Should be writtern as follows</p>
                    	<p>eg : (fameuz@fameuz.com, fameuz@gmail.com)</p>
                    </div>
                </label>
            </div>
            <div class="form-group">
                <label>Subject</label>
                <input type="text" class="form-control" placeholder="Subject">
            </div>
            <div class="form-group">
                <label>Photos</label>
                <div class="photos_email">
                	<div class="max_width_photos">
                        <img src="http://localhost:8888/fameuz/uploads/albums/2015-09-27/thumb/1443339948_2_3.jpg" />
                        <img src="http://localhost:8888/fameuz/uploads/albums/2015-09-27/thumb/1443339928_2_2.jpg" />
                        <img src="http://localhost:8888/fameuz/uploads/albums/2015-09-27/thumb/1443339897_2_0.jpg" /><?php /*?>
                        <img src="http://localhost:8888/fameuz/uploads/albums/2015-09-27/thumb/1443339969_2_4.jpg" />
                        <img src="http://localhost:8888/fameuz/uploads/albums/2015-09-27/thumb/1443339918_2_1.jpg" /><?php */?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label >Message</label>
                <textarea class="form-control" rows="5" placeholder="Message"></textarea>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary">Sent</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(function () {
		var popDiv	=	$('.pop_over_help');
	  $('.help').click(function(e) {
        popDiv.toggle();
	  });
	});
</script>