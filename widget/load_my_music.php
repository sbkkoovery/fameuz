<?php
@session_start();
include_once("../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/music.php");
$objCommon				   =	new common();
$objMusic					=	new music();
$perPage 				   	 = 	20;
$page 					    = 	1;
if(!empty($_GET["page"])) {
	$page 					=	$_GET["page"];
}
$sqlMusicTop			 	 =	"SELECT music.music_id,music.music_title,music.music_descr,music.music_artist,music.music_thumb,music.music_url ,music.music_created
										FROM music
										WHERE music.user_id=".$_SESSION['userId']." ORDER BY music_created DESC";
$start 							=	($page-1)*$perPage;
if($start < 0) { $start = 0; }
$queryMusicTop 				 	= 	$sqlMusicTop . " limit " . $start . "," . $perPage; 
if(empty($_GET["rowcount"])) {
	$_GET["rowcount"] 			 =	$countLoadMusic	=	$objMusic->countRows($sqlMusicTop);
}
$pages  							=	ceil($_GET["rowcount"]/$perPage);
$getTopMusic				   	  =	$objMusic->listQuery($queryMusicTop);
$countTopMusic			   		=	count($getTopMusic);
if($start < $countLoadMusic){
if($countTopMusic >0){
	echo '<input type="hidden" class="pagenum" value="' . $page . '" /><input type="hidden" class="total-page" value="'.$pages.'" /><input type="hidden" id="total-count" value="'.$countLoadMusic.'" />';
foreach($getTopMusic as $keyTopMusic=>$allTopMusic){
	if(($keyTopMusic+1)%2==1){
		echo '<div class="albums c"><div class="row">';
	}
	$musicThumb				   =	($allTopMusic['music_thumb'])?$allTopMusic['music_thumb']:'audio-thump.jpg';
	$musicCreated				 =	date(" F d, Y ",strtotime($allTopMusic['music_created']));
?>
	<div class="col-sm-6 col-lg-sp-5-alt <?php echo (($keyTopMusic+1)%2==0)?'pull-right':''?>">
	<div class="album c ">
		<div class="row">
			<div class="col-sm-7 alt-padding-r-alt">
				<div class="desc ">
					<h6><?php echo $objCommon->html2text($allTopMusic['music_title']);?></h6>
					<p><?php echo $objCommon->limitWords($allTopMusic['music_descr'],150);?></p>
						<div class="player"> 
							<a href="javascript:;" class="list right editPopup" data-musicid="<?php echo $allTopMusic['music_id']?>" data-toggle="modal" data-target="#myModal">
								<span class="hidden-sm hidden-md">Edit</span>
							</a>
						</div>
					</div>
				</div>
		<div class="col-sm-5 alt-padding-l-alt"><div class="thumb_audio"><img src="<?php echo SITE_ROOT_AWS_MUSIC.'uploads/music/'.$musicThumb?>" alt="thump" /></div></div>
		</div>
	</div>
	</div>
<?php
 if(($keyTopMusic+1)%2==0 ){
	echo '</div></div>';
}else if(($keyTopMusic ==($countTopMusic-1))){
	echo '</div></div>';
}
}
}else{
	echo '<p>No music found...</p>';
}
}
?>
<script>
$(".album").on("mouseover",function(){
	$(this).find(".player").show();
});
$(".album").on("mouseout",function(){
	$(".player").hide();
});
$(".editPopup").on("click",function(){
	var that			=	this;
	music_id			=	$(that).data("musicid");
	if(music_id){
		$.ajax({
				url:'<?php echo SITE_ROOT.'ajax/my_music_edit.php'?>',
				data:{music_id:music_id},
				type:"POST",
				success: function(result){
					$(".editLoad").html(result);
				}
			});
	}
});
</script>
<div class="modal fade mymusic-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit My Music</h4>
      </div>
	  <div class="editLoad">
	  </div>
    </div>
  </div>
</div>