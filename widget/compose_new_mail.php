<?php
$getMyFriendsSql			=	"SELECT user.user_id,user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz
									FROM following AS follow
									LEFT JOIN users AS user ON (follow.follow_user1 = user.user_id and follow.follow_user1 !='".$_SESSION['userId']."') or (follow.follow_user2 = user.user_id and follow.follow_user2 !='".$_SESSION['userId']."')
									LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
									WHERE ((follow.follow_user2='".$_SESSION['userId']."') or (follow.follow_user1='".$_SESSION['userId']."')) AND user.email_validation=1 AND user.status=1";									
$getMyFriends			   =	$objUsers->listQuery($getMyFriendsSql);
?>
<link href="<?php echo SITE_ROOT?>filer-dragdropbox/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="<?php echo SITE_ROOT?>filer-dragdropbox/css/themes/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo SITE_ROOT?>filer-dragdropbox/js/jquery.filer.min.js?v=1.0.5"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT?>filer-dragdropbox/js/custom.js?v=1.0.5"></script>
<form action="<?php echo SITE_ROOT?>filer-dragdropbox/php/upload.php" method="post" enctype="multipart/form-data">
<div class="new_msg">
	<div class="to">
		<div class="title_to">To</div>
		<div class="input_fields">
			<select id="tokenize" multiple="multiple" class="tokenize-sample">
				<?php
				foreach($getMyFriends as $allMyFriends){
				?>
				<option value="<?php echo $allMyFriends['user_id']?>"><?php echo $objCommon->displayName($allMyFriends)?></option>
				<?php
				}
				?>
			</select>
		</div>
	</div>
	<div class="to">
		<div class="title_to">Subject</div>
		<div class="input_fields">
			<input type="text" placeholder="Type Subject" name="email_subject" />
		</div>
	</div>
	<div class="message_attachments">
		<div class="write_message replay_message">
			<div class="form-group">
				 <textarea class="wysihtml5 form-control" rows="6" name="email_descr" placeholder="Write Message" id="blog_descr"><?php echo $objCommon->html2text($getContent['blog_descr'])?></textarea>
			 </div>
		</div>
		<div class="attachments_field">
			<div class="attach_field">
				<input type="file" name="files[]" id="filer_input" multiple>
			</div>
		</div>
	</div>
	<div class="submitBtn">
			<button class="btn SendBtn">Send</button>
		</div>
</div>
</form>