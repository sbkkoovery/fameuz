<?php
$sql_worked_with_me		= "SELECT social.usl_fameuz,user.user_id,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,following.follow_user1,following.follow_user2,following.follow_status,cat.c_name,aImgs.ai_images,uPhotos.photo_url
FROM worked_together AS worked 
LEFT JOIN users AS user ON CASE WHEN worked.send_to=".$_SESSION['userId']." THEN worked.send_from = user.user_id ELSE worked.send_to = user.user_id END
LEFT JOIN user_social_links AS social ON user.user_id=social.user_id 
LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1 
LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 

LEFT JOIN user_photos AS uPhotos ON user.user_id = uPhotos.user_id AND uPhotos.photo_set_main=1
LEFT JOIN album_images AS aImgs	ON user.user_id = aImgs.user_id AND aImgs.ai_set_main=1


LEFT JOIN following ON (worked.send_from = following.follow_user1 OR worked.send_from = following.follow_user2) AND (following.follow_user1='".$_SESSION['userId']."' OR following.follow_user2='".$_SESSION['userId']."') 
WHERE worked.from_status=1 AND worked.to_status=1 AND (worked.send_to ='".$_SESSION['userId']."' OR worked.send_from='".$_SESSION['userId']."') AND user.user_id !='".$_SESSION['userId']."' AND user.email_validation=1 AND user.status=1 
GROUP BY user.user_id 
ORDER BY worked.worked_date DESC LIMIT 6"; 
$getWorkedWithMe		  =	$objUsers->listQuery($sql_worked_with_me);
if(count($getWorkedWithMe)>0){
?>
<div class="profile_border_box working_together ">
	<h2 class="heading with_border">
		<span class="worked_together">Worked Together</span>
		<span class="seemore worked_together"><a href="<?php echo SITE_ROOT.'user/'.$user_url.'/worked-with'?>">See More <i class="fa fa-play "></i></a></span>
	</h2>
	<div class="owl-worked">
	<?php 
	foreach($getWorkedWithMe as $allWorkedWithMe){
		$workedMeImage		   =	SITE_ROOT_AWS_IMAGES.'uploads/profile_images/'.(($allWorkedWithMe['upi_img_url'])?$allWorkedWithMe['upi_img_url']:"profile_pic.jpg");
		$workedMeName			=	$objCommon->displayName($allWorkedWithMe);
		if($allWorkedWithMe['follow_status']==2){ 
			$friendStatus =2; 
		}else if($allWorkedWithMe['follow_status']==1 && $allWorkedWithMe['follow_user1']==$_SESSION['userId']){ 
			$friendStatus =1; 
		}else { 
			$friendStatus=0; 
		}
		if($allWorkedWithMe['ai_images'] !=''){
			$coverImg			=	SITE_ROOT_AWS_IMAGES.'uploads/albums/'.$allWorkedWithMe['ai_images'];
		}else if($allWorkedWithMe['photo_url'] !=''){
			$coverImg			=	SITE_ROOT_AWS_IMAGES.'uploads/albums/'.$allWorkedWithMe['photo_url'];
		}else{
			$coverImg			=	SITE_ROOT_AWS_IMAGES.'images/cover.jpg';
		}
	?>
   <div class="worked_together_box">
	<div class="cover_image_profile">
		<a href="<?php echo SITE_ROOT.$objCommon->html2text($allWorkedWithMe['usl_fameuz'])?>" title="<?php echo $workedMeName?>"><img class="img-responsive" src="<?php echo $workedMeImage ?>" /></a>
		<div class="shadow_worked">
			<img src="<?php echo SITE_ROOT_AWS_IMAGES ?>images/shadow_worked.png" />
		</div>
	</div>
	<div class="profile_img_info">
		<div class="user_profile_info">
			<h5><?php echo $workedMeName;?></h5>
			<p class="proffesion username"><?php echo $objCommon->html2text($allWorkedWithMe['c_name']);?></p>
		</div>
		<div class="follow_status">
		<a href="<?php echo SITE_ROOT.$objCommon->html2text($allWorkedWithMe['usl_fameuz'])?>" class="friend">View</a>
			<?php /*?><?php
			if($friendStatus==2){
			?>
			<a href="javascript:;" class="friend"><i class="fa fa-star-o"></i> Friend</a>
			<?php
			}else if($friendStatus==1){
			?>
			<a href="javascript:;" class="following"><i class="fa fa-check"></i> Following</a>
			<?php
			}else if($friendStatus==0){
			?>
			<a href="javascript:;" class="follow has-spinner" data-friendid="<?php echo $objCommon->html2text($allFollowers['user_id'])?>">
				<span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span>&nbsp;&nbsp;
				<i class="fa fa-plus"></i> Follow
			</a>
			<?php
			}
			?><?php */?>
		</div>
	</div>
   </div>
   <?php }?>
   </div>
</div>
<?php
}
?>
<script>
$('.follow').click(function(e) {
	var friendid		=	$(this).data('friendid');
	$(this).addClass('active');
	$(this).find('.fa-plus').hide();
	that	=	this;
	$.get('<?php echo SITE_ROOT?>ajax/sent_friend_request.php',{"friendId":friendid},function(data){
	  setTimeout(function () {
			$(that).removeClass('active');
			$(that).parent().html('<a href="javascript:;" class="friend"><i class="fa fa-star-o"></i> Friend</a>');
			
		},3000);
	});
});
$(document).ready(function(e) {
	 $('.owl-worked').owlCarousel({
	loop:true,
	margin:10,
	nav:true,
	navText:['<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>'],
	responsive:{
		0:{
			items:3
		},
		600:{
			items:3
		},
		1000:{
			items:3
		}
	}
})
});

	</script>