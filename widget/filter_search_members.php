<?php
$getFilterCategory			=	$objUsers->listQuery("SELECT c_id,c_name FROM category ORDER BY c_name");
$getFilterDicipline		   =	$objUsers->listQuery("SELECT model_dis_id,model_dis_name FROM model_disciplines ORDER BY model_dis_name");
?>


<form method="get" action="<?php echo SITE_ROOT?>search">
	<div class="row serach_filter">
		<div class="col-sm-4 col-md-4 col-lg-3">
			<div class="checkbox">
			<label>
			  <input type="checkbox" name="filter_online" id="filter_online" value="1" <?php echo ($_GET['filter_online']==1)?'checked':''?>> Online
			</label>
			</div>
		</div>
		<div class="col-sm-8 col-md-8 col-lg-6">
			<div class="checkbox">
			<!--<label>
			  <input type="checkbox"> Top Members	
			</label>-->
			</div>
		</div>
		<div class="col-sm-12">
			<div class="form-group upload-help">
			<select class="form-control" name="filter_category" id="filter_category">
				<option value="">Select Category</option>
				<?php
				foreach($getFilterCategory as $allFilterCategory){
				?>
				<option value="<?php echo $allFilterCategory['c_id']?>" <?php echo ($_GET['filter_category']==$allFilterCategory['c_id'])?'selected':''?>><?php echo $objCommon->html2text($allFilterCategory['c_name'])?></option>
				<?php
				}
				?>
			</select>
			</div>
		</div>
		<div class="col-sm-12">
			<div class="form-group upload-help">
			<select class="form-control" name="filter_dicipline" id="filter_dicipline">
				<option value="">Select Discipline</option>
				<?php
				foreach($getFilterDicipline as $allFilterDicipline){
				?>
				<option value="<?php echo $allFilterDicipline['model_dis_id']?>" <?php echo ($_GET['filter_dicipline']==$allFilterDicipline['model_dis_id'])?'selected':''?>><?php echo $objCommon->html2text($allFilterDicipline['model_dis_name'])?></option>
				<?php
				}
				?>
			</select>
			</div>
		</div>
		<div class="col-sm-12">
			<div class="form-group upload-help">
			<select class="form-control" name="filter_gender" id="filter_gender">
				<option value="">Select Gender</option>
				<option value="1" <?php echo ($_GET['filter_gender']==1)?'selected':''?>>Male</option>
				<option value="2" <?php echo ($_GET['filter_gender']==2)?'selected':''?>>Female</option>
			</select>
			</div>
		</div>
		<div class="col-sm-12">
			<div class="form-group">
			<input type="text" class="form-control checkme" name="filter_location" id="filter_location"  placeholder="Type Location eg: New york,Washington" value="<?php echo $_GET['filter_location']?>">
			</div>
		</div>
		<div class="col-sm-12">
			<div class="form-group">
			<input type="text" class="form-control checkme"  name="filter_keywords" id="filter_keywords" placeholder="Type Keywords eg: model,actor" value="<?php echo $_GET['filter_keywords']?>">
			</div>
		</div>
		<div class="col-sm-12">
		  <input type="hidden" name="cat" value="members" />
		  <button type="submit" class="btn btn-default">Submit</button>
		  <a href="javascript:;" class="clear_all pull-right">Clear All</a>
		</div>
	</div>
</form>
<script type="application/javascript">
$(".clear_all").on("click",function(){
	$('#filter_online').attr('checked', false); 
	$("#filter_category").val('');
	$("#filter_dicipline").val('');
	$("#filter_gender").val('');
	$("#filter_location").val('');
	$("#filter_keywords").val('');	
});
</script>