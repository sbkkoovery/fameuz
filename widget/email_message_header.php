<div class="nav_items">
	<ul>
		<li <?php echo ($cssName=='message-inside' || $cssName=='message')?'class="activeTabMsg"':''?>><a href="<?php echo SITE_ROOT.'user/my-messages'?>" class="colorFameuz">Inbox</a></li>
		<li <?php echo ($cssName=='sent-message' || $cssName=='sent-message-inside')?'class="activeTabMsg"':''?>><a href="<?php echo SITE_ROOT.'user/my-sent-messages'?>" class="colorFameuz">Sent Messages</a></li>
		<li <?php echo ($cssName=='message_chat' || $cssName=='message_chat_inside')?'class="activeTabMsg"':''?>><a href="<?php echo SITE_ROOT.'user/my-chats'?>" class="colorFameuz">My Chats</a></li>
	</ul>
</div>
<?php
 echo $objCommon->displayMsg2();
if($cssName=='message-inside'){
?>
	<div class="nav_bar_message">
		<span class="more_optn">
			<a href="<?php echo SITE_ROOT.'access/delete_email_message_single.php?msgId='.$objCommon->url_encryption($msg_id)?>"><i class="fa fa-trash"></i></a>
		<!--	<a href="javascript:;" class="small_more">More <i class="fa fa-caret-down"></i></a>-->
		</span>
		<!--<div class="compose_msg text-right pull-right" style="margin-right:10px;">
			<a href="javascript:;" id="new_msg">New Message</a>
		</div>-->
	</div>
<?php
include_once(DIR_ROOT."widget/compose_new_mail.php");
}else if($cssName=='sent-message-inside'){
?>
	<div class="nav_bar_message">
		<span class="more_optn">
			<a href="<?php echo SITE_ROOT.'access/delete_email_message_single.php?msgId='.$objCommon->url_encryption($msg_id)?>"><i class="fa fa-trash"></i></a>
		</span>
	</div>
<?php
include_once(DIR_ROOT."widget/compose_new_mail.php");
}
else if($cssName=='message_chat_inside'){
?>
	<div class="nav_bar_message">
		<div class="search_bar_msg">
			<!--<input type="text" placeholder="Search Chats"/>
			<button><i class="fa fa-search"></i></button>-->
		</div>
		<div class="settings_msg">
			<a href="javascript:;" id="more_optn"><i class="fa fa-gear"></i></a>
			<a href="javascript:;"><i class="fa fa-refresh"></i></a><span class="drop_down_settings">
			<span class="options del_conv">Delete Conversation</span>
				<span class="options del_msg">Delete Messages</span>
				<span class="options blk_usr">Block this user</span>
			</span>
		</div>
		<div class="compose_msg text-right open_chatbox">
			<a href="javascript:;" id="new_msg" data-chatid="<?php echo $getFriendDetails['user_id']?>">Open in chatbox</a>
		</div>
	</div>
<?php
}else{
?>
	<div class="nav_bar_message">
		<div class="checkAll">
			<input type="checkbox" />
		</div>
		<div class="search_bar_msg">
			<input type="text" placeholder="Search Messages" id="searchMsg" name="searchMsg"/>
			<button class="searchMsgBtn"><i class="fa fa-search"></i></button>
		</div>
		<div class="settings_msg">
			<a href="javascript:;"><i class="fa fa-gear"></i></a>
			<a href="javascript:;" class="refreshMessageList"><i class="fa fa-refresh"></i></a>
		</div>
		<div class="compose_msg text-right">
			<a href="javascript:;" id="new_msg">New Message</a>
		</div>
	</div>
	<div class="options_onCheck">
		<span class="message_selected">
			<p><span class="count_msg">20</span> messages in this page is selected <a href="javascript:;" class="undo">undo</a></p>
		</span>
		<span class="more_optn">
			<a href="javascript:;" id="deleteThis"><i class="fa fa-trash"></i></a>
			<a href="javascript:;" class="small_more">More <i class="fa fa-caret-down"></i></a>
		</span>
		
	</div>
<?php
include_once(DIR_ROOT."widget/compose_new_mail.php");
}
?>


