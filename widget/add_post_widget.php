<form action="<?php echo SITE_ROOT.'ajax/add_post.php'?>" method="post" enctype="multipart/form-data">
<div class="working_comments" role="presentation">
	<div class="thumb"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($getUserDetails['upi_img_url'])?$getUserDetails['upi_img_url']:'profile_pic.jpg'?>" alt="thumb" /></div>
	<div class="comments">
		<div class="comments_arrow"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/comments_arrow.png" alt="comments_arrow" /></div>
		<textarea placeholder="What are you working on?" id="working" name="post_descr"></textarea>
		<div class="upload_section">
			<div class="post_list">
				<a href="#" class="option_btn" id="option_btn">Who can see it</a>
				<div class="option_box" id="option_box">
					<div class="option_list select">
						<div class="dis"><label><input type="checkbox" name="post_privacy1" value="1" /> Public</label></div>
					</div>
					<div class="option_list select">
						<div class="dis"><label><input type="checkbox" name="post_privacy2" value="1" /> Friends</label></div>
					</div>
					<div class="option_list select">
						<div class="dis"><label><input type="checkbox" name="post_privacy4" value="1" /> Only for me</label></div>
					</div>
				</div>
			</div>
			<div class="post_btns">
				<a href="javascript:void(0);">
					<i class="fa fa-camera"></i><span id="add_photo_str">Add a photo</span>
					<input type="file" name="post_img[]" multiple="multiple" class="add_post_img" id="add_post_img" />
				</a>
                <a href="javascript:;" id="close-post" />Cancel</a>
				<input type="submit" value="Post" name="add_post" />
			</div>
			<div class="clr"></div>
		</div>
	</div>
    <div class="clr"></div>
</div>
</form>
<?php
	include_once(DIR_ROOT.'js/include/add_post_widget.php');
?>