<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/games.php");
$objGames								=	new games();
$gameId								  =	$objCommon->esc($_GET['gameId']);
$gameIdExpl							  =	explode("-",$gameId);
$getGameId							   =	end($gameIdExpl);
$getGameAliasArr					     =	array_slice($gameIdExpl, 0, -1);
$getGameAlias						  	=	implode("-",$getGameAliasArr);
$getGame								 =	$objGames->getRowSql("SELECT games.*,avg(rating.gr_rating) AS rateAvg,play.game_users,cat.gc_name,group_concat(imgDescr.gdi_url) AS descrImgs
												FROM games 
												LEFT JOIN game_rating AS rating ON games.g_id = rating.g_id
												LEFT JOIN game_category AS cat ON games.gc_id = cat.gc_id
												LEFT JOIN (SELECT COUNT(gp_id) AS game_users,g_id FROM game_player GROUP BY g_id) AS play ON games.g_id = play.g_id
												LEFT JOIN game_descr_images AS imgDescr ON games.g_id = imgDescr.g_id
												WHERE games.g_id =".$getGameId." AND games.g_alias='".$getGameAlias."'");
$checkRated							  =	$objGames->getRowSql("SELECT gr_id FROM game_rating WHERE g_id =".$getGameId." AND user_id=".$_SESSION['userId']);
$checkLiked							  =	$objGames->getRowSql("SELECT gl_id FROM game_like WHERE g_id =".$getGameId." AND user_id=".$_SESSION['userId']." AND gl_status=1");
$likeCount							   =	$objGames->getRowSql("SELECT count(gl_id) AS likeCount FROM game_like WHERE g_id =".$getGameId." AND gl_status=1");
?>
<link href="<?php echo SITE_ROOT?>css/rating.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.form.js"></script>
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
  <div class="inner_top_border">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-sp-9">
      <div class="jobs-head">
        <div class="pagination_box_jobs pagination_box">
        <a href="<?php echo SITE_ROOT.'user/home'?>">Home <i class="fa fa-caret-right"></i></a>
        <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
        <a href="<?php echo SITE_ROOT?>games" class="active"> Games</a>
       <?php
        include_once(DIR_ROOT."widget/notification_head.php");
        ?>
        </div>
       </div>
        <div class="tab_white_most_wanted">
			<div class="main-banner-games"> 
				<div class="row">
					<div class="col-md-6">
						<h3><?php echo $objCommon->html2text($getGame['g_name']);?></h3>
					
					
						<?php
						if($checkRated['gr_id'] !=''){
							$avgRate				 =	$getGame['rateAvg']*20;
							echo '<div class="pull-left"><div class="rating_box"><div class="rating_yellow" style="width:'.round($avgRate).'%;"></div></div></div>';
						}else{
							?>
							<div class="rate-ex2-cnt pull-left">
								<div id="1" class="rate-btn-1 rate-btn"></div>
								<div id="2" class="rate-btn-2 rate-btn"></div>
								<div id="3" class="rate-btn-3 rate-btn"></div>
								<div id="4" class="rate-btn-4 rate-btn"></div>
								<div id="5" class="rate-btn-5 rate-btn"></div>
							</div>
							<div class="has-spinner  pull-left"><span class="spinner"><i class="fa fa-spinner fa-pulse"></i></span></div>
							<div class="show_rate_result pull-left"></div>
							<?php
						}
						?>
                    </div>
					<div class="col-md-3 text-right">
						<div class="request_sec">
							<div class="request"> <a data-toggle="modal" data-target=".likes"  class="likedPopUp" href="javascript:;"><i class="fa_invite"></i>Invite Friends</a> </div>
						</div>
					</div>
					<div class="col-md-3 text-right">
						<div class="share"> <a  href="javascript:;" data-userby="<?php echo $_SESSION['userId']?>" data-category="12" data-share="<?php echo $getGame['g_id']?>" class="share_btn"><i class="fa_share"></i>Share </a> </div>
						<div class="like">
						<?php
						if($checkLiked['gl_id'] !=''){
						?>
							<a href="javascript:;"  data-like="<?php echo $getGame['g_id']?>" class="like_btn liked-game"><i class="fa fa-heart"></i> <span class="text">Liked</span> </a>
						<?php
						}else{
							?>
							<a href="javascript:;"  data-like="<?php echo $getGame['g_id']?>" class="like_btn"><i class="fa fa-heart"></i> <span class="text">Like</span> </a>
							<?php
						}
						?>
						</div>
					</div>
				</div>  
				<div class="loadGame">
				<embed width="100%"  height="600" src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/games/file/'.$objCommon->html2text($getGame['g_file'])?>">
				</div>
					<div class="row shareComposite">
					<div class="col-md-12 text-right">
						<!-- Load Facebook SDK for JavaScript -->  
						<span class='st_facebook' displayText='Facebook'></span>
						<span class='st_googleplus' displayText='Google +'></span>
						<span class='st_twitter' displayText='Tweet'></span>
						<span class='st_linkedin' displayText='LinkedIn'></span>
						<span class='st_pinterest' displayText='Pinterest'></span>
						<span class='st_email' displayText='Email'></span>
						<script type="text/javascript">var switchTo5x=true;</script>
						<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
						<script type="text/javascript">stLight.options({publisher: "318b9a8f-b81c-4b92-89da-cb0ad4ccf6d6", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
						<!---------------------------------->
					</div>
				</div>
			</div>
		</div>
      </div>
       <?php
	  include_once(DIR_ROOT."widget/games_right_widget.php");
	  ?>
    </div>
    </div>
  </div>
</div>
<div class="modal fade" id="newone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content showGamePop">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="game-details_sec">
        	<div class="game-img">
                <img src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/games/img/'.$getGame['g_img']?>" />
            </div>
            <div class="game-desc">
            	<h4><?php echo $objCommon->html2text($getGame['g_name']);?> 
					<span class="like_share pull-right">
						<div class="like">
						<?php
						if($checkLiked['gl_id'] !=''){
						?>
							<a href="javascript:;"  data-like="<?php echo $getGame['g_id']?>" class="like_btn liked-game"><i class="fa fa-heart"></i> <span class="text">Liked</span> </a>
						<?php
						}else{
							?>
							<a href="javascript:;"  data-like="<?php echo $getGame['g_id']?>" class="like_btn"><i class="fa fa-heart"></i> <span class="text">Like</span> </a>
							<?php
						}
						?>
						</div>
					<!--<a href="#"><i class="fa fa-share-alt"></i>Share</a>-->
					</span>
				</h4>
                <p><span><i class="fa fa-thumbs-up"></i><?php echo number_format($likeCount['likeCount'])?> Likes</span><span><i class="fa fa-user"></i><?php echo number_format($getGame['game_users'])?> Players</span></p>
                <h5><?php echo $objCommon->html2text($getGame['gc_name'])?></h5>
                <p><?php echo $objCommon->html2text($getGame['g_descr'])?></p>
            </div>
            <div class="clearfix"></div>
        </div>
      </div>
      <div class="modal-body">
      	<div class="screenshots_container">
		<?php
		if($getGame['descrImgs']){
			$explDescrImgs			=	explode(",",$getGame['descrImgs']);
			$explDescrImgs			=	array_filter($explDescrImgs);
			?>
        	<div class="img-scroll-sec">
				
				
            	<ul>
					<?php
					foreach($explDescrImgs as $allDescrImgs){
					?>
                	<li><img class="img-responsive" src="<?php echo SITE_ROOT_AWS_IMAGES.'uploads/games/descr_images/'.$allDescrImgs?>" /></li>
					<?php
					}
					?>
                </ul>
            </div>
			<?php
			}
			?>
        </div>
        <div class="play_now_game">
        	<a href="#"><img src="<?php echo SITE_ROOT_AWS_IMAGES ?>images/gameicon.png" />Play Now</a>
            <p>By clicking on 	"Play Now" above, <span>Born 2 Rac</span>e will receive the following info: your public profile.</p>
            <div class="privacy_reviws">
            	<a href="#"><img src="<?php echo SITE_ROOT_AWS_IMAGES ?>images/pencil_green.png" />Review the info you provide</a>
                <p>By proceeding, you agree to Born 2 Race's <a href="#"> Privacy Policy</a>.</p>
                <p><img src="<?php echo SITE_ROOT_AWS_IMAGES ?>images/lock.png" /> This does not let app post to facebook</p>
                <div class="bottom-a">
                    <a href="#"><img src="<?php echo SITE_ROOT_AWS_IMAGES ?>images/block.png" />Block</a>
                    <a href="#"><img src="<?php echo SITE_ROOT_AWS_IMAGES ?>images/caustion.png" />Report Problem</a>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(window).load(function(e) {
	$('.fixer').fixedSidebar();
});
$(document).ready(function() {
	$(".loadGame").children('embed').hide();
	$('#newone').modal('show');
	$(".play_now_game a,.modal").click(function(){
		$('#newone').modal('hide');
		$(".loadGame").children('embed').show();
	});
	$(".screenshots_container").perfectScrollbar();
	$.ajax({
		url:'<?php echo SITE_ROOT?>ajax/game_player.php',
		data:{video_id:'<?php echo $getGameId?>'},
		type:"post",
		success:function(data){
		}
	});
	 $('.rate-btn').hover(function(){
		$('.rate-btn').removeClass('rate-btn-hover');
		var therate = $(this).attr('id');
		for (var i = therate; i >= 0; i--) {
			$('.rate-btn-'+i).addClass('rate-btn-hover');
		};
	});
	$('.rate-btn').click(function(){
		$(".has-spinner").addClass("active");
		var therate = $(this).attr('id');
		if(therate){
			$.ajax({
				url:"<?php echo SITE_ROOT?>ajax/game_rate.php",
				data:{game_id:<?php echo $getGame['g_id']?>,rating:therate},
				type:"POST",
				success: function(data){
					$(".has-spinner").hide();
					$(".rate-ex2-cnt").hide();
					$(".show_rate_result").html(data).show();
					}
				});
		}
	 });
	$('[data-dismiss="modal"]').click(function(){
		window.location.href = '<?php echo SITE_ROOT?>games';
	}).find('.showGamePop').on('click', function (e) {
		e.stopPropagation();
	});
});
$(".like").on("click",".like_btn",function(){
	var game_id			=	$(this).data("like");
	var that			   =	$(this);
	if(game_id){
		$.ajax({
			url:"<?php echo SITE_ROOT?>ajax/like_game.php",
			type:"POST",
			data:{game_id:game_id},
			success: function(data){
				$(that).parent().html(data);
			}
		});
	}
});
$(".likedPopUp").on("click",function(){
	$(".loadLikedUserName").load('<?php echo SITE_ROOT?>widget/game_invite_frinds.php?g_id=<?php echo $getGame['g_id']?>',function(){
	$(".modal-body").perfectScrollbar();
	});
});
$(".share").on("click",".share_btn",function(){
	var shareId			   =	$(this).data("share");
	var shareCat			  =	$(this).data("category");
	var noti_whome			=	$(this).data("userby");
	var that				  =	$(this);
	if(shareId != '' && shareCat != ''){
		$.ajax({
			url:'<?php echo SITE_ROOT?>ajax/share.php',
			data:{shareId:shareId,shareCat:shareCat,shareWhoseId:noti_whome},
			type:"POST",
			success: function(data4){
				$(that).html('<i class="fa_share"></i>'+data4);
				return false;
			}
		})
	}
});
</script> 
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/fixed_sidebar.js"></script>
<?php
include_once(DIR_ROOT."widget/like_view_popup.php");
include_once(DIR_ROOT."includes/footer.php");
?>
  