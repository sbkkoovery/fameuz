<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Roboto:500,900,100,300,700,400' rel='stylesheet' type='text/css'>
<title>Composite</title>
<style>
	body{
		font-family: 'Roboto', sans-serif;
	}
	.main-container{
		width:976px;
		height:690px;
		overflow:hidden;
		margin:0px auto;
		background:#f4f4f4;
	}
	.left-section, .right-section{
		width:450.5px;
		height:650px;
		overflow:hidden;
		float:left;
		padding:15px;
		border-top:5px solid #4e4f53;
		border-bottom:5px solid #4e4f53;
	}
	.left-section{
		border-left:5px solid #4e4f53;
		border-right:2.5px solid #4e4f53;
	}
	.right-section{
		background:#fff;
		border-right:5px solid #4e4f53;
		border-left:2.5px solid #4e4f53;
	}
	.image-sections{
		width:100%;
		height:650px;
		overflow:hidden;
		position:relative;
	}
	.image-sections img{
		width:100%;
		min-height:100%;
	}
	.top-right-section, .right-bottom-section{
		width:100%;
		height:317.5px;
	}
	.top-right-section{
		padding-bottom:7.5px
	}
	.right-bottom-section{
		padding-top:7.5px
	}
	.top-left, .top-right{
		width:217.75px;
		height:317.5px;
		float:left;
		overflow:hidden;
	}
	.top-left{
		padding-right:7.5px;
	}
	.top-right{
		padding-left:7.5px;
	}
	.top-left img, .top-right img{
		width:100%;
		min-height:100%;
	}
	.log-section{
		width:102px;
		height:45px;
		margin:0 auto;
	}
	.log-section img{
		width:100% !important;
		height:100% !important;
	}
	.info-user-section ul{
		
	}
	.info-user-section ul li{
		list-style:none;
		font-size:12px;
		margin-bottom:5px;
	}
	.info-user-section ul li span{
		font-weight:bold;
	}
	.shadow{
		width:100%;
		height:300px;
		background:url('images/shadow-compo.png') no-repeat center bottom;
		background-size:100% auto;
		position:absolute;
		bottom:0;
		left:0;
	}
	.user-name{
		position:relative;
		margin-top:-70px;
		text-align:center;
		color:#fff;
		z-index:10;
	}
	.clearfix{
		float:none;
		clear:both;
	}
</style>
</head>

<body>
	<div class="main-container">
    	<div class="left-section">
        	<div class="image-sections">
            	<img src="images/2.jpg" />
                <p class="user-name">Olesya Luenburger</p>
                <div class="shadow"></div>
            </div>
        </div>
        <div class="right-section">
        	<div class="top-right-section">
            	<div class="top-left">
                	<img src="images/4.jpg" />
                </div>
                <div class="top-right">
                	<img src="images/3.jpg" />
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="right-bottom-section">
            	<div class="top-left">
                	<div class="info-user-section">
                    	<div class="log-section">
                        	<img src="images/log-compo.png" />
                        </div>
                        <div class="info-user-section">
                        	<ul>
                            	<li>Height : <span> 108cm</span></li>
                                <li>Bust : <span> 84cm / 33.1"</span></li>
                                <li>Eyes : <span> Brown</span></li>
                                <li>Hair : <span> Black</span></li>
                                <li>Waist : <span> 97cm / 38.3"</span></li>
                                <li>Hips : <span> 95cm / 37.5"</span></li>
                                <li>Dress : <span> 40 EU, 13 UK, 14US</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="top-right">
                	<img src="images/1.jpg" />
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</body>
</html>