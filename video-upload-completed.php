<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/videos.php");
$objVideos				=	new videos();
$vidId					=	$objCommon->esc($_GET['id']);
$getVideoDetails		  =	$objVideos->getRowSql("SELECT vid.video_id,vid.video_url,vid.video_encr_id,vid.video_thumb,vid.video_duration,vid.video_type,vid.video_title,vid.video_visits,vid.video_created,vid.video_privacy,vid.video_tags,vid.video_descr
										FROM videos AS vid
										LEFT JOIN users AS user ON  vid.user_id = user.user_id
										WHERE   user.status=1 AND user.email_validation =1 AND vid.user_id='".$_SESSION['userId']."' AND vid.video_encr_id='".$vidId."'");

if($getVideoDetails['video_url'] ==''){
	header("location:".SITE_ROOT.'video/upload');
}
?>
<script type="text/javascript" src="<?php echo SITE_ROOT?>js/jquery.form.new.js"></script>
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
    <div class="inner_top_border">
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
          <div class="row">
            <div class="col-sm-12">
              <div class="pagination_box"> <a href="#">Home <i class="fa fa-caret-right"></i></a> <a href="#" class="active"> My profile </a>
                <?php
				include_once(DIR_ROOT."widget/notification_head.php");
				?>
              </div>
            </div>
          </div>
          <div class="main-background-white">
            <div class="row">
              <div class="col-sm-12">
                <div class="upload-sec">
				<?php echo $objCommon->displayMsg();?>
                  <div class="upload-warning">
                    <div class="upload-warning-head">
                      <p>Please follow this rules :-</p>
                    </div>
                    <div class="upload-warning-points">
                      <ol>
                        <li> Lorem Ipsum is printing and typesetting industry. </li>
                        <li>Lorem Ipsum is simply dummy text of typesetting industry. </li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting ind. </li>
                        <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </li>
                      </ol>
                    </div>
                    <div class="upload-warning-footer">
                      <p>Still have questions? Read the full <a href="#"><strong>Fameuz Guidelines.</strong></a> </p>
                      <img class="down-arrow" src="<?php echo SITE_ROOT?>images/down-arrow.png" /> </div>
                  </div>
                </div>
              </div>
            </div>
			<div class="uploading-section" id="uploading-section">
			<?php
			include_once(DIR_ROOT."includes/upload_video_after_completed.php");
			?>
			</div>
          </div>
        </div>
         <?php
		include_once(DIR_ROOT."widget/right_static_ad_bar.php");
		?>
      </div>
    </div>
  </div>
</div>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
