<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/pagination-class-front.php");
$num_results_per_page		= 	20;
$num_page_links_per_page 	 = 	5;
$pg_param 					= 	"";
$pagesection				 = 	'';
$getMyJobsSql				=	"SELECT promo.* FROM promotion AS promo WHERE promo.user_id=".$_SESSION['userId']." ORDER BY promo.promo_created_date DESC";
pagination($getMyJobsSql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$getMyJobs			   	   =	$objUsers->listQuery($pg_result);
?>
<link href="<?php echo SITE_ROOT?>css/my-booking.css" rel="stylesheet" type="text/css">
<link href="<?php echo SITE_ROOT?>css/pagination.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.css" />
<script src="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.js"></script>
    <div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="user_profile">
						<?php echo $objCommon->checkEmailverification();?>
						<div class="content">
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                                    <div class="pagination_box">
                                		<a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
                                        <a href="<?php echo SITE_ROOT.$getUserDetails['usl_fameuz']?>"> My profile </a><i class="fa fa-caret-right"></i></a>
                                         <a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                        <a href="javascript:;" class="active">My Promotions  </a>
									   <?php
                                        include_once(DIR_ROOT."widget/notification_head.php");
                                        ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-8 col-lg-sp-3">
                                    <div class="sidebar no-border">
                                        <div class="search_box">
                                            <form action="<?php echo SITE_ROOT.'user/search-result'?>" method="get"><input type="text" placeholder="Search"  name="keywordSearch" /></form></div>
                                        </div>
                                </div>
                            </div>
                                  <div class="row">
                                    <div class="col-lg-sp-2">
                            <?php
							include_once(DIR_ROOT."includes/profile_left.php");
							?>
							</div>
                            <div class="col-lg-sp-10">
                            <div class="tab_white_search">
                                <div class="section_search">
                                    <ul>
                                        <li class="visitor_head_box"><h5>My Promotions</h5><span class="point-it"></span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="sidebar">
                                <div class="clr"></div>
                                <div class="booking">
									<?php echo $objCommon->displayMsg();?>
                                    <div class="tab">
                                    	<ul class="c">
                                        	<li class="active"><a href="javascript:;">All Promotions</a></li>
                                        </ul>
                                    </div>
                                    <div class="table">
                                    	<div class="table_hed c">
                                            <div class="hash left17">&nbsp;</div>
                                            <div class="name left30">Title</div>
											<div class="name left13">Start Date</div>
											<div class="name left13">End Date</div>
                                            <div class="e_date left13"><i class="fa fa-calendar-o"></i> Created Date</div>
                                            <div class="status left10"><i class="fa fa-info"></i> Status</div>
                                        </div>
										<?php
										 if(count($getMyJobs)>0){
											 $page_id			 	=	($_GET['page_id'])? ($num_results_per_page*($_GET['page_id']-1)):'';
											 foreach($getMyJobs as $keyJobs=>$allJobs){
												$contentTitle=$contentImg=	'';
											 	$siNo				=	$page_id+($keyJobs+1);
												$blog_status	 	 =	($allJobs['promo_admin_status']==1 && $allJobs['promo_status']==1)?'Enable':'Disable';;
												$todayDate		   =	date("Y-m-d");
												$promoStart		  =	($allJobs['promo_start_date'])?date("Y-m-d",strtotime($allJobs['promo_start_date'])):'&nbsp;';
												$promoEnd			=	($allJobs['promo_end_date'])?date("Y-m-d",strtotime($allJobs['promo_end_date'])):'&nbsp;';
												$getLocations		=	$objUsers->getRowSql("SELECT group_concat(country_name) AS allCounties FROM country WHERE country_id IN (".trim($allJobs['promo_country'],",").")");
												$getCategories	   =	$objUsers->getRowSql("SELECT group_concat(c_name) AS allCategories FROM category WHERE c_id IN (".trim($allJobs['promo_category'],",").")");
												if($allJobs['promo_type']=='video'){
													$getVideoDetails				   =	$objUsers->getRowSql("SELECT video_id,user_id,video_thumb,video_title FROM videos WHERE video_id='".$allJobs['promo_content_id']."'");
													$contentTitle					  =	$getVideoDetails['video_title'];
													$contentImg						=	SITE_ROOT_AWS_VIDEOS.'uploads/videos/'.$objCommon->html2text($getVideoDetails['video_thumb']);
												}else if($allJobs['promo_type']=='music'){
													$getMusicDetails				   =	$objUsers->getRowSql("SELECT music_id,user_id,music_thumb,music_title FROM music WHERE music_id='".$allJobs['promo_content_id']."'");
													$contentTitle					  =	$getMusicDetails['music_title'];
													$contentImg						=	SITE_ROOT_AWS_MUSIC.'uploads/music'.($getMusicDetails['music_thumb'])?$getMusicDetails['music_thumb']:'audio-thump.jpg';
												}
												?>
                                        <div class="t_data show<?php echo $objCommon->html2text($allJobs['promo_id']);?>">
                                        	<div class="data_hed">
                                            	<div class="hash left17">
                                                	<span class="count"><?php echo $siNo?></span>
                                                    <div class="details" data-booking="<?php echo $objCommon->html2text($allJobs['promo_id']);?>" >Details <span class="fa fa-angle-down"></span></div>
                                                </div>
                                                <div class="name left30 read"><?php echo $objCommon->html2text(ucfirst($allJobs['promo_type']));?> Promotion</div>
												<div class="e_date left13 read"><?php echo date("d M Y",strtotime($allJobs['promo_start_date']));?></div>
												<div class="e_date left13 read"><?php echo date("d M Y",strtotime($allJobs['promo_end_date']));?></div>
                                                <div class="e_date left13 read"><?php echo date("d M Y",strtotime($allJobs['promo_created_date']));?></div>
                                                <div class="status left10"><?php echo $blog_status?></div>
                                                <div class="clr"></div>
                                            </div>
                                            <div class="data_content">
                                            	<div class="main_content">
                                                	<h5><?php echo $objCommon->html2text(ucfirst($allJobs['promo_type']));?> Promotion</h5>
													<div class="left100">
														<div class="left70">
                                                    		<p><?php echo $objCommon->html2textarea($contentTitle);?></p>
														</div>
														<div class="left30 descrImage">
														<?php
														if($contentImg){
														?>
														<img src="<?php echo $contentImg?>" />
														<?php
														}
														?>
														</div>
													</div>
                                                    <h3><span>Details</span></h3>
                                                    <div class="w_details">
														<?php
														echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-sun-o"></i>Promotion Duration</div><div class="detail_colon">:</div><div class="detail_con">'.$objCommon->html2textarea($allJobs['promo_duration']).' Days</div> </div>';
														echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-map-marker"></i>Promotion Locations</div><div class="detail_colon">:</div><div class="detail_con">'.$getLocations['allCounties'].'</div> </div>';
														echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-tasks"></i>Promotion Categories</div><div class="detail_colon">:</div><div class="detail_con">'.$getCategories['allCategories'].'</div> </div>';				
														echo '<div class="detail_row c"><div class="detail_hed"><i class="fa fa-calendar"></i>Created Date</div><div class="detail_colon">:</div><div class="detail_con">'.date("Y-m-d",strtotime($allJobs['promo_created_date'])).'</div> </div>';
														
														?>
                                                    </div>
													<div class="duration">
                                                    	<h4>Duration</h4>
                                                        <div class="duration_dates">
                                                        	<div class="date round5">
                                                            	<div class="from">From</div>
                                                                <div class="day"><?php echo date('d',strtotime($promoStart));?></div>
                                                                <div class="month"><?php echo date('F Y',strtotime($promoStart));?></div>
                                                            </div>
                                                            <div class="date round5">
                                                            	<div class="from">To</div>
                                                                <div class="day"><?php echo date('d',strtotime($promoEnd));?></div>
                                                                <div class="month"><?php echo date('F Y',strtotime($promoEnd));?></div>
                                                            </div>
                                                        </div>
                                                    </div>
													<div class="clr"></div>
                                                </div>
                                                
                                                <div class="clr"></div>
                                            </div>
                                        </div>
										<?php
										}
										echo '<div class="paginationDiv">'.$pagination_output.'</div>';
										 }else{
											 echo '<p> No Results found... </p>';
										 }
										 ?>
                                    </div>
                                </div>
                            </div>
                            </div>
                            </div> 
                            <div class="clr"></div>                        
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
	$(document).ready(function(e) {
		var	showDiv		=	'<?php echo $show?>';
		if(showDiv){
			$(".show"+showDiv).toggleClass('active');
		}
		$('.details').click(function(e) {
		var t_data = $(this).parent().parent().parent();
		t_data.toggleClass('active');
		t_data.siblings().removeClass('active');
		return false;
		});
		$('.user_profile .sidebar .booking .t_data .data_hed .hash .detail_setting').click(function(e) {
			$(this).find('.setting_option').fadeToggle();
		});
	});
</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  