<?php
@session_start();
include_once("../../includes/site_root.php");
include_once(DIR_ROOT."class/common_class.php");
include_once(DIR_ROOT."class/email_message.php");
include_once(DIR_ROOT."class/email_message_users.php");
include_once(DIR_ROOT."class/email_message_attachments.php");
include('class.uploader.php');
$objCommon				   =	new common();
$objEmailMsg				 =	new email_message();
$objEmailMessageUser	  	 =	new email_message_users();
$objEmailMsgAttach		   =	new email_message_attachments();
$userId				   	  =	$_SESSION['userId'];
$emailto					 =	$_POST['emailto'];
if(count($emailto)>0 && $userId!='' && $_POST['email_subject'] !='' && $_POST['email_descr'] != ''){
	mysql_query("START TRANSACTION");
	$_POST['em_category']	   		      =	'email_message';
	$_POST['em_subject']	   		       =	$objCommon->esc($_POST['email_subject']);
	$_POST['em_message']	   			   =	$objCommon->esc($_POST['email_descr']);
	$_POST['em_createdon']	   		   	 =	date("Y-m-d H:i:s");
	$_POST['em_status']	   		   		=	1;
	$objEmailMsg->insert($_POST);
	$insertid							  =	$objEmailMsg->insertId();
	$_POST['emu_from']		   		 	 =	$userId;
	$_POST['em_id']	   		   	   	    =	$insertid;
	$_POST['emu_read_status']			  =	0;
	$_POST['emu_delete_from']			  =	1;
	$_POST['emu_delete_to']			  	=	1;
	foreach($emailto as $allEmailTo){
		
		/*for notification */
		$myDetails				   		   	   =	$objUsers->getRowSql("SELECT user.first_name,user.last_name,user.display_name,user.email,social.usl_fameuz FROM users AS user LEFT JOIN user_social_links AS social ON user.user_id=social.user_id WHERE user.status=1 AND user.user_id=".$userId);
		$displayName						  	 =	$objCommon->displayName($myDetails);
		$_POST['emu_to']	   		   	         =	$allEmailTo;
		$friend_id                               =    $_POST['emu_to'];
		$notiType								=	'messages';
		$notiImg								 =	'';
		$notiDescr   	 	 					   =	'<b>'.$displayName.'</a></b> has sent a new mail for you</b>.';
		$notiUrl  								 =	SITE_ROOT.'user/my-messages';
		
		$objCommon->pushNotification($friend_id,$userId,$notiType,$notiImg,$notiDescr,$notiUrl);
		$objEmailMessageUser->insert($_POST);
	}
	if($_FILES['files']['tmp_name'] !=''){
		$path			 				  =	'../../uploads/email_attachment/';
		$todayDate				   		 =	date('Y-m-d');
		if(!file_exists($path.$todayDate)){
			mkdir($path.$todayDate);
		}
		$path							  =	$path.$todayDate."/";
		$uploader 						  =	new Uploader();
		$data 							  =	$uploader->upload($_FILES['files'], array(
			'limit' => 15, //Maximum Limit of files. {null, Number}
			'maxSize' => 25, //Maximum Size of files {null, Number(in MB's)}
			'extensions' => null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
			'required' => false, //Minimum one file is required for upload {Boolean}
			'uploadDir' => $path, //Upload directory {String}
			'title' => array('auto', 10), //New file name {null, String, Array} *please read documentation in README.md
			'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
			'perms' => null, //Uploaded file permisions {null, Number}
			'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
			'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
			'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
			'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
			'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
			'onRemove' => 'onFilesRemoveCallback' //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
		));
		
		if($data['isComplete']){
			$files				 =	$data['data']['files'];
			foreach($files as $allFiles){
				$fileNameFull	  =	$allFiles;
				$fileNameFullExpl  =	explode('email_attachment/',$fileNameFull);
				$_POST['ema_file'] =	$fileNameFullExpl[1];
				$objEmailMsgAttach->insert($_POST);
			}
			//echo "<pre>";print_r($files);echo "</pre>";
		}
		
		if($data['hasErrors']){
			$errors 				=	$data['errors'];
			//print_r($errors);
		}
		
		function onFilesRemoveCallback($removed_files){
			foreach($removed_files as $key=>$value){
				$file = $path . $value;
				if(file_exists($file)){
					unlink($file);
				}
			}    
			return $removed_files;
		}
	}
	$objCommon->addMsg('Message has been sent',1);
	mysql_query("COMMIT");
}
header("location:".$_SERVER['HTTP_REFERER']);
exit;
?>
