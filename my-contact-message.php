<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
include_once(DIR_ROOT."class/pagination-class-front.php");
$book_status				 =	$objCommon->esc($_GET['book_status']);
$num_results_per_page		= 	20;
$num_page_links_per_page 	 = 	5;
$pg_param 					= 	"";
$pagesection				 = 	'';
$show						=	$objCommon->esc($_GET['show']);
$getMyJobsSql				=	"SELECT msg.*,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz FROM email_message AS msg 
									LEFT JOIN users AS user ON msg.em_sent_from = user.user_id 
									LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
									LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
									WHERE user.status=1 AND user.email_validation=1 AND msg.em_status=1  AND msg.em_sent_to=".$_SESSION['userId']." ORDER BY msg.em_createdon DESC";
pagination($getMyJobsSql, $num_results_per_page, $num_page_links_per_page, $pg_param,$pagesection);
$getMyJobs			   	   =	$objUsers->listQuery($pg_result);
?>
<link href="<?php echo SITE_ROOT?>css/my-booking.css" rel="stylesheet" type="text/css">
<link href="<?php echo SITE_ROOT?>css/pagination.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.css" />
<script src="<?php echo SITE_ROOT?>jquery.confirm/jquery.confirm.js"></script>
    <div class="inner_content_section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <div class="inner_top_border">
                    	<div class="user_profile">
						<?php echo $objCommon->checkEmailverification();?>
						<div class="content">
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
                                    <div class="pagination_box">
                                        <a href="<?php echo SITE_ROOT ?>user/home">Home <i class="fa fa-caret-right"></i></a>
										<a title="Back" href="javascript:history.back()" class="backBtnNew">Go Back <i class="fa fa-caret-right"></i></a>
                                        <a href="#" class="active">My Contact Messages  </a>
									   <?php
                                        include_once(DIR_ROOT."widget/notification_head.php");
                                        ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-8 col-lg-sp-3">
                                    <div class="sidebar no-border">
                                        <div class="search_box">
											<form action="<?php echo SITE_ROOT.'user/search-result'?>" method="get">
                                            <input type="text" placeholder="Search"  name="keywordSearch" /></form></div>
                                        </div>
                                </div>
                            </div>
                                  <div class="row">
                                    <div class="col-lg-sp-2">
                            <?php
							include_once(DIR_ROOT."includes/profile_left.php");
							?>
							</div>
                            <div class="col-lg-sp-10">
                            <div class="tab_white_search">
                                <div class="section_search">
                                    <ul>
                                        <li class="visitor_head_box"><h5>My Contact Messages</h5><span class="point-it"></span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="sidebar">
                                <div class="clr"></div>
                                <div class="booking">
									<?php echo $objCommon->displayMsg();?>
                                    <div class="tab">
                                    	<ul class="c">
                                        	<li class="active"><a href="javascript:;">All Messages</a></li>
                                        </ul>
                                    </div>
                                    <div class="table">
                                    	<div class="table_hed c">
                                            <div class="hash left17">&nbsp;</div>
                                            <div class="name left55">&nbsp;</div>
                                            <div class="e_date left15"><i class="fa fa-calendar-o"></i> Date</div>
                                            <div class="delete left8">&nbsp;</div>
                                        </div>
										<?php
										 if(count($getMyJobs)>0){
											 $page_id			 	=	($_GET['page_id'])? ($num_results_per_page*($_GET['page_id']-1)):'';
											 foreach($getMyJobs as $keyJobs=>$allJobs){
											 	$siNo				=	$page_id+($keyJobs+1);
												?>
                                        <div class="t_data show<?php echo $objCommon->html2text($allJobs['em_id']);?> <?php echo ($allJobs['em_id']===(int)$show)?' active':''?>">
                                        	<div class="data_hed <?php echo ($allJobs['em_read_status']!=1)?'unread':''?>">
                                            	<div class="hash left17">
                                                	<span class="count"><?php echo $siNo?></span>
                                                    <div class="details" data-booking="<?php echo $objCommon->html2text($allJobs['em_id']);?>"  data-read="<?php echo $objCommon->html2text($allJobs['em_read_status']);?>">Details <span class="fa fa-angle-down"></span></div>                                              
                                                </div>
                                                <div class="name left55"><a href="<?php echo SITE_ROOT.$objCommon->html2text($allJobs['usl_fameuz'])?>" title="<?php echo $objCommon->displayName($allJobs);?>"><?php echo $objCommon->displayName($allJobs);?></a></div>
                                                <div class="e_date left15"><?php echo date("d M Y",strtotime($allJobs['em_createdon']));?></div>
                                                <div class="delete left8">
													<a data-delid="<?php echo $objCommon->html2text($allJobs['em_id'])?>" class="delete_booking" href="javascript:;"><i class="fa fa-trash"></i></a>
												</div>
                                                <div class="clr"></div>
                                            </div>
                                            <div class="data_content">
                                            	<div class="main_content">
                                                	<p><?php echo $objCommon->html2text($allJobs['em_message']);?></p>
													<div class="clr"></div>
                                                </div>
                                                
                                                <div class="clr"></div>
                                            </div>
                                        </div>
										<?php
										}
										echo '<div class="paginationDiv">'.$pagination_output.'</div>';
										 }else{
											 echo '<p> No Results found... </p>';
										 }
										 ?>
                                    </div>
                                </div>
                            </div>
                            </div>
                            </div> 
                            <div class="clr"></div>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
if($show){
?>
<script type="text/javascript">
$.get("<?php echo SITE_ROOT?>ajax/change_contact_message_read_status.php",{"bookId":'<?php echo $show?>'},function(){	
});
</script>
<?php
}
?>
    <script type="text/javascript">
		$(document).ready(function(e) {
			var	showDiv		=	'<?php echo $show?>';
			if(showDiv){
				$(".show"+showDiv).toggleClass('active');
			}
			$('.details').click(function(e) {
				var bookId		=	$(this).data("booking");
				var read		  =	$(this).data("read");
				$(this).parent().parent().removeClass("unread");
				if(bookId !='' && read =='0'){
					$.get("<?php echo SITE_ROOT?>ajax/change_contact_message_read_status.php",{"bookId":bookId,"read":read},function(){	
					});
				}
				var t_data = $(this).parent().parent().parent();
				t_data.toggleClass('active');
				t_data.siblings().removeClass('active');
				return false;
            });
			$('.user_profile .sidebar .booking .t_data .data_hed .hash .detail_setting').click(function(e) {
				$(this).find('.setting_option').fadeToggle();
            });
			
			$('.delete_booking').click(function(){
				var delid = $(this).data("delid");
				var elem = $(this).closest('.item');
				var that			=	this;
				$.confirm({
					'title'		: 'Delete Confirmation',
					'message'	: 'You are about to delete this item ?',
					'buttons'	: {
					'Yes'	: {
					'class'	: 'blue',
					'action': function(){
					elem.slideUp();
					window.location.href = '<?php echo SITE_ROOT?>access/delete_contact_message.php?action=del_job&jobId='+delid;
					}
					},
					'No'	: {
					'class'	: 'gray',
					'action': function(){}	// Nothing to do in this case. You can as well omit the action property.
					}
					}
				});
			
			});
        });
	</script>
<?php
include_once(DIR_ROOT."includes/footer.php");
?>
  