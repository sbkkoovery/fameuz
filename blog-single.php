<?php
include_once("includes/site_root.php");
include_once(DIR_ROOT."includes/header.php");
include_once(DIR_ROOT."includes/session_check.php");
$blogId								  =	$objCommon->esc($_GET['blogId']);
if($blogId){
	$adIdExpl							=	explode("-",$blogId);
	$getAdId							 =	end($adIdExpl);
	$getAdAliasArr					   =	array_slice($adIdExpl, 0, -1);
	$getAdAlias						  =	implode("-",$getAdAliasArr);
	$getBlogs					  		=	$objUsers->getRowSql("SELECT b.*,user.first_name,user.last_name,user.display_name,user.email,profileImg.upi_img_url,social.usl_fameuz,cat.c_name,group_concat(l.user_id) AS likeUser
										FROM blogs AS b  
										LEFT JOIN users AS user ON b.user_id = user.user_id
										LEFT JOIN user_social_links AS social ON user.user_id=social.user_id
										LEFT JOIN personal_details AS personal ON user.user_id=personal.user_id
										LEFT JOIN user_categories AS ucat ON user.user_id=ucat.user_id
										LEFT JOIN category as cat ON ucat.uc_c_id = cat.c_id 
										LEFT JOIN user_profile_image as profileImg ON user.user_id=profileImg.user_id AND profileImg.upi_status=1
										LEFT JOIN blogs_like AS l ON b.blog_id = l.blog_id AND l.bl_status = 1
										WHERE user.status=1 AND user.email_validation=1  AND b.blog_status=1  AND b.blog_id=".$getAdId." AND b.blog_alias ='".$getAdAlias."' GROUP BY b.blog_id ");
	if($getBlogs['blog_id'] ==''){
		header("loaction:".SITE_ROOT.'blog');
		exit;
	}
}else{
	header("loaction:".SITE_ROOT.'blog');
	exit;
}
?>
<div class="inner_content_section col-spcl">
  <div class="container" style="position:relative;">
  	<div class="border-top-s"></div>
    <div class="inner_top_border">
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-sp-9">
        <div class="jobs-head">
		<?php include_once(DIR_ROOT."widget/blog_search_widget.php");?>  
        </div>
<?php
	$likeUser					=	$getBlogs['likeUser'];
	$likeUserArr				 =	'';
	$youLike			 		 =	'0';
	if($likeUser){
		$likeUserArr			 =	explode(",",$likeUser);
		$likeUserArr			 =	array_filter($likeUserArr);
		$likeCounts			  =	count($likeUserArr).' Likes';
		if(in_array($_SESSION['userId'],$likeUserArr)){
			$youLike			 =	'1';
		}else{
			$youLike			 =	'0';
		}
	}else{
		$likeCounts			  =	'0 Likes';
	}
?>
            <div class="blog">
            	<div class="blog-sec">
				<div class="blog-img-single">
					<div class="row">
						<div class="col-sm-12">
							
						</div>
					</div>
					<div class="row marginBtm10">
						<div class="col-sm-2 col-md-2 alt-width-user-img">
							<div class="img-user-blog">
								<a href="<?php echo SITE_ROOT.$objCommon->html2text($getBlogs['usl_fameuz'])?>"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($getBlogs['upi_img_url'])?$getBlogs['upi_img_url']:'profile_pic.jpg'?>" /></a>
							</div>
						 </div>
						<div class="col-sm-10 col-md-4 alt-padding-l">
							<div class="blog-user-desc">
								<p><a href="<?php echo SITE_ROOT.$objCommon->html2text($getBlogs['usl_fameuz'])?>"><?php echo $objCommon->displayName($getBlogs)?></a></p>
							</div>
							<div class="place-job">
								<p>
									<span class="proffesion"></span>
									<span><?php echo $objCommon->html2text($getBlogs['c_name'])?></span>
								</p>
							</div>
						</div>
						<div class="col-sm-12 col-md-6 alt-padding-l">
							
						</div>
                    </div>
						<?php
						if($getBlogs['blog_img']){
						?>
                            <img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/blogs/<?php echo $objCommon->html2text($getBlogs['blog_img'])?>" class="img-responsive" />
						<?php
						}
						?>
                  </div>
                    <div class="blog-info">
                    	<div class="user-info-blog">
                        	<div class="blog-head">
								<p><?php echo $objCommon->html2text($getBlogs['blog_title'])?></p>
							</div>
                            <div class="blog-info-more">
								<p class="text-left"><span><?php echo date("F d, Y",strtotime($getBlogs['created_time']))?></span><span class="commentcount"> 0 Comments </span><span class="likecount<?php echo $getBlogs['blog_id']?>"> <?php echo $likeCounts?> </span></p>
							</div>
                            <div class="blog-desc">
                            	<p><?php echo $objCommon->html2text($getBlogs['blog_descr'])?></p>
                            </div>
                            <div class="like-comment-share-blog">
                            	<div class="row">
                                	<div class="col-sm-2 alt-width-like-blog">
                                    	<div class="like-blog">
                                        	<a href="javascript:;" data-like="<?php echo $getBlogs['blog_id']?>" class="like <?php echo ($youLike==1)?'liked-blog':''?>"><i class="fa fa-heart"></i><span><?php echo ($youLike==1)?'Liked':'Like'?></span></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-lg-6">
                                    <div class="like-blog">
                                        	<a href="javascript:;"  data-share="<?php echo $getBlogs['blog_id']?>" data-blogby="<?php echo $getBlogs['user_id']?>" class="shareBlog"><i class="fa fa-share-alt"></i>Share</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-4 pull-right">
                                    	<!--<div class="share-btns pull-right">
                                        	<a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                            <a href="#"><i class="fa fa-instagram"></i></a>
                                            <a href="#" class="email-blog pull-right"><i class="fa fa-envelope"></i><span>Email</span></a>
                                        </div>--> <!-- social share commeted -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="arw-downs">
                	<img src="<?php echo SITE_ROOT_AWS_IMAGES ?>images/arw-up-msg-1.png" />
                </div>
                </div>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery.form.js"></script>
<script src="<?php echo SITE_ROOT_AWS_CDN?>js/jquery.elastic.source.js" type="text/javascript" charset="utf-8"></script>
<div id="preview"></div>
				<div class="col-sm-12 no-padding bg_white">
					<div class="comment_box_outer">
						<div class="comment_head"><h1>All Comments </h1></div>
						<div class="working_comments">
							<div class="thumb"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>uploads/profile_images/<?php echo($getUserDetails['upi_img_url'])?$getUserDetails['upi_img_url']:'profile_pic.jpg'?>" alt="thumb"></div>
							<div class="comments">
								<div class="comments_arrow"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/comments_arrow.png" alt="comments_arrow"></div>
								<form id="comment_form" method="post" action="<?php echo SITE_ROOT?>access/post_blog_comment.php?action=add_comment&blogId=<?php echo $blogId?>&userto=<?php echo $getBlogs['user_id']?>">
									<textarea placeholder="Your comment...."  name="comment_descr_input" class="comment_descr_input1" rows="1"></textarea>
									<div class="ajaxloader text-center"><img src="<?php echo SITE_ROOT_AWS_IMAGES?>images/ajax-loader.gif" alt="comments_arrow"></div> 
								</form>
								
								<div class="clr"></div>
							</div>
						</div>
						<div class="loadCommentBox"></div>
					</div>
				</div>
            </div>
       </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-sp-3">
          <?php
		  include_once('widget/right_ads_sidebar.php');
		  ?>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo SITE_ROOT_AWS_CDN?>js/fixed_sidebar.js"></script>
<?php
include_once(DIR_ROOT."js/include/blog_single_js.php");
include_once(DIR_ROOT."includes/footer.php");
?>
